#!/bin/bash
mvn -B -U clean verify -DskipTests=true -Dcheckstyle.skip=true -Djacoco.skip=true -Dfindbugs.skip=true -Penable-owasp
