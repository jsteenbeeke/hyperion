package com.jeroensteenbeeke.hyperion.solitary;

import com.jeroensteenbeeke.hyperion.util.Randomizer;
import io.vavr.control.Option;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

class H2RunnerTest {
	@Test
	public void testH2Runner() throws SQLException, IOException {
		var runner = new H2Runner(1337, Randomizer.random(12), "Hyperion-Solitary", Option. <InputStream> none());

		var startOk = runner.start(false);

		assertTrue(startOk.isOk());

		var stopOk = runner.stop(false);

		assertTrue(stopOk.isOk());


	}

}
