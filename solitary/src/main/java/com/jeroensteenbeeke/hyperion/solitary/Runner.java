package com.jeroensteenbeeke.hyperion.solitary;

import com.jeroensteenbeeke.lux.ActionResult;

import java.util.HashMap;
import java.util.Map;

/**
 * Standalone process involved in running embedded server
 */
interface Runner {
	/**
	 * Starts the process
	 * @param consoleAvailable Whether or not the console is available
	 * @return An ActionResult indicating success or the reason for failure
	 */
	ActionResult start(boolean consoleAvailable);

	/**
	 * Stops the process
	 * @param consoleAvailable Whether or not the console is available
	 * @return An ActionResult indicating success or the reason for failure
	 */
	ActionResult stop(boolean consoleAvailable);

	/**
	 * Returns a set of configuration values relevant for making other Hyperion components (primarily Solstice) work
	 * with this runner
	 * @return A map of configuration values
	 */
	default Map<String,String> getApplicationConfiguration() {
		return new HashMap<>();
	}
}
