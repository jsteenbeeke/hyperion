package com.jeroensteenbeeke.hyperion.solitary;

import com.jeroensteenbeeke.hyperion.ConfigContext;
import com.jeroensteenbeeke.hyperion.util.Randomizer;
import com.jeroensteenbeeke.lux.ActionResult;
import io.vavr.collection.Array;
import io.vavr.control.Option;
import org.danekja.java.util.function.serializable.SerializableConsumer;
import org.eclipse.jetty.ee10.webapp.WebAppContext;
import org.eclipse.jetty.server.Server;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.util.*;

/**
 * Class for running web applications in-memory. Configures an embedded Jetty instance, and spins up
 * either an in-memory H2 instance, or a PostgreSQL docker container.
 */
public class InMemory {
	private static final Logger log = LoggerFactory.getLogger(InMemory.class);

	private static final String GENERATED_USERNAME = Randomizer.random(12);

	private static final String DATABASE_PASSWORD = Randomizer.random(32);

	private static final int DATABASE_MINIMUM_PORT_OFFSET = 1120; // Webserver on 8080 should yield database
	// on 9200

	/**
	 * Start building the application launcher
	 *
	 * @param applicationName The name of the application to launch
	 * @return A builder object to continue the configuration
	 */
	public static InMemoryBuilder run(String applicationName) {
		return new InMemoryBuilder(applicationName);
	}


	/**
	 * Builder class for specifying the context path of the application
	 */
	public static class InMemoryBuilder {
		private final String applicationName;

		private InMemoryBuilder(String applicationName) {
			this.applicationName = applicationName;
		}

		/**
		 * Set the context path of the application
		 *
		 * @param contextPath The context path, generally this is something like {@code /} or
		 *                    {@code /appname}
		 * @return A finalizer class for additional options and starting the server
		 */
		public InMemoryFinalizer withContextPath(@NotNull String contextPath) {
			return new InMemoryFinalizer(applicationName, contextPath);
		}
	}

	/**
	 * Finalizer class to finish starting up the web application. Allows caller to pass
	 * additional options and settings
	 */
	public static class InMemoryFinalizer {
		private final String applicationName;

		private final String contextPath;

		private final Map<String, String> props;

		private boolean showSql = true;

		private boolean consoleAvailable = true;

		private DatabaseType databaseType = DatabaseType.H2;

		private final List<SerializableConsumer<WebAppContext>> contextConsumers = new LinkedList<>();

		private final List<SerializableConsumer<Server>> onServerStarted = new LinkedList<>();

		private final List<SerializableConsumer<Server>> onServerStopped = new LinkedList<>();

		private InputStream databasePrototype;

		private String module = null;

		private int securePort = 8484;

		private InMemoryFinalizer(String applicationName, String contextPath) {
			this.applicationName = applicationName;
			this.contextPath = contextPath;
			this.props = new HashMap<>();
		}

		/**
		 * Disables console output (which may be distracting during tests), logging instead
		 * only to SLF4J
		 *
		 * @return The current finalizer object
		 */
		public InMemoryFinalizer withoutConsoleOutput() {
			this.consoleAvailable = false;
			return this;
		}

		/**
		 * Add a context consumer, which allows the calling code to perform additional operations on
		 * Jetty's WebAppContext object
		 *
		 * @param consumer A consumer that accepts a WebAppContext
		 * @return The current finalizer object
		 */
		public InMemoryFinalizer withContextConsumer(
				SerializableConsumer<WebAppContext> consumer) {
			contextConsumers.add(consumer);
			return this;
		}

		/**
		 * Sets up the in-memory launcher to use PostgreSQL instead of H2
		 *
		 * @return The current finalizer object
		 */
		public InMemoryFinalizer withDockerizedPostgres() {
			this.databaseType = DatabaseType.Postgres;
			return this;
		}

		/**
		 * Sets up the in-memory launcher to not start a database at all
		 *
		 * @return The current finalizer object
		 */
		public InMemoryFinalizer withoutDatabase() {
			this.databaseType = DatabaseType.None;
			return this;
		}

		/**
		 * Add an additional configuration property to the hyperion config file. This is generally
		 * consumed from Spring
		 *
		 * @param key   The key of the property
		 * @param value The value of the property
		 * @return The current finalizer object
		 */
		public InMemoryFinalizer withProperty(String key, String value) {
			this.props.put(key, value);
			return this;
		}

		/**
		 * Add a listener that is invoked on server start
		 *
		 * @param listener The listener to invoke
		 * @return The current finalizer object
		 */
		public InMemoryFinalizer withStartListener(SerializableConsumer<Server> listener) {
			onServerStarted.add(listener);
			return this;
		}

		/**
		 * Add a listener that is invoked on server stop
		 *
		 * @param listener The listener to invoke
		 * @return The current finalizer object
		 */
		public InMemoryFinalizer withStopListener(SerializableConsumer<Server> listener) {
			onServerStopped.add(listener);
			return this;
		}


		/**
		 * Disables SQL query output from Hibernate
		 *
		 * @return The current finalizer object
		 */
		public InMemoryFinalizer withoutShowingSql() {
			this.showSql = false;
			return this;
		}

		/**
		 * Sets a prototype file to use for creating the database
		 *
		 * @param inputStream A stream for the prototype file
		 * @return The current finalizer object
		 */
		public InMemoryFinalizer withDatabasePrototype(InputStream inputStream) {
			this.databasePrototype = inputStream;
			return this;
		}

		/**
		 * Marks the current context as being part of a Maven module
		 *
		 * @param module The module this context is running in
		 * @return The current finalizer object
		 */
		public InMemoryFinalizer inModule(@NotNull String module) {
			this.module = module;
			return this;
		}

		/**
		 * Sets the HTTPS port (default 8484)
		 *
		 * @param securePort The secure port
		 * @return This finalizer
		 */
		public InMemoryFinalizer withSecurePort(int securePort) {
			this.securePort = securePort;
			return this;
		}


		/**
		 * Starts running Jetty at the given port
		 *
		 * @param port The TCP port to listen on
		 * @return Optionally a handler
		 * @throws Exception If launching the server fails
		 */
		public Optional<Handler> atPort(int port) throws Exception {
			System.setProperty("server.base.url", "https://localhost:%d%s".formatted(securePort, contextPath));

			int dbPort = port + DATABASE_MINIMUM_PORT_OFFSET + new Random().nextInt(1000);

			if (consoleAvailable) {
				log.info(">>> STARTING STANDALONE JETTY SERVER");
				log.info("");
				log.info("\tApplication            : " + applicationName);
				log.info("\tPort                   : " + port);
				log.info("\tContext Path           : " + contextPath);

				if (databaseType != DatabaseType.None) {
					log.info("\tDatabase Port          : " + dbPort);
				}
			}

			List<Runner> runners = new ArrayList<>(2);
			if (databaseType == DatabaseType.Postgres) {
				runners.add(new PostgresDockerRunner(GENERATED_USERNAME, DATABASE_PASSWORD, applicationName, dbPort));
			} else if (databaseType == DatabaseType.H2) {
				runners.add(new H2Runner(dbPort, DATABASE_PASSWORD, applicationName, Option.of(databasePrototype)));
			}
			runners.add(new JettyRunner(module, contextPath, port, securePort, contextConsumers, onServerStarted,
					onServerStopped));

			runners.stream().map(Runner::getApplicationConfiguration).forEach(props::putAll);

			if (initConfig(consoleAvailable, applicationName, port, contextPath, showSql, props)) {
				if (consoleAvailable) {
					log.info("");
				}

				for (Runner runner : runners) {
					ActionResult start = runner.start(consoleAvailable);

					if (!start.isOk()) {
						log.error(start.getMessage());
						return Optional.empty();
					}
				}

				return Optional.of(new Handler(consoleAvailable, runners));

			} else {
				if (consoleAvailable) {
					System.err.println("Could not start in-memory servers");
				} else {
					log.error("Could not start in-memory servers");
				}
				System.exit(-1);
				return Optional.empty();
			}
		}
	}

	/**
	 * Callback for controlling how the server should be shut down
	 */
	public static class Handler {
		private final boolean consoleAvailable;
		private final List<Runner> runners;

		/**
		 * Creates a new handler for the given server and database port
		 *
		 * @param consoleAvailable Whether or not console output is allowed
		 * @param runners          All active runners
		 */
		Handler(boolean consoleAvailable, List<Runner> runners) {
			this.consoleAvailable = consoleAvailable;
			this.runners = runners;
		}

		/**
		 * Runs the server until a user provides a keypress on standard in
		 */
		public void waitForKeyPress() {
			if (!consoleAvailable) {
				throw new IllegalStateException("Cannot wait for keypress without console");
			}
			log.info(
					">>> INMEMORY RUNNER INITIALIZED");
			try {
				System.in.read();
				log.info(">>> STOPPING INMEMORY RUNNER");
				for (Runner runner : Array.ofAll(runners).reverse()) {
					runner.stop(consoleAvailable).ifNotOk(System.err::println);
				}

				System.exit(0);
			} catch (Exception e) {
				log.info(">>> INMEMORY RUNNER STOPPED UNEXPECTEDLY <<<");
				log.error(e.getMessage(), e);
				System.exit(-1);
			}
		}

		/**
		 * Shutdown the server manually
		 */
		public void terminate() {
			log.info(">>> STOPPING INMEMORY RUNNER");
			for (Runner runner : Array.ofAll(runners).reverse()) {
				runner.stop(consoleAvailable).ifNotOk(e -> {
					if (consoleAvailable) {
						System.err.println(e);
					} else {
						log.error(e);
					}
				});
			}
		}
	}

	private static synchronized boolean initConfig(
			boolean consoleAvailable, String applicationName,
			int port, String contextPath,
			boolean showSql,
			Map<String, String> extraProperties) {
		try {
			File configDir;
			if (System.getProperty("hyperion.configdir") == null) {
				configDir = Files.createTempDirectory(".hyperion").toFile();
				System.setProperty("hyperion.configdir",
						configDir.getAbsolutePath());
				ConfigContext.Instance.setConfigDirectory(configDir);

			} else {
				configDir = new File(System.getProperty("hyperion.configdir"));
			}

			File config = new File(configDir,
					String.format("%s.properties", applicationName));

			if (consoleAvailable) {
				log.info("\tConfig        : " + config.getAbsolutePath());
			}

			Map<String, String> props = new LinkedHashMap<>();
			props.put("application.name", applicationName);
			props.put("database.showsql", Boolean.toString(showSql));
			props.put("application.baseurl",
					String.format("http://localhost:%d%s", port, contextPath));
			props.put("database.poolsize", "5");
			props.put("database.timeout", "30000");

			extraProperties.forEach(props::put);

			try (FileOutputStream out = new FileOutputStream(config);
				 PrintStream ps = new PrintStream(out)) {
				props.forEach((key, value) -> {
					ps.print(key);
					ps.print("=");
					ps.println(value);
				});
				ps.flush();
			}

			return true;
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			return false;
		}
	}

	private enum DatabaseType {
		H2, Postgres, None;
	}
}
