package com.jeroensteenbeeke.hyperion.solitary;

import com.jeroensteenbeeke.lux.ActionResult;
import com.jeroensteenbeeke.lux.TypedResult;
import io.vavr.Tuple2;
import org.hibernate.dialect.PostgreSQLDialect;
import org.postgresql.xa.PGXADataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

class PostgresDockerRunner implements Runner {
	private static final Logger log = LoggerFactory.getLogger(PostgresDockerRunner.class);

	private final String username;

	private final String password;

	private final String database;

	private final int port;

	private Process databaseProcess;

	PostgresDockerRunner(String username, String password, String database, int port) {
		this.username = username;
		this.password = password;
		this.database = database;
		this.port = port;

		execute("docker info").throwIfNotOk(msg -> new IllegalStateException("Could not launch Docker: " + msg));
	}

	@Override
	public ActionResult start(boolean consoleAvailable) {
		if (consoleAvailable) {
			log.info("Starting Postgres Docker Container");
		}

		execute("docker stop hyperion-inmemory");
		execute("docker rm hyperion-inmemory");

		return ActionResult.attempt(() -> {
			databaseProcess = new ProcessBuilder().command("docker", "run", "-p", port + ":5432",
					"--name", "hyperion-inmemory",
					"-e", "POSTGRES_DB=" + database,
					"-e", "POSTGRES_USER=" + username,
					"-e", "POSTGRES_PASSWORD=" + password,
					"postgres:alpine")
				.redirectErrorStream(true)
				.start();

			databaseProcess.info().totalCpuDuration().map(Duration::toSeconds).ifPresent(seconds -> log.info("Process lasted {} seconds", seconds));

			log.info("Waiting for Postgres to start");

			boolean success = false;
			for (int timeout = 10; timeout > 0; timeout--) {
				Thread.sleep(Duration.ofSeconds(3));

				var dockerLogsProc = new ProcessBuilder().command("docker", "logs", "hyperion-inmemory")
					.redirectErrorStream(true)
					.start();
				int retVal = dockerLogsProc.waitFor();
				if (retVal != 0) {
					log.warn("Docker logs returned {}", retVal);
				}

				String outLog;

				try (var in = dockerLogsProc.getInputStream(); var out = new ByteArrayOutputStream()) {
					in.transferTo(out);
					outLog = out.toString(StandardCharsets.UTF_8);
				}

				if (outLog.contains("database system is ready to accept connections")) {
					success = true;
					break;
				}

				System.out.println(outLog);
			}


			if (!success) {
				throw new IllegalStateException("Postgres container did not start within expected time");
			}

			if (consoleAvailable) {
				log.info("\tConnection command:");
				log.info("\t\tpsql -h localhost -p {} -U {} -W {}", port, username, database);
				log.info("");
				log.info("\tPassword:");
				log.info("\t\t{}", password);
				log.info("");
			}
		});
	}

	@Override
	public ActionResult stop(boolean consoleAvailable) {
		if (consoleAvailable) {
			log.warn("Shutting down Postgres Docker Container");
		}

		return ActionResult.attempt(() -> databaseProcess.destroyForcibly().waitFor());

	}

	@Override
	public Map<String, String> getApplicationConfiguration() {
		Map<String, String> config = new HashMap<>();

		config.put("database.username", username);
		config.put("database.password", password);
		config.put("database.driver", org.postgresql.Driver.class.getName());
		config.put("database.dialect", PostgreSQLDialect.class.getName());
		config.put("database.sourceClass", PGXADataSource.class.getName());
		config.put("database.sslMode", "disable");
		config.put("database.type", "postgres");
		config.put("database.url", String.format("jdbc:postgresql://localhost:%d/%s", port,
			database));

		return config;
	}

	private static TypedResult<Integer> execute(String command) {
		return TypedResult.attempt(() -> {
			Process process = Runtime.getRuntime().exec(command);

			return new Tuple2<>(process, process.waitFor());
		}).filter(t -> t._2 == 0, t -> toError(t._1.getErrorStream())).map(Tuple2::_2);
	}

	private static String toError(InputStream stream) {
		try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
			int i;
			while ((i = stream.read()) != -1) {
				bos.write(i);
			}
			bos.flush();
			bos.close();

			return bos.toString();
		} catch (IOException e) {
			return "Failed to write error output to String: " + e.getMessage();
		}
	}
}
