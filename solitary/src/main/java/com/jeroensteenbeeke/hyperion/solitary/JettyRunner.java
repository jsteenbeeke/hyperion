package com.jeroensteenbeeke.hyperion.solitary;

import com.jeroensteenbeeke.hyperion.util.CryptoUtil;
import com.jeroensteenbeeke.hyperion.util.Randomizer;
import com.jeroensteenbeeke.lux.ActionResult;
import io.vavr.collection.Array;
import io.vavr.control.Option;
import org.danekja.java.util.function.serializable.SerializableConsumer;
import org.eclipse.jetty.ee10.webapp.WebAppContext;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.util.resource.PathResourceFactory;
import org.eclipse.jetty.util.resource.ResourceFactory;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.List;
import java.util.function.Function;

class JettyRunner implements Runner {
	private static final String[] WEB_XML_LOCATIONS = {"WEB-INF", "src/main/webapp/WEB-INF",
		"src/test/webapp/WEB-INF", "src/main/resources/WEB-INF", "src/test/resources/WEB-INF"};

	private static final Logger log = LoggerFactory.getLogger(JettyRunner.class);

	private static final String KEYSTORE_PASSWORD = Randomizer.random(42);

	private static final String KEY_PASSWORD = Randomizer.random(42);

	private final Server server;

	private final List<SerializableConsumer<Server>> onServerStarted;

	private final List<SerializableConsumer<Server>> onServerStopped;

	JettyRunner(
		@Nullable String moduleName,
		@NotNull String contextPath,
		int port,
		int securePort,
		@NotNull List<SerializableConsumer<WebAppContext>> contextConsumers,
		@NotNull List<SerializableConsumer<Server>> onServerStarted,
		@NotNull List<SerializableConsumer<Server>> onServerStopped) throws Exception {
		this.onServerStarted = onServerStarted;
		this.onServerStopped = onServerStopped;

		final int timeout = 1000 * 60 * 60;

		this.server = new Server();

		HttpConfiguration http_config = new HttpConfiguration();
		http_config.setSecurePort(securePort);
		http_config.setSecureScheme("https");

		ServerConnector http = new ServerConnector(server,
			new HttpConnectionFactory(http_config));

		configureHttps(http_config);

		http.setPort(port);
		http.setIdleTimeout(timeout);
		server.addConnector(http);

		WebAppContext context = new WebAppContext();
		context.setContextPath(contextPath);

		URL web_xml = null;
		URL webapp = null;

		Option<Path> modulePath = Option.of(moduleName).map(Path::of);

		Array<Path> candidatePaths = Array
			.of(WEB_XML_LOCATIONS)
			.flatMap(c -> modulePath
				.map(m -> m.resolve(c))
				.map(Array::of)
				.map(a -> a.append(Path.of(c)))
				.getOrElse(() -> Array.of(Path.of(c))));

		for (Path webInfCandidate : candidatePaths) {
			if (Files.isDirectory(webInfCandidate)) {
				Path webXmlCandidate = webInfCandidate.resolve("web.xml");
				if (Files.exists(webXmlCandidate)) {
					webapp = webInfCandidate.getParent().toUri().toURL();
					web_xml = webXmlCandidate.toUri().toURL();

					log.info("web.xml found at {}", webXmlCandidate.toAbsolutePath().toString());
				}
			}
		}

		if (web_xml != null) {
			context.setDescriptor(web_xml.toExternalForm());
			try {
				context.setWar(webapp.toURI().toString());
			} catch (URISyntaxException e) {
				MalformedURLException malformedURLException = new MalformedURLException(e.getMessage());
				malformedURLException.initCause(e);
				throw malformedURLException;
			}
		} else {
			log.info(
				"/webapp resource is missing from jar, defaulting to src/main/webapp");
			context.setWar("src/main/webapp");
		}

		server.setHandler(context);

		contextConsumers.forEach(c -> c.accept(context));
	}

	private void configureHttps(HttpConfiguration config) throws Exception {
		if ("true".equals(System.getProperty("testmode"))) {
			return;
		}

		Path hyperionDir = Path.of(System.getProperty("user.home")).resolve(".hyperion");
		Path keyFile = hyperionDir.resolve("localhost.key");
		Path certFile = hyperionDir.resolve("localhost.crt");

		boolean keyFileExists = Files.exists(keyFile);
		boolean certFileExists = Files.exists(certFile);

		if (keyFileExists && certFileExists) {
			Path keyStorePath = Files.createTempFile("jetty-runner", ".keystore");
			KeyStore keyStore = KeyStore.getInstance("JKS");
			keyStore.load(null, null);

			Function<Throwable, IllegalArgumentException> wrapThrowable = IllegalArgumentException::new;
			Certificate certificate = CryptoUtil.readSSLCertificate(certFile).getOrElseThrow(wrapThrowable);
			PrivateKey privateKey = CryptoUtil.readPrivateKey(keyFile).getOrElseThrow(wrapThrowable);

			keyStore.setCertificateEntry("wicket", certificate);
			keyStore.setKeyEntry("wicket", privateKey, KEY_PASSWORD.toCharArray(), new Certificate[]{certificate});

			try (OutputStream out = Files.newOutputStream(keyStorePath)) {
				keyStore.store(out, KEYSTORE_PASSWORD.toCharArray());
				out.flush();
			}

			ResourceFactory resourceFactory = new PathResourceFactory();
			SslContextFactory.Server sslContextFactory = new SslContextFactory.Server();
			sslContextFactory.setKeyStoreResource(resourceFactory.newResource(keyStorePath));
			sslContextFactory.setKeyStorePassword(KEYSTORE_PASSWORD);
			sslContextFactory.setKeyManagerPassword(KEY_PASSWORD);

			HttpConfiguration https_config = new HttpConfiguration(config);
			https_config.addCustomizer(new SecureRequestCustomizer());

			ServerConnector https = new ServerConnector(server,
				sslContextFactory, new HttpConnectionFactory(https_config));
			https.setPort(config.getSecurePort());
			https.setIdleTimeout(500000);

			server.addConnector(https);

		} else {
			if (!keyFileExists) {
				log.warn("Missing {} in {}, cannot configure HTTPS", keyFile.getFileName().toString(), hyperionDir
					.toAbsolutePath()
					.toString());
			}
			if (!certFileExists) {
				log.warn("Missing {} in {}, cannot configure HTTPS", certFile.getFileName().toString(), hyperionDir
					.toAbsolutePath()
					.toString());
			}
		}
	}

	@Override
	public ActionResult start(boolean consoleAvailable) {
		return ActionResult.attempt(() -> {
			if (consoleAvailable) {
				log.info("Starting embedded Jetty server");
			}

			server.start();
			onServerStarted.forEach(l -> l.accept(server));
		});
	}

	@Override
	public ActionResult stop(boolean consoleAvailable) {
		return ActionResult.attempt(() -> {
			if (consoleAvailable) {
				log.info("Stopping embedded Jetty server");
			}
			server.stop();

			onServerStopped.forEach(l -> l.accept(server));

			server.join();
		});
	}
}
