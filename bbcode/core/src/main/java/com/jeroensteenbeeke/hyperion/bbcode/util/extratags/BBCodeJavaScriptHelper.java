/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util.extratags;

import java.util.Optional;

/**
 * Helper class for creating JS calls to initialize extra tag buttons for a BBCode input
 */
public final class BBCodeJavaScriptHelper {

	/**
	 * Create a simple tag button, which renders a tag as {@code [tag][/tag]}
	 * @param tag The tag to render
	 * @return An optional containing a Javascript call to create a button for the given tag
	 */
	public static Optional<String> createSimpleButton(String tag) {
		return Optional.of(String.format("createInsertButton('%s', '[%s]', '[/%s]')", tag,
				tag, tag));
	}

	/**
	 * Creates a more complicated tag button, which does not enforce standard square bracket positions
	 * @param descriptor The descriptor of the button
	 * @param openTag The text to place before the cursor
	 * @param closeTag The text to place behind the cursor
	 * @return An optional containing a Javascript call to create a button for the given tag
	 */
	public static Optional<String> createAdvancedButton(String descriptor,
											  String openTag, String closeTag) {
		return Optional.of(String.format("createInsertButton('%s', '%s', '%s')",
				descriptor, openTag, closeTag));

	}
}
