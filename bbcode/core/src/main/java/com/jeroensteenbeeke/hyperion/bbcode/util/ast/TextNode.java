/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util.ast;

import com.jeroensteenbeeke.hyperion.bbcode.util.BBAstNode;
import com.jeroensteenbeeke.hyperion.bbcode.util.DefaultNode;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * AST node that represents a piece of normal unadorned text. Generally speaking you should not
 * need to
 * create instances of this class yourself
 */
public class TextNode extends DefaultNode {

	private final String text;

	/**
	 * Create a new TextNode
	 *
	 * @param parent The parent to place this node under, or {@code null} if this node is a root
	 *               node
	 * @param text   The text to display
	 */
	public TextNode(
			@Nullable
					BBAstNode parent,
			@NotNull
					String text) {
		super(parent);

		this.text = text;
	}

	@Override
	public void renderTo(
			@NotNull
					StringBuilder builder, Integer targetCharacters) {
		if (targetCharacters != null) {

			for (char c : text.toCharArray()) {
				if (limitReached(builder, targetCharacters)
						&& Character.isWhitespace(c)) {
					break;
				}

				if (c == '\n') {
					builder.append("<br />");
				} else {
					builder.append(c);
				}
			}

		} else {
			builder.append(text.replaceAll("\\n", "<br />"));
		}
	}

	@Override
	public void renderForEditor(
			@NotNull
					StringBuilder builder) {
		builder.append(text);
	}

}
