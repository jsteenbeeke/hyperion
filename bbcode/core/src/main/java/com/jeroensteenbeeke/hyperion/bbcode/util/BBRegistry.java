/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util;

/**
 * Registry singleton for BBCode-specific configuration, such as the origin to use for embedded YouTube videos
 */
public class BBRegistry {
	private static final BBRegistry instance = new BBRegistry();

	private String origin = null;

	/**
	 * Get the origin to use in YouTube embed fragments
	 * @return The origin to use
	 */
	public static String getOrigin() {
		return instance.origin;
	}

	/**
	 * Set the origin to use in YouTube embed fragments
	 * @param origin The origin to set
	 */
	public static void setOrigin(String origin) {
		instance.origin = origin;
	}

}
