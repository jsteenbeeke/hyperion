/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util;

import com.jeroensteenbeeke.hyperion.bbcode.util.scope.TagScope;
import com.jeroensteenbeeke.lux.ActionResult;
import org.jetbrains.annotations.NotNull;

import java.io.Serial;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Default BBCode tags supported by every editor
 */
public enum DefaultBBTags implements IBBCodeTag {
	YOUTUBE("[youtube]", "[/youtube]") {
		@NotNull
		@Override
		public ActionResult parse(@NotNull BBParser parser, BBAstNode current, @NotNull ParseBuffer buffer) {
			return parser.parseYoutube(current, buffer);
		}
	},
	BOLD("[b]", "[/b]") {
		@NotNull
		@Override
		public ActionResult parse(@NotNull BBParser parser, BBAstNode current, @NotNull ParseBuffer buffer) {
			return parser.parseBold(current, buffer);
		}
	},
	UNDERLINE("[u]", "[/u]") {
		@NotNull
		@Override
		public ActionResult parse(@NotNull BBParser parser, BBAstNode current, @NotNull ParseBuffer buffer) {
			return parser.parseUnderline(current, buffer);
		}
	},
	ITALIC("[i]", "[/i]") {
		@NotNull
		@Override
		public ActionResult parse(@NotNull BBParser parser, BBAstNode current, @NotNull ParseBuffer buffer) {
			return parser.parseItalic(current, buffer);
		}
	},
	STRIKETHROUGH("[s]", "[/s]") {
		@NotNull
		@Override
		public ActionResult parse(@NotNull BBParser parser, BBAstNode current, @NotNull ParseBuffer buffer) {
			return parser.parseStrikeThrough(current, buffer);
		}
	},
	QUOTE("[quote", "[/quote]") {
		@NotNull
		@Override
		public ActionResult parse(@NotNull BBParser parser, BBAstNode current, @NotNull ParseBuffer buffer) {
			return parser.parseQuote(current, buffer);
		}
	},
	URL("[url", "[/url]") {
		@NotNull
		@Override
		public ActionResult parse(@NotNull BBParser parser, BBAstNode current, @NotNull ParseBuffer buffer) {
			return parser.parseUrl(current, buffer);
		}
	},
	IMG("[img", "[/img]") {
		@NotNull
		@Override
		public ActionResult parse(@NotNull BBParser parser, BBAstNode current, @NotNull ParseBuffer buffer) {
			return parser.parseImage(current, buffer);
		}
	};

	private final String opening;

	final String terminator;

	DefaultBBTags(String opening, String terminator) {
		this.opening = opening;
		this.terminator = terminator;
	}

	@Override
	public boolean canParse(BBTokenStream stream) {

		return opening.equals(stream.peekTokens(opening.length()));
	}

	@NotNull
	@Override
	public String getTerminator() {
		return terminator;
	}

	@NotNull
	@Override
	public Optional<String> getCreateButtonJavascript() {
		// Default implementation already contains these buttons, no added
		// Javascript required
		return Optional.empty();
	}

	@Override
	public String toString() {
		return opening.endsWith("]") ? opening : opening + "]";
	}

	/**
	 * Tag scope that encompasses all default tags
	 */
	public static TagScope SCOPE = new TagScope() {
		@Serial
		private static final long serialVersionUID = 1L;

		@Override
		@NotNull
		public Set<? extends IBBCodeTag> getTags() {
			return Arrays.stream(DefaultBBTags.values()).collect(Collectors.toUnmodifiableSet());
		}
	};
}
