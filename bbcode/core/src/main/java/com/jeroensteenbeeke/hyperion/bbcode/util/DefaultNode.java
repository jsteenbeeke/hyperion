/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * Default implementation of the BBAstNode class, containing logic that generally does
 * not differ between implementations
 */
public abstract class DefaultNode implements BBAstNode {
	private final List<BBAstNode> children = new LinkedList<>();

	private final BBAstNode parent;

	/**
	 * Create a new node with (optionally), the given node as parent
	 *
	 * @param parent The parent to place this node under, or {@code null} if this node is a root
	 *               node
	 */
	protected DefaultNode(@Nullable BBAstNode parent) {
		this.parent = parent;
	}

	@Override
	public void addChild(BBAstNode node) {
		children.add(node);
	}

	@NotNull
	@Override
	public List<BBAstNode> getChildren() {
		return List.copyOf(children);
	}

	@NotNull
	@Override
	public Optional<BBAstNode> getParent() {
		return Optional.ofNullable(parent);
	}

	/**
	 * Checks if the given StringBuilder has reached the given limit (if any)
	 *
	 * @param builder          A StringBuilder that output can be written to
	 * @param targetCharacters An (optional) number of characters that should be the target limit of the StringBuilder
	 * @return {@code true} if either the StringBuilder contains enough content, or no limit was given, {@code false}
	 * if the limit was given but not reached yet
	 */
	protected static boolean limitReached(
			@NotNull StringBuilder builder,
			@Nullable Integer targetCharacters) {
		return targetCharacters != null && builder.length() >= targetCharacters;

	}
}
