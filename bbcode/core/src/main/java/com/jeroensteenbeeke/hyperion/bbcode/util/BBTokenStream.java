/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.util;

import java.util.Optional;

/**
 * Wrapper around input being parsed. Tracks the input and the current position of the input being parsed
 */
public class BBTokenStream {
	private String input;
	private int position;

	BBTokenStream(String input) {
		this.input = input != null ? input : "";
		this.position = 0;
	}

	/**
	 * Moves the pointer by one position
	 */
	public void consumeToken() {
		this.position++;
	}

	/**
	 * Moves the pointer by n positions
	 * @param n The number of positions to move the pointer
	 */
	public void consumeTokens(int n) {
		this.position += n;
	}

	/**
	 * Checks if the end of the input has been reached
	 * @return {@code true} if there is still input left to parse, {@code false} otherwise
	 */
	public boolean hasMoreTokens() {
		return position < input.length();
	}

	/**
	 * Get the next token in the stream. Please ensure to check {@code hasMoreTokens()} before calling this
	 * @return The next character in the stream
	 */
	public char nextToken() {
		return input.charAt(position);
	}

	/**
	 * Gets the next several tokens in the stream. If the lookahead exceeds the number of remaining tokens in the
	 * stream,
	 * the remainder of the stream is returned
	 * @param lookahead The number of tokens to return
	 * @return A String of (at most) {@code lookahead} tokens
	 */
	public String peekTokens(int lookahead) {
		if (input.length() < (position + lookahead)) {
			return input.substring(position);
		}

		return input.substring(position, position + lookahead);
	}

	/**
	 * Skips the given number of tokens
	 * @param count The number of tokens to skip
	 */
	public void skipTokens(int count) {
		this.position = position + count;
	}

	/**
	 * Returns whatever tokens remainin the stream until the given closing character is encountered
	 * @param close The character to check for
	 * @return A String containing all characters until the closing character, or an empty optional, if the end of
	 * stream is reached before reaching the character
	 */
	public Optional<String> peekUntil(char close) {
		StringBuilder lookahead = new StringBuilder();

		for (int i = position; i < input.length(); i++) {
			char next = input.charAt(i);

			if (next == close) {
				return Optional.of(lookahead.toString());
			}

			lookahead.append(next);

		}

		return Optional.empty();
	}

	/**
	 * Calculates the number of tokens remaining
	 * @return The number of tokens left in the stream
	 */
	public int getRemainingTokens() {
		return input.length() - position;
	}

}
