package com.jeroensteenbeeke.hyperion.bbcode.util;

import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.*;
import com.jeroensteenbeeke.hyperion.bbcode.util.scope.TagScope;
import com.jeroensteenbeeke.lux.TypedResult;
import org.jetbrains.annotations.NotNull;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class BBCodeTest {
	static <T> void assertResult(@NotNull T expected, @NotNull TypedResult<T> actual) {
		assertTrue(actual.isOk());
		assertEquals(expected, actual.getObject());
	}

	static <T> void assertFailure(@NotNull String expectedError, @NotNull TypedResult<T> actual) {
		assertFalse(actual.isOk());
		assertEquals(expectedError, actual.getMessage());
	}

	static BBCodeBuilder tag(@NotNull String tag) {
		return new BBCodeBuilder(tag);
	}

	static class BBCodeBuilder {
		private final String tag;

		private BBCodeBuilder(@NotNull String tag) {
			this.tag = tag;
		}

		public String around(@NotNull String text) {
			return String.format("[%s]%s[/%s]", tag, text, tag);
		}
	}

	static class TableTagScope implements TagScope {
		private final boolean showButton;

		TableTagScope(boolean showButton) {
			this.showButton = showButton;
		}

		@NotNull
		@Override
		public Set<? extends IBBCodeTag> getTags() {
			TableCellTag td = new TableCellTag(showButton);
			TableHeaderCellTag th = new TableHeaderCellTag(showButton);
			TableRowTag tr = new TableRowTag(td, th, showButton);
			TableFooterTag tfoot = new TableFooterTag(tr, showButton);
			TableBodyTag tbody = new TableBodyTag(tr, showButton);
			TableHeadTag thead = new TableHeadTag(tr, showButton);
			TableTag table = new TableTag(thead, tbody, tfoot, tr, showButton);

			return Set.of(td, th, tr, tfoot, tbody, thead, table);
		}
	}
}
