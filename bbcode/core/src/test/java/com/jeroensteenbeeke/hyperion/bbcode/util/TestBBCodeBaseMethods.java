package com.jeroensteenbeeke.hyperion.bbcode.util;

import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.HtmlEntityTag;
import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.ListItemTag;
import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.OrderedListTag;
import com.jeroensteenbeeke.hyperion.bbcode.util.extratags.UnorderedListTag;
import com.jeroensteenbeeke.hyperion.bbcode.util.scope.TagScope;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.io.Serial;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class TestBBCodeBaseMethods {
	private static final String BUTTON_REGEX = "createInsertButton\\(\\s*'.*',\\s*'.*',\\s*'.*'\\s*\\)";
	private final TagScope entityScope = new TagScope() {
		@Serial
		private static final long serialVersionUID = 1L;

		@NotNull
		@Override
		public Set<? extends IBBCodeTag> getTags() {
			return Set.of(HtmlEntityTag.INSTANCE);
		}
	};

	private final TagScope listScope = new TagScope() {
		@Serial
		private static final long serialVersionUID = 1L;

		@NotNull
		@Override
		public Set<? extends IBBCodeTag> getTags() {
			return Set.of(OrderedListTag.INSTANCE, UnorderedListTag.INSTANCE, ListItemTag.INSTANCE);
		}
	};

	private final TagScope tableScope = new BBCodeTest.TableTagScope(true);

	private final TagScope noButtonTableScope = new BBCodeTest.TableTagScope(true);

	@Test
	public void testJavascriptCreationScripts() {
		Set<IBBCodeTag> tags = BBCodeUtil
				.allTags(Set.of(entityScope, listScope, tableScope, noButtonTableScope));

		assertFalse(tags.isEmpty());

		for (IBBCodeTag tag : tags) {
			tag.getCreateButtonJavascript().ifPresent(js -> assertTrue(js.matches(BUTTON_REGEX)));
		}

	}

	@Test
	public void testToString() {
		Set<IBBCodeTag> tags = BBCodeUtil
				.allTags(Set.of(entityScope, listScope, tableScope, noButtonTableScope));

		assertFalse(tags.isEmpty());

		for (IBBCodeTag tag : tags) {
			assertFalse(tag.toString().startsWith("com.jeroensteenbeeke"),
					String.format("Tag %s has no toString implementation", tag.getClass().getSimpleName()));
			assertNotEquals(String.format("Tag %s has an empty toString()", tag.getClass().getSimpleName()), "",
					tag.toString());
		}

	}

	@Test
	public void testOpenTag() {
		Set<IBBCodeTag> tags = BBCodeUtil
				.allTags(Set.of(entityScope, listScope, tableScope, noButtonTableScope));

		assertFalse(tags.isEmpty());

		for (IBBCodeTag tag : tags) {
			if (tag instanceof SimpleTag st) {
				assertNotNull(String.format("Tag %s has no getOpenTag()", tag.getClass().getSimpleName()),
						st.getOpenTag());
				assertNotEquals(0, st.getOpenTag().length(),
						String.format("Tag %s returns an empty getOpenTag()", tag.getClass().getSimpleName()));
			}
		}
	}
}
