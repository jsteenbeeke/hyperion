/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.bbcode.web.components;

import org.apache.wicket.markup.head.HeaderItem;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.resource.JQueryPluginResourceReference;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

/**
 * Wicket Resource reference for the BBCode-editor JQuery plugin
 */
public class BBCodeTextAreaPluginResource extends JQueryPluginResourceReference {
	@Serial
	private static final long serialVersionUID = 1L;

	private static final BBCodeTextAreaPluginResource instance = new BBCodeTextAreaPluginResource();

	private BBCodeTextAreaPluginResource() {
		super(BBCodeTextAreaPluginResource.class, "js/bbcode.js");
	}

	@Override
	public List<HeaderItem> getDependencies() {
		List<HeaderItem> deps = new ArrayList<>(super.getDependencies());

		deps.add(JavaScriptHeaderItem
				.forReference(JQueryTextareaCaretPluginResource.get()));

		return List.copyOf(deps);
	}

	/**
	 * Get a singleton instance of this JavaScript resource
	 * @return A singleton instance of the BBCodeTextAreaPluginResource
	 */
	public static BBCodeTextAreaPluginResource get() {
		return instance;
	}
}
