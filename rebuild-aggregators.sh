mvn clean install -pl .,\
deps,\
deps/backend,\
deps/backend-test,\
deps/rest-contract,\
deps/rest-contract-test,\
deps/rest-data,\
deps/rest-data-test,\
deps/retrofit-contract,\
deps/sdk,\
deps/wicket-frontend,\
deps/wicket-frontend-test,\
deps/wicket-standalone,\
deps/wicket-standalone-test,\
deps/keycloak-secured-service,\
deps/keycloak-secured-service-test
