package com.jeroensteenbeeke.hyperion;

import io.vavr.control.Option;
import org.junit.jupiter.api.Test;

import static com.jeroensteenbeeke.hyperion.test.matcher.ComparableMatchers.greaterThan;
import static com.jeroensteenbeeke.hyperion.test.matcher.PatternMatchers.matchesPattern;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

public class HyperionTest {
	@Test
	public void testPresentRevision() {
		assumingThat(Hyperion.class.getResourceAsStream("revision.txt") != null, () -> {
			Option<String> revision = Hyperion.getRevision();

			assertTrue(revision.isDefined());
			assertThat(revision.get(), matchesPattern("^([a-fA-F0-9])+$"));
		});
	}

	@Test
	public void testAbsentRevision() {
		assumingThat(Hyperion.class.getResourceAsStream("revision.txt") == null, () -> {
			Option<String> revision = Hyperion.getRevision();

			assertTrue(revision.isEmpty());
		});
	}

	@Test
	public void testPresentCommitTitle() {
		assumingThat(Hyperion.class.getResourceAsStream("commit.txt") != null, () -> {
			Option<String> revision = Hyperion.getCommitTitle();

			assertTrue(revision.isDefined());
			assertThat(revision.get().length(), greaterThan(0));
		});
	}

	@Test
	public void testAbsentCommitTitle() {
		assumingThat(Hyperion.class.getResourceAsStream("commit.txt") == null, () -> {
			Option<String> revision = Hyperion.getCommitTitle();

			assertTrue(revision.isEmpty());
		});
	}

	@Test
	public void testPresentCommitNotes() {
		assumingThat(Hyperion.class.getResourceAsStream("commit-notes.txt") != null, () -> {
			Option<String> revision = Hyperion.getCommitNotes();

			// Notes can be empty, in which case they also yield an empty option. We cannot test for content
			assertNotNull(revision);
		});
	}

	@Test
	public void testAbsentCommitNotes() {
		assumingThat(Hyperion.class.getResourceAsStream("commit-notes.txt") == null, () -> {
			Option<String> revision = Hyperion.getCommitNotes();

			assertTrue(revision.isEmpty());
		});
	}


	@Test
	public void testSettingsInvocation() {
		HyperionSettings settings = Hyperion.getSettings();

		assertThat(settings, notNullValue());
		assertThat(settings.getUserDisplayName(), notNullValue());
	}
}
