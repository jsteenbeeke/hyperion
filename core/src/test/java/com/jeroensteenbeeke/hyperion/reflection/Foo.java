package com.jeroensteenbeeke.hyperion.reflection;

public class Foo {
	private String bar;

	private final String baz;

	public Foo(String baz) {
		this.baz = baz;
	}

	public String getBar() {
		return bar;
	}

	public void setBar(String bar) {
		this.bar = bar;
	}

	public String getBaz() {
		return baz;
	}
}
