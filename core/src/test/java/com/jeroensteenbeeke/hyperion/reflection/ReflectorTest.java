package com.jeroensteenbeeke.hyperion.reflection;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ReflectorTest {
	@Test
	public void testReflector() {
		List<Property<Foo, ?>> properties = Reflector.getProperties(Foo.class);

		assertEquals(2, properties.size());

		assertTrue(Reflector.instance(Foo.class, "test").flatMap(f -> Reflector.invokeGetter(f, "baz"))
				.map(baz -> {
					assertEquals("test", baz);
					return baz;
				}).isOk());
	}
}
