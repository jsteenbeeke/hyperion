package com.jeroensteenbeeke.hyperion.util;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RandomizerTest {
	private static final int MANY_TIMES = 10000;

	private static final int LENGTH = 50;

	@Test
	public void testRandomizer() {
		Set<String> randoms = new HashSet<>();
		manyTimes(() ->
				assertTrue(randoms.add(Randomizer.random(LENGTH)))
		);

		assertEquals(MANY_TIMES, randoms.size());
	}

	@Test
	public void testCharRandomization() {
		Set<char[]> randoms = new HashSet<>();
		manyTimes(() ->
				assertTrue(randoms.add(Randomizer.randomChars(LENGTH)))
		);

		assertEquals(MANY_TIMES, randoms.size());
	}

	@Test
	public void testByteRandomization() {
		Set<byte[]> randoms = new HashSet<>();
		manyTimes(() ->
				assertTrue(randoms.add(Randomizer.randomBytes(LENGTH)))
		);

		assertEquals(MANY_TIMES, randoms.size());
	}

	private void manyTimes(Runnable operation) {
		for (int i = 0; i < MANY_TIMES; i++) {
			operation.run();
		}

	}
}
