/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.util;

import io.vavr.collection.Array;
import io.vavr.collection.Multimap;
import io.vavr.collection.Seq;
import io.vavr.collection.Traversable;
import org.junit.jupiter.api.Test;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TimeUtilTest {
	@Test
	public void testJanuary2014() {
		List<LocalDate> january = createMonth(2014, 1);

		assertEquals(31, january.size());

		Multimap<Integer, LocalDate> perWeek = TimeUtil.getMonthPerWeek(january
				.get(14));

		assertEquals(31, perWeek.size());
		assertEquals(5, perWeek.keySet().size());

		Traversable<LocalDate> week1 = perWeek.get(1).getOrElse(Array::empty);
		Traversable<LocalDate> week2 = perWeek.get(2).getOrElse(Array::empty);
		Traversable<LocalDate> week3 = perWeek.get(3).getOrElse(Array::empty);
		Traversable<LocalDate> week4 = perWeek.get(4).getOrElse(Array::empty);
		Traversable<LocalDate> week5 = perWeek.get(5).getOrElse(Array::empty);

		assertNotNull(week1);
		assertNotNull(week2);
		assertNotNull(week3);
		assertNotNull(week4);
		assertNotNull(week5);

		assertDates(week1, january, 1, 2, 3, 4, 5);
		assertDates(week2, january, 6, 7, 8, 9, 10, 11, 12);
		assertDates(week3, january, 13, 14, 15, 16, 17, 18, 19);
		assertDates(week4, january, 20, 21, 22, 23, 24, 25, 26);
		assertDates(week5, january, 27, 28, 29, 30, 31);
	}

	@Test
	public void testFebruary2014() {
		List<LocalDate> february = createMonth(2014, 2);

		assertEquals(28, february.size());

		Multimap<Integer, LocalDate> perWeek = TimeUtil
				.getMonthPerWeek(february.get(14));

		assertEquals(28, perWeek.size());
		assertEquals(5, perWeek.keySet().size());

		Traversable<LocalDate> week1 = perWeek.get(5).getOrElse(Array::empty);
		Traversable<LocalDate> week2 = perWeek.get(6).getOrElse(Array::empty);
		Traversable<LocalDate> week3 = perWeek.get(7).getOrElse(Array::empty);
		Traversable<LocalDate> week4 = perWeek.get(8).getOrElse(Array::empty);
		Traversable<LocalDate> week5 = perWeek.get(9).getOrElse(Array::empty);

		assertNotNull(week1);
		assertNotNull(week2);
		assertNotNull(week3);
		assertNotNull(week4);
		assertNotNull(week5);

		assertDates(week1, february, 1, 2);
		assertDates(week2, february, 3, 4, 5, 6, 7, 8, 9);
		assertDates(week3, february, 10, 11, 12, 13, 14, 15, 16);
		assertDates(week4, february, 17, 18, 19, 20, 21, 22, 23);
		assertDates(week5, february, 24, 25, 26, 27, 28);
	}

	private void assertDates(
			Traversable<LocalDate> week, List<LocalDate> month,
			int... days) {
		assertEquals(days.length, week.size());

		for (int day : days) {
			LocalDate date = month.get(day - 1);
			assertTrue(week.contains(date),
					String.format("%s in week %s", date.toString(), week));
		}
	}

	private List<LocalDate> createMonth(int year, int month) {
		final LocalDate first = LocalDate.of(year, month, 1)
				.atStartOfDay().toLocalDate();
		final LocalDate last = first.plusMonths(1).minusDays(1);

		List<LocalDate> dates = new ArrayList<>(31);

		LocalDate next = first;
		while (!next.isAfter(last)) {
			dates.add(next);

			next = next.plusDays(1);
		}

		return dates;
	}

}
