package com.jeroensteenbeeke.hyperion.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AssertsTest {
	@Test
	public void testNumberParam() {
		IllegalArgumentException e = null;

		try {
			Asserts.numberParam("number").withValue(5).atLeast(0);
			Asserts.numberParam("number").withValue(5).atMost(10);
			Asserts.numberParam("number").withValue(5).between(0, 10);
			Asserts.numberParam("number").withValue(5).satisfies(Integer.valueOf(5)::equals);
		} catch (IllegalArgumentException iae) {
			e = iae;
		}

		assertNull(e);
		try {
			Asserts.numberParam("number").withValue(5).atLeast(10);
		} catch (IllegalArgumentException iae) {
			e = iae;
		}

		assertNotNull(e);
		assertEquals("Parameter number is 5, but must be at least 10", e.getMessage());

		try {
			Asserts.numberParam("number").withValue(5).atMost(1);
		} catch (IllegalArgumentException iae) {
			e = iae;
		}

		assertNotNull(e);
		assertEquals("Parameter number is 5, but must be at most 1", e.getMessage());

		try {
			Asserts.numberParam("number").withValue(5).between(1, 4);
		} catch (IllegalArgumentException iae) {
			e = iae;
		}

		assertNotNull(e);
		assertEquals("Parameter number is 5, but must be between 1 and 4", e.getMessage());

		try {
			Asserts.numberParam("number").withValue(5).satisfies(i -> false);
		} catch (IllegalArgumentException iae) {
			e = iae;
		}

		assertNotNull(e);
		assertEquals("Invalid value for number: 5", e.getMessage());
	}

	@Test
	public void testNumberVariable() {
		IllegalArgumentException e = null;

		try {
			Asserts.numberVariable("number").withValue(5).atLeast(0);
			Asserts.numberVariable("number").withValue(5).atMost(10);
			Asserts.numberVariable("number").withValue(5).between(0, 10);
			Asserts.numberVariable("number").withValue(5).satisfies(Integer.valueOf(5)::equals);
		} catch (IllegalArgumentException iae) {
			e = iae;
		}

		assertNull(e);
		try {
			Asserts.numberVariable("number").withValue(5).atLeast(10);
		} catch (IllegalArgumentException iae) {
			e = iae;
		}

		assertNotNull(e);
		assertEquals("Variable number is 5, but must be at least 10", e.getMessage());

		try {
			Asserts.numberVariable("number").withValue(5).atMost(1);
		} catch (IllegalArgumentException iae) {
			e = iae;
		}

		assertNotNull(e);
		assertEquals("Variable number is 5, but must be at most 1", e.getMessage());

		try {
			Asserts.numberVariable("number").withValue(5).between(1, 4);
		} catch (IllegalArgumentException iae) {
			e = iae;
		}

		assertNotNull(e);
		assertEquals("Variable number is 5, but must be between 1 and 4", e.getMessage());

		try {
			Asserts.numberVariable("number").withValue(5).satisfies(i -> false);
		} catch (IllegalArgumentException iae) {
			e = iae;
		}

		assertNotNull(e);
		assertEquals("Invalid value for number: 5", e.getMessage());
	}

	@Test
	public void testObjectParam() {
		IllegalArgumentException e = null;

		try {
			Asserts.objectParam("obj").notNull("I am an object");
		} catch (IllegalArgumentException iae) {
			e = iae;
		}

		assertNull(e);

		try {
			Asserts.objectParam("obj").notNull(null);
		} catch (IllegalArgumentException iae) {
			e = iae;
		}

		assertNotNull(e);
		assertEquals("Parameter obj should not be null", e.getMessage());
	}

	@Test
	public void testObjectVar() {
		IllegalArgumentException e = null;

		try {
			Asserts.objectVariable("obj").notNull("I am an object");
		} catch (IllegalArgumentException iae) {
			e = iae;
		}

		assertNull(e);

		try {
			Asserts.objectVariable("obj").notNull(null);
		} catch (IllegalArgumentException iae) {
			e = iae;
		}

		assertNotNull(e);
		assertEquals("Variable obj should not be null", e.getMessage());
	}

	@Test
	public void testNullParams1() {
		assertThrows(NullPointerException.class, () -> Asserts.objectVariable(null));
	}

	@Test
	public void testNullParams2() {

		assertThrows(NullPointerException.class, () -> Asserts.objectParam(null));
	}

	@Test
	public void testNullParams3() {
		assertThrows(NullPointerException.class, () -> Asserts.numberVariable(null));
	}

	@Test
	public void testNullParams4() {
		assertThrows(NullPointerException.class, () -> Asserts.numberParam(null));
	}

	@Test
	public void testNullParams5() {
		assertThrows(NullPointerException.class, () -> Asserts.numberVariable("var").withValue(null));
	}

	@Test
	public void testNullParams6() {
		assertThrows(NullPointerException.class, () -> Asserts.numberParam("param").withValue(null));
	}
}
