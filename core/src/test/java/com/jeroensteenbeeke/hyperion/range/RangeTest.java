package com.jeroensteenbeeke.hyperion.range;

import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.*;

class RangeTest {
	private static final Range<Integer> a = Range.closed(0, 5);
	private static final Range<Integer> b = Range.closed(2, 7);
	private static final Range<Integer> c = Range.open(5, 7);
	private static final Range<Integer> d = Range.closed(6, 7);
	private static final Range<Integer> e = Range.all();
	private static final Range<Integer> f = Range.atMost(1337);
	private static final Range<Integer> g = Range.atLeast(-5000);
	private static final Range<Integer> h = Range.lessThan(1200);
	private static final Range<Integer> i = Range.greaterThan(-200);
	private static final Range<Integer> j = Range.closedOpen(1250, 1275);
	private static final Range<Integer> k = Range.openClosed(1225, 1250);
	private static final Range<Integer> l = Range.closedOpen(0, 3);
	private static final Range<Integer> m = Range.openClosed(3, 6);

	private static final Range<Integer> n = Range.openClosed(Integer.MIN_VALUE, Integer.MAX_VALUE);

	private static final Range<Integer> o = Range.lessThan(3);

	private static final Range<Integer> q = Range.greaterThan(3);

	@ParameterizedTest
	@MethodSource("testSpecGenerator")
	void overlap(Range<Integer> first, Range<Integer> second, TestSpecification specification) {
		if (specification.shouldOverlap()) {
			shouldOverlap(first, second);
		} else {
			shouldNotOverlap(first, second);
		}
	}

	@ParameterizedTest
	@MethodSource("testSpecGenerator")
	void unions(Range<Integer> first, Range<Integer> second, TestSpecification specification) {
		var union = specification.union();
		if (union != null) {
			assertThat("Union of %s and %s should be %s".formatted(first, second, union), first.union(second), equalTo(union));
		} else {
			assertThrows(IllegalArgumentException.class, () -> first.union(second));
		}
	}

	@ParameterizedTest
	@MethodSource("testSpecGenerator")
	void intersections(Range<Integer> first, Range<Integer> second, TestSpecification specification) {
		var intersection = specification.intersection();
		if (intersection != null) {
			assertThat(first.intersection(second), equalTo(intersection));
		} else {
			assertThrows(IllegalArgumentException.class, () -> first.intersection(second));
		}
	}

	@Test
	void known_edge_cases() {
		shouldNotOverlap(l, m);
		shouldNotOverlap(o, m);
		shouldNotOverlap(o, q);
	}

	@Test
	void emptyCheck() {
		assertTrue(Range.open(5, 5).isEmpty());
		assertTrue(Range.closedOpen(5, 5).isEmpty());
		assertTrue(Range.openClosed(5, 5).isEmpty());
		assertFalse(Range.closed(5, 5).isEmpty());
	}

	@Test
	void intersections_should_be_valid() {
		assertEquals("Cannot create intersection of non-overlapping ranges [5,6] and [7,8]", assertThrows(IllegalArgumentException.class, () -> Range.closed(5, 6).intersection(Range.closed(7, 8)).isEmpty()).getMessage());

		assertEquals(Range.closed(5,6),  Range.closed(0, 10).intersection(Range.closed(5, 6)));
		assertEquals(Range.closed(3,5),  Range.closed(0, 5).intersection(Range.closed(3, 10)));
		assertEquals(Range.closed(3,5),  Range.closed(3, 10).intersection(Range.closed(0, 5)));
		assertEquals(Range.closed(3,5),  Range.closed(3, 10).intersection(Range.closed(3, 5)));

	}

	private void shouldOverlap(Range<Integer> a, Range<Integer> b) {
		assertTrue(a.overlaps(b), "%s should overlap %s".formatted(a, b));
	}

	private void shouldNotOverlap(Range<Integer> a, Range<Integer> b) {
		assertFalse(a.overlaps(b), "%s should not overlap %s".formatted(a, b));
	}

	private record TestSpecification(boolean shouldOverlap, @Nullable Range<Integer> union,
									 @Nullable Range<Integer> intersection) {
	}

	public static Stream<Arguments> testSpecGenerator() {
		Stream.Builder<Arguments> argumentBuilder = Stream.builder();

		List<Range<Integer>> testRanges = List.of(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, q);

		for (Range<Integer> first : testRanges) {
			var firstLower = first.getLowerBoundValue().getOrNull();
			var firstLowerComparisonMethod = first.getLowerBound() instanceof ValuedBound<Integer> vb ? vb.comparisonMethod() : BoundComparisonMethod.GreaterThan;
			var firstUpper = first.getUpperBoundValue().getOrNull();
			var firstUpperComparisonMethod = first.getUpperBound() instanceof ValuedBound<Integer> vb ? vb.comparisonMethod() : BoundComparisonMethod.LessThan;

			for (Range<Integer> second : testRanges) {
				var secondLower = second.getLowerBoundValue().getOrNull();
				var secondLowerComparisonMethod = second.getLowerBound() instanceof ValuedBound<Integer> vb ? vb.comparisonMethod() : BoundComparisonMethod.GreaterThan;
				var secondUpper = second.getUpperBoundValue().getOrNull();
				var secondUpperComparisonMethod = second.getUpperBound() instanceof ValuedBound<Integer> vb ? vb.comparisonMethod() : BoundComparisonMethod.LessThan;

				if (firstLower == null && firstUpper == null) {
					// Range all

					argumentBuilder.add(Arguments.of(first, second, new TestSpecification(true, first, second)));
				} else if (firstLower == null) {
					// Negative infinity to set value

					// --------------------|
					//                   |----|
					//                   |-------------------
					//       |--------|         |------|
					boolean shouldOverlap = secondLower == null ||
						firstUpperComparisonMethod.satisfiedBy(secondLower, firstUpper) && secondLowerComparisonMethod.satisfiedBy(firstUpper, secondLower);

					Range<Integer> union = null;
					Range<Integer> intersection = null;

					if (shouldOverlap) {
						Integer unionEnd = secondUpper == null ? null : Stream.of(firstUpper, secondUpper).max(Integer::compare).orElse(null);
						BoundComparisonMethod unionEndMethod = unionEnd != null && unionEnd.equals(
							firstUpper) ? firstUpperComparisonMethod : secondUpperComparisonMethod;
						Integer intersectionEnd = Stream.of(firstUpper, secondUpper).filter(Objects::nonNull).min(Integer::compare).orElse(null);
						BoundComparisonMethod intersectionEndMethod = intersectionEnd.equals(
							firstUpper) ? firstUpperComparisonMethod : secondUpperComparisonMethod;

						union = unionEnd != null ? Range.singular(unionEndMethod, unionEnd) : Range.all();

						if (secondLower == null) {
							intersection = intersectionEnd != null ? Range.singular(intersectionEndMethod, intersectionEnd) : Range.all();
						} else {
							intersection = intersectionEnd != null ? Range.dual(secondLowerComparisonMethod, intersectionEndMethod, secondLower,
								intersectionEnd) : Range.singular(secondLowerComparisonMethod, secondLower);
						}

					}


					argumentBuilder.add(Arguments.of(first, second, new TestSpecification(shouldOverlap, union, intersection)));
				} else if (firstUpper == null) {
					boolean shouldOverlap = secondUpper == null || firstLowerComparisonMethod.satisfiedBy(secondUpper,
						firstLower) && secondUpperComparisonMethod.satisfiedBy(firstLower, secondUpper);


					Range<Integer> union = null;
					Range<Integer> intersection = null;

					// Set value to positive infinity
					if (shouldOverlap) {
						// Both ranges to positive infinity
						Integer unionStart = secondLower == null ? null : Stream.of(firstLower, secondLower).min(Integer::compare).orElse(null);
						BoundComparisonMethod unionStartMethod = unionStart != null && unionStart.equals(
							firstLower) ? firstLowerComparisonMethod : secondLowerComparisonMethod;
						Integer intersectionStart = Stream.of(firstLower, secondLower).filter(Objects::nonNull).max(Integer::compare).orElse(null);
						BoundComparisonMethod intersectionStartMethod = intersectionStart.equals(
							firstLower) ? firstLowerComparisonMethod : secondLowerComparisonMethod;

						union = unionStart != null ? Range.singular(unionStartMethod, unionStart) : Range.all();

						if (secondUpper == null) {
							intersection = intersectionStart != null ? Range.singular(intersectionStartMethod, intersectionStart) : Range.all();
						} else {
							intersection = intersectionStart != null ? Range.dual(intersectionStartMethod, secondUpperComparisonMethod,
								intersectionStart,
								secondUpper) : Range.singular(secondUpperComparisonMethod, secondUpper);
						}
					}

					argumentBuilder.add(Arguments.of(first, second, new TestSpecification(shouldOverlap, union, intersection)));

				} else {
					// First is a discrete range
					if (secondLower == null && secondUpper == null) {
						argumentBuilder.add(Arguments.of(first, second, new TestSpecification(true, second, first)));
					} else if (secondLower == null) {
						boolean shouldOverlap =
							secondUpperComparisonMethod.satisfiedBy(firstLower, secondUpper) && firstLowerComparisonMethod.satisfiedBy(secondUpper,
								firstLower);

						Range<Integer> union = null;
						Range<Integer> intersection = null;

						if (shouldOverlap) {
							Integer unionUpper = Integer.max(firstUpper, secondUpper);
							BoundComparisonMethod unionComparisonMethod = firstUpper.equals(
								unionUpper) ? firstUpperComparisonMethod : secondUpperComparisonMethod;
							union = Range.singular(unionComparisonMethod, unionUpper);

							Integer intersectionUpper = Integer.min(firstUpper, secondUpper);
							BoundComparisonMethod intersectionComparisonMethod = firstUpper.equals(
								intersectionUpper) ? firstUpperComparisonMethod : secondUpperComparisonMethod;

							intersection = Range.dual(firstLowerComparisonMethod, intersectionComparisonMethod, firstLower, intersectionUpper);
						}

						argumentBuilder.add(Arguments.of(first, second, new TestSpecification(shouldOverlap, union, intersection)));
					} else if (secondUpper == null) {
						boolean shouldOverlap = secondLowerComparisonMethod.satisfiedBy(firstUpper,
							secondLower) && firstUpperComparisonMethod.satisfiedBy(secondLower, firstUpper);

						Range<Integer> union = null;
						Range<Integer> intersection = null;

						if (shouldOverlap) {
							Integer unionLower = Integer.min(firstLower, secondLower);
							BoundComparisonMethod unionComparisonMethod = firstLower.equals(
								unionLower) ? firstLowerComparisonMethod : secondLowerComparisonMethod;
							union = Range.singular(unionComparisonMethod, unionLower);

							Integer intersectionLower = Integer.max(firstLower, secondLower);
							BoundComparisonMethod intersectionComparisonMethod = firstLower.equals(
								intersectionLower) ? firstLowerComparisonMethod : secondLowerComparisonMethod;

							intersection = Range.dual(intersectionComparisonMethod, firstUpperComparisonMethod, intersectionLower, firstUpper);
						}

						argumentBuilder.add(Arguments.of(first, second, new TestSpecification(shouldOverlap, union, intersection)));
					} else {
						boolean shouldOverlap = between(firstLower, secondLower, secondLowerComparisonMethod, secondUpper,
							secondUpperComparisonMethod) ||
							between(firstUpper, secondLower, secondLowerComparisonMethod, secondUpper, secondUpperComparisonMethod) ||
							between(secondLower, firstLower, firstLowerComparisonMethod, firstUpper, firstUpperComparisonMethod) ||
							between(secondUpper, firstLower, firstLowerComparisonMethod, firstUpper, firstUpperComparisonMethod) ||
							(firstLower.equals(secondLower) && firstUpper.equals(
								secondUpper) && firstUpperComparisonMethod == secondUpperComparisonMethod && firstLowerComparisonMethod == secondLowerComparisonMethod);

						Range<Integer> union = null;
						Range<Integer> intersection = null;

						if (shouldOverlap) {
							Integer unionLower = Integer.min(firstLower, secondLower);
							BoundComparisonMethod unionLowerComparisonMethod = firstLower.equals(
								unionLower) ? firstLowerComparisonMethod : secondLowerComparisonMethod;
							Integer unionUpper = Integer.max(firstUpper, secondUpper);
							BoundComparisonMethod unionUpperComparisonMethod = firstUpper.equals(unionUpper) ?
								firstUpperComparisonMethod : secondUpperComparisonMethod;

							if (firstLower.equals(secondLower)) {
								unionLowerComparisonMethod = Stream.of(firstLowerComparisonMethod, secondLowerComparisonMethod).min(
									Comparator.naturalOrder()).orElse(unionLowerComparisonMethod);
							}

							if (firstUpper.equals(secondUpper)) {
								unionUpperComparisonMethod = Stream.of(firstUpperComparisonMethod, secondUpperComparisonMethod).max(
									Comparator.naturalOrder()).orElse(unionUpperComparisonMethod);
							}


							union = Range.dual(unionLowerComparisonMethod, unionUpperComparisonMethod, unionLower, unionUpper);

							Integer intersectionLower = Integer.max(firstLower, secondLower);
							BoundComparisonMethod intersectionLowerComparisonMethod = firstLower.equals(
								intersectionLower) ? firstLowerComparisonMethod : secondLowerComparisonMethod;
							Integer intersectionUpper = Integer.min(firstUpper, secondUpper);
							BoundComparisonMethod intersectionUpperComparisonMethod = firstUpper.equals(intersectionUpper) ?
								firstUpperComparisonMethod : secondUpperComparisonMethod;

							if (firstLower.equals(secondLower)) {
								intersectionLowerComparisonMethod = Stream.of(firstLowerComparisonMethod, secondLowerComparisonMethod).max(
									Comparator.naturalOrder()).orElse(intersectionLowerComparisonMethod);
							}

							if (firstUpper.equals(secondUpper)) {
								intersectionUpperComparisonMethod = Stream.of(firstUpperComparisonMethod, secondUpperComparisonMethod).min(
									Comparator.naturalOrder()).orElse(intersectionUpperComparisonMethod);
							}

							intersection = Range.dual(intersectionLowerComparisonMethod, intersectionUpperComparisonMethod, intersectionLower,
								intersectionUpper);
						}

						argumentBuilder.add(Arguments.of(first, second, new TestSpecification(shouldOverlap, union, intersection)));
					}
				}

			}
		}

		return argumentBuilder.build();
	}

	private static boolean between(int toTest, int low, BoundComparisonMethod lowTest, int high, BoundComparisonMethod highTest) {
		return lowTest.satisfiedBy(toTest, low) && highTest.satisfiedBy(toTest, high);
	}

}
