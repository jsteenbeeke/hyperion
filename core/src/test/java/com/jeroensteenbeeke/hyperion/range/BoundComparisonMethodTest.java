package com.jeroensteenbeeke.hyperion.range;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BoundComparisonMethodTest {

	@Test
	void lessThan() {
		assertTrue(BoundComparisonMethod.LessThan.satisfiedBy(4, 5));
		assertFalse(BoundComparisonMethod.LessThan.satisfiedBy(5, 5));
		assertFalse(BoundComparisonMethod.LessThan.satisfiedBy(6, 5));
	}

	@Test
	void lessThanOrEqualTo() {
		assertTrue(BoundComparisonMethod.LessThanOrEqualTo.satisfiedBy(4, 5));
		assertTrue(BoundComparisonMethod.LessThanOrEqualTo.satisfiedBy(5, 5));
		assertFalse(BoundComparisonMethod.LessThanOrEqualTo.satisfiedBy(6, 5));
	}

	@Test
	void greaterThan() {
		assertFalse(BoundComparisonMethod.GreaterThan.satisfiedBy(4, 5));
		assertFalse(BoundComparisonMethod.GreaterThan.satisfiedBy(5, 5));
		assertTrue(BoundComparisonMethod.GreaterThan.satisfiedBy(6, 5));
	}

	@Test
	void greaterThanOrEqualTo() {
		assertFalse(BoundComparisonMethod.GreaterThanOrEqualTo.satisfiedBy(4, 5));
		assertTrue(BoundComparisonMethod.GreaterThanOrEqualTo.satisfiedBy(5, 5));
		assertTrue(BoundComparisonMethod.GreaterThanOrEqualTo.satisfiedBy(6, 5));
	}

}
