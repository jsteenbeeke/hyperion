package com.jeroensteenbeeke.hyperion.annotation;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.beans.Transient;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AnnotationsTest {
	@Test
	public void testClassFunctionality() {
		Annotations a = Annotations.fromClass(AnnotationsTestClass.class);

		assertFalse(a.hasAnnotation(TestField.class));
		assertTrue(a.hasAnnotation(Disabled.class));

		Optional<Disabled> annotation = a.getAnnotation(Disabled.class);

		assertTrue(annotation.isPresent());
	}

	@Test
	public void testMethodAnnotation() throws NoSuchMethodException {
		Method method = AnnotationsTestClass.class.getMethod("getField");

		Annotations a = Annotations.fromMethod(method);

		assertTrue(a.hasAnnotation(Transient.class));
		assertFalse(a.hasAnnotation(Disabled.class));

		Optional<Transient> annotation = a.getAnnotation(Transient.class);

		assertTrue(annotation.isPresent());
	}

	@Test
	public void testFieldAnnotation() throws NoSuchFieldException {
		Field field = AnnotationsTestClass.class.getDeclaredField("field");

		Annotations a = Annotations.fromField(field);

		assertTrue(a.hasAnnotation(TestField.class));
		assertFalse(a.hasAnnotation(Disabled.class));

		Optional<TestField> annotation = a.getAnnotation(TestField.class);

		assertTrue(annotation.isPresent());
	}

	@Disabled
	private class AnnotationsTestClass {
		@TestField
		private String field;

		@Transient
		public String getField() {
			return field;
		}
	}

	@Target(ElementType.FIELD)
	@Retention(RetentionPolicy.RUNTIME)
	private @interface TestField {

	}
}
