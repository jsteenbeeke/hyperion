package com.jeroensteenbeeke.hyperion.function;

import io.vavr.Tuple2;
import io.vavr.Tuple3;
import io.vavr.control.Option;
import org.junit.jupiter.api.Test;

import static com.jeroensteenbeeke.hyperion.function.Fn.*;
import static com.jeroensteenbeeke.vavr.hamcrest.VavrMatchers.isNone;
import static com.jeroensteenbeeke.vavr.hamcrest.VavrMatchers.isSome;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class FnTest {

	@Test
	void combine_with_yields_tuple2() {
		var o = Option.some(5).map(combineWith(() -> "Beer"));

		assertThat(o, isSome(new Tuple2<>(5, "Beer")));

		o = Option.some(5).flatMap(maybeCombineWith(() -> Option.some("Beer")));

		assertThat(o, isSome(new Tuple2<>(5, "Beer")));

		o = Option.<Integer> none().flatMap(maybeCombineWith(() -> Option.some("Beer")));

		assertThat(o, isNone());

		o = Option.<Integer> none().flatMap(maybeCombineWith(Option::<String>none));

		assertThat(o, isNone());

		o = Option.some(5).flatMap(maybeCombineWith(Option::<String>none));

		assertThat(o, isNone());


	}

	@Test
	void combine_with2_yields_tuple3() {
		var o = Option.some(5).map(combineWith(() -> "Beer")).map(combineWith2(() -> false));

		assertThat(o, isSome(new Tuple3<>(5, "Beer", false)));
	}
}
