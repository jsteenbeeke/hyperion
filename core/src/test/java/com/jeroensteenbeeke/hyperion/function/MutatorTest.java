package com.jeroensteenbeeke.hyperion.function;

import com.jeroensteenbeeke.hyperion.function.mutator.Mutator;
import io.vavr.collection.Array;
import org.junit.jupiter.api.Test;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Function;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MutatorTest {
	@Test
	void testMutators() {
		Element element = new Element("Element");
		UUID elementUUID = UUID.randomUUID();
		element = element.withRefs(Array.of(new Ref(elementUUID)));

		var container = new Container("Container").withElements(Array.of(element));

		var containerMutator = Mutator.set(Container::withDescription).andGet(Container::getDescription);

		var elementsMutator = Mutator.setSeq(Container::getElements)
				.using(Container::withElements)
				.matching(e -> e.getName().equals("Element"));

		var elementMutator = elementsMutator.andThen(Element::withValue).andGet(Element::getValue);

		assertThat(container.getDescription(), equalTo(""));
		var modified = containerMutator.set(container, "This is a container");
		assertThat(modified.getDescription(), equalTo("This is a container"));

		assertThat(modified.getElements(), hasItem(element));

		modified = elementMutator.set(modified, "I am a value");

		assertThat(modified.getElements(), hasItem(element.withValue("I am a value")));
		assertTrue(modified.getElements().forAll(e -> !e.getRefs().exists(r -> "New target".equals(r.getTarget()))));

		var refMutator = elementsMutator.andThenSeq(Element::withRefs)
				.andGet(Element::getRefs).matching(r -> r.getUuid().equals(elementUUID));

		var refTargetMutator = refMutator.andThen(Ref::withTarget).andGet(Ref::getTarget);

		modified = refTargetMutator.set(modified, "New target");

		assertTrue(modified.getElements().forAll(e -> e.getRefs().forAll(r -> r.getTarget().equals("New target"))));
	}
}

final class Container {
	private final String name;

	private final String description;

	private final Array<Element> elements;

	public Container(@NotNull String name) {
		this.name = name;
		this.description = "";
		this.elements = Array.empty();
	}

	private Container(String name, String description, Array<Element> elements) {
		this.name = name;
		this.description = description;
		this.elements = elements;
	}

	public String getDescription() {
		return description;
	}

	public Array<Element> getElements() {
		return elements;
	}

	public Container withDescription(@NotNull String description) {
		return new Container(name, description, elements);
	}

	public Container withElements(@NotNull Array<Element> elements) {
		return new Container(name, description, elements);
	}

	public Container withElements(@NotNull Function<Array<Element>, Array<Element>> elements) {
		return new Container(name, description, elements.apply(this.elements));
	}
}

final class Element {
	private final String name;

	private final String value;

	private final Array<Ref> refs;

	public Element(String name) {
		this.name = name;
		this.value = null;
		this.refs = Array.empty();
	}

	private Element(String name, String value, Array<Ref> refs) {
		this.name = name;
		this.value = value;
		this.refs = refs;
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

	public Array<Ref> getRefs() {
		return refs;
	}

	public Element withValue(@NotNull String value) {
		return new Element(name, value, refs);
	}

	public Element withoutValue() {
		return new Element(name, null, refs);
	}

	public Element withRefs(@NotNull Array<Ref> refs) {
		return new Element(name, value, refs);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Element element = (Element) o;
		return name.equals(element.name) && Objects.equals(value, element.value);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, value);
	}
}

class Ref {
	private final UUID uuid;

	private final String target;

	public Ref(@NotNull UUID uuid) {
		this.uuid = uuid;
		this.target = null;
	}

	private Ref(@NotNull UUID uuid, @Nullable String target) {
		this.uuid = uuid;
		this.target = target;
	}

	public UUID getUuid() {
		return uuid;
	}

	public String getTarget() {
		return target;
	}

	public Ref withTarget(@NotNull String target) {
		return new Ref(uuid, target);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Ref ref = (Ref) o;
		return uuid.equals(ref.uuid) && Objects.equals(target, ref.target);
	}

	@Override
	public int hashCode() {
		return Objects.hash(uuid, target);
	}
}
