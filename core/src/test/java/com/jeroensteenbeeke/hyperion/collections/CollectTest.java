package com.jeroensteenbeeke.hyperion.collections;

import io.vavr.*;
import io.vavr.control.Option;
import org.danekja.java.util.function.serializable.SerializableFunction;
import org.hamcrest.Matcher;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static com.jeroensteenbeeke.hyperion.collections.Collect.*;
import static com.jeroensteenbeeke.vavr.hamcrest.VavrMatchers.isNone;
import static com.jeroensteenbeeke.vavr.hamcrest.VavrMatchers.isSome;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

class CollectTest {
	@Test
	void concat() {
		var a = List.of("A");
		var bc = List.of("B", "C");

		var abc = Collect.concat(a, bc);
		var bca = Collect.concat(bc, a);

		assertNotEquals(abc, bca);
		assertThat(abc, equalTo(List.of("A", "B", "C")));
		assertThat(bca, equalTo(List.of("B", "C", "A")));

		assertThrows(UnsupportedOperationException.class, () -> abc.add("D"));
		assertThrows(UnsupportedOperationException.class, () -> bca.add("D"));
	}

	@Test
	void mutableConcat() {
		var a = List.of("A");
		var bc = List.of("B", "C");

		var abc = Collect.mutableConcat(a, bc);
		var bca = Collect.mutableConcat(bc, a);

		assertNotEquals(abc, bca);
		assertThat(abc, equalTo(List.of("A", "B", "C")));
		assertThat(bca, equalTo(List.of("B", "C", "A")));


		assertDoesNotThrow(() -> abc.add("D"));
		assertDoesNotThrow(() -> bca.add("D"));

		assertNotEquals(abc, bca);
		assertThat(abc, equalTo(List.of("A", "B", "C", "D")));
		assertThat(bca, equalTo(List.of("B", "C", "A", "D")));

	}

	@Test
	void union() {
		var a = Set.of("A");
		var bc = Set.of("B", "C");

		var union = Collect.union(a, bc);
		var union2 = Collect.union(bc, a);

		assertThat(union, notNullValue());
		assertThat(union.size(), equalTo(3));
		assertThat(union, hasItem("A"));
		assertThat(union, hasItem("B"));
		assertThat(union, hasItem("C"));

		assertEquals(union, union2);

		assertThrows(UnsupportedOperationException.class, () -> union.add("D"));
	}

	@Test
	void mutableUnion() {
		var a = Set.of("A");
		var bc = Set.of("B", "C");

		var union = Collect.mutableUnion(a, bc);
		var union2 = Collect.mutableUnion(bc, a);

		assertThat(union, notNullValue());
		assertThat(union.size(), equalTo(3));
		assertThat(union, hasItem("A"));
		assertThat(union, hasItem("B"));
		assertThat(union, hasItem("C"));

		assertEquals(union, union2);

		assertDoesNotThrow(() -> union.add("D"));

		assertThat(union.size(), equalTo(4));
		assertThat(union, hasItem("A"));
		assertThat(union, hasItem("B"));
		assertThat(union, hasItem("C"));
		assertThat(union, hasItem("D"));

		assertNotEquals(union, union2);

	}

	@Test
	void testSetOf() {
		assertThat(setOf(List.of()), equalTo(Set.of()));
		assertThat(setOf(List.of(), "D", "E"), equalTo(Set.of("D", "E")));
		assertThat(setOf(List.of("A", "B", "C")), equalTo(Set.of("A", "B", "C")));
		assertThat(setOf(List.of("A", "B", "C"), "D"), equalTo(Set.of("A", "B", "C", "D")));
		assertThat(setOf(List.of("A", "B", "C"), "D", "E"), equalTo(Set.of("A", "B", "C", "D", "E")));
		assertThat(mutableSetOf(List.of()), equalTo(Set.of()));
		assertThat(mutableSetOf(List.of(), "D", "E"), equalTo(Set.of("D", "E")));
		assertThat(mutableSetOf(List.of("A", "B", "C")), equalTo(Set.of("A", "B", "C")));
		assertThat(mutableSetOf(List.of("A", "B", "C"), "D"), equalTo(Set.of("A", "B", "C", "D")));
		assertThat(mutableSetOf(List.of("A", "B", "C"), "D", "E"), equalTo(Set.of("A", "B", "C", "D", "E")));

		assertThrows(UnsupportedOperationException.class, () -> setOf(List.of("A")).add("B"));
		assertDoesNotThrow(() -> mutableSetOf(List.of("A")).add("B"));
	}

	@Test
	void testListOf() {
		assertThat(listOf(List.of()), equalTo(List.of()));
		assertThat(listOf(List.of(), "D", "E"), equalTo(List.of("D", "E")));
		assertThat(listOf(List.of("A", "B", "C")), equalTo(List.of("A", "B", "C")));
		assertThat(listOf(List.of("A", "B", "C"), "D"), equalTo(List.of("A", "B", "C", "D")));
		assertThat(listOf(List.of("A", "B", "C"), "D", "E"), equalTo(List.of("A", "B", "C", "D", "E")));
		assertThat(mutableListOf(List.of()), equalTo(List.of()));
		assertThat(mutableListOf(List.of(), "D", "E"), equalTo(List.of("D", "E")));
		assertThat(mutableListOf(List.of("A", "B", "C")), equalTo(List.of("A", "B", "C")));
		assertThat(mutableListOf(List.of("A", "B", "C"), "D"), equalTo(List.of("A", "B", "C", "D")));
		assertThat(mutableListOf(List.of("A", "B", "C"), "D", "E"), equalTo(List.of("A", "B", "C", "D", "E")));

		assertThrows(UnsupportedOperationException.class, () -> setOf(List.of("A")).add("B"));
		assertDoesNotThrow(() -> mutableListOf(List.of("A")).add("B"));
	}

	@Test
	void testMapList() {
		var len = mapList(String::length);

		assertThat(len.apply(List.of("A", "big", "old", "failure")), equalTo(List.of(1, 3, 3, 7)));
	}

	@Test
	void testMapSet() {
		var len = mapSet(String::length);

		assertThat(len.apply(Set.of("A", "big", "old", "failure")), equalTo(Set.of(1, 3, 7)));
	}


	@ParameterizedTest
	@MethodSource("coalesce2Specs")
	void coalesce2(Tuple2<Integer, Integer> values, @NotNull Matcher<Option<Integer>> matcher) {
		assertThat(Collect.coalesce(values._1(), values._2()), matcher);
	}

	@ParameterizedTest
	@MethodSource("coalesce3Specs")
	void coalesce3(Tuple3<Integer, Integer, Integer> values, @NotNull Matcher<Option<Integer>> matcher) {
		assertThat(Collect.coalesce(values._1(), values._2(), values._3()), matcher);
	}

	@ParameterizedTest
	@MethodSource("coalesce4Specs")
	void coalesce4(Tuple4<Integer, Integer, Integer, Integer> values, @NotNull Matcher<Option<Integer>> matcher) {
		assertThat(Collect.coalesce(values._1(), values._2(), values._3(), values._4()), matcher);
	}

	@ParameterizedTest
	@MethodSource("coalesce5Specs")
	void coalesce5(Tuple5<Integer, Integer, Integer, Integer, Integer> values, @NotNull Matcher<Option<Integer>> matcher) {
		assertThat(Collect.coalesce(values._1(), values._2(), values._3(), values._4(), values._5()),
			matcher);
	}

	@ParameterizedTest
	@MethodSource("coalesce6Specs")
	void coalesce6(Tuple6<Integer, Integer, Integer, Integer, Integer, Integer> values, @NotNull Matcher<Option<Integer>> matcher) {
		assertThat(Collect.coalesce(values._1(), values._2(), values._3(), values._4(), values._5(), values._6()),
			matcher);
	}

	@ParameterizedTest
	@MethodSource("coalesce7Specs")
	void coalesce7(Tuple7<Integer, Integer, Integer, Integer, Integer, Integer, Integer> values, @NotNull Matcher<Option<Integer>> matcher) {
		assertThat(Collect.coalesce(values._1(), values._2(), values._3(), values._4(), values._5(), values._6(), values._7()),
			matcher);
	}

	@ParameterizedTest
	@MethodSource("coalesce8Specs")
	void coalesce8(Tuple8<Integer, Integer, Integer, Integer, Integer, Integer, Integer, Integer> values, @NotNull Matcher<Option<Integer>> matcher) {
		assertThat(Collect.coalesce(values._1(), values._2(), values._3(), values._4(), values._5(), values._6(), values._7(), values._8()),
			matcher);
	}

	public static Stream<Arguments> coalesce2Specs() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
		return generateCoalesceSpecs(2, Tuple2.class);
	}

	public static Stream<Arguments> coalesce3Specs() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
		return generateCoalesceSpecs(3, Tuple3.class);
	}

	public static Stream<Arguments> coalesce4Specs() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
		return generateCoalesceSpecs(4, Tuple4.class);
	}

	public static Stream<Arguments> coalesce5Specs() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
		return generateCoalesceSpecs(5, Tuple5.class);
	}

	public static Stream<Arguments> coalesce6Specs() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
		return generateCoalesceSpecs(6, Tuple6.class);
	}


	public static Stream<Arguments> coalesce7Specs() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
		return generateCoalesceSpecs(7, Tuple7.class);
	}

	public static Stream<Arguments> coalesce8Specs() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
		return generateCoalesceSpecs(8, Tuple8.class);
	}

	private static Stream<Arguments> generateCoalesceSpecs(
		int params,
		Class<? extends Tuple> targetClass) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
		Stream.Builder<Arguments> builder = Stream.builder();

		// 1 param -> 2 variations 0
		// 2 params -> 4 variations 00, 01, 10, 11

		int max = (1 << params) - 1;
		Class<?>[] ctorParams = new Class<?>[params];
		Arrays.fill(ctorParams, Object.class);

		for (int i = 0; i <= max; i++) {
			Object[] targetParams = new Object[params];
			boolean hasExpectedValue = false;
			Matcher<?> expectedValue = isNone();

			for (int paramIndex = 0; paramIndex < params; paramIndex++) {
				int paramBit = 1 << paramIndex;
				Integer value = (paramBit & i) == paramBit ? (params - paramIndex) : null;
				if (!hasExpectedValue && value != null) {
					expectedValue = isSome(value);
					hasExpectedValue = true;
				}

				targetParams[paramIndex] = value;
			}

			builder.add(Arguments.of(targetClass.getConstructor(ctorParams).newInstance(targetParams), expectedValue));
		}

		return builder.build();
	}

}
