package com.jeroensteenbeeke.hyperion;

import org.jetbrains.annotations.NotNull;

/**
 * Listener interface for the HyperionApp class. Allows various other Hyperion components to listen to
 * the setting of certain values
 */
public interface HyperionAppListener {
	/**
	 * Called when the application version and commit message become known
	 * @param version The version
	 * @param commitMessage The commit message
	 */
	void onApplicationVersion(@NotNull String version, @NotNull String commitMessage);
}
