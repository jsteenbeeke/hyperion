package com.jeroensteenbeeke.hyperion.range;

import org.jetbrains.annotations.NotNull;

/**
 * Represents a bound that has a value
 * @param value The value this bound represents
 * @param comparisonMethod The comparison method that represents the function of this bound >, >=, <, <=
 * @param <T> The type of value in the bound
 */
public record ValuedBound<T extends Comparable<T>>(@NotNull T value, @NotNull BoundComparisonMethod comparisonMethod) implements Bound<T> {
	@Override
	public boolean satisfiedBy(@NotNull T value) {
		return comparisonMethod().satisfiedBy(value, value());
	}

	@Override
	public int compareTo(@NotNull Bound<T> other) {
		return switch (other) {
			case PositiveInfinity() -> -1;
			case NegativeInfinity() -> 1;
			case ValuedBound<T> v -> {
				int diff = value().compareTo(v.value());
				yield diff == 0 ? comparisonMethod().compareTo(v.comparisonMethod()) : diff;
			}
		};
	}

	@Override
	public String toLowerBoundString() {
		return comparisonMethod().getLowerBoundString(value());
	}

	@Override
	public String toUpperBoundString() {
		return comparisonMethod().getUpperboundString(value());
	}

	/**
	 * Creates a valued bound that requires values to be less than the given value
	 * @param value The upper limit (exclusive)
	 * @return A bound
	 * @param <V> The value type
	 */
	public static <V extends Comparable<V>> ValuedBound<V> lt(V value) {
		return new ValuedBound<>(value, BoundComparisonMethod.LessThan);
	}

	/**
	 * Creates a valued bound that requires values to be less than or equal to the given value
	 * @param value The upper limit (inclusive)
	 * @return A bound
	 * @param <V> The value type
	 */
	public static <V extends Comparable<V>> ValuedBound<V> le(V value) {
		return new ValuedBound<>(value, BoundComparisonMethod.LessThanOrEqualTo);
	}

	/**
	 * Creates a valued bound that requires values to be greater than or equal to the given value
	 * @param value The lower limit (inclusive)
	 * @return A bound
	 * @param <V> The value type
	 */
	public static <V extends Comparable<V>> ValuedBound<V> ge(V value) {
		return new ValuedBound<>(value, BoundComparisonMethod.GreaterThanOrEqualTo);
	}

	/**
	 * Creates a valued bound that requires values to be greater than the given value
	 * @param value The lower limit (exclusive)
	 * @return A bound
	 * @param <V> The value type
	 */
	public static <V extends Comparable<V>> ValuedBound<V> gt(V value) {
		return new ValuedBound<>(value, BoundComparisonMethod.GreaterThan);
	}
}
