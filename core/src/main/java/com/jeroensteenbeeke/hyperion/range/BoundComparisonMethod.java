package com.jeroensteenbeeke.hyperion.range;

import org.jetbrains.annotations.NotNull;

/**
 * A comparison method for bounds
 */
public enum BoundComparisonMethod {
	/**
	 * Represents a less than bound
	 */
	LessThan {
		@Override
		protected boolean isExpectedValue(int diff) {
			return diff < 0;
		}

		@Override
		protected <T extends Comparable<T>> String getUpperboundString(@NotNull T value) {
			return "%s)".formatted(value);
		}
	},
	/**
	 * Represents a less than or equal to bound
	 */
	LessThanOrEqualTo {
		@Override
		protected boolean isExpectedValue(int diff) {
			return diff <= 0;
		}

		@Override
		protected <T extends Comparable<T>> String getUpperboundString(@NotNull T value) {
			return "%s]".formatted(value);
		}
	},
	/**
	 * Represents a greater than or equal to bound
	 */
	GreaterThanOrEqualTo {
		@Override
		protected boolean isExpectedValue(int diff) {
			return diff >= 0;
		}

		@Override
		protected <T extends Comparable<T>> String getLowerBoundString(@NotNull T value) {
			return "[%s".formatted(value);
		}
	},
	/**
	 * Represents a greater than bound
	 */
	GreaterThan {
		@Override
		protected boolean isExpectedValue(int diff) {
			return diff > 0;
		}

		@Override
		protected <T extends Comparable<T>> String getLowerBoundString(@NotNull T value) {
			return "(%s".formatted(value);
		}
	};

	/**
	 * Determines whether the given value satisfies the bound
	 * @param value The value to test
	 * @param threshold The value defined by the bound
	 * @return {@code true} if the value falls within the bound, {@code false} otherwise
	 * @param <T> The type of value contained by the bound
	 */
	public <T extends Comparable<T>> boolean satisfiedBy(T value, T threshold) {
		return isExpectedValue(value.compareTo(threshold));
	}

	/**
	 * Determines whether a given compareTo score is valid for the given bound
	 * @param diff The diff outputted by Comparable's compareTo
	 * @return {@code true} if the diff falls within the bound, {@code false} otherwise
	 */
	protected abstract boolean isExpectedValue(int diff);

	/**
	 * Creates a String representation of this comparison method as a lower bound
	 * @param value The value that represents the lower bound
	 * @return A String representation
	 * @param <T> The type of value
	 * @throws IllegalStateException if the comparisonMethod cannot serve as a lower bound
	 */
	protected <T extends Comparable<T>> String getLowerBoundString(@NotNull T value) {
		throw new IllegalStateException();
	}

	/**
	 * Creates a String representation of this comparison method as an upper bound
	 * @param value The value that represents the upper bound
	 * @return A String representation
	 * @param <T> The type of value
	 * @throws IllegalStateException if the comparisonMethod cannot serve as an upper bound
	 */
	protected <T extends Comparable<T>> String getUpperboundString(@NotNull T value) {
		throw new IllegalStateException();
	}
}
