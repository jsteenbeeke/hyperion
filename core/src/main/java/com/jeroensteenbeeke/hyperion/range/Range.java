package com.jeroensteenbeeke.hyperion.range;

import io.vavr.control.Option;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.Objects;

/**
 * Range of comparables
 *
 * @param <T> The type of comparable contained in the range
 */
public final class Range<T extends Comparable<T>> implements Serializable {

	private final Bound<T> lowerBound;

	private final Bound<T> upperBound;

	private Range(
		@NotNull Bound<T> lowerBound,
		@NotNull Bound<T> upperBound) {
		if (lowerBound instanceof PositiveInfinity) {
			throw new IllegalArgumentException("Lower bound may not be positive infinity");
		}
		if (upperBound instanceof NegativeInfinity) {
			throw new IllegalArgumentException("Upper bound may not be negative infinity");
		}

		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
	}

	/**
	 * Internal method for single-bounded ranges in unit tests
	 *
	 * @param comparisonMethod The comparison method at the bound
	 * @param value            The bound value
	 * @param <V>              The type of value
	 * @return A range
	 */
	static <V extends Comparable<V>> Range<V> singular(@NotNull BoundComparisonMethod comparisonMethod, @NotNull V value) {
		return switch (comparisonMethod) {
			case LessThan -> lessThan(value);
			case LessThanOrEqualTo -> atMost(value);
			case GreaterThanOrEqualTo -> atLeast(value);
			case GreaterThan -> greaterThan(value);
		};
	}

	/**
	 * Internal method for dual-bounded ranges for testing purposes
	 *
	 * @param lower The lower bound comparison method
	 * @param upper The upper bound comparison method
	 * @param vlow  The lower bound
	 * @param vhigh The upper bound
	 * @param <V>   The type of value in the range
	 * @return A range
	 */
	static <V extends Comparable<V> & Serializable> Range<V> dual(
		@NotNull BoundComparisonMethod lower, @NotNull BoundComparisonMethod upper, @NotNull V vlow, @NotNull V vhigh) {
		if (vlow.compareTo(vhigh) > 0) {
			throw new IllegalArgumentException();
		}

		return switch (lower) {
			case GreaterThanOrEqualTo -> switch (upper) {

				case LessThan -> Range.closedOpen(vlow, vhigh);
				case LessThanOrEqualTo -> Range.closed(vlow, vhigh);
				default -> throw new IllegalStateException("Invalid upper bound: " + upper);
			};
			case GreaterThan -> switch (upper) {
				case LessThan -> Range.open(vlow, vhigh);
				case LessThanOrEqualTo -> Range.openClosed(vlow, vhigh);
				default -> throw new IllegalStateException("Invalid upper bound: " + upper);

			};
			default -> throw new IllegalArgumentException("Invalid lower bound: " + lower);
		};
	}

	/**
	 * Returns the lower bound of the range
	 *
	 * @return The lower bound
	 */
	@NotNull
	public Bound<T> getLowerBound() {
		return lowerBound;
	}

	/**
	 * Returns the upper bound of the range
	 *
	 * @return The upper bound
	 */
	@NotNull
	public Bound<T> getUpperBound() {
		return upperBound;
	}

	/**
	 * Returns the value associated with the lower bound, or an empty option if there is no lower bound
	 *
	 * @return Option containing either the lower bound value, or empty
	 */
	@NotNull
	public Option<T> getLowerBoundValue() {
		return Option.of(lowerBound instanceof ValuedBound<T> bound ? bound.value() : null);
	}

	/**
	 * Returns the value associated with the upper bound, or an empty option if there is no lower bound
	 *
	 * @return Option containing either the upper bound value, or empty
	 */
	@NotNull
	public Option<T> getUpperBoundValue() {

		return Option.of(upperBound instanceof ValuedBound<T> bound ? bound.value() : null);
	}

	/**
	 * Determine whether a given value exists within this range
	 *
	 * @param value The value to test
	 * @return {@code true} if the value falls within the range, {@code false} otherwise
	 */
	public boolean contains(@NotNull T value) {
		return lowerBound.satisfiedBy(value) && upperBound.satisfiedBy(value);
	}

	/**
	 * Determines whether or not there is overlap between the two ranges, i.e. if a value exists that
	 * falls in both the current range and the given range
	 *
	 * @param other The range to test against
	 * @return {@code true} if the ranges overlap, {@code false} otherwise
	 */
	public boolean overlaps(@NotNull Range<T> other) {
		return equals(other) || other.getLowerBoundValue().map(this::contains).getOrElse(false) ||
			other.getUpperBoundValue().map(this::contains).getOrElse(false) ||
			getLowerBoundValue().map(other::contains).getOrElse(false) ||
			getUpperBoundValue().map(other::contains).getOrElse(false);
	}

	/**
	 * Returns the union (OR) of this range and the given range, assuming they overlap
	 *
	 * @param other The range to join with
	 * @return A range that is the union of both ranges
	 */
	public Range<T> union(@NotNull Range<T> other) {
		if (!overlaps(other)) {
			throw new IllegalArgumentException("Cannot create union of non-overlapping ranges %s and %s".formatted(this, other));
		}

		var lowestBound = lowerBound.compareTo(other.lowerBound) <= 0 ? lowerBound : other.lowerBound;
		var highestBound = upperBound.compareTo(other.upperBound) >= 0 ? upperBound : other.upperBound;

		return new Range<>(lowestBound, highestBound);
	}

	/**
	 * Returns a range that is the intersection (AND) of the current range and the given range
	 *
	 * @param other The range to intersect with
	 * @return The combined range
	 */
	public Range<T> intersection(@NotNull Range<T> other) {
		if (!overlaps(other)) {
			throw new IllegalArgumentException("Cannot create intersection of non-overlapping ranges %s and %s".formatted(this, other));
		}

		var highestLowerBound = lowerBound.compareTo(other.lowerBound) > 0 ? lowerBound : other.lowerBound;
		var lowestUpperBound = upperBound.compareTo(other.upperBound) < 0 ? upperBound : other.upperBound;

		if (highestLowerBound instanceof ValuedBound<T> lower && lowestUpperBound instanceof ValuedBound<T> upper) {
			if (lower.value().compareTo(upper.value()) > 0) {
				throw new IllegalArgumentException("Intersection of ranges %s and %s yields to invalid range %s.%s".formatted(
					this, other, highestLowerBound.toLowerBoundString(), lowestUpperBound.toUpperBoundString()
				));
			}
		}

		return new Range<>(highestLowerBound, lowestUpperBound);
	}

	/**
	 * Determines whether or not the current range is empty (i.e. no value exists that satisfies the range)
	 * @return {@code true} if there exists a value that satisfies the range. {@code false} otherwise
	 */
	public boolean isEmpty() {
		if (lowerBound instanceof ValuedBound<T> lowerValued) {
			if (upperBound instanceof ValuedBound<T> upperValued) {
				if (lowerValued.value().compareTo(upperValued.value()) == 0) {
					return lowerValued.comparisonMethod() == BoundComparisonMethod.GreaterThan || upperValued.comparisonMethod() == BoundComparisonMethod.LessThan;
				}
			}
		}

		return false;
	}

	/**
	 * Returns a range values that includes all values greater than or equal to the lower bound and less than or equal to the upper bound
	 *
	 * @param lowerBound The lower bound (inclusive)
	 * @param upperBound The upper bound (inclusive)
	 * @param <V>        The type of value in the range
	 * @return A range
	 */
	public static <V extends Comparable<V>> Range<V> closed(@NotNull V lowerBound, @NotNull V upperBound) {
		if (upperBound.compareTo(lowerBound) < 0) {
			throw new IllegalArgumentException("Upper bound %s below lower bound %s".formatted(upperBound, lowerBound));
		}

		return new Range<>(ValuedBound.ge(lowerBound), ValuedBound.le(upperBound));
	}

	/**
	 * Returns a range values that includes all values greater than the lower bound and less than the upper bound
	 *
	 * @param lowerBound The lower bound (exclusive)
	 * @param upperBound The upper bound (exclusive)
	 * @param <V>        The type of value in the range
	 * @return A range
	 */
	public static <V extends Comparable<V>> Range<V> open(@NotNull V lowerBound, @NotNull V upperBound) {
		if (upperBound.compareTo(lowerBound) < 0) {
			throw new IllegalArgumentException("Upper bound %s below lower bound %s".formatted(upperBound, lowerBound));
		}

		return new Range<>(ValuedBound.gt(lowerBound), ValuedBound.lt(upperBound));
	}

	/**
	 * Returns a range values that includes all values greater than or equal to the lower bound and less than the upper bound
	 *
	 * @param lowerBound The lower bound (inclusive)
	 * @param upperBound The upper bound (exclusive)
	 * @param <V>        The type of value in the range
	 * @return A range
	 */
	public static <V extends Comparable<V>> Range<V> closedOpen(@NotNull V lowerBound, @NotNull V upperBound) {
		if (upperBound.compareTo(lowerBound) < 0) {
			throw new IllegalArgumentException("Upper bound %s below lower bound %s".formatted(upperBound, lowerBound));
		}

		return new Range<>(ValuedBound.ge(lowerBound), ValuedBound.lt(upperBound));
	}

	/**
	 * Returns a range values that includes all values greater than the lower bound and less than or equal to the upper bound
	 *
	 * @param lowerBound The lower bound (exclusive)
	 * @param upperBound The upper bound (inclusive)
	 * @param <V>        The type of value in the range
	 * @return A range
	 */
	public static <V extends Comparable<V>> Range<V> openClosed(@NotNull V lowerBound, @NotNull V upperBound) {
		if (upperBound.compareTo(lowerBound) < 0) {
			throw new IllegalArgumentException("Upper bound %s below lower bound %s".formatted(upperBound, lowerBound));
		}

		return new Range<>(ValuedBound.gt(lowerBound), ValuedBound.le(upperBound));
	}

	/**
	 * Returns the range of all values that are greater than or equal to the given bound
	 *
	 * @param lowerBound The lower bound
	 * @param <V>        The type of value in the range
	 * @return A range
	 */
	public static <V extends Comparable<V>> Range<V> atLeast(@NotNull V lowerBound) {
		return new Range<>(ValuedBound.ge(lowerBound), new PositiveInfinity<>());
	}

	/**
	 * Returns the range of all values that are greater than the given bound
	 *
	 * @param lowerBound The lower bound
	 * @param <V>        The type of value in the range
	 * @return A range
	 */
	public static <V extends Comparable<V>> Range<V> greaterThan(@NotNull V lowerBound) {
		return new Range<>(ValuedBound.gt(lowerBound), new PositiveInfinity<>());
	}

	/**
	 * Returns the range of all values that are less than or equal to the given bound
	 *
	 * @param upperBound The upper bound
	 * @param <V>        The type of value in the range
	 * @return A range
	 */
	public static <V extends Comparable<V>> Range<V> atMost(@NotNull V upperBound) {
		return new Range<>(new NegativeInfinity<>(), ValuedBound.le(upperBound));
	}

	/**
	 * Returns the range of all values that are less than the given bound
	 *
	 * @param upperBound The upper bound
	 * @param <V>        The type of value in the range
	 * @return A range
	 */
	public static <V extends Comparable<V>> Range<V> lessThan(@NotNull V upperBound) {
		return new Range<>(new NegativeInfinity<>(), ValuedBound.lt(upperBound));
	}

	/**
	 * Returns the range that contains all values
	 *
	 * @param <V> The type of values in the range
	 * @return A range
	 */
	public static <V extends Comparable<V>> Range<V> all() {
		return new Range<V>(new NegativeInfinity<>(), new PositiveInfinity<>());
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Range<?> range = (Range<?>) o;
		return Objects.equals(lowerBound, range.lowerBound) && Objects.equals(upperBound, range.upperBound);
	}

	@Override
	public int hashCode() {
		return Objects.hash(lowerBound, upperBound);
	}

	@Override
	public String toString() {
		return "%s,%s".formatted(lowerBound.toLowerBoundString(), upperBound.toUpperBoundString());
	}
}


