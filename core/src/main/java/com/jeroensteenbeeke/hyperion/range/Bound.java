package com.jeroensteenbeeke.hyperion.range;

import org.jetbrains.annotations.NotNull;

/**
 * Represents a bound within a range, an upper or lower edge value
 *
 * @param <T> The type of value in the bound
 */
public sealed interface Bound<T extends Comparable<T>> extends Comparable<Bound<T>> permits InfiniteBound, ValuedBound {
	/**
	 * Determines whether or not a given value satisfies this bound, i.e. it is valid with regard to this bound
	 *
	 * @param value The value to test
	 * @return {@code true} if valid, {@code false} otherwise
	 */
	boolean satisfiedBy(@NotNull T value);

	/**
	 * Creates a String description if this is a lower bound
	 * @return The description
	 * @throws IllegalStateException If this bound cannot act as a lower bound
	 */
	String toLowerBoundString();

	/**
	 * Creates a String description if this is an upper bound
	 * @return The description
	 * @throws IllegalStateException If this bound cannot act as an upper bound
	 */
	String toUpperBoundString();
}



