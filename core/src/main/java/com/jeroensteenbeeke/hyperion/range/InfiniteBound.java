package com.jeroensteenbeeke.hyperion.range;

/**
 * Bound subinterface that represents an infinite bound, i.e. a bound that cannot be crossed regardless of value
 * @param <T> The type of value in the bound
 */
public sealed interface InfiniteBound<T extends Comparable<T>> extends Bound<T> permits NegativeInfinity, PositiveInfinity {

}
