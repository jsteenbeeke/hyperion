package com.jeroensteenbeeke.hyperion.range;

import org.jetbrains.annotations.NotNull;

/**
 * Bound that represents positive infinity. We distinguish between positive and negative infinity for the sake of camparison
 * @param <T> The type of value contained by the bound
 */
public record PositiveInfinity<T extends Comparable<T>>() implements InfiniteBound<T> {

	@Override
	public boolean satisfiedBy(@NotNull T value) {
		// Values are always less than infinite
		return true;
	}

	@SuppressWarnings("ComparatorMethodParameterNotUsed")
	@Override
	public int compareTo(@NotNull Bound<T> other) {
		if (other instanceof PositiveInfinity) {
			return 0;
		}

		return 1;
	}

	@Override
	public String toLowerBoundString() {
		throw new IllegalStateException();
	}

	@Override
	public String toUpperBoundString() {
		return "♾)";
	}
}
