package com.jeroensteenbeeke.hyperion.range;

import org.jetbrains.annotations.NotNull;

/**
 * Bound that represents negative infinity. We distinguish between positive and negative infinity for the sake of camparison
 * @param <T> The type of value contained by the bound
 */
public record NegativeInfinity<T extends Comparable<T>>() implements InfiniteBound<T> {
	@Override
	public boolean satisfiedBy(@NotNull T value) {
		return true;
	}

	@SuppressWarnings("ComparatorMethodParameterNotUsed")
	@Override
	public int compareTo(@NotNull Bound<T> other) {
		if (other instanceof NegativeInfinity) {
			return 0;
		}
		return -1;
	}

	@Override
	public String toLowerBoundString() {
		return "(♾";
	}

	@Override
	public String toUpperBoundString() {
		throw new IllegalStateException();
	}

}
