package com.jeroensteenbeeke.hyperion.function;

/**
 * Unary operator for short primitives
 */
@FunctionalInterface
public interface ShortUnaryOperator {
	/**
     * Applies the function to the given short
	 * @param s A short
	 * @return Another short
	 */
	short apply(short s);
}
