package com.jeroensteenbeeke.hyperion.function;

import org.danekja.java.util.function.serializable.SerializablePredicate;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

/**
 * Factory class for creating comparison based predicates
 */
public class Compare {
	/**
	 * Returns a predicate that is satisfied when the tested value is less than the supplied value
	 * @param value The value to test against
	 * @return A predicate
	 * @param <T> The type of comparable
	 */
	public static <T extends Serializable & Comparable<T>> SerializablePredicate<T> lessThan(@NotNull T value) {
		return actual -> actual.compareTo(value) < 0;
	}

	/**
	 * Returns a predicate that is satisfied when the tested value is less than or equal to the supplied value
	 * @param value The value to test against
	 * @return A predicate
	 * @param <T> The type of comparable
	 */
	public static <T extends Serializable & Comparable<T>> SerializablePredicate<T> lessThanOrEqualTo(@NotNull T value) {
		return actual -> actual.compareTo(value) <= 0;
	}

	/**
	 * Returns a predicate that is satisfied when the tested value is greater than the supplied value
	 * @param value The value to test against
	 * @return A predicate
	 * @param <T> The type of comparable
	 */
	public static <T extends Serializable & Comparable<T>> SerializablePredicate<T> greaterThan(@NotNull T value) {
		return actual -> actual.compareTo(value) > 0;
	}

	/**
	 * Returns a predicate that is satisfied when the tested value is greater than or equal to the supplied value
	 * @param value The value to test against
	 * @return A predicate
	 * @param <T> The type of comparable
	 */
	public static <T extends Serializable & Comparable<T>> SerializablePredicate<T> greaterThanOrEqualTo(@NotNull T value) {
		return actual -> actual.compareTo(value) >= 0;
	}
}
