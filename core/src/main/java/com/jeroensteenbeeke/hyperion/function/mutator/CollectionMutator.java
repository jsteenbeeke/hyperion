package com.jeroensteenbeeke.hyperion.function.mutator;

import io.vavr.collection.Seq;

import org.jetbrains.annotations.NotNull;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * A mutator that returns a collection element
 *
 * @param <A> The base object type
 * @param <E> The element type
 * @param <S> The type of collection
 */
public final class CollectionMutator<A, E, S extends Seq<E>> extends Mutator<A, E> {
	private final Function<A, S> collectionGetter;

	private final BiFunction<A, S, A> collectionSetter;

	private final Predicate<E> elementMatcher;

	CollectionMutator(
			Function<A, S> collectionGetter, BiFunction<A, S, A> collectionSetter,
			Predicate<E> elementMatcher) {
		this.collectionGetter = collectionGetter;
		this.collectionSetter = collectionSetter;
		this.elementMatcher = elementMatcher;
	}

	@Override
	public E get(@NotNull A target) {
		return collectionGetter.apply(target).find(elementMatcher)
				.getOrElseThrow(() -> new IllegalStateException("No elements matched by predicate"));
	}

	@Override
	public A set(@NotNull A target, @NotNull E newValue) {
		S apply = collectionGetter.apply(target);
		return apply.find(elementMatcher).map(element -> {
					@SuppressWarnings("unchecked")
					S replacement = (S) apply.replace(element, newValue);
					return collectionSetter.apply(target,
							replacement);
				})
				.getOrElse(target);
	}
}
