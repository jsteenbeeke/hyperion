package com.jeroensteenbeeke.hyperion.function;

/**
 * Unary operator for float primitives
 */
@FunctionalInterface
public interface FloatUnaryOperator {
	/**
     * Applies the function to the given float
	 * @param f A float
	 * @return Another float
	 */
	float apply(float f);
}
