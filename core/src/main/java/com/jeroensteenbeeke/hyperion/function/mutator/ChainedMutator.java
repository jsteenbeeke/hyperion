package com.jeroensteenbeeke.hyperion.function.mutator;

import org.danekja.java.util.function.serializable.SerializableBiFunction;
import org.danekja.java.util.function.serializable.SerializableFunction;

import org.jetbrains.annotations.NotNull;

/**
 * Chains getters and setters on top of an existing mutator
 * @param <A> The target object
 * @param <B> The property returned by the base mutator
 * @param <C> The desired property
 */
public final class ChainedMutator<A,B,C> extends Mutator<A,C> {
	private final Mutator<A,B> base;

	private final SerializableFunction<B,C> getter;

	private final SerializableBiFunction<B,C,B> setter;

	ChainedMutator(Mutator<A, B> base,
			SerializableFunction<B, C> getter,
			SerializableBiFunction<B, C, B> setter) {
		this.base = base;
		this.getter = getter;
		this.setter = setter;
	}

	@Override
	public C get(@NotNull A target) {
		return getter.apply(base.get(target));
	}

	@Override
	public A set(@NotNull A target, @NotNull C newValue) {
		return base.set(target, setter.apply(base.get(target), newValue));
	}
}
