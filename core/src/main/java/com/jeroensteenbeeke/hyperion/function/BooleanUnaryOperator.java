package com.jeroensteenbeeke.hyperion.function;

/**
 * Unary operator for boolean primitives
 */
@FunctionalInterface
public interface BooleanUnaryOperator {
	/**
	 * Applies the function to the given boolean
	 * @param b A boolean
	 * @return Another boolean
	 */
	boolean apply(boolean b);
}
