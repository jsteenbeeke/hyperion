package com.jeroensteenbeeke.hyperion.function.mutator;

import org.jetbrains.annotations.NotNull;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * The base type of mutator that returns a simple property
 * @param <A> The base object type
 * @param <B> The property type
 */
public final class SimpleMutator<A,B> extends Mutator<A,B> {
	private final Function<A,B> getter;

	private final BiFunction<A, B, A> setter;

	SimpleMutator(Function<A,B> getter, BiFunction<A, B, A> setter) {
		this.getter = getter;
		this.setter = setter;
	}

	@Override
	public B get(@NotNull A target) {
		return getter.apply(target);
	}

	@Override
	public A set(@NotNull A target, @NotNull B newValue) {
		return setter.apply(target, newValue);
	}
}
