package com.jeroensteenbeeke.hyperion.function.mutator;

import io.vavr.collection.Seq;
import io.vavr.control.Option;
import org.danekja.java.util.function.serializable.SerializableBiFunction;
import org.danekja.java.util.function.serializable.SerializableFunction;

import org.jetbrains.annotations.NotNull;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;

/**
 * Abstraction for dealing with immutable objects in a mutable fashion, similar to lenses, but slightly more complicated
 * to handle immutable vavr.io collections
 *
 * @param <A> The type of containing object
 * @param <B> The type of contained value
 */
public abstract sealed class Mutator<A, B> permits SimpleMutator, CollectionMutator, ChainedMutator {

	/**
	 * Returns a new instance of A with the modified value
	 *
	 * @param target   The target object
	 * @param newValue The value to modify
	 * @return A modified instance of A
	 */
	public abstract A set(@NotNull A target, @NotNull B newValue);

	/**
	 * Returns a new instance of A with the modified value
	 *
	 * @param target   The target object
	 * @param newValue The function for adapting the existing value
	 * @return A modified instance of A
	 */
	public A set(@NotNull A target, @NotNull UnaryOperator<B> newValue) {
		return set(target, newValue.apply(get(target)));
	}

	/**
	 * Returns the current value. Mostly used internally for chaining
	 *
	 * @param target The target object
	 * @return The current value
	 */
	public abstract B get(@NotNull A target);

	/**
	 * Chains the current mutator, allowing access of underlying properties that update the parent reference
	 *
	 * @param setter The setter for the property
	 * @param <C>    The type of property
	 * @return A builder step allowing the specification of the getter
	 */
	public <C> AndGet<B, C, ChainedMutator<A, B, C>> andThen(@NotNull SerializableBiFunction<B, C, B> setter) {
		return getter -> new ChainedMutator<>(this, getter, setter);
	}

	/**
	 * Casts the current mutator to a subtype.
	 *
	 * @param target The target subtype
	 * @param <C>    The type of property
	 * @return A builder step allowing the specification of the getter
	 */
	public <C extends B> AndGet<B, C, ChainedMutator<A, B, C>> subtype(@SuppressWarnings("unused") Class<C> target) {
		return getter -> new ChainedMutator<>(this, getter, (b, c) -> c);
	}

	/**
	 * Chains the current mutator, allowing access of underlying properties that update the parent reference, specialized
	 * for vavr sequences
	 *
	 * @param setter The setter for the property
	 * @param <C>    The type of property
	 * @param <S>    The type of collection
	 * @return A builder step allowing the specification of the getter
	 */
	public <C, S extends Seq<C>> AndGet<B, S, Matching<C, ChainedMutator<A, B, C>>> andThenSeq(
			@NotNull SerializableBiFunction<B, S, B> setter) {
		return getter -> predicate -> {
			SerializableFunction<B, C> propertyGetter = getter.andThen(
					seq -> seq.find(predicate)
							.getOrElseThrow(() -> new IllegalStateException("Predicate has no matches")));
			SerializableBiFunction<B, C, B> propertySetter = (base, element) -> {
				S currentSeq = getter.apply(base);
				@SuppressWarnings("unchecked")
				S replacement = (S) currentSeq.replace(propertyGetter.apply(base), element);
				return setter.apply(base,
						replacement);
			};

			return new ChainedMutator<>(this, propertyGetter, propertySetter
			);
		};
	}

	/**
	 * Builder for creating a new simple mutator
	 *
	 * @param setter The setter to set the property
	 * @param <A>    The type of target object
	 * @param <B>    The type of value
	 * @return A builder step for specifying the target
	 */
	public static <A, B> AndGet<A, B, SimpleMutator<A, B>> set(@NotNull SerializableBiFunction<A, B, A> setter) {
		return getter -> new SimpleMutator<>(getter, setter);
	}

	/**
	 * Creates specialized mutator that deals with vavr sequences
	 *
	 * @param collectionGetter The getter that retrieves the collection
	 * @param <A>              The type of base object
	 * @param <E>              The elements in the collection
	 * @param <S>              The type of collection
	 * @return The next builder step
	 */
	public static <A, E, S extends Seq<E>> Using<A, E, S, Matching<E, CollectionMutator<A, E, S>>> setSeq(
			@NotNull SerializableFunction<A, S> collectionGetter) {
		return collectionSetter -> predicate -> new CollectionMutator<>(
				collectionGetter,
				collectionSetter, predicate);
	}


	/**
	 * Builder step for specifying the getter
	 *
	 * @param <A>    The type of base object
	 * @param <B>    The type of property
	 * @param <NEXT> The next step in the builder chain
	 */
	@FunctionalInterface
	public interface AndGet<A, B, NEXT> {
		/**
		 * Specifies the getter for the given property
		 *
		 * @param getter The getter
		 * @return The next builder step
		 */
		NEXT andGet(@NotNull SerializableFunction<A, B> getter);

		/**
		 * Specifies the getter for the given property, allowing for optional return types
		 * @param getter The getter
		 * @return The next builder step
		 */
		default NEXT andGetOpt(@NotNull SerializableFunction<A, Option<B>> getter) {
			return andGet(getter.andThen(Option::get));
		}
	}

	/**
	 * Builder step for specifying the collection setter
	 *
	 * @param <A>    The type of base object
	 * @param <E>    The type of collection element
	 * @param <S>    The type of collection
	 * @param <NEXT> The next builder step
	 */
	@FunctionalInterface
	public interface Using<A, E, S extends Seq<E>, NEXT> {
		/**
		 * Sets the collection
		 *
		 * @param collectionSetter The collection
		 * @return The next builder step
		 */
		NEXT using(@NotNull SerializableBiFunction<A, S, A> collectionSetter);
	}

	/**
	 * Builder step for specifying the predicate to match an element by
	 *
	 * @param <E>    The type of element
	 * @param <NEXT> The next step
	 */
	@FunctionalInterface
	public interface Matching<E, NEXT> {
		/**
		 * Builder step for specifying the predicate for matching the proper element in the collection
		 *
		 * @param elementMatcher The predicate for matching the collection element
		 * @return The next builder step
		 */
		NEXT matching(@NotNull Predicate<E> elementMatcher);
	}

}
