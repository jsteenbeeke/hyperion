package com.jeroensteenbeeke.hyperion.function;

import io.vavr.Function1;
import io.vavr.Tuple2;
import io.vavr.Tuple3;
import io.vavr.control.Option;
import org.jetbrains.annotations.NotNull;

import java.util.function.Supplier;

/**
 * Utility class with functional synthetic sugar
 */
public class Fn {

	/**
	 * Supplies a function that combines the input into a tuple with the value provided by the given supplier
	 *
	 * @param supplier The supplier for the new value
	 * @param <T>      The type already present
	 * @param <U>      The newly supplied type
	 * @return The function
	 */
	@NotNull
	public static <T, U> Function1<T, Tuple2<T, U>> combineWith(@NotNull Supplier<U> supplier) {
		return t -> new Tuple2<>(t, supplier.get());
	}

	/**
	 * Supplies a function that combines the input into a tuple with the value provided by the given supplier, if
	 * it is a non-empty option
	 *
	 * @param supplier The supplier for the new value
	 * @param <T>      The type already present
	 * @param <U>      The newly supplied type
	 * @return The function
	 */
	@NotNull
	public static <T, U> Function1<T, Option<Tuple2<T, U>>> maybeCombineWith(@NotNull Supplier<Option<U>> supplier) {
		return t -> supplier.get().map(u -> new Tuple2<>(t, u));
	}

	/**
	 * Supplies a function that combines a given Tuple2 with a new value to form a tuple3
	 *
	 * @param supplier The supplier of the new value
	 * @param <T>      The first type of the existing tuple
	 * @param <U>      The second type of the existing tuple
	 * @param <V>      The type to add
	 * @return The function
	 */
	@NotNull
	public static <T, U, V> Function1<Tuple2<T, U>, Tuple3<T, U, V>> combineWith2(@NotNull Supplier<V> supplier) {
		return t -> new Tuple3<>(t._1, t._2, supplier.get());
	}


	/**
	 * Supplies a function that combines a given Tuple2 with a new value (if it is a non-empty option) to form a tuple3
	 *
	 * @param supplier The supplier of the new value
	 * @param <T>      The first type of the existing tuple
	 * @param <U>      The second type of the existing tuple
	 * @param <V>      The type to add
	 * @return The function
	 */
	@NotNull
	public static <T, U, V> Function1<Tuple2<T, U>, Option<Tuple3<T, U, V>>> maybeCombineWith2(@NotNull Supplier<Option<V>> supplier) {
		return t -> supplier.get().map(v -> new Tuple3<>(t._1, t._2, v));
	}

}
