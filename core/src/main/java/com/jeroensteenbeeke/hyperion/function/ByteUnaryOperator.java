package com.jeroensteenbeeke.hyperion.function;

/**
 * Unary operator for byte primitives
 */
@FunctionalInterface
public interface ByteUnaryOperator {
	/**
     * Applies the function to the given byte
	 * @param b A byte
	 * @return Another byte
	 */
	byte apply(byte b);
}
