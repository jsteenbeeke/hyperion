package com.jeroensteenbeeke.hyperion;

import com.jeroensteenbeeke.hyperion.util.ResourceUtil;
import io.vavr.control.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

/**
 * Version information class
 */
public class Hyperion {
	private static final Logger log = LoggerFactory.getLogger(Hyperion.class);

	private static HyperionSettings settings;

	/**
	 * Returns the revision (git hash) of the current Hyperion version
	 *
	 * @return A git hash, if known, an empty Option otherwise
	 */
	public static Option<String> getRevision() {
		return ResourceUtil.readResourceAsString(Hyperion.class, "revision.txt").filter(s -> !s.isEmpty());
	}

	/**
	 * Returns the commit title (the first line of the commit message) of the current Hyperion version
	 *
	 * @return The commit title, if known, an empty Option otherwise
	 */
	public static Option<String> getCommitTitle() {
		return ResourceUtil.readResourceAsString(Hyperion.class, "commit.txt").filter(s -> !s.isEmpty());
	}

	/**
	 * Returns the commit notes (everything from the second line of the commit message) of the current Hyperion version
	 *
	 * @return The commit notes, if known, an empty Option otherwise
	 */
	public static Option<String> getCommitNotes() {
		return ResourceUtil.readResourceAsString(Hyperion.class, "commit-notes.txt").filter(s -> !s.isEmpty());
	}

	/**
	 * Returns the Maven version of the current Hyperion version
	 *
	 * @return The Maven version, if known, an empty Option otherwise
	 */
	public static Option<String> getMavenVersion() {
		return ResourceUtil.readResourceAsString(Hyperion.class, "maven-version.txt").filter(s -> !s.isEmpty());
	}

	/**
	 * Gets user settings for use with Hyperion
	 *
	 * @return A HyperionSettings object
	 */
	public static synchronized HyperionSettings getSettings() {
		if (settings != null) {
			return settings;
		}
		Path userHome = Path.of(System.getProperty("user.home"));
		Path hyperionRc = userHome.resolve(".hyperionrc");
		if (Files.exists(hyperionRc)) {
			Properties props = new Properties();
			try (FileInputStream fis = new FileInputStream(hyperionRc.toFile())) {
				props.load(fis);

				return settings = new HyperionSettings(props.getProperty("user.display.name", System.getProperty("user.name", "unknown")));
			} catch (IOException e) {
				log.error("Could not open .hyperionrc, falling back to defaults");
			}
		}
		return settings = new HyperionSettings(System.getProperty("user.name", "unknown"));
	}

}
