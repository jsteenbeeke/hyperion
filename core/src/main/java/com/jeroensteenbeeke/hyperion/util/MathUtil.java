/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.util;

/**
 * Mathematical calculations not covered by java.lang.Math
 */
public final class MathUtil {
	private MathUtil() {

	}

	/**
	 * Determines the greatest common divisor between two longs
	 * @param a The first long
	 * @param b The second long
	 * @return The greatest common divisor (the largest number that both a and b can be divided without requiring fractions)
	 */
	public static long greatestCommonDivisor(long a, long b)
	{
		while (a != b) // while the two numbers are not equal...
	    { 
	        // ...subtract the smaller one from the larger one

	        if (a > b) a -= b; // if a is larger than b, subtract b from a
	        else b -= a; // if b is larger than a, subtract a from b
	    }

	    return a; // or return b, a will be equal to b either way
	}

	/**
	 * Determines the greatest common divisor between two integers
	 * @param a The first integer
	 * @param b The second integer
	 * @return The greatest common divisor (the largest number that both a and b can be divided without requiring fractions)
	 */
	public static int greatestCommonDivisor(int a, int b)
	{
		while (a != b) // while the two numbers are not equal...
	    { 
	        // ...subtract the smaller one from the larger one

	        if (a > b) a -= b; // if a is larger than b, subtract b from a
	        else b -= a; // if b is larger than a, subtract a from b
	    }

	    return a; // or return b, a will be equal to b either way
	}

	/**
	 * Determines the lowest common denominator of two longs
	 * @param a The first long
	 * @param b The second long
	 * @return The lowest common denominator (the smallest number that is a multiple of both a and b)
	 */
	public static long lowestCommonDenominator(long a, long b)
	{
	    return a * (b / greatestCommonDivisor(a, b));
	}

	/**
	 * Determines the lowest common denominator of two integers
	 * @param a The first integer
	 * @param b The second integer
	 * @return The lowest common denominator (the smallest number that is a multiple of both a and b)
	 */
	public static int lowestCommonDenominator(int a, int b)
	{
	    return a * (b / greatestCommonDivisor(a, b));
	}
}
