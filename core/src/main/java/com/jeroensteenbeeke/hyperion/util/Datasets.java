package com.jeroensteenbeeke.hyperion.util;

import com.jeroensteenbeeke.hyperion.annotation.Dataset;

import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Singleton for keeping track of the currently active test dataset
 */
public enum Datasets {
	INSTANCE;

	private final List<String> current = new ArrayList<>();

	Datasets() {
		current.add(DEFAULT);
	}

	/**
	 * Sets the given dataset. Should generally not be called directly except by framework methods
	 * @param dataset The dataset to set
	 */
	public void set(@NotNull String dataset) {
		if (!DEFAULT.equals(dataset)) {
			current.add(dataset);
		}
	}

	/**
	 * Unsets the current dataset unless it already is the default
	 */
	public void unset() {
		if (current.size() > 1) {
			current.remove(current.size()-1);
		}
	}

	/**
	 * Get the currently active dataset
	 * @return The active dataset
	 */
	@NotNull
	public String getActiveDataset() {
		return current.get(current.size()-1);
	}

	/**
	 * The value used for the default dataset
	 */
	public static final String DEFAULT = "$DEFAULT$";

	/**
	 * Determines the dataset used by the given class
	 * @param target The class to check
	 * @return Optionally the dataset defined by the given class
	 */
	@NotNull
	public static Optional<String> readFromClass(@NotNull Class<?> target) {
		return Optional.ofNullable(target.getAnnotation(Dataset.class)).map(Dataset::value);
	}

	/**
	 * Determines the dataset used by the given object
	 * @param target The object to check
	 * @return Optionally the dataset defined by the given object
	 */
	@NotNull
	public static Optional<String> readFromObject(@NotNull Object target) {
		return readFromClass(target.getClass());
	}
}
