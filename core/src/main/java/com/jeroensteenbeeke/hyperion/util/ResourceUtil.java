package com.jeroensteenbeeke.hyperion.util;

import io.vavr.control.Option;

import org.jetbrains.annotations.NotNull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * Utility class for handling class resources
 */
public class ResourceUtil {

	/**
	 * Reads the given class-relative resource as a String. Used for loading files within the same jar/war into a String
	 * @param resourceClass The base class
	 * @param resourceName The name of the resource to load
	 * @return An Option containing the contents of the resource, or empty in case it could not be read
	 */
	public static Option<String> readResourceAsString(@NotNull Class<?> resourceClass, @NotNull String resourceName) {
		try (InputStream stream = resourceClass.getResourceAsStream(resourceName); ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
			int i;

			if (stream == null) {
				return Option.none();
			}

			while ((i = stream.read()) != -1) {
				bos.write(i);
			}

			return Option.of(bos.toString(StandardCharsets.UTF_8).trim());
		} catch (IOException ioe) {
			return Option.none();
		}
	}
}
