/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.util;

import io.vavr.collection.HashMultimap;
import io.vavr.collection.Multimap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;
import java.time.temporal.WeekFields;
import java.util.Optional;

/**
 * Utility class for time-related operations
 */
public final class TimeUtil {

	private TimeUtil() {

	}

	/**
	 * Returns a Multimap with all dates in the given date's month grouped by week
	 *
	 * @param referenceDate The date to use as a basis
	 * @return A Multimap that has the week number as key, and the days of that week as values (inserted in
	 * chronological order)
	 */
	@NotNull
	public static Multimap<Integer, LocalDate> getMonthPerWeek(
		@NotNull LocalDate referenceDate) {
		LocalDate current = referenceDate.withDayOfMonth(1);

		final Month month = current.getMonth();

		Multimap<Integer, LocalDate> weekToDays = HashMultimap.withSeq().empty();

		while (current.getMonth() == month) {
			weekToDays = weekToDays.put(current.get(WeekFields.ISO.weekOfWeekBasedYear()), current);

			current = current.plusDays(1);
		}
		return weekToDays;
	}

	/**
	 * Formats the given date in yyyy-MM-dd format
	 *
	 * @param date The date to format
	 * @return An optional that contains the given date
	 */
	@NotNull
	public static Optional<String> formatYearMonthDay(@Nullable Temporal date) {
		return Optional.ofNullable(date).map(DateTimeFormatter.ISO_LOCAL_DATE::format);
	}

	/**
	 * Formats the given date in yyyy-MM-dd hh:mm format
	 *
	 * @param date The date to format
	 * @return An optional that contains the given date
	 */
	@NotNull
	public static Optional<String> formatYearMonthDayHourMinute(@Nullable Temporal date) {
		return Optional.ofNullable(date).map(DateTimeFormatter.ISO_LOCAL_DATE_TIME::format);

	}

}
