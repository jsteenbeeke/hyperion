/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.util;

import java.io.File;
import java.io.FileFilter;
import java.util.function.Consumer;

/**
 * Utility class for common file operations not covered by the standard Java APIs
 */
public final class FileUtil {
	private FileUtil() {

	}

	private static final char UNDERSCORE = '_';

	/**
	 * Sanitizes a filename. Removes all characters except letters, digits and underscores. Replaces whitespaces
	 * by underscores and prevents the presence of multiple subsequent underscores
	 * @param input The filename to change
	 * @return A sanitized filename, retaining only letters, digits and underscores
	 */
	public static String makeFilenameSafe(String input) {
		StringBuilder result = new StringBuilder();

		char last = 0;
		for (char c : input.toCharArray()) {
			if (Character.isLetterOrDigit(c)) {
				result.append(Character.toLowerCase(c));
				last = c;
			} else if (c == '_') {
				if (last != UNDERSCORE) {
					result.append(c);
					last = UNDERSCORE;
				}
			} else if (Character.isWhitespace(c)) {
				if (last != UNDERSCORE) {
					result.append(UNDERSCORE);
					last = UNDERSCORE;
				}
			} else {
				last = c;
			}
		}

		return result.toString();
	}

	/**
	 * Deletes the given file, and any files that are contained within it if the given file is a directory
	 * @param file The file to delete recursively
	 *             @return {@code true} if the delete succeeded, {@code false} otherwise
	 */
	public static boolean deleteTree(File file) {
		boolean result = true;

		if (file.exists()) {
			if (file.isDirectory()) {
				File[] files = file.listFiles();
				if (files != null) {
					for (File f : files) {
						result = result && deleteTree(f);
					}
				}
			}

			result = result && file.delete();
		}

		return result;
	}

	/**
	 * Analyzes the contents of a given directory and any subdirectories,
	 * applying the {@code onFound} consumer on any file matching {@code filter}
	 * 
	 * @param directory
	 *            The directory to start searching
	 * @param filter
	 *            The filter to apply to files
	 * @param onFound
	 *            The action to take on any file found
	 */
	public static void recursively(File directory, FileFilter filter,
			Consumer<File> onFound) {
		if (!directory.isDirectory() || !directory.exists()) {
			return;
		}
		for (File file : directory.listFiles()) {
			if (file.isDirectory()) {
				recursively(file, filter, onFound);
			} else {
				if (filter.accept(file)) {
					onFound.accept(file);
				}
			}
		}
	}
}
