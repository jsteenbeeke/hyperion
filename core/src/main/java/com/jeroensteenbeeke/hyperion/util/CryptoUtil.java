package com.jeroensteenbeeke.hyperion.util;

import io.vavr.control.Try;
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.util.io.pem.PemReader;

import org.jetbrains.annotations.NotNull;
import java.io.BufferedReader;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;

/**
 * Utility class for various common tasks relating to cryptographic functionality
 */
public class CryptoUtil {
	/**
	 * Reads an X509-encoded SSL certificate from file
	 *
	 * @param path The path that stores the certificate
	 * @return A Try containing the certificate
	 */
	public static Try<Certificate> readSSLCertificate(@NotNull Path path) {
		return Try.of(() -> {
			try (InputStream certStream = Files.newInputStream(path)) {
				return CertificateFactory.getInstance("X.509").generateCertificate(certStream);
			}
		});
	}

	/**
	 * Reads a RSA Private Key from a PEM file
	 *
	 * @param path The path that stores the key
	 * @return A Try containing the key
	 */
	public static Try<PrivateKey> readPrivateKey(@NotNull Path path) {
		return Try.of(() -> {
			try (BufferedReader certStream = Files.newBufferedReader(path);
				 PemReader keyReader = new PemReader(certStream)) {
				PEMParser pemParser = new PEMParser(keyReader);
				JcaPEMKeyConverter converter = new JcaPEMKeyConverter();
				PrivateKeyInfo privateKeyInfo = PrivateKeyInfo.getInstance(pemParser.readObject());

				return converter.getPrivateKey(privateKeyInfo);

			}
		});
	}
}
