package com.jeroensteenbeeke.hyperion;

import io.vavr.Tuple2;
import io.vavr.control.Option;

import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Metadata class, stores information about the currently running application
 */
public class HyperionApp {
	private static HyperionApp instance = new HyperionApp();

	private List<HyperionAppListener> listeners = new ArrayList<>();

	private String applicationVersion;

	private String applicationCommitMessage;

	/**
	 * Returns the singleton instance of HyperionApp
	 *
	 * @return The app reference
	 */
	public static HyperionApp get() {
		return instance;
	}

	/**
	 * Returns the version of this application, if known
	 *
	 * @return Optionally the version
	 */
	public Option<String> getApplicationVersion() {
		return Option.of(applicationVersion);
	}

	/**
	 * Returns the commit message of this application, if known
	 *
	 * @return Optionally the commit message
	 */
	public Option<String> getApplicationCommitMessage() {
		return Option.of(applicationCommitMessage);
	}

	/**
	 * Returns the application version data (version and commit message)
	 * @return Optionally a tuple with both the version (1) and the commit message (2)
	 */
	public Option<Tuple2<String, String>> getApplicationVersionData() {
		return getApplicationVersion().flatMap(v -> getApplicationCommitMessage().map(cm -> new Tuple2<>(v, cm)));
	}

	/**
	 * Sets the application version
	 *
	 * @param applicationVersion The version of the application
	 */
	public void setApplicationVersion(@NotNull String applicationVersion) {
		this.applicationVersion = applicationVersion;
		listeners.forEach(l -> getApplicationVersionData().forEach(vd -> l.onApplicationVersion(vd._1, vd._2)));
	}

	/**
	 * Sets the application commit message
	 * @param applicationCommitMessage The commit message
	 */
	public void setApplicationCommitMessage(String applicationCommitMessage) {
		this.applicationCommitMessage = applicationCommitMessage;
		listeners.forEach(l -> getApplicationVersionData().forEach(vd -> l.onApplicationVersion(vd._1, vd._2)));
	}

	/**
	 * Registers a new listener
	 *
	 * @param listener The listener in to register
	 */
	public void registerListener(@NotNull HyperionAppListener listener) {
		listeners.add(listener);
	}
}
