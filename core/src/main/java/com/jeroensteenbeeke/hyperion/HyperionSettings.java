package com.jeroensteenbeeke.hyperion;

/**
 * Settings for Hyperion. Allows the user to specify certain defaults in cases where information
 * about the user is required and system properties provide a nonsensical default
 */
public class HyperionSettings {
	private final String userDisplayName;

	/**
	 * Create a new settings object
	 * @param userDisplayName The user's display name
	 */
	public HyperionSettings(String userDisplayName) {
		this.userDisplayName = userDisplayName;
	}

	/**
	 * Returns the display name as configured by the user
	 * @return The display name, or if not configured: the username
	 */
	public String getUserDisplayName() {
		return userDisplayName;
	}
}
