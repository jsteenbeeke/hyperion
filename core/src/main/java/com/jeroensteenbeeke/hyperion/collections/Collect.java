package com.jeroensteenbeeke.hyperion.collections;

import io.vavr.control.Option;
import org.danekja.java.util.function.serializable.SerializableFunction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Convenience methods for working with collections that are absent in Java.
 * <p>
 * Useful when wanting to avoid Guava
 */
public class Collect {

	/**
	 * Concatenates two lists into an immutable list
	 *
	 * @param a   The first list
	 * @param b   The second list
	 * @param <T> The type contained in the list
	 * @return An immutable list that contains all elements of list a followed by all elements of list b
	 */
	public static <T> List<T> concat(@NotNull List<T> a, List<T> b) {
		return List.copyOf(mutableConcat(a, b));
	}

	/**
	 * Concatenates two lists into a mutable list
	 *
	 * @param a   The first list
	 * @param b   The second list
	 * @param <T> The type contained in the list
	 * @return A mutable list that contains all elements of list a followed by all elements of list b
	 */
	@NotNull
	public static <T> List<T> mutableConcat(@NotNull List<T> a, List<T> b) {
		List<T> result = new ArrayList<>(a.size() + b.size());
		result.addAll(a);
		result.addAll(b);
		return result;
	}

	/**
	 * Concatenates two sets into an immutable set
	 *
	 * @param a   The first set
	 * @param b   The second set
	 * @param <T> The type contained in the set
	 * @return An immutable set that contains all elements of set a and all elements of set b
	 */
	public static <T> Set<T> union(@NotNull Set<T> a, Set<T> b) {
		return Set.copyOf(mutableUnion(a, b));
	}

	/**
	 * Concatenates two sets into a mutable set
	 *
	 * @param a   The first set
	 * @param b   The second set
	 * @param <T> The type contained in the set
	 * @return A mutable set that contains all elements of set a and all elements of set b
	 */
	public static <T> Set<T> mutableUnion(Set<T> a, Set<T> b) {
		Set<T> result = new HashSet<>(a.size() + b.size(), 1.0f);
		result.addAll(a);
		result.addAll(b);
		return result;
	}

	/**
	 * Returns the first non-null parameter, in order
	 * @param first The first parameter
	 * @param second The second parameter
	 * @return An Option containing the first non-null parameter, or empty if they are all null
	 * @param <T> The type of value
	 */
	@NotNull
	public static <T> Option<T> coalesce(@Nullable T first, @Nullable T second) {
		return coalesce(first, second, null, null, null, null, null, null);

	}

	/**
	 * Returns the first non-null parameter, in order
	 * @param first The first parameter
	 * @param second The second parameter
	 * @param third The third parameter
	 * @return An Option containing the first non-null parameter, or empty if they are all null
	 * @param <T> The type of value
	 */
	@NotNull
	public static <T> Option<T> coalesce(@Nullable T first, @Nullable T second, @Nullable T third) {
		return coalesce(first, second, third, null, null, null, null, null);

	}

	/**
	 * Returns the first non-null parameter, in order
	 * @param first The first parameter
	 * @param second The second parameter
	 * @param third The third parameter
	 * @param fourth The fourth parameter
	 * @return An Option containing the first non-null parameter, or empty if they are all null
	 * @param <T> The type of value
	 */
	@NotNull
	public static <T> Option<T> coalesce(@Nullable T first, @Nullable T second, @Nullable T third, @Nullable T fourth) {
		return coalesce(first, second, third, fourth, null, null, null, null);
	}

	/**
	 * Returns the first non-null parameter, in order
	 * @param first The first parameter
	 * @param second The second parameter
	 * @param third The third parameter
	 * @param fourth The fourth parameter
	 * @param fifth The fifth parameter
	 * @return An Option containing the first non-null parameter, or empty if they are all null
	 * @param <T> The type of value
	 */
	@NotNull
	public static <T> Option<T> coalesce(@Nullable T first, @Nullable T second, @Nullable T third, @Nullable T fourth, @Nullable T fifth) {
		return coalesce(first, second, third, fourth, fifth, null, null, null);
	}

	/**
	 * Returns the first non-null parameter, in order
	 * @param first The first parameter
	 * @param second The second parameter
	 * @param third The third parameter
	 * @param fourth The fourth parameter
	 * @param fifth The fifth parameter
	 * @param sixth The sixth parameter
	 * @return An Option containing the first non-null parameter, or empty if they are all null
	 * @param <T> The type of value
	 */
	@NotNull
	public static <T> Option<T> coalesce(
		@Nullable T first, @Nullable T second, @Nullable T third, @Nullable T fourth, @Nullable T fifth, @Nullable T sixth) {
		return coalesce(first, second, third, fourth, fifth, sixth, null, null);

	}

	/**
	 * Returns the first non-null parameter, in order
	 * @param first The first parameter
	 * @param second The second parameter
	 * @param third The third parameter
	 * @param fourth The fourth parameter
	 * @param fifth The fifth parameter
	 * @param sixth The sixth parameter
	 * @param seventh The seventh parameter
	 * @return An Option containing the first non-null parameter, or empty if they are all null
	 * @param <T> The type of value
	 */
	@NotNull
	public static <T> Option<T> coalesce(
		@Nullable T first, @Nullable T second, @Nullable T third, @Nullable T fourth, @Nullable T fifth, @Nullable T sixth, @Nullable T seventh) {
		return coalesce(first, second, third, fourth, fifth, sixth, seventh, null);
	}

	/**
	 * Returns the first non-null parameter, in order
	 * @param first The first parameter
	 * @param second The second parameter
	 * @param third The third parameter
	 * @param fourth The fourth parameter
	 * @param fifth The fifth parameter
	 * @param sixth The sixth parameter
	 * @param seventh The seventh parameter
	 * @param eighth The eighth parameter
	 * @return An Option containing the first non-null parameter, or empty if they are all null
	 * @param <T> The type of value
	 */
	@NotNull
	public static <T> Option<T> coalesce(
		@Nullable T first, @Nullable T second, @Nullable T third, @Nullable T fourth, @Nullable T fifth, @Nullable T sixth, @Nullable T seventh,
		@Nullable T eighth) {
		if (first != null) {
			return Option.some(first);
		} else if (second != null) {
			return Option.some(second);
		} else if (third != null) {
			return Option.some(third);
		} else if (fourth != null) {
			return Option.some(fourth);
		} else if (fifth != null) {
			return Option.some(fifth);
		} else if (sixth != null) {
			return Option.some(sixth);
		} else if (seventh != null) {
			return Option.some(seventh);
		} else if (eighth != null) {
			return Option.some(eighth);
		}

		return Option.none();
	}

	/**
	 * Combines a collection and one or more elements into a new immutable list
	 * @param collection The collection to insert into the list
	 * @param elements The additional elements to insert into the list
	 * @return A combined List
	 * @param <T> The type of element in the list
	 */
	public static <T> List<T> listOf(Collection<? extends T> collection, T... elements) {
		return List.copyOf(mutableListOf(collection, elements));
	}

	/**
	 * Combines a collection and one or more elements into a new mutable list
	 * @param collection The collection to insert into the list
	 * @param elements The additional elements to insert into the list
	 * @return A combined List
	 * @param <T> The type of element in the list
	 */
	public static <T> List<T> mutableListOf(Collection<? extends T> collection, T... elements) {
		List<T> list = new ArrayList<>(collection.size() + elements.length);
		list.addAll(collection);
		list.addAll(Arrays.asList(elements));
		return list;
	}

	/**
	 * Combines a collection and one or more elements into a new immutable list
	 * @param collection The collection to insert into the list
	 * @param elements The additional elements to insert into the list
	 * @return A combined List
	 * @param <T> The type of element in the list
	 */
	public static <T> Set<T> setOf(Collection<? extends T> collection, T... elements) {
		return Set.copyOf(mutableSetOf(collection, elements));
	}

	/**
	 * Combines a collection and one or more elements into a new mutable list
	 * @param collection The collection to insert into the list
	 * @param elements The additional elements to insert into the list
	 * @return A combined List
	 * @param <T> The type of element in the list
	 */
	public static <T> Set<T> mutableSetOf(Collection<? extends T> collection, T... elements) {
		Set<T> list = new HashSet<>(collection.size() + elements.length);
		list.addAll(collection);
		list.addAll(Arrays.asList(elements));
		return list;
	}

	/**
	 * Applies a function to all elements in a list
	 * @param elementMapper The function
	 * @return A list containing the mapped elements
	 * @param <A> The type contained in the source list
	 * @param <B> The type contained in the target list
	 */
	@NotNull
	public static <A, B> SerializableFunction<List<A>, List<B>> mapList(@NotNull SerializableFunction<A, B> elementMapper) {
		return list -> list.stream().map(elementMapper).toList();
	}

	/**
	 * Applies a function to all elements in a set
	 * @param elementMapper The function
	 * @return A set containing the mapped elements
	 * @param <A> The type contained in the source set
	 * @param <B> The type contained in the target set
	 */
	@NotNull
	public static <A, B> SerializableFunction<Set<A>, Set<B>> mapSet(@NotNull SerializableFunction<A, B> elementMapper) {
		return set -> set.stream().map(elementMapper).collect(Collectors.toSet());
	}
}
