package com.jeroensteenbeeke.hyperion.annotation;

import java.lang.annotation.*;

/**
 * Constructor annotation to indicate a builder should be generated for invoking said constructor
 */
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.CONSTRUCTOR)
@Inherited
public @interface Buildable {
}
