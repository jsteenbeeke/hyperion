package com.jeroensteenbeeke.hyperion.annotation;

import java.lang.annotation.*;

/**
 * Class annotation to indicate a helper object with static fields representing the class' fields
 */
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.TYPE)
@Inherited
public @interface Reflectable {
}
