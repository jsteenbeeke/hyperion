package com.jeroensteenbeeke.hyperion.annotation;

import java.lang.annotation.*;

/**
 * Annotation used to mark a test class as requiring or providing the given dataset
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface Dataset {
	/**
	 * The dataset tied to the annotated class
	 * @return The name of the dataset
	 */
	String value();
}
