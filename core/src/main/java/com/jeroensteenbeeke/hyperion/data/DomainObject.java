/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.data;

import java.io.Serializable;

/**
 * Base interface that represents a domain object: a stored object
 * that models our problem domain
 */
public interface DomainObject extends Serializable {
	/**
	 * Get the ID of this DomainObject
	 * @return A serializable id
	 */
	Serializable getDomainObjectId();

	/**
	 * Checks if this object has been persisted
	 * @return {@code true} if the object has been persisted, {@code false} otherwise
	 */
	default boolean isSaved() {
		return getDomainObjectId() != null;
	}

	/**
	 * Delegator for getClass, circumvents proxy
	 * @return The type of BaseDomainObject
	 */
	default Class<? extends DomainObject> getEntityClass() {
		return getClass();
	}
}
