package com.jeroensteenbeeke.hyperion;

import org.jetbrains.annotations.NotNull;
import java.io.File;

/**
 * Singleton class for determining configuration locations
 */
public enum ConfigContext {
	Instance;

	private File configDirectory = null;

	/**
	 * Determine the configuration directory
	 *
	 * @return A file pointing to the configuration. Need not exist
	 */
	@NotNull
	public File getConfigDirectory() {
		if (configDirectory == null) {
			final String home = System.getProperty("user.home");
			final String sep = System.getProperty("file.separator");
			final String cfgdirname = String.format("%s%s.hyperion%s", home, sep,
					sep);

			configDirectory = new File(cfgdirname);
		}

		return configDirectory;
	}

	/**
	 * Set the config directory to the given file
	 *
	 * @param configDirectory The file to use as config directory. Does not need to exist, just has to be non-null
	 */
	public void setConfigDirectory(@NotNull File configDirectory) {
		this.configDirectory = configDirectory;
	}

}
