package com.jeroensteenbeeke.hyperion.keycloak;

/**
 * Container interface for constants
 */
public class HyperionKeycloak {
	/**
	 * Name of the system property used to indicate that Hyperion is running in testmode, and that the Keycloak layer
	 * should be disabled
	 */
	public static String PROPERTY_TESTMODE = "hyperion.testmode";

	/**
	 * User UUID used when Keycloak is bypassed due to testmode
	 */
	public static String TESTMODE_USER_UUID = "905d7a97-197c-48b2-bde2-aee0611ecd10";
}
