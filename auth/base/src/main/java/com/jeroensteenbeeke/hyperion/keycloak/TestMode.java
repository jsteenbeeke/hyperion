package com.jeroensteenbeeke.hyperion.keycloak;

import io.vavr.Tuple2;
import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import io.vavr.control.Option;

import org.jetbrains.annotations.NotNull;
import java.util.Deque;
import java.util.LinkedList;

/**
 * Helper class for dealing with various user roles in test mode
 */
public enum TestMode
{
	INSTANCE;

	private final Deque<Set<String>> roles;

	/**
	 * Constructor
	 */
	TestMode() {
		this.roles = new LinkedList<>();
		this.roles.offer(HashSet.empty());
	}

	/**
	 * Gets the current test mode user, if it exists
	 * @return A tuple of the user and their roles
	 */
	@NotNull
	public Option<Tuple2<String,Set<String>>> user() {
		if (System.getProperty(HyperionKeycloak.PROPERTY_TESTMODE) == null) {
			return Option.none();
		}

		return Option.some(new Tuple2<>(HyperionKeycloak.TESTMODE_USER_UUID, roles.peekLast()));
	}

	/**
	 * Executes the given runnable with the given role assigned to the test user
	 * @param role The role granted
	 * @param runnable The action to execute
	 * @throws Exception If the action fails
	 */
	public void forRole(@NotNull String role, @NotNull Action runnable) throws Exception {
		try {
			Set<String> currentRoles = Option.of(roles.peekLast()).getOrElse(HashSet::empty);
			roles.offer(currentRoles.add(role));
			runnable.run();
		} finally {
			roles.pollLast();
		}
	}

	/**
	 * Action to be executed for a specific role
	 */
	public interface Action {
		/**
		 * Executes the action
		 * @throws Exception If an error occurs
		 */
		void run() throws Exception;
	}
}
