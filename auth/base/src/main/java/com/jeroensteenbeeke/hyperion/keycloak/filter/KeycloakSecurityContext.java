package com.jeroensteenbeeke.hyperion.keycloak.filter;

import io.vavr.collection.Set;
import org.jetbrains.annotations.NotNull;

/**
 * Security context for Keycloak
 * @param userId The user ID
 * @param roles The roles of the current user
 */
public record KeycloakSecurityContext(@NotNull String userId, @NotNull Set<String> roles) {
}
