package com.jeroensteenbeeke.hyperion.keycloak.filter;

import com.jeroensteenbeeke.hyperion.metrics.MetricsProvidingFilter;
import com.nimbusds.jose.JWSAlgorithm;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.pac4j.core.adapter.FrameworkAdapter;
import org.pac4j.core.authorization.authorizer.DefaultAuthorizers;
import org.pac4j.core.authorization.authorizer.IsAuthenticatedAuthorizer;
import org.pac4j.core.client.Client;
import org.pac4j.core.client.Clients;
import org.pac4j.core.config.Config;
import org.pac4j.core.engine.DefaultCallbackLogic;
import org.pac4j.core.profile.ProfileHelper;
import org.pac4j.core.profile.factory.ProfileManagerFactory;
import org.pac4j.http.client.direct.HeaderClient;
import org.pac4j.jee.context.JEEContextFactory;
import org.pac4j.jee.context.JEEFrameworkParameters;
import org.pac4j.jee.context.session.JEESessionStoreFactory;
import org.pac4j.jee.http.adapter.JEEHttpActionAdapter;
import org.pac4j.oidc.client.KeycloakOidcClient;
import org.pac4j.oidc.config.KeycloakOidcConfiguration;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.jeroensteenbeeke.hyperion.keycloak.HyperionKeycloak.PROPERTY_TESTMODE;

/**
 * Servlet filter class that enabled Keycloak authentication through OIDC, except when
 * the system property {@code hyperion.testmode} is set OR the request is an OPTIONS request.
 * <p>
 * Also adds {@code Access-Control-Allow-Origin: *} and {@code Access-Control-Allow-Headers: authorization content-type} headers
 */
public class HyperionKeycloakFilter implements MetricsProvidingFilter {
	public static final String HYPERION_KEYCLOAK_USER_PROFILE = "hyperion.keycloak.user.profile";

	private static final String ACCESS_CONTROL_ALLOW_HEADERS = "accessControlAllowHeaders";

	private static final String SKIP_PATTERN_KEY = "keycloak.config.skipPattern";

	private static final String KEYCLOAK_URL_KEY = "keycloak.config.url";

	private static final String KEYCLOAK_REALM_KEY = "keycloak.config.realm";

	private static final String KEYCLOAK_CLIENT_ID_KEY = "keycloak.config.client.id";

	private static final String KEYCLOAK_CLIENT_SECRET_KEY = "keycloak.config.client.secret";

	private static final String DEFAULT_ACCESS_CONTROL_ALLOW_HEADERS = "authorization, content-type";

	private String accessControlAllowHeaders;

	private KeycloakOidcClient keycloakOidcClient;

	private Pattern skipPattern;

	private Config pac4jConfig;

	private Clients clients;

	private String serverBaseUrl;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		String headers = filterConfig.getInitParameter(ACCESS_CONTROL_ALLOW_HEADERS);

		accessControlAllowHeaders = Objects.requireNonNullElse(headers, DEFAULT_ACCESS_CONTROL_ALLOW_HEADERS);

		var keycloakOidcConfiguration = new KeycloakOidcConfiguration();
		keycloakOidcConfiguration.setBaseUri(getRequiredInitParam(filterConfig, KEYCLOAK_URL_KEY));
		keycloakOidcConfiguration.setRealm(getRequiredInitParam(filterConfig, KEYCLOAK_REALM_KEY));
		keycloakOidcConfiguration.setClientId(getRequiredInitParam(filterConfig, KEYCLOAK_CLIENT_ID_KEY));
		keycloakOidcConfiguration.setPreferredJwsAlgorithm(JWSAlgorithm.RS256);
		processOptionalParameter(filterConfig, KEYCLOAK_CLIENT_SECRET_KEY, keycloakOidcConfiguration::setSecret);
		processOptionalParameter(filterConfig, SKIP_PATTERN_KEY, pattern -> skipPattern = Pattern.compile(pattern));

		serverBaseUrl = Optional.ofNullable(System.getenv("SERVER_BASE_URL"))
			.or(() -> Optional.ofNullable(System.getProperty("server.base.url")))
			.orElse("http://localhost:8080");
		var callbackUrl = serverBaseUrl
						  + "/callback";

		keycloakOidcClient = new KeycloakOidcClient(keycloakOidcConfiguration);
		keycloakOidcClient.setName("KeycloakOIDC");
		keycloakOidcClient.setSaveProfileInSession(true);
		keycloakOidcClient.setCallbackUrl(callbackUrl);
		keycloakOidcClient.init();

		HeaderClient headerClient = new HeaderClient("Authorization", "Bearer ", keycloakOidcClient.getProfileCreator());

		pac4jConfig = new Config(callbackUrl, keycloakOidcClient, headerClient);
		pac4jConfig.setCallbackLogic(DefaultCallbackLogic.INSTANCE);
		pac4jConfig.setHttpActionAdapter(JEEHttpActionAdapter.INSTANCE);
		pac4jConfig.setProfileManagerFactory(ProfileManagerFactory.DEFAULT);
		pac4jConfig.setSessionStoreFactoryIfUndefined(JEESessionStoreFactory.INSTANCE);
		pac4jConfig.setWebContextFactoryIfUndefined(JEEContextFactory.INSTANCE);

		pac4jConfig.addAuthorizer(DefaultAuthorizers.IS_AUTHENTICATED, new IsAuthenticatedAuthorizer());

		clients = new Clients(callbackUrl, keycloakOidcClient, headerClient);

		initializeVersionMetrics();
	}


	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpServletRequest = Optional.ofNullable(req)
			.filter(r -> r instanceof HttpServletRequest)
			.map(r -> (HttpServletRequest) r)
			.orElseThrow(() -> new ServletException("ServletRequest is not a HttpServletRequest"));
		HttpServletResponse httpServletResponse = Optional.ofNullable(res)
			.filter(r -> r instanceof HttpServletResponse)
			.map(r -> (HttpServletResponse) r)
			.orElseThrow(() -> new ServletException("ServletResponse is not a HttpServletResponse"));
		httpServletResponse.addHeader("Access-Control-Allow-Origin", "*");
		httpServletResponse.addHeader("Access-Control-Allow-Headers", accessControlAllowHeaders);

		withMetrics(httpServletRequest, httpServletResponse, (rq, rs) -> {
			if (System.getProperty(PROPERTY_TESTMODE) != null || rq.getMethod().equals("OPTIONS")) {
				chain.doFilter(req, res);
			} else {
				internalDoFilter(req, res, chain);
			}
		});
	}

	private void internalDoFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;

		if (shouldSkip(request)) {
			chain.doFilter(req, res);
			return;
		}

		FrameworkAdapter.INSTANCE.applyDefaultSettingsIfUndefined(pac4jConfig);

		String requestPath = request.getRequestURI().substring(request.getContextPath().length());

		JEEFrameworkParameters frameworkParameters = new JEEFrameworkParameters(request, response);
		if (requestPath.startsWith("/callback")) {
			pac4jConfig.getCallbackLogic().perform(pac4jConfig, serverBaseUrl, true, keycloakOidcClient.getName(), frameworkParameters);
		} else {
			pac4jConfig.getSecurityLogic().perform(pac4jConfig, (ctx, session, profiles) -> {
					ProfileHelper.flatIntoOneProfile(profiles).ifPresent(userProfile -> request.setAttribute(HYPERION_KEYCLOAK_USER_PROFILE, userProfile));
					chain.doFilter(request, response);
					request.removeAttribute(HYPERION_KEYCLOAK_USER_PROFILE);
					return null;
				}, clients.getClients().stream().map(Client::getName).collect(Collectors.joining(",")),
				String.join(",", pac4jConfig.getAuthorizers().keySet()), String.join(",", pac4jConfig.getMatchers().keySet()), frameworkParameters);
		}
	}

	private boolean shouldSkip(HttpServletRequest request) {

		if (skipPattern == null) {
			return false;
		}

		String requestPath = request.getRequestURI().substring(request.getContextPath().length());
		return skipPattern.matcher(requestPath).matches();
	}


	private void processOptionalParameter(FilterConfig filterConfig, String key, Consumer<String> ifSet) {
		String value = filterConfig.getInitParameter(key);
		if (value != null) {
			ifSet.accept(value);
		}
	}

	private String getRequiredInitParam(FilterConfig filterConfig, String key) throws ServletException {
		var value = filterConfig.getInitParameter(key);

		if (value == null) {
			throw new ServletException("Invalid HyperionKeycloakFilter configuration, missing init param " + key);
		}

		return value;
	}
}
