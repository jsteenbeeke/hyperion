package com.jeroensteenbeeke.hyperion.keycloak.filter;

import com.jeroensteenbeeke.hyperion.HyperionApp;
import com.jeroensteenbeeke.hyperion.rest.test.AbstractBackendTest;
import com.jeroensteenbeeke.hyperion.solitary.InMemory;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.NullServiceProvider;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.ServiceProvider;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

public class HyperionKeycloakFilterTest extends AbstractBackendTest {
	@Test
	public void testKeycloakFilter() throws Exception {
		HyperionApp.get().setApplicationVersion("1337");
		HyperionApp.get().setApplicationCommitMessage("Commit message");

		String metrics = get("/metrics").expectingOK().getAs(String.class);

		assertThat(metrics, containsString("hyperion_version"));
		assertThat(metrics, containsString("docker_image_version"));
		assertThat(metrics, containsString("application_version"));
		assertThat(metrics, containsString("application_start_time"));
	}

	@Override
	protected InMemory.Handler createApplicationHandler() throws Exception {
		return InMemory.run("keycloak-test").withContextPath("/").atPort(8081).orElseThrow(IllegalStateException::new);
	}

	@Override
	protected String getApplicationHeaderKey() {
		return "X-Unused-Header";
	}

	@Override
	protected String getDefaultApplicationHeader() {
		return "Test";
	}

	@Override
	@NotNull
	protected ServiceProvider getServiceProvider() {
		return new NullServiceProvider();
	}
}
