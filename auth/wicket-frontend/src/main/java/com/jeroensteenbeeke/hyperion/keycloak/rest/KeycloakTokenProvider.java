package com.jeroensteenbeeke.hyperion.keycloak.rest;

import com.jeroensteenbeeke.hyperion.keycloak.wicket.KeycloakWebSession;
import com.jeroensteenbeeke.hyperion.solstice.spring.resteasy.CustomHeaderProvider;
import com.jeroensteenbeeke.hyperion.solstice.spring.resteasy.CustomHeaderProviderRegistry;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.InitializingBean;

import jakarta.ws.rs.core.MultivaluedMap;
import java.net.URI;

/**
 * CustomHeaderProvider implementation that adds authorization headers with the known Keycloak token
 */
public class KeycloakTokenProvider implements CustomHeaderProvider, InitializingBean {
	@Override
	public void afterPropertiesSet() {
		CustomHeaderProviderRegistry.instance.registerProvider(this);
	}

	@Override
	public void addHeaders(@NotNull URI uri, @NotNull MultivaluedMap<String, Object> headers) {
		String token = KeycloakWebSession.get().getKeycloakToken();
		if (token != null) {
			headers.add("Authorization", "Bearer ".concat(token));
		}
	}
}
