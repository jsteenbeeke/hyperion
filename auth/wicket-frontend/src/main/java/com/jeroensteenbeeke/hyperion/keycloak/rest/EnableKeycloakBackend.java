package com.jeroensteenbeeke.hyperion.keycloak.rest;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * Spring autoconfigure annotation that enables the Keycloak token interceptor
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(KeycloakInterceptorConfig.class)
public @interface EnableKeycloakBackend {
}
