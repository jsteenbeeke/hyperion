package com.jeroensteenbeeke.hyperion.keycloak.wicket;

import com.jeroensteenbeeke.hyperion.keycloak.filter.HyperionKeycloakFilter;
import com.jeroensteenbeeke.hyperion.keycloak.filter.KeycloakSecurityContext;
import com.nimbusds.oauth2.sdk.token.AccessToken;
import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import io.vavr.control.Option;
import org.apache.wicket.Session;
import org.apache.wicket.authroles.authentication.AbstractAuthenticatedWebSession;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.protocol.http.servlet.ServletWebRequest;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.cycle.RequestCycle;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.pac4j.oidc.profile.keycloak.KeycloakOidcProfile;

import java.util.function.Function;

import static com.jeroensteenbeeke.hyperion.keycloak.filter.HyperionKeycloakFilter.HYPERION_KEYCLOAK_USER_PROFILE;

/**
 * Wicket session implementation that contains information about the current keycloak session. Requires
 * KeycloakOIDCFilter to be filtering the current request
 */
public class KeycloakWebSession extends AbstractAuthenticatedWebSession {
	private static final long serialVersionUID = 7802297771186768506L;

	private final String applicationResourceId;

	/**
	 * Constructor. Note that {@link RequestCycle} is not available until this constructor returns.
	 *
	 * @param applicationResourceId The resource ID of the current application
	 * @param request               The current request
	 */
	public KeycloakWebSession(String applicationResourceId, Request request) {
		super(request);
		this.applicationResourceId = applicationResourceId;
	}

	/**
	 * Returns the current security token as returned by Keycloak
	 *
	 * @return A valid Keycloak token
	 */
	public String getKeycloakToken() {
		return getKeycloakProfileProperty(KeycloakOidcProfile::getAccessToken).map(AccessToken::getValue)
			.getOrElseThrow(() -> new IllegalStateException("No keycloak token present"));
	}

	/**
	 * Returns the Keycloak security context for the current request
	 *
	 * @return The security context
	 */
	@Nullable
	protected KeycloakSecurityContext getSecurityContext() {
		Request request = RequestCycle.get().getRequest();
		if (request instanceof ServletWebRequest servletWebRequest && servletWebRequest.getContainerRequest()
			.getAttribute(HYPERION_KEYCLOAK_USER_PROFILE) instanceof KeycloakOidcProfile userProfile) {
			return new KeycloakSecurityContext(userProfile.getId(), HashSet.ofAll(userProfile.getRoles()));
		} else {
			return null;
		}
	}

	@Override
	public Roles getRoles() {
		return new Roles(getKeycloakRoles().toJavaSet());
	}

	@Override
	public boolean isSignedIn() {
		return getKeycloakToken() != null;
	}

	/**
	 * Return the Keycloak user ID of the logged in user, if any
	 *
	 * @return An Option that may contain the user ID
	 */
	@NotNull
	public Option<String> getKeycloakUserId() {
		return getKeycloakProfileProperty(KeycloakOidcProfile::getId);
	}

	/**
	 * Extracts a property from the ID token, if an ID token is present
	 *
	 * @param propertyExtractor Function to extract the property desired
	 * @param <T>               The type of the target property, usually inferred based on the passed function
	 * @return Optionally the requested token
	 */
	@NotNull
	public <T> Option<T> getKeycloakProfileProperty(@NotNull Function<KeycloakOidcProfile, T> propertyExtractor) {
		Request request = RequestCycle.get().getRequest();
		if (request instanceof ServletWebRequest servletWebRequest &&
			servletWebRequest.getContainerRequest().getAttribute(HYPERION_KEYCLOAK_USER_PROFILE) instanceof KeycloakOidcProfile profile) {
			return Option.some(propertyExtractor.apply(profile));
		} else {
			return Option.none();
		}
	}

	/**
	 * Determines the set of roles the current user has
	 *
	 * @return A set of roles the current user has
	 */
	public Set<String> getKeycloakRoles() {
		return Option.of(getSecurityContext())
			.map(KeycloakSecurityContext::roles)
			.getOrElse(HashSet::empty);
	}

	/**
	 * Gets the current Keycloak WebSession
	 *
	 * @return A KeycloakWebSession
	 */
	public static KeycloakWebSession get() {
		return (KeycloakWebSession) Session.get();
	}

	/**
	 * Trick the Maven dependency plugin so we can keep hyperion-keycloak-base as a transitive dependency
	 */
	private void unused(HyperionKeycloakFilter filter) {
		System.out.println(filter.getClass().getName());
	}
}
