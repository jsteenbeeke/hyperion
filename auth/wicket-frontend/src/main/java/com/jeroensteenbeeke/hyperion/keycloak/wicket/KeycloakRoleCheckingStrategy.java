package com.jeroensteenbeeke.hyperion.keycloak.wicket;

import org.apache.wicket.authroles.authentication.AbstractAuthenticatedWebSession;
import org.apache.wicket.authroles.authorization.strategies.role.IRoleCheckingStrategy;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;

/**
 * Implementation of an IRoleCheckingStrategy that checks the AbstractAuthenticatedWebSession against
 * the roles required
 */
public class KeycloakRoleCheckingStrategy implements IRoleCheckingStrategy {
	@Override
	public boolean hasAnyRole(Roles roles) {
		final Roles sessionRoles = AbstractAuthenticatedWebSession.get().getRoles();
		return (sessionRoles != null) && sessionRoles.hasAnyRole(roles);
	}
}
