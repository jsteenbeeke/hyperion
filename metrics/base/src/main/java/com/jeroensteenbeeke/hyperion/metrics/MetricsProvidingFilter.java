package com.jeroensteenbeeke.hyperion.metrics;

import com.jeroensteenbeeke.hyperion.Hyperion;
import com.jeroensteenbeeke.hyperion.HyperionApp;
import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.Counter;
import io.prometheus.client.Gauge;
import io.prometheus.client.Histogram;
import io.prometheus.client.exporter.common.TextFormat;
import io.vavr.control.Option;

import jakarta.servlet.Filter;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Common interface defining logic for serving out Prometheus metrics and counting active and total requests
 */
public interface MetricsProvidingFilter extends Filter {
	Histogram REQUEST_TIMES = Histogram.build()
			.name("requests_time_seconds").help("Request latency in seconds.").register();

	Gauge ACTIVE_REQUESTS = Gauge.build()
			.name("requests_active").help("Active requests").register();

	Counter TOTAL_REQUESTS = Counter.build()
			.name("requests_total").help("Total requests").register();

	Gauge HEAP_USED = Gauge.build()
			.name("jvm_heap_used").help("JVM heap used").register();

	Gauge HEAP_COMMITTED = Gauge.build()
			.name("jvm_heap_committed").help("JVM heap committed").register();

	Gauge HEAP_MAX = Gauge.build()
			.name("jvm_heap_max").help("JVM max heap size").register();

	Gauge GC_COLLECTION_COUNT = Gauge.build().name("jvm_gc_collection_count").help("Collection count of GC components")
			.labelNames("pool")
			.register();

	Gauge GC_COLLECTION_TIME = Gauge.build().name("jvm_gc_collection_time").help("Collection time of GC components")
			.labelNames("pool")
			.register();


	AtomicBoolean versionsInitialized = new AtomicBoolean(false);

	/**
	 * Execute logic for basic prometheus metrics. If the URL matches the application context path suffixed by /metrics,
	 * this will output collected prometheus metrics. If not, this will defer execution to the given invocation
	 *
	 * @param request   The HTTP request
	 * @param response  The HTTP response
	 * @param operation The operation to invoke if not matching /metrics
	 * @throws IOException      If for some reason we can't perform the operation or output data
	 * @throws ServletException If something else fails
	 */
	default void withMetrics(
			HttpServletRequest request, HttpServletResponse response,
			ServletOrFilterInvocation operation) throws IOException, ServletException {
		Histogram.Timer timer = REQUEST_TIMES.startTimer();
		try {

			String contextPath = request.getServletContext().getContextPath();
			String targetUrl = contextPath.endsWith("/") ? contextPath + "metrics" : contextPath + "/metrics";

			updateHeapMetrics();

			if (request.getRequestURI().startsWith(targetUrl)) {
				response.setContentType(TextFormat.CONTENT_TYPE_004);

				try (ServletOutputStream out = response.getOutputStream(); Writer writer = new OutputStreamWriter(
						out)) {
					TextFormat.write004(writer, CollectorRegistry.defaultRegistry.metricFamilySamples());
					writer.flush();
					out.flush();
				}
			} else {
				try {
					ACTIVE_REQUESTS.inc();
					TOTAL_REQUESTS.inc();

					operation.invoke(request, response);
				} finally {
					ACTIVE_REQUESTS.dec();
				}
			}
		} finally {
			timer.observeDuration();
			timer.close();
		}
	}

	private void updateHeapMetrics() {
		Optional.ofNullable(ManagementFactory.getMemoryMXBean()).map(MemoryMXBean::getHeapMemoryUsage)
				.ifPresent(heap -> {
					HEAP_USED
							.set(heap.getUsed());
					HEAP_COMMITTED
							.set(heap.getCommitted());
					HEAP_MAX
							.set(heap.getMax());
				});

		ManagementFactory.getGarbageCollectorMXBeans().forEach(mx -> {
			GC_COLLECTION_COUNT.labels(mx.getName()).set(mx.getCollectionCount());
			GC_COLLECTION_TIME.labels(mx.getName()).set(mx.getCollectionTime());
		});
	}

	/**
	 * Initialize all version metrics
	 */
	default void initializeVersionMetrics() {
		if (versionsInitialized.compareAndSet(false, true)) {
			registerAppStartMetric();
			registerHyperionVersionMetric();
			registerHyperionMavenVersionMetric();
			registerDockerVersion();
			registerApplicationVersion();
			registerJvmMetrics();
		}
	}

	/**
	 * Exposes various JVM metrics
	 */
	default void registerJvmMetrics() {
		Gauge.build().name("jvm_info")
				.help("The JVM vendor")
				.labelNames("vendor", "version", "processors_available", "gc")
				.register()
				.labels(System.getProperty("java.vendor"), System.getProperty("java.version"), Integer.toString(Runtime.getRuntime().availableProcessors()), identifyGarbageCollector())
				.inc();
		Gauge.build().name("jvm_os_info")
				.help("The name of the OS running the JVM")
				.labelNames("arch", "name", "version")
				.register()
				.labels(System.getProperty("os.arch"), System.getProperty("os.name"), System.getProperty("os.version"))
				.inc();
	}

	/**
	 * Tries to identify the Garbage Collector based on MX Bean info
	 * @return The name of the garbage collector
	 */
	private String identifyGarbageCollector() {
		List<String> gcs = ManagementFactory.getGarbageCollectorMXBeans().stream().map(GarbageCollectorMXBean::getName)
				.toList();

		if (gcs.size() >= 2) {
			if (gcs.contains("Copy") && gcs.contains("MarkSweepCompact")) {
				return "SerialGC";
			} else if (gcs.contains("PS MarkSweep") && gcs.contains("PS Scavenge")) {
				return "ParallelGC";
			} else if (gcs.contains("G1 Young Generation") && gcs.contains("G1 Old Generation")) {
				return "G1GC";
			} else if (gcs.contains("ZGC Cycles") && gcs.contains("ZGC Pauses")) {
				return "ZGC";
			} else if (gcs.contains("Shenandoah Pauses") && gcs.contains("Shenandoah Cycles")) {
				return "Shenandoah";
			}
		} else if (gcs.size() == 1) {
			if (gcs.contains("Epsilon Heap")) {
				return "Epsilon";
			}
		}

		return "Unknown";
	}

	/**
	 * Exposes the Hyperion version as a metric
	 */
	default void registerHyperionVersionMetric() {
		Gauge
				.build()
				.name("hyperion_version")
				.help("The Hyperion version currently running")
				.labelNames("version")
				.register().labels(Hyperion.getRevision().getOrElse("unknown")).inc();
	}

	/**
	 * Exposes the Hyperion Maven version as a metric
	 */
	default void registerHyperionMavenVersionMetric() {
		Gauge
				.build()
				.name("hyperion_version_maven")
				.help("The Hyperion version as set in Maven currently running")
				.labelNames("version")
				.register().labels(Hyperion.getMavenVersion().getOrElse("unknown")).inc();
	}

	/**
	 * Exposes the Docker image version as a metric
	 */
	default void registerDockerVersion() {
		Gauge
				.build()
				.name("docker_image_version")
				.help("The Docker image hash currently running")
				.labelNames("version")
				.register().labels(Option.of(System.getenv("DOCKER_IMAGE_ID")).getOrElse("unknown")).inc();
	}

	/**
	 * Attemps to register the app version, if known. If not, it registers a listener for when it is known
	 */
	default void registerApplicationVersion() {
		HyperionApp.get().getApplicationVersion()
				.peek(this::registerAppVersion)
				.onEmpty(() -> HyperionApp.get().registerListener((v, c) -> registerAppVersion(v)));

	}

	/**
	 * Registers the app version as a metric
	 *
	 * @param version The version to register
	 */
	private void registerAppVersion(String version) {
		Gauge
				.build()
				.name("application_version")
				.help("The application version currently running")
				.labelNames("version")
				.register().labels(version).inc();
	}

	/**
	 * Registers a metric that details application start time
	 */
	default void registerAppStartMetric() {
		Gauge
				.build()
				.name("application_start_time")
				.help("The time the application started")
				.labelNames("startTime")
				.register().labels(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(LocalDateTime.now())).inc();
	}

	/**
	 * Functional interface representing a call to a servlet or filter operation
	 */
	@FunctionalInterface
	interface ServletOrFilterInvocation {
		/**
		 * Invoke the operation
		 *
		 * @param req The servlet request
		 * @param res The servlet response
		 * @throws IOException      If communication fails
		 * @throws ServletException If something else fails
		 */
		void invoke(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException;
	}
}
