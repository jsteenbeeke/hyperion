package com.jeroensteenbeeke.hyperion.metrics.filter;

import com.jeroensteenbeeke.hyperion.metrics.MetricsProvidingFilter;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

/**
 * Basic implementation of MetricsProvidingFilter that defers execution to other filters in chain. Useful as an extra
 * filter for standalone applications that implement their own authentication scheme (rather than deferring to Keycloak)
 */
public class MetricsFilter implements MetricsProvidingFilter {

	@Override
	public void init(FilterConfig filterConfig) {
		initializeVersionMetrics();
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpServletRequest = Optional.ofNullable(request)
														.filter(r -> r instanceof HttpServletRequest)
														.map(r -> (HttpServletRequest) r)
														.orElseThrow(() -> new ServletException("ServletRequest is not a HttpServletRequest"));
		HttpServletResponse httpServletResponse = Optional.ofNullable(response)
														  .filter(r -> r instanceof HttpServletResponse)
														  .map(r -> (HttpServletResponse) r)
														  .orElseThrow(() -> new ServletException("ServletResponse is not a HttpServletResponse"));

		withMetrics(httpServletRequest, httpServletResponse, chain::doFilter);
	}

	@Override
	public void destroy() {

	}
}
