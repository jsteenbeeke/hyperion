package com.jeroensteenbeeke.hyperion.color;

import org.junit.jupiter.api.Test;

import static com.jeroensteenbeeke.hyperion.color.Ansi.*;

public class AnsiTest {

	@Test
	public void outputAllColors() {
		System.out.println(black("Black"));
		System.out.println(red("Red"));
		System.out.println(green("Green"));
		System.out.println(brown("Brown"));
		System.out.println(blue("Blue"));
		System.out.println(purple("Purple"));
		System.out.println(cyan("Cyan"));
		System.out.println(lightGray("Light gray"));
		System.out.println(darkGray("Dark gray"));
		System.out.println(lightRed("Light Red"));
		System.out.println(lightGreen("Light Green"));
		System.out.println(yellow("Yellow"));
		System.out.println(lightBlue("Light Blue"));
		System.out.println(lightPurple("Light Purple"));
		System.out.println(lightCyan("Light Cyan"));
		System.out.println(white("White"));

	}
}
