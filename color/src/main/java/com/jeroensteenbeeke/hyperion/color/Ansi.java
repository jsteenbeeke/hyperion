package com.jeroensteenbeeke.hyperion.color;

import org.jetbrains.annotations.NotNull;

/**
 * Utility class for using console colors
 */
public class Ansi {
	private static final String RESET = "\u001B[0m";
	private static final String BLACK = "\u001B[30m";
	private static final String RED = "\u001B[31m";
	private static final String GREEN = "\u001B[32m";
	private static final String BROWN = "\u001B[33m";
	private static final String BLUE = "\u001B[34m";
	private static final String PURPLE = "\u001B[35m";
	private static final String CYAN = "\u001B[36m";
	private static final String LIGHT_GRAY = "\u001B[37m";


	private static final String DARK_GRAY = "\u001B[30;1m";
	private static final String LIGHT_RED = "\u001B[31;1m";
	private static final String LIGHT_GREEN = "\u001B[1;32m";
	private static final String YELLOW = "\u001B[1;33m";
	private static final String LIGHT_BLUE = "\u001B[1;34m";
	private static final String LIGHT_PURPLE = "\u001B[1;35m";
	private static final String LIGHT_CYAN = "\u001B[1;36m";
	private static final String WHITE = "\u001B[1;37m";

	private static final boolean ansiEnabled = shouldAnsiBeEnabled();

	private static boolean shouldAnsiBeEnabled() {
		var classPath = System.getProperty("java.class.path");

		return classPath.contains("idea_rt.jar") || System.getenv().get("TERM") != null;
	}

	/**
	 * Turns the input black
	 * @param input The input
	 * @return The colored output
	 */
	@NotNull
	public static String black(@NotNull String input) {
		return ansify(BLACK, input);
	}

	/**
	 * Turns the input red
	 * @param input The input
	 * @return The colored output
	 */
	@NotNull
	public static String red(@NotNull String input) {
		return ansify(RED, input);
	}

	/**
	 * Turns the input green
	 * @param input The input
	 * @return The colored output
	 */
	@NotNull
	public static String green(@NotNull String input) {
		return ansify(GREEN, input);
	}

	/**
	 * Turns the input brown
	 * @param input The input
	 * @return The colored output
	 */
	@NotNull
	public static String brown(@NotNull String input) {
		return ansify(BROWN, input);
	}

	/**
	 * Turns the input blue
	 * @param input The input
	 * @return The colored output
	 */
	@NotNull
	public static String blue(@NotNull String input) {
		return ansify(BLUE, input);
	}

	/**
	 * Turns the input purple
	 * @param input The input
	 * @return The colored output
	 */
	@NotNull
	public static String purple(@NotNull String input) {
		return ansify(PURPLE, input);
	}

	/**
	 * Turns the input cyan
	 * @param input The input
	 * @return The colored output
	 */
	@NotNull
	public static String cyan(@NotNull String input) {
		return ansify(CYAN, input);
	}

	/**
	 * Turns the input light gray
	 * @param input The input
	 * @return The colored output
	 */
	@NotNull
	public static String lightGray(@NotNull String input) {
		return ansify(LIGHT_GRAY, input);
	}

	/**
	 * Turns the input dark gray
	 * @param input The input
	 * @return The colored output
	 */
	@NotNull
	public static String darkGray(@NotNull String input) {
		return ansify(DARK_GRAY, input);
	}

	/**
	 * Turns the input light red
	 * @param input The input
	 * @return The colored output
	 */
	@NotNull
	public static String lightRed(@NotNull String input) {
		return ansify(LIGHT_RED, input);
	}

	/**
	 * Turns the input light green
	 * @param input The input
	 * @return The colored output
	 */
	@NotNull
	public static String lightGreen(@NotNull String input) {
		return ansify(LIGHT_GREEN, input);
	}

	/**
	 * Turns the input yellow
	 * @param input The input
	 * @return The colored output
	 */
	@NotNull
	public static String yellow(@NotNull String input) {
		return ansify(YELLOW, input);
	}

	/**
	 * Turns the input light blue
	 * @param input The input
	 * @return The colored output
	 */
	@NotNull
	public static String lightBlue(@NotNull String input) {
		return ansify(LIGHT_BLUE, input);
	}

	/**
	 * Turns the input light purple
	 * @param input The input
	 * @return The colored output
	 */
	@NotNull
	public static String lightPurple(@NotNull String input) {
		return ansify(LIGHT_PURPLE, input);
	}

	/**
	 * Turns the input light cyan
	 * @param input The input
	 * @return The colored output
	 */
	@NotNull
	public static String lightCyan(@NotNull String input) {
		return ansify(LIGHT_CYAN, input);
	}

	/**
	 * Turns the input white
	 * @param input The input
	 * @return The colored output
	 */
	@NotNull
	public static String white(@NotNull String input) {
		return ansify(WHITE, input);
	}

	/**
	 * Gets the ANSI reset code
	 * @return The reset code
	 */
	public static String reset() {
		if (ansiEnabled) {
			return Ansi.RESET;
		}

		return "";
	}

	/**
	 * Turns the input black
	 * @return The colored output
	 */
	@NotNull
	public static String black() {
		return BLACK;
	}

	/**
	 * Turns the input red
	 * @return The colored output
	 */
	@NotNull
	public static String red() {
		return RED;
	}

	/**
	 * Turns the input green
	 * @return The colored output
	 */
	@NotNull
	public static String green() {
		return GREEN;
	}

	/**
	 * Turns the input brown
	 * @return The colored output
	 */
	@NotNull
	public static String brown() {
		return BROWN;
	}

	/**
	 * Turns the input blue
	 * @return The colored output
	 */
	@NotNull
	public static String blue() {
		return BLUE;
	}

	/**
	 * Turns the input purple
	 * @return The colored output
	 */
	@NotNull
	public static String purple() {
		return PURPLE;
	}

	/**
	 * Turns the input cyan
	 * @return The colored output
	 */
	@NotNull
	public static String cyan() {
		return CYAN;
	}

	/**
	 * Turns the input light gray
	 * @return The colored output
	 */
	@NotNull
	public static String lightGray() {
		return LIGHT_GRAY;
	}

	/**
	 * Turns the input dark gray
	 * @return The colored output
	 */
	@NotNull
	public static String darkGray() {
		return DARK_GRAY;
	}

	/**
	 * Turns the input light red
	 * @return The colored output
	 */
	@NotNull
	public static String lightRed() {
		return LIGHT_RED;
	}

	/**
	 * Turns the input light green
	 * @return The colored output
	 */
	@NotNull
	public static String lightGreen() {
		return LIGHT_GREEN;
	}

	/**
	 * Turns the input yellow
	 * @return The colored output
	 */
	@NotNull
	public static String yellow() {
		return YELLOW;
	}

	/**
	 * Turns the input light blue
	 * @return The colored output
	 */
	@NotNull
	public static String lightBlue() {
		return LIGHT_BLUE;
	}

	/**
	 * Turns the input light purple
	 * @return The colored output
	 */
	@NotNull
	public static String lightPurple() {
		return LIGHT_PURPLE;
	}

	/**
	 * Turns the input light cyan
	 * @return The colored output
	 */
	@NotNull
	public static String lightCyan() {
		return LIGHT_CYAN;
	}

	/**
	 * Determines whether or not Ansi support is enabled
	 * @return {@code true} if ANSI is enabled, {@code false} otherwise
	 */
	public static boolean isAnsiEnabled() {
		return ansiEnabled;
	}

	/**
	 * Turns the input white
	 * @return The colored output
	 */
	@NotNull
	public static String white() {
		return WHITE;
	}

	private static String ansify(@NotNull String color, @NotNull String input) {
		if (ansiEnabled) {
			return color + input + RESET;
		}

		return input;
	}
}

