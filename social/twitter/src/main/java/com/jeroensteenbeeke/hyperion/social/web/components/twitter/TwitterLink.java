package com.jeroensteenbeeke.hyperion.social.web.components.twitter;

import com.jeroensteenbeeke.hyperion.social.beans.twitter.TwitterHandler;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.request.flow.RedirectToUrlException;
import twitter4j.OAuthAuthorization;
import twitter4j.RequestToken;
import twitter4j.TwitterException;

import jakarta.inject.Inject;

/**
 * Wicket Link for initiating a Twitter login
 */
public class TwitterLink extends Link<Void> {

	private static final long serialVersionUID = 1L;

	@Inject
	private TwitterHandler twitterHandler;

	/**
	 * Constructor
	 *
	 * @param id The wicket:id
	 */
	public TwitterLink(String id) {
		super(id);
	}

	@Override
	public void onClick() {
		OAuthAuthorization auth = twitterHandler.getOAuthAuthorization();

		try {
			RequestToken requestToken = auth
					.getOAuthRequestToken(twitterHandler.getCallbackUrl());

			twitterHandler.setRequestToken(requestToken);

			throw new RedirectToUrlException(
					requestToken.getAuthorizationURL());
		} catch (TwitterException e) {
			twitterHandler.handleError(e);
		}
	}

}
