package com.jeroensteenbeeke.hyperion.social;

import com.jeroensteenbeeke.hyperion.social.beans.twitter.TwitterHandler;
import com.jeroensteenbeeke.hyperion.social.web.pages.twitter.TwitterCallbackPage;
import com.jeroensteenbeeke.hyperion.solstice.spring.ApplicationContextProvider;
import org.apache.wicket.protocol.http.WebApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

/**
 * Initializer class for Twitter
 */
public enum Twitter implements SocialMediumInitializer {
	/**
	 * Singleton instance
	 */
	integration;

	private static final Logger log = LoggerFactory.getLogger(Twitter.class);

	
	@Override
	public <T extends WebApplication & ApplicationContextProvider> void initialize(
			T application, String prefix) {
		log.info("Initializing Twitter login support");
		
		ApplicationContext context = application.getApplicationContext();
		TwitterHandler twitterHandler = context.getBean(TwitterHandler.class);

		var twitterBuilder = twitter4j.Twitter.newBuilder()
						.oAuthConsumer(twitterHandler.getConsumerKey(), twitterHandler.getConsumerSecret());
		
		twitterHandler.setTwitterBuilder(twitterBuilder);
		twitterHandler.setPathPrefix(prefix);
		
		String contextRelativeUrl = prefix.concat("/twitter/callback");
		application.mountPage(contextRelativeUrl, TwitterCallbackPage.class);
		
		log.info("Mounted Twitter callback at {}", contextRelativeUrl);
	}
}
