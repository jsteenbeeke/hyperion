package com.jeroensteenbeeke.hyperion.social.beans.twitter;

import io.vavr.control.Option;
import org.jetbrains.annotations.Nullable;
import twitter4j.*;

import org.jetbrains.annotations.NotNull;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Controller base implementation for Twitter functionality. Implementing applications are required to
 * provide an implementation of this class for complete Twitter functionality
 */
public abstract class TwitterHandler {
	private Twitter.TwitterBuilder twitterBuilder;
	
	private String pathPrefix = "";

	/**
	 * Sets the factory for creating Twitter objects
	 * @param twitterBuilder The factory
	 */
	public void setTwitterBuilder(@NotNull Twitter.TwitterBuilder twitterBuilder) {
		this.twitterBuilder = twitterBuilder;
	}

	/**
	 * Returns an instance of the Twitter class, which is a facade for the
	 * Twitter API
	 * @return A Twitter object
	 */
	public Twitter getTwitter() {
		return twitterBuilder.build();
	}

	/**
	 * Returns an instance of the Twitter class, with the given access token provider.
	 * @param provider The provider
	 * @return A Twitter object
	 */
	public Twitter getTwitter(@NotNull IAccessTokenProvider provider) {
		return twitterBuilder.oAuthAccessToken(provider.get()).build();
	}

	/**
	 * Returns the request token for the current session. Implementing classes
	 * should also provide the logic for determining the current session
	 *
	 * @return The request token
	 */
	@NotNull
	public abstract Option<RequestToken> getRequestToken();

	/**
	 * Sets the request token for the current session. Implementing classes should provide logic
	 * for per-session storage.
	 * @param token The request otken
	 */
	public abstract void setRequestToken(@Nullable RequestToken token);

	/**
	 * Callback method for actions to take upon acquiring an AccessToken
	 * @param token The token that was acquired
	 */
	public abstract void onAccessTokenAcquired(@NotNull AccessToken token);

	/**
	 * Callback method for actions to take upon encountering an error
	 * @param message The error message(s) encountered
	 */
	public abstract void onError(@NotNull String message);

	/**
	 * Returns the Twitter API consumer key
	 * @return The key
	 */
	@NotNull
	public abstract String getConsumerKey();

	/**
	 * Returns the Twitter API consumer secret
	 * @return The secret
	 */
	@NotNull
	public abstract String getConsumerSecret();

	/**
	 * Returns the OAuth callback URL for the current application
	 * @return The callback URL
	 */
	@NotNull
	public abstract String getCallbackUrl();

	/**
	 * Method called upon encountering a Twitter exception
	 * @param e The exception
	 */
	public final void handleError(@NotNull TwitterException e) {
		Stream.Builder<String> msg = Stream.builder();
		msg.add(String.format("[%d]", e.getAccessLevel()));
		msg.add(e.getErrorMessage());
		msg.add(e.getExceptionCode());
		msg.add(e.getMessage());
		
		Throwable ex = e;
		while (ex.getCause() != null) {
			msg.add(ex.getMessage());
			
			ex = ex.getCause();
		}
			
		onError(msg.build().filter(Objects::nonNull).collect(Collectors.joining(" ")));
	}

	/**
	 * Convenience method for constructing a callback URL from a base URL. Useful when also
	 * using the Wicket logic
	 * @param baseUrl The base URL of the application
	 * @return A complete callback URL
	 */
	public final String buildCallbackUrl(String baseUrl) {
		String prefix = baseUrl.endsWith("/") ? baseUrl : baseUrl.concat("/");
		
		return prefix.concat(pathPrefix).concat("twitter/callback");
	}

	/**
	 * Sets the current path prefix, a context-specific prefix that should always
	 * be prefixed to relative URLs
	 * @param prefix The prefix
	 */
	public final void setPathPrefix(String prefix) {
		String sanitized = prefix;
		
		if (!sanitized.isEmpty() && !sanitized.endsWith("/")) {
			sanitized = sanitized.concat("/");
		}
		while (sanitized.startsWith("/")) {
			sanitized = sanitized.substring(1);
		}
		
		this.pathPrefix = sanitized;
		
	}

	/**
	 * Create an OAuth authorization object
	 * @return The authorization
	 */
	public OAuthAuthorization getOAuthAuthorization() {
		return OAuthAuthorization.newBuilder()
				.oAuthConsumer(getConsumerKey(), getConsumerSecret()).build();
	}
}
