package com.jeroensteenbeeke.hyperion.social.beans.youtube;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import org.danekja.java.util.function.serializable.SerializableFunction;

/**
 * Provider that determines YouTube credentials based on Google client secrets
 */
public interface IYouTubeCredentialProvider extends SerializableFunction<GoogleClientSecrets,Credential> {

}
