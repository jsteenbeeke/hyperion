package com.jeroensteenbeeke.hyperion.social.web.components.linkedin;

import com.github.scribejava.core.oauth.OAuth20Service;
import com.jeroensteenbeeke.hyperion.social.beans.linkedin.LinkedInHandler;
import com.jeroensteenbeeke.hyperion.util.Randomizer;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.request.flow.RedirectToUrlException;

import jakarta.inject.Inject;
import java.io.IOException;

/**
 * Link for starting a login flow with LinkedIn
 */
public class LinkedInLink extends Link<Void> {

	private static final long serialVersionUID = 1L;

	@Inject
	private LinkedInHandler linkedInHandler;

	/**
	 * Constructor
	 * @param id The wicket:id of the link
	 */
	public LinkedInLink(String id) {
		super(id);
	}

	@Override
	public void onClick() {
		String state = Randomizer.random(20);
		
		linkedInHandler.setUserState(state);
		 
		try (OAuth20Service service = linkedInHandler.createService()) {
			throw new RedirectToUrlException(service.getAuthorizationUrl(state));
		} catch (IOException e) {
			linkedInHandler.onError(e.getMessage());
		}
	}

}
