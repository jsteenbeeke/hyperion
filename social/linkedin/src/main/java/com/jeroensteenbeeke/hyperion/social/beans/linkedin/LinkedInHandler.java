package com.jeroensteenbeeke.hyperion.social.beans.linkedin;

import com.github.scribejava.apis.LinkedInApi20;
import com.github.scribejava.core.builder.ScopeBuilder;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.oauth.OAuth20Service;
import io.vavr.control.Option;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Handler class for LinkedIn integration. Applications that use LinkedIn integration
 * are required to implement this class
 */
public abstract class LinkedInHandler {
	private String urlPrefix;

	/**
	 * Returns the OAuth client ID of the LinkedIn application
	 * @return The client ID
	 */
	@NotNull
	public abstract String getClientId();

	/**
	 * Returns the OAuth client secret of the LinkedIn application
	 * @return The client secret
	 */
	@NotNull
	public abstract String getClientSecret();

	/**
	 * Returns the base URL of the application
	 * @return The base URL
	 */
	@NotNull
	public abstract String getApplicationBaseUrl();

	/**
	 * Callback method for errors
	 * @param message The error that just occurred
	 */
	public abstract void onError(@NotNull String message);

	/**
	 * Returns the URL prefix as set by the integration
	 * @return the url prefix
	 */
	public String getUrlPrefix() {
		return urlPrefix;
	}

	/**
	 * Sets the URL prefix required for the integration
	 * @param urlPrefix the url prefix
	 */
	public void setUrlPrefix(String urlPrefix) {
		this.urlPrefix = urlPrefix.startsWith("/") ? urlPrefix : "/".concat(urlPrefix);
	}

	/**
	 * Returns the required OAuth scopes (space separated). Defaults to {@code r_basicprofile}
	 * @return The scopes
	 */
	@NotNull
	public List<String> getScopes() {
		return List.of("r_basicprofile");
	}

	/**
	 * Creates a new scribe service for communicating with LinkedIn
	 * @return The service object
	 */
	@NotNull
	public final OAuth20Service createService() {
		return new ServiceBuilder(getClientId())
				.apiSecret(getClientSecret())
				.defaultScope(new ScopeBuilder(getScopes()))
				.callback(
						String.format("%s%s/linkedin/callback",
								getApplicationBaseUrl(), getUrlPrefix()))
				.build(LinkedInApi20.instance());
	}

	/**
	 * Callback method for when an access token has been acquired
	 * @param service The service to use for follow-up requests
	 * @param accessToken The access token to use for these requests
	 */
	public abstract void onAccessTokenReceived(@NotNull OAuth20Service service, @NotNull OAuth2AccessToken accessToken);

	/**
	 * Returns the user state (the randomized state String provided to prevent replay attacks).
	 * This should be session/user-specific. Responsibility for making the data session/user-specific
	 * lies with the implementor.
	 *
	 * @return Optionally the state
	 */
	@NotNull
	public abstract Option<String> getUserState();

	/**
	 * Sets the user state for the current user. This should be session/user-spcific. Responsibility for making the data session/user-specific
	 * lies with the implementor.
	 * @param state The state to set
	 */
	public abstract void setUserState(@NotNull String state);

}
