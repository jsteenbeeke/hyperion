package com.jeroensteenbeeke.hyperion.social.api.webhook.blockkit;

import io.vavr.collection.Array;

import org.jetbrains.annotations.NotNull;
import jakarta.json.Json;
import jakarta.json.JsonObjectBuilder;

/**
 * Section that consists of fields
 */
public class FieldsSection implements Block {
	private final Array<TextObject> fields;

	private FieldsSection(@NotNull Array<TextObject> fields) {
		this.fields = fields;
	}

	/**
	 * Creates a new section with the added field
	 *
	 * @param field The field to add
	 * @return A new section, or the current section if there are already 10 fields
	 */
	public FieldsSection withField(@NotNull TextObject field) {
		if (fields.size() == 10) {
			return this;
		}

		return new FieldsSection(fields.append(field));
	}

	/**
	 * Adds an accessory to the section, yielding a new block
	 *
	 * @param element The accessory to add
	 * @return A block with the added accessory
	 */
	public Block withAccessory(@NotNull BlockElement element) {
		return () -> FieldsSection.this
				.toJsonObjectBuilder()
				.add("accessory", element.toJsonObjectBuilder());
	}

	@Override
	public JsonObjectBuilder toJsonObjectBuilder() {
		JsonObjectBuilder objectBuilder = Json.createObjectBuilder()
											  .add("type", "section");

		if (!fields.isEmpty()) {
			objectBuilder
					.add("fields", fields.foldLeft(Json.createArrayBuilder(), (array, field) -> array.add(field.toJsonObjectBuilder())));
		}

		return objectBuilder;
	}

	/**
	 * Creates a new section that has no text header and instead uses fields
	 *
	 * @param firstField The first field
	 * @return A section
	 */
	public static FieldsSection create(@NotNull TextObject firstField) {
		return new FieldsSection(Array.of(firstField));
	}
}
