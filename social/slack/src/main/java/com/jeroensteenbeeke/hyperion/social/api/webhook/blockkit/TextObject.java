package com.jeroensteenbeeke.hyperion.social.api.webhook.blockkit;

/**
 * Text object that can be included in various blocks
 */
public interface TextObject extends JsonAble {

}
