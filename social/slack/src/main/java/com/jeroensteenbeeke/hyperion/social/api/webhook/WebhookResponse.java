package com.jeroensteenbeeke.hyperion.social.api.webhook;

/**
 * Response object for webhook invocations
 */
public sealed interface WebhookResponse permits WebhookSuccess, WebhookFailure {
}
