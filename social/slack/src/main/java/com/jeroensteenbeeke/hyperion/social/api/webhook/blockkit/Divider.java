package com.jeroensteenbeeke.hyperion.social.api.webhook.blockkit;

import jakarta.json.Json;
import jakarta.json.JsonObjectBuilder;

/**
 * Divider
 */
public class Divider implements Block {
	@Override
	public JsonObjectBuilder toJsonObjectBuilder() {
		return Json.createObjectBuilder().add("type", "divider");
	}
}
