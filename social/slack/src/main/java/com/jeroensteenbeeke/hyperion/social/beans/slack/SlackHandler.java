package com.jeroensteenbeeke.hyperion.social.beans.slack;

import com.github.scribejava.apis.SlackApi;
import com.github.scribejava.core.builder.ScopeBuilder;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.jeroensteenbeeke.lux.TypedResult;
import io.vavr.control.Option;
import org.jetbrains.annotations.NotNull;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Handler class for Slack OAuth2 functionality. Applications that use the Slack functionality need to provide
 * an instance of this class for the integration to work
 */
public abstract class SlackHandler {
	private static final int STATUS_OK = 200;

	private static final String IDENTITY_URL = "https://slack.com/api/users.identity";

	private String urlPrefix;

	/**
	 * Returns the Client ID of the application
	 *
	 * @return The client ID
	 */
	@NotNull
	public abstract String getClientId();

	/**
	 * Returns the Client Secret of the application
	 *
	 * @return The client secret
	 */
	@NotNull
	public abstract String getClientSecret();

	/**
	 * Returns the base URL of the application
	 *
	 * @return The application base URL
	 */
	@NotNull
	public abstract String getApplicationBaseUrl();

	/**
	 * Callback for when an error occurs
	 *
	 * @param message The error message
	 */
	public abstract void onError(@NotNull String message);

	/**
	 * Returns the URL prefix. An absolute path to prefix to all URLs pertaining to callbacks
	 *
	 * @return the URL prefix
	 */
	public String getUrlPrefix() {
		return urlPrefix;
	}

	/**
	 * Sets the URL prefix
	 *
	 * @param urlPrefix The URL prefix
	 */
	public void setUrlPrefix(String urlPrefix) {
		this.urlPrefix = urlPrefix.startsWith("/") ? urlPrefix : "/".concat(urlPrefix);
	}

	/**
	 * Returns a list of required OAuth2 scopes (space-separated)
	 *
	 * @return The scopes
	 */
	@NotNull
	public List<String> getScopes() {
		return List.of("identity.basic");
	}

	/**
	 * Creates the OAuth2Service to use with Scribe
	 *
	 * @return A service
	 */
	@NotNull
	public OAuth20Service createService() {
		String callbackUrl = String.format("%s%s/slack/callback",
				getApplicationBaseUrl(), getUrlPrefix());

		while (callbackUrl.contains("//slack")) {
			callbackUrl = callbackUrl.replace("//slack", "/slack");
		}

		SlackApi api = getSlackAPI();

		return new ServiceBuilder(getClientId())
				.apiSecret(getClientSecret())
				.defaultScope(new ScopeBuilder(getScopes()))
				.callback(
						callbackUrl)
				.build(api);
	}

	/**
	 * Returns an instance of the SlackAPI
	 *
	 * @return A SlackAPI object
	 */
	protected SlackApi getSlackAPI() {
		return SlackApi.instance();
	}

	/**
	 * Returns the URL to use for fetching the user's identity after login
	 *
	 * @return The identity URL
	 */
	protected String getIdentityUrl() {
		return IDENTITY_URL;
	}

	/**
	 * Try to obtain information about the current user from Slack
	 *
	 * @param service     The service to use
	 * @param accessToken The user's access token
	 * @return A TypedResult containing either the user's info as a JSON object, or an error message stating why the operation failed
	 */
	public TypedResult<JSONObject> getUserInfo(
			@NotNull OAuth20Service service, @NotNull OAuth2AccessToken accessToken) {
		final OAuthRequest request = new OAuthRequest(Verb.GET, getIdentityUrl());
		service.signRequest(accessToken, request);

		final Response response;
		try {
			response = service.execute(request);

			if (response.getCode() != STATUS_OK) {
				return TypedResult.fail("Could not request profile information");
			} else {
				JSONParser parser = new JSONParser();
				try {

					JSONObject profile = (JSONObject) parser.parse(response
							.getBody());

					return TypedResult.ok(profile);
				} catch (ParseException | IOException e) {
					return TypedResult.fail("Could not parse profile information: ".concat(e
							.getMessage()));
				}
			}
		} catch (InterruptedException | ExecutionException | IOException e) {
			return TypedResult.fail("Could not request profile information (%s): %s", e.getClass().getSimpleName(),
					e.getMessage());
		}


	}

	/**
	 * Method that is called when an access token has been successfully received
	 *
	 * @param service     The service to use for subsequent requests
	 * @param accessToken The accessToken
	 */
	public abstract void onAccessTokenReceived(@NotNull OAuth20Service service, @NotNull OAuth2AccessToken accessToken);

	/**
	 * Returns the user/session-specific state for the current OAuth2 flow. Used to prevent replay attacks. Logic
	 * for making it user/session specific is the responsibility of the implementing class.
	 *
	 * @return Optionally the state.
	 */
	@NotNull
	public abstract Option<String> getUserState();

	/**
	 * Sets the user/session-specific state for the current OAuth2 flow. Used to prevent replay attacks. Logic for making it
	 * user/session specific is the responsibility of the implementing class.
	 *
	 * @param state The state to set. This should be a unique non-guessable randomly generated String
	 */
	public abstract void setUserState(@NotNull String state);

}
