package com.jeroensteenbeeke.hyperion.social.api.webhook.blockkit;

import com.jeroensteenbeeke.hyperion.annotation.Buildable;

import org.jetbrains.annotations.NotNull;
import jakarta.json.Json;
import jakarta.json.JsonObjectBuilder;

/**
 * Image that is included as part of another element
 */
public class ImageElement implements BlockElement {
	private final String url;

	private final String alt;

	/**
	 * Constructor
	 * @param url The image URL
	 * @param alt The alt text
	 */
	@Buildable
	ImageElement(@NotNull String url, @NotNull String alt) {
		this.url = url;
		this.alt = alt;
	}

	@Override
	public JsonObjectBuilder toJsonObjectBuilder() {
		return Json.createObjectBuilder()
				.add("type", "image")
				.add("image_url", url)
				.add("alt_text", alt);
	}
}
