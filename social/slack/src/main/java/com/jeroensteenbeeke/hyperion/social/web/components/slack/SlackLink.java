package com.jeroensteenbeeke.hyperion.social.web.components.slack;

import com.github.scribejava.core.oauth.OAuth20Service;
import com.jeroensteenbeeke.hyperion.social.beans.slack.SlackHandler;
import com.jeroensteenbeeke.hyperion.util.Randomizer;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.request.flow.RedirectToUrlException;

import jakarta.inject.Inject;
import java.io.IOException;

/**
 * Link for initiating a Slack OAuth2 login flow
 */
public class SlackLink extends Link<Void> {

	private static final long serialVersionUID = 1L;

	@Inject
	private SlackHandler slackHandler;

	/**
	 * Constructor
	 *
	 * @param id The wicket:id
	 */
	public SlackLink(String id) {
		super(id);
	}

	@Override
	public void onClick() {
		String state = Randomizer.random(20);

		slackHandler.setUserState(state);

		try (OAuth20Service service = slackHandler.createService()) {
			throw new RedirectToUrlException(service.getAuthorizationUrl(state));
		} catch (IOException e) {
			slackHandler.onError(e.getMessage());
		}
	}

}
