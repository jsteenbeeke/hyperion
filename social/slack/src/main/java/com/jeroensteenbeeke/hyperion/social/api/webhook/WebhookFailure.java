package com.jeroensteenbeeke.hyperion.social.api.webhook;

import org.jetbrains.annotations.NotNull;

/**
 * Failure specification for webhook response
 * @param code The HTTP response code
 * @param message The HTTP status line message
 * @param body The body of the response. Can be empty
 */
public record WebhookFailure(int code, @NotNull String message, @NotNull String body) implements WebhookResponse {
}
