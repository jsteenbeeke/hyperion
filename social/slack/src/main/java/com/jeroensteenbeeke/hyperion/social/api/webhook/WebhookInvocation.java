package com.jeroensteenbeeke.hyperion.social.api.webhook;

import io.vavr.control.Try;
import okhttp3.*;

import org.jetbrains.annotations.NotNull;
import java.io.IOException;

/**
 * Helper class for posting data to webhooks
 */
public class WebhookInvocation {
	private static final OkHttpClient client = new OkHttpClient();

	/**
	 * Invoke the given webhook
	 * @param webhookUrl The webhook URL
	 * @return A builder for adding the payload
	 */
	public static WithPayload invoke(@NotNull String webhookUrl) {
		return payload -> {
			Request request = new Request.Builder()
					.post(RequestBody.create(
							payload.toJson(), MediaType.parse("application/json")))
					.url(webhookUrl) //post in designated channel
					.build();


			try (Response response = client.newCall(request).execute()) {
				if (!response.isSuccessful()) {
					ResponseBody body = response
							.body();

					return Try.success(new WebhookFailure(response.code(), response.message(), body != null ? body.string() : ""));
				}

				return Try.success(WebhookSuccess.SUCCESS);
			} catch (IOException e) {
				return Try.failure(e);
			}
		};
	}

	/**
	 * Builder interface for the payload
	 */
	public interface WithPayload {
		/**
		 * Specifies the payload to send with the webhook request
		 * @param payload The payload
		 * @return A Try either containing a response or possible an error
		 */
		Try<WebhookResponse> withPayload(@NotNull WebhookPayload payload);
	}
}
