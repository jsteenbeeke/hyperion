package com.jeroensteenbeeke.hyperion.social.api.webhook.blockkit;

/**
 * A Block element as defined by the Slack API
 */
public interface BlockElement extends JsonAble {
}
