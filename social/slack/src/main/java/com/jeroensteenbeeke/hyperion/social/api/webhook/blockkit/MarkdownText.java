package com.jeroensteenbeeke.hyperion.social.api.webhook.blockkit;

import org.jetbrains.annotations.NotNull;
import jakarta.json.Json;
import jakarta.json.JsonObjectBuilder;

/**
 * Text block that uses markdown (formatted) text
 * @param text The text to format using Markdown
 */
public record MarkdownText(@NotNull String text) implements TextObject {
	@Override
	public JsonObjectBuilder toJsonObjectBuilder() {
		return Json.createObjectBuilder()
				.add("type", "mrkdwn")
				.add("text", text)
				;
	}

	/**
	 * Specifies that the text should be used verbatim: i.e. links should not be automatically converted
	 * @return A TextObject that is parsed verbatim
	 */
	public TextObject verbatim() {
		return () -> MarkdownText.this.toJsonObjectBuilder().add("verbatim", true);
	}
}
