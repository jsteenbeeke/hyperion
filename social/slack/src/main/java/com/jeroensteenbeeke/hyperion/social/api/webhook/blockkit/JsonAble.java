package com.jeroensteenbeeke.hyperion.social.api.webhook.blockkit;

import jakarta.json.JsonObjectBuilder;

/**
 * An element that can be turned to JSON
 */
public interface JsonAble {
	/**
	 * Converts the block to a JSON object
	 * @return The build for the object
	 */
	JsonObjectBuilder toJsonObjectBuilder();
}
