package com.jeroensteenbeeke.hyperion.social.api.webhook.blockkit;

import jakarta.json.Json;
import jakarta.json.JsonObjectBuilder;

/**
 * Section that displays a line of text
 */
public class TextSection implements Block {
	private final TextObject text;

	/**
	 * Constructor
	 * @param text The text to display
	 */
	public TextSection(TextObject text) {
		this.text = text;
	}

	@Override
	public JsonObjectBuilder toJsonObjectBuilder() {
		return Json.createObjectBuilder()
				   .add("type", "section")
				   .add("text", text.toJsonObjectBuilder());
	}
}
