package com.jeroensteenbeeke.hyperion.social.api.webhook;

import com.jeroensteenbeeke.hyperion.social.api.webhook.blockkit.Block;
import io.vavr.collection.Array;

import org.jetbrains.annotations.NotNull;
import jakarta.json.Json;
import jakarta.json.JsonObjectBuilder;

/**
 * Payload for a Webhook call
 */
public class WebhookPayload {
	private final String text;

	private final Array<Block> blocks;

	/**
	 * Constructor
	 *
	 * @param text The main text of the message, can contain markdown formatting
	 */
	public WebhookPayload(@NotNull String text) {
		this(text, Array.empty());
	}

	private WebhookPayload(@NotNull String text, @NotNull Array<Block> blocks) {
		this.text = text;
		this.blocks = blocks;
	}

	/**
	 * Adds a block to the payload
	 *
	 * @param block The block to add
	 * @return A new payload with the added block
	 */
	public WebhookPayload withBlock(@NotNull Block block) {
		return new WebhookPayload(text, blocks.append(block));
	}

	/**
	 * Converts the payload to an actual JSON string
	 * @return The JSON representation of the payload
	 */
	public String toJson() {
		JsonObjectBuilder builder = Json.createObjectBuilder();

		builder.add("text", text);
		if (!blocks.isEmpty()) {
			builder.add("blocks",
						blocks.foldLeft(Json.createArrayBuilder(),
										(array, block) -> array.add(block.toJsonObjectBuilder())));
		}

		return builder.build().toString();
	}


}
