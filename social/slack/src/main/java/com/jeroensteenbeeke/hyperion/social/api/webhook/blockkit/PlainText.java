package com.jeroensteenbeeke.hyperion.social.api.webhook.blockkit;

import org.jetbrains.annotations.NotNull;
import jakarta.json.Json;
import jakarta.json.JsonObjectBuilder;

/**
 * A plaintext text block, without fancy frills
 * @param text The text to display
 */
public record PlainText(@NotNull String text) implements TextObject {
	@Override
	public JsonObjectBuilder toJsonObjectBuilder() {
		return Json.createObjectBuilder()
				   .add("type", "plain_text")
				   .add("text", text);
	}

	/**
	 * Indicates whether emoji expressions (such as {@code :smile:}) should be interpreted. The default
	 * is false
	 * @return A TextObject with emoji's enabled
	 */
	public TextObject withEmoji() {
		return () -> PlainText.this.toJsonObjectBuilder().add("emoji", true);
	}
}
