package com.jeroensteenbeeke.hyperion.social.api.webhook;

/**
 * Success object for webhook responses. Singleton
 */
public enum WebhookSuccess implements WebhookResponse{
	/**
	 * Singleton instance
	 */
	SUCCESS
}
