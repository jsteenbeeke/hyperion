docker pull registry.jeroensteenbeeke.nl/maven:latest
docker run -w /work -v $(pwd):/work registry.jeroensteenbeeke.nl/maven:latest mvn clean verify package $@
