mvn clean install -pl .,\
recipes,\
recipes/core,\
recipes/hibernate,\
recipes/rest-api,\
recipes/spring,\
recipes/wicket,\
recipes/wicket-standalone,\
recipes/wicket-with-rest
