package com.jeroensteenbeeke.hyperion.logging.api;

import io.vavr.control.Option;

import org.jetbrains.annotations.NotNull;
import java.util.UUID;

/**
 * Extra logging metadata
 */
public class HyperionLoggingScope {
	private static final ThreadLocal<String> SCOPE = new ThreadLocal<>();

	private static final ThreadLocal<String> REQUEST = new ThreadLocal<>();

	/**
	 * Sets the current scope to the given String. Overwrites any previously set scope
	 * @param scope The scope to set
	 */
	public static void openScope(@NotNull String scope) {
		SCOPE.set(scope);
	}

	/**
	 * Removes the current scope
	 */
	public static void closeScope() {
		SCOPE.remove();
	}

	/**
	 * Registers the current request, giving it a UUID
	 */
	public static void startRequest() {
		REQUEST.set(UUID.randomUUID().toString());
	}

	/**
	 * Ends the current request, removing the UUID
	 */
	public static void endRequest() {
		REQUEST.remove();
	}

	/**
	 * Returns the current scope, if one is registered
	 * @return An Option that may contain a scope
	 */
	public static Option<String> currentScope() {
		return Option.of(SCOPE.get());
	}

	/**
	 * Returns the current request, if one is registered
	 * @return An Option that may contain a request UUID
	 */
	public static Option<String> currentRequest() {
		return Option.of(REQUEST.get());
	}
}
