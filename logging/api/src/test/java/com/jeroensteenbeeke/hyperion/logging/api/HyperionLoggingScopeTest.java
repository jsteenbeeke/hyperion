package com.jeroensteenbeeke.hyperion.logging.api;

import io.vavr.control.Option;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

public class HyperionLoggingScopeTest {
	@Test
	public void testRequest() {
		assertThat(HyperionLoggingScope.currentRequest(), equalTo(Option.none()));
		HyperionLoggingScope.startRequest();
		assertThat(HyperionLoggingScope.currentRequest(), not(equalTo(Option.none())));
		HyperionLoggingScope.endRequest();
		assertThat(HyperionLoggingScope.currentRequest(), equalTo(Option.none()));
	}

	@Test
	public void testScope() {
		assertThat(HyperionLoggingScope.currentRequest(), equalTo(Option.none()));
		HyperionLoggingScope.openScope("Test");
		assertThat(HyperionLoggingScope.currentRequest(), not(equalTo(Option.some("Test"))));
		HyperionLoggingScope.openScope("Overwritten");
		assertThat(HyperionLoggingScope.currentRequest(), not(equalTo(Option.some("Overwritten"))));
		HyperionLoggingScope.closeScope();
		assertThat(HyperionLoggingScope.currentRequest(), equalTo(Option.none()));
	}
}
