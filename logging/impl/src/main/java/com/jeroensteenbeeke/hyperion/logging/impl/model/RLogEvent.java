package com.jeroensteenbeeke.hyperion.logging.impl.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A single log "line", i.e. something that happens in an application
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RLogEvent implements Serializable {
	private static final long serialVersionUID = 7969577261672635053L;

	private ZonedDateTime timestamp;

	private String channel;

	private String source;

	private LogLevel level;

	private String message;

	private String thread;

	private String scope;

	private String request;

	private ExceptionDescriptor exception;

	/**
	 * Non-args constructor for reflective construction
	 */
	protected RLogEvent() {
	}

	/**
	 * Constructor
	 * @param timestamp The time at which the log event occurred
	 * @param channel The channel in which the log event should be categorized
	 * @param thread The thread logging the item
	 * @param source The source of the log (which class/logger)
	 * @param level The log level (info, warn, error, etc)
	 * @param message The log message
	 * @param scope The scope of the message (optional)
	 * @param request The request tied to the message (optional)
	 * @param exception Optionally, the exception that occurred
	 */
	public RLogEvent(@NotNull ZonedDateTime timestamp, @NotNull String channel,
					 @NotNull String thread,
					 @NotNull String source,
					 @NotNull LogLevel level,
					 @NotNull String message,
					 @Nullable String scope,
					 @Nullable String request,
					 @Nullable ExceptionDescriptor exception) {
		this.timestamp = timestamp;
		this.channel = channel;
		this.source = source;
		this.thread = thread;
		this.level = level;
		this.message = message;
		this.scope = scope;
		this.request = request;
		this.exception = exception;
	}

	/**
	 * The name of the logger producing the log message
	 * @return The source
	 */
	@NotNull
	public String getSource() {
		return source;
	}

	/**
	 * Sets the name of the logger producing the log message
	 * @param source The source
	 */
	public void setSource(@NotNull String source) {
		this.source = source;
	}

	/**
	 * The thread executing the logging
	 * @return The thread
	 */
	@NotNull
	public String getThread() {
		return thread;
	}

	/**
	 * Sets the thread executing the logging
	 * @param thread The thread
	 */
	public void setThread(@NotNull String thread) {
		this.thread = thread;
	}

	/**
	 * The time at which the log event occurred
	 * @return The timestamp
	 */
	@NotNull
	public ZonedDateTime getTimestamp() {
		return timestamp;
	}

	/**
	 * Sets the time at which the log event occurred
	 * @param timestamp The timestamp
	 */
	public void setTimestamp(@NotNull ZonedDateTime timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * The log level (info, warn, error, etc)
	 * @return The log level
	 */
	@NotNull
	public LogLevel getLevel() {
		return level;
	}

	/**
	 * Set the log level
	 * @param level The log level
	 */
	public void setLevel(@NotNull LogLevel level) {
		this.level = level;
	}

	/**
	 * The channel in which the log event is categorized
	 * @return The channel
	 */
	@NotNull
	public String getChannel() {
		return channel;
	}

	/**
	 * Sets the channel in which the log event is categorized
	 * @param channel The channel
	 */
	public void setChannel(@NotNull String channel) {
		this.channel = channel;
	}

	/**
	 * The log message
	 * @return A message
	 */
	@NotNull
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the log message
	 * @param message A message
	 */
	public void setMessage(@NotNull String message) {
		this.message = message;
	}

	/**
	 * The exception which triggered the log event
	 * @return An exception
	 */
	@Nullable
	public ExceptionDescriptor getException() {
		return exception;
	}

	/**
	 * Sets the exception which triggered the log event
	 * @param exception The exception
	 */
	public void setException(@Nullable ExceptionDescriptor exception) {
		this.exception = exception;
	}

	/**
	 * Returns the scope of the log event, user-defined and optional
	 * @return The scope
	 */
	public String getScope() {
		return scope;
	}

	/**
	 * Sets the scope of the log event
	 * @param scope The scope
	 */
	public void setScope(String scope) {
		this.scope = scope;
	}

	/**
	 * Gets the request for which the log event was made. Usually auto-set by filter
	 * @return The request
	 */
	public String getRequest() {
		return request;
	}

	/**
	 * Sets the log event's request. Generally speaking you don't need to call this method, as it
	 * should be set at constructor level and normally does not change
	 * @param request The request
	 */
	public void setRequest(String request) {
		this.request = request;
	}
}
