package com.jeroensteenbeeke.hyperion.logging.impl.cfg;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlValue;

/**
 * A channel definition, defines a pattern indicating a logger belongs to a certain channel. Useful for grouping
 */
@XmlRootElement(name = "channel")
public class ChannelDefinition {
	private String pattern;

	private String channel;

	/**
	 * The pattern to match. An asterix is a catch-all wildcard
	 * @return The pattern
	 */
	@XmlAttribute
	public String getPattern() {
		return pattern;
	}

	/**
	 * Sets the pattern
	 * @param pattern The pattern
	 */
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	/**
	 * The channel this pattern should match
	 * @return The channel
	 */
	@XmlValue
	public String getChannel() {
		return channel;
	}

	/**
	 * Sets the channel
	 * @param channel The channel
	 */
	public void setChannel(String channel) {
		this.channel = channel;
	}
	
}
