package com.jeroensteenbeeke.hyperion.logging.impl.model;

import org.jetbrains.annotations.NotNull;

/**
 * The logging level
 */
public enum LogLevel {
	TRACE(1, 1),
	DEBUG(1, 1),
	INFO(1, 2),
	WARN(1, 2),
	ERROR(1, 1);

	private final int leadingSpaces;

	private final int trailingSpaces;

	/**
	 * Creates a new log level
	 * @param leadingSpaces The number of leading spaces in the log representation
	 * @param trailingSpaces The number of trailing spaces in the log representation
	 */
	LogLevel(int leadingSpaces, int trailingSpaces) {
		this.leadingSpaces = leadingSpaces;
		this.trailingSpaces = trailingSpaces;
	}

	/**
	 * Returns a fixed-width representation of the current log level
	 * @return A representation
	 */
	@NotNull
	public String getLogRepresentation() {
		return " ".repeat(leadingSpaces) + name() + " ".repeat(trailingSpaces);
	}
}
