package com.jeroensteenbeeke.hyperion.logging.impl;

class IncrementingPrefixFormat {
	private int max;

	private String format;

	public IncrementingPrefixFormat(int defaultMax) {
		this.format = "%1$"+ defaultMax +"s";
		this.max = defaultMax;
	}

	public void observeLength(int length) {
		synchronized (this) {
			if (length > max) {
				max = length;
				format = "%1$"+ length +"s";
			}
		}
	}

	public String getFormat() {
		synchronized (this) {
			return format;
		}
	}
}
