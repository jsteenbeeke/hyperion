package com.jeroensteenbeeke.hyperion.logging.impl;

import com.jeroensteenbeeke.hyperion.logging.impl.model.LogLevel;
import io.vavr.Tuple2;
import org.slf4j.Logger;
import org.slf4j.Marker;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.jeroensteenbeeke.hyperion.color.Ansi.*;

/**
 * Logger implementation that outputs to stdout and maintains a queue of events
 * to be sent to remote storage
 */
public class HyperionLoggerImpl implements Logger {
	private static final long serialVersionUID = -5976528145200868343L;

	private static final DateTimeFormatter TIME_FORMATTER = new DateTimeFormatterBuilder()
			.parseCaseInsensitive().append(DateTimeFormatter.ISO_LOCAL_TIME)
			.toFormatter();

	private static final DateTimeFormatter OFFSET_FORMATTER = new DateTimeFormatterBuilder()
			.parseCaseInsensitive().parseLenient().appendOffsetId()
			.parseStrict().toFormatter();

	private static final IncrementingPrefixFormat loggerNameLength = new IncrementingPrefixFormat(46); // Default based on LocalContainerEntityManagerFactoryBean

	private static final IncrementingPrefixFormat threadLength = new IncrementingPrefixFormat(31); // Default determined based on Quartz thread names


	private final String channel;

	private final LogLevel minimumLogLevel;

	private final String name;

	private final String channelFormat;

	HyperionLoggerImpl(@NotNull String name, @NotNull HyperionLoggerConfig config) {
		this.name = name;
		this.channel = config.findChannel(name).orElse(null);
		this.minimumLogLevel = config.findMinimumLogLevel(name).orElse(null);
		this.channelFormat = "%1$" + config.getMaxChannelLength() + "s";
	}


	@Override
	public String getName() {
		return name;
	}


	static String shorten(String name) {
		Tuple2<String, String> shortened = abbreviatePrefix(name, "\\.", s -> s.substring(0, 1), ".");

		String pre = shortened._1;

		if (pre.isBlank()) {
			if (name.contains("$")) {
				Tuple2<String, String> preDollarSignStrip = abbreviatePrefix(name, "\\$", HyperionLoggerImpl::stripLowercase, "$");

				return preDollarSignStrip._1() + "$" + preDollarSignStrip._2();
			}

			return name;
		}

		if (shortened._2().contains("$")) {
			Tuple2<String, String> preDollarSignStrip = abbreviatePrefix(shortened._2(), "\\$", HyperionLoggerImpl::stripLowercase, "$");

			return pre + "." + preDollarSignStrip._1() + "$" + preDollarSignStrip._2();
		}

		return pre + "." + shortened._2();
	}

	private static String stripLowercase(String input) {
		StringBuilder sb = new StringBuilder();

		for (char c : input.toCharArray()) {
			if (Character.isUpperCase(c)) {
				sb.append(c);
			}
		}

		return sb.toString();
	}

	private static Tuple2<String, String> abbreviatePrefix(@NotNull String input, @NotNull String regex, @NotNull Function<String, String> transformation, @NotNull String delimiter) {
		String[] parts = input.split(regex);

		String[] prefix = new String[parts.length - 1];
		System.arraycopy(parts, 0, prefix, 0, prefix.length);

		String pre = Arrays.stream(prefix).filter(s -> !s.isBlank())
						   .map(transformation).collect(Collectors.joining(delimiter));

		String finalPart = parts[prefix.length];

		return new Tuple2<>(pre, finalPart);
	}

	private void internalLog(@NotNull LogLevel level, @NotNull String message) {
		if (isLevelEnabled(level)) {
			ZonedDateTime now = ZonedDateTime.now();

			logToStandardOut(now, level, message, null);
		}
	}

	private void internalLog(@NotNull LogLevel level, @NotNull String message,
							 @NotNull Throwable throwable) {
		if (isLevelEnabled(level)) {
			ZonedDateTime now = ZonedDateTime.now();

			logToStandardOut(now, level, message, throwable);
		}
	}

	private void internalLog(@NotNull LogLevel level, @NotNull String format,
							 @Nullable Object param1) {
		if (isLevelEnabled(level)) {
			ZonedDateTime now = ZonedDateTime.now();

			String[] fragments = findPlaceholders(format);

			String message;
			Throwable t = param1 instanceof Throwable ? (Throwable) param1 : null;

			if (fragments.length == 1) {
				message = fragments[0];
			} else {
				message = fragments[0] + param1 + fragments[1];
			}

			logToStandardOut(now, level, message, t);
		}
	}

	private void internalLog(@NotNull LogLevel level, @NotNull String format,
							 @Nullable Object param1, @Nullable Object param2) {
		if (isLevelEnabled(level)) {
			ZonedDateTime now = ZonedDateTime.now();

			String[] fragments = findPlaceholders(format);

			String message;
			Throwable t = param1 instanceof Throwable ? (Throwable) param1 : param2 instanceof Throwable ? (Throwable) param2 : null;

			if (fragments.length == 1) {
				message = fragments[0];
			} else if (fragments.length == 2) {
				message = fragments[0] + param1 + fragments[1];
			} else {
				message = fragments[0] + param1 + fragments[1] + param2 + fragments[2];
			}

			logToStandardOut(now, level, message, t);
		}
	}

	private void internalLog(@NotNull LogLevel level, @NotNull String format,
							 @NotNull Object... params) {
		if (isLevelEnabled(level)) {
			ZonedDateTime now = ZonedDateTime.now();
			Throwable t = null;

			String[] fragments = findPlaceholders(format);

			int i = 0;
			StringBuilder message = new StringBuilder(fragments[i++]);
			for (Object param : params) {
				if (i >= fragments.length) {
					break;
				}
				if (t == null && param instanceof Throwable) {
					t = (Throwable) param;
				}
				message.append(param);
				message.append(fragments[i++]);
			}

			logToStandardOut(now, level, message.toString(), t);
		}
	}

	private String[] findPlaceholders(String format) {
		int placeholderCount = 0;

		// Initial pass: count placeholders so we don't need to use lists or temporary arrays
		for (int i = 0; i < format.length() - 1; i++) {
			if (format.charAt(i) == '{' && format.charAt(i + 1) == '}') {
				placeholderCount++;
			}
		}

		String[] result = new String[placeholderCount + 1];
		StringBuilder next = new StringBuilder();

		int current = 0;

		for (int i = 0; i < format.length() - 1; i++) {
			if (format.charAt(i) == '{' && format.charAt(i + 1) == '}') {
				result[current++] = next.toString();
				next = new StringBuilder();
				i++;
			} else {
				next.append(format.charAt(i));
			}
		}

		result[current] = next.toString();

		return result;
	}

	private synchronized void logToStandardOut(@NotNull ZonedDateTime dateTime,
											   @NotNull LogLevel level, @NotNull String message,
											   @Nullable Throwable throwable) {
		logToStandardOut(dateTime, level, channel, name, message, throwable, channelFormat);

	}

	static void logToStandardOut(ZonedDateTime dateTime, LogLevel level, String channel, String name, String message, Throwable throwable, String channelFormat) {
		String shortName = shorten(name);

		loggerNameLength.observeLength(shortName.length());

		String threadName = Thread.currentThread().getName();

		threadLength.observeLength(threadName.length());

		System.out.print("[");
		printColoredLevel(level);
		System.out.print("] {");
		System.out.printf(purple(channelFormat), channel);

		System.out.print("} (");
		System.out.printf(cyan(threadLength.getFormat()), threadName);
		System.out.print(") [ ");

		String formattedTime = TIME_FORMATTER.format(dateTime); // Max 16
		System.out.print(lightBlue(formattedTime));
		System.out.print(" ".repeat(18 - formattedTime.length()));
		System.out.print(lightBlue(OFFSET_FORMATTER.format(dateTime)));
		System.out.print(" ] ");
		System.out.printf(brown(loggerNameLength.getFormat()), shortName);
		System.out.print(" - ");
		System.out.println(message);

		if (throwable != null) {
			throwable.printStackTrace(System.out);
		}
	}

	private static void printColoredLevel(@NotNull LogLevel level) {
		switch (level) {
			case INFO -> System.out.print(lightGreen(level.getLogRepresentation()));
			case WARN -> System.out.print(yellow(level.getLogRepresentation()));
			case ERROR -> System.out.print(lightRed(level.getLogRepresentation()));
			default -> System.out.print(level.getLogRepresentation());
		}
	}

	private boolean isLevelEnabled(@NotNull LogLevel level) {
		return channel != null && minimumLogLevel != null
				&& level.compareTo(minimumLogLevel) >= 0;
	}

	@Override
	public void trace(String msg) {
		internalLog(LogLevel.TRACE, msg);
	}

	@Override
	public void debug(String msg) {
		internalLog(LogLevel.DEBUG, msg);
	}

	@Override
	public void info(String msg) {
		internalLog(LogLevel.INFO, msg);
	}

	@Override
	public void warn(String msg) {
		internalLog(LogLevel.WARN, msg);
	}

	@Override
	public void error(String msg) {
		internalLog(LogLevel.ERROR, msg);
	}

	@Override
	public boolean isTraceEnabled() {
		return isLevelEnabled(LogLevel.TRACE);
	}

	@Override
	public boolean isDebugEnabled() {
		return isLevelEnabled(LogLevel.DEBUG);
	}

	@Override
	public boolean isInfoEnabled() {
		return isLevelEnabled(LogLevel.INFO);
	}

	@Override
	public boolean isWarnEnabled() {
		return isLevelEnabled(LogLevel.WARN);
	}

	@Override
	public boolean isErrorEnabled() {
		return isLevelEnabled(LogLevel.ERROR);
	}

	@Override
	public void trace(String format, Object arg) {
		internalLog(LogLevel.TRACE, format, arg);
	}

	@Override
	public void trace(String format, Object arg1, Object arg2) {
		internalLog(LogLevel.TRACE, format, arg1, arg2);
	}

	@Override
	public void trace(String format, Object... arguments) {
		internalLog(LogLevel.TRACE, format, arguments);
	}

	@Override
	public void trace(String msg, Throwable t) {
		internalLog(LogLevel.TRACE, msg, t);
	}

	@Override
	public void debug(String format, Object arg) {
		internalLog(LogLevel.DEBUG, format, arg);
	}

	@Override
	public void debug(String format, Object arg1, Object arg2) {
		internalLog(LogLevel.DEBUG, format, arg1, arg2);
	}

	@Override
	public void debug(String format, Object... arguments) {
		internalLog(LogLevel.DEBUG, format, arguments);
	}

	@Override
	public void debug(String msg, Throwable t) {
		internalLog(LogLevel.DEBUG, msg, t);
	}

	@Override
	public void info(String format, Object arg) {
		internalLog(LogLevel.INFO, format, arg);
	}

	@Override
	public void info(String format, Object arg1, Object arg2) {
		internalLog(LogLevel.INFO, format, arg1, arg2);
	}

	@Override
	public void info(String format, Object... arguments) {
		internalLog(LogLevel.INFO, format, arguments);
	}

	@Override
	public void info(String msg, Throwable t) {
		internalLog(LogLevel.INFO, msg, t);
	}

	@Override
	public void warn(String format, Object arg) {
		internalLog(LogLevel.WARN, format, arg);
	}

	@Override
	public void warn(String format, Object arg1, Object arg2) {
		internalLog(LogLevel.WARN, format, arg1, arg2);
	}

	@Override
	public void warn(String format, Object... arguments) {
		internalLog(LogLevel.WARN, format, arguments);
	}

	@Override
	public void warn(String msg, Throwable t) {
		internalLog(LogLevel.WARN, msg, t);
	}

	@Override
	public void error(String format, Object arg) {
		internalLog(LogLevel.ERROR, format, arg);
	}

	@Override
	public void error(String format, Object arg1, Object arg2) {
		internalLog(LogLevel.ERROR, format, arg1, arg2);
	}

	@Override
	public void error(String format, Object... arguments) {
		internalLog(LogLevel.ERROR, format, arguments);
	}

	@Override
	public void error(String msg, Throwable t) {
		internalLog(LogLevel.ERROR, msg, t);
	}

	@Override
	public boolean isTraceEnabled(Marker marker) {
		return isTraceEnabled();
	}

	@Override
	public void trace(Marker marker, String msg) {
		trace(msg);
	}

	@Override
	public void trace(Marker marker, String format, Object arg) {
		trace(format, arg);
	}

	@Override
	public void trace(Marker marker, String format, Object arg1, Object arg2) {
		trace(format, arg1, arg2);
	}

	@Override
	public void trace(Marker marker, String format, Object... arguments) {
		trace(format, arguments);
	}

	@Override
	public void trace(Marker marker, String msg, Throwable t) {
		trace(msg, t);
	}

	@Override
	public boolean isDebugEnabled(Marker marker) {
		return isDebugEnabled();
	}

	@Override
	public void debug(Marker marker, String msg) {
		debug(msg);
	}

	@Override
	public void debug(Marker marker, String format, Object arg) {
		debug(format, arg);
	}

	@Override
	public void debug(Marker marker, String format, Object arg1, Object arg2) {
		debug(format, arg1, arg2);
	}

	@Override
	public void debug(Marker marker, String format, Object... arguments) {
		debug(format, arguments);
	}

	@Override
	public void debug(Marker marker, String msg, Throwable t) {
		debug(msg, t);
	}

	@Override
	public boolean isInfoEnabled(Marker marker) {
		return isInfoEnabled();
	}

	@Override
	public void info(Marker marker, String msg) {
		info(msg);
	}

	@Override
	public void info(Marker marker, String format, Object arg) {
		info(format, arg);
	}

	@Override
	public void info(Marker marker, String format, Object arg1, Object arg2) {
		info(format, arg1, arg2);
	}

	@Override
	public void info(Marker marker, String format, Object... arguments) {
		info(format, arguments);
	}

	@Override
	public void info(Marker marker, String msg, Throwable t) {
		info(msg, t);
	}

	@Override
	public boolean isWarnEnabled(Marker marker) {
		return isWarnEnabled();
	}

	@Override
	public void warn(Marker marker, String msg) {
		warn(msg);
	}

	@Override
	public void warn(Marker marker, String format, Object arg) {
		warn(format, arg);
	}

	@Override
	public void warn(Marker marker, String format, Object arg1, Object arg2) {
		warn(format, arg1, arg2);
	}

	@Override
	public void warn(Marker marker, String format, Object... arguments) {
		warn(format, arguments);
	}

	@Override
	public void warn(Marker marker, String msg, Throwable t) {
		warn(msg, t);
	}

	@Override
	public boolean isErrorEnabled(Marker marker) {
		return isErrorEnabled();
	}

	@Override
	public void error(Marker marker, String msg) {
		error(msg);
	}

	@Override
	public void error(Marker marker, String format, Object arg) {
		error(format, arg);
	}

	@Override
	public void error(Marker marker, String format, Object arg1, Object arg2) {
		error(format, arg1, arg2);
	}

	@Override
	public void error(Marker marker, String format, Object... arguments) {
		error(format, arguments);
	}

	@Override
	public void error(Marker marker, String msg, Throwable t) {
		error(msg, t);
	}
}
