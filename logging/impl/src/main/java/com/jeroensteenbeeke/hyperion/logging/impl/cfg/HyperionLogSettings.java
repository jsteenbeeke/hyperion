package com.jeroensteenbeeke.hyperion.logging.impl.cfg;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementWrapper;
import jakarta.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * JAXB definition for the hyperion-log.xml file.
 */
@XmlRootElement(name = "settings")
public class HyperionLogSettings {
	private List<ChannelDefinition> channelDefinitions;

	private List<LevelDefinition> levelDefinitions;

	/**
	 * @return The list of channel definitions
	 */
	@XmlElementWrapper(name = "channels")
	@XmlElement(name = "channel")
	public List<ChannelDefinition> getChannelDefinitions() {
		return channelDefinitions;
	}

	/**
	 * Sets the channel definitions
	 * @param channelDefinitions A list
	 */
	public void setChannelDefinitions(List<ChannelDefinition> channelDefinitions) {
		this.channelDefinitions = channelDefinitions;
	}

	/**
	 * @return The list of log level settings
	 */
	@XmlElementWrapper(name = "levels")
	@XmlElement(name = "level")
	public List<LevelDefinition> getLevelDefinitions() {
		return levelDefinitions;
	}

	/**
	 * Sets the list of level definitions
	 * @param levelDefinitions The list
	 */
	public void setLevelDefinitions(List<LevelDefinition> levelDefinitions) {
		this.levelDefinitions = levelDefinitions;
	}
}
