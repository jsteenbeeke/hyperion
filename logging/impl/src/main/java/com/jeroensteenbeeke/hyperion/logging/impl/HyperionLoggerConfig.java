package com.jeroensteenbeeke.hyperion.logging.impl;

import com.jeroensteenbeeke.hyperion.logging.impl.cfg.ChannelDefinition;
import com.jeroensteenbeeke.hyperion.logging.impl.cfg.HyperionLogSettings;
import com.jeroensteenbeeke.hyperion.logging.impl.cfg.LevelDefinition;
import com.jeroensteenbeeke.hyperion.logging.impl.model.LogLevel;

import org.jetbrains.annotations.NotNull;
import jakarta.xml.bind.JAXB;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

class HyperionLoggerConfig {
	private static final Set<String> VALID_LOG_LEVELS = Arrays
			.stream(LogLevel.values()).map(Enum::name)
			.collect(Collectors.toSet());

	private final List<ChannelConfig> channels;

	private final List<LogLevelConfig> logLevels;

	private HyperionLoggerConfig(@NotNull List<ChannelConfig> channels,
								 List<LogLevelConfig> logLevels) {
		this.channels = channels;
		this.logLevels = logLevels;
	}

	public int getMaxChannelLength() {
		return channels.stream().map(ChannelConfig::getChannel)
				.mapToInt(String::length).max().orElse(0);
	}

	public static HyperionLoggerConfig fromStream(
			@NotNull InputStream inputStream) {
		HyperionLogSettings logSettings = JAXB.unmarshal(inputStream, HyperionLogSettings.class);

		List<ChannelConfig> channels;
		List<LogLevelConfig> levels = null;

		List<ChannelDefinition> channelDefinitions = logSettings.getChannelDefinitions();
		if (channelDefinitions == null || channelDefinitions.isEmpty()) {
			channels = List.of(new ChannelConfig(".*", "default"));
		} else {
			channels = channelDefinitions.stream()
										 .map(def -> new ChannelConfig(
												 determinePattern(def.getPattern()),
												 def.getChannel()))
										 .collect(Collectors.toList());
		}

		List<LevelDefinition> levelDefinitions = logSettings.getLevelDefinitions();
		if (levelDefinitions != null && !levelDefinitions.isEmpty()) {
			levels = levelDefinitions.stream()
									 .filter(def -> isValidLogLevel(def.getLogLevel()))
									 .map(def -> new LogLevelConfig(
											 determinePattern(def.getPattern()), LogLevel.valueOf(
											 def.getLogLevel().toUpperCase())))
									 .collect(Collectors.toList());

		}

		if (levels == null || levels.isEmpty()) {
			levels = List.of(new LogLevelConfig(".*", LogLevel.INFO));
		}

		return new HyperionLoggerConfig(channels, levels);
	}

	private static boolean isValidLogLevel(Object value) {
		if (value instanceof String) {
			String v = (String) value;

			return VALID_LOG_LEVELS.contains(v.toUpperCase());
		}

		return false;
	}

	private static String determinePattern(String pattern) {
		if (pattern.startsWith("\"") && pattern.endsWith("\"")) {
			pattern = pattern.substring(1, pattern.length() - 1);
		}

		if (pattern.equals("*")) {
			return ".*";
		}

		pattern = pattern.replace(".", "\\.");
		pattern = pattern.replace("*", ".*");
		return pattern;
	}

	public static HyperionLoggerConfig empty() {
		return new HyperionLoggerConfig(
				List.of(new ChannelConfig(".*", "default")),
				List.of(new LogLevelConfig(".*", LogLevel.INFO)));
	}

	public Optional<String> findChannel(@NotNull String name) {
		for (ChannelConfig channel : channels) {
			if (name.matches(channel.getPattern())) {
				return Optional.of(channel.getChannel());
			}
		}

		return Optional.empty();
	}

	public Optional<LogLevel> findMinimumLogLevel(@NotNull String name) {
		for (LogLevelConfig logLevel : logLevels) {
			if (name.matches(logLevel.getPattern())) {
				return Optional.of(logLevel.getLevel());
			}
		}

		return Optional.empty();
	}

	private static class ChannelConfig {
		private final String pattern;

		private final String channel;

		private ChannelConfig(String pattern, String channel) {
			this.pattern = pattern;
			this.channel = channel;
		}

		public String getPattern() {
			return pattern;
		}

		public String getChannel() {
			return channel;
		}
	}

	private static class LogLevelConfig {
		private final String pattern;

		private final LogLevel level;

		public LogLevelConfig(@NotNull String pattern,
							  @NotNull LogLevel level) {
			this.pattern = pattern;
			this.level = level;
		}

		public String getPattern() {
			return pattern;
		}

		public LogLevel getLevel() {
			return level;
		}
	}

}
