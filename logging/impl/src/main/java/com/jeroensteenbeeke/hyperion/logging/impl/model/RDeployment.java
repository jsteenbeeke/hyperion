package com.jeroensteenbeeke.hyperion.logging.impl.model;

import org.jetbrains.annotations.NotNull;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A deployment of an application
 */
public class RDeployment implements Serializable {
	private static final long serialVersionUID = 2889476021668263779L;

	private String version;

	private String commitMessage;

	private String hyperionVersion;

	private String hyperionCommitMessage;

	private ZonedDateTime deploymentTime;

	/**
	 * Non-args constructor for reflective construction
	 */
	protected RDeployment() {
	}

	/**
	 * Constructor
	 * @param version The application version
	 * @param commitMessage The application's last commit message
	 * @param hyperionVersion The version of Hyperion
	 * @param hyperionCommitMessage The commit message of Hyperion
	 * @param deploymentTime The time of the deployment
	 */
	public RDeployment(@NotNull String version,
					   @NotNull String commitMessage,
					   @NotNull String hyperionVersion,
					   @NotNull String hyperionCommitMessage,
					   @NotNull ZonedDateTime deploymentTime) {
		this.version = version;
		this.commitMessage = commitMessage;
		this.hyperionVersion = hyperionVersion;
		this.hyperionCommitMessage = hyperionCommitMessage;
		this.deploymentTime = deploymentTime;
	}

	/**
	 * Get the version
	 * @return The version
	 */
	@NotNull
	public String getVersion() {
		return version;
	}

	/**
	 * Set the version
	 * @param version The version
	 */
	public void setVersion(@NotNull String version) {
		this.version = version;
	}

	/**
	 * Get the commit message
	 * @return The message
	 */
	@NotNull
	public String getCommitMessage() {
		return commitMessage;
	}

	/**
	 * Sets the commit message
	 * @param commitMessage The message
	 */
	public void setCommitMessage(@NotNull String commitMessage) {
		this.commitMessage = commitMessage;
	}

	/**
	 * Get the Hyperion version
	 * @return The Hyperion version
	 */
	@NotNull
	public String getHyperionVersion() {
		return hyperionVersion;
	}

	/**
	 * Sets the Hyperion version
	 * @param hyperionVersion The Hyperion version
	 */
	public void setHyperionVersion(@NotNull String hyperionVersion) {
		this.hyperionVersion = hyperionVersion;
	}

	/**
	 * Gets the Hyperion Commit Message
	 * @return The commit message
	 */
	@NotNull
	public String getHyperionCommitMessage() {
		return hyperionCommitMessage;
	}

	/**
	 * Sets the Hyperion Commit Message
	 * @param hyperionCommitMessage The commit message
	 */
	public void setHyperionCommitMessage(@NotNull String hyperionCommitMessage) {
		this.hyperionCommitMessage = hyperionCommitMessage;
	}

	/**
	 * Get the deployment time
	 * @return The time
	 */
	@NotNull
	public ZonedDateTime getDeploymentTime() {
		return deploymentTime;
	}

	/**
	 * Sets the deployment time
	 * @param deploymentTime The time
	 */
	public void setDeploymentTime(@NotNull ZonedDateTime deploymentTime) {
		this.deploymentTime = deploymentTime;
	}
}
