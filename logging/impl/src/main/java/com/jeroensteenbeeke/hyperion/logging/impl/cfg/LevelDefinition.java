package com.jeroensteenbeeke.hyperion.logging.impl.cfg;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlValue;

/**
 * Log level declaration. For all classes matching the pattern, only messages of log level X or higher are saved
 */
@XmlRootElement(name="level")
public class LevelDefinition {
	private String pattern;

	private String logLevel;

	/**
	 * The pattern to match. An asterix is a catch-all wildcard
	 * @return The pattern
	 */
	@XmlAttribute
	public String getPattern() {
		return pattern;
	}

	/**
	 * Sets the pattern
	 * @param pattern The pattern
	 */
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	/**
	 * The log level
	 * @return The log level to use for this pattern
	 */
	@XmlValue
	public String getLogLevel() {
		return logLevel;
	}

	/**
	 * Sets the log level
	 * @param logLevel The level. Case insensitive. valid values are: TRACE, DEBUG, INFO, WARN and ERROR
	 */
	public void setLogLevel(String logLevel) {
		this.logLevel = logLevel;
	}
}
