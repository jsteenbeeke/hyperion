package com.jeroensteenbeeke.hyperion.logging.impl.model;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.io.Serializable;

/**
 * Descriptor of a Java StackTraceElement
 */
public class StackTraceElementDescriptor implements Serializable {
	private static final long serialVersionUID = 6458772725777481141L;

	private String classLoaderName;

	private String moduleName;

	private String moduleVersion;

	private String declaringClass;

	private String methodName;

	private String fileName;

	private int lineNumber;

	private boolean nativeMethod;

	/**
	 * Non-args constructor for reflective construction
	 */
	protected StackTraceElementDescriptor() {
	}

	/**
	 * Constructor
	 * @param classLoaderName The name of the classLoader
	 * @param moduleName The name of the module
	 * @param moduleVersion The version of the module
	 * @param declaringClass The class declaring the method
	 * @param methodName The method name
	 * @param fileName The file name
	 * @param lineNumber The line number
	 * @param nativeMethod Whether or not the method was a native method
	 */
	public StackTraceElementDescriptor(@Nullable String classLoaderName,
			@Nullable String moduleName, @Nullable String moduleVersion, @NotNull String declaringClass,
			@NotNull String methodName, @Nullable String fileName, int lineNumber,
			boolean nativeMethod) {
		this.classLoaderName = classLoaderName;
		this.moduleName = moduleName;
		this.moduleVersion = moduleVersion;
		this.declaringClass = declaringClass;
		this.methodName = methodName;
		this.fileName = fileName;
		this.lineNumber = lineNumber;
		this.nativeMethod = nativeMethod;
	}

	/**
	 * Returns whether or not the method in question is a native method
	 * @return {@code true} if the method is native, {@code false} otherwise
	 */
	public boolean isNativeMethod() {
		return nativeMethod;
	}

	/**
	 * Sets whether or not the method is native
	 * @param nativeMethod {@code true} if the method is native, {@code false} otherwise
	 */
	public void setNativeMethod(boolean nativeMethod) {
		this.nativeMethod = nativeMethod;
	}

	/**
	 * The name of the classloader
	 * @return The classloader
	 */
	@Nullable
	public String getClassLoaderName() {
		return classLoaderName;
	}

	/**
	 * Sets the name of the classloader
	 * @param classLoaderName The classloader
	 */
	public void setClassLoaderName(@Nullable String classLoaderName) {
		this.classLoaderName = classLoaderName;
	}

	/**
	 * The name of the module containing the class
	 * @return The name of the module
	 */
	@Nullable
	public String getModuleName() {
		return moduleName;
	}

	/**
	 * Sets the name of the module
	 * @param moduleName The name of the module
	 */
	public void setModuleName(@Nullable String moduleName) {
		this.moduleName = moduleName;
	}

	/**
	 * The version of the module
	 * @return The version of the module
	 */
	@Nullable
	public String getModuleVersion() {
		return moduleVersion;
	}

	/**
	 * Sets the version of the module
	 * @param moduleVersion The module version
	 */
	public void setModuleVersion(@Nullable String moduleVersion) {
		this.moduleVersion = moduleVersion;
	}

	/**
	 * Get the class declaring the method of this stacktrace element
	 * @return The class declaring the method
	 */
	@NotNull
	public String getDeclaringClass() {
		return declaringClass;
	}

	/**
	 * Sets the class declaring the method of this stacktrace element
	 * @param declaringClass The declaring class
	 */
	public void setDeclaringClass(@NotNull String declaringClass) {
		this.declaringClass = declaringClass;
	}

	/**
	 * The method from which this element originated
	 * @return The method name
	 */
	@NotNull
	public String getMethodName() {
		return methodName;
	}

	/**
	 * Sets the name of the method
	 * @param methodName The method
	 */
	public void setMethodName(@NotNull String methodName) {
		this.methodName = methodName;
	}

	/**
	 * The name of the file in which the method was declared
	 * @return The name
	 */
	@Nullable
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the name of the file
	 * @param fileName The name of the file
	 */
	public void setFileName(@Nullable String fileName) {
		this.fileName = fileName;
	}

	/**
	 * The line number of the element
	 * @return The line number
	 */
	public int getLineNumber() {
		return lineNumber;
	}

	/**
	 * Sets the line number of the element
	 * @param lineNumber The line number
	 */
	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}

	/**
	 * Turns a Java StackTraceElement into a descriptor
	 * @param element The element
	 * @return A descriptor
	 */
	@NotNull
	public static StackTraceElementDescriptor fromStackTraceElement(@NotNull StackTraceElement element) {
		return new StackTraceElementDescriptor(
				element.getClassLoaderName(),
				element.getModuleName(),
				element.getModuleVersion(),
				element.getClassName(),
				element.getMethodName(),
				element.getFileName(),
				element.getLineNumber(),
				element.isNativeMethod()
		);
	}

	@NotNull
	@Override
	public String toString() {
		String s = "";
		if (classLoaderName != null &&
				!classLoaderName.isEmpty()) {
			s += classLoaderName + "/";
		}
		if (moduleName != null && !moduleName.isEmpty()) {
			s += moduleName;

			if (moduleVersion != null &&
					!moduleVersion.isEmpty()) {
				s += "@" + moduleVersion;
			}
		}
		s = s.isEmpty() ? declaringClass : s + "/" + declaringClass;

		return s + "." + methodName + "(" +
				(isNativeMethod() ? "Native Method)" :
						(fileName != null && lineNumber >= 0 ?
								fileName + ":" + lineNumber + ")" :
								(fileName != null ?  ""+fileName+")" : "Unknown Source)")));
	}
}
