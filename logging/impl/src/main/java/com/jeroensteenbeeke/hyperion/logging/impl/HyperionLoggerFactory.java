package com.jeroensteenbeeke.hyperion.logging.impl;

import com.jeroensteenbeeke.hyperion.logging.impl.model.LogLevel;
import io.vavr.control.Option;
import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.slf4j.bridge.SLF4JBridgeHandler;

import org.jetbrains.annotations.NotNull;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Hyperion-specific logger implementation
 */
public class HyperionLoggerFactory implements ILoggerFactory {
	private static final String INITIAL_CHANNEL_FORMAT = "%1$21s";
	private final Map<String, Logger> loggerMap;

	private final HyperionLoggerConfig config;

	/**
	 * Constructor
	 */
	public HyperionLoggerFactory() {
		this.loggerMap = new ConcurrentHashMap<>();
		ClassLoader classLoader = Thread.currentThread()
										.getContextClassLoader();

		this.config = Optional.ofNullable(
				classLoader.getResourceAsStream("hyperion-log-test.xml"))
							  .map(stream -> {
								  HyperionLoggerImpl.logToStandardOut(ZonedDateTime
																			  .now(), LogLevel.WARN, "$HyperionLoggerFactory$", HyperionLoggerFactory.class
																			  .getName(), "Hyperion Logging initialized with hyperion-log-test.xml in WAR file", null, INITIAL_CHANNEL_FORMAT);
								  return stream;
							  })
							  .or(() -> Option.of(System.getProperty("user.home"))
											  .map(Path::of)
											  .peek(p -> {
												  if (!Files.exists(p)) {
													  HyperionLoggerImpl.logToStandardOut(ZonedDateTime
																								  .now(), LogLevel.WARN, "$HyperionLoggerFactory$", HyperionLoggerFactory.class
																								  .getName(), String
																								  .format("User home %s does not exist", p), null, INITIAL_CHANNEL_FORMAT);
												  } else if (!Files.isDirectory(p)) {
													  HyperionLoggerImpl.logToStandardOut(ZonedDateTime
																								  .now(), LogLevel.WARN, "$HyperionLoggerFactory$", HyperionLoggerFactory.class
																								  .getName(), String
																								  .format("User home %s exists but is not a directory", p), null, INITIAL_CHANNEL_FORMAT);
												  }
											  })
											  .filter(Files::isDirectory)
											  .map(p -> p.resolve("hyperion-log.xml"))
											  .peek(p -> {
												  if (!Files.exists(p)) {
													  HyperionLoggerImpl.logToStandardOut(ZonedDateTime
																								  .now(), LogLevel.WARN, "$HyperionLoggerFactory$", HyperionLoggerFactory.class
																								  .getName(), String
																								  .format("File %s does not exist", p), null, INITIAL_CHANNEL_FORMAT);

												  } else if (!Files.isRegularFile(p)) {
													  HyperionLoggerImpl.logToStandardOut(ZonedDateTime
																								  .now(), LogLevel.WARN, "$HyperionLoggerFactory$", HyperionLoggerFactory.class
																								  .getName(), String
																								  .format("File %s exists but is not a regular file", p), null, INITIAL_CHANNEL_FORMAT);

												  }
											  })
											  .filter(Files::isRegularFile)
											  .toTry(NoSuchElementException::new)
											  .mapTry(Files::newInputStream)
											  .peek(stream -> HyperionLoggerImpl.logToStandardOut(ZonedDateTime
																						  .now(), LogLevel.WARN, "$HyperionLoggerFactory$", HyperionLoggerFactory.class
																						  .getName(), "Hyperion Logging initialized with /hyperion-log.xml in Servlet Container Home", null, INITIAL_CHANNEL_FORMAT))
											  .onFailure(t -> HyperionLoggerImpl.logToStandardOut(ZonedDateTime
																						  .now(), LogLevel.WARN, "$HyperionLoggerFactory$", HyperionLoggerFactory.class
																						  .getName(), "Could not open hyperion-log.xml in user home, stream is null", null, INITIAL_CHANNEL_FORMAT))
											  .toJavaOptional())
							  .or(() -> Optional
									  .ofNullable(classLoader.getResourceAsStream("hyperion-log.xml"))
									  .map(stream -> {
										  HyperionLoggerImpl.logToStandardOut(ZonedDateTime
																					  .now(), LogLevel.WARN, "$HyperionLoggerFactory$", HyperionLoggerFactory.class
																					  .getName(), "Hyperion Logging initialized with hyperion-log.xml in WAR file", null, INITIAL_CHANNEL_FORMAT);

										  return stream;
									  }))
							  .map(HyperionLoggerConfig::fromStream)
							  .orElseGet(() -> {
								  HyperionLoggerImpl.logToStandardOut(ZonedDateTime
																			  .now(), LogLevel.WARN, "$HyperionLoggerFactory$", HyperionLoggerFactory.class
																			  .getName(), "Hyperion Logging initialized with EMPTY CONFIG", null, INITIAL_CHANNEL_FORMAT);

								  return HyperionLoggerConfig.empty();
							  });

		SLF4JBridgeHandler.removeHandlersForRootLogger();
		SLF4JBridgeHandler.install();
	}

	@Override
	public Logger getLogger(@NotNull String name) {
		return loggerMap.computeIfAbsent(name, loggerName -> new HyperionLoggerImpl(loggerName, config));
	}
}
