package com.jeroensteenbeeke.hyperion.logging.impl.model;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Serialized form of an exception
 */
public class ExceptionDescriptor implements Serializable {
	private static final long serialVersionUID = 6609028755768139133L;

	private String exceptionClass;

	private String message;

	private List<StackTraceElementDescriptor> stackTrace;

	private ExceptionDescriptor cause;

	private List<ExceptionDescriptor> suppressed;

	/**
	 * Non-args constructor for reflective construction
	 */
	protected ExceptionDescriptor() {
	}

	/**
	 * Constructor
	 * @param exceptionClass The exception class thrown
	 * @param message The message of the exception
	 * @param stackTrace The stack trace of the exception
	 * @param cause The exception that caused this exception
	 * @param suppressed A list of supressed exceptions
	 */
	public ExceptionDescriptor(@NotNull String exceptionClass,
			@Nullable String message,
			@NotNull List<StackTraceElementDescriptor> stackTrace,
			@Nullable ExceptionDescriptor cause,
			@NotNull List<ExceptionDescriptor> suppressed) {
		this.exceptionClass = exceptionClass;
		this.message = message;
		this.stackTrace = stackTrace;
		this.cause = cause;
		this.suppressed = suppressed;
	}

	/**
	 * Gets the type of the exception
	 * @return An FQDN
	 */
	@NotNull
	public String getExceptionClass() {
		return exceptionClass;
	}

	/**
	 * Sets the type of the exception
	 * @param exceptionClass An FQDN
	 */
	public void setExceptionClass(@NotNull String exceptionClass) {
		this.exceptionClass = exceptionClass;
	}

	/**
	 * Gets the message supplied with the exception
	 * @return The message
	 */
	@Nullable
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message supplied with the exception
	 * @param message The message
	 */
	public void setMessage(@Nullable String message) {
		this.message = message;
	}

	/**
	 * Gets the stack trace of the exception
	 * @return The stack trace
	 */
	@NotNull
	public List<StackTraceElementDescriptor> getStackTrace() {
		return stackTrace;
	}

	/**
	 * Sets the stack trace of the exception
	 * @param stackTrace The stack trace
	 */
	public void setStackTrace(
			@NotNull List<StackTraceElementDescriptor> stackTrace) {
		this.stackTrace = stackTrace;
	}

	/**
	 * If this exception was caused by another exception, returns the cause
	 * @return The cause of the exception
	 */
	@Nullable
	public ExceptionDescriptor getCause() {
		return cause;
	}

	/**
	 * Sets the cause of this exception
	 * @param cause The cause
	 */
	public void setCause(@Nullable ExceptionDescriptor cause) {
		this.cause = cause;
	}

	/**
	 * Get any exceptions that were suppressed by this exception
	 * @return A list of suppressed exceptions
	 */
	@NotNull
	public List<ExceptionDescriptor> getSuppressed() {
		return suppressed;
	}

	/**
	 * Sets the list of suppressed exceptions
	 * @param suppressed Suppressed exceptions
	 */
	public void setSuppressed(@NotNull List<ExceptionDescriptor> suppressed) {
		this.suppressed = suppressed;
	}

	/**
	 * Create an ExceptionDescriptor from a Throwable
	 * @param throwable The throwable to describe
	 * @return An ExceptionDescriptor
	 */
	@NotNull
	public static ExceptionDescriptor fromThrowable(
			@NotNull Throwable throwable) {

		return new ExceptionDescriptor(
				throwable.getClass().getName(),
				throwable.getMessage(),
				Arrays.stream(throwable.getStackTrace())
						.map(StackTraceElementDescriptor::fromStackTraceElement)
						.collect(Collectors.toList()),
				Optional.ofNullable(throwable.getCause())
						.map(ExceptionDescriptor::fromThrowable).orElse(null),
				Arrays.stream(throwable.getSuppressed())
						.map(ExceptionDescriptor::fromThrowable)
						.collect(Collectors.toList()));
	}
}
