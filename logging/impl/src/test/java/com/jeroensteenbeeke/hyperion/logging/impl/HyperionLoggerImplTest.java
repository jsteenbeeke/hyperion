package com.jeroensteenbeeke.hyperion.logging.impl;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

import static com.jeroensteenbeeke.hyperion.logging.impl.HyperionLoggerImpl.shorten;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class HyperionLoggerImplTest {
	private static final Logger classLogger = LoggerFactory
			.getLogger(HyperionLoggerImplTest.class);

	private static final Logger errorAndAboveLogger = LoggerFactory
			.getLogger("ErrorAndAbove");

	private static final Logger warnAndAboveLogger = LoggerFactory
			.getLogger("WarnAndAbove");

	private static final Logger placeholderHandler = LoggerFactory
			.getLogger("PlaceholderHandler");

	@Test
	public void testLogging() {
		var oldOut = System.out;
		var myOut = new ByteArrayOutputStream();
		
		System.setOut(new PrintStream(myOut));
		
		classLogger.info("Info in Hyperion Logger");
		classLogger.warn("Warn in Hyperion Logger");
		classLogger.error("Error in Hyperion Logger");
		errorAndAboveLogger.trace("Trace, should not show");
		errorAndAboveLogger.debug("Debug, should not show");
		errorAndAboveLogger.info("Info, should not show");
		errorAndAboveLogger.warn("Warn, should not show");
		errorAndAboveLogger.error("Error, should show");
		warnAndAboveLogger.trace("Trace, should not show");
		warnAndAboveLogger.debug("Debug, should not show");
		warnAndAboveLogger.info("Info, should not show");
		warnAndAboveLogger.warn("Warn, should show");
		warnAndAboveLogger.error("Error, should show");
		
		System.setOut(oldOut);

		var actualOutput = myOut.toString(StandardCharsets.UTF_8);

		assertThat(actualOutput.contains("should not show"), equalTo(false));
	}

	@Test
	public void testShorten() {
		assertThat(shorten("John"), equalTo("John"));
		assertThat(
				shorten("com.jeroensteenbeeke.hyperion.logging.impl.HyperionLoggerTest"),
				equalTo("c.j.h.l.i.HyperionLoggerTest"));
		assertThat(
				shorten("com.jeroensteenbeeke.hyperion.logging.impl.HyperionLoggerTest$FrickenHugeClassName"),
				equalTo("c.j.h.l.i.HLT$FrickenHugeClassName"));
	}

	@Test
	public void testReplacement() {
		var oldOut = System.out;
		var myOut = new ByteArrayOutputStream();

		System.setOut(new PrintStream(myOut));
		
		placeholderHandler.info("{}", 5);
		placeholderHandler.info("{} {}", 6, 7);
		placeholderHandler.info("{} {} {}", 8, 9, 10);
		placeholderHandler.info("{} {} {}", 11, 12, 13, 14);
		placeholderHandler.info("{} {}", 15, 16, 17);
		placeholderHandler.info("{}", 18, 19, 20);
		placeholderHandler.info("{}", 21, 22);
		placeholderHandler.info("", 23, 24);
		placeholderHandler.info("", 25);
		placeholderHandler.info("{} {}", 26);

		System.setOut(oldOut);
		
		var actualOutput = myOut.toString(StandardCharsets.UTF_8);
		
		assertThat(actualOutput.contains("5"), equalTo(true));
		assertThat(actualOutput.contains("6 7"), equalTo(true));
		assertThat(actualOutput.contains("8 9 10"), equalTo(true));
		assertThat(actualOutput.contains("11 12 13"), equalTo(true));
		assertThat(actualOutput.contains("15 16"), equalTo(true));
		assertThat(actualOutput.contains("18"), equalTo(true));
		assertThat(actualOutput.contains("21"), equalTo(true));
		assertThat(actualOutput.contains("26 "), equalTo(true));
	}
}
