package com.jeroensteenbeeke.hyperion.logging.api;

import com.jeroensteenbeeke.hyperion.logging.impl.model.ExceptionDescriptor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class ExceptionDescriptorTest {
	@Test
	public void transformExceptionIncludingStackTrace() {
		Integer value = null;

		try {
			value.toString();

			Assertions.fail("This line should not be reached");
		} catch (NullPointerException e) {
			ExceptionDescriptor exceptionDescriptor = ExceptionDescriptor
					.fromThrowable(e);

			assertThat(exceptionDescriptor.getCause(), nullValue());
			assertThat(exceptionDescriptor.getExceptionClass(), equalTo("java.lang.NullPointerException"));
			assertThat(exceptionDescriptor.getMessage(), equalTo("Cannot invoke \"java.lang.Integer.toString()\" because \"value\" is null"));
			assertThat(exceptionDescriptor.getSuppressed(), equalTo(List.of()));
			assertThat(exceptionDescriptor.getStackTrace().get(0).toString(), equalTo("app//com.jeroensteenbeeke.hyperion.logging.api.ExceptionDescriptorTest.transformExceptionIncludingStackTrace(ExceptionDescriptorTest.java:19)"));
		}
	}

}
