package com.jeroensteenbeeke.hyperion.logging.filter;

import com.jeroensteenbeeke.hyperion.rest.test.AbstractBackendTest;
import com.jeroensteenbeeke.hyperion.solitary.InMemory;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.NullServiceProvider;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.ServiceProvider;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;


public class LogRequestFilterTest extends AbstractBackendTest {

	@Test
	public void doFilter() throws IOException {
		assertThat(get("/").expectingOK().getAs(String.class), not(equalTo("ERROR")));
	}

	@Override
	protected InMemory.Handler createApplicationHandler() throws Exception {
		return InMemory
				.run("logrequest-test")
				.withContextPath("/")
				.atPort(8081)
				.orElseThrow(IllegalStateException::new);
	}

	@Override
	protected String getApplicationHeaderKey() {
		return "X-Unused-Header";
	}

	@Override
	protected String getDefaultApplicationHeader() {
		return "Test";
	}

	@Override
	@NotNull
	protected ServiceProvider getServiceProvider() {
		return new NullServiceProvider();
	}
}
