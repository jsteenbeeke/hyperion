package com.jeroensteenbeeke.hyperion.logging.filter;

import com.jeroensteenbeeke.hyperion.logging.api.HyperionLoggingScope;

import jakarta.servlet.*;
import java.io.IOException;

public class InnerFilter implements Filter
{
	@Override
	public void init(FilterConfig filterConfig) {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException {
		response.getWriter().println(HyperionLoggingScope.currentRequest().getOrElse("ERROR"));
	}

	@Override
	public void destroy() {

	}
}
