package com.jeroensteenbeeke.hyperion.logging.filter;

import com.jeroensteenbeeke.hyperion.logging.api.HyperionLoggingScope;

import jakarta.servlet.*;
import java.io.IOException;

/**
 * Servlet filter that sets Hyperion logging request UUIDss
 */
public class LogRequestFilter implements Filter {
	@Override
	public void init(FilterConfig filterConfig) {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		try {
			HyperionLoggingScope.startRequest();
			chain.doFilter(request, response);
		} finally {
			HyperionLoggingScope.endRequest();
		}
	}

	@Override
	public void destroy() {

	}
}
