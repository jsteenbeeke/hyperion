package com.jeroensteenbeeke.hyperion.meld.filter;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.BaseSearchFilter;
import com.jeroensteenbeeke.hyperion.meld.SearchFilter;
import com.jeroensteenbeeke.hyperion.meld.web.EntityEncapsulator;
import org.apache.wicket.model.IDetachable;
import org.apache.wicket.model.IModel;
import org.danekja.java.util.function.serializable.SerializableFunction;

import org.jetbrains.annotations.NotNull;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Subquery;
import jakarta.persistence.metamodel.SingularAttribute;
import java.io.Serializable;

/**
 * Filter field representing an entity value, usually columns that reference other tables
 * @param <CONTAINING_ENTITY> The entity containing the field
 * @param <ENTITY_ATTRIBUTE> The entity matching the field's attribute
 * @param <F> The current searchfilter
 * @param <EF> The type of searchfilter one would use to find entities of type ENTITY_ATTRIBUTE
 */
public class EntityFilterField<CONTAINING_ENTITY extends DomainObject, ENTITY_ATTRIBUTE extends DomainObject, ID extends Serializable, F extends SearchFilter<CONTAINING_ENTITY, F>, EF extends BaseSearchFilter<ENTITY_ATTRIBUTE, EF>>
		extends SimpleFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, F> implements IEntityFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, ID, F, EF> {
	private static final long serialVersionUID = 1L;

	private ID explicitId;

	private final SingularAttribute<? super ENTITY_ATTRIBUTE, ? extends Serializable> attributeToId;

	private EF subFilter;

	private boolean negated = false;

	/**
	 * Constructor
	 * @param attribute The attribute to create a field for
	 * @param filter The filter this field is defined in
	 * @param attributeToId A function for extracting IDs from the attribute modelled by this entity
	 */
	public EntityFilterField(SingularAttribute<? super CONTAINING_ENTITY, ENTITY_ATTRIBUTE> attribute,
							 F filter,
							 SingularAttribute<? super ENTITY_ATTRIBUTE, ? extends Serializable> attributeToId
	) {
		super(attribute, filter);
		this.attributeToId = attributeToId;
	}

	@Override
	protected IModel<ENTITY_ATTRIBUTE> createModel(ENTITY_ATTRIBUTE value) {
		return EntityEncapsulator.createModel(value);
	}

	@NotNull
	@Override
	public F id(@NotNull ID id) {
		assertNotSet();

		this.explicitId = id;
		return getFilter();
	}

	@NotNull
	@Override
	public F idNot(@NotNull ID id) {
		assertNotSet();

		this.explicitId = id;
		this.negated = true;
		return getFilter();
	}

	@Override
	public boolean isSet() {
		return subFilter != null || explicitId != null || super.isSet();
	}

	@NotNull
	@Override
	public F byFilter(@NotNull EF filter) {
		assertNotSet();

		this.subFilter = filter;

		return getFilter();
	}

	@Override
	protected <FT extends Serializable> Predicate createPredicate(CriteriaBuilder builder, Expression<?> left,
																  ENTITY_ATTRIBUTE right, SerializableFunction<BaseSearchFilter<?,?>,
			Subquery<FT>> subqueryCreator) {
		if (subFilter != null) {
			return left.in(subqueryCreator.apply(subFilter));
		}

		if (explicitId != null) {
			if (negated) {
				return builder.notEqual(left, explicitId);
			} else {
				return builder.equal(left, explicitId);
			}
		}

		return super.createPredicate(builder, left, right, subqueryCreator);
	}

	@Override
	protected <FT extends Serializable> Expression<?> transform(JPA<FT, CONTAINING_ENTITY> jpa,
																CriteriaBuilder builder,
																Expression<ENTITY_ATTRIBUTE> base) {
		if (explicitId != null) {
			return jpa.getRoot().get(getAttribute()).get(attributeToId);
		}

		return super.transform(jpa, builder, base);
	}

	@Override
	public EntityFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, ID, F, EF> createNew() {
		return new EntityFilterField<>(getAttribute(), getFilter(), attributeToId);
	}

	@Override
	public void detach() {
		super.detach();
		if (subFilter instanceof IDetachable detachable) {
			detachable.detach();
		}
	}
}
