package com.jeroensteenbeeke.hyperion.meld.filter;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.BaseSearchFilter;

import org.jetbrains.annotations.NotNull;

/**
 * Filter field containing a String
 * @param <CONTAINING_ENTITY> The entity containing the String
 * @param <F> The filter type
 */
public interface IStringFilterField<CONTAINING_ENTITY extends DomainObject, F extends BaseSearchFilter<CONTAINING_ENTITY, F>>
		extends IBasicFilterField<CONTAINING_ENTITY, String, F> {
	/**
	 * Sets this field to use a case insensitive equals
	 * @param target The expression to match against
	 * @return The current filter
	 */
	@NotNull
	F equalsIgnoreCase(@NotNull String target);

	/**
	 * Sets this field to use a negated case insensitive equals
	 * @param target The expression to match against
	 * @return The current filter
	 */
	@NotNull
	F notEqualsIgnoreCase(@NotNull String target);

	/**
	 * Sets this field to use a case insensitive like expression
	 * @param expression The expression to match against
	 * @return The current filter
	 */
	@NotNull
	F like(@NotNull String expression);

	/**
	 * Sets this field to use a negated case sensitive like expression
	 * @param expression The expression to match against
	 * @return The current filter
	 */
	@NotNull
	F unlike(@NotNull String expression);

	/**
	 * Sets this field to use a case insensitive like expression
	 * @param expression The expression to match against
	 * @return The current filter
	 */
	@NotNull
	F ilike(@NotNull String expression);

	/**
	 * Sets this field to use a negated case insensitive like expression
	 * @param expression The expression to match against
	 * @return The current filter
	 */
	@NotNull
	F iUnlike(@NotNull String expression);


}
