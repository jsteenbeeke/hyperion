package com.jeroensteenbeeke.hyperion.meld.filter;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.BaseSearchFilter;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

/**
 * A filter value representing an entity value
 * @param <CONTAINING_ENTITY> The entity to which the field belongs
 * @param <ENTITY_ATTRIBUTE> The attribute type contained by the field
 * @param <F> The containing filter type
 * @param <EF> Filter type for the entity attribute
 */
public interface IEntityFilterField<CONTAINING_ENTITY extends DomainObject, ENTITY_ATTRIBUTE extends DomainObject, ID extends Serializable, F
		extends BaseSearchFilter<CONTAINING_ENTITY,F>, EF extends BaseSearchFilter<ENTITY_ATTRIBUTE,EF>>
		extends IFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE,F> {
	/**
	 * Entities should have the current field equal to the one with the given ID
	 * @param id The ID the current property should have
	 * @return The current filter
	 */
	@NotNull
	F id(@NotNull ID id);

	/**
	 * Entities should have the current field not equal to the one with the given ID
	 * @param id The ID the current property should have
	 * @return The current filter
	 */
	@NotNull
	F idNot(@NotNull ID id);

	/**
	 * Entities should have the current field not set
	 * @return The containing filter
	 */
	@NotNull
	F isNull();

	/**
	 * Entities should have the current field set to any value
	 * @return The containing filter
	 */
	@NotNull
	F isNotNull();

	/**
	 * Entities should have the current field conform to the given filter
	 * @param filter The filter to apply to the property
	 * @return The containing filter
	 */
	@NotNull
	F byFilter(@NotNull EF filter);
}
