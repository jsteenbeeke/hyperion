package com.jeroensteenbeeke.hyperion.meld.filter;

import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Annotation to mark a given entity field as having a custom filter type
 */
@Retention(RUNTIME)
@Target(FIELD)
@Repeatable(CustomFilters.class)
public @interface CustomFilter {
	/**
	 * The custom filter type of the field
	 * @return The field type
	 */
	@SuppressWarnings("rawtypes")
	Class<? extends IFilterField> type();
	
	/**
	 * The name of the filter field
	 * @return A String describing the name of the field
	 */
	String name();

}
