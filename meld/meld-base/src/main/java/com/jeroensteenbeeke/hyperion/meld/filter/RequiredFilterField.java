package com.jeroensteenbeeke.hyperion.meld.filter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to indicate a given field should always be set in a searchfilter.
 * This annotation is generally placed on an entity field. The searchfilter generator
 * plugin will then copy this annotation to the filter field, which instructs
 * the DAO implementation to check if the field is set
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface RequiredFilterField {
}
