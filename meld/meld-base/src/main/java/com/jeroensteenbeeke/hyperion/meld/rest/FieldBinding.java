package com.jeroensteenbeeke.hyperion.meld.rest;

import com.jeroensteenbeeke.hyperion.meld.BaseSearchFilter;
import com.jeroensteenbeeke.hyperion.meld.filter.IFilterField;
import com.jeroensteenbeeke.hyperion.rest.querysupport.IQueryProperty;
import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import org.danekja.java.util.function.serializable.SerializableBiConsumer;
import org.danekja.java.util.function.serializable.SerializableFunction;

import java.io.Serializable;

/**
 * Field binding, contains logic on how to transform a field from a query
 * filter to a database filter
 *
 * @param <Q>  The query object type
 * @param <QP> The property type
 * @param <F>  The search filter type
 */
class FieldBinding<Q extends QueryObject, QP extends IQueryProperty, F
	extends
		BaseSearchFilter<?, F>> implements Serializable {

	private final SerializableFunction<Q, QP> sourcePropertyFunction;

	private final SerializableBiConsumer<QP, F> targetSetter;

	private final SerializableFunction<F, ? extends IFilterField<?, ?, F>>
		targetPropertyFunction;

	/**
	 * Constructor
	 *
	 * @param sourcePropertyFunction Function for locating source property
	 * @param targetSetter           Function for setting target value
	 * @param targetPropertyFunction Function for location target property
	 */
	FieldBinding(SerializableFunction<Q, QP> sourcePropertyFunction,
				 SerializableBiConsumer<QP, F> targetSetter,
				 SerializableFunction<F, ? extends IFilterField<?, ?, F>>
					 targetPropertyFunction) {
		this.sourcePropertyFunction = sourcePropertyFunction;
		this.targetSetter = targetSetter;
		this.targetPropertyFunction = targetPropertyFunction;
	}

	/**
	 * Returns a function that retrieves the source property from the query object
	 *
	 * @return A function
	 */
	public SerializableFunction<Q, QP> getSourcePropertyFunction() {
		return sourcePropertyFunction;
	}

	/**
	 * Returns a BiConsumer that sets the given value on the target filter
	 * @return A BiConsumer
	 */
	public SerializableBiConsumer<QP, F> getTargetSetter() {
		return targetSetter;
	}

	/**
	 * Returns a function for location the property in the search filter
	 *
	 * @return A function
	 */
	public SerializableFunction<F, ? extends IFilterField<?, ?, F>> getTargetPropertyFunction() {
		return targetPropertyFunction;
	}
}
