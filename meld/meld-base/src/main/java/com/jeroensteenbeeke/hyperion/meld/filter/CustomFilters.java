package com.jeroensteenbeeke.hyperion.meld.filter;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * If a given field has more than one custom filter, this annotation can group them
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface CustomFilters {
	/**
	 * The filters applicable to the field
	 * @return All applicable custom filters
	 */
	CustomFilter[] value();
}
