package com.jeroensteenbeeke.hyperion.meld.rest;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.BaseSearchFilter;
import com.jeroensteenbeeke.hyperion.meld.filter.*;
import io.vavr.collection.Array;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import org.mockito.stubbing.Answer;

import jakarta.persistence.criteria.Order;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestFilter extends BaseSearchFilter<TestEntity, TestFilter> {

	private static final long serialVersionUID = -3218970900536532704L;
	private TestBasicFilterField<TestEntity, Boolean, TestFilter> booleanVal = basicMock();

	private TestStringFilterField<TestEntity, TestFilter> name = stringMock();

	private TestBasicFilterField<TestEntity, TestEnum, TestFilter> enumVal = basicMock();

	private TestComparableFilterField<TestEntity, BigDecimal, TestFilter> bigdecimal =
			comparableMock();

	private TestComparableFilterField<TestEntity, BigInteger, TestFilter> biginteger =
			comparableMock();

	private TestComparableFilterField<TestEntity, Double, TestFilter> doubleVal = comparableMock();

	private TestComparableFilterField<TestEntity, Float, TestFilter> floatVal = comparableMock();

	private TestComparableFilterField<TestEntity, Integer, TestFilter> integer = comparableMock();

	private TestComparableFilterField<TestEntity, LocalDate, TestFilter> localDate = comparableMock();

	private TestComparableFilterField<TestEntity, LocalDateTime, TestFilter> localDateTime =
			comparableMock();

	private TestComparableFilterField<TestEntity, Long, TestFilter> longVal = comparableMock();

	private TestComparableFilterField<TestEntity, Short, TestFilter> shortVal = comparableMock();

	private TestComparableFilterField<TestEntity, ZonedDateTime, TestFilter> zonedDateTime =
			comparableMock();

	private TestComparableFilterField<TestEntity, Date, TestFilter> classicDate =
			comparableMock();

	private TestEntityFilterField<TestEntity, TestOwner, Long, TestFilter, TestOwnerFilter> owner = entityMock();

	@SuppressWarnings("unchecked")
	private <P extends Serializable> TestBasicFilterField<TestEntity, P, TestFilter> basicMock() {

		return applyOrderBy(mock(TestBasicFilterField
																				.class));
	}

	@SuppressWarnings("unchecked")
	private <P extends Serializable, F extends IFilterField<TestEntity, P, TestFilter>> F applyOrderBy(F mock) {
		Order ascending = mock(Order.class);
		when(ascending.isAscending()).thenReturn(true);

		Order descending = mock(Order.class);
		when(descending.isAscending()).thenReturn(false);

		AtomicReference<Boolean> sort = new AtomicReference<>();

		when(mock.orderBy(anyBoolean())).thenAnswer((Answer<TestFilter>) invocation -> {
			sort.set(invocation.getArgument(0));
			return ((F) invocation.getMock()).getFilter();

		});

		when(mock.toOrderBy(any())).thenAnswer((Answer<Option<Seq<Order>>>) invocation -> {
			if (sort.get() != null) {
				if (sort.get()) {
					return Option.of(Array.of(ascending));
				} else {
					return Option.of(Array.of(descending));
				}
			}

			return Option.none();
		});

		return mock;
	}

	@SuppressWarnings("unchecked")
	private <P extends DomainObject, PF extends BaseSearchFilter<P, PF>, ID extends Serializable> TestEntityFilterField<TestEntity, P, ID, TestFilter, PF> entityMock() {
		return applyOrderBy(mock(TestEntityFilterField.class));
	}

	@SuppressWarnings("unchecked")
	private TestStringFilterField<TestEntity, TestFilter> stringMock() {
		return applyOrderBy((TestStringFilterField<TestEntity, TestFilter>) mock(TestStringFilterField.class));
	}

	@SuppressWarnings("unchecked")
	private <C extends Comparable<? super C> & Serializable> TestComparableFilterField<TestEntity, C,
			TestFilter> comparableMock() {
		return applyOrderBy((TestComparableFilterField<TestEntity, C, TestFilter>) mock(TestComparableFilterField.class));
	}

	public TestBasicFilterField<TestEntity, Boolean, TestFilter> booleanVal() {
		return booleanVal;
	}

	public TestBasicFilterField<TestEntity, TestEnum, TestFilter> enumVal() {
		return enumVal;
	}

	public TestStringFilterField<TestEntity, TestFilter> name() {
		return name;
	}

	public TestComparableFilterField<TestEntity, ZonedDateTime, TestFilter> zonedDateTime() {
		return zonedDateTime;
	}

	public TestComparableFilterField<TestEntity, BigDecimal, TestFilter> bigdecimal() {
		return bigdecimal;
	}

	public TestComparableFilterField<TestEntity, BigInteger, TestFilter> biginteger() {
		return biginteger;
	}

	public TestComparableFilterField<TestEntity, Double, TestFilter> doubleVal() {
		return doubleVal;
	}

	public TestComparableFilterField<TestEntity, Float, TestFilter> floatVal() {
		return floatVal;
	}

	public TestComparableFilterField<TestEntity, Integer, TestFilter> integer() {
		return integer;
	}

	public TestComparableFilterField<TestEntity, LocalDate, TestFilter> localDate() {
		return localDate;
	}

	public TestComparableFilterField<TestEntity, LocalDateTime, TestFilter> localDateTime() {
		return localDateTime;
	}

	public TestComparableFilterField<TestEntity, Long, TestFilter> longVal() {
		return longVal;
	}

	public TestComparableFilterField<TestEntity, Short, TestFilter> shortVal() {
		return shortVal;
	}

	public TestComparableFilterField<TestEntity, Date, TestFilter> classicDate() {
		return classicDate;
	}

	public TestEntityFilterField<TestEntity, TestOwner, Long, TestFilter, TestOwnerFilter> owner() {
		return owner;
	}
}

class TestOwnerFilter extends BaseSearchFilter<TestOwner, TestOwnerFilter> {

}

interface TestComparableFilterField<
		CONTAINING_ENTITY extends DomainObject,
		ENTITY_ATTRIBUTE extends Comparable<? super ENTITY_ATTRIBUTE> & Serializable,
		F extends BaseSearchFilter<CONTAINING_ENTITY, F>> extends IComparableFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, F> {
}

interface TestStringFilterField<CONTAINING_ENTITY extends DomainObject, F extends BaseSearchFilter<CONTAINING_ENTITY, F>> extends IStringFilterField<CONTAINING_ENTITY, F> {

}

interface TestEntityFilterField<CONTAINING_ENTITY extends DomainObject, ENTITY_ATTRIBUTE extends DomainObject, ID extends Serializable, F
		extends BaseSearchFilter<CONTAINING_ENTITY, F>, EF extends BaseSearchFilter<ENTITY_ATTRIBUTE, EF>> extends IEntityFilterField<CONTAINING_ENTITY, ENTITY_ATTRIBUTE, ID, F, EF> {

}

interface TestBasicFilterField<CONTAINING_ENTITY extends DomainObject, ENTITY_ATTRIBUTE extends Serializable, FILTER extends BaseSearchFilter<CONTAINING_ENTITY,FILTER>> extends IBasicFilterField<CONTAINING_ENTITY,ENTITY_ATTRIBUTE,FILTER> {

}
