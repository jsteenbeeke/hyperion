package com.jeroensteenbeeke.hyperion.meld.rest;

import com.jeroensteenbeeke.hyperion.meld.filter.JPA;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import org.junit.jupiter.api.Test;

import jakarta.persistence.criteria.*;
import jakarta.persistence.metamodel.SingularAttribute;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class RESTQueryBindingTest {
	@Test
	public void testEmptyBinding() {
		assertThrows(IllegalStateException.class,
				() -> RESTQueryBinding.bind(TestObject.class).to(TestFilter.class).build());
	}

	@Test
	public void testPartialBinding() {
		assertThrows(IllegalStateException.class, () -> RESTQueryBinding.bind(TestObject.class).to(TestFilter.class)
				.withBoolean(TestObject::booleanVal, TestFilter::booleanVal).build());
	}

	@Test
	public void testPartialBinding2() {
		RESTQueryBinding.BindingFieldCollector<TestObject, TestFilter> to1 = RESTQueryBinding
				.bind(TestObject.class)
				.to(TestFilter.class);
		RESTQueryBinding.BindingFieldCollector<TestObject, TestFilter> testObjectTestFilterBindingFieldCollector = to1
				.withBoolean(TestObject::booleanVal, TestFilter::booleanVal);
		assertThrows(IllegalStateException.class, () -> testObjectTestFilterBindingFieldCollector
				.withString(TestObject::name, TestFilter::name)
				.withComparable(TestObject::zonedDateTime, TestFilter::zonedDateTime)
				.withStringEnum(TestObject::enumVal, TestFilter::enumVal,
						TestEnum::valueOf)
				.build());
	}

	@Test
	public void testPartialBinding3() {
		RESTQueryBinding.BindingFieldCollector<TestObject, TestFilter> testObjectTestFilterBindingFieldCollector = RESTQueryBinding
				.bind(TestObject.class)
				.to(TestFilter.class)
				.withBoolean(TestObject::booleanVal, TestFilter::booleanVal);
		assertThrows(IllegalStateException.class, () -> testObjectTestFilterBindingFieldCollector
				.withComparable(TestObject::bigdecimal, TestFilter::bigdecimal)
				.withComparable(TestObject::biginteger, TestFilter::biginteger)
				.withComparable(TestObject::doubleVal, TestFilter::doubleVal)
				.withComparable(TestObject::floatVal, TestFilter::floatVal)
				.withComparable(TestObject::integer, TestFilter::integer)
				.withComparable(TestObject::localDate, TestFilter::localDate)
				.withComparable(TestObject::localDateTime,
						TestFilter::localDateTime)
				.withComparable(TestObject::longVal, TestFilter::longVal)
				.withComparable(TestObject::shortVal, TestFilter::shortVal)
				.withString(TestObject::name, TestFilter::name)
				.withComparable(TestObject::zonedDateTime,
						TestFilter::zonedDateTime)
				.withTemporal(TestObject::convertableDateTime,
						TestFilter::classicDate, ld -> Date.from(ld.atZone(ZoneId
								.systemDefault()).toInstant()))
				.withEntity(TestObject::owner, TestFilter::owner)
				.build());
	}

	@Test
	public void testCompleteBinding() {
		assertNotNull(createFullBinding());
	}

	@Test
	@SuppressWarnings("unchecked")
	public void testOrdering() {
		RESTQueryBinding<TestObject, TestFilter> binding = createFullBinding();

		TestObject t = new TestObject();
		t.bigdecimal().orderBy(true);
		t.doubleVal().orderBy(false);

		TestFilter filter = binding.convert(t);

		Predicate pred = mock(Predicate.class);

		Path<TestEntity> path = mock(Path.class);
		when(path.in((Collection<?>) any())).thenReturn(pred);

		CriteriaBuilder builder = mock(CriteriaBuilder.class);
		when(builder.equal(any(Expression.class), any(String.class))).thenReturn(pred);
		when(builder.notEqual(any(Expression.class), any(String.class))).thenReturn(pred);
		when(builder.between(any(Expression.class), any(String.class), any(String.class))).thenReturn(pred);
		when(builder.not(any(Expression.class))).thenReturn(pred);
		when(builder.greaterThan(any(Expression.class), any(String.class))).thenReturn(pred);
		when(builder.greaterThanOrEqualTo(any(Expression.class), any(String.class))).thenReturn(pred);
		when(builder.lessThan(any(Expression.class), any(String.class))).thenReturn(pred);
		when(builder.lessThanOrEqualTo(any(Expression.class), any(String.class))).thenReturn(pred);
		when(builder.isNull(any(Expression.class))).thenReturn(pred);
		when(builder.isNotNull(any(Expression.class))).thenReturn(pred);

		Root<TestEntity> root = mock(Root.class);
		when(root.get(any(SingularAttribute.class))).thenReturn(path);

		TestComparableFilterField<TestEntity, BigDecimal, TestFilter> bigdecimal = filter.bigdecimal();
		Option<Seq<Order>> obd = bigdecimal.toOrderBy(new JPA<BigDecimal, TestEntity>(
				builder,
				root,
				mock(CriteriaQuery.class),
				f -> mock(Subquery.class)
		));


		assertThat(obd, notNullValue());
		assertTrue(obd.isDefined());

		Seq<Order> orders = obd.get();
		assertThat(orders.size(), equalTo(1));

		Order order = orders.get(0);

		assertThat(order, notNullValue());
		assertTrue(order.isAscending());

		TestComparableFilterField<TestEntity, Double, TestFilter> doubleVal = filter
				.doubleVal();
		Option<Seq<Order>> odv = doubleVal.toOrderBy(new JPA<Double, TestEntity>(
				builder,
				root,
				mock(CriteriaQuery.class),
				f -> mock(Subquery.class)
		));


		assertThat(odv, notNullValue());
		assertTrue(odv.isDefined());

		orders = odv.get();
		assertThat(orders.size(), equalTo(1));

		order = orders.get(0);

		assertThat(order, notNullValue());
		assertFalse(order.isAscending());
	}

	@Test
	public void testBooleanConversion() {
		RESTQueryBinding<TestObject, TestFilter> binding = createFullBinding();

		for (boolean value : List.of(true, false)) {
			TestFilter filter = binding.convert(new TestObject().booleanVal().equalTo(value));
			assertNotNull(filter);
			verify(filter.booleanVal()).equalTo(value);

			filter = binding.convert(new TestObject().booleanVal().not().equalTo(value));
			assertNotNull(filter);
			verify(filter.booleanVal()).equalTo(!value);
		}

		TestFilter filter = binding.convert(new TestObject().booleanVal().isNull());
		assertNotNull(filter);
		verify(filter.booleanVal()).isNull();

		filter = binding.convert(new TestObject().booleanVal().not().isNull());
		assertNotNull(filter);
		verify(filter.booleanVal()).isNotNull();
	}

	@Test
	public void testIntegerEnumConversion() {
		RESTQueryBinding<TestObject, TestFilter> binding = createFullBinding();

		TestEnum value = TestEnum.THREE;

		TestFilter filter = binding.convert(new TestObject().intEnum().equalTo(value
				.ordinal()));
		assertNotNull(filter);
		verify(filter.enumVal()).equalTo(value);

		filter = binding.convert(new TestObject().intEnum().not().equalTo(value
				.ordinal()));
		assertNotNull(filter);
		verify(filter.enumVal()).notEqualTo(value);

		filter = binding.convert(new TestObject().intEnum().isNull());
		assertNotNull(filter);
		verify(filter.enumVal()).isNull();

		filter = binding.convert(new TestObject().intEnum().not().isNull());
		assertNotNull(filter);
		verify(filter.enumVal()).isNotNull();
	}

	@Test
	public void testStringEnumConversion() {
		RESTQueryBinding<TestObject, TestFilter> binding = createFullBinding();

		TestEnum value = TestEnum.THREE;

		TestFilter filter = binding.convert(new TestObject().enumVal().equalTo(value
				.toString()));
		assertNotNull(filter);
		verify(filter.enumVal()).equalTo(value);

		filter = binding.convert(new TestObject().enumVal().not().equalTo(value
				.toString()));
		assertNotNull(filter);
		verify(filter.enumVal()).notEqualTo(value);

		filter = binding.convert(new TestObject().enumVal().isNull());
		assertNotNull(filter);
		verify(filter.enumVal()).isNull();

		filter = binding.convert(new TestObject().enumVal().not().isNull());
		assertNotNull(filter);
		verify(filter.enumVal()).isNotNull();
	}

	@Test
	public void testStringConversion() {
		RESTQueryBinding<TestObject, TestFilter> binding = createFullBinding();

		TestFilter filter = binding.convert(new TestObject().name().equalTo("Test"));
		assertNotNull(filter);
		verify(filter.name()).equalTo("Test");

		filter = binding.convert(new TestObject().name().equalToIgnoreCase("Test"));
		assertNotNull(filter);
		verify(filter.name()).equalsIgnoreCase("Test");

		filter = binding.convert(new TestObject().name().not().equalTo("Test"));
		assertNotNull(filter);
		verify(filter.name()).notEqualTo("Test");

		filter = binding.convert(new TestObject().name().not().equalToIgnoreCase("Test"));
		assertNotNull(filter);
		verify(filter.name()).notEqualsIgnoreCase("Test");

		filter = binding.convert(new TestObject().name().isNull());
		assertNotNull(filter);
		verify(filter.name()).isNull();

		filter = binding.convert(new TestObject().name().not().isNull());
		assertNotNull(filter);
		verify(filter.name()).isNotNull();

		filter = binding.convert(new TestObject().name().like("Test"));
		assertNotNull(filter);
		verify(filter.name()).like("Test");

		filter = binding.convert(new TestObject().name().ilike("Test"));
		assertNotNull(filter);
		verify(filter.name()).ilike("Test");


		filter = binding.convert(new TestObject().name().not().like("Test"));
		assertNotNull(filter);
		verify(filter.name()).unlike("Test");

		filter = binding.convert(new TestObject().name().not().ilike("Test"));
		assertNotNull(filter);
		verify(filter.name()).iUnlike("Test");
	}

	@Test
	public void testDateConversion() {
		RESTQueryBinding<TestObject, TestFilter> binding = createFullBinding();

		LocalDateTime time = LocalDateTime.now();
		Date date = Date.from(time.atZone(ZoneId.systemDefault()).toInstant());

		TestFilter filter = binding.convert(new TestObject().convertableDateTime().equalTo(time));
		assertNotNull(filter);
		verify(filter.classicDate()).equalTo(date);

		filter = binding.convert(new TestObject().convertableDateTime().lessThan(time));
		assertNotNull(filter);
		verify(filter.classicDate()).lessThan(date);

		filter = binding.convert(new TestObject().convertableDateTime().lessThanOrEqualTo(time));
		assertNotNull(filter);
		verify(filter.classicDate()).lessThanOrEqualTo(date);

		filter = binding.convert(new TestObject().convertableDateTime().greaterThan(time));
		assertNotNull(filter);
		verify(filter.classicDate()).greaterThan(date);

		filter = binding.convert(new TestObject().convertableDateTime().greaterThanOrEqualTo
				(time));
		assertNotNull(filter);
		verify(filter.classicDate()).greaterThanOrEqualTo(date);

		filter = binding.convert(new TestObject().convertableDateTime().between(time, time));
		assertNotNull(filter);
		verify(filter.classicDate()).between(date, date);

		filter = binding.convert(new TestObject().convertableDateTime().isNull());
		assertNotNull(filter);
		verify(filter.classicDate()).isNull();

		filter = binding.convert(new TestObject().convertableDateTime().not().equalTo(time));
		assertNotNull(filter);
		verify(filter.classicDate()).notEqualTo(date);

		filter = binding.convert(new TestObject().convertableDateTime().not().lessThan(time));
		assertNotNull(filter);
		verify(filter.classicDate()).greaterThanOrEqualTo(date);

		filter = binding.convert(
				new TestObject().convertableDateTime().not().lessThanOrEqualTo(time));
		assertNotNull(filter);
		verify(filter.classicDate()).greaterThan(date);

		filter = binding.convert(new TestObject().convertableDateTime().not().greaterThan(time));
		assertNotNull(filter);
		verify(filter.classicDate()).lessThanOrEqualTo(date);

		filter = binding.convert(
				new TestObject().convertableDateTime().not().greaterThanOrEqualTo(time));
		assertNotNull(filter);
		verify(filter.classicDate()).lessThan(date);

		filter = binding.convert(new TestObject().convertableDateTime().not().between(time, time));
		assertNotNull(filter);
		verify(filter.classicDate()).notBetween(date, date);

		filter = binding.convert(new TestObject().convertableDateTime().not().isNull());
		assertNotNull(filter);
		verify(filter.classicDate()).isNotNull();
	}

	@Test
	public void testEntityConversion() {
		RESTQueryBinding<TestObject, TestFilter> binding = createFullBinding();

		Long id = 666L;

		TestFilter filter = binding.convert(new TestObject().owner().equalTo(id));
		assertNotNull(filter);
		verify(filter.owner()).id(id);

		filter = binding.convert(new TestObject().owner().isNull());
		assertNotNull(filter);
		verify(filter.owner()).isNull();

		filter = binding.convert(new TestObject().owner().not().isNull());
		assertNotNull(filter);
		verify(filter.owner()).isNotNull();

	}

	@Test
	public void testNumberConversion() {
		RESTQueryBinding<TestObject, TestFilter> binding = createFullBinding();

		Long inputNumber = 5L;

		TestFilter filter = binding.convert(new TestObject().longVal().equalTo(inputNumber));
		assertNotNull(filter);
		verify(filter.longVal()).equalTo(inputNumber);

		filter = binding.convert(new TestObject().longVal().lessThan(inputNumber));
		assertNotNull(filter);
		verify(filter.longVal()).lessThan(inputNumber);

		filter = binding.convert(new TestObject().longVal().lessThanOrEqualTo(inputNumber));
		assertNotNull(filter);
		verify(filter.longVal()).lessThanOrEqualTo(inputNumber);

		filter = binding.convert(new TestObject().longVal().greaterThan(inputNumber));
		assertNotNull(filter);
		verify(filter.longVal()).greaterThan(inputNumber);

		filter = binding.convert(new TestObject().longVal().greaterThanOrEqualTo(inputNumber));
		assertNotNull(filter);
		verify(filter.longVal()).greaterThanOrEqualTo(inputNumber);

		filter = binding.convert(new TestObject().longVal().between(inputNumber, inputNumber));
		assertNotNull(filter);
		verify(filter.longVal()).between(inputNumber, inputNumber);

		filter = binding.convert(new TestObject().longVal().isNull());
		assertNotNull(filter);
		verify(filter.longVal()).isNull();

		filter = binding.convert(new TestObject().longVal().not().equalTo(inputNumber));
		assertNotNull(filter);
		verify(filter.longVal()).notEqualTo(inputNumber);

		filter = binding.convert(new TestObject().longVal().not().lessThan(inputNumber));
		assertNotNull(filter);
		verify(filter.longVal()).greaterThanOrEqualTo(inputNumber);

		filter = binding.convert(new TestObject().longVal().not().lessThanOrEqualTo(inputNumber));
		assertNotNull(filter);
		verify(filter.longVal()).greaterThan(inputNumber);

		filter = binding.convert(new TestObject().longVal().not().greaterThan(inputNumber));
		assertNotNull(filter);
		verify(filter.longVal()).lessThanOrEqualTo(inputNumber);

		filter =
				binding.convert(new TestObject().longVal().not().greaterThanOrEqualTo
						(inputNumber));
		assertNotNull(filter);
		verify(filter.longVal()).lessThan(inputNumber);

		filter =
				binding.convert(new TestObject().longVal().not().between(inputNumber,
						inputNumber));
		assertNotNull(filter);
		verify(filter.longVal()).notBetween(inputNumber, inputNumber);

		filter = binding.convert(new TestObject().longVal().not().isNull());
		assertNotNull(filter);
		verify(filter.longVal()).isNotNull();
	}

	@Test
	public void testInvalidObjectBinding() {
		assertThrows(IllegalStateException.class, () ->
				RESTQueryBinding.bind(CorruptTestObject.class).to(TestFilter.class)
						.withIntegerEnum(CorruptTestObject::intEnumVal,
								TestFilter::enumVal,
								ord ->
										TestEnum.values()[ord])
						.build());


	}

	@Test
	public void testInvalidObjectBinding2() {
		assertThrows(IllegalStateException.class,
				() -> RESTQueryBinding.bind(CorruptTestObject2.class).to(TestFilter.class)
						.withIntegerEnum(CorruptTestObject2::intEnumVal,
								TestFilter::enumVal,
								ord ->
										TestEnum.values()[ord])
						.build());


	}

	@Test
	public void testToString() {
		assertEquals(
				"RESTQueryBinding{sourceClass=class com.jeroensteenbeeke.hyperion.meld.rest.TestObject, targetClass=class com.jeroensteenbeeke.hyperion.meld.rest.TestFilter}",
				createFullBinding().toString());
	}

	private RESTQueryBinding<TestObject, TestFilter> createFullBinding() {
		return RESTQueryBinding.bind(TestObject.class).to(TestFilter.class)
				.withBoolean(TestObject::booleanVal, TestFilter::booleanVal)
				.withComparable(TestObject::bigdecimal, TestFilter::bigdecimal)
				.withComparable(TestObject::biginteger, TestFilter::biginteger)
				.withComparable(TestObject::doubleVal, TestFilter::doubleVal)
				.withStringEnum(TestObject::enumVal, TestFilter::enumVal,
						TestEnum::valueOf)
				.withIntegerEnum(TestObject::intEnum, TestFilter::enumVal,
						o -> TestEnum.values()[o])
				.withComparable(TestObject::floatVal, TestFilter::floatVal)
				.withComparable(TestObject::integer, TestFilter::integer)
				.withComparable(TestObject::localDate, TestFilter::localDate)
				.withComparable(TestObject::localDateTime,
						TestFilter::localDateTime)
				.withComparable(TestObject::longVal, TestFilter::longVal)
				.withComparable(TestObject::shortVal, TestFilter::shortVal)
				.withString(TestObject::name, TestFilter::name)
				.withComparable(TestObject::zonedDateTime,
						TestFilter::zonedDateTime)
				.withTemporal(TestObject::convertableDateTime,
						TestFilter::classicDate, ld -> Date.from(ld.atZone(ZoneId
								.systemDefault()).toInstant()))
				.withEntity(TestObject::owner, TestFilter::owner)
				.build();
	}

}
