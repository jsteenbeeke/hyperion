package com.jeroensteenbeeke.hyperion.meld.rest;

import com.jeroensteenbeeke.hyperion.data.BaseDomainObject;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import java.io.Serializable;

@Entity
public class TestEntity extends BaseDomainObject{
	@Id
	private Long id;

	@ManyToOne
	private TestOwner owner;

	@Override
	public Serializable getDomainObjectId() {
		return id;
	}


}
