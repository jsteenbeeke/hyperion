package com.jeroensteenbeeke.hyperion.icons;

import java.io.Serializable;

/**
 * Basic interface representing an icon that can be included with a simple CSS class
 */
public interface Icon extends Serializable {
	/**
	 * Returns the CSS class (or classes) that need to be added to an element to display this icon
	 * @return The css class (or classes) to display the icon
	 */
	String getCssClasses();
}
