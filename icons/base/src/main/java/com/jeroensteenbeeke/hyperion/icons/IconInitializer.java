package com.jeroensteenbeeke.hyperion.icons;

import org.apache.wicket.protocol.http.WebApplication;

import org.jetbrains.annotations.NotNull;

/**
 * Initializer interface for adding icon files to a Wicket application's pages
 */
public interface IconInitializer {
	/**
	 * Initialize the icons, calling relevant methods on the Wicket application
	 * @param application The running Wicket application
	 */
	void initialize(@NotNull WebApplication application);
}
