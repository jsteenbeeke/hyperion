package com.jeroensteenbeeke.hyperion.icons;

import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.html.IHeaderContributor;
import org.apache.wicket.request.resource.CssResourceReference;

import java.util.Arrays;

/**
 * Contributor for adding a CSS reference to all Wicket pages
 */
public class IconCssContributorListener implements IHeaderContributor {
	private final CssResourceReference[] refs;

	/**
	 * Creates a new icon CSS contributor based on the given CSS reference
	 * @param refs The references to include in all pages
	 */
	public IconCssContributorListener(CssResourceReference... refs) {
		this.refs = refs;
	}

	@Override
	public void renderHead(IHeaderResponse response) {
		Arrays.stream(refs).forEach(ref -> response.render(CssHeaderItem.forReference(ref)));
	}
}
