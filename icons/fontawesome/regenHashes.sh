#!/bin/bash

TARGET=src/main/java/com/jeroensteenbeeke/hyperion/icons/fontawesome/webfonts

for i in `ls $TARGET`; do
	NAME=$(echo $i | sed -e 's/-/_/g' | sed 's/_[0-9]00//g' | sed 's/\./_/g')

	HASH=`cat $TARGET/$i | openssl dgst -sha384 -binary | openssl base64 -A`

	echo -e "\t/**"
	echo -e "\t * $i"
	echo -e "\t */"
	echo -e "\tpublic static final FontAwesomeFile ${NAME^^} = new FontAwesomeFile(\"$i\", \"$HASH\");"
	echo
done

for i in `ls $TARGET`; do
	NAME=$(echo $i | sed -e 's/-/_/g' | sed 's/_[0-9]00//g' | sed 's/\./_/g')

	echo -e "FontAwesomeFile.${NAME^^}.mount(application);"
done

TARGET=src/main/java/com/jeroensteenbeeke/hyperion/icons/fontawesome/css

HASH1=$(cat $TARGET/fontawesome.css | openssl dgst -sha384 -binary | openssl base64 -A)
HASH2=$(cat $TARGET/solid.css | openssl dgst -sha384 -binary | openssl base64 -A)
HASH3=$(cat $TARGET/brands.css | openssl dgst -sha384 -binary | openssl base64 -A)
HASH4=$(cat $TARGET/regular.css | openssl dgst -sha384 -binary | openssl base64 -A)

echo -e "application.getCspSettings().blocking().add(CSPDirective.STYLE_SRC,"
echo -e "\t// fontawesome.css"
echo -e "\t\"'sha384-$HASH1'\","
echo -e "\t// fontawesome-solid.css"
echo -e "\t\"'sha384-$HASH2'\","
echo -e "\t// fontawesome-brands.css"
echo -e "\t\"'sha384-$HASH3'\","
echo -e "\t// fontawesome-regular.css"
echo -e "\t\"'sha384-$HASH4'\");"
