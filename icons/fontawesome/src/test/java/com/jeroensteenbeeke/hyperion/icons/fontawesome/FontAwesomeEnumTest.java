package com.jeroensteenbeeke.hyperion.icons.fontawesome;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FontAwesomeEnumTest {
	@Test
	public void testValuesGenerated() {
		FontAwesome[] icons = FontAwesome.values();
		assertTrue(icons.length > 0);

		for (FontAwesome icon: icons) {
			assertNotNull(String.format("Icon %s has CSS defined", icon.name()), icon
					.getCssClasses());
			assertFalse(icon.getCssClasses()
																						 .isEmpty(), String.format("Icon %s has CSS defined", icon.name()));
		}
	}

	@Test
	public void testCssPrefixCorrect() {
		assertEquals("fas fa-edit", FontAwesome.edit.getCssClasses());
	}
}
