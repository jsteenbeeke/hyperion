package com.jeroensteenbeeke.hyperion.icons.fontawesome;

import com.jeroensteenbeeke.hyperion.icons.IconCssContributorListener;
import com.jeroensteenbeeke.hyperion.icons.IconInitializer;
import com.jeroensteenbeeke.hyperion.icons.fontawesome.css.FontAwesomeStylesheets;
import com.jeroensteenbeeke.hyperion.icons.fontawesome.webfonts.FontAwesomeFonts;
import org.apache.wicket.protocol.http.WebApplication;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

/**
 * Icon initializer for FontAwesome icons
 */
public class FontAwesomeInitializer implements IconInitializer {
	private static final FontAwesomeInitializer INSTANCE = new FontAwesomeInitializer();

	private FontAwesomeInitializer() {

	}

	@Override
	public void initialize(@NotNull WebApplication application) {
		application.getHeaderContributorListeners().add(
			new IconCssContributorListener(
				FontAwesomeCssReference.FONTAWESOME,
				FontAwesomeCssReference.FONTAWESOME_REGULAR,
				FontAwesomeCssReference.FONTAWESOME_BRANDS,
				FontAwesomeCssReference.FONTAWESOME_SOLID
			));
		application.mountResource("css/fontawesome.css", FontAwesomeCssReference.FONTAWESOME);
		application.mountResource("css/fontawesome-regular.css", FontAwesomeCssReference.FONTAWESOME_REGULAR);
		application.mountResource("css/fontawesome-brands.css", FontAwesomeCssReference.FONTAWESOME_BRANDS);
		application.mountResource("css/fontawesome-solid.css", FontAwesomeCssReference.FONTAWESOME_SOLID);

		Arrays.stream(FontAwesomeStylesheets.values()).forEach(fas -> fas.registerCSP(application));

		Arrays.stream(FontAwesomeFonts.values()).forEach(fas -> {
			fas.mount(application, "webfonts/");
			fas.registerCSP(application);
		});

	}

	/**
	 * Get a singleton instance of the FontAwesome initializer
	 *
	 * @return The initializer
	 */
	public static synchronized FontAwesomeInitializer get() {
		return INSTANCE;
	}
}
