package com.jeroensteenbeeke.hyperion.icons.fontawesome;

import org.apache.wicket.request.resource.CssResourceReference;

/**
 * CSS references for the FontAwesome css file
 */
public class FontAwesomeCssReference extends CssResourceReference {
	private static final long serialVersionUID = 1100636208472388994L;

	/**
	 * Reference for the base FontAwesome css
	 */
	public static final FontAwesomeCssReference FONTAWESOME =
			new FontAwesomeCssReference("css/fontawesome.css");

	/**
	 * Reference for the Solid FontAwesome css
	 */
	public static final FontAwesomeCssReference FONTAWESOME_SOLID =
			new FontAwesomeCssReference("css/solid.css");

	/**
	 * Reference for the Brands FontAwesome css
	 */
	public static final FontAwesomeCssReference FONTAWESOME_BRANDS =
			new FontAwesomeCssReference("css/brands.css");

	/**
	 * Reference for the Regular FontAwesome css
	 */
	public static final FontAwesomeCssReference FONTAWESOME_REGULAR =
			new FontAwesomeCssReference("css/regular.css");

	private FontAwesomeCssReference(String packageRelativeFilename) {
		super(FontAwesomeCssReference.class, packageRelativeFilename);
	}

}
