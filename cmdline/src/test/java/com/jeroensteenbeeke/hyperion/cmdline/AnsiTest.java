package com.jeroensteenbeeke.hyperion.cmdline;

import com.jeroensteenbeeke.hyperion.color.Ansi;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class AnsiTest {
	static final String JACOCO_DATA = "$jacocoData";
	private static final String ANSI_ENABLED = "ansiEnabled";

	@Test
	public void testAnsiValues() throws IllegalAccessException {
		for (Field field : Ansi.class.getDeclaredFields()) {
			final int modifiers = field.getModifiers();

			final String name = field.getName();
			
			if (JACOCO_DATA.equals(name)) {
				continue;
			}
			
			if (ANSI_ENABLED.equals(name)) {
				continue;
			}

			field.setAccessible(true);
			
			assertTrue(Modifier.isStatic(modifiers) && Modifier.isFinal(modifiers), String.format("Ansi.%s is either not static or not final", name));
			assertEquals(String.class, field.getType(), String.format("Ansi.%s is not a String", name));
			String fieldValue = (String) field.get(null);

			assertNotNull(String.format("Ansi.%s is not set", name));
			assertTrue(fieldValue.matches("^\\u001b\\[[0-9a-fA-F]+(;[0-9a-fA-F]+)?m?$"), String.format("Ansi.%s is not a valid value (%s)", name, fieldValue));

			System.out.println(fieldValue);
		}

	}
}
