package com.jeroensteenbeeke.hyperion.cmdline;


import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class InputTest {
	private static InputStream oldIn;

	private static PrintStream oldOut;

	private static ByteArrayOutputStream byteOut;


	@BeforeAll
	public static void setUp() {
		oldIn = System.in;
		oldOut = System.out;
		System.setOut(new PrintStream(byteOut = new ByteArrayOutputStream()));

	}

	@Test
	@Timeout(3)
	public void testYesNoQuestion() {
		println("0", "3", "horse", "1");
		assertTrue(Input.yesOrNo("Yes or no?"));
		println("2");
		assertFalse(Input.yesOrNo("Yes or no?"));
	}

	@Test
	public void testMultipleChoiceQuestion() {
		Exception except = null;
		try {
			Input.multipleChoice("Are you an elephant?", List.of());
		} catch (IllegalArgumentException e) {
			except = e;
		}
		assertNotNull(except);

		assertTrue(Input.multipleChoice("Are you an elephant", List.of(true)));
	}

	@Test
	public void testOpenQuestion() {
		println("rabbit");
		assertEquals("rabbit", Input.openQuestion("What animal are you?"));
	}

	private void print(String input) {
		System.setIn(new ByteArrayInputStream(input.getBytes()));
	}

	private void println(String... input) {
		String newline = System.getProperty("line.separator");
		print(Arrays.stream(input).collect(
				Collectors.joining(newline, "", newline)));
	}

	@AfterAll
	public static void tearDown() throws Exception {
		System.setIn(oldIn);
		System.setOut(oldOut);
	}
}
