package com.jeroensteenbeeke.hyperion.cmdline;

import com.jeroensteenbeeke.hyperion.color.Ansi;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ratpack.server.RatpackServer;
import ratpack.server.ServerConfig;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.net.URL;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumingThat;

public class OutputTest {
	private static PrintStream oldSysout;

	private static AtomicReference<ByteArrayOutputStream> byteOut;

	private static RatpackServer ratpack;

	@BeforeAll
	public static void setUp() throws Exception {
		oldSysout = System.out;
		byteOut = new AtomicReference<>(new ByteArrayOutputStream());
		System.setOut(new PrintStream(byteOut.get()));
		ratpack =
				RatpackServer.start(server -> server.serverConfig(ServerConfig.builder().port(8765).build())
						.handlers(chain -> {
							chain.get("good", ctx -> ctx.render("I am a successful response!"));
							chain.get("bad", ctx -> ctx.clientError(405));
						}));
	}

	@Test
	public void testColorOutput() {
		assumingThat(Ansi.isAnsiEnabled(), () -> {
			final String input = "I am test-text";

			testInput(input, Ansi.black(), Ansi::black);
			testInput(input, Ansi.red(), Ansi::red);
			testInput(input, Ansi.green(), Ansi::green);
			testInput(input, Ansi.brown(), Ansi::brown);
			testInput(input, Ansi.blue(), Ansi::blue);
			testInput(input, Ansi.purple(), Ansi::purple);
			testInput(input, Ansi.cyan(), Ansi::cyan);
			testInput(input, Ansi.darkGray(), Ansi::darkGray);
			testInput(input, Ansi.yellow(), Ansi::yellow);
			testInput(input, Ansi.lightBlue(), Ansi::lightBlue);
			testInput(input, Ansi.white(), Ansi::white);
		});
	}

	@Test
	public void testOutputFunction() {
		assumingThat(Ansi.isAnsiEnabled(), () -> {
			final String newline = System.getProperty("line.separator");

			Util.assertOutput(byteOut, "[" + Ansi.cyan() + "++++" + Ansi.reset() + "] Test" + newline, "Test", Output::header);
			Util.assertOutput(byteOut, "[" + Ansi.cyan() + "info" + Ansi.reset() + "] Test" + newline, "Test", Output::info);
			Util.assertOutput(byteOut, "[" + Ansi.green() + " ok " + Ansi.reset() + "] Test" + newline, "Test", Output::ok);
			Util.assertOutput(byteOut, "[" + Ansi.yellow() + " q? " + Ansi.reset() + "] Test" + newline, "Test", Output::question);
			Util.assertOutput(byteOut, "[" + Ansi.brown() + "warn" + Ansi.reset() + "] Test" + newline, "Test", Output::warn);
			Util.assertOutput(byteOut, "[" + Ansi.red() + "fail" + Ansi.reset() + "] Test" + newline, "Test", Output::fail);
			Util.assertOutput(byteOut, "[" + Ansi.purple() + "done" + Ansi.reset() + "] Test" + newline, "Test", Output::done);
			Util.assertOutput(byteOut, "[" + Ansi.lightBlue() + "1" + Ansi.reset() + "] Test" + newline, "Test", i -> Output.option(1, 1,
					i));
		});
	}

	@Test
	public void testSuccessfulDownload() {
		assumingThat(Ansi.isAnsiEnabled(), () -> {
			final String newline = System.getProperty("line.separator");

			File target = File.createTempFile("target", ".txt");

			URL url = new URL("http://localhost:8765/good");
			Util.assertOutput(byteOut,
					Ansi.reset() + String.format("Downloading %-160s", url) + " [" +
							Ansi.green() + "++++++++++++++++++++++++++++++" + Ansi.reset() + "]" + newline + "[" +
							Ansi.green() + " ok " + Ansi.reset() +
							"] Downloaded " + url + newline, url,
					u -> assertTrue(FileFetcher.bar(u, target).isOk()));
			Util.assertOutput(byteOut,
					Ansi.reset() + String.format("Downloading %-160s", url) + " [" + Ansi.green() +
							rotatify("-\\|/-\\|/-\\|/-\\|/-\\|/-\\|/-\\|/-\\|") + Ansi.reset() + "]" + newline +
							"[" +
							Ansi.green() + " ok " + Ansi.reset() +
							"] Downloaded " + url + newline, url,
					u -> assertTrue(FileFetcher.rotator(u, target).isOk()));

			assertTrue(FileFetcher.invisible(url, target).isOk());

			target.deleteOnExit();
		});
	}

	private String rotatify(String input) {
		StringBuilder sb = new StringBuilder();

		for (char c : input.toCharArray()) {
			if (sb.length() > 0) {
				sb.append(Output.KEY_LEFT);
			}
			sb.append(c);
		}

		return sb.toString();
	}

	@Test
	public void testFailedDownload() throws Exception {
		assumingThat(Ansi.isAnsiEnabled(), () -> {
			final String newline = System.getProperty("line.separator");

			File target = File.createTempFile("target", ".txt");

			URL url = new URL("http://localhost:8765/bad");
			try {
				Util.assertOutput(byteOut,
						"Ratpack started (development) for http://localhost:8765\n" +
								Ansi.reset() + String.format("Downloading %-160s", url) + " [" +
								Ansi.green() + Ansi.red() + "!" + Ansi.reset() + "]" + newline + "[" +
								Ansi.red() + "fail" + Ansi.reset() +
								"] Failed to download http://localhost:8765/bad" + newline, url,
						u -> assertFalse(FileFetcher.bar(u, target).isOk()));
			} catch (AssertionError e) {
				Util.assertOutput(byteOut,
						Ansi.reset() + String.format("Downloading %-160s", url) + " [" +
								Ansi.green() + Ansi.red() + "!" + Ansi.reset() + "]" + newline + "[" +
								Ansi.red() + "fail" + Ansi.reset() +
								"] Failed to download http://localhost:8765/bad" + newline, url,
						u -> assertFalse(FileFetcher.bar(u, target).isOk()));
			}
			Util.assertOutput(byteOut,
					Ansi.reset() + String.format("Downloading %-160s", url) + " [" +
							Ansi.green() + "-" + Ansi.red() + "!" + Ansi.reset() + "]" + newline + "[" +
							Ansi.red() + "fail" + Ansi.reset() +
							"] Failed to download http://localhost:8765/bad" + newline, url,
					u -> assertFalse(FileFetcher.rotator(u, target).isOk()));
			assertFalse(FileFetcher.invisible(url, target).isOk());
		});
	}


	private void testInput(String input, String colorConstant, Function<String, String> colorizeFunction) {
		assertEquals(colorConstant + input + Ansi.reset(), colorizeFunction.apply(input));
	}

	@AfterAll
	public static void tearDown() throws Exception {
		System.setOut(oldSysout);
		ratpack.stop();
	}


}
