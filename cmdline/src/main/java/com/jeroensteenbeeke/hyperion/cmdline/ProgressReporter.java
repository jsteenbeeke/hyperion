package com.jeroensteenbeeke.hyperion.cmdline;

import org.jetbrains.annotations.NotNull;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

final class ProgressReporter {
	static final long SEGMENTSIZE = 30;

	static void copyStream(@NotNull InputStream in, @NotNull OutputStream out, long total) throws IOException {
		int data;
		long count = 0L;
		long perc = 0;

		while ((data = in.read()) != -1) {
			out.write(data);

			count++;

			long np = SEGMENTSIZE * count / total;

			while (perc < np) {
				System.out.print("+");
				perc++;
			}
		}
		out.flush();
	}

}
