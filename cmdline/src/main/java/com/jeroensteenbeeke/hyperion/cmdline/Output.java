/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cmdline;

import com.jeroensteenbeeke.hyperion.color.Ansi;

import org.jetbrains.annotations.NotNull;
import java.util.function.Function;

/**
 * Utility class for outputting colored text. This class contains two types of methods: status methods and translator
 * methods. A status method outputs a given text status (formatable like String.format) with a predefined 4 character
 * prefix
 * between brackets. A translator method simply takes the given input and gives it the expected color
 */
public final class Output {
	static final String KEY_LEFT = "\u001b[1D";

	private Output() {

	}

	/**
	 * Status method that prefixes the text with 4 cyan-colored + symbols
	 *
	 * @param string  The string (format) to show
	 * @param objects Parameters to the String format
	 */
	public static void header(@NotNull String string, @NotNull Object... objects) {
		reportAction(Ansi::cyan, "++++", string, objects);
	}

	/**
	 * Status method that prefixes the text with the word "info" in cyan
	 *
	 * @param string  The string (format) to show
	 * @param objects Parameters to the String format
	 */
	public static void info(@NotNull String string, @NotNull Object... objects) {
		reportAction(Ansi::cyan, "info", string, objects);
	}

	/**
	 * Status method that prefixes the text with the word "ok" in green, pre- and postfixed with a single space
	 *
	 * @param string  The string (format) to show
	 * @param objects Parameters to the String format
	 */
	public static void ok(@NotNull String string, @NotNull Object... objects) {
		reportAction(Ansi::green, " ok ", string, objects);
	}

	/**
	 * Status method that prefixes the text with the text "q?" in yellow, pre- and postfixed with a single space
	 *
	 * @param string  The string (format) to show
	 * @param objects Parameters to the String format
	 */
	public static void question(@NotNull String string, @NotNull Object... objects) {
		reportAction(Ansi::yellow, " q? ", string, objects);
	}

	/**
	 * Status method that prefixes the text with the word "warn" in brown
	 *
	 * @param string  The string (format) to show
	 * @param objects Parameters to the String format
	 */
	public static void warn(@NotNull String string, @NotNull Object... objects) {
		reportAction(Ansi::brown, "warn", string, objects);
	}

	/**
	 * Status method that prefixes the text with the word "fail" in red
	 *
	 * @param string  The string (format) to show
	 * @param objects Parameters to the String format
	 */
	public static void fail(@NotNull String string, @NotNull Object... objects) {
		reportAction(Ansi::red, "fail", string, objects);
	}

	/**
	 * Status method that prefixes the text with the word "done" in purple
	 *
	 * @param string  The string (format) to show
	 * @param objects Parameters to the String format
	 */
	public static void done(@NotNull String string, @NotNull Object... objects) {
		reportAction(Ansi::purple, "done", string, objects);
	}

	/**
	 * Status method that outputs the given text as an option, prefixed with an option
	 * number and a number of leading zeroes to make the number of digits match the maximum value
	 *
	 * @param index  The number to display with this option
	 * @param max    The maximum number possible for all options
	 * @param option The text belonging to this option
	 */
	public static void option(int index, int max, @NotNull String option) {
		String format = "%0".concat(Integer.toString(Integer.toString(max).length())).concat("d");
		reportAction(Ansi::lightBlue, String.format(format, index), option);

	}

	private static void reportAction(Function<String,String> ansi, String status,
									 String message, Object... objects) {
		System.out.print("[");
		System.out.print(ansi.apply(status));
		System.out.print("] ");

		System.out.printf(message, objects);
		System.out.println();
	}


}
