/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cmdline;

import com.jeroensteenbeeke.hyperion.cmdline.arguments.*;
import com.jeroensteenbeeke.lux.TypedResult;

import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import org.jetbrains.annotations.NotNull;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Class for parsing command-line arguments
 */
public class Arguments {

	private final String programName;

	private final List<Argument<?>> requiredArguments;

	private final List<Argument<?>> optionalArguments;


	private Arguments(
			@NotNull String programName, @NotNull List<Argument<?>> requiredArguments,
			@NotNull List<Argument<?>> optionalArguments) {
		this.programName = programName;
		this.requiredArguments = requiredArguments;
		this.optionalArguments = optionalArguments;
	}

	/**
	 * Prints the proper usage of the program represented by this argument specification
	 *
	 * @param target The PrintStream to print the usage to
	 */
	public void printUsage(@NotNull PrintStream target) {
		target.println("Usage:");
		target.print("\t");
		target.print(programName);
		for (Argument<?> a : requiredArguments) {
			target.printf(" %s", a.getName());
		}
		for (Argument<?> a : optionalArguments) {
			target.printf(" [%s]", a.getName());
		}
		target.println();
		target.println("Parameters:");
		for (Argument<?> a : requiredArguments) {
			target.printf("\t%-15s (%s)\n\t\t%s", a.getName(), a.getType(), a.getDescription());
			target.println();
		}
		for (Argument<?> a : optionalArguments) {
			target.printf("\t%-15s (%s, optional)\n\t\t%s", a.getName(), a.getType(), a.getDescription());
			target.println();
		}
	}

	/**
	 * Checks if the given set of arguments conforms to this argument-specification
	 *
	 * @param args The arguments to check
	 * @return A TypedResult either the converted (parsed) arguments, or the reason they could not be parsed
	 */
	@NotNull
	public TypedResult<Map<String, Object>> checkValidity(@NotNull String[] args) {
		int min = requiredArguments.size();
		int max = min + optionalArguments.size();

		TypedResult<Map<String, Object>> result = TypedResult.ok(HashMap.empty());

		List<String> joined = joinArguments(args);

		if (joined.size() >= min && joined.size() <= max) {
			for (int i = 0; i < min; i++) {
				String argument = joined.get(i);

				Argument<?> expected = requiredArguments.get(i);
				result =
						expected.parse(argument)
								.foldResult(result, (r, t) -> r.put(expected.getName(), t));
				if (!result.isOk()) {
					return result;
				}
			}

			for (int i = 0; i < optionalArguments.size(); i++) {
				int index = min + i;
				if (index >= joined.size()) {
					break;
				}
				String argument = joined.get(min + i);
				Argument<?> expected = optionalArguments.get(i);

				result =
						expected.parse(argument)
								.foldResult(result, (r, t) -> r.put(expected.getName(), t));
				if (!result.isOk()) {
					return result;
				}
			}

			return result;
		}

		return TypedResult.fail("Invalid number of arguments");
	}

	private List<String> joinArguments(String[] args) {
		List<String> result = new LinkedList<>();
		StringBuilder buffer = new StringBuilder();

		for (int i = 0; i < args.length; i++) {
			String next = args[i];

			if (next == null) {
				result.add(null);
				continue;
			}

			if (buffer.length() == 0) {
				if (next.startsWith("\"")) {
					buffer.append(next.substring(1));
				} else {
					result.add(next);
				}
			} else {
				if (next.endsWith("\"") && !next.endsWith("\\\"")) {
					buffer.append(' ').append(next.substring(0, next.length() - 1));
					result.add(buffer.toString());
					buffer = new StringBuilder();
				} else {
					buffer.append(' ').append(next.replace("\\\"", "\""));
				}
			}
		}

		if (buffer.length() > 0) {
			result.add("\"".concat(buffer.toString()));
		}

		return result;

	}

	/**
	 * Checks the given arguments for validity, executing the first consumer if successful, or the second one if it fails
	 *
	 * @param args    The arguments to use
	 * @param program The action to perform if the arguments parse correctly
	 * @param onError The action to perform if an error occurs
	 */
	public void run(
			@NotNull String[] args, @NotNull Consumer<Map<String, Object>> program, @NotNull Consumer<String> onError) {
		checkValidity(args).ifNotOk(onError).asOptional().ifPresent(program);
	}

	/**
	 * Create a new builder for creating an argument specification
	 *
	 * @return A builder for adding required arguments
	 */
	@NotNull
	public static RequiredArgumentsBuilder builder() {
		return new RequiredArgumentsBuilder();
	}

	/**
	 * Base class for building a set of arguments. Defines all base logic for adding arguments, while not enforcing
	 * any rules about the arguments being required/optional
	 *
	 * @param <T> The type of ArgumentsBuilder to return between steps
	 */
	public static abstract class ArgumentsBuilder<T extends ArgumentsBuilder<T>> {
		/**
		 * Adds a String argument
		 *
		 * @param name        The name of the argument
		 * @param description The description of the argument
		 * @return The current builder
		 */
		public T withStringArgument(String name, String description) {
			return addArgument(new StringArgument(name, description));
		}

		/**
		 * Adds a integer argument
		 *
		 * @param name        The name of the argument
		 * @param description The description of the argument
		 * @return The current builder
		 */
		public T withIntArgument(String name, String description) {
			return addArgument(new IntegerArgument(name, description));
		}

		/**
		 * Adds a boolean argument
		 *
		 * @param name        The name of the argument
		 * @param description The description of the argument
		 * @return The current builder
		 */
		public T withBooleanArgument(String name, String description) {
			return addArgument(new BooleanArgument(name, description));
		}

		/**
		 * Adds a long argument
		 *
		 * @param name        The name of the argument
		 * @param description The description of the argument
		 * @return The current builder
		 */
		public T withLongArgument(String name, String description) {
			return addArgument(new LongArgument(name, description));
		}

		/**
		 * Adds a file argument. File arguments are required to point to existing files
		 *
		 * @param name        The name of the argument
		 * @param description The description of the argument
		 * @return The current builder
		 */
		public T withFileArgument(String name, String description) {
			return addArgument(new FileArgument(name, description));
		}

		/**
		 * Adds a folder argument. Folder arguments are not required to point to existing directories
		 *
		 * @param name        The name of the argument
		 * @param description The description of the argument
		 * @return The current builder
		 */
		public T withFolderArgument(String name, String description) {
			return addArgument(new FolderArgument(name, description, false));
		}

		/**
		 * Adds a URL argument
		 *
		 * @param name        The name of the argument
		 * @param description The description of the argument
		 * @return The current builder
		 */
		public T withUrlArgument(String name, String description) {
			return addArgument(new UrlArgument(name, description));
		}

		/**
		 * Adds a folder argument, with the added requirement that the directory already exists
		 *
		 * @param name        The name of the argument
		 * @param description The description of the argument
		 * @return The current builder
		 */
		public T withExistingFolderArgument(String name, String description) {
			return addArgument(new FolderArgument(name, description, true));
		}

		/**
		 * Construct the Arguments object
		 *
		 * @param programName The name of the program that uses these arguments (used for printing usage)
		 * @return An Arguments object
		 */
		public Arguments forProgram(String programName) {
			return new Arguments(programName, getRequired(), getOptional());
		}

		/**
		 * Add an argument to the specification
		 *
		 * @param argument The argument to add
		 * @return The current builder
		 */
		protected abstract T addArgument(Argument<?> argument);

		/**
		 * Get the list of required arguments currently in the builder
		 *
		 * @return A list of required arguments
		 */
		protected abstract List<Argument<?>> getRequired();

		/**
		 * Get the list of optional arguments currently in the builder
		 *
		 * @return A list of optional arguments
		 */
		protected abstract List<Argument<?>> getOptional();
	}

	/**
	 * ArgumentsBuilder that marks all added arguments as required. Can also be used to first specify a number of
	 * required arguments that should then be followed by optional arguments
	 */
	public static class RequiredArgumentsBuilder extends ArgumentsBuilder<RequiredArgumentsBuilder> {
		private final List<Argument<?>> arguments;

		private RequiredArgumentsBuilder() {
			this.arguments = new ArrayList<>();

		}

		@Override
		protected RequiredArgumentsBuilder addArgument(Argument<?> argument) {
			arguments.add(argument);
			return this;
		}

		@Override
		protected List<Argument<?>> getRequired() {
			return arguments;
		}

		@Override
		protected List<Argument<?>> getOptional() {
			return List.of();
		}

		/**
		 * Indicate that any further arguments added are optional
		 *
		 * @return A new builder for adding optional arguments
		 */
		public OptionalArgumentBuilder andOptionally() {
			return new OptionalArgumentBuilder(arguments);
		}

	}

	/**
	 * ArgumentsBuilder that marks all added arguments as optional. Generally gets constructed with
	 * an existing list of required arguments to preceed the optional ones.
	 */
	public static class OptionalArgumentBuilder extends ArgumentsBuilder<OptionalArgumentBuilder> {
		private final List<Argument<?>> required;

		private final List<Argument<?>> optional;

		private OptionalArgumentBuilder(List<Argument<?>> required) {
			super();
			this.required = required;
			this.optional = new ArrayList<>();
		}

		@Override
		protected OptionalArgumentBuilder addArgument(Argument<?> argument) {
			optional.add(argument);
			return this;
		}

		@Override
		protected List<Argument<?>> getOptional() {
			return optional;
		}

		@Override
		protected List<Argument<?>> getRequired() {
			return required;
		}


	}
}
