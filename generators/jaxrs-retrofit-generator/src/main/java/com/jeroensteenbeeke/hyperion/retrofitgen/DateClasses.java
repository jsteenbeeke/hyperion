package com.jeroensteenbeeke.hyperion.retrofitgen;

/**
 * Enum indicator to specify which date types to use
 */
public enum DateClasses {
	/**
	 * Use java.time.* classes
	 */
	JavaTime("java.time"),
	/**
	 * Use org.threeten.bp.* classes
	 */
	ThreeTenBackport("org.threeten.bp");

	private final String packageName;

	/**
	 * Constructor
	 * @param packageName The package the classes are in
	 */
	DateClasses(String packageName) {
		this.packageName = packageName;
	}

	/**
	 * @return The package name for this type
	 */
	public String getPackageName() {
		return packageName;
	}
}
