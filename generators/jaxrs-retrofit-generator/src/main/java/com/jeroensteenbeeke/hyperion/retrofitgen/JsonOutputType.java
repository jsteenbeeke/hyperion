package com.jeroensteenbeeke.hyperion.retrofitgen;


import io.swagger.v3.oas.annotations.media.Schema;

import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.Set;

/**
 * Enum for the type of annotations to use for annotating model classes
 */
public enum JsonOutputType {
	/**
	 * Use GSON annotations
	 */
	GSON {
		@Override
		public Set<String> getAdditionalImports() {
			return Set.of("com.google.gson.annotations.SerializedName");
		}

		@Override
		public void annotateField(Field field, PrintWriter printWriter) {
			String name = field.getName();

			Schema prop = field.getAnnotation(Schema.class);

			if (prop != null) {
				if (!"".equals(prop.name())) {
					name = prop.name();
				}
			}
			printWriter.printf("\t@SerializedName(\"%s\")", name);
			printWriter.println();

		}
	},
	/**
	 * Use Jackson annotations
	 */
	JACKSON {
		@Override
		public Set<String> getAdditionalImports() {
			return Set.of("com.fasterxml.jackson.annotation.JsonProperty");
		}

		@Override
		public void annotateField(Field field, PrintWriter printWriter) {
			String name = field.getName();

			Schema prop = field.getAnnotation(Schema.class);

			if (prop != null) {
				if (!"".equals(prop.name())) {
					name = prop.name();
				}
			}
			printWriter.printf("\t@JsonProperty(\"%s\")", name);
			printWriter.println();
		}
	};

	/**
	 * Returns a set of additional import statements that should be added to the generated file
	 *
	 * @return A set of fully qualified domain names
	 */
	public abstract Set<String> getAdditionalImports();

	/**
	 * Adds annotations to the given field
	 *
	 * @param field       The field to annotate
	 * @param printWriter The printwriter being used to generate the class
	 */
	public abstract void annotateField(Field field, PrintWriter printWriter);
}
