package com.jeroensteenbeeke.hyperion.retrofitgen;

import org.jetbrains.annotations.NotNull;

import java.util.Optional;

/**
 * The type of object returned by service methods. Allows project to choose between regular Retrofit/OkHttp or RXJava
 */
public enum ServiceMethodOutputType {
	/**
	 * Retrofit Call
	 */
	CALL {
		@Override
		@NotNull
		public String getReturnType() {
			return "Call";
		}

		@Override
		@NotNull
		public String getImport() {
			return "import retrofit2.Call";
		}

		@Override
		@NotNull
		public String getVoidType() {
			return "Void";
		}

		@Override
		public boolean isVoidTypeWrapped() {
			return true;
		}

		@Override
		@NotNull
		public Optional<String> getVoidTypeImport() {
			return Optional.empty();
		}

		@Override
		@NotNull
		public String createJavadoc(@NotNull String returnTypeName) {
			return String.format("@return A Call object encapsulating the %s return type", returnTypeName);
		}
	},
	/**
	 * RXJava observable
	 */
	OBSERVABLE {
		@Override
		@NotNull
		public String getReturnType() {
			return "Observable";
		}

		@Override
		@NotNull
		public String getImport() {
			return "io.reactivex.rxjava3.core.Observable";
		}

		@Override
		public boolean isVoidTypeWrapped() {
			return false;
		}

		@Override
		@NotNull
		public String getVoidType() {
			return "Completable";
		}

		@Override
		@NotNull
		public Optional<String> getVoidTypeImport() {
			return Optional.of("io.reactivex.rxjava3.core.Completable");
		}


		@Override
		@NotNull
		public String createJavadoc(@NotNull String returnTypeName) {
			return String.format("@return An RxJava Observable that will eventually yield a %s", returnTypeName);
		}

	};

	/**
	 * The return type of the method
	 * @return An identifier
	 */
	@NotNull
	public abstract String getReturnType();

	/**
	 * The import required for this output type
	 * @return A fully qualified domain name
	 */
	@NotNull
	public abstract String getImport();

	/**
	 * Returns the type to use for void values
	 * @return The type to use for Void
	 */
	@NotNull
	public abstract String getVoidType();

	/**
	 * Indicates whether or not the normal return type of this output type should be applied
	 * @return {@code true} if the type returns by #getReturnType should be applied, {@code false} otherwhise
	 */
	public abstract boolean isVoidTypeWrapped();

	/**
	 * Returns the import to use for void values
	 * @return The FQDN to use for Void
	 */
	@NotNull
	public abstract Optional<String> getVoidTypeImport();

	/**
	 * Generates JavaDoc for the given return type
	 * @param returnTypeName The type returned by the current implementation
	 * @return A snippet of Javadoc
	 */
	@NotNull
	public abstract String createJavadoc(@NotNull String returnTypeName);
}
