/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version. <p> This program is distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details. <p> You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.retrofitgen;

import com.github.codeteapot.maven.plugin.testing.MavenPluginContext;
import com.github.codeteapot.maven.plugin.testing.MavenPluginExecutionException;
import com.github.codeteapot.maven.plugin.testing.junit.jupiter.MavenPluginExtension;
import com.google.testing.compile.Compilation;
import com.google.testing.compile.JavaFileObjects;
import com.jeroensteenbeeke.andalite.java.analyzer.AnalyzedClass;
import com.jeroensteenbeeke.andalite.java.analyzer.AnalyzedMethod;
import com.jeroensteenbeeke.andalite.java.analyzer.AnalyzedSourceFile;
import com.jeroensteenbeeke.andalite.java.analyzer.ClassAnalyzer;
import com.jeroensteenbeeke.andalite.java.transformation.returntypes.NamedReturnType;
import com.jeroensteenbeeke.hyperion.maven.test.HyperionMavenConfiguration;
import com.jeroensteenbeeke.lux.TypedResult;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.util.Locale;

import static com.google.testing.compile.CompilationSubject.assertThat;
import static com.google.testing.compile.Compiler.javac;
import static com.jeroensteenbeeke.hyperion.maven.test.MavenFileUtil.getTestFile;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MavenPluginExtension.class)
public class GeneratorMojoTest {

	@Test
	public void testGenerateInterfaces(MavenPluginContext context) throws
		MalformedURLException, MavenPluginExecutionException {
		File pom = getTestFile("src/test/resources/unit/testproject/pom.xml");

		File resourceFile = getTestFile(
			"src/test/resources/unit/testproject/target/generated-sources/retrofit/com/jeroensteenbeeke/hyperion"
			+
			"/retrofitgen/RFFakeResource.java");
		File modelFile = getTestFile(
			"src/test/resources/unit/testproject/target/generated-sources/retrofit/com/jeroensteenbeeke/hyperion"
			+
			"/retrofitgen/RFFake.java");
		File queryFile = getTestFile(
			"src/test/resources/unit/testproject/target/generated-sources/retrofit/com/jeroensteenbeeke/hyperion"
			+
			"/retrofitgen/RFFake_Query.java");

		if (resourceFile.exists()) {
			assertTrue(resourceFile.delete());
		}

		if (modelFile.exists()) {
			assertTrue(modelFile.delete());
		}

		if (queryFile.exists()) {
			assertTrue(queryFile.delete());
		}

		File testRoot = pom.getParentFile();

		HyperionMavenConfiguration.configure(context, testRoot);
		context.goal("generate").set(MavenPluginContext.configuration()
			.set(MavenPluginContext.configurationValue("basePackage", "com.jeroensteenbeeke.hyperion.retrofitgen"))
			.set(MavenPluginContext.configurationValue("resourceClassPrefix", "RF"))
			.set(MavenPluginContext.configurationValue("queryClassPrefix", "RF"))
			.set(MavenPluginContext.configurationValue("modelClassPrefix", "RF"))
			.set(MavenPluginContext.configurationValue("generatedSourcesSubdir", "retrofit"))
			.set(MavenPluginContext.configurationValue("serviceMethodOutputType", "OBSERVABLE"))
		).execute();

		try {
			assertTrue(resourceFile.exists(), "RFFakeResource was generated");
			assertTrue(modelFile.exists(), "RFFake was generated");
			assertTrue(queryFile.exists(), "RFFake_Query was generated");

			TypedResult<AnalyzedSourceFile> result = new ClassAnalyzer(
				resourceFile).analyze();

			if (!result.isOk()) {
				throw new AssertionError("Could not parse RFFakeResource: " + result.getMessage());
			}

			AnalyzedSourceFile source = result.getObject();

			boolean methodsProperlyAnnotated = source.getInterfaces()
				.stream()
				.filter(i -> i.getDenominationName().equals("RFFakeResource"))
				.flatMap(i -> i.getMethods().stream())
				.allMatch(
					m -> m.hasAnnotation("GET")
						 || m.hasAnnotation("POST")
						 || m.hasAnnotation("PUT")
						 || m.hasAnnotation("DELETE")
				);

			assertTrue(methodsProperlyAnnotated,
				"All methods annotated with one of: @GET, @POST, @PUT, @DELETE");

			assertTrue(source.getInterfaces()
					.stream()
					.filter(i -> i.getDenominationName().equals("RFFakeResource"))
					.flatMap(i -> i.getMethods().stream())
					.filter(m -> m.getName().equals("updateFake"))
					.allMatch(m ->
						m.getParameters().stream().filter(p -> p.getName().equals("id"))
							.allMatch(p -> p.hasAnnotation("Path"))
						&&
						m.getParameters().stream()
							.filter(p -> p.getName().equals("payload"))
							.allMatch(p -> p.hasAnnotation("Body")
							) &&
						m.getParameters().stream()
							.filter(p -> p.getName().equals("xAuthToken"))
							.allMatch(p -> p.hasAnnotation("Header")
							)),
				"Method updateFake does not have parameters 'id' and 'payload'");

			result = new ClassAnalyzer(
				modelFile).analyze();
			assertTrue(result.isOk(), "RFFake parsed successfully");
			source = result.getObject();

			MatcherAssert.assertThat(source.getClasses().size(), CoreMatchers.equalTo(1));
			AnalyzedClass rffake = source.getClasses().get(0);

			try (var lines = Files.lines(modelFile.toPath())) {
				lines.forEach(System.out::println);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}

			AnalyzedMethod getBars = rffake.getMethod().withReturnType(new NamedReturnType("List<Bar>")).named("getBars");
			MatcherAssert.assertThat(getBars, notNullValue());
			MatcherAssert.assertThat(getBars.getAnnotation("NotNull"), notNullValue());

			result = new ClassAnalyzer(
				queryFile).analyze();
			assertTrue(result.isOk(), "RFFake_Query parsed successfully");
			source = result.getObject();

			assertTrue(source.getInterfaces().stream()
				.filter(i -> i.getDenominationName().equals("RFFake_Query"))
				.flatMap(i -> i.getMethods().stream())
				.filter(m -> m.getName().equals("toMap")).allMatch(m ->
					m.getParameters().isEmpty() &&
					"Map<String,String>".equals(m.getReturnType().toJavaString())
				), "RFFake_Query has a toMap() method");

			Compilation compilation = javac().withOptions("-source", "17", "-target", "17", "-classpath", System.getProperty("java.class.path"))
				.compile(JavaFileObjects.forResource(modelFile.toURI().toURL()),
					JavaFileObjects.forResource(queryFile.toURI().toURL()),
					JavaFileObjects.forResource(resourceFile.toURI().toURL())
				);

			compilation.diagnostics().asList()
				.forEach(d -> System.out.println(d.getMessage(Locale.ENGLISH)));

			assertThat(
				compilation)
				.succeededWithoutWarnings();
		} finally {
			if (resourceFile.exists()) {
				// expectedFile.deleteOnExit();
			}
		}
	}

}
