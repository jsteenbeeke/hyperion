/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.retrofitgen;

import com.jeroensteenbeeke.hyperion.rest.annotation.Model;
import com.jeroensteenbeeke.hyperion.rest.annotation.Required;
import com.jeroensteenbeeke.hyperion.rest.annotations.Queryable;

import org.jetbrains.annotations.NotNull;
import java.util.List;

@Model
public class Fake {
	@Queryable
	private String title;

	@Required
	private List<Bar> bars;

	public String getTitle() {
		return title;
	}

	public Fake setTitle(String title) {
		this.title = title;
		return this;
	}

	@NotNull
	public List<Bar> getBars() {
		return bars;
	}

	public void setBars(@NotNull List<Bar> bars) {
		this.bars = bars;
	}
}
