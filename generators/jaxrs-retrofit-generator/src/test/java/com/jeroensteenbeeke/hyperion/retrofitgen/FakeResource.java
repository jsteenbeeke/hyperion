/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version. <p> This program is distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details. <p> You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.retrofitgen;

import com.jeroensteenbeeke.hyperion.rest.annotation.NoContent;
import com.jeroensteenbeeke.hyperion.rest.annotation.Returns;
import com.jeroensteenbeeke.hyperion.rest.annotation.ReturnsListOf;
import com.jeroensteenbeeke.hyperion.rest.annotation.ReturnsSetOf;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;

import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Response;

@Tag(name = "FakeResource", description = "Endpoint to work with fake news")
@Path("/fake")
public interface FakeResource {

	@GET
	@Path("/{id}")
	@Operation(description = "Get a Fake object by ID")
	@Returns(Fake.class)
	Response getFake(
			@Parameter(name = "The identifier of the fake", required = true) @PathParam("id") Long id);

	@GET
	@ReturnsListOf(Fake.class)
	Response listFakes(@BeanParam Fake_Query query);

	@GET
	@ReturnsSetOf(Fake.class)
	Response listUniqueFakes();

	@PUT
	@Path("/{id}")
	@Operation(description = "Update the fake")
	@Parameters(
			@Parameter(name = "X-Auth-Token", in = ParameterIn.HEADER, schema = @Schema(type = "string"), example = "dfsdf32r324", required = true)
	)
	@Returns(Fake.class)
	Response updateFake(
			@Parameter(description = "The identifier of the fake", required = true) @PathParam("id") Long id,
			Fake fake);

	@POST
	@Parameters(
			@Parameter(name = "X-Auth-Token", in = ParameterIn.HEADER, schema = @Schema(type = "string"), example = "dfsdf32r324")
	)
	@Returns(Fake.class)
	Response createFake(Fake fake);

	@DELETE
	@Path("/{id}")
	@NoContent
	Response deleteFake(
			@Parameter(description = "The identifier of the fake", required = true) @PathParam("id") Long id);

	@GET
	@Path("/simplesearch")
	@Operation(description = "Search for fakes matching the given query")
	@ReturnsListOf(Fake.class)
	Response doSimpleSearch(
			@Parameter(description = "The text to search for", required = true) @QueryParam("query") String query);
}
