package ${package};

<#list imports as import>
import ${import};
</#list>

@Generated(value = "com.jeroensteenbeeke.hyperion.reflectable.generator.ReflectableGenerator",
    date = "${generationTime}")
public class ${className} {
<#list elements as element>
    public static final String ${element.constantName()} = "${element.constantValue()}";

</#list>
}
