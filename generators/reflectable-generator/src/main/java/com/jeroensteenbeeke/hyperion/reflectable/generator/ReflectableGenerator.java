package com.jeroensteenbeeke.hyperion.reflectable.generator;

import com.jeroensteenbeeke.hyperion.annotation.Reflectable;
import com.jeroensteenbeeke.hyperion.apt.core.data.MethodDescriptor;
import com.jeroensteenbeeke.hyperion.apt.core.processor.extended.ExtendedClassWithFields;
import com.jeroensteenbeeke.hyperion.apt.core.processor.extended.ExtendedFieldDescriptor;
import com.jeroensteenbeeke.hyperion.apt.core.processor.extended.ExtendedHyperionClassProcessor;
import com.jeroensteenbeeke.hyperion.mojo.core.GeneratedType;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import io.vavr.collection.Array;
import io.vavr.collection.List;
import org.jetbrains.annotations.NotNull;

import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;


/**
 * Annotation processor for generating method and field information to use in reflection scenarios
 */
@SupportedAnnotationTypes(ReflectableGenerator.REFLECTABLE)
@SupportedSourceVersion(SourceVersion.RELEASE_21)
public class ReflectableGenerator extends ExtendedHyperionClassProcessor {
	static final String REFLECTABLE = "com.jeroensteenbeeke.hyperion.annotation.Reflectable";

	private static final Set<String> ENTITY_ANNOTATIONS = Set.of(
		"javax.persistence.Entity",
		"jakarta.persistence.Entity"
	);

	private GeneratedType generatedType;

	@Override
	protected String getGeneratorName() {
		return "ReflectableGenerator";
	}

	@Override
	protected List<Class<? extends Annotation>> getRequiredAnnotations() {
		return List.of(Reflectable.class);
	}

	@Override
	protected void generateCode(@NotNull ExtendedClassWithFields classDescriptor) {
		boolean hasEntity = !classDescriptor.getAnnotations()
			.filter(a -> ENTITY_ANNOTATIONS.contains(a.getType().getFQDN()))
			.isEmpty();

		Configuration configuration = new Configuration(Configuration.VERSION_2_3_32);
		configuration.setClassForTemplateLoading(ReflectableGenerator.class, "templates");
		configuration.setDefaultEncoding("UTF-8");

		// Skip entities, or we'll clash with Hibernate Metamodel
		if (!hasEntity) {
			checkInitialization();

			Array<ConstRef> elements = classDescriptor.getMethods().map(MethodDescriptor::getName).sorted().distinct()
				.map(md -> new ConstRef("METHOD_" + determineConstantName(md), md)).foldLeft(Array.empty(), Array::append);
			elements = classDescriptor.getFields().map(ExtendedFieldDescriptor::getName).sorted().distinct()
				.map(fd -> new ConstRef("FIELD_" + determineConstantName(fd), fd)).foldLeft(elements, Array::append);

			Map<String, Object> dataModel = new HashMap<>();
			dataModel.put("package", classDescriptor.getPackageName());
			dataModel.put("className", classDescriptor.getClassName() + "_");
			dataModel.put("elements", elements.toJavaList());
			dataModel.put("imports", generatedType.getImports());
			dataModel.put("generationTime", DateTimeFormatter.ISO_DATE_TIME.format(LocalDateTime.now()));

			try {
				JavaFileObject classFile = processingEnv
					.getFiler()
					.createSourceFile(classDescriptor.getFQDN() + "_");

				try (Writer out = classFile.openWriter()) {
					Template template = configuration.getTemplate("reflectable.ftl");

					template.process(dataModel, out);
				}
			} catch (IOException | TemplateException e) {
				throw new RuntimeException(e);
			}
		}
	}

	private String determineConstantName(String input) {
		StringBuilder result = new StringBuilder();
		boolean previousIsLowerCase = false;

		for (char c : input.toCharArray()) {
			if (Character.isUpperCase(c) && previousIsLowerCase) {
				result.append("_");
			}

			result.append(Character.toUpperCase(c));

			previousIsLowerCase = Character.isLowerCase(c);
		}

		return result.toString();
	}

	private void checkInitialization() {
		if (generatedType == null) {
			generatedType = Optional.ofNullable(processingEnv.getOptions().get("generatedType"))
				.map(GeneratedType::valueOf).orElse(GeneratedType.JakartaEE);
		}
	}

}
