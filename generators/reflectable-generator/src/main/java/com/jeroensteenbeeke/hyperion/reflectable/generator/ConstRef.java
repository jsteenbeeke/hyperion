package com.jeroensteenbeeke.hyperion.reflectable.generator;

import org.jetbrains.annotations.NotNull;

/**
 * References a constant
 * @param constantName The name of the constant
 * @param constantValue The value of the constant
 */
public record ConstRef(@NotNull String constantName, @NotNull String constantValue) {
}
