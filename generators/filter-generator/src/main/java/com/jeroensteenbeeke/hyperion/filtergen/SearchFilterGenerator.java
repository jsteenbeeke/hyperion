package com.jeroensteenbeeke.hyperion.filtergen;

import com.jeroensteenbeeke.hyperion.apt.core.processor.basic.BasicClassWithFields;
import com.jeroensteenbeeke.hyperion.apt.core.processor.basic.BasicFieldDescriptor;
import com.jeroensteenbeeke.hyperion.apt.core.processor.basic.BasicHyperionClassProcessor;
import com.jeroensteenbeeke.hyperion.solstice.spring.db.EnableSolstice;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import io.vavr.collection.Seq;
import org.jetbrains.annotations.NotNull;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import jakarta.persistence.Entity;
import javax.tools.Diagnostic.Kind;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.util.*;
import java.util.function.Predicate;

/**
 * Annotation processor for generating search filter classes for entities
 */
@SupportedAnnotationTypes({"jakarta.persistence.Entity", "com.jeroensteenbeeke.hyperion.solstice.spring.db.EnableSolstice"})
@SupportedSourceVersion(SourceVersion.RELEASE_21)
public class SearchFilterGenerator extends BasicHyperionClassProcessor {
	private static final String CUSTOM_FILTER_ANNOTATION =
			"com.jeroensteenbeeke.hyperion.meld.filter.CustomFilter";

	private static final String CUSTOM_FILTERS_ANNOTATION =
			"com.jeroensteenbeeke.hyperion.meld.filter.CustomFilters";

	private static final io.vavr.collection.Set<String> FORBIDDEN_TYPES = io.vavr.collection.HashSet
			.of("java.sql.Blob");


	private Context context;

	private Messager messager;

	@Override
	public synchronized void init(ProcessingEnvironment processingEnv) {
		if (context == null) {
			this.context = new Context(processingEnv);
		}

		this.messager = processingEnv.getMessager();

		processingEnv.getMessager().printMessage(Kind.NOTE, "Initialized SearchFilterGenerator");

		super.init(processingEnv);
	}

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		roundEnv
				.getElementsAnnotatedWith(EnableSolstice.class).stream()
				.findFirst()
				.map(a -> a.getAnnotation(EnableSolstice.class))
				.map(EnableSolstice::entityBasePackage)
				.ifPresent(context::setEntityBasePackage);

		return super.process(annotations, roundEnv);
	}

	@Override
	protected String getGeneratorName() {
		return "com.jeroensteenbeeke.hyperion.filtergen.SearchFilterGenerator";
	}

	@Override
	protected io.vavr.collection.List<Class<? extends Annotation>> getRequiredAnnotations() {
		return io.vavr.collection.List.of(Entity.class);
	}

	@Override
	protected void generateCode(@NotNull BasicClassWithFields data) {
		try {
			final String targetPackage = determineFilterPackage(context, data.getPackageName());
			final String targetClassName = data.getClassName().concat("Filter");
			final String fqdn = String.format("%s.%s", targetPackage, targetClassName);

			messager.printMessage(Kind.NOTE, "Generating " + fqdn);

			JavaFileObject classFile = context.getEnvironment().getFiler().createSourceFile(fqdn);
			Set<String> imports = new TreeSet<>();
			imports.add("com.jeroensteenbeeke.hyperion.meld.SearchFilter");
			imports.add("jakarta.annotation.Generated");
			imports.add("org.jetbrains.annotations.NotNull");

			imports.add(data.getFQDN());
			imports.add(String.format("%s_", data.getFQDN()));
			data.getFields().flatMap(BasicFieldDescriptor::getImports)
					.filter(s -> !s.startsWith("java.lang.") || s.substring(10).contains("."))
					.forEach(imports::add);

			// Fields
			io.vavr.collection.List<FieldData> mappedFields = data
					.getFields()
					.filter(f -> !f.isStatic())
					.filter(f -> !FORBIDDEN_TYPES.contains(f.getType().getFQDN()))
					.flatMap(f -> mapToFields(data.getClassName(), f));
			mappedFields.flatMap(FieldData::getImports).forEach(imports::add);

			Configuration configuration = new Configuration(Configuration.VERSION_2_3_28);
			configuration.setClassForTemplateLoading(SearchFilterGenerator.class, "templates");
			configuration.setDefaultEncoding("UTF-8");

			try (PrintWriter pw = new PrintWriter(classFile.openWriter())) {
				pw.print("package ");
				pw.print(targetPackage);
				pw.println(";");
				pw.println();
				imports.forEach(i -> {
					if (i.indexOf('.') == -1) {
						throw new IllegalArgumentException("Default package imports not allowed");
					}
					pw.print("import ");
					pw.print(i);
					pw.println(";");
				});
				pw.println();
				pw.printf(
						"@Generated(value = \"com.jeroensteenbeeke.hyperion.filtergen" +
								".SearchFilterGenerator\",\n date= \"%s\")",
						new Date());
				pw.println();
				pw.print("public class ");
				pw.print(targetClassName);
				pw.print(" extends SearchFilter <");
				pw.print(data.getClassName());
				pw.print(",");
				pw.print(targetClassName);
				pw.println("> {");
				pw.println();
				pw.println("\tprivate static final long serialVersionUID = 1L;");
				pw.println();


				mappedFields.forEach(field -> {
					final String propertyType = field.getPropertyType();
					final String filterType = field.getFilterType();
					final String classType = field.getClassType();
					final String fieldName = field.getFieldName();
					final String attributeName = field.getAttributeName();

					Map<String, Object> model = new HashMap<>();
					model.put("propertyType", propertyType);
					model.put("propertyName", attributeName);
					model.put("entityType", classType);
					model.put("fieldType", filterType);
					model.put("filterType", targetClassName);
					model.put("fieldName", fieldName);
					model.put("isEntity", field.isEntity());
					model.put("isRequired", field.isRequired());
					model.put("idType", field.getIdType());

					if (field.isEntity()) {
						try {
							Template template = configuration.getTemplate("entityfilterfield.ftl");

							template.process(model, pw);
						} catch (TemplateException | IOException e) {
							throw new RuntimeException(e);
						}
					} else {
						try {
							Template template = configuration.getTemplate("regularfilterfield.ftl");

							template.process(model, pw);

						} catch (TemplateException | IOException e) {
							throw new RuntimeException(e);
						}
					}

				});

				// Setters
				mappedFields.forEach(field -> {
					final String propertyType = field.getPropertyType();
					final String filterType = field.getFilterType();
					final String classType = field.getClassType();
					final boolean generified = field.isGenerified();
					final String attributeName = field.getAttributeName();
					final String fieldName = field.getFieldName();

					Map<String, Object> model = new HashMap<>();
					model.put("propertyType", propertyType);
					model.put("propertyName", attributeName);
					model.put("entityType", classType);
					model.put("fieldType", filterType);
					model.put("filterType", targetClassName);
					model.put("fieldName", fieldName);
					model.put("isEntity", field.isEntity());
					model.put("isRequired", field.isRequired());
					model.put("idType", field.getIdType());

					if (field.isEntity()) {
						try {
							Template template = configuration.getTemplate("entityaccessors.ftl");

							template.process(model, pw);

						} catch (TemplateException | IOException e) {
							throw new RuntimeException(e);
						}
					} else {
						try {
							Template template = configuration.getTemplate("regularaccessors.ftl");

							template.process(model, pw);

						} catch (TemplateException | IOException e) {
							throw new RuntimeException(e);
						}
					}


				});

				pw.println("}");
				pw.flush();

			}

			context.getEnvironment().getMessager()
					.printMessage(Kind.NOTE, "Generated " + classFile.getName());

		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private Seq<FieldData> mapToFields(String entityClassName, BasicFieldDescriptor field) {
		return field
				.getAnnotations()
				.find(a -> a.getType().getFQDN().equals(CUSTOM_FILTERS_ANNOTATION))
				.toArray()
				.flatMap(ad -> ad.getAnnotationValues("value").toArray())
				.getOrElse(() -> field
						.getAnnotations()
						.find(a -> a.getType().getFQDN().equals(CUSTOM_FILTER_ANNOTATION))
						.toList())
				.flatMap(ad -> FieldData.ofCustomFilterField(entityClassName, ad, field))
				.appendAll(FieldData.ofRegularField(context, entityClassName, field));
	}

	/**
	 * Determines in which package searchfilters are stored based on the package entities are stored
	 *
	 * @param context       The annotation processor context
	 * @param entityPackage The package entities are stored in
	 * @return The package
	 */
	static String determineFilterPackage(Context context, String entityPackage) {
		String[] parts = entityPackage.split("\\.");

		BaseAndExtra pkg = extractBaseAndExtra(context, parts,
				"entities"::equals);

		return pkg.join("filter");
	}

	private static BaseAndExtra extractBaseAndExtra(
			Context context, String[] parts,
			Predicate<String> condition) {
		List<String> extra = new LinkedList<>();
		List<String> base = new LinkedList<>();

		boolean e = true;

		if (context.getEntityBasePackage() != null) {
			String[] configuredPackage = context.getEntityBasePackage().split("\\.");

			int cutoff = -1;
			for (int i = 0; i < Math.min(configuredPackage.length, parts.length); i++) {
				cutoff = i;
				if (configuredPackage[i].equals(parts[i])) {
					base.add(configuredPackage[i]);
				} else {
					break;
				}
			}

			if (cutoff != -1) {
				extra.addAll(Arrays.asList(parts).subList(cutoff + 1, parts.length));
			}

			return new BaseAndExtra(base, extra);
		}

		for (int i = parts.length - 1; i >= 0; i--) {
			if (condition.test(parts[i])) {
				e = false;
			}

			if (e) {
				extra.add(0, parts[i]);
			} else {
				base.add(0, parts[i]);
			}
		}

		return new BaseAndExtra(base, extra);
	}

	private static final class BaseAndExtra {
		private final List<String> base;

		private final List<String> extra;

		private BaseAndExtra(List<String> base, List<String> extra) {
			super();
			this.base = base;
			this.extra = extra;
		}

		String getBasePackage() {
			return String.join(".", base);
		}

		String getExtraPackages() {
			return String.join(".", extra);
		}

		String join(String... packagesBetween) {
			if (packagesBetween.length == 0) {
				return getBasePackage().concat(".").concat(getExtraPackages());
			}

			if (getExtraPackages().isEmpty()) {
				return getBasePackage().concat(".").concat(
						String.join(".", packagesBetween));
			} else {

				return getBasePackage()
						.concat(".")
						.concat(String.join(".", packagesBetween)).concat(".")
						.concat(getExtraPackages());
			}
		}
	}
}
