    @NotNull
    public ${fieldType}<${entityType},${propertyType}<#if idType??>,${idType}</#if>,${filterType},${propertyType}Filter> ${propertyName}() {
        return setLastUserFilterField(this.${fieldName});
    }

    @NotNull
    public ${filterType} ${propertyName}(${propertyType} value) {
        return setLastUserFilterField(this.${fieldName}).equalTo(value);
    }

    @NotNull
    public ${filterType} ${propertyName}(${propertyType}Filter filter) {
        return setLastUserFilterField(this.${fieldName}).byFilter(filter);
    }

    @NotNull
    public ${fieldType}<${entityType},${propertyType}<#if idType??>,${idType}</#if>,${filterType}, ${propertyType}Filter> or${propertyName?cap_first}() {
        return disjunction(this.${fieldName}.createNew());
    }

    @NotNull
    public ${filterType} or${propertyName?cap_first}(${propertyType} value) {
        return disjunction(this.${fieldName}.createNew()).equalTo(value);
    }

    @NotNull
    public ${filterType} or${propertyName?cap_first}(${propertyType}Filter filter) {
        return disjunction(this.${fieldName}.createNew()).byFilter(filter);
    }

    @NotNull
    public ${fieldType}<${entityType},${propertyType}<#if idType??>,${idType}</#if>,${filterType}, ${propertyType}Filter> and${propertyName?cap_first}() {
        return conjunction(this.${fieldName}.createNew());
    }

    @NotNull
    public ${filterType} and${propertyName?cap_first}(${propertyType} value) {
        return conjunction(this.${fieldName}.createNew()).equalTo(value);
    }

    @NotNull
    public ${filterType} and${propertyName?cap_first}(${propertyType}Filter filter) {
        return conjunction(this.${fieldName}.createNew()).byFilter(filter);
    }
