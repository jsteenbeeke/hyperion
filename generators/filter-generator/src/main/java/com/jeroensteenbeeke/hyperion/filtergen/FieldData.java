package com.jeroensteenbeeke.hyperion.filtergen;


import com.jeroensteenbeeke.hyperion.apt.core.data.*;
import com.jeroensteenbeeke.hyperion.apt.core.processor.basic.BasicFieldDescriptor;
import com.jeroensteenbeeke.hyperion.util.StringUtil;
import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import io.vavr.control.Option;

import org.jetbrains.annotations.NotNull;

class FieldData implements ImportSupplier {

	private static final String ONE_TO_ONE = "jakarta.persistence.OneToOne";

	private static final String MANY_TO_ONE = "jakarta.persistence.ManyToOne";

	public static final String REQUIRED_FILTER_FIELD =
			"com.jeroensteenbeeke.hyperion.meld.filter.RequiredFilterField";


	private final String filterType;

	private final String propertyType;

	private final String classType;

	private final String attributeName;

	private final String idType;

	private final boolean generified;

	private final boolean entity;

	private final String fieldName;

	private final Set<String> imports;

	private final boolean required;

	public FieldData(boolean generified, boolean entity, String filterType,
					 String propertyType, String classType, String attributeName,
					 String fieldName, Set<String> imports, boolean required, String idType) {
		this.generified = generified;
		this.entity = entity;
		this.filterType = filterType;
		this.propertyType = propertyType;
		this.classType = classType;
		this.fieldName = fieldName;
		this.attributeName = attributeName;
		this.imports = imports;
		this.required = required;
		this.idType = idType;
	}

	@NotNull
	@Override
	public Set<String> getImports() {
		return imports;
	}

	public String getFilterType() {
		return filterType;
	}

	public String getPropertyType() {
		return propertyType;
	}

	public String getClassType() {
		return classType;
	}

	public String getFieldName() {
		return fieldName;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public boolean isGenerified() {
		return generified;
	}

	public boolean isEntity() {
		return entity;
	}

	public boolean isRequired() {
		return required;
	}

	public String getIdType() {
		return idType;
	}

	public static Option<FieldData> ofRegularField(@NotNull Context context, @NotNull String targetFilterClassName, @NotNull BasicFieldDescriptor field) {
		return field.getType().visit(new TypeVisitor<>() {
			@NotNull
			@Override
			public Option<FieldData> visit(@NotNull PrimitiveType type) {
				String filterFieldType = type
						.getSymbol()
						.equals("boolean") ? "SimpleFilterField" : "ComparableFilterField";

				String propertyType;
				switch (type.getSymbol()) {
					case "char":
						propertyType = "Character";
						break;
					case "int":
						propertyType = "Integer";
						break;
					default:
						propertyType = StringUtil.capitalizeFirst(type.getSymbol());
				}

				Set<String> imports = HashSet.of("com.jeroensteenbeeke.hyperion.meld.filter." + filterFieldType);

				return Option.some(new FieldData(true, false,
						filterFieldType, propertyType, targetFilterClassName,
						field.getName(), field.getName(), imports, field
						.getAnnotations()
						.find(ad -> ad
								.getType()
								.getFQDN()
								.equals(REQUIRED_FILTER_FIELD))
						.isDefined(), null
				));
			}

			@NotNull
			@Override
			public Option<FieldData> visit(@NotNull GenerifiedType type) {
				return Option.none();
			}

			@NotNull
			@Override
			public Option<FieldData> visit(@NotNull ReferenceType type) {
				boolean entity = field
						.getAnnotations()
						.find(a -> a.getType().getFQDN().equals(MANY_TO_ONE) || a
								.getType()
								.getFQDN()
								.equals(ONE_TO_ONE)).isDefined();

				String filterFieldType;
				String idType = null;

				if (type.getFQDN().equals("java.lang.String")) {
					filterFieldType = "StringFilterField";
				} else if (entity) {
					filterFieldType = "EntityFilterField";
					idType = type.getIdType();
				} else if (type
						.getImplementedInterfaces()
						.find(t -> t.getFQDN().equals("java.lang.Comparable"))
						.isDefined() && !(type instanceof EnumReferenceType)) {
					filterFieldType = "ComparableFilterField";
				} else {
					filterFieldType = "SimpleFilterField";
				}

				Set<String> imports = HashSet.of("com.jeroensteenbeeke.hyperion.meld.filter." + filterFieldType, type.getFQDN());

				if (entity) {
					imports = imports.add(String.format("%s.%sFilter", SearchFilterGenerator.determineFilterPackage(context, type.getPackageName()), type.getClassName()));
					imports = imports.add(type.getFQDN() + "_");
				}

				return Option.some(new FieldData(true, entity,
						filterFieldType, type.getSymbol(), targetFilterClassName,
						field.getName(), field.getName(), imports, field
						.getAnnotations()
						.find(ad -> ad
								.getType()
								.getFQDN()
								.equals(REQUIRED_FILTER_FIELD))
						.isDefined(), idType
				));
			}

			@NotNull
			@Override
			public Option<FieldData> visit(@NotNull ArrayType type) {
				return Option.none();
			}
		});
	}

	public static Option<FieldData> ofCustomFilterField(@NotNull String targetFilterClassName, @NotNull AnnotationDescriptor customFilterField, @NotNull BasicFieldDescriptor field) {
		Option<ReferenceType> type = customFilterField
				.getTypeValue("type")
				.flatMap(t -> t.visit(new TypeVisitor<>() {
					@NotNull
					@Override
					public Option<ReferenceType> visit(@NotNull ReferenceType gt) {
						return Option.some(gt);
					}
				}));

		Option<String> name = customFilterField.getStringValue("name");

		return type
				.toArray()
				.zip(name)
				.headOption()
				.map(t -> new FieldData(false, false, t._1.getSymbol(), null, targetFilterClassName,
						t._2,
						t._2, HashSet.of(t._1.getFQDN()), field
						.getAnnotations()
						.find(ad -> ad
								.getType()
								.getFQDN()
								.equals(REQUIRED_FILTER_FIELD))
						.isDefined(), null));

	}
}
