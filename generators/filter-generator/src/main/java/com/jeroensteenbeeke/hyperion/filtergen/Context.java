package com.jeroensteenbeeke.hyperion.filtergen;

import javax.annotation.processing.ProcessingEnvironment;
import javax.tools.Diagnostic;

class Context {
	private final ProcessingEnvironment environment;

	private String entityBasePackage;

	public Context(ProcessingEnvironment environment) {
		super();
		this.environment = environment;
	}

	public ProcessingEnvironment getEnvironment() {
		return environment;
	}

	public String getEntityBasePackage() {
		return entityBasePackage;
	}

	public void setEntityBasePackage(String entityBasePackage) {
		environment.getMessager().printMessage(Diagnostic.Kind.NOTE, "Entity base package %s extracted from @EnableSolstice annotation".formatted(entityBasePackage));
		this.entityBasePackage = entityBasePackage;
	}
}
