<#if isRequired>
    @RequiredFilterField
</#if>
    private ${fieldType}<${entityType},${propertyType},${idType},${filterType},${propertyType}Filter> ${propertyName}
        = new ${fieldType}<>(${entityType}_.${fieldName}, this, ${propertyType}_.id);
