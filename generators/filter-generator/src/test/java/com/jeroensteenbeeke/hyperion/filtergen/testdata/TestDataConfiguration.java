package com.jeroensteenbeeke.hyperion.filtergen.testdata;

import com.jeroensteenbeeke.hyperion.solstice.spring.db.EnableSolstice;

@EnableSolstice(entityBasePackage = "com.jeroensteenbeeke.hyperion.filtergen.testdata.entities")
public class TestDataConfiguration {
}
