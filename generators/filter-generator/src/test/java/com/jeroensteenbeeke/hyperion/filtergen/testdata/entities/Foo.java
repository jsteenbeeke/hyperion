package com.jeroensteenbeeke.hyperion.filtergen.testdata.entities;

import com.jeroensteenbeeke.hyperion.data.BaseDomainObject;
import com.jeroensteenbeeke.hyperion.meld.filter.RequiredFilterField;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import java.io.Serial;
import java.io.Serializable;
import java.util.UUID;

@Entity
public class Foo extends BaseDomainObject {
	@Serial
	private static final long serialVersionUID = 1L;

	@Id
	private UUID id;

	@Column
	@RequiredFilterField
	private String name;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public Serializable getDomainObjectId() {
		return getId();
	}


}
