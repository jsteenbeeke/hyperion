package com.jeroensteenbeeke.hyperion.filtergen.testdata.entities.sub;

import com.jeroensteenbeeke.hyperion.data.BaseDomainObject;
import com.jeroensteenbeeke.hyperion.filtergen.testdata.entities.Bar;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import java.io.Serializable;

@Entity
public class Pony extends BaseDomainObject {

	private static final long serialVersionUID = 1L;

	@Id
	private Long id;
	
	@ManyToOne(optional=false)
	private Bar bar;

	public Long getId() {
		return id;
	}
	
	@Override
	public Serializable getDomainObjectId() {
		return getId();
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Bar getBar() {
		return bar;
	}

	public void setBar(Bar bar) {
		this.bar = bar;
	}
	
	
}
