package com.jeroensteenbeeke.hyperion.filtergen;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.SearchFilter;
import com.jeroensteenbeeke.hyperion.meld.filter.StringFilterField;

import jakarta.persistence.metamodel.SingularAttribute;
import java.io.Serializable;

public class WTFilter extends SearchFilter<WTF, WTFilter> {
	private final StringFilterField<WTF, WTF, WTFilter> name = new StringFilterField<>(WTF_.name, this);
}

class WTF implements DomainObject {

	@Override
	public Serializable getDomainObjectId() {
		return 1L;
	}
}

class WTF_ {
	public static volatile SingularAttribute<WTF, String> name;

}
