package com.jeroensteenbeeke.hyperion.filtergen;

import org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

import javax.tools.*;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject.Kind;
import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ProcessorTest {
	@Test
	public void correct_invocation_of_filter_yields_compilation_success(TestInfo testInfo) throws Exception {
		assertTrue(createCompilationTask(testInfo, "decorate-barfilter-correct.ftl",
				"decorate-barbarfilter-correct.ftl",
				"decorate-foofilter-correct.ftl",
				"decorate-ponyfilter-correct.ftl").call(), "Should compile");
	}

	@Test
	public void incorrect_invocation_of_filter_yields_compilation_failure(TestInfo testInfo) throws Exception {
		assertFalse(createCompilationTask(testInfo, "decorate-barfilter-incorrect.ftl",
				"decorate-barbarfilter-incorrect.ftl").call(), "Should not compile");
	}

	private CompilationTask createCompilationTask(TestInfo testInfo, String... extraSourceTemplates) throws Exception {
		Iterable<JavaFileObject> files = getSourceFiles();

		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();

		Path tempDir = Files.createTempDirectory("ProcessorTest-" + testInfo.getTestMethod().orElseThrow(AssertionError::new));

		List<String> options = List.of("-d", tempDir.toAbsolutePath().toString());

		CompilationTask task = compiler.getTask(new PrintWriter(System.out),
				null, null, options, null, files);
		task.setProcessors(Arrays.asList(new ExtraTestDataGenerator(extraSourceTemplates), new JPAMetaModelEntityProcessor(), new SearchFilterGenerator()));

		return task;
	}

	private Iterable<JavaFileObject> getSourceFiles()
			throws Exception {
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		StandardJavaFileManager files = compiler.getStandardFileManager(null,
				null, null);

		files.setLocation(StandardLocation.SOURCE_PATH,
				List.of(new File("src/test/java/com/jeroensteenbeeke/hyperion/filtergen/testdata/")));

		Set<Kind> fileKinds = Collections.singleton(Kind.SOURCE);
		return files.list(StandardLocation.SOURCE_PATH, "", fileKinds, true);
	}
}
