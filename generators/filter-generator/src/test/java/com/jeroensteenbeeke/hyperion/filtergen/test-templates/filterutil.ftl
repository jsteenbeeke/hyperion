package com.jeroensteenbeeke.hyperion.filtergen.testdata;

import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;

import com.jeroensteenbeeke.hyperion.filtergen.testdata.entities.BarCategory;
import com.jeroensteenbeeke.hyperion.filtergen.testdata.entities.filter.BarFilter;
import com.jeroensteenbeeke.hyperion.filtergen.testdata.entities.filter.BarBarFilter;
import com.jeroensteenbeeke.hyperion.filtergen.testdata.entities.filter.FooFilter;
import com.jeroensteenbeeke.hyperion.filtergen.testdata.entities.filter.sub.PonyFilter;

public class FilterUtil {
<#list subtemplates as template>
    <#include template>
</#list>
}
