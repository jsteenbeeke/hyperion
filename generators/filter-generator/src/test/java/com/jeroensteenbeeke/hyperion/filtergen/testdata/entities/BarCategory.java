package com.jeroensteenbeeke.hyperion.filtergen.testdata.entities;

public enum BarCategory {
	SEEDY, CLASSY
}
