package com.jeroensteenbeeke.hyperion.filtergen.testdata.entities;

import com.jeroensteenbeeke.hyperion.data.BaseDomainObject;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import java.io.Serializable;
import java.sql.Blob;
import java.time.LocalDateTime;

@Entity
public class Bar extends BaseDomainObject {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;
	
	@Column
	private String name;
	
	@Column
	private long countables;

	@Column
	private byte[] picture;

	@Column
	private LocalDateTime created;

	@Column
	@Lob
	private Blob bigPicture;

	@Column
	private BarCategory category;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public Serializable getDomainObjectId() {
		return getId();
	}
	
	public long getCountables() {
		return countables;
	}
	
	public void setCountables(long countables) {
		this.countables = countables;
	}

	public byte[] getPicture() {
		return picture;
	}

	public void setPicture(byte[] picture) {
		this.picture = picture;
	}

	public Blob getBigPicture() {
		return bigPicture;
	}

	public void setBigPicture(Blob bigPicture) {
		this.bigPicture = bigPicture;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public BarCategory getCategory() {
		return category;
	}

	public void setCategory(BarCategory category) {
		this.category = category;
	}
}
