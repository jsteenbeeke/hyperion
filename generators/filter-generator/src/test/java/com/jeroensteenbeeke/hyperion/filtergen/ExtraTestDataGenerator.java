package com.jeroensteenbeeke.hyperion.filtergen;

import com.jeroensteenbeeke.hyperion.apt.core.processor.HyperionProcessor;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SupportedAnnotationTypes("jakarta.persistence.Entity")
@SupportedSourceVersion(SourceVersion.RELEASE_21)
public class ExtraTestDataGenerator extends HyperionProcessor {
	private boolean run = false;

	private final List<String> subtemplates;

	public ExtraTestDataGenerator(String... subtemplates) {
		this.subtemplates = List.of(subtemplates);
	}

	@Override
	public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		try {
			if (run) {
				return false;
			}

			run = true;

			JavaFileObject sourceFile = processingEnv
					.getFiler()
					.createSourceFile("com.jeroensteenbeeke.hyperion.filtergen.testdata.FilterUtil");

			processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, "Generated " + sourceFile.toUri().getPath());

			Configuration configuration = new Configuration(Configuration.VERSION_2_3_28);
			configuration.setClassForTemplateLoading(ExtraTestDataGenerator.class, "test-templates");
			configuration.setDefaultEncoding("UTF-8");

			try (PrintWriter pw = new PrintWriter(sourceFile.openWriter())) {
				Template template = configuration.getTemplate("filterutil.ftl");

				Map<String, Object> model = Map.of("subtemplates", subtemplates);

				template.process(model, pw);

				pw.flush();
			}
		} catch (IOException | TemplateException e) {
			throw new AssertionError(e.getMessage());
		}
		return false;
	}
}
