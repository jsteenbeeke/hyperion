	public static final BarBarFilter decorateBarFilter(@NotNull BarBarFilter barBarFilter) {
		barBarFilter.shortVal().between((short) 5, (short) 6).orShortVal((short) 8);
		barBarFilter.doubleVal().between(5.0, 6.0).orDoubleVal(8.0);
		barBarFilter.intVal().between(5, 6).orIntVal(8);
		barBarFilter.byteVal().between((byte) 5, (byte) 6).orByteVal((byte) 8);
		barBarFilter.charVal().between('a', 'b').orCharVal('D');
		barBarFilter.floatVal().between(5.0f, 6.0f).orFloatVal(8.0f);
		barBarFilter.booleanVar().lessThan(false);
		return barBarFilter;
	}
