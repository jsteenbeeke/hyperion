	public static final BarFilter decorateBarFilter(@NotNull BarFilter barFilter) {
		barFilter.id().between(5L, 6L).orId(8L);
		barFilter.name("Test").orName().ilike("Test%");
		barFilter.countables().greaterThan(5L);
		barFilter.created().lessThan(LocalDateTime.now());
		barFilter.category(BarCategory.SEEDY).andCategory().isNotNull();
		return barFilter;
	}
