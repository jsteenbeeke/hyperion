    public static final FooFilter decorateFooFilter(@NotNull FooFilter fooFilter) {
        fooFilter.id().equalTo(java.util.UUID.randomUUID());
        fooFilter.name().like("Test%");

        return fooFilter;
    }
