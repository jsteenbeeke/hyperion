    public static final PonyFilter decoratePonyFilter(@NotNull PonyFilter ponyFilter, @NotNull BarFilter barFilter) {
        ponyFilter.bar(barFilter);
        ponyFilter.id().greaterThan(0L);

        return ponyFilter;
    }
