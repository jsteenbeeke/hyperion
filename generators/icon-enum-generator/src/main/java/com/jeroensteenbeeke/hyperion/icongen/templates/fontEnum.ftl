package ${packageName};

import org.apache.wicket.csp.CSPDirective;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.resource.PackageResourceReference;

import org.jetbrains.annotations.NotNull;

public enum ${prefix}Fonts {
    <#list files as file>
    ${file.enumConstant}("${file.name}", "${file.hash}")<#sep>,
    </#list>;

    private final PackageResourceReference reference;

    private final String hash;

    private ${prefix}Fonts(@NotNull String name, @NotNull String hash) {
        this.reference = new PackageResourceReference(${prefix}Fonts.class, name);
        this.hash = hash;
    }

    public void mount(@NotNull WebApplication application, @NotNull String prefix) {
        application.mountResource(prefix + reference.getName(), reference);
    }

    public void registerCSP(@NotNull WebApplication application) {
        application.getCspSettings().blocking().add(CSPDirective.FONT_SRC, new String[]{String.format("'sha384-%s'", this.hash)});
    }
}
