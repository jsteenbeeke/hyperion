package com.jeroensteenbeeke.hyperion.icongen;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import io.vavr.Tuple2;
import io.vavr.collection.*;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.*;
import org.apache.maven.project.MavenProject;
import org.jetbrains.annotations.NotNull;
import org.sonatype.plexus.build.incremental.BuildContext;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Comparator;
import java.util.Optional;

/**
 * Mojo for generating CSP enums for use in icon plugins
 */
@Mojo(name = "generate-csp")
public class CSPEnumGeneratorMojo extends AbstractMojo {
	@Parameter(required = true)
	public String[] folders;

	@Parameter(required = true)
	public String targetPackage;

	@Parameter(required = true)
	public String enumPrefix;

	/**
	 * @component
	 */
	@Component
	public BuildContext buildContext;

	/**
	 * @component
	 */
	@Parameter(required = true, property = "project")
	@Inject
	public MavenProject project;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		buildContext = Optional
				.ofNullable(buildContext)
				.orElseThrow(() -> new MojoFailureException("BuildContext not available"));
		project = Optional
				.ofNullable(project)
				.orElseThrow(() -> new MojoFailureException("Maven Project not available"));

		folders = Optional.ofNullable(folders)
				.orElseThrow(() -> new MojoFailureException("Required configuration parameter folders mssing"));
		targetPackage = Optional.ofNullable(targetPackage)
				.orElseThrow(() -> new MojoFailureException("Required configuration parameter targetPackage mssing"));
		enumPrefix = Optional.ofNullable(enumPrefix)
				.orElseThrow(() -> new MojoFailureException("Required configuration parameter enumPrefix mssing"));

		Path projectBaseDir = project.getBasedir().toPath();
		Path srcMainJava = projectBaseDir.resolve("src/main/java");

		Path targetBase = projectBaseDir.resolve("target/generated-sources/icon");

		Multimap<PackageKey, FileDescriptor> cssFilesPerPackage = TreeMultimap.withSet().empty();
		Multimap<PackageKey, FileDescriptor> fontFilesPerPackage = TreeMultimap.withSet().empty();

		for (String folder : folders) {
			Path folderPath = srcMainJava.resolve(folder);

			String folderToPackage = folder.replace("/", ".");
			while (folderToPackage.startsWith(".")) {
				folderToPackage = folderToPackage.substring(1);
			}

			while (folderToPackage.endsWith(".")) {
				folderToPackage = folderToPackage.substring(0, folderToPackage.length()-1);
			}

			PackageKey key = new PackageKey(folderToPackage);

			cssFilesPerPackage = filesMatching(folderPath, ".css").foldLeft(cssFilesPerPackage,
					(map, file) -> map.put(key, file));
			fontFilesPerPackage = filesMatching(folderPath, ".ttf", ".woff", ".woff2", ".otf", ".eot", ".svg").foldLeft(
					fontFilesPerPackage,
					(map, file) -> map.put(key, file));
		}

		try {
			Configuration configuration = new Configuration(Configuration.VERSION_2_3_31);
			configuration.setClassForTemplateLoading(CSPEnumGeneratorMojo.class, "templates");
			configuration.setDefaultEncoding("UTF-8");

			// Generate CSS constant
			for (Tuple2<PackageKey, Traversable<FileDescriptor>> packageCssFiles : cssFilesPerPackage.asMap()) {
				calculateHashesAndWriteToFile(targetBase, configuration, "cssEnum.ftl", "Stylesheets", packageCssFiles);
			}

			// Generate font constants
			for (Tuple2<PackageKey, Traversable<FileDescriptor>> packageFontFiles : fontFilesPerPackage.asMap()) {
				calculateHashesAndWriteToFile(targetBase, configuration, "fontEnum.ftl", "Fonts", packageFontFiles);

			}
		} catch (NoSuchAlgorithmException | IOException | TemplateException e) {
			throw new MojoExecutionException(e);
		}

	}

	private void calculateHashesAndWriteToFile(
			Path targetBase, Configuration configuration, String templateFile, String classSuffix,
			Tuple2<PackageKey, Traversable<FileDescriptor>> packageCssFiles) throws NoSuchAlgorithmException, IOException, TemplateException {
		var packageKey = packageCssFiles._1;
		var files = packageCssFiles._2;
		var hashedFiles = new ArrayList<java.util.Map<String,String>>();

		for (FileDescriptor file : files.toSortedSet(Comparator.comparing(FileDescriptor::filename))) {
			hashedFiles.add(calculateHash(file));
		}

		java.util.Map<String,Object> dataModel = java.util.Map.of("packageName", packageKey.packageName(),
				"prefix", enumPrefix,
				"files", hashedFiles);

		Path targetFile = targetBase.resolve(packageKey.packageName().replace(".", File.separator))
				.resolve(enumPrefix + classSuffix + ".java");

		Path targetFolder = targetFile.getParent();

		if (!Files.isDirectory(targetFolder)) {
			Files.createDirectories(targetFolder);
		}

		if (!Files.exists(targetFile)) {
			Files.createFile(targetFile);
		}

		try (Writer out = Files.newBufferedWriter(targetFile, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE)) {
			Template template = configuration.getTemplate(templateFile);

			template.process(dataModel, out);
		}
	}

	private Foldable<FileDescriptor> filesMatching(
			Path path, String... allowedExtensions) throws MojoExecutionException {
		try (var files = Files.list(path)) {
			Set<String> extensionArray = HashSet.of(allowedExtensions);

			return files.map(p -> new FileDescriptor(p, p.getFileName().toString()))
					.filter(fn -> extensionArray.find(extension -> fn.filename().endsWith(extension)).isDefined())
					.collect(Array.collector());
		} catch (IOException e) {
			throw new MojoExecutionException(e);
		}
	}

	private java.util.Map<String,String> calculateHash(
			@NotNull FileDescriptor fileDescriptor) throws NoSuchAlgorithmException, IOException {
		MessageDigest sha384Digest = MessageDigest.getInstance("SHA-384");

		byte[] buffer = new byte[8192];

		try (var in = Files.newInputStream(fileDescriptor.path())) {
			int read = in.read(buffer, 0, 8192);
			sha384Digest.update(buffer, 0, read);
		}

		return new FileDescriptorHash(fileDescriptor, Base64.getEncoder().encodeToString(sha384Digest.digest())).toMap();
	}

	private record PackageKey(String packageName) implements Comparable<PackageKey> {
		@Override
		public int compareTo(@NotNull CSPEnumGeneratorMojo.PackageKey that) {
			return packageName.compareTo(that.packageName);
		}
	}

	private record FileDescriptor(@NotNull Path path, @NotNull String filename) {

	}

	private record FileDescriptorHash(@NotNull FileDescriptor descriptor, @NotNull String hash) {
		public java.util.Map<String, String> toMap() {
			return java.util.Map.of("enumConstant", determineEnumConstant(descriptor), "name", descriptor().filename,
					"hash", hash());
		}

		private String determineEnumConstant(FileDescriptor descriptor) {
			String name = descriptor.filename;

			if (!Character.isJavaIdentifierStart(name.charAt(0))) {
				name = "_".concat(name);
			}

			StringBuilder sanitized = new StringBuilder();
			for (char n : name.toCharArray()) {
				if (n == '-' || n == '.') {
					sanitized.append('_');
				} else if (Character.isJavaIdentifierPart(n)) {
					sanitized.append(n);
				}
			}
			return IconEnumGeneratorMojo.sanitize(sanitized.toString()).toUpperCase();
		}
	}
}
