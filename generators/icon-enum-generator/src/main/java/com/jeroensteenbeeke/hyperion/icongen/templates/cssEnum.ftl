package ${packageName};

import org.apache.wicket.csp.CSPDirective;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.resource.CssResourceReference;

import org.jetbrains.annotations.NotNull;

public enum ${prefix}Stylesheets {
    <#list files as file>
    ${file.enumConstant}("${file.name}", "${file.hash}")<#sep>,
    </#list>;

    private final CssResourceReference reference;

    private final String hash;

    private ${prefix}Stylesheets(@NotNull String name, @NotNull String hash) {
        this.reference = new CssResourceReference(${prefix}Stylesheets.class, name);
        this.hash = hash;
    }

    public void mount(@NotNull WebApplication application, @NotNull String prefix) {
        application.mountResource(prefix + reference.getName(), reference);
    }

    public void registerCSP(@NotNull WebApplication application) {
        application.getCspSettings().blocking().add(CSPDirective.STYLE_SRC, new String[]{String.format("'sha384-%s'", this.hash)});
    }
}
