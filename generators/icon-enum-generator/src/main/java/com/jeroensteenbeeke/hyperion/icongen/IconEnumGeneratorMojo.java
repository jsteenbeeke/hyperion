package com.jeroensteenbeeke.hyperion.icongen;

import com.jeroensteenbeeke.hyperion.icongen.cssparser.CSS3BaseVisitor;
import com.jeroensteenbeeke.hyperion.icongen.cssparser.CSS3Lexer;
import com.jeroensteenbeeke.hyperion.icongen.cssparser.CSS3Parser;
import org.antlr.v4.runtime.BailErrorStrategy;
import org.antlr.v4.runtime.BufferedTokenStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTree;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.*;
import org.apache.maven.project.MavenProject;
import org.sonatype.plexus.build.incremental.BuildContext;

import javax.inject.Inject;
import java.io.*;
import java.nio.file.Files;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;

/**
 * Maven plugin that generates icon enums
 */
@Mojo(name = "generate", defaultPhase = LifecyclePhase.GENERATE_SOURCES,
	requiresDependencyResolution = ResolutionScope.COMPILE)
public class IconEnumGeneratorMojo extends AbstractMojo {
	private static final Set<String> RESERVED_WORDS = Set.of(
		"abstract",
		"assert",
		"boolean",
		"break",
		"byte",
		"case",
		"catch",
		"char",
		"class",
		"const",
		"continue",
		"default",
		"do",
		"double",
		"else",
		"enum",
		"exports",
		"extends",
		"final",
		"finally",
		"float",
		"for",
		"if",
		"goto",
		"implements",
		"import",
		"instanceof",
		"int",
		"interface",
		"long",
		"module",
		"native",
		"new",
		"open",
		"opens",
		"package",
		"private",
		"protected",
		"provides",
		"public",
		"requires",
		"return",
		"short",
		"static",
		"strictfp",
		"super",
		"switch",
		"synchronized",
		"this",
		"throw",
		"throws",
		"to",
		"transient",
		"transitive",
		"try",
		"uses",
		"void",
		"volatile",
		"while",
		"with"
	);

	@Parameter(required = true)
	public String cssPath;

	@Parameter(required = true, defaultValue = "before")
	public String pseudo;

	@Parameter
	public String cssPrefixToStrip;

	@Parameter(required = false)
	public String impliedCssClasses;

	@Parameter(required = true)
	public String targetPackage;

	@Parameter(required = true)
	public String enumName;

	/**
	 * @component
	 */
	@Component
	public BuildContext buildContext;

	/**
	 * @component
	 */
	@Parameter(required = true, property = "project")
	@Inject
	public MavenProject project;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {

		buildContext = Optional
			.ofNullable(buildContext)
			.orElseThrow(() -> new MojoFailureException("BuildContext not available"));
		project = Optional
			.ofNullable(project)
			.orElseThrow(() -> new MojoFailureException("Maven Project not available"));
		cssPath = Optional.ofNullable(cssPath)
			.orElseThrow(() -> new MojoFailureException("Required configuration parameter cssPath mssing"));
		targetPackage = Optional.ofNullable(targetPackage)
			.orElseThrow(() -> new MojoFailureException("Required configuration parameter targetPackage mssing"));
		enumName = Optional.ofNullable(enumName)
			.orElseThrow(() -> new MojoFailureException("Required configuration parameter enumName mssing"));
		cssPrefixToStrip = Optional.ofNullable(cssPrefixToStrip).orElse("");

		File css = new File(project.getBasedir(), cssPath);

		getLog().info(String.format("Detecting icons from %s", css.getAbsolutePath()));

		File base = new File(project.getBasedir(), "target/generated-sources/icon");

		if (!base.exists()) {
			if (!base.mkdirs()) {
				throw new MojoFailureException("Could not create target directory " +
					base.getAbsolutePath()
					+ " as user " +
					System.getProperty("user.name"));
			}
		}

		File pkg = new File(base, targetPackage.replace('.', File.separatorChar));

		if (!pkg.exists()) {
			if (!pkg.mkdirs()) {
				throw new MojoFailureException("Could not create target directory " +
					pkg.getAbsolutePath()
					+ " as user " +
					System.getProperty("user.name"));
			}
		}

		File enumFile = new File(pkg, enumName.concat(".java"));

		try (InputStream in = new FileInputStream(css)) {
			CSS3Lexer lexer = new CSS3Lexer(CharStreams.fromStream(in));
			CSS3Parser parser = new CSS3Parser(new BufferedTokenStream(lexer));
			parser.setErrorHandler(new BailErrorStrategy());

			CSS3Parser.StylesheetContext stylesheet = parser.stylesheet();

			Set<String> iconClasses = new TreeSet<>();

			AtomicReference<String> containingClass = new AtomicReference<>();

			stylesheet.accept(new CSS3BaseVisitor<Void>() {
				@Override
				public Void visitSimpleSelectorSequence(
					CSS3Parser.SimpleSelectorSequenceContext
						ctx) {
					List<ParseTree> children = ctx.children;

					for (int i = 0; i < children.size(); i++) {
						ParseTree currentChild = children.get(i);
						if (currentChild instanceof CSS3Parser.ClassNameContext className) {
							if ((i + 1) < children.size()) {
								ParseTree nextChild = children.get(i + 1);
								if (nextChild instanceof CSS3Parser.PseudoContext pseudo) {
									String cssClass = className.getText();

									if (IconEnumGeneratorMojo.this.pseudo.equals(pseudo
										.ident()
										.Ident()
										.getText())) {
										containingClass.set(cssClass);
									}
								}
							}
						}
					}

					return super.visitSimpleSelectorSequence(ctx);
				}

				@Override
				public Void visitDeclarationList(CSS3Parser.DeclarationListContext ctx) {
					if (containingClass.get() == null) {
						return super.visitDeclarationList(ctx);
					}

					List<CSS3Parser.DeclarationContext> declaration = ctx.declaration();

					if (declaration.size() == 1) {
						CSS3Parser.DeclarationContext declarationContext = declaration.get(0);
						CSS3Parser.Property_Context prop = null;

						if (declarationContext instanceof CSS3Parser.UnknownDeclarationContext unknown) {
							prop = unknown.property_();
						} else if (declarationContext instanceof CSS3Parser.KnownDeclarationContext known) {
							prop = known.property_();
						}

						if (prop != null && "content".equals(prop.getText())) {
							// Highly likely to be an icon
							String iconClass = containingClass.get();
							if (iconClass.startsWith(".")) {
								iconClass = iconClass.substring(1);
							}
							if (cssPrefixToStrip != null && iconClass.startsWith(cssPrefixToStrip)) {
								iconClass = iconClass.substring(cssPrefixToStrip.length());
							}

							iconClasses.add(iconClass);
						}
					}

					return super.visitDeclarationList(ctx);
				}
			});


			writeClassesToFile(iconClasses, enumFile);

			project.addCompileSourceRoot(base.getPath());
		} catch (IOException e) {
			throw new MojoExecutionException(e.getMessage());
		} catch (ParseCancellationException pce) {
			// Regex fallback
			getLog().warn("Could not parse CSS file, falling back to regular expression recognition");

			try {
				String file = Files.readString(css.toPath());
				var matcher = Pattern.compile(
						"[.]%s(.*?)::before\\s* \\{\\s*content:\\s*[\"'](.*?)[\"'];\\s*}".formatted(cssPrefixToStrip))
					.matcher(file);


				Set<String> iconClasses = new TreeSet<>();

				while (matcher.find()) {
					String cssClass = matcher.group(1);

					if (cssClass.startsWith(".")) {
						throw new IllegalStateException("CSS class dot start detected, potential regex internal error");
					}

					iconClasses.add(cssClass);
					getLog().debug("Found CSS class " + cssClass);
				}

				writeClassesToFile(iconClasses, enumFile);

				project.addCompileSourceRoot(base.getPath());
			} catch (IOException e) {
				throw new MojoExecutionException(e.getMessage());
			}
		}
	}

	private void writeClassesToFile(Set<String> iconClasses, File enumFile) throws IOException {
		try (FileWriter fw = new FileWriter(enumFile); PrintWriter pw = new PrintWriter(fw)) {
			pw.printf("package %s;", targetPackage).println();
			pw.println();
			pw.println("import com.jeroensteenbeeke.hyperion.icons.Icon;");
			pw.println("import jakarta.annotation.Generated;");
			pw.println();
			pw.println("/**");
			pw.printf(" * Automatically generated enum for %s icons", enumName).println();
			pw.println(" */");
			pw.printf("@Generated(value=\"%s\", date=\"%s\")", getClass().getName(),
				ZonedDateTime.now().format(
					DateTimeFormatter.ISO_OFFSET_DATE_TIME)).println();
			pw.printf("public enum %s implements Icon {", enumName).println();
			AtomicBoolean first = new AtomicBoolean(true);

			iconClasses.forEach(c -> {
				if (!first.getAndSet(false)) {
					pw.println(",");
				}

				String name = c;

				String actualClass = c;

				if (name.startsWith(".")) {
					name = name.substring(1);
					actualClass = name;
				}

				if (!Character.isJavaIdentifierStart(name.charAt(0))) {
					name = "_".concat(name);
				}

				StringBuilder sanitized = new StringBuilder();
				for (char n : name.toCharArray()) {
					if (n == '-') {
						sanitized.append('_');
					} else if (Character.isJavaIdentifierPart(n)) {
						sanitized.append(n);
					}
				}
				name = sanitize(sanitized.toString());


				pw.print("\t");
				pw.print(name);
				pw.print("(\"");
				if (impliedCssClasses != null) {
					pw.print(impliedCssClasses);
					pw.print(" ");
				}
				if (cssPrefixToStrip != null && !cssPrefixToStrip.isBlank() && !actualClass.startsWith(cssPrefixToStrip)) {
					pw.print(cssPrefixToStrip);
				}
				pw.print(actualClass);
				pw.print("\")");

			});

			pw.println("\t;");
			pw.println();
			pw.println("\tprivate final String cssClasses;");
			pw.println();
			pw.printf("\t%s(String cssClasses) {", enumName).println();
			pw.println("\t\tthis.cssClasses = cssClasses;");
			pw.println("\t}");
			pw.println();
			pw.println("\t@Override");
			pw.println("\tpublic String getCssClasses() {");
			pw.println("\t\treturn this.cssClasses;");
			pw.println("\t}");
			pw.println("}");
			pw.flush();
		}
	}

	static String sanitize(String input) {
		if (RESERVED_WORDS.contains(input)) {
			return "_" + input;
		}

		return input;
	}
}
