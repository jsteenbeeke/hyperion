package com.jeroensteenbeeke.hyperion.apt.core.processor.extended;

import com.jeroensteenbeeke.hyperion.apt.core.data.ConstructorDescriptor;
import com.jeroensteenbeeke.hyperion.apt.core.data.MethodDescriptor;
import com.jeroensteenbeeke.hyperion.apt.core.data.ReferenceType;
import com.jeroensteenbeeke.hyperion.apt.core.data.Type;
import com.jeroensteenbeeke.hyperion.apt.core.processor.HyperionClassProcessor;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;

import org.jetbrains.annotations.NotNull;
import javax.lang.model.element.*;
import javax.lang.model.type.TypeMirror;

/**
 * A more detailed implementation of the ClassProcessor which provides more metadata about classes analyzed, such
 * as constructors, methods and relevant annotations
 */
public abstract class ExtendedHyperionClassProcessor extends HyperionClassProcessor<ExtendedClassWithFields, ExtendedFieldDescriptor> {

	@Override
	protected ExtendedClassWithFields createClass(@NotNull TypeElement type, @NotNull String packageName, @NotNull String className) {
		ExtendedClassWithFields extendedClass = new ExtendedClassWithFields(className, packageName);

		for (TypeMirror anInterface : type.getInterfaces()) {
			var refType = translateType(anInterface).filter(t -> t instanceof ReferenceType).map(t -> (ReferenceType) t);
			if (refType.isDefined()) {
				extendedClass = extendedClass.withImplementedType(refType.get());
			}
		}

		for (Element enclosedElement : type.getEnclosedElements()) {

			if (enclosedElement.getKind() == ElementKind.CONSTRUCTOR) {
				ExecutableElement ctor = (ExecutableElement) enclosedElement;

				extendedClass = extendedClass.withConstructor(List
																	  .ofAll(ctor.getAnnotationMirrors())
																	  .map(this::analyzeAnnotation)
																	  .foldLeft(extractParameters(ctor).foldLeft(new ConstructorDescriptor(), ConstructorDescriptor::withParameter), ConstructorDescriptor::withAnnotation));


			} else if (enclosedElement.getKind() == ElementKind.METHOD) {
				ExecutableElement method = (ExecutableElement) enclosedElement;
				final var target = extendedClass;

				extendedClass = translateType(method.getReturnType())
						.map(t -> target.withMethod(List
															.ofAll(method.getAnnotationMirrors())
															.map(this::analyzeAnnotation)
															.foldLeft(extractParameters(method).foldLeft(new MethodDescriptor(t, method
																	.getSimpleName()
																	.toString(), List.ofAll(method.getModifiers())), MethodDescriptor::withParameter), MethodDescriptor::withAnnotation)))
						.getOrElse(target);

			}
		}

		return extendedClass;
	}


	@Override
	protected ExtendedFieldDescriptor createField(@NotNull VariableElement field, @NotNull Type type, @NotNull String fieldName) {
		HashSet<Modifier> modifiers = HashSet.ofAll(field.getModifiers());
		boolean isStatic = modifiers
				.find(Modifier.STATIC::equals)
				.isDefined();
		boolean isFinal = modifiers
				.find(Modifier.FINAL::equals)
				.isDefined();
		return modifiers
				.foldLeft(new ExtendedFieldDescriptor(type, fieldName, isStatic, isFinal),
						  ExtendedFieldDescriptor::withModifier);
	}
}
