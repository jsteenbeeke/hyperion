package com.jeroensteenbeeke.hyperion.apt.core.data;

import io.vavr.collection.Set;
import io.vavr.control.Option;

import org.jetbrains.annotations.NotNull;

/**
 * Type representing an array
 */
public class ArrayType implements Type {
	private final Type baseType;

	/**
	 * Create a new ArrayType
	 * @param baseType The base type this type is an array of
	 */
	public ArrayType(@NotNull Type baseType) {
		this.baseType = baseType;
	}

	/**
	 * The base type of the array. For example: int[] has base type int. int[][] has base type int[]
	 * @return The base type
	 */
	public Type getBaseType() {
		return baseType;
	}

	@Override
	public String getSymbol() {
		return baseType.getSymbol() + "[]";
	}

	@Override
	public String getFQDN() {
		return baseType.getFQDN() + "[]";
	}

	@Override
	public Set<String> getImports() {
		return baseType.getImports();
	}

	@NotNull
	@Override
	public <R> Option<R> visit(@NotNull TypeVisitor<R> visitor) {
		return visitor.visit(this);
	}
}
