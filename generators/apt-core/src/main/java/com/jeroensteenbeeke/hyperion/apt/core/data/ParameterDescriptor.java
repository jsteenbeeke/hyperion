package com.jeroensteenbeeke.hyperion.apt.core.data;

import io.vavr.collection.List;
import io.vavr.collection.Set;

import org.jetbrains.annotations.NotNull;

/**
 * Descriptor of a constructor or method's parameters
 */
public class ParameterDescriptor implements Annotated, ImportSupplier {
	private final Type type;

	private final String name;

	private final List<AnnotationDescriptor> annotations;

	/**
	 * Constructor
	 * @param type The type of the parameter
	 * @param name The name of the parameter
	 */
	public ParameterDescriptor(@NotNull Type type, @NotNull String name) {
		this(type, name, List.empty());
	}

	private ParameterDescriptor(@NotNull Type type, @NotNull String name, @NotNull List<AnnotationDescriptor> annotations) {
		this.type = type;
		this.name = name;
		this.annotations = annotations;
	}

	/**
	 * Returns the name of the parameter
	 * @return The name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the type of the parameter
	 * @return The type
	 */
	public Type getType() {
		return type;
	}

	/**
	 * Returns the annotations associated with this parameter
	 * @return The annotations
	 */
	@Override
	public List<AnnotationDescriptor> getAnnotations() {
		return annotations;
	}

	/**
	 * Creates a new ParameterDescriptor with the given annotation added
	 * @param annotationDescriptor The annotation
	 * @return A new ParameterDescriptor
	 */
	public ParameterDescriptor withAnnotation(AnnotationDescriptor annotationDescriptor) {
		return new ParameterDescriptor(type, name, annotations.append(annotationDescriptor));
	}

	@Override
	public Set<String> getImports() {
		return join(List.of(getAnnotations())).addAll(type.getImports());
	}
}
