package com.jeroensteenbeeke.hyperion.apt.core.data;

import io.vavr.collection.Set;

import org.jetbrains.annotations.NotNull;
import java.util.Objects;

/**
 * Representation of an enum constant, for use in annotations
 */
public class EnumConstant implements ImportSupplier {
	private final ReferenceType type;

	private final String constant;

	/**
	 * Create a new EnumConstant
	 * @param type The type containing the constant
	 * @param constant The name of the constant
	 */
	public EnumConstant(@NotNull ReferenceType type, @NotNull String constant) {
		this.type = type;
		this.constant = constant;
	}

	/**
	 * The type the enum constant belongs to
	 * @return The ReferenceType of the type
	 */
	@NotNull
	public ReferenceType getType() {
		return type;
	}

	/**
	 * The identifier of the constant referenced
	 * @return A constant
	 */
	@NotNull
	public String getConstant() {
		return constant;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		EnumConstant that = (EnumConstant) o;
		return type.equals(that.type) &&
				constant.equals(that.constant);
	}

	@Override
	public int hashCode() {
		return Objects.hash(type, constant);
	}

	@Override
	public Set<String> getImports() {
		return type.getImports();
	}
}
