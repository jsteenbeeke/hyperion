package com.jeroensteenbeeke.hyperion.apt.core.data;

import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import io.vavr.control.Option;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

/**
 * Type that is passed by reference, such as a class instance
 */
public class ReferenceType implements Type {
	private final String packageName;

	private final String className;

	private final Set<ReferenceType> implementedInterfaces;

	private final String idType;

	/**
	 * Create a new reference type
	 * @param packageName The package the class is in
	 * @param className The name of the class
	 */
	public ReferenceType(@NotNull String packageName, @NotNull String className) {
		this.packageName = packageName;
		this.className = className;
		this.implementedInterfaces = HashSet.empty();
		this.idType = null;
	}

	/**
	 * Creates a new referencetype (internal use)
	 * @param packageName The package name
	 * @param className The name of the class
	 * @param implementedInterfaces The set of implemented interfaces
	 * @param idType The type of ID for this type, if applicable
	 */
	protected ReferenceType(String packageName, String className, Set<ReferenceType> implementedInterfaces, String idType) {
		this.packageName = packageName;
		this.className = className;
		this.implementedInterfaces = implementedInterfaces;
		this.idType = idType;
	}

	/**
	 * Returns an immutable copy of the current object with the given interface as added import
	 * @param iface The type of the interface
	 * @return A new ReferenceType
	 */
	public ReferenceType withImplementedInterface(@NotNull ReferenceType iface) {
		return createNew(packageName, className, implementedInterfaces.add(iface), idType);
	}

	/**
	 * Returns an immutable copy of the current object with the given idType set
	 * @param idType The type of the id
	 * @return A new ReferenceType
	 */
	public ReferenceType withIdType(@NotNull String idType) {
		return createNew(packageName, className, implementedInterfaces, idType);
	}

	/**
	 * Creates a new instance of the current type
	 * @param packageName The package
	 * @param className The class name
	 * @param implementedInterfaces The set of implemented interface
	 * @return A new type
	 */
	protected ReferenceType createNew(String packageName, String className, Set<ReferenceType> implementedInterfaces, String idType) {
		return new ReferenceType(packageName, className, implementedInterfaces, idType);
	}

	/**
	 * The package name of the type. Defaults to empty for primitives
	 * @return The package name
	 */
	public String getPackageName() {
		return packageName;
	}

	/**
	 * The class name of the type (or name of primitive in case of primitives) not including generics
	 * @return The class name
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * Returns a set of types representing all implemented interfaces
	 * @return The set of interfaces
	 */
	public Set<ReferenceType> getImplementedInterfaces() {
		return implementedInterfaces;
	}

	@Override
	public String getFQDN() {
		return packageName + "." + className;
	}

	@Override
	public String getSymbol() {
		return className;
	}

	@Override
	public Set<String> getImports() {
		if (!getPackageName().equals("java.lang") && !getPackageName().isEmpty()) {
			return HashSet.of(getFQDN());
		} else {
			return HashSet.empty();
		}
	}

	@NotNull
	@Override
	public <R> Option<R> visit(@NotNull TypeVisitor<R> visitor) {
		return visitor.visit(this);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ReferenceType)) return false;
		ReferenceType that = (ReferenceType) o;
		return packageName.equals(that.packageName) && className.equals(that.className) && implementedInterfaces.equals(
				that.implementedInterfaces);
	}

	@Override
	public int hashCode() {
		return Objects.hash(packageName, className, implementedInterfaces);
	}

	/**
	 * Returns the ID type, if this is an entity reference
	 * @return The ID type
	 */
	@Nullable
	public String getIdType() {
		return idType;
	}
}
