package com.jeroensteenbeeke.hyperion.apt.core.data;

import io.vavr.collection.Set;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Specialization of ReferenceType that represents enums
 */
public class EnumReferenceType extends ReferenceType{
	/**
	 * Create a new enum reference type
	 *
	 * @param packageName The package the class is in
	 * @param className   The name of the class
	 */
	public EnumReferenceType(@NotNull String packageName, @NotNull String className) {
		super(packageName, className);
	}

	private EnumReferenceType(String packageName, String className, Set<ReferenceType> implementedInterfaces, @Nullable String idType) {
		super(packageName, className, implementedInterfaces, idType);
	}

	@Override
	protected ReferenceType createNew(String packageName, String className, Set<ReferenceType> implementedInterfaces, @Nullable String idType) {
		return new EnumReferenceType(packageName, className, implementedInterfaces, idType);
	}
}
