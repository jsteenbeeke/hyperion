package com.jeroensteenbeeke.hyperion.apt.core.data;

import io.vavr.collection.*;
import io.vavr.control.Option;

import org.jetbrains.annotations.NotNull;

/**
 * Describes an annotation
 */
public class AnnotationDescriptor implements ImportSupplier {
	private final Map<String, Object> values;

	private final ReferenceType type;

	/**
	 * Create a new AnnotationDescriptor
	 *
	 * @param type The type of the annotation
	 */
	public AnnotationDescriptor(ReferenceType type) {
		this.type = type;
		this.values = HashMap.empty();
	}

	private AnnotationDescriptor(ReferenceType type, Map<String, Object> values) {
		this.values = values;
		this.type = type;
	}

	/**
	 * Gets the type of the annotation
	 *
	 * @return The type of the annotation
	 */
	public ReferenceType getType() {
		return type;
	}

	/**
	 * Add an annotation value to this annotation
	 *
	 * @param key                  The key for the value
	 * @param annotationDescriptor The annotation to add
	 * @return A new AnnotationDescriptor
	 */
	public AnnotationDescriptor withValue(@NotNull String key, @NotNull AnnotationDescriptor annotationDescriptor) {
		return new AnnotationDescriptor(type, values.put(key, annotationDescriptor));
	}

	/**
	 * Add an String value to this annotation
	 *
	 * @param key   The key for the value
	 * @param value The value
	 * @return A new AnnotationDescriptor
	 */
	public AnnotationDescriptor withValue(@NotNull String key, @NotNull String value) {
		return new AnnotationDescriptor(type, values.put(key, value));
	}

	/**
	 * Add an Integer value to this annotation
	 *
	 * @param key   The key for the value
	 * @param value The value
	 * @return A new AnnotationDescriptor
	 */
	public AnnotationDescriptor withValue(@NotNull String key, @NotNull Integer value) {
		return new AnnotationDescriptor(type, values.put(key, value));
	}

	/**
	 * Add an Byte value to this annotation
	 *
	 * @param key   The key for the value
	 * @param value The value
	 * @return A new AnnotationDescriptor
	 */
	public AnnotationDescriptor withValue(@NotNull String key, @NotNull Byte value) {
		return new AnnotationDescriptor(type, values.put(key, value));
	}

	/**
	 * Add an Float value to this annotation
	 *
	 * @param key   The key for the value
	 * @param value The value
	 * @return A new AnnotationDescriptor
	 */
	public AnnotationDescriptor withValue(@NotNull String key, @NotNull Float value) {
		return new AnnotationDescriptor(type, values.put(key, value));
	}

	/**
	 * Add an Double value to this annotation
	 *
	 * @param key   The key for the value
	 * @param value The value
	 * @return A new AnnotationDescriptor
	 */
	public AnnotationDescriptor withValue(@NotNull String key, @NotNull Double value) {
		return new AnnotationDescriptor(type, values.put(key, value));
	}

	/**
	 * Add an Short value to this annotation
	 *
	 * @param key   The key for the value
	 * @param value The value
	 * @return A new AnnotationDescriptor
	 */
	public AnnotationDescriptor withValue(@NotNull String key, @NotNull Short value) {
		return new AnnotationDescriptor(type, values.put(key, value));
	}

	/**
	 * Add an Short value to this annotation
	 *
	 * @param key   The key for the value
	 * @param value The value
	 * @return A new AnnotationDescriptor
	 */
	public AnnotationDescriptor withValue(@NotNull String key, @NotNull Long value) {
		return new AnnotationDescriptor(type, values.put(key, value));
	}

	/**
	 * Add an Character value to this annotation
	 *
	 * @param key   The key for the value
	 * @param value The value
	 * @return A new AnnotationDescriptor
	 */
	public AnnotationDescriptor withValue(@NotNull String key, @NotNull Character value) {
		return new AnnotationDescriptor(type, values.put(key, value));
	}

	/**
	 * Add an Boolean value to this annotation
	 *
	 * @param key   The key for the value
	 * @param value The value
	 * @return A new AnnotationDescriptor
	 */
	public AnnotationDescriptor withValue(@NotNull String key, @NotNull Boolean value) {
		return new AnnotationDescriptor(type, values.put(key, value));
	}

	/**
	 * Add an array value to this annotation
	 *
	 * @param key   The key for the value
	 * @param value The value
	 * @return A new AnnotationDescriptor
	 */
	public AnnotationDescriptor withValue(@NotNull String key, @NotNull List<?> value) {
		return new AnnotationDescriptor(type, values.put(key, value));
	}

	/**
	 * Add a type value to this annotation
	 *
	 * @param key   The key for the value
	 * @param value The value
	 * @return A new AnnotationDescriptor
	 */
	public AnnotationDescriptor withValue(@NotNull String key, @NotNull Type value) {
		return new AnnotationDescriptor(type, values.put(key, value));
	}

	/**
	 * Add an enum constant value to this annotation
	 *
	 * @param key   The key for the value
	 * @param value The value
	 * @return A new AnnotationDescriptor
	 */
	public AnnotationDescriptor withValue(@NotNull String key, @NotNull EnumConstant value) {
		return new AnnotationDescriptor(type, values.put(key, value));
	}

	/**
	 * Check if the current annotation has a value with the given key of type annotation, and
	 * returns that annotation
	 *
	 * @param key The key to check
	 * @return If the key exists and is an annotation, an option containing said value
	 */
	public Option<AnnotationDescriptor> getAnnotationValue(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof AnnotationDescriptor)
					 .map(o -> (AnnotationDescriptor) o);
	}

	/**
	 * Check if the current annotation has a value with the given key of type integer, and
	 * returns that integer
	 *
	 * @param key The key to check
	 * @return If the key exists and is an integer, an option containing said value
	 */
	public Option<Integer> getIntValue(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof Integer)
					 .map(o -> (Integer) o);
	}

	/**
	 * Check if the current annotation has a value with the given key of type boolean, and
	 * returns that boolean
	 *
	 * @param key The key to check
	 * @return If the key exists and is an boolean, an option containing said value
	 */
	public Option<Boolean> getBooleanValue(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof Boolean)
					 .map(o -> (Boolean) o);
	}

	/**
	 * Check if the current annotation has a value with the given key of type byte, and
	 * returns that byte
	 *
	 * @param key The key to check
	 * @return If the key exists and is a byte, an option containing said value
	 */
	public Option<Byte> getByteValue(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof Byte)
					 .map(o -> (Byte) o);
	}

	/**
	 * Check if the current annotation has a value with the given key of type char, and
	 * returns that char
	 *
	 * @param key The key to check
	 * @return If the key exists and is an char, an option containing said value
	 */
	public Option<Character> getCharValue(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof Character)
					 .map(o -> (Character) o);
	}

	/**
	 * Check if the current annotation has a value with the given key of type double, and
	 * returns that double
	 *
	 * @param key The key to check
	 * @return If the key exists and is an double, an option containing said value
	 */
	public Option<Double> getDoubleValue(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof Double)
					 .map(o -> (Double) o);
	}

	/**
	 * Check if the current annotation has a value with the given key of type float, and
	 * returns that float
	 *
	 * @param key The key to check
	 * @return If the key exists and is an float, an option containing said value
	 */
	public Option<Float> getFloatValue(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof Float)
					 .map(o -> (Float) o);
	}

	/**
	 * Check if the current annotation has a value with the given key of type long, and
	 * returns that long
	 *
	 * @param key The key to check
	 * @return If the key exists and is a long, an option containing said value
	 */
	public Option<Long> getLongValue(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof Long)
					 .map(o -> (Long) o);
	}

	/**
	 * Check if the current annotation has a value with the given key of type long, and
	 * returns that short
	 *
	 * @param key The key to check
	 * @return If the key exists and is a short, an option containing said value
	 */
	public Option<Short> getShortValue(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof Short)
					 .map(o -> (Short) o);
	}

	/**
	 * Check if the current annotation has a value with the given key of type enum constant, and
	 * returns that enum constant
	 *
	 * @param key The key to check
	 * @return If the key exists and is an enum constant, an option containing said value
	 */
	public Option<EnumConstant> getEnumValue(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof EnumConstant)
					 .map(o -> (EnumConstant) o);
	}

	/**
	 * Check if the current annotation has a value with the given key of type Type (class reference), and
	 * returns that type
	 *
	 * @param key The key to check
	 * @return If the key exists and is a type, an option containing said value
	 */
	public Option<Type> getTypeValue(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof Type)
					 .map(o -> (Type) o);
	}

	/**
	 * Check if the current annotation has a value with the given key of type String, and
	 * returns that String
	 *
	 * @param key The key to check
	 * @return If the key exists and is an String, an option containing said value
	 */
	public Option<String> getStringValue(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof String)
					 .map(o -> (String) o);
	}

	/**
	 * Check if the current annotation has a value with the given key of type annotation, and
	 * returns that annotation
	 *
	 * @param key The key to check
	 * @return If the key exists and is an annotation, an option containing said value
	 */
	public Option<List<AnnotationDescriptor>> getAnnotationValues(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof List<?>)
					 .map(o -> asListOf(o, AnnotationDescriptor.class))
					 .orElse(getAnnotationValue(key).map(List::of));
	}

	/**
	 * Check if the current annotation has a value with the given key of type integer, and
	 * returns that integer
	 *
	 * @param key The key to check
	 * @return If the key exists and is an integer, an option containing said value
	 */
	public Option<List<Integer>> getIntValues(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof List<?>)
					 .map(o -> asListOf(o, Integer.class))
					 .orElse(getIntValue(key).map(List::of));
	}

	/**
	 * Check if the current annotation has a value with the given key of type boolean, and
	 * returns that boolean
	 *
	 * @param key The key to check
	 * @return If the key exists and is an boolean, an option containing said value
	 */
	public Option<List<Boolean>> getBooleanValues(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof List<?>)
					 .map(o -> asListOf(o, Boolean.class))
					 .orElse(getBooleanValue(key).map(List::of));
	}

	/**
	 * Check if the current annotation has a value with the given key of type byte, and
	 * returns that byte
	 *
	 * @param key The key to check
	 * @return If the key exists and is a byte, an option containing said value
	 */
	public Option<List<Byte>> getByteValues(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof List<?>)
					 .map(o -> asListOf(o, Byte.class))
					 .orElse(getByteValue(key).map(List::of));
	}

	/**
	 * Check if the current annotation has a value with the given key of type char, and
	 * returns that char
	 *
	 * @param key The key to check
	 * @return If the key exists and is an char, an option containing said value
	 */
	public Option<List<Character>> getCharValues(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof List<?>)
					 .map(o -> asListOf(o, Character.class))
					 .orElse(getCharValue(key).map(List::of));
	}

	/**
	 * Check if the current annotation has a value with the given key of type double, and
	 * returns that double
	 *
	 * @param key The key to check
	 * @return If the key exists and is an double, an option containing said value
	 */
	public Option<List<Double>> getDoubleValues(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof List<?>)
					 .map(o -> asListOf(o, Double.class))
					 .orElse(getDoubleValue(key).map(List::of));
	}

	/**
	 * Check if the current annotation has a value with the given key of type float, and
	 * returns that float
	 *
	 * @param key The key to check
	 * @return If the key exists and is an float, an option containing said value
	 */
	public Option<List<Float>> getFloatValues(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof List<?>)
					 .map(o -> asListOf(o, Float.class))
					 .orElse(getFloatValue(key).map(List::of));
	}

	/**
	 * Check if the current annotation has a value with the given key of type long, and
	 * returns that long
	 *
	 * @param key The key to check
	 * @return If the key exists and is a long, an option containing said value
	 */
	public Option<List<Long>> getLongValues(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof List<?>)
					 .map(o -> asListOf(o, Long.class))
					 .orElse(getLongValue(key).map(List::of));
	}


	/**
	 * Check if the current annotation has a value with the given key of type long, and
	 * returns that short
	 *
	 * @param key The key to check
	 * @return If the key exists and is a short, an option containing said value
	 */
	public Option<List<Short>> getShortValues(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof List<?>)
					 .map(o -> asListOf(o, Short.class))
					 .orElse(getShortValue(key).map(List::of));
	}

	/**
	 * Check if the current annotation has a value with the given key of type enum constant, and
	 * returns that enum constant
	 *
	 * @param key The key to check
	 * @return If the key exists and is an enum constant, an option containing said value
	 */
	public Option<List<EnumConstant>> getEnumValues(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof List<?>)
					 .map(o -> asListOf(o, EnumConstant.class))
					 .orElse(getEnumValue(key).map(List::of));
	}

	/**
	 * Check if the current annotation has a value with the given key of type Type (class reference), and
	 * returns that type
	 *
	 * @param key The key to check
	 * @return If the key exists and is a type, an option containing said value
	 */
	public Option<List<Type>> getTypeValues(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof List<?>)
					 .map(o -> asListOf(o, Type.class))
					 .orElse(getTypeValue(key).map(List::of))
				;
	}

	/**
	 * Check if the current annotation has a value with the given key of type String, and
	 * returns that String
	 *
	 * @param key The key to check
	 * @return If the key exists and is an String, an option containing said value
	 */
	public Option<List<String>> getStringValues(@NotNull String key) {
		return values.get(key).filter(o -> o instanceof List<?>)
					 .map(o -> asListOf(o, String.class))
					 .orElse(getStringValue(key).map(List::of));
	}

	private <T> List<T> asListOf(Object input, Class<T> outputType) {
		if (!(input instanceof List)) {
			throw new IllegalArgumentException("Input object should be a list");
		}

		for (Object o : ((List<?>) input)) {
			if (!outputType.isAssignableFrom(o.getClass())) {
				throw new IllegalStateException("List contains types other than " + outputType.getName());
			}
		}

		@SuppressWarnings("unchecked")
		List<T> output = (List<T>) input;

		return output;
	}

	/**
	 * Returns the imports required by these annotation descriptors
	 *
	 * @return An immutable set of imports
	 */
	public Set<String> getImports() {
		return HashSet
				.ofAll(type.getImports())
				.addAll(values
								.values()
								.filter(o -> o instanceof ImportSupplier)
								.map(o -> (ImportSupplier) o)
								.flatMap(ImportSupplier::getImports))
				.addAll(values.values().filter(o -> o instanceof Iterable)
							  .map(o -> (Iterable<?>) o)
							  .flatMap(HashSet::ofAll)
							  .filter(o -> o instanceof ImportSupplier).map(o -> (ImportSupplier) o)
							  .flatMap(ImportSupplier::getImports))
				;
	}

}
