package com.jeroensteenbeeke.hyperion.apt.core.data;

import io.vavr.collection.List;
import io.vavr.collection.Set;

import org.jetbrains.annotations.NotNull;
import javax.lang.model.element.Modifier;

/**
 * Describes a method inside a class
 */
public class MethodDescriptor implements Annotated, ImportSupplier {
	private final Type returnType;
	
	private final String name;
	
	private final List<ParameterDescriptor> parameters;
	
	private final List<AnnotationDescriptor> annotations;
	private final List<Modifier> modifiers;

	/**
	 * Constructor
	 * @param returnType The return type of the method
	 * @param name The name of the method
	 * @param modifiers The method's modifiers
	 */
	public MethodDescriptor(@NotNull Type returnType, @NotNull String name, @NotNull List<Modifier> modifiers) {
		this(returnType, name, List.empty(), List.empty(), modifiers);
	}

	private MethodDescriptor(@NotNull Type returnType, @NotNull String name, @NotNull List<ParameterDescriptor> parameters, @NotNull List<AnnotationDescriptor> annotations,
							 List<Modifier> modifiers) {
		this.returnType = returnType;
		this.name = name;
		this.parameters = parameters;
		this.annotations = annotations;
		this.modifiers = modifiers;
	}

	/**
	 * Creates a new method instance with the added parameter
	 * @param parameterDescriptor The paramter
	 * @return A new method descriptor
	 */
	@NotNull
	public MethodDescriptor withParameter(@NotNull ParameterDescriptor parameterDescriptor) {
		return new MethodDescriptor(returnType, name, parameters.append(parameterDescriptor), annotations, modifiers);
	}

	/**
	 * Creates a new method instance with the added annotation
	 * @param annotation The annotation
	 * @return A new method descriptor
	 */
	public MethodDescriptor withAnnotation(@NotNull AnnotationDescriptor annotation) {
		return new MethodDescriptor(returnType, name, parameters, annotations.append(annotation), modifiers);
	}

	/**
	 * Retrieves the return type of the method
	 * @return The return type
	 */
	public Type getReturnType() {
		return returnType;
	}

	/**
	 * Retrieves the name of the method
	 * @return The name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the annotations associated with this method
	 * @return The annotations
	 */
	@Override
	public List<AnnotationDescriptor> getAnnotations() {
		return annotations;
	}

	/**
	 * Returns the parameters belonging to this method
	 * @return The parameters
	 */
	public List<ParameterDescriptor> getParameters() {
		return parameters;
	}

	@Override
	public Set<String> getImports() {
		return join(List.of(getAnnotations(), getParameters())).addAll(returnType.getImports());
	}

	/**
	 * Return a list of modifiers applicable to the field
	 * @return The modifiers
	 */
	public List<Modifier> getModifiers() {
		return modifiers;
	}
}
