package com.jeroensteenbeeke.hyperion.apt.core.processor.extended;

import com.jeroensteenbeeke.hyperion.apt.core.data.AnnotationDescriptor;
import com.jeroensteenbeeke.hyperion.apt.core.data.FieldDescriptor;
import com.jeroensteenbeeke.hyperion.apt.core.data.ImportSupplier;
import com.jeroensteenbeeke.hyperion.apt.core.data.Type;
import io.vavr.collection.List;
import io.vavr.collection.Set;

import org.jetbrains.annotations.NotNull;
import javax.lang.model.element.Modifier;

/**
 * A slightly more in-depth implementation of the FieldDescriptor than the one in the basic variant
 */
public class ExtendedFieldDescriptor extends FieldDescriptor<ExtendedFieldDescriptor> implements ImportSupplier {
	private final List<Modifier> modifiers;

	/**
	 * Constructor
	 * @param type The type of the field
	 * @param name The name of the field
	 * @param isStatic Whether the field is static
	 * @param isFinal Whether the field is final
	 */
	public ExtendedFieldDescriptor(Type type, String name, boolean isStatic, boolean isFinal) {
		this(type, name, isStatic, isFinal, List.empty(), List.empty());
	}

	/**
	 * Constructor for internal use
	 * @param type The type of the field
	 * @param name The name of the field
	 * @param isStatic Whether the field is static
	 * @param isFinal Whether the field is final
	 * @param annotations Annotations associated with the field
	 * @param modifiers The field modifiers
	 */
	protected ExtendedFieldDescriptor(Type type, String name, boolean isStatic, boolean isFinal, List<AnnotationDescriptor> annotations, List<Modifier> modifiers) {
		super(type, name, isStatic, isFinal, annotations);
		this.modifiers = modifiers;
	}

	@Override
	public ExtendedFieldDescriptor newInstance(Type type, String name, boolean isStatic, boolean isFinal, List<AnnotationDescriptor> annotations) {
		return new ExtendedFieldDescriptor(type, name, isStatic, isFinal, annotations, modifiers);
	}

	/**
	 * Adds the given modifier to this field
	 * @param modifier The modifier to check
	 * @return A new FieldDescriptor
	 */
	public ExtendedFieldDescriptor withModifier(@NotNull Modifier modifier) {
		return  new ExtendedFieldDescriptor(getType(), getName(), isStatic(), isFinal(), getAnnotations(), modifiers.append(modifier));
	}

	/**
	 * Return a list of modifiers applicable to the field
	 * @return The modifiers
	 */
	public List<Modifier> getModifiers() {
		return modifiers;
	}

	@Override
	public Set<String> getImports() {
		return join(List.of(getAnnotations())).addAll(getType().getImports());
	}
}
