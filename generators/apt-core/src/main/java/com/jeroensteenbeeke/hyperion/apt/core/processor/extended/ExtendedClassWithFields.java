package com.jeroensteenbeeke.hyperion.apt.core.processor.extended;

import com.jeroensteenbeeke.hyperion.apt.core.data.*;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;

import org.jetbrains.annotations.NotNull;

/**
 * A more detailled class representation
 */
public class ExtendedClassWithFields extends ClassWithFields<ExtendedClassWithFields, ExtendedFieldDescriptor> implements ImportSupplier {
	private final List<ConstructorDescriptor> constructors;

	private final List<MethodDescriptor> methods;

	private final List<ReferenceType> implementedInterfaces;

	/**
	 * Constructor
	 * @param className The name of the class
	 * @param packageName The class package
	 */
	public ExtendedClassWithFields(@NotNull String className, @NotNull String packageName) {
		this(className, packageName, List.empty(), List.empty(), List.empty(), List.empty(), List.empty());
	}

	private ExtendedClassWithFields(@NotNull String className,
									@NotNull String packageName,
									@NotNull List<ExtendedFieldDescriptor> fields,
									@NotNull List<AnnotationDescriptor> annotations,
									@NotNull List<ConstructorDescriptor> constructors,
									@NotNull List<MethodDescriptor> methods,
									@NotNull List<ReferenceType> implementedInterfaces
									) {
		super(className, packageName, fields, annotations);
		this.constructors = constructors;
		this.methods = methods;
		this.implementedInterfaces = implementedInterfaces;
	}

	@NotNull
	@Override
	protected ExtendedClassWithFields newInstance(@NotNull String className, @NotNull String packageName, @NotNull List<ExtendedFieldDescriptor> fields, @NotNull List<AnnotationDescriptor> annotations) {
		return new ExtendedClassWithFields(className, packageName, fields, annotations, constructors, methods, implementedInterfaces);
	}

	/**
	 * Creates a new class representation with the added constructor
	 * @param constructor The constructor
	 * @return A new class representation
	 */
	@NotNull
	public ExtendedClassWithFields withConstructor(@NotNull ConstructorDescriptor constructor) {
		return new ExtendedClassWithFields(getClassName(), getPackageName(), getFields(), getAnnotations(), constructors.append(constructor), methods, implementedInterfaces);
	}

	/**
	 * Creates a new class representation with the added method
	 * @param method The method
	 * @return A new class representation
	 */
	@NotNull
	public ExtendedClassWithFields withMethod(@NotNull MethodDescriptor method) {
		return new ExtendedClassWithFields(getClassName(), getPackageName(), getFields(), getAnnotations(), constructors, methods.append(method), implementedInterfaces);
	}

	/**
	 * Adds an implemented type to this class
	 * @param implementedType The implemented type
	 * @return A new class representation
	 */
	public ExtendedClassWithFields withImplementedType(@NotNull ReferenceType implementedType) {
		return new ExtendedClassWithFields(getClassName(), getPackageName(), getFields(), getAnnotations(), constructors, methods, implementedInterfaces.append(implementedType));
	}

	/**
	 * Returns the list of constructors for this class
	 * @return The constructors
	 */
	public List<ConstructorDescriptor> getConstructors() {
		return constructors;
	}

	/**
	 * Returns the list of methods for this class
	 * @return The methods
	 */
	public List<MethodDescriptor> getMethods() {
		return methods;
	}

	@Override
	public Set<String> getImports() {
		return join(List.of(
				getFields(),
				getAnnotations(),
				getConstructors(),
				getMethods()
		));
	}

	/**
	 * Determines whether or not the given type is implemented by this class
	 * @param refType The type
	 * @return {@code true} if this class implements the given time. {@code false} otherwise
	 */
	public boolean implementsType(ReferenceType refType) {
		Set<ReferenceType> allTypes = getDirectlyAndIndirectlyImplementedInterfaces();

		return allTypes.exists(t -> t.getFQDN().equals(refType.getFQDN()));
	}

	/**
	 * Returns the set of directly and indirectly implemented interfaces
	 * @return A set of interfaces
	 */
	public Set<ReferenceType> getDirectlyAndIndirectlyImplementedInterfaces() {
		Set<ReferenceType> allTypes = HashSet.empty();
		Set<ReferenceType> todo = HashSet.ofAll(implementedInterfaces);

		while (!todo.isEmpty()) {
			ReferenceType next = todo.get();
			allTypes = allTypes.add(next);
			todo = todo.addAll(next.getImplementedInterfaces());
			todo = todo.removeAll(allTypes);
		}
		return allTypes;
	}
}
