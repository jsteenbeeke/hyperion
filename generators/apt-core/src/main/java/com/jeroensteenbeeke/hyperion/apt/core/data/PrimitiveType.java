package com.jeroensteenbeeke.hyperion.apt.core.data;

import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import io.vavr.control.Option;

import org.jetbrains.annotations.NotNull;

/**
 * Primitive type representation
 */
public enum PrimitiveType implements Type {
	/**
	 * boolean
	 */
	Boolean("boolean"),
	/**
	 * byte
	 */
	Byte("byte"),
	/**
	 * char
	 */
	Char("char"),
	/**
	 * double
	 */
	Double("double"),
	/**
	 * float
	 */
	Float("float"),
	/**
	 * int
	 */
	Int("int"),
	/**
	 * long
	 */
	Long("long"),
	/**
	 * short
	 */
	Short("short");

	private final String symbol;

	PrimitiveType(String symbol) {
		this.symbol = symbol;
	}

	@Override
	public String getSymbol() {
		return symbol;
	}

	@Override
	public String getFQDN() {
		return symbol;
	}

	@Override
	public Set<String> getImports() {
		return HashSet.empty();
	}

	@NotNull
	@Override
	public <R> Option<R> visit(@NotNull TypeVisitor<R> visitor) {
		return visitor.visit(this);
	}
}
