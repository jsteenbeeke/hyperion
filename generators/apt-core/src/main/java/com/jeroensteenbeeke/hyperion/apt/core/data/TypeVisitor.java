package com.jeroensteenbeeke.hyperion.apt.core.data;

import io.vavr.control.Option;

import org.jetbrains.annotations.NotNull;

/**
 * Visitor for Type
 * @param <R> The type of result
 */
public interface TypeVisitor<R> {
	/**
	 * Visit primitive types
	 * @param type The primitive type
	 * @return Optionally a return value
	 */
	@NotNull
	default Option<R> visit(@NotNull PrimitiveType type) {
		return Option.none();
	}

	/**
	 * Visit generified types
	 * @param type The generified type
	 * @return Optionally a return value
	 */
	@NotNull
	default Option<R> visit(@NotNull GenerifiedType type) {
		return Option.none();
	}

	/**
	 * Visit reference types
	 * @param type The reference type
	 * @return Optionally a return value
	 */
	@NotNull
	default Option<R> visit(@NotNull ReferenceType type) {
		return Option.none();
	}

	/**
	 * Visit array types
	 * @param type The array types type
	 * @return Optionally a return value
	 */
	@NotNull
	default Option<R> visit(@NotNull ArrayType type) {
		return Option.none();
	}
}
