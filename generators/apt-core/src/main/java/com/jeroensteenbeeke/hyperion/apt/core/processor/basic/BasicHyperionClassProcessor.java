package com.jeroensteenbeeke.hyperion.apt.core.processor.basic;

import com.jeroensteenbeeke.hyperion.apt.core.data.Type;
import com.jeroensteenbeeke.hyperion.apt.core.processor.HyperionClassProcessor;

import org.jetbrains.annotations.NotNull;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;

/**
 * Implementation of HyperionClassProcessor that uses DefaultClassWithFields and FieldDescriptor rather than custom
 * subclasses
 */
public abstract class  BasicHyperionClassProcessor extends HyperionClassProcessor<BasicClassWithFields, BasicFieldDescriptor> {
	@Override
	protected BasicClassWithFields createClass(@NotNull TypeElement type, @NotNull String packageName, @NotNull String className) {
		return new BasicClassWithFields(className, packageName);
	}

	@Override
	protected BasicFieldDescriptor createField(@NotNull VariableElement field, @NotNull Type type, @NotNull String fieldName) {
		return new BasicFieldDescriptor(type, fieldName, field
				.getModifiers()
				.stream()
				.anyMatch(Modifier.STATIC::equals),
										field
												  .getModifiers()
												  .stream()
												  .anyMatch(Modifier.FINAL::equals));
	}
}
