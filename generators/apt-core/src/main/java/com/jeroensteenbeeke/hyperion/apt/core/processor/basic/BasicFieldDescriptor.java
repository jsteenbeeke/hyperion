package com.jeroensteenbeeke.hyperion.apt.core.processor.basic;

import com.jeroensteenbeeke.hyperion.apt.core.data.AnnotationDescriptor;
import com.jeroensteenbeeke.hyperion.apt.core.data.FieldDescriptor;
import com.jeroensteenbeeke.hyperion.apt.core.data.Type;
import io.vavr.collection.List;

/**
 * Barebones implementation of FieldDescriptor
 */
public class BasicFieldDescriptor extends FieldDescriptor<BasicFieldDescriptor> {
	/**
	 * Create a new DefaultFieldDescriptor
	 * @param type The (fully qualified) type of the field
	 * @param name The name of the field
	 * @param isStatic Whether or not the field is static
	 * @param isFinal Whether or not the field is final
	 */
	public BasicFieldDescriptor(Type type, String name, boolean isStatic, boolean isFinal) {
		super(type, name, isStatic, isFinal);
	}

	private BasicFieldDescriptor(Type type, String name, boolean isStatic, boolean isFinal, List<AnnotationDescriptor> annotations) {
		super(type, name, isStatic, isFinal, annotations);
	}

	@Override
	public BasicFieldDescriptor newInstance(Type type, String name, boolean isStatic, boolean isFinal, List<AnnotationDescriptor> annotations) {
		return new BasicFieldDescriptor(type, name, isStatic, isFinal, annotations);
	}
}
