package com.jeroensteenbeeke.hyperion.apt.core.data;

import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import io.vavr.control.Option;

import org.jetbrains.annotations.NotNull;
import javax.lang.model.type.TypeMirror;
import java.util.Objects;

/**
 * Representation of a type in Java that can be used
 * in code generation
 */
public class GenerifiedType extends ReferenceType {

	private final List<GenerifiedType> genericParameters;

	private GenerifiedType(String packageName, String className, List<GenerifiedType> genericParameters) {
		super(packageName, className);
		this.genericParameters = genericParameters;
	}


	@Override
	public String getSymbol() {
		return genericParameters.isEmpty() ? getClassName() : getClassName() + getGenericParameters()
				.map(GenerifiedType::getSymbol)
				.mkString("<", ",", ">");
	}

	@Override
	public Set<String> getImports() {
		if (!getPackageName().equals("java.lang") && !getPackageName().isEmpty()) {
			return getGenericParameters().foldLeft(HashSet.of(getFQDN()), (l, gp) -> l.addAll(gp.getImports()));
		} else {
			return getGenericParameters().flatMap(GenerifiedType::getImports).toSet();
		}
	}

	@Override
	public String toString() {
		return getPackageName() + "." + getSymbol();
	}

	/**
	 * Get all types that were specified as generics to the current type
	 * @return The generic parameters
	 */
	public List<GenerifiedType> getGenericParameters() {
		return genericParameters;
	}

	/**
	 * Add a generic type as parameter to the current type.
	 * @param type The type to add as parameter
	 * @return A new generified type
	 */
	public GenerifiedType withGenericParameter(@NotNull GenerifiedType type) {
		return new GenerifiedType(getPackageName(), getClassName(), genericParameters.append(type));
	}

	/**
	 * Create a new GenerifiedType based on the given TypeMirror
	 * @param typeMirror The TypeMirror
	 * @return A generifiedType
	 */
	public static GenerifiedType fromTypeMirror(@NotNull TypeMirror typeMirror) {
		return fromRepresentation(typeMirror.toString());
	}

	/**
	 * Returns a new GenerifiedType based on the given representation
	 * @param representation The representation
	 * @return A new GenerifiedType
	 */
	static GenerifiedType fromRepresentation(String representation) {
		// Remove annotation garbage from Java 19
		StringBuilder sanitized = new StringBuilder();
		boolean inAnnotation = false;
		for (char c : representation.toCharArray()) {
			if (inAnnotation) {
				if (Character.isWhitespace(c)) {
					inAnnotation = false;
				}
			} else {
				if (c == '@') {
					inAnnotation = true;
				} else {
					sanitized.append(c);
				}
			}
		}

		representation = sanitized.toString();

		int firstBracket = representation.indexOf('<');
		int lastBracket = representation.lastIndexOf('>');


		if (firstBracket == -1 || lastBracket == -1) {
			int lastDot = representation.lastIndexOf('.');

			if (lastDot == -1) {
				return new GenerifiedType("", representation, List.empty());
			} else {
				return new GenerifiedType(representation.substring(0, lastDot),
										  representation.substring(lastDot + 1), List.empty());
			}
		} else {

			GenerifiedType result = getGenerifiedTypeFromRepresentation(representation.substring(0, firstBracket));


			String params = representation.substring(firstBracket + 1, lastBracket);

			return disect(params)
					.map(GenerifiedType::fromRepresentation)
					.foldLeft(result, GenerifiedType::withGenericParameter);
		}
	}

	@NotNull
	private static GenerifiedType getGenerifiedTypeFromRepresentation(String representation) {
		int lastSpace = representation.lastIndexOf(' ');

		if (lastSpace != -1) {
			return getGenerifiedTypeFromRepresentation(representation.substring(lastSpace+1));
		}

		int lastDot = representation.lastIndexOf('.');

		if (lastDot == -1) {
			return new GenerifiedType("", representation, List.empty());
		} else {
			return new GenerifiedType(representation
					.substring(0, lastDot),
					representation
							.substring(lastDot + 1), List
					.empty());
		}
	}

	private static List<String> disect(String params) {
		int inGenericBlock = 0;
		StringBuilder curr = new StringBuilder();
		List<String> result = List.empty();

		for (char c : params.toCharArray()) {
			if (inGenericBlock > 0) {
				if (c == '<') {
					inGenericBlock++;
				} else if (c == '>') {
					inGenericBlock--;
				}
				curr.append(c);
			} else {
				if (c == '<') {
					inGenericBlock++;
					curr.append(c);
				} else if (c == ',') {
					result = result.append(curr.toString());
					curr = new StringBuilder();
				} else {
					curr.append(c);
				}
			}
		}

		if (curr.length() > 0) {
			result = result.append(curr.toString());
		}

		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		GenerifiedType that = (GenerifiedType) o;
		return genericParameters.equals(that.genericParameters);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), genericParameters);
	}

	@NotNull
	@Override
	public <R> Option<R> visit(@NotNull TypeVisitor<R> visitor) {
		return visitor.visit(this);
	}
}
