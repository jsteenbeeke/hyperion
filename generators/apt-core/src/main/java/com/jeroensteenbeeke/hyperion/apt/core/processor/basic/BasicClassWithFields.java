package com.jeroensteenbeeke.hyperion.apt.core.processor.basic;

import com.jeroensteenbeeke.hyperion.apt.core.data.AnnotationDescriptor;
import com.jeroensteenbeeke.hyperion.apt.core.data.ClassWithFields;
import io.vavr.collection.List;

import org.jetbrains.annotations.NotNull;

/**
 * Barebones implementation of ClassWithField with no extra frills
 */
public class BasicClassWithFields extends ClassWithFields<BasicClassWithFields, BasicFieldDescriptor> {
	/**
	 * Create a new ClassWithFields for the given class and package name
	 *
	 * @param className   The name of the class
	 * @param packageName The package the class is in
	 */
	public BasicClassWithFields(@NotNull String className, @NotNull String packageName) {
		super(className, packageName);
	}

	private BasicClassWithFields(@NotNull String className, @NotNull String packageName, @NotNull List<BasicFieldDescriptor> fields, @NotNull List<AnnotationDescriptor> annotations) {
		super(className, packageName, fields, annotations);
	}

	@Override
	protected BasicClassWithFields newInstance(@NotNull String className, @NotNull String packageName, @NotNull List<BasicFieldDescriptor> fields, @NotNull List<AnnotationDescriptor> annotations) {
		return new BasicClassWithFields(className, packageName, fields, annotations);
	}
}
