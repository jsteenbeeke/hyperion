package com.jeroensteenbeeke.hyperion.apt.core;

import io.vavr.Function1;
import io.vavr.Tuple2;
import io.vavr.collection.*;
import io.vavr.control.Option;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.util.Objects;

/**
 * Immutable class that can determine which groups of packages belong together (i.e. are subpackages of one another)
 * @param <T> The type of class contained
 */
public class PackageGrouper<T> {
	private final Map<String, Node> nodes;

	private final Set<Node> rootNodes;

	private final Map<Node, Array<T>> nodesToClasses;

	private final Function1<T, String> packageNameFunction;

	/**
	 * Creates a new grouper
	 * @param packageNameFunction Function to extract package name from class representation used
	 */
	public PackageGrouper(Function1<T, String> packageNameFunction) {
		this(HashMap.empty(), HashSet.empty(), HashMap.empty(), packageNameFunction);
	}

	private PackageGrouper(Map<String, Node> nodes,
						   Set<Node> rootNodes,
						   Map<Node, Array<T>> nodesToClasses, Function1<T, String> packageNameFunction) {
		this.nodes = nodes;
		this.rootNodes = rootNodes;
		this.nodesToClasses = nodesToClasses;
		this.packageNameFunction = packageNameFunction;
	}

	/**
	 * Adds the given class to the grouper, yielding a new object
	 * @param aClass The class representation to add
	 * @return A new grouper with the added class
	 */
	@NotNull
	public PackageGrouper<T> withClass(@NotNull T aClass) {
		String packageName = packageNameFunction.apply(aClass);
		var nodes = this.nodes;
		var rootNodes = this.rootNodes;
		var nodesToClasses = this.nodesToClasses;

		Array<String> path = Array.empty();
		Node previous = null;
		for (String s : packageName.split("\\.")) {
			path = path.append(s);

			String currentPackage = path.mkString(".");
			if (!nodes.containsKey(currentPackage)) {
				nodes = nodes.put(currentPackage, new Node(previous, currentPackage));
			}
			Node next = nodes.get(currentPackage).get();

			if (previous == null) {
				if (!rootNodes.contains(next)) {
					rootNodes = rootNodes.add(next);
				}
			}

			previous = next;
		}

		if (previous != null) {
			nodesToClasses = nodesToClasses.put(previous, Array.of(aClass), Array::appendAll);
			return new PackageGrouper<>(nodes, rootNodes, nodesToClasses, packageNameFunction);
		}

		return this;
	}

	/**
	 * Tries to group packages into clusters. If a group of packages have at least some common prefix,
	 * they will be clustered together. For instance: com.a.b and com.a.c have a common prefix, but com.a.b and net.a.b do not
	 * @return A map with the topmost node as key, and a map of packages mapped to classes as value
	 */
	Map<Node, Map<Array<Node>, Array<T>>> groupClassesByRootNode() {
		Map<Node, Map<Array<Node>, Array<T>>> groupedClasses = HashMap.empty();

		for (Tuple2<Node, Array<T>> nodesToClass : nodesToClasses) {
			var node = nodesToClass._1;
			var root = node.getRoot();
			var path = node.getPath();
			var classes = nodesToClass._2;

			for (T aClass : classes) {
				groupedClasses = groupedClasses.put(root, HashMap.of(path, Array.of(aClass)),
													(a, b) -> a.merge(b, Array::appendAll));
			}
		}

		return groupedClasses;
	}

	/**
	 * Determines the root packages of the grouper: the topmost package prefixes that have no common
	 * parent package aside from the default package
	 * @return A set of package names
	 */
	public Set<String> getBasePackages() {
		Set<String> result = HashSet.empty();

		for (Tuple2<Node, Map<Array<Node>, Array<T>>> group : groupClassesByRootNode()) {
			var nodes = group._2();
			var pathSizes = HashMap.<Integer, Array<Array<Node>>>empty();

			for (Tuple2<Array<Node>, Array<T>> pathToClasses : nodes) {
				var path = pathToClasses._1;

				pathSizes = pathSizes.put(path.length(), Array.of(path), Array::appendAll);
			}

			var res = result;
			result = pathSizes.minBy(Tuple2::_1)
							  .map(Tuple2::_2)
							  .map(p -> p.map(a -> a.map(Node::getSegmentName).mkString(".")))
							  .map(this::reconcilePaths)
							  .filter(Option::isDefined)
							  .map(Option::get)
							  .map(res::add)
							  .getOrElse(result);
		}


		return result;
	}

	private Option<String> reconcilePaths(Array<String> arrays) {
		return arrays
				.reduceOption(this::greatestCommonPrefix)
				.map(s -> s.endsWith(".") ? s.substring(0, s.length() - 1) : s);
	}

	private String greatestCommonPrefix(String a, String b) {
		int minLength = Math.min(a.length(), b.length());
		for (int i = 0; i < minLength; i++) {
			if (a.charAt(i) != b.charAt(i)) {
				return a.substring(0, i);
			}
		}
		return a.substring(0, minLength);
	}


	private static class Node {
		private final Node parent;

		private final String name;

		public Node(@Nullable Node parent, @NotNull String name) {
			this.parent = parent;
			this.name = name;
		}

		public String getName() {
			return name;
		}

		public String getSegmentName() {
			int idx = name.lastIndexOf(".");

			if (idx == -1) {
				return name;
			}

			return name.substring(idx + 1);
		}

		public Node getRoot() {
			if (parent == null) {
				return this;
			}

			return parent.getRoot();
		}

		public Array<Node> getPath() {
			if (parent != null) {
				return parent.getPath().append(this);
			}

			return Array.of(this);
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (!(o instanceof Node)) return false;
			Node node = (Node) o;
			return Objects.equals(parent, node.parent) && name.equals(node.name);
		}

		@Override
		public int hashCode() {
			return Objects.hash(parent, name);
		}
	}
}
