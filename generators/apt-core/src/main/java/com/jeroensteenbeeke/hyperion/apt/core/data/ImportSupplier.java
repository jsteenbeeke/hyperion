package com.jeroensteenbeeke.hyperion.apt.core.data;

import io.vavr.collection.List;
import io.vavr.collection.Set;
import io.vavr.collection.TreeSet;

/**
 * Interfaces that denotes that a language element may supply an import
 */
public interface ImportSupplier {
	/**
	 * Returns the set of imports required by this element
	 *
	 * @return A set if imports
	 */
	Set<String> getImports();

	/**
	 * Creates a set of imports from several lists of import suppliers
	 *
	 * @param suppliers The suppliers
	 * @return A Set of imports
	 */
	default Set<String> join(List<List<? extends ImportSupplier>> suppliers) {
		return suppliers
				.foldLeft(TreeSet.empty(), (imports, supplierList) -> imports.addAll(supplierList.flatMap(ImportSupplier::getImports)));
	}
}
