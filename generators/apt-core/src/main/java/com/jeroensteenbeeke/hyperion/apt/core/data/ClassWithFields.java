package com.jeroensteenbeeke.hyperion.apt.core.data;

import io.vavr.collection.List;

import org.jetbrains.annotations.NotNull;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * Programmer-friendly representation of a class as scanned by the annotation processor. This class
 * and any subclasses are assumed to be immutable.
 *
 * @param <C> The type of ClassWithFields added by the annotation processor
 * @param <F> The type of FieldDescriptor added by the annotation processor
 */
public abstract class ClassWithFields<C extends ClassWithFields<C,F>, F extends FieldDescriptor<?>> {
	private final String className;

	private final String packageName;

	private final List<F> fields;

	private final List<AnnotationDescriptor> annotations;

	/**
	 * Create a new ClassWithFields
	 * @param className The name of the class
	 * @param packageName The package the class is located in
	 */
	protected ClassWithFields(@NotNull String className, @NotNull String packageName) {
		this.className = className;
		this.packageName = packageName;
		this.fields = List.empty();
		this.annotations = List.empty();
	}

	/**
	 * Create a new ClassWithFields
	 * @param className The name of the class
	 * @param packageName The package the class is located in
	 * @param fields The fields inside this class
	 * @param annotations The annotations attached to this class
	 */
	protected ClassWithFields(@NotNull String className,
							  @NotNull String packageName,
							  @NotNull List<F> fields,
							  @NotNull List<AnnotationDescriptor> annotations) {
		this.className = className;
		this.packageName = packageName;
		this.fields = fields;
		this.annotations = annotations;
	}

	/**
	 * Add the given field to the class descriptor, creating a new instance
	 * @param field The field to add
	 * @return A new instance with the added field
	 */
	@NotNull
	public C withField(@NotNull F field) {
		return newInstance(className, packageName, fields.append(field), annotations);
	}

	/**
	 * Creates a new instance of the current class (needs to be overriden if using a custom class)
	 * @param className The name of the class
	 * @param packageName The package the class is in
	 * @param fields The fields in the class
	 * @param annotations The annotations to include
	 * @return A new instance of type C
	 */
	@NotNull
	protected abstract C newInstance(@NotNull String className,
									 @NotNull String packageName,
									 @NotNull List<F> fields,
									 @NotNull List<AnnotationDescriptor> annotations);

	/**
	 * Applies a filter to all fields in the current class
	 * @param predicate The predicate to apply
	 * @return A new instance with filtered fields
	 */
	@NotNull
	public C filterFields(@NotNull Predicate<F> predicate) {
		return newInstance(className, packageName, fields.filter(predicate), annotations);
	}

	/**
	 * Add an annotation to the class descriptor, yielding a new instance
	 * @param annotation The annotation to add
	 * @return A new instance
	 */
	@NotNull
	public C withAnnotation(@NotNull AnnotationDescriptor annotation) {
		return newInstance(className, packageName, fields, annotations.append(annotation));
	}

	/**
	 * Get the name of the class represented by this descriptor
	 * @return The classname
	 */
	@NotNull
	public String getClassName() {
		return className;
	}

	/**
	 * Get the package of the class represented by this descriptor
	 * @return A packagename
	 */
	@NotNull
	public String getPackageName() {
		return packageName;
	}

	/**
	 * Get the fields of the class represented by this descriptor
	 * @return An immutable list of fields
	 */
	@NotNull
	public List<F> getFields() {
		return fields;
	}

	/**
	 * Returns the annotations attached to this class
	 * @return The annotations for this class
	 */
	@NotNull
	public List<AnnotationDescriptor> getAnnotations() {
		return annotations;
	}

	/**
	 * Returns the fully qualified domain name (FQDN) of the class represented by this descriptor
	 * @return The FQDN
	 */
	@NotNull
	public String getFQDN() {
		return packageName.isEmpty() ? className : packageName + "." + className;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ClassWithFields)) return false;
		ClassWithFields<?, ?> that = (ClassWithFields<?, ?>) o;
		return className.equals(that.className) && packageName.equals(that.packageName) && fields.equals(that.fields) && annotations.equals(that.annotations);
	}

	@Override
	public int hashCode() {
		return Objects.hash(className, packageName, fields, annotations);
	}
}
