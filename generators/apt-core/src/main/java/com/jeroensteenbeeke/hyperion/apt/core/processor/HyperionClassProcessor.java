package com.jeroensteenbeeke.hyperion.apt.core.processor;

import com.jeroensteenbeeke.hyperion.apt.core.data.*;
import io.vavr.Tuple3;
import io.vavr.collection.Array;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import io.vavr.control.Option;

import org.jetbrains.annotations.NotNull;

import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.*;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.ElementFilter;
import javax.tools.Diagnostic;
import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.Objects;

/**
 * Annotation processor that reads classes with a given annotation type, and passes read class information to the code generator
 *
 * @param <C> The type of class descriptor to use in the implementation
 * @param <F> The type of field descriptor to use in the implementation
 */
public abstract class HyperionClassProcessor<C extends ClassWithFields<C, F>, F extends FieldDescriptor<F>> extends HyperionProcessor {

	private Set<String> classes = HashSet.empty();

	@Override
	public boolean process(java.util.Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
		Set<? extends Element> annotatedElements = getRequiredAnnotations().flatMap(
				annotation -> List.ofAll(roundEnv.getElementsAnnotatedWith(annotation)))
			.toSet();

		var classes = annotatedElements
			.flatMap(this::analyzeElement)
			.filter(this::filterClass)
			.map(c -> c.filterFields(this::retainFieldIf));

		processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE,
			getGeneratorName() + " found " + classes.size() + " eligible classes");
		System.out.printf("%s found %d eligible classes%n", getGeneratorName(), classes.size());

		var validationError = validate(classes);
		if (validationError.isDefined()) {
			throw new RuntimeException(validationError.get());
		}

		classes.forEach(this::generateCode);

		return false;
	}

	/**
	 * Perform a sanity check
	 *
	 * @param classes The classes to check
	 * @return On success: an empty Option. On failure: an Option containing an error message
	 */
	protected Option<String> validate(@NotNull Set<C> classes) {
		return Option.none();
	}


	/**
	 * Returns the name of the generator, i.e. what this class generates
	 *
	 * @return The name of the generator
	 */
	protected abstract String getGeneratorName();

	/**
	 * Returns the annotation classes we are looking for to base our code generation on
	 *
	 * @return The required annotation classes
	 */
	protected abstract List<Class<? extends Annotation>> getRequiredAnnotations();

	/**
	 * Generate code based on the given class descriptor
	 *
	 * @param classDescriptor The class scanned
	 */
	protected abstract void generateCode(@NotNull C classDescriptor);

	/**
	 * Create a new class descriptor
	 *
	 * @param type        The type scanned by the annotation processor
	 * @param packageName The package the class is in
	 * @param className   The name of the clas
	 * @return A new class descriptor
	 */
	protected abstract C createClass(@NotNull TypeElement type, @NotNull String packageName, @NotNull String className);

	/**
	 * Create a new field descriptor
	 *
	 * @param field     The field scanned by the annotation processor
	 * @param type      The type as determined by Hyperion
	 * @param fieldName The name of the field
	 * @return A new field descriptor
	 */
	protected abstract F createField(@NotNull VariableElement field, @NotNull Type type, @NotNull String fieldName);

	/**
	 * Filter class descriptors. Removes any descriptor from processing that does not cause this method to return true
	 *
	 * @param classDescriptor The descriptor
	 * @return {@code true} if the descriptor should be retained, {@code false} otherwise
	 */
	protected boolean filterClass(@NotNull C classDescriptor) {
		return true;
	}

	/**
	 * Filter fields. Retains the given field if this method returns {@code true}
	 *
	 * @param f The field to check
	 * @return {@code true} if the descriptor should be retained, {@code false} otherwise
	 */
	protected boolean retainFieldIf(F f) {
		return true;
	}

	/**
	 * Returns a list of annotations that qualify a field for processing. If any of the annotation classes is present, the field is retained.
	 * If the list is empty, no annotations are checked
	 *
	 * @return A list of possible annotation classes
	 */
	protected List<Class<? extends Annotation>> getFieldShouldHaveOneOf() {
		return List.of();
	}

	private List<C> analyzeElement(@NotNull Element e) {
		if (e.getKind() != ElementKind.CLASS && e.getKind() != ElementKind.ANNOTATION_TYPE) {
			Element enclosingElement = e.getEnclosingElement();
			if (enclosingElement != null) {
				return analyzeElement(enclosingElement);
			} else {
				return List.empty();
			}
		}


		TypeElement type = ((TypeElement) e);

		if (classes.contains(type.getQualifiedName().toString())) {
			return List.empty();
		}
		classes = classes.add(type.getQualifiedName().toString());

		String fqdn = type.getQualifiedName().toString();

		final String className = extractClass(fqdn);
		final String packageName = extractPackageName(fqdn);

		C result = createClass(type, packageName, className);

		for (AnnotationMirror annotationMirror : type.getAnnotationMirrors()) {
			result = result.withAnnotation(analyzeAnnotation(annotationMirror));
		}

		List<Element> toExplore = List.ofAll(e.getEnclosedElements());

		TypeMirror superType = type.getSuperclass();

		while (superType != null) {
			if (superType instanceof DeclaredType dt) {

				Element element = dt.asElement();

				if (element instanceof TypeElement t && containsAnnotation(element,
					getRequiredAnnotations().map(Class::getName).mkString(", "))) {

					toExplore = toExplore.appendAll(t.getEnclosedElements());

					superType = t.getSuperclass();
				} else {
					break;
				}
			} else {
				break;
			}

		}


		return List.of(analyzeFields(result, toExplore));
	}

	/**
	 * Translate the annotation to something we actually understand
	 *
	 * @param annotationMirror The annotation
	 * @return An internal representation of the annotation
	 */
	@NotNull
	protected AnnotationDescriptor analyzeAnnotation(AnnotationMirror annotationMirror) {
		final var fqdn = annotationMirror.getAnnotationType().toString();

		var descriptor = new AnnotationDescriptor(new ReferenceType(extractPackageName(fqdn), extractClass(fqdn)));

		var values = annotationMirror.getElementValues();
		if (values != null) {
			for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : values.entrySet()) {
				final String key = entry.getKey().getSimpleName().toString();
				AnnotationValue value = entry.getValue();

				final var currentDescriptor = descriptor;

				descriptor = value.accept(new AnnotationValueVisitor<>() {
					@Override
					public AnnotationDescriptor visit(AnnotationValue av) {
						return visit(av, currentDescriptor);
					}

					@Override
					public AnnotationDescriptor visit(
						AnnotationValue annotationValue, AnnotationDescriptor annotationDescriptor) {
						return annotationValue.accept(this, annotationDescriptor);
					}

					@Override
					public AnnotationDescriptor visitBoolean(boolean b, AnnotationDescriptor annotationDescriptor) {
						return annotationDescriptor.withValue(key, b);
					}

					@Override
					public AnnotationDescriptor visitByte(byte b, AnnotationDescriptor annotationDescriptor) {
						return annotationDescriptor.withValue(key, b);
					}

					@Override
					public AnnotationDescriptor visitChar(char c, AnnotationDescriptor annotationDescriptor) {
						return annotationDescriptor.withValue(key, c);
					}

					@Override
					public AnnotationDescriptor visitDouble(double v, AnnotationDescriptor annotationDescriptor) {
						return annotationDescriptor.withValue(key, v);
					}

					@Override
					public AnnotationDescriptor visitFloat(float v, AnnotationDescriptor annotationDescriptor) {
						return annotationDescriptor.withValue(key, v);
					}

					@Override
					public AnnotationDescriptor visitInt(int i, AnnotationDescriptor annotationDescriptor) {
						return annotationDescriptor.withValue(key, i);
					}

					@Override
					public AnnotationDescriptor visitLong(long l, AnnotationDescriptor annotationDescriptor) {
						return annotationDescriptor.withValue(key, l);
					}

					@Override
					public AnnotationDescriptor visitShort(short i, AnnotationDescriptor annotationDescriptor) {
						return annotationDescriptor.withValue(key, i);
					}

					@Override
					public AnnotationDescriptor visitString(String s, AnnotationDescriptor annotationDescriptor) {
						return annotationDescriptor.withValue(key, s);
					}

					@Override
					public AnnotationDescriptor visitType(
						TypeMirror typeMirror, AnnotationDescriptor annotationDescriptor) {
						return translateType(typeMirror)
							.map(type -> annotationDescriptor.withValue(key, type))
							.getOrElse(annotationDescriptor);
					}

					@Override
					public AnnotationDescriptor visitEnumConstant(
						VariableElement variableElement, AnnotationDescriptor annotationDescriptor) {
						if (annotationDescriptor == null) {
							return currentDescriptor;
						}

						return translateType(variableElement.asType())
							.filter(t -> t instanceof ReferenceType)
							.map(type -> annotationDescriptor.withValue(key,
								new EnumConstant((ReferenceType) type, variableElement
									.getSimpleName()
									.toString())))
							.getOrElse(annotationDescriptor);
					}

					@Override
					public AnnotationDescriptor visitAnnotation(
						AnnotationMirror annotationMirror, AnnotationDescriptor annotationDescriptor) {
						return annotationDescriptor.withValue(key, analyzeAnnotation(annotationMirror));
					}

					@Override
					public AnnotationDescriptor visitArray(
						java.util.List<? extends AnnotationValue> list, AnnotationDescriptor annotationDescriptor) {
						return annotationDescriptor.withValue(key, list
							.stream()
							.map(this::visit)
							.collect(List.collector()));
					}

					@Override
					public AnnotationDescriptor visitUnknown(
						AnnotationValue annotationValue, AnnotationDescriptor annotationDescriptor) {
						return annotationDescriptor;
					}
				}, descriptor);
			}

		}

		return descriptor;
	}

	private C analyzeFields(C result, List<Element> toExplore) {
		for (Element child : toExplore) {
			if (child.getKind() == ElementKind.FIELD) {
				if (getFieldShouldHaveOneOf().isEmpty() || getFieldShouldHaveOneOf()
					.exists(a -> containsAnnotation(child, a
						.getName()))) {
					VariableElement field = (VariableElement) child;

					TypeMirror declaredType = field.asType();

					final String fieldName = field.getSimpleName().toString();

					final C intermediate = result;

					result = translateType(declaredType)
						.map(t -> intermediate.withField(field
							.getAnnotationMirrors()
							.stream()
							.map(this::analyzeAnnotation)
							.collect(Array.collector())
							.foldLeft(createField(field, t, fieldName), FieldDescriptor::withAnnotation)))
						.getOrElse(result);
				}
			}
		}

		return result;
	}

	/**
	 * Extracts parameters from the given executable element (i.e. method or constructor)
	 *
	 * @param element The element
	 * @return All parameters
	 */
	protected List<ParameterDescriptor> extractParameters(ExecutableElement element) {
		return element.getParameters().stream()
			.map(p -> translateType(p.asType()).map(t -> new Tuple3<>(t, p
				.getSimpleName()
				.toString(), p.getAnnotationMirrors())))
			.filter(Option::isDefined)
			.map(Option::get)
			.map(t -> {
				ParameterDescriptor parameterDescriptor = new ParameterDescriptor(t._1, t._2);

				return t._3.stream().map(this::analyzeAnnotation)
					.collect(List.collector())
					.foldLeft(parameterDescriptor, ParameterDescriptor::withAnnotation);
			})
			.collect(List.collector());
	}

	/**
	 * Translate an annotation-processor type to one we can more easily work with
	 *
	 * @param declaredType The type
	 * @return An Option either containing the translated type, or empty if we have no idea what to do with it
	 */
	protected Option<Type> translateType(TypeMirror declaredType) {
		if (declaredType instanceof ArrayType at) {
			return translateType(at.getComponentType()).map(com.jeroensteenbeeke.hyperion.apt.core.data.ArrayType::new);
		} else if (declaredType instanceof DeclaredType rt) {
			if (!rt.getTypeArguments().isEmpty()) {
				return Option.some(GenerifiedType.fromTypeMirror(rt));
			} else {
				TypeElement element = (TypeElement) rt.asElement();
				String packageName = extractPackageName(element
					.getQualifiedName()
					.toString());
				String className = extractClass(element
					.getQualifiedName()
					.toString());
				ReferenceType referenceType = element.getKind() == ElementKind.ENUM ? new EnumReferenceType(packageName,
					className) :
					new ReferenceType(packageName, className);

				for (VariableElement field : ElementFilter.fieldsIn(element.getEnclosedElements())) {
					if (field.getAnnotationMirrors().stream()
						.map(this::analyzeAnnotation)
						.anyMatch(ad -> ad.getType().getFQDN().equals("jakarta.persistence.Id"))) {
						// This is the entity ID
						String idType = translateType(field.asType()).map(Type::getFQDN).getOrNull();

						if (idType != null) {
							referenceType = referenceType.withIdType(idType);
						}
						break;
					}
				}

				Set<TypeMirror> interfaces = determineAllInterfaces(element);
				referenceType = interfaces
					.foldLeft(referenceType, (ref, type) -> translateType(type)
						.map((Type iface) -> {
							if (iface instanceof ReferenceType refType) {
								return ref.withImplementedInterface(refType);
							}

							return ref;
						})
						.getOrElse(ref));

				return Option.some(referenceType);
			}
		} else if (declaredType instanceof PrimitiveType pt) {
			switch (pt.getKind()) {
				case BOOLEAN:
					return Option.some(com.jeroensteenbeeke.hyperion.apt.core.data.PrimitiveType.Boolean);
				case BYTE:
					return Option.some(com.jeroensteenbeeke.hyperion.apt.core.data.PrimitiveType.Byte);
				case CHAR:
					return Option.some(com.jeroensteenbeeke.hyperion.apt.core.data.PrimitiveType.Char);
				case DOUBLE:
					return Option.some(com.jeroensteenbeeke.hyperion.apt.core.data.PrimitiveType.Double);
				case FLOAT:
					return Option.some(com.jeroensteenbeeke.hyperion.apt.core.data.PrimitiveType.Float);
				case INT:
					return Option.some(com.jeroensteenbeeke.hyperion.apt.core.data.PrimitiveType.Int);
				case LONG:
					return Option.some(com.jeroensteenbeeke.hyperion.apt.core.data.PrimitiveType.Long);
				case SHORT:
					return Option.some(com.jeroensteenbeeke.hyperion.apt.core.data.PrimitiveType.Short);

			}
		}
		return Option.none();
	}

	private Set<TypeMirror> determineAllInterfaces(TypeElement element) {
		Set<TypeMirror> result = HashSet.empty();

		Array<? extends TypeMirror> currentInterfaces = Array.ofAll(element.getInterfaces());


		result = currentInterfaces.foldLeft(result, Set::add);
		Set<TypeMirror> superClassInterfaces = Option.of(element.getSuperclass())
			.flatMap(this::toTypeElement)
			.map(this::determineAllInterfaces)
			.getOrElse(HashSet::empty);
		result = superClassInterfaces.foldLeft(result, Set::add);

		Array<TypeMirror> superInterface = currentInterfaces
			.flatMap(this::toTypeElement)
			.flatMap(TypeElement::getInterfaces);

		result = superInterface.foldLeft(result, Set::add);

		Set<TypeMirror> superSuperInterfaces = superInterface
			.filter(Objects::nonNull)
			.flatMap(this::toTypeElement)
			.map(this::determineAllInterfaces)
			.getOrElse(HashSet::empty);
		result = superSuperInterfaces.foldLeft(result, Set::add);

		return result;
	}
}
