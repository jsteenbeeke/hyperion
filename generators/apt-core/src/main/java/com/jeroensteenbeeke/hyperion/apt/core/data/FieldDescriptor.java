package com.jeroensteenbeeke.hyperion.apt.core.data;

import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;

import org.jetbrains.annotations.NotNull;

/**
 * Describes a field within a class. This class is immutable, and subclasses are also required to be immutable
 * @param <F> The type of the current class
 */
public abstract class FieldDescriptor<F extends FieldDescriptor<F>> implements Annotated {
	private final Type type;

	private final String name;

	private final boolean isStatic;

	private final boolean isFinal;

	private final List<AnnotationDescriptor> annotations;

	/**
	 * Create a new FieldDescriptor
	 * @param type The (fully qualified) type of the field
	 * @param name The name of the field
	 * @param isStatic Whether or not the field is static
	 * @param isFinal Whether or not the field is final
	 */
	protected FieldDescriptor(Type type, String name, boolean isStatic, boolean isFinal) {
		this.type = type;
		this.name = name;
		this.isStatic = isStatic;
		this.isFinal = isFinal;
		this.annotations = List.empty();
	}

	/**
	 * Create a new FieldDescriptor
	 * @param type The (fully qualified) type of the field
	 * @param name The name of the field
	 * @param isStatic Whether or not the field is static
	 * @param isFinal Whether or not the field is final
	 * @param annotations The annotations applied to this field
	 */
	protected FieldDescriptor(Type type, String name, boolean isStatic, boolean isFinal, List<AnnotationDescriptor> annotations) {
		this.type = type;
		this.name = name;
		this.isStatic = isStatic;
		this.isFinal = isFinal;
		this.annotations = annotations;
	}

	/**
	 * Create a new instance
	 * @param type The (fully qualified) type of the field
	 * @param name The name of the field
	 * @param isStatic Whether or not the field is static
	 * @param isFinal Whether or not the field is final
     * @param annotations The annotations applied to this field
	 * @return The new instance
	 */
	public abstract F newInstance(Type type, String name, boolean isStatic, boolean isFinal, List<AnnotationDescriptor> annotations);

	/**
	 * Add an annotation to the field, yielding a new instance
	 * @param annotation The annotation to add
	 * @return The new instance
	 */
	public F withAnnotation(@NotNull AnnotationDescriptor annotation) {
		return newInstance(type, name, isStatic, isFinal, annotations.append(annotation));
	}

	/**
	 * Returns the type of this field
	 * @return The type
	 */
	public Type getType() {
		return type;
	}

	/**
	 * The name of the field
	 * @return The name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the list of annotations that are tied to this field
	 * @return The list of annotations
	 */
	@Override
	@NotNull
	public List<AnnotationDescriptor> getAnnotations() {
		return annotations;
	}

	/**
	 * Returns the imports required by this field
	 * @return An immutable set of imports
	 */
	public Set<String> getImports() {
		return HashSet.ofAll(type.getImports()).addAll(annotations.flatMap(AnnotationDescriptor::getImports));
	}

	/**
	 * Whether or not the field is declared as static
	 * @return {@code true} if the field is static, {@code false} otherwise
	 */
	public boolean isStatic() {
		return isStatic;
	}

	/**
	 * Whether or not the field is declared as final
	 * @return {@code true} if the field is final, {@code false} otherwise
	 */
	public boolean isFinal() {
		return isFinal;
	}
}
