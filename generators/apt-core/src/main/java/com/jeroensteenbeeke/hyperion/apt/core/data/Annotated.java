package com.jeroensteenbeeke.hyperion.apt.core.data;

import io.vavr.collection.List;

import org.jetbrains.annotations.NotNull;
import java.lang.annotation.Annotation;

/**
 * Provides logic for common annotation-based logic
 */
public interface Annotated {
	/**
	 * Returns the annotations present on the current element
	 * @return the annotations
	 */
	List<AnnotationDescriptor> getAnnotations();

	/**
	 * Determines if the current element is annotated with the given annotation
	 * @param fqdn The FQDN of the annotation we are looking for
	 * @return {@code true} if the annotation is present, {@code false otherwise}
	 */
	default boolean isAnnotatedWith(@NotNull String fqdn) {
		return getAnnotations()
				.map(AnnotationDescriptor::getType)
				.map(ReferenceType::getFQDN)
				.find(fqdn::equals)
				.isDefined();
	}

	/**
	 * Determines if the current element is annotated with the given annotation
	 * @param annotationClass The class of the annotation we are looking for
	 * @return {@code true} if the annotation is present, {@code false otherwise}
	 */
	default boolean isAnnotatedWith(@NotNull Class<? extends Annotation> annotationClass) {
		return isAnnotatedWith(annotationClass.getName());
	}
}
