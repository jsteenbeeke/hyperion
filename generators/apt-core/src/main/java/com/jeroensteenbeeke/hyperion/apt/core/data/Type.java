package com.jeroensteenbeeke.hyperion.apt.core.data;

import io.vavr.collection.Set;
import io.vavr.control.Option;

import org.jetbrains.annotations.NotNull;

/**
 * Represents a Java type
 */
public interface Type extends ImportSupplier {
	/**
	 * Returns the symbol of this type. i.e. the smallest possible representation of this type including generic parameters
	 * that will still compile as a parameter
	 * @return The symbol
	 */
	String getSymbol();

	/**
	 * The fully qualified domain name (package + class name)
	 * @return The FQDN
	 */
	String getFQDN();

	/**
	 * Get a set of all imports required by both the current type and any generics bound to it
	 * @return The imports
	 */
	@Override
	Set<String> getImports();

	/**
	 * Visits the given type
	 * @param visitor The visitor to use
	 * @param <R> The type of return value the visitor yields
	 * @return Optionally the return value of the visitor
	 */
	@NotNull
	<R> Option<R> visit(@NotNull TypeVisitor<R> visitor);
}
