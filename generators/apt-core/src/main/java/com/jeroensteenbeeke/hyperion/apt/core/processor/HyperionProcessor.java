package com.jeroensteenbeeke.hyperion.apt.core.processor;

import com.jeroensteenbeeke.hyperion.apt.core.data.ReferenceType;
import io.vavr.control.Option;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.AnnotatedConstruct;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * Base annotation processor class
 */
public abstract class HyperionProcessor extends AbstractProcessor {
	private Optional<ProcessingEnvironment> processingEnvironment = Optional.empty();

	private Optional<Types> types = Optional.empty();

	@Override
	public synchronized void init(ProcessingEnvironment processingEnv) {
		super.init(processingEnv);
		initEnvironment(processingEnv);
	}

	/**
	 * Sets up local variables
	 *
	 * @param environment The environment to use
	 */
	public void initEnvironment(@NotNull ProcessingEnvironment environment) {
		this.processingEnvironment = Optional.of(environment);
		this.types = processingEnvironment.map(ProcessingEnvironment::getTypeUtils);
	}

	/**
	 * Gets the package name from a given class name
	 *
	 * @param fqdn The fully qualified domain name of the given class
	 * @return The package name
	 */
	protected String extractPackageName(String fqdn) {
		int last = fqdn.lastIndexOf(".");
		int brackets = fqdn.indexOf("<");

		if (brackets != -1 && brackets < last) {
			// Generified type
			return extractPackageName(fqdn.substring(0, brackets));
		}

		return last != -1 ? fqdn.substring(0, last) : "";
	}


	/**
	 * Gets the class name from a given fully qualified domain name
	 *
	 * @param fqdn The fully qualified domain name of the given class
	 * @return The class name
	 */
	protected String extractClass(String fqdn) {
		int last = fqdn.lastIndexOf(".");
		int brackets = fqdn.indexOf("<");

		if (brackets != -1 && brackets < last) {
			// Generified type
			last = fqdn.substring(0, brackets).lastIndexOf('.');
		}

		return last != -1 ? fqdn.substring(last + 1) : fqdn;
	}

	/**
	 * Checks if the given annotation type is of the given fully qualified domain name type
	 *
	 * @param annotationMirror The mirror
	 * @param fqdn             The domain name
	 * @return {@code true} if the annotation is an instance of the given FQDN, {@code false} otherwise
	 */
	protected static boolean isAnnotationMirrorOfType(@NotNull AnnotationMirror annotationMirror,
													  @NotNull String fqdn) {
		return annotationMirror.getAnnotationType().toString().equals(fqdn);
	}

	/**
	 * Gets the annotation mirror from the given element that is an instance of the given qualified domain name
	 *
	 * @param element The element to check for annotations
	 * @param fqdn    The fully qualified domain name
	 * @return An Optional that may contain an AnnotationMirror matching the given FQDN
	 */
	protected static Optional<? extends AnnotationMirror> getAnnotationMirror(@NotNull Element element,
																			  @NotNull String fqdn) {

		return element
				.getAnnotationMirrors()
				.stream()
				.filter(mirror -> isAnnotationMirrorOfType(mirror, fqdn))
				.findFirst();
	}

	/**
	 * Checks if the given element is annotated with any of the annotations given as parameter
	 *
	 * @param element     the element to check for annotations
	 * @param annotations The fully qualified domain names of annotations to check for
	 * @return {@code true} if any of the annotations are matched. {@code false} otherwise
	 */
	protected static boolean containsAnnotation(AnnotatedConstruct element, String... annotations) {
		List<String> annotationClassNames = new ArrayList<>();
		Collections.addAll(annotationClassNames, annotations);

		return element.getAnnotationMirrors().stream()
					  .anyMatch(m -> annotationClassNames.contains(m
																		   .getAnnotationType()
																		   .toString()));
	}

	/**
	 * Checks if the given element is of the expected class
	 *
	 * @param expectedClass The class expected
	 * @param element       The element to check
	 * @return {@code true} if the element is of the given type, {@code false} otherwise
	 */
	protected boolean isInstanceOf(Class<?> expectedClass, TypeElement element) {
		if (expectedClass == null) {
			return false;
		}

		if (element == null) {
			return false;
		}

		return types.map(t -> t.isAssignable(element.asType(),
											 processingEnvironment
													 .map(ProcessingEnvironment::getElementUtils)
													 .map(e -> e.getTypeElement(expectedClass.getName()))
													 .map(TypeElement::asType)
													 .orElse(null)))
					.orElse(false);

	}

	/**
	 * Checks if the given element is of the expected class
	 *
	 * @param expectedClass The class expected
	 * @param type       The type to check
	 * @return {@code true} if the type is of the given class, {@code false} otherwise
	 */
	protected boolean isInstanceOf(@Nullable Class<?> expectedClass, @Nullable ReferenceType type) {
		if (expectedClass == null) {
			return false;
		}

		if (type == null) {
			return false;
		}

		return type.getFQDN().equals(expectedClass.getName());

	}

	/**
	 * Converts the given TypeMirror to a TypeElement for easier processing
	 * @param mirror The mirror to convert
	 * @return Optionally a TypeElement, if the given TypeMirror can be converted
	 */
	@NotNull
	protected Option<TypeElement> toTypeElement(@NotNull TypeMirror mirror) {
		Optional<Element> elementOptional = processingEnvironment.map(ProcessingEnvironment::getTypeUtils)
														 .map(types -> types.asElement(mirror));
		Optional<Element> filteredOptional = elementOptional
				.filter(t -> t instanceof TypeElement);
		return filteredOptional
									.map(t -> (TypeElement) t)
									.map(Option::some)
									.orElseGet(Option::none);
	}
}
