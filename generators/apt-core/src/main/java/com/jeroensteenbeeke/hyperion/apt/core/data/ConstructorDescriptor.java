package com.jeroensteenbeeke.hyperion.apt.core.data;

import io.vavr.collection.List;
import io.vavr.collection.Set;

import org.jetbrains.annotations.NotNull;
import java.util.Objects;

/**
 * Representation of a class constructor. Mainly used to keep track of required parameters
 */
public class ConstructorDescriptor implements Annotated, ImportSupplier {
	private final List<ParameterDescriptor> parameters;

	private final List<AnnotationDescriptor> annotations;

	/**
	 * Constructor
	 */
	public ConstructorDescriptor() {
		this(List.empty(), List.empty());
	}

	private ConstructorDescriptor(@NotNull List<ParameterDescriptor> parameters, @NotNull List<AnnotationDescriptor> annotations) {
		this.parameters = parameters;
		this.annotations = annotations;
	}

	/**
	 * Creates a new constructor instance with the added parameter
	 * @param parameterDescriptor The paramter
	 * @return A new constructor descriptor
	 */
	@NotNull
	public ConstructorDescriptor withParameter(@NotNull ParameterDescriptor parameterDescriptor) {
		return new ConstructorDescriptor(parameters.append(parameterDescriptor), annotations);
	}

	/**
	 * Creates a new constructor instance with the added annotation
	 * @param annotation The annotation
	 * @return A new constructor descriptor
	 */
	public ConstructorDescriptor withAnnotation(@NotNull AnnotationDescriptor annotation) {
		return new ConstructorDescriptor(parameters, annotations.append(annotation));
	}

	/**
	 * Returns the annotations associated with this constructor
	 * @return The annotations
	 */
	public List<AnnotationDescriptor> getAnnotations() {
		return annotations;
	}

	/**
	 * Returns the parameters belonging to this constructor
	 * @return The parameters
	 */
	public List<ParameterDescriptor> getParameters() {
		return parameters;
	}

	@Override
	public Set<String> getImports() {
		return join(List.of(getParameters(), getAnnotations()));
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof ConstructorDescriptor)) return false;
		ConstructorDescriptor that = (ConstructorDescriptor) o;
		return Objects.equals(parameters, that.parameters);
	}

	@Override
	public int hashCode() {
		return Objects.hash(parameters);
	}
}
