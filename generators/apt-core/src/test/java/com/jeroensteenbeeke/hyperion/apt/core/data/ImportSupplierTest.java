package com.jeroensteenbeeke.hyperion.apt.core.data;

import com.google.testing.compile.Compilation;
import com.google.testing.compile.CompilationSubject;
import com.google.testing.compile.JavaFileObjects;
import com.jeroensteenbeeke.hyperion.annotation.Buildable;
import com.jeroensteenbeeke.hyperion.apt.core.processor.extended.ExtendedClassWithFields;
import com.jeroensteenbeeke.hyperion.apt.core.processor.extended.ExtendedHyperionClassProcessor;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.tools.JavaFileObject;
import java.io.*;
import java.lang.annotation.Annotation;

import static com.google.testing.compile.Compiler.javac;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class ImportSupplierTest {
	@Test
	public void annotationImportSupplierTest() {
		JavaFileObject file =
				JavaFileObjects.forSourceLines("com.jeroensteenbeeke.novelcrafter.app.model.Novel",
						"""
																
								package com.jeroensteenbeeke.novelcrafter.app.model;
								        
								import com.jeroensteenbeeke.hyperion.annotation.Buildable;
								        
								import io.vavr.Function1;
								        
								import jakarta.annotation.Generated;
								import org.jetbrains.annotations.NotNull;
								import org.jetbrains.annotations.Nullable;
								        
								@Generated(value = "com.jeroensteenbeeke.hyperion.immutable.generator.GeneratorMojo", date = "2022-01-31T10:42:16.22017952")
								public class Novel {
								    private final String title;
								        
								    @Buildable
								    public Novel(
								        @NotNull String title    ) {
								        this.title = title;
								    }
								        
								    @NotNull
								    public String getTitle() {
								        return this.title;
								    }
								        
								    @NotNull
								    public Novel withTitle(@NotNull String title) {
								        return new Novel(title);
								    }
								        
								    @NotNull
								    public Novel withTitle(@NotNull Function1<String,String> titleFunction) {
								        return new Novel(titleFunction.apply(title));
								    }
								}
																
								""");

		Compilation compilation = javac().withOptions("-source", "17", "-target", "17", "-verbose", "-cp", System.getProperty("java" +
						".class.path"))
				.withProcessors(new DummyGenerator())
				.compile
						(file);

		CompilationSubject.assertThat(compilation).succeededWithoutWarnings();

	}
}

@SupportedAnnotationTypes("com.jeroensteenbeeke.hyperion.annotation.Buildable")
@SupportedSourceVersion(SourceVersion.RELEASE_21)
class DummyGenerator extends ExtendedHyperionClassProcessor {

	@Override
	protected String getGeneratorName() {
		return "DummyGenerator";
	}

	@Override
	protected List<Class<? extends Annotation>> getRequiredAnnotations() {
		return List.of(Buildable.class);
	}

	@Override
	protected void generateCode(@NotNull ExtendedClassWithFields classDescriptor) {
		assertThat(classDescriptor.getImports(), not(hasItem(startsWith("@"))));
	}
}
