package com.jeroensteenbeeke.hyperion.apt.core.data;

import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import io.vavr.collection.Set;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;


import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class GenerifiedTypeTest {
	@Test
	public void testGenerifiedTypeAnalysis() {
		whenTranslating("int").thenClassNameShouldBe("int").withPackageName("").andSymbolShouldBe("int")
				.withoutImports().withoutTypeParams();

		whenTranslating("java.lang.String").thenClassNameShouldBe("String").withPackageName("java.lang")
				.andSymbolShouldBe("String").withoutImports().withoutTypeParams();

		whenTranslating("java.util.List<java.lang.String>").thenClassNameShouldBe("List")
				.withPackageName("java.util")
				.andSymbolShouldBe("List<String>")
				.andImportsShouldBe("java.util.List")
				.andTypeParamsShouldAdhereTo(
						aParameter("String").withPackageName("java.lang")
								.andSymbolShouldBe("String").withoutImports().withoutTypeParams()
				);

		whenTranslating("java.util.Map<java.lang.String,java.util.Map<java.lang.String,java.lang.Integer>>")
				.thenClassNameShouldBe("Map")
				.withPackageName("java.util")
				.andSymbolShouldBe("Map<String,Map<String,Integer>>")
				.andImportsShouldBe("java.util.Map")
				.andTypeParamsShouldAdhereTo(
						aParameter("String").withPackageName("java.lang")
								.andSymbolShouldBe("String"
								).withoutImports().withoutTypeParams(),
						aParameter("Map").withPackageName("java.util")
								.andSymbolShouldBe("Map<String,Integer>")
								.andImportsShouldBe("java.util.Map")
								.andTypeParamsShouldAdhereTo(
										aParameter("String").withPackageName("java.lang")
												.andSymbolShouldBe("String"
												).withoutImports().withoutTypeParams(),
										aParameter("Integer").withPackageName("java.lang")
												.andSymbolShouldBe("Integer"
												).withoutImports().withoutTypeParams()
								)
				).check();
	}

	@Test
	public void translateJava19StringRepresentations() {
		whenTranslating("io.vavr.collection.@org.jetbrains.annotations.NotNull Array<java.lang.String>")
				.thenClassNameShouldBe("Array")
				.withPackageName("io.vavr.collection")
				.andSymbolShouldBe("Array<String>")
				.andImportsShouldBe("io.vavr.collection.Array")
				.andTypeParamsShouldAdhereTo(
						aParameter("String")
								.withPackageName("java.lang")
								.andSymbolShouldBe("String")
								.withoutImports()
								.withoutTypeParams()
				).check();

		whenTranslating(
				"io.vavr.collection.@org.jetbrains.annotations.NotNull List<java.lang.String>").thenClassNameShouldBe(
						"List")
				.withPackageName("io.vavr.collection")
				.andSymbolShouldBe("List<String>")
				.andImportsShouldBe("io.vavr.collection.List")
				.andTypeParamsShouldAdhereTo(
						aParameter("String")
								.withPackageName("java.lang")
								.andSymbolShouldBe("String")
								.withoutImports()
								.withoutTypeParams()
				).check();
		whenTranslating("io.vavr.collection.@org.jetbrains.annotations.NotNull HashSet<java.lang.String>")
				.thenClassNameShouldBe("HashSet")
				.withPackageName("io.vavr.collection")
				.andSymbolShouldBe("HashSet<String>")
				.andImportsShouldBe("io.vavr.collection.HashSet")
				.andTypeParamsShouldAdhereTo(
						aParameter("String")
								.withPackageName("java.lang")
								.andSymbolShouldBe("String")
								.withoutImports()
								.withoutTypeParams()
				).check();
		whenTranslating(
				"io.vavr.collection.@org.jetbrains.annotations.NotNull TreeSet<java.lang.String>").thenClassNameShouldBe(
						"TreeSet")
				.withPackageName("io.vavr.collection")
				.andSymbolShouldBe("TreeSet<String>")
				.andImportsShouldBe("io.vavr.collection.TreeSet")
				.andTypeParamsShouldAdhereTo(
						aParameter("String")
								.withPackageName("java.lang")
								.andSymbolShouldBe("String")
								.withoutImports()
								.withoutTypeParams()
				).check();

	}

	public ThenClassnameShouldBe<Checkable> whenTranslating(@NotNull String representation) {
		return className -> packageName -> symbol -> imports -> params -> () -> {
			GenerifiedType generifiedType = GenerifiedType.fromRepresentation(representation);
			applyCheck(generifiedType, className, packageName, symbol, HashSet.of(imports), List.of(params));
		};
	}

	public WithPackageName<ParameterChecker> aParameter(@NotNull String className) {
		return packageName -> symbol -> imports -> params -> generifiedType -> applyCheck(generifiedType, className,
				packageName, symbol, HashSet.of(imports), List.of(params));
	}

	private void applyCheck(
			@NotNull GenerifiedType generifiedType, @NotNull String className, @NotNull String packageName,
			@NotNull String symbol,
			@NotNull Set<String> imports, @NotNull List<ParameterChecker> parameterCheckers) {
		assertThat("Classname should be %s".formatted(className), generifiedType.getClassName(),
				equalTo(className));
		assertThat("Package name should be %s".formatted(packageName), generifiedType.getPackageName(),
				equalTo(packageName));
		assertThat("Symbol should be %s".formatted(symbol), generifiedType.getSymbol(),
				equalTo(symbol));
		assertThat("Imports should be %s".formatted(symbol), generifiedType.getImports(),
				equalTo(imports));

		assertThat("Generic type should have %d parameters".formatted(parameterCheckers.size()),
				generifiedType.getGenericParameters().size(), equalTo(parameterCheckers.size()));

		int i = 0;
		for (ParameterChecker parameterChecker : parameterCheckers) {
			parameterChecker.check(generifiedType.getGenericParameters().get(i++));
		}
	}


	private interface ThenClassnameShouldBe<T> {
		WithPackageName<T> thenClassNameShouldBe(@NotNull String className);
	}

	private interface WithPackageName<T> {
		AndSymbolShouldBe<T> withPackageName(@NotNull String packageName);
	}

	private interface AndSymbolShouldBe<T> {
		AndImportsShouldBe<T> andSymbolShouldBe(@NotNull String symbol);
	}

	private interface AndImportsShouldBe<T> {
		AndTypeParamsShouldAdhereTo<T> andImportsShouldBe(@NotNull String... imports);

		default AndTypeParamsShouldAdhereTo<T> withoutImports() {
			return andImportsShouldBe();
		}
	}

	private interface AndTypeParamsShouldAdhereTo<T> {
		T andTypeParamsShouldAdhereTo(@NotNull ParameterChecker... predicates);

		default T withoutTypeParams() {
			return andTypeParamsShouldAdhereTo();
		}
	}

	private interface Checkable {
		void check();
	}

	private interface ParameterChecker {
		void check(@NotNull GenerifiedType generifiedType);
	}
}
