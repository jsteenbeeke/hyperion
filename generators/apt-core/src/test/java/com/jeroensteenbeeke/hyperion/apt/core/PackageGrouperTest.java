package com.jeroensteenbeeke.hyperion.apt.core;

import com.jeroensteenbeeke.hyperion.apt.core.data.AnnotationDescriptor;
import com.jeroensteenbeeke.hyperion.apt.core.data.ClassWithFields;
import com.jeroensteenbeeke.hyperion.apt.core.data.FieldDescriptor;
import com.jeroensteenbeeke.hyperion.apt.core.data.Type;
import io.vavr.collection.HashSet;
import io.vavr.collection.List;
import org.junit.jupiter.api.Test;

import org.jetbrains.annotations.NotNull;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class PackageGrouperTest {
	@Test
	public void testPackageGrouper() {
		var grouper = new PackageGrouper<>(TestClassWithFields::getPackageName)
				.withClass(new TestClassWithFields("A", "com.a.c.d"))
				.withClass(new TestClassWithFields("B", "com.a.c.d.e"));

		assertThat(grouper.groupClassesByRootNode().keySet().size(), equalTo(1));
		assertThat(grouper.getBasePackages(), equalTo(HashSet.of("com.a.c.d")));

		grouper = new PackageGrouper<>(TestClassWithFields::getPackageName)
				.withClass(new TestClassWithFields("A", "com.a.c.d"))
				.withClass(new TestClassWithFields("B", "com.a.c.e"));

		assertThat(grouper.groupClassesByRootNode().keySet().size(), equalTo(1));
		assertThat(grouper.getBasePackages(), equalTo(HashSet.of("com.a.c")));

		grouper = new PackageGrouper<>(TestClassWithFields::getPackageName)
				.withClass(new TestClassWithFields("A", "com.a.c.d"))
				.withClass(new TestClassWithFields("B", "net.a.c.e"));

		assertThat(grouper.groupClassesByRootNode().keySet().size(), equalTo(2));
		assertThat(grouper.getBasePackages(), equalTo(HashSet.of("com.a.c.d", "net.a.c.e")));


	}

	private static class TestClassWithFields extends ClassWithFields<TestClassWithFields, TestFieldDescriptor> {
		public TestClassWithFields(@NotNull String className, @NotNull String packageName) {
			super(className, packageName);
		}

		private TestClassWithFields(@NotNull String className, @NotNull String packageName, @NotNull List<TestFieldDescriptor> fields, @NotNull List<AnnotationDescriptor> annotations) {
			super(className, packageName, fields, annotations);
		}

		@NotNull
		@Override
		protected TestClassWithFields newInstance(@NotNull String className,
												  @NotNull String packageName,
												  @NotNull List<TestFieldDescriptor> fields,
												  @NotNull List<AnnotationDescriptor> annotations) {
			return new TestClassWithFields(className, packageName, fields, annotations);
		}
	}

	private static class TestFieldDescriptor extends FieldDescriptor<TestFieldDescriptor> {

		public TestFieldDescriptor(Type type, String name, boolean isStatic, boolean isFinal) {
			super(type, name, isStatic, isFinal);
		}

		private TestFieldDescriptor(Type type, String name, boolean isStatic, boolean isFinal, List<AnnotationDescriptor> annotations) {
			super(type, name, isStatic, isFinal, annotations);
		}

		@Override
		public TestFieldDescriptor newInstance(Type type,
											   String name,
											   boolean isStatic,
											   boolean isFinal,
											   List<AnnotationDescriptor> annotations) {
			return new TestFieldDescriptor(type, name, isStatic, isFinal, annotations);
		}
	}
}
