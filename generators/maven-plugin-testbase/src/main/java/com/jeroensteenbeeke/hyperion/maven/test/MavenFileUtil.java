package com.jeroensteenbeeke.hyperion.maven.test;

import java.io.File;

/**
 * File utilities copied from plexus utils to circumvent JUnit 3 dependency
 */
public class MavenFileUtil {
	/**
	 * Resolves a file relative to a specified basedir
	 * @param path The path to resolve
	 * @return A File object representing the file
	 */
	public static File getTestFile(String path) {
		return new File(getBasedir(), path);
	}

	/**
	 * Determines the test execution context
	 * @return The base directory
	 */
	public static String getBasedir() {
		return Lazy.BASEDIR;
	}

	private static final class Lazy {
		static final String BASEDIR;

		static {
			String path = System.getProperty("basedir");
			BASEDIR = path != null ? path : (new File("")).getAbsolutePath();
		}

		private Lazy() {
		}
	}
}
