package com.jeroensteenbeeke.hyperion.maven.test;

import com.github.codeteapot.maven.plugin.testing.MavenPluginContext;
import org.apache.maven.project.MavenProject;
import org.jetbrains.annotations.NotNull;

import java.io.File;

/**
 * Factory for creating test harness Maven configurations
 */
public class HyperionMavenConfiguration {
	/**
	 * Sets up the given context to inject a Maven Project stub implementation
	 * @param context The context to configure
	 * @param baseDir The base directory of the plugin project
	 */
	public static void configure(@NotNull MavenPluginContext context, @NotNull File baseDir) {
		context.inject(new MavenProjectStub(baseDir), MavenProject.class.getName());
	}

}
