package com.jeroensteenbeeke.hyperion.listrefgen;

import com.jeroensteenbeeke.hyperion.apt.core.processor.HyperionProcessor;
import com.jeroensteenbeeke.hyperion.rest.annotation.Model;
import com.jeroensteenbeeke.hyperion.rest.annotations.Queryable;
import io.vavr.collection.Multimap;
import io.vavr.collection.TreeMultimap;
import org.jetbrains.annotations.NotNull;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import javax.tools.FileObject;
import javax.tools.StandardLocation;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.Collections;
import java.util.Date;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Generator for creating generictype implementations for classes annotated with Queryable
 */
@SupportedAnnotationTypes({"com.jeroensteenbeeke.hyperion.rest.annotations.Queryable", "io.swagger.annotations.ApiModel"})
@SupportedSourceVersion(SourceVersion.RELEASE_21)
public class ListReferenceGenerator extends HyperionProcessor {
	private Multimap<String, String> packagesToClasses;

	@Override
	public boolean process(@NotNull Set<? extends TypeElement> annotations, @NotNull RoundEnvironment roundEnv) {
		packagesToClasses = TreeMultimap.withSet().empty();

		Set<? extends Element> annotatedElements = joinSets(roundEnv
				.getElementsAnnotatedWith(Queryable.class), roundEnv.getElementsAnnotatedWith(
				Model.class));
		processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE,
				"ListReferenceGenerator found " + annotatedElements.size() + " eligible classes");

		Consumer<TypeElement> classHandler = e -> {
			String fqdn = e.getQualifiedName().toString();

			int finalDot = fqdn.lastIndexOf('.');
			String pkg = finalDot == -1 ? "" : fqdn.substring(0, finalDot);
			String className = finalDot == -1 ? fqdn : fqdn.substring(finalDot + 1);

			packagesToClasses = packagesToClasses.put(pkg, className);
		};

		annotatedElements.stream()
				.filter(e -> e.getKind() == ElementKind.CLASS)
				.map(e -> (TypeElement) e)
				.forEach(classHandler);

		annotatedElements.stream()
				.filter(e -> e.getKind() == ElementKind.FIELD)
				.map(Element::getEnclosingElement)
				.filter(e -> e.getKind() == ElementKind.CLASS)
				.map(e -> (TypeElement) e)
				.forEach(classHandler);

		packagesToClasses.asMap().forEach((pkg, classes) -> {
			try {
				String fqdn = pkg.concat(".").concat("ListOf");

				Filer filer = processingEnv.getFiler();
				FileObject classFile;
				try {
					classFile = filer.createSourceFile(fqdn);
				} catch (FilerException f) {
					// File already exists, get resource instead to overwrite
					classFile = filer.getResource(StandardLocation.SOURCE_OUTPUT, pkg,
							"ListOf.java");
				}

				try (Writer fw = classFile.openWriter(); PrintWriter pw = new PrintWriter(fw)) {
					pw.print("package ");
					pw.print(pkg);
					pw.println(";");
					pw.println();
					pw.println("import java.util.List;");
					pw.println("import jakarta.annotation.Generated;");
					pw.println("import jakarta.ws.rs.core.GenericType;");
					pw.println();
					pw.printf(
							"@Generated(value = \"com.jeroensteenbeeke.hyperion.listrefgen.ListReferenceGenerator\",\n date="
									+
									" " +
									"\"%s\")",
							getDateString());
					pw.println();
					pw.println("public class ListOf {");
					for (String className : classes) {
						pw.print("\tpublic static final GenericType<List<");
						pw.print(className);
						pw.print(">> ");
						pw.print(className.toUpperCase());
						pw.print(" = new GenericType<List<");
						pw.print(className);
						pw.println(">>() {};");
						pw.println();
					}
					pw.println("}");
					pw.println();
				}
			} catch (IOException e) {
				processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, e.getMessage());
			}
		});

		return false;
	}

	@NotNull
	private Set<? extends Element> joinSets(@NotNull
			Set<? extends Element> a, @NotNull Set<? extends Element> b) {
		Set<Element> result = new java.util.HashSet<>();

		result.addAll(a);
		result.addAll(b);

		return Collections.unmodifiableSet(result);
	}

	String getDateString() {
		return new Date().toString();
	}
}
