package com.jeroensteenbeeke.hyperion.listrefgen;

import com.jeroensteenbeeke.hyperion.rest.annotation.Model;

import jakarta.ws.rs.Path;

/**
 * Dependencies used by generated code, but not detected by dependency plugin
 */
public class FakeReferences {
	public FakeReferences() {
		System.out.println(Path.class.getName());
		System.out.println(Model.class.getName());
	}
}
