    ${nonnull}
    ${modifier} ${returnType} ${keyword}${field.name?cap_first}(${field.elementType}... elements) {
<#switch field.collectionType>
    <#case "java.util.List">
        return ${keyword}${field.name?cap_first}(new java.util.ArrayList<>(java.util.List.of(elements)));
        <#break>
    <#case "java.util.Set">
        return ${keyword}${field.name?cap_first}(new java.util.HashSet<>(java.util.Set.of(elements)));
        <#break>
    <#case "io.vavr.collection.Array">
        return ${keyword}${field.name?cap_first}(Array.of(elements));
        <#break>
    <#case "io.vavr.collection.List">
        return ${keyword}${field.name?cap_first}(List.of(elements));
        <#break>
    <#case "io.vavr.collection.HashSet">
        return ${keyword}${field.name?cap_first}(HashSet.of(elements));
        <#break>
    <#case "io.vavr.collection.TreeSet">
        return ${keyword}${field.name?cap_first}(TreeSet.of(elements));
        <#break>
</#switch>
    }
