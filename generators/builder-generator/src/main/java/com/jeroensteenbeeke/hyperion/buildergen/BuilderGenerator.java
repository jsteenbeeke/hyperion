package com.jeroensteenbeeke.hyperion.buildergen;

import com.jeroensteenbeeke.hyperion.annotation.Buildable;
import com.jeroensteenbeeke.hyperion.apt.core.data.ConstructorDescriptor;
import com.jeroensteenbeeke.hyperion.apt.core.data.ParameterDescriptor;
import com.jeroensteenbeeke.hyperion.apt.core.data.PrimitiveType;
import com.jeroensteenbeeke.hyperion.apt.core.processor.extended.ExtendedClassWithFields;
import com.jeroensteenbeeke.hyperion.apt.core.processor.extended.ExtendedHyperionClassProcessor;
import com.jeroensteenbeeke.hyperion.mojo.core.GeneratedType;
import com.jeroensteenbeeke.hyperion.mojo.core.NullabilityAnnotationType;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import io.vavr.Tuple2;
import io.vavr.collection.*;
import io.vavr.control.Option;
import org.jetbrains.annotations.NotNull;

import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.util.Date;
import java.util.Optional;

/**
 * Annotation processor that creates builders
 */
@SupportedAnnotationTypes(BuilderGenerator.BUILDABLE)
@SupportedSourceVersion(SourceVersion.RELEASE_21)
public class BuilderGenerator extends ExtendedHyperionClassProcessor {
	static final String BUILDABLE = "com.jeroensteenbeeke.hyperion.annotation.Buildable";

	private static String commaSeparate(String a, String b) {
		return a + ", " + b;
	}

	private NullabilityAnnotationType annotationType;

	private GeneratedType generatedType;

	@Override
	protected String getGeneratorName() {
		return getClass().getName();
	}

	@Override
	protected List<Class<? extends Annotation>> getRequiredAnnotations() {
		return List.of(Buildable.class);
	}

	@Override
	protected Option<String> validate(@NotNull io.vavr.collection.Set<ExtendedClassWithFields> classes) {
		checkInitialization();

		return classes.filter(eClass -> findBuildableConstructor(eClass).size() > 1)
				.map(ExtendedClassWithFields::getFQDN)
				.reduceOption(BuilderGenerator::commaSeparate)
				.map(offending -> "Found classes with more than 1 constructor with @Buildable annotation" + offending)
				.orElse(() -> classes
						.flatMap(c -> c.getConstructors().map(ct -> new Tuple2<>(c, ct)))
						.flatMap(t -> t._2().getParameters().map(p -> t.map2(ct -> p)))
						.filter(t -> isMissingNullabilityAnnotation(t._2) && !(t._2.getType() instanceof PrimitiveType))
						.map(t -> String.format("Constructor parameter '%s' in class %s", t._2.getName(),
								t._1.getFQDN()))
						.reduceOption(BuilderGenerator::commaSeparate)
						.map(offending -> "Found constructors with missing %s and/or %s annotations: %s".formatted(
								annotationType.getNonnullFQDN(), annotationType.getNullableFQDN(), offending)))
				;
	}

	private void checkInitialization() {
		if (annotationType == null) {
			annotationType = Optional.ofNullable(processingEnv.getOptions().get("annotationType"))
					.map(NullabilityAnnotationType::valueOf).orElse(NullabilityAnnotationType.Jetbrains);
		}
		if (generatedType == null) {
			generatedType = Optional.ofNullable(processingEnv.getOptions().get("generatedType"))
					.map(GeneratedType::valueOf).orElse(GeneratedType.JakartaEE);
		}
	}

	@SuppressWarnings("deprecation")
	private boolean isMissingNullabilityAnnotation(ParameterDescriptor descriptor) {
		return switch (annotationType) {
			case Findbugs -> !descriptor.isAnnotatedWith(NullabilityAnnotationType.Findbugs.getNonnullFQDN()) &&
					!descriptor.isAnnotatedWith(NullabilityAnnotationType.Findbugs.getNullableFQDN());
			case Jetbrains -> !descriptor.isAnnotatedWith(NullabilityAnnotationType.Jetbrains.getNonnullFQDN()) &&
					!descriptor.isAnnotatedWith(NullabilityAnnotationType.Jetbrains.getNullableFQDN());
		};
	}

	@Override
	protected void generateCode(@NotNull ExtendedClassWithFields target) {
		List<ConstructorDescriptor> constructors = findBuildableConstructor(target);
		// Size should be 0 or 1 due to validation step
		for (ConstructorDescriptor constructor : constructors) {
			List<ParameterDescriptor> allParameters = List.empty();
			List<ParameterDescriptor> required = List.empty();
			List<ParameterDescriptor> optional = List.empty();

			for (ParameterDescriptor parameter : constructor.getParameters()) {
				if (isRequired(parameter)) {
					required = required.append(parameter);
				} else if (isOptional(parameter)) {
					optional = optional.append(parameter);
				} else if (parameter.getType() instanceof PrimitiveType) {
					required = required.append(parameter);
				} else {
					throw new IllegalStateException(
							"Missing %s and/or %s annotation on parameter '%s' of class %s. This should have been caught by validation".formatted(
									annotationType.getNonnullFQDN(), annotationType.getNullableFQDN(),
									parameter.getName(),
									target.getFQDN()
							));
				}

				allParameters = allParameters.append(parameter);
			}

			boolean hasOptional = !optional.isEmpty();

			Set<String> imports = target.getImports().addAll(generatedType.getImports())
					.remove("com.jeroensteenbeeke.hyperion.annotation.Buildable");

			imports = constructor.getParameters().foldLeft(imports, (imp, param) -> {
				if (isRequired(param)) {
					return imp.add(annotationType.getNonnullFQDN());
				} else if (isOptional(param)) {
					return imp.add(annotationType.getNullableFQDN());
				}

				return imp;
			});

			HashMap<String, Object> dataModel = HashMap.of("imports",
					imports.add(annotationType.getNonnullFQDN()).toJavaSet(),
					"allFields", allParameters.map(BuildableField::new),
					"requiredFields", required.map(BuildableField::new),
					"optionalFields", optional.map(BuildableField::new),
					"package", target.getPackageName(),
					"target", target.getClassName(),
					"nonnull", annotationType.getNonnullToken(),
					"generated", generatedType.getToken(),
					"generationDate", getDateString(),
					"article", guessArticle(target.getClassName()));

			Configuration configuration = new Configuration(Configuration.VERSION_2_3_32);
			configuration.setClassForTemplateLoading(BuilderGenerator.class, "templates");
			configuration.setDefaultEncoding("UTF-8");

			String generated = "";

			try {
				JavaFileObject classFile = processingEnv
						.getFiler()
						.createSourceFile(target.getFQDN() + "Builder");

				try (Writer out = classFile.openWriter()) {
					Template template = configuration.getTemplate(
							hasOptional ? "optional-builder.ftl" : "non-optional-builder.ftl");

					template.process(deVavr(dataModel), out);
				}
			} catch (IOException | TemplateException e) {
				System.out.println(generated);
				throw new RuntimeException(e);
			}

		}
	}

	@SuppressWarnings("deprecation")
	private boolean isRequired(ParameterDescriptor parameter) {
		return switch (annotationType) {
			case Findbugs -> parameter.isAnnotatedWith(NullabilityAnnotationType.Findbugs.getNonnullFQDN());
			case Jetbrains -> parameter.isAnnotatedWith(NullabilityAnnotationType.Jetbrains.getNonnullFQDN());
		};
	}

	@SuppressWarnings("deprecation")
	private boolean isOptional(ParameterDescriptor parameter) {
		return switch (annotationType) {
			case Findbugs -> parameter.isAnnotatedWith(NullabilityAnnotationType.Findbugs.getNullableFQDN());
			case Jetbrains -> parameter.isAnnotatedWith(NullabilityAnnotationType.Jetbrains.getNullableFQDN());
		};
	}

	private List<ConstructorDescriptor> findBuildableConstructor(@NotNull ExtendedClassWithFields classWithFields) {
		return classWithFields.getConstructors()
				.filter(c -> c.isAnnotatedWith(Buildable.class.getName()));
	}


	private static final TreeSet<String> VOWEL_EXCEPTIONS = TreeSet.of(
			"hour",
			"honor",
			"honorary",
			"heir",
			"heirloom",
			"herb",
			"humble",
			"homage"
	);


	private String guessArticle(String word) {
		char firstChar = word.toLowerCase().charAt(0);

		if (VOWEL_EXCEPTIONS.contains(word.toLowerCase())) {
			return "an";
		} else if (word.matches("[A-Z]{2}.*")) {
			// Abbreviation, judge by sound
			switch (firstChar) {
				case 'a':
				case 'e':
				case 'f':
				case 'h':
				case 'i':
				case 'l':
				case 'm':
				case 'n':
				case 'o':
				case 'r':
				case 's':
				case 'u':
				case 'x':
					return "an";
				default:
					return "a";
			}
		} else if (word.toLowerCase().startsWith("uni") && !word.startsWith("unin")) {
			// Universe, unique, etc. are pronounced you-niverse, you-nique. But unintended is not
			return "a";
		}

		// Standard rule: vowels get an, consonants get a
		switch (firstChar) {
			case 'a':
			case 'e':
			case 'i':
			case 'o':
			case 'u':
				return "an";
			default:
				return "a";
		}
	}

	private java.util.Map<String, Object> deVavr(HashMap<String, Object> dataModel) {
		java.util.Map<String, Object> target = new java.util.HashMap<>();

		dataModel.forEach((k, v) -> {
			if (v instanceof io.vavr.collection.Set) {
				target.put(k, ((io.vavr.collection.Set<?>) v).toJavaSet());
			} else if (v instanceof io.vavr.collection.Seq) {
				target.put(k, ((Seq<?>) v).toJavaList());
			} else {
				target.put(k, v);
			}
		});

		return target;
	}

	/**
	 * Gets the generation date as String
	 *
	 * @return The generation date
	 */
	protected String getDateString() {
		return new Date().toString();
	}
}
