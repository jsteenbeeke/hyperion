package ${package};

<#list imports as import>
import ${import};
</#list>

${generated}(date = "${generationDate}", value = "com.jeroensteenbeeke.hyperion.buildergen.BuilderGenerator")
public class ${target}Builder {
    ${nonnull}
    public static With${requiredFields?first.name?cap_first} ${article}${target}() {
        return <#list requiredFields as field>${field.name} -> </#list> new ${target}(
            <#list allFields as field>${field.name}<#sep>, </#list>
        );
    }

<#list requiredFields as field>
    <#assign modifier>default</#assign>
    <#if field_has_next>
        <#assign returnType>With${requiredFields[field?index + 1].name?cap_first}</#assign>
        <#assign keyword>with</#assign>
    <#else>
        <#assign returnType>${target}</#assign>
        <#assign keyword>and</#assign>
    </#if>
    public interface With${field.name?cap_first} {
    ${nonnull}
    ${returnType} ${keyword}${field.name?cap_first}(${field.annotation}${field.type} ${field.name});
    <#if field.collection>
        <#include "varargs.ftl" parse=true>
    </#if>
    }
</#list>
}
