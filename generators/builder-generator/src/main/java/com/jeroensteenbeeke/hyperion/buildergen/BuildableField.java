package com.jeroensteenbeeke.hyperion.buildergen;

import com.jeroensteenbeeke.hyperion.apt.core.data.GenerifiedType;
import com.jeroensteenbeeke.hyperion.apt.core.data.ParameterDescriptor;
import com.jeroensteenbeeke.hyperion.apt.core.data.ReferenceType;
import com.jeroensteenbeeke.hyperion.apt.core.data.TypeVisitor;
import com.jeroensteenbeeke.hyperion.mojo.core.NullabilityAnnotationType;
import io.vavr.Tuple2;
import io.vavr.control.Option;

import org.jetbrains.annotations.NotNull;
import java.util.Set;

/**
 * Field reference for use in Templates
 */
public class BuildableField {
	private static final Set<String> LIST_TYPES = Set.of(
			"java.util.List",
			"java.util.Set",
			"io.vavr.collection.HashSet",
			"io.vavr.collection.TreeSet",
			"io.vavr.collection.Array",
			"io.vavr.collection.List"
	);
	private final String annotation;

	private final String type;

	private final String name;

	private final String elementType;

	private final String collectionType;

	BuildableField(ParameterDescriptor descriptor) {
		this.annotation = descriptor.getType().visit(new TypeVisitor<String>() {
			@NotNull
			@Override
			public Option<String> visit(@NotNull ReferenceType type) {
				for (NullabilityAnnotationType annotationType : NullabilityAnnotationType.values()) {
					if (descriptor.isAnnotatedWith(annotationType.getNonnullFQDN())) {
						return Option.some("%s ".formatted(annotationType.getNonnullToken()));
					} else if (descriptor.isAnnotatedWith(annotationType.getNullableFQDN())) {
						return Option.some("%s ".formatted(annotationType.getNullableToken()));
					}
				}


				return Option.none();
			}

			@NotNull
			@Override
			public Option<String> visit(@NotNull GenerifiedType type) {
				for (NullabilityAnnotationType annotationType : NullabilityAnnotationType.values()) {
					if (descriptor.isAnnotatedWith(annotationType.getNonnullFQDN())) {
						return Option.some("%s ".formatted(annotationType.getNonnullToken()));
					} else if (descriptor.isAnnotatedWith(annotationType.getNullableFQDN())) {
						return Option.some("%s ".formatted(annotationType.getNullableToken()));
					}
				}

				return Option.none();
			}
		}).getOrElse("");

		this.type = descriptor.getType().getSymbol();
		this.name = descriptor.getName();

		Option<Tuple2<String,String>> genericData = descriptor.getType().visit(new TypeVisitor<>() {
			@NotNull
			@Override
			public Option<Tuple2<String, String>> visit(@NotNull GenerifiedType type) {
				if (LIST_TYPES.contains(type.getFQDN())) {
					return Option.some(new Tuple2<>(type.getFQDN(), type.getGenericParameters().get(0).getClassName()));
				}

				return Option.none();
			}
		});

		this.elementType = genericData.map(Tuple2::_2).getOrNull();
		this.collectionType = genericData.map(Tuple2::_1).getOrNull();
	}

	/**
	 * Get the annotation required for this field. Includes trailing space
	 *
	 * @return An annotation, or empty string
	 */
	public String getAnnotation() {
		return annotation;
	}

	/**
	 * Gets the type of the field
	 *
	 * @return The type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Gets the name of the field
	 *
	 * @return The name
	 */
	public String getName() {
		return name;
	}

	/**
	 * If the current field is a list or set, returns the type of element contained in the list/set
	 *
	 * @return The type in the element
	 */
	public String getElementType() {
		return elementType;
	}

	/**
	 * If the current field is a list or set, returns the type of list or set
	 *
	 * @return The collection type
	 */
	public String getCollectionType() {
		return collectionType;
	}

	/**
	 * Convenience method for checking whether the type represents a collection
	 * @return {@code true} if the type is a collection, {@code false} otherwise
	 */
	public boolean isCollection() {
		return elementType != null && collectionType != null;
	}
}
