package ${package};

<#list imports as import>
import ${import};
</#list>

${generated}(date = "${generationDate}", value = "com.jeroensteenbeeke.hyperion.buildergen.BuilderGenerator")
public class ${target}Builder {
<#if requiredFields?size = 0>
    ${nonnull}
    public static Finalizer ${article}${target}() {
        return new Finalizer();
    }
<#else>
    ${nonnull}
    public static With${requiredFields?first.name?cap_first} ${article}${target}() {
        return <#list requiredFields as field>${field.name} -> </#list> new Finalizer(
            <#list requiredFields as field>${field.name}<#sep>, </#list>
        );
    }
</#if>

<#list requiredFields as field>
    <#assign modifier>default</#assign>
    <#assign keyword>with</#assign>
    <#if field_has_next>
        <#assign returnType>With${requiredFields[field?index + 1].name?cap_first}</#assign>
    <#else>
        <#assign returnType>Finalizer</#assign>
    </#if>
    public interface With${field.name?cap_first} {
        ${nonnull}
        ${returnType} ${keyword}${field.name?cap_first}(${field.annotation}${field.type} ${field.name});
        <#if field.collection>
            <#include "varargs.ftl" parse=true>
        </#if>
    }
</#list>

    public static class Finalizer {
<#list requiredFields as field>
        private final ${field.type} ${field.name};
</#list>

<#list optionalFields as field>
        private ${field.type} ${field.name};
</#list>

        private Finalizer(
            <#list requiredFields as field>${field.type} ${field.name}<#sep>, </#list>
        ) {
            <#list requiredFields as field>this.${field.name} = ${field.name};<#sep>${'\n'}${'\t\t\t'}</#list>
        }

<#list optionalFields as field>
        <#assign returnType>Finalizer</#assign>
        <#assign modifier>public</#assign>
        <#assign keyword>with</#assign>

        ${nonnull}
        public Finalizer with${field.name?cap_first}(${field.annotation}${field.type} ${field.name}) {
            this.${field.name} = ${field.name};
            return this;
        }

    <#if field.collection>
        <#include "varargs.ftl" parse=true>
    </#if>
</#list>
        ${nonnull}
        public ${target} build() {
                return new ${target}(<#list allFields as field>${field.name}<#sep>, </#list>
            );
        }
    }
}
