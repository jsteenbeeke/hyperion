package com.jeroensteenbeeke.hyperion.buildergen;

import com.google.testing.compile.Compilation;
import com.google.testing.compile.JavaFileObjects;
import org.junit.jupiter.api.Test;

import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.tools.JavaFileObject;

import static com.google.testing.compile.CompilationSubject.assertThat;
import static com.google.testing.compile.Compiler.javac;

public class BuilderGeneratorTest {
	@Test
	public void testBuilderGeneratorWithOptionalFields() {
		JavaFileObject file =
				JavaFileObjects.forSourceLines("com.jeroensteenbeeke.hyperion.buildergen.TestObject",
						"package com.jeroensteenbeeke.hyperion.buildergen;",
						"",
						"import com.jeroensteenbeeke.hyperion.annotation.Buildable;",
						"import org.jetbrains.annotations.NotNull;",
						"import org.jetbrains.annotations.Nullable;",
						"import io.vavr.collection.List;",
						"",
						"public class TestObject {",
						"\tprivate final int a;",
						"\tprivate final String b;",
						"\tprivate List<String> c;",
						"\tprivate final String d;",
						"\tprivate final String e;",
						"\t@Buildable",
						"\tTestObject(int a, @Nullable String d, @NotNull String b, @NotNull List<String> c, @Nullable String e) {",
						"\t\tthis.a = a;",
						"\t\tthis.b = b;",
						"\t\tthis.c = c;",
						"\t\tthis.d = d;",
						"\t\tthis.e = e;",
						"\t}",
						"}");

		Compilation compilation = javac().withOptions("-source", "17", "-target", "17", "-verbose", "-cp",
						System.getProperty("java" +
								".class.path"))
				.withProcessors(new TestBuilderGenerator())
				.compile
						(file);
		assertThat(compilation).succeeded();
		assertThat(compilation)
				.generatedSourceFile(
						"com.jeroensteenbeeke.hyperion.buildergen.TestObjectBuilder")
				.hasSourceEquivalentTo(JavaFileObjects
						.forSourceLines(
								"com.jeroensteenbeeke.hyperion.buildergen.TestObjectBuilder",
								"package com.jeroensteenbeeke.hyperion.buildergen;",
								"",
								"import io.vavr.collection.List;",
								"import jakarta.annotation.Generated;",
								"import org.jetbrains.annotations.NotNull;",
								"import org.jetbrains.annotations.Nullable;",
								"",
								"@Generated(date = \"now\", value = \"com.jeroensteenbeeke.hyperion.buildergen.BuilderGenerator\")",
								"public class TestObjectBuilder {",
								"@NotNull",
								"public static WithA aTestObject() {",
								"return a -> b -> c -> new Finalizer(a, b, c);",
								"}",
								"",
								"public interface WithA {",
								"@NotNull",
								"WithB withA(int a);",
								"}",
								"",
								"public interface WithB {",
								"@NotNull",
								"WithC withB(@NotNull String b);",
								"}",
								"",
								"public interface WithC {",
								"@NotNull",
								"Finalizer withC(@NotNull List<String> c);",
								"",
								"@NotNull",
								"default Finalizer withC(String... elements) {",
								"return withC(List.of(elements));",
								"}",
								"}",
								"",
								"public static class Finalizer {",
								"private final int a;",
								"private final String b;",
								"private final List<String> c;",
								"",
								"private String d;",
								"private String e;",
								"",
								"private Finalizer(int a, String b, List<String> c) {",
								"this.a = a;",
								"this.b = b;",
								"this.c = c;",
								"}",
								"",
								"@NotNull",
								"public Finalizer withD(@Nullable String d) {",
								"this.d = d;",
								"return this;",
								"}",
								"",
								"@NotNull",
								"public Finalizer withE(@Nullable String e) {",
								"this.e = e;",
								"return this;",
								"}",
								"",
								"@NotNull",
								"public TestObject build() {",
								"return new TestObject(a, d, b, c, e);",
								"}",
								"}",
								"}"


						));
	}

	@Test
	public void testBuilderGeneratorWithOnlyOptionalFields() {
		JavaFileObject file =
				JavaFileObjects.forSourceLines("com.jeroensteenbeeke.hyperion.buildergen.TestObject",
						"package com.jeroensteenbeeke.hyperion.buildergen;",
						"",
						"import com.jeroensteenbeeke.hyperion.annotation.Buildable;",
						"import org.jetbrains.annotations.NotNull;",
						"import org.jetbrains.annotations.Nullable;",
						"import java.util.List;",
						"",
						"public class TestObject {",
						"\tprivate final String d;",
						"\tprivate final String e;",
						"\t@Buildable",
						"\tTestObject(@Nullable String d, @Nullable String e) {",
						"\t\tthis.d = d;",
						"\t\tthis.e = e;",
						"\t}",
						"}");

		Compilation compilation = javac().withOptions("-source", "17", "-target", "17", "-verbose", "-cp",
						System.getProperty("java" +
								".class.path"))
				.withProcessors(new TestBuilderGenerator())
				.compile
						(file);
		assertThat(compilation).succeededWithoutWarnings();
		assertThat(compilation)
				.generatedSourceFile(
						"com.jeroensteenbeeke.hyperion.buildergen.TestObjectBuilder")
				.hasSourceEquivalentTo(JavaFileObjects
						.forSourceLines(
								"com.jeroensteenbeeke.hyperion.buildergen.TestObjectBuilder",
								"package com.jeroensteenbeeke.hyperion.buildergen;",
								"",
								"import jakarta.annotation.Generated;",
								"import org.jetbrains.annotations.NotNull;",
								"import org.jetbrains.annotations.Nullable;",
								"",
								"@Generated(date = \"now\", value = \"com.jeroensteenbeeke.hyperion.buildergen.BuilderGenerator\")",
								"public class TestObjectBuilder {",
								"@NotNull",
								"public static Finalizer aTestObject() {",
								"return new Finalizer();",
								"}",
								"",
								"public static class Finalizer {",
								"",
								"private String d;",
								"private String e;",
								"",
								"private Finalizer() {",
								"}",
								"",
								"@NotNull",
								"public Finalizer withD(@Nullable String d) {",
								"this.d = d;",
								"return this;",
								"}",
								"@NotNull",
								"public Finalizer withE(@Nullable String e) {",
								"this.e = e;",
								"return this;",
								"}",
								"",
								"@NotNull",
								"public TestObject build() {",
								"return new TestObject(",
								"d, e",
								");",
								"}",
								"}",
								"}"

						));
	}


	@Test
	public void testBuilderGenerator() {
		JavaFileObject file =
				JavaFileObjects.forSourceLines("com.jeroensteenbeeke.hyperion.buildergen.TestObject",
						"package com.jeroensteenbeeke.hyperion.buildergen;",
						"",
						"import org.jetbrains.annotations.NotNull;",
						"import com.jeroensteenbeeke.hyperion.annotation.Buildable;",
						"import java.util.List;",
						"",
						"public class TestObject {",
						"\tprivate final int a;",
						"\tprivate final String b;",
						"\tprivate final List<String> c;",
						"\t@Buildable",
						"\tTestObject(int a, @NotNull String b, @NotNull List<String> c) {",
						"\t\tthis.a = a;",
						"\t\tthis.b = b;",
						"\t\tthis.c = c;",
						"\t}",
						"}");

		Compilation compilation = javac().withOptions("-source", "17", "-target", "17", "-verbose", "-cp",
						System.getProperty("java" +
								".class.path"))
				.withProcessors(new TestBuilderGenerator())
				.compile
						(file);
		assertThat(compilation).succeededWithoutWarnings();
		assertThat(compilation)
				.generatedSourceFile(
						"com.jeroensteenbeeke.hyperion.buildergen.TestObjectBuilder")
				.hasSourceEquivalentTo(JavaFileObjects
						.forSourceLines(
								"com.jeroensteenbeeke.hyperion.buildergen.TestObjectBuilder",
								"package com.jeroensteenbeeke.hyperion.buildergen;",
								"",
								"import jakarta.annotation.Generated;",
								"import java.util.List;",
								"import org.jetbrains.annotations.NotNull;",
								"",
								"@Generated(date = \"now\", value = \"com.jeroensteenbeeke.hyperion.buildergen.BuilderGenerator\")",
								"public class TestObjectBuilder {",
								"@NotNull",
								"public static WithA aTestObject() {",
								"return a -> b -> c ->  new TestObject(",
								"a, b, c",
								");",
								"}",
								"",
								"public interface WithA {",
								"@NotNull",
								"WithB withA(int a);",
								"}",
								"public interface WithB {",
								"@NotNull",
								"WithC withB(@NotNull String b);",
								"}",
								"public interface WithC {",
								"@NotNull",
								"TestObject andC(@NotNull List<String> c);",
								"@NotNull",
								"default TestObject andC(String... elements) {",
								"return andC(new java.util.ArrayList<>(java.util.List.of(elements)));",
								"}",
								"}",
								"}"
						));
	}

	@Test
	public void testVavrCollectionDetection() {
		JavaFileObject file =
				JavaFileObjects.forSourceLines("com.jeroensteenbeeke.hyperion.buildergen.TestObject",
						"package com.jeroensteenbeeke.hyperion.buildergen;",
						"",
						"import com.jeroensteenbeeke.hyperion.annotation.Buildable;",
						"import org.jetbrains.annotations.NotNull;",
						"import org.jetbrains.annotations.Nullable;",
						"import io.vavr.collection.*;",
						"",
						"public class TestObject {",
						"\tprivate final Array<String> a;",
						"\tprivate final List<String> b;",
						"\tprivate final HashSet<String> c;",
						"\tprivate final TreeSet<String> d;",
						"\t@Buildable",
						"\tTestObject(@NotNull Array<String> a, @NotNull List<String> b, @NotNull HashSet<String> c, @NotNull TreeSet<String> d) {",
						"\t\tthis.a = a;",
						"\t\tthis.b = b;",
						"\t\tthis.c = c;",
						"\t\tthis.d = d;",
						"\t}",
						"}");

		Compilation compilation = javac().withOptions("-source", "17", "-target", "17", "-verbose",
						"-AannotationType=Jetbrains", "-cp", System.getProperty("java" +
								".class.path"))
				.withProcessors(new TestBuilderGenerator())
				.compile
						(file);
		assertThat(compilation).succeeded();
		assertThat(compilation)
				.generatedSourceFile(
						"com.jeroensteenbeeke.hyperion.buildergen.TestObjectBuilder")
				.hasSourceEquivalentTo(JavaFileObjects
						.forSourceLines("com.jeroensteenbeeke.hyperion.buildergen.TestObjectBuilder",
								"package com.jeroensteenbeeke.hyperion.buildergen;",
								"",
								"import io.vavr.collection.Array;",
								"import io.vavr.collection.HashSet;",
								"import io.vavr.collection.List;",
								"import io.vavr.collection.TreeSet;",
								"import jakarta.annotation.Generated;",
								"import org.jetbrains.annotations.NotNull;",
								"",
								"@Generated(date = \"now\", value = \"com.jeroensteenbeeke.hyperion.buildergen.BuilderGenerator\")",
								"public class TestObjectBuilder {",
								"@NotNull",
								"public static WithA aTestObject() {",
								"return a -> b -> c -> d -> new TestObject(a, b, c, d);",
								"}",
								"",
								"public interface WithA {",
								"@NotNull",
								"WithB withA(@NotNull Array<String> a);",
								"",
								"@NotNull",
								"default WithB withA(String... elements) {",
								"return withA(Array.of(elements));",
								"}",
								"}",
								"",
								"public interface WithB {",
								"@NotNull",
								"WithC withB(@NotNull List<String> b);",
								"",
								"@NotNull",
								"default WithC withB(String... elements) {",
								"return withB(List.of(elements));",
								"}",
								"}",
								"",
								"public interface WithC {",
								"@NotNull",
								"WithD withC(@NotNull HashSet<String> c);",
								"",
								"@NotNull",
								"default WithD withC(String... elements) {",
								"return withC(HashSet.of(elements));",
								"}",
								"}",
								"",
								"public interface WithD {",
								"@NotNull",
								"TestObject andD(@NotNull TreeSet<String> d);",
								"",
								"@NotNull",
								"default TestObject andD(String... elements) {",
								"return andD(TreeSet.of(elements));",
								"}",
								"}",
								"}"

						));
	}

	@SupportedAnnotationTypes("com.jeroensteenbeeke.hyperion.annotation.Buildable")
	@SupportedSourceVersion(SourceVersion.RELEASE_21)
	private static class TestBuilderGenerator extends BuilderGenerator {
		@Override
		protected String getDateString() {
			return "now";
		}
	}

}
