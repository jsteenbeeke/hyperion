package com.jeroensteenbeeke.hyperion.mojo.core;

import io.vavr.collection.Array;

/**
 * Represents a user choice between using Java EE or Jakarta EE {@code @Generated} annotations
 */
public enum GeneratedType {
	/**
	 * Java EE generated type
	 */
	JavaEE("@Generated") {
		@Override
		public Iterable<String> getImports() {
			return Array.of("javax.annotation.Generated").toJavaList();
		}
	},
	/**
	 * Jakarta EE generated type
	 */
	JakartaEE("@Generated") {
		@Override
		public Iterable<String> getImports() {
			return Array.of("jakarta.annotation.Generated").toJavaList();
		}
	};

	private final String token;

	/**
	 * Constructor
	 * @param token The Java code to place in the generated code
	 */
	GeneratedType(String token) {
		this.token = token;
	}

	/**
	 * Returns the Java code
	 * @return The code representing the annotation
	 */
	public String getToken() {
		return token;
	}

	/**
	 * Gets an iterable of required imports for this annotation type
	 * @return An iterable of imports
	 */
	public abstract Iterable<String> getImports();
}
