package com.jeroensteenbeeke.hyperion.mojo.core;

import org.jetbrains.annotations.NotNull;

/**
 * Represents a use-selectable nullability annotation type set
 */
public enum NullabilityAnnotationType {
	/**
	 * Use the JSR305/Findbugs annotations
	 *
	 * @deprecated JSR was never adopted, better alternatives that do not use Java EE namespace exist
	 */
	@Deprecated
	Findbugs("@Nonnull", "@Nullable") {
		@Override
		public String getNonnullFQDN() {
			return "javax.annotation.Nonnull";
		}

		@Override
		public String getNullableFQDN() {
			return "javax.annotation.Nullable";
		}
	},
	/**
	 * Use Jetbrains annotations
	 */
	Jetbrains("@NotNull", "@Nullable") {
		@Override
		public String getNonnullFQDN() {
			return "org.jetbrains.annotations.NotNull";
		}

		@Override
		public String getNullableFQDN() {
			return "org.jetbrains.annotations.Nullable";
		}
	};

	private final String nonnullToken;

	private final String nullableToken;

	/**
	 * Constructor
	 *
	 * @param nonnullToken  The annotation for non-null
	 * @param nullableToken The annotation for nullable
	 */
	NullabilityAnnotationType(@NotNull String nonnullToken, @NotNull String nullableToken) {
		this.nonnullToken = nonnullToken;
		this.nullableToken = nullableToken;
	}

	/**
	 * Returns the Java token used for the non-null annotation
	 * @return The token
	 */
	public String getNonnullToken() {
		return nonnullToken;
	}

	/**
	 * Returns the Java token used for the nullable annotation
	 * @return The token
	 */
	public String getNullableToken() {
		return nullableToken;
	}

	/**
	 * Returns the fully qualified domain name of the non-null annotation type
	 * @return The FQDN
	 */
	public abstract String getNonnullFQDN();

	/**
	 * Returns the fully qualified domain name of the nullable annotation type
	 * @return The FQDN
	 */
	public abstract String getNullableFQDN();
}
