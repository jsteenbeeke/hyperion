grammar Specification;

specification: packageSpecification*;

packageSpecification: PACKAGE fqdn LPAREN classOrGroupSpecification* RPAREN;

classOrGroupSpecification: classSpecification | groupSpecification;
groupSpecification: GROUP IDENTIFIER LPAREN classSpecification* RPAREN;
classSpecification: CLASS IDENTIFIER LPAREN member* RPAREN;
member: modifier* FIELD IDENTIFIER COLON type;
type: simpleType | arrayType | generifiedType;

simpleType: fqdn;
arrayType: fqdn ARRAY_BRACES ;
generifiedType: fqdn LANGLE type (COMMA type)* RANGLE;
modifier: REQUIRED | CONSTANT;
fqdn: IDENTIFIER (DOT IDENTIFIER)*;

COLON: ':';
LPAREN: '{';
RPAREN: '}';
LANGLE: '<';
RANGLE: '>';
ARRAY_BRACES: '[]';
REQUIRED: 'required';
CONSTANT: 'const';
COMMA: ',';
PACKAGE: 'package';
CLASS: 'class';
GROUP: 'group';
FIELD: 'field';
TYPED: 'typed';
IDENTIFIER: [a-zA-Z$_][a-zA-Z0-9$_]*;
OF: 'OF';
DOT: '.';
WS : [ \t\r\n]+ -> skip ;
