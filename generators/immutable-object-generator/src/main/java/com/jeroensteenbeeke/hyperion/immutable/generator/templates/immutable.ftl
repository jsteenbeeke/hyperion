package ${package};

import com.jeroensteenbeeke.hyperion.annotation.Buildable;

import java.util.Objects;

import io.vavr.Function1;
import io.vavr.control.Option;

<#list imports as import>
import ${import};
</#list>

${generated}(value = "com.jeroensteenbeeke.hyperion.immutable.generator.GeneratorMojo", date = "${generationDate}")
public final class ${class} <#if group??>implements ${group}</#if>{
<#list fields as field>
    private final ${field.type} ${field.name};
</#list>

    @Buildable
    public ${class}(
<#list fields as field>
        <#if field.required>${nonnull}<#else>@Nullable</#if> ${field.type} ${field.name}<#sep >,
</#list>
    ) {
<#list fields as field>
        this.${field.name} = ${field.name};
</#list>
    }

<#list fields as field>
    <#if field.required>
    ${nonnull}
    public ${field.type} get${field.name?cap_first}() {
        return this.${field.name};
    }
    <#else>
    ${nonnull}
    public Option<${field.type}> get${field.name?cap_first}() {
        return Option.of(this.${field.name});
    }
    </#if>

    <#if !field.constant>
    ${nonnull}
    public ${class} with${field.name?cap_first}(${nonnull} ${field.type} ${field.name}) {
        return new ${class}(<#list fields as cparam>${cparam.name}<#sep>,</#list>);
    }

    <#if field.primitive>
    ${nonnull}
    public ${class} with${field.name?cap_first}(${nonnull} ${field.unaryOperator} ${field.unaryOperator?uncap_first}) {
        return new ${class}(<#list fields as cparam><#if cparam.name = field.name>${field.unaryOperator?uncap_first}.${field.unaryOperatorApplyMethod}(${cparam.name})<#else>${cparam.name}</#if><#sep>,</#list>);
    }
    <#elseif field.vavrListElementType??>
    ${nonnull}
    public ${class} with${field.name?cap_first}(${nonnull} ${field.vavrListElementType} element, ${nonnull} Function1<${field.vavrListElementType},${field.vavrListElementType}> ${field.name}Function) {
        return with${field.name?cap_first}(element::equals, ${field.name}Function);
    }

    ${nonnull}
    public ${class} with${field.name?cap_first}(${nonnull} Predicate<${field.vavrListElementType}> predicate, ${nonnull} Function1<${field.vavrListElementType},${field.vavrListElementType}> ${field.name}Function) {
        return new ${class}(<#list fields as cparam>${cparam.name}<#if cparam.name = field.name>.map(elem -> predicate.test(elem) ? ${field.name}Function.apply(elem) : elem)</#if><#sep>,</#list>);
    }

    ${nonnull}
    public ${class} with${field.name?cap_first}(${nonnull} Function1<${field.type},${field.type}> ${field.name}Function) {
        return new ${class}(<#list fields as cparam>${cparam.name}<#if cparam.name = field.name>Function.apply(${cparam.name})</#if><#sep>,</#list>);
    }

    ${nonnull}
    public ${class} add${field.name?cap_first}(${nonnull} ${field.vavrListElementType}... elements) {
        return with${field.name?cap_first}(current -> current.${field.vavrListElementTypeAppendOperation}(Array.of(elements)));
    }

    ${nonnull}
    public ${class} remove${field.name?cap_first}(${nonnull} ${field.vavrListElementType}... elements) {
        return with${field.name?cap_first}(current -> current.removeAll(Array.of(elements)));
    }
    <#else>
    ${nonnull}
    public ${class} with${field.name?cap_first}(${nonnull} Function1<${field.type},${field.type}> ${field.name}Function) {
        return new ${class}(<#list fields as cparam>${cparam.name}<#if cparam.name = field.name>Function.apply(${cparam.name})</#if><#sep>,</#list>);
    }
    </#if>
    </#if>


</#list>

<#if group??>
    ${nonnull}
    @Override
    public Option<${class}> as${class}() {
        return Option.some(this);
    }

    ${nonnull}
    @Override
    public <T> Option<T> visit(${group}Visitor<T> visitor) {
        return visitor.visit(this);
    }
</#if>

    public int hashCode() {
            return Objects.hash(
    <#list fields as field>
                ${field.name}<#sep>,
    </#list>);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ${class} that = (${class}) o;

        return <#list fields as field><#if field.primitive>this.${field.name} == that.${field.name}<#elseif field.required>Objects.equals(this.${field.name}, that.${field.name})<#else>this.${field.name}.equals(that.${field.name})</#if><#sep>
                && </#list>;
    }
}
