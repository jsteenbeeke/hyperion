/*
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version. <p> This program is distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details. <p> You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.immutable.generator;

import com.jeroensteenbeeke.hyperion.immutable.generator.specification.SpecificationLexer;
import com.jeroensteenbeeke.hyperion.immutable.generator.specification.SpecificationParser;
import com.jeroensteenbeeke.hyperion.mojo.core.GeneratedType;
import com.jeroensteenbeeke.hyperion.mojo.core.NullabilityAnnotationType;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import io.vavr.Tuple2;
import io.vavr.collection.*;
import io.vavr.control.Option;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.*;
import org.apache.maven.project.MavenProject;
import org.jetbrains.annotations.NotNull;
import org.sonatype.plexus.build.incremental.BuildContext;

import javax.inject.Inject;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import static java.util.function.Predicate.not;

/**
 * Maven plugin for generating retrofit classes for JAX-RS services
 */
@Mojo(name = "generate", defaultPhase = LifecyclePhase.GENERATE_SOURCES,
	requiresDependencyResolution = ResolutionScope.COMPILE)
public class GeneratorMojo extends AbstractMojo {

	@Parameter(required = false, defaultValue = "generated-sources")
	public String generatedSourcesDir;

	@Parameter(required = false, defaultValue = "immutables")
	public String generatedSourcesSubdir;

	@Parameter(required = false, defaultValue = "Jetbrains")
	public NullabilityAnnotationType annotationType;

	@Parameter(required = false, defaultValue = "JakartaEE")
	public GeneratedType generatedType;

	@Component
	public BuildContext buildContext;

	@Parameter(required = true, property = "project")
	@Inject
	public MavenProject project;

	@Override
	@SuppressWarnings("deprecation")
	public void execute() throws MojoExecutionException, MojoFailureException {
		project = Optional
			.ofNullable(project)
			.orElseThrow(() -> new MojoExecutionException("Required parameter 'project' missing"));

		annotationType = Optional.ofNullable(annotationType).orElse(NullabilityAnnotationType.Jetbrains);
		generatedType = Optional.ofNullable(generatedType).orElse(GeneratedType.JakartaEE);

		Path target = project.getBasedir().toPath().resolve("target");
		Path specifications = project
			.getBasedir()
			.toPath()
			.resolve("src")
			.resolve("main")
			.resolve("specification");
		File targetDir = target.toFile();

		Path codeGenerationDir = target
			.resolve(Optional.ofNullable(generatedSourcesDir).orElse("generated-sources"))
			.resolve(Optional.ofNullable(generatedSourcesSubdir).orElse("immutables"));

		if (!Files.exists(codeGenerationDir) && !codeGenerationDir.toFile().mkdirs()) {
			throw new MojoFailureException("Could not create target directory: " + codeGenerationDir.toFile());
		}

		try {

			HashMap<ClassKey, List<FieldDescriptor>> classesAndFields = HashMap.empty();
			HashMultimap<GroupKey, ClassKey> groups = HashMultimap.withSet().empty();
			HashMap<ClassKey, GroupKey> keyToGroup = HashMap.empty();

			for (Path spec : findSpecifications(specifications.toFile())) {
				try {
					SpecificationLexer lexer = new SpecificationLexer(
						new ANTLRInputStream(Files.newBufferedReader(spec))
					);
					lexer.removeErrorListeners();
					lexer.addErrorListener(ThrowingErrorListener.INSTANCE);

					SpecificationParser parser = new SpecificationParser(new CommonTokenStream(lexer));
					parser.removeErrorListeners();
					parser.addErrorListener(ThrowingErrorListener.INSTANCE);

					SpecificationParser.SpecificationContext specification = parser.specification();

					for (SpecificationParser.PackageSpecificationContext packageSpecificationContext : specification.packageSpecification()) {
						final String packageName = packageSpecificationContext.fqdn().getText();

						for (SpecificationParser.ClassOrGroupSpecificationContext classOrGroupSpecificationContext : packageSpecificationContext.classOrGroupSpecification()) {
							SpecificationParser.ClassSpecificationContext classSpecificationContext = classOrGroupSpecificationContext.classSpecification();
							SpecificationParser.GroupSpecificationContext groupSpecificationContext = classOrGroupSpecificationContext.groupSpecification();

							if (classSpecificationContext != null) {
								final String className = classSpecificationContext
									.IDENTIFIER().getText();

								var classKey = new ClassKey(packageName, className);
								classesAndFields = classesAndFields.put(classKey, List.empty());

								for (SpecificationParser.MemberContext memberContext : classSpecificationContext.member()) {
									SpecificationParser.TypeContext type = memberContext.type();

									String typeText = sanitizeType(type);

									String identifier = memberContext.IDENTIFIER().getText();
									boolean r = false;
									boolean c = false;

									for (SpecificationParser.ModifierContext modifierContext : memberContext.modifier()) {
										if (modifierContext.REQUIRED() != null) {
											r = true;
										} else if (modifierContext.CONSTANT() != null) {
											c = true;
										}
									}

									boolean required = r;
									boolean constant = c;

									if (!required && isPrimitive(typeText)) {
										throw new MojoExecutionException(
											"Class %s in package %s has optional field %s which is a primitive".formatted(className, packageName,
												identifier));
									}

									Set<String> imports = findImports(type)
										.filter(t -> !isPrimitive(t))
										.filter(t -> !t.startsWith("java.lang") || (t.length() > 10 && t
											.substring(10)
											.indexOf('.') != -1))
										.filter(t -> !extractPackage(t).equals(packageName));

									classesAndFields = classesAndFields.put(classKey, classesAndFields
										.get(classKey)
										.map(
											l -> l.append(new FieldDescriptor(required, constant, typeText, identifier, imports)))
										.getOrElse(List::empty));
								}
							} else if (groupSpecificationContext != null) {
								var group = groupSpecificationContext.IDENTIFIER().getText();

								for (SpecificationParser.ClassSpecificationContext classSpecContext : groupSpecificationContext.classSpecification()) {
									final String className = classSpecContext
										.IDENTIFIER().getText();

									var classKey = new ClassKey(packageName, className);
									classesAndFields = classesAndFields.put(classKey, List.empty());

									for (SpecificationParser.MemberContext memberContext : classSpecContext.member()) {
										SpecificationParser.TypeContext type = memberContext.type();

										String typeText = sanitizeType(type);

										String identifier = memberContext.IDENTIFIER().getText();
										boolean r = false;
										boolean c = false;

										for (SpecificationParser.ModifierContext modifierContext : memberContext.modifier()) {
											if (modifierContext.REQUIRED() != null) {
												r = true;
											} else if (modifierContext.CONSTANT() != null) {
												c = true;
											}
										}

										boolean required = r;
										boolean constant = c;

										if (!required && isPrimitive(typeText)) {
											throw new MojoExecutionException(
												"Class %s in package %s has optional field %s which is a primitive".formatted(className, packageName,
													identifier));
										}

										Set<String> imports = findImports(type)
											.filter(t -> !isPrimitive(t))
											.filter(t -> !t.startsWith("java.lang") || (t.length() > 10 && t
												.substring(10)
												.indexOf('.') != -1))
											.filter(t -> !extractPackage(t).equals(packageName));

										classesAndFields = classesAndFields.put(classKey, classesAndFields
											.get(classKey)
											.map(
												l -> l.append(new FieldDescriptor(required, constant, typeText, identifier, imports)))
											.getOrElse(List::empty));
									}

									GroupKey groupKey = new GroupKey(packageName, group);
									groups = groups.put(groupKey, classKey);
									keyToGroup = keyToGroup.put(classKey, groupKey);
								}

							}

						}

					}
				} catch (IOException e) {
					throw new MojoExecutionException("Could not parse specification: " + spec, e);
				}
			}

			for (GroupKey group : groups.keySet()) {
				var members = groups.get(group).getOrElse(Array::empty).map(ClassKey::className).toJavaList();

				String packageName = group.packageName();
				String groupName = group.groupName();

				java.util.Map<String, Object> dataModel = java.util.Map.of("package", packageName,
					"group", groupName,
					"nonnull", annotationType.getNonnullToken(),
					"generated", generatedType.getToken(),
					"generationDate", DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(LocalDateTime.now()),
					"members", members,
					"imports", Array.of(annotationType.getNonnullFQDN()).appendAll(generatedType.getImports()).toJavaList()
				);

				File packageDir = createPackage(codeGenerationDir.toFile(), packageName);

				File groupFile = new File(packageDir, groupName.concat(".java"));
				File visitorFile = new File(packageDir, groupName.concat("Visitor.java"));

				Configuration configuration = new Configuration(Configuration.VERSION_2_3_28);
				configuration.setClassForTemplateLoading(GeneratorMojo.class, "templates");
				configuration.setDefaultEncoding("UTF-8");

				try (FileWriter out = new FileWriter(groupFile)) {
					Template template = configuration.getTemplate("group.ftl");

					template.process(dataModel, out);
					out.flush();
				} catch (IOException | TemplateException e) {
					throw new MojoExecutionException(e.getMessage(), e);
				}

				try (FileWriter out = new FileWriter(visitorFile)) {
					Template template = configuration.getTemplate("visitor.ftl");

					template.process(dataModel, out);
					out.flush();
				} catch (IOException | TemplateException e) {
					throw new MojoExecutionException(e.getMessage(), e);
				}
			}

			for (Tuple2<ClassKey, List<FieldDescriptor>> classesAndField : classesAndFields) {
				final ClassKey classFQDN = classesAndField._1();
				java.util.List<FieldDescriptor> fieldDescriptors = classesAndField
					._2()
					.toJavaList();

				String packageName = classFQDN.packageName();
				String className = classFQDN.className();


				java.util.Map<String, Object> dataModel;

				List<String> imports = classesAndField
					._2()
					.flatMap(FieldDescriptor::getImports);

				if (classesAndField._2().find(FieldDescriptor::isRequired).isDefined()) {
					imports = imports.append(annotationType.getNonnullFQDN());
				}
				if (classesAndField._2().find(not(FieldDescriptor::isRequired)).isDefined()) {
					imports = imports.append(annotationType.getNullableFQDN());
				}

				imports = imports.appendAll(generatedType.getImports())
					.sorted();

				if (keyToGroup.containsKey(classFQDN)) {
					GroupKey groupKey = keyToGroup.get(classFQDN).getOrElseThrow(IllegalStateException::new);

					dataModel = java.util.Map.of("package", packageName,
						"class", className,
						"generated", generatedType.getToken(),
						"generationDate", DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(LocalDateTime.now()),
						"nonnull", annotationType.getNonnullToken(),
						"imports", imports
							.toJavaList(),
						"fields", fieldDescriptors,
						"group", groupKey.groupName()
					);
				} else {
					dataModel = java.util.Map.of("package", packageName,
						"class", className,
						"generated", generatedType.getToken(),
						"generationDate", DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(LocalDateTime.now()),
						"nonnull", annotationType.getNonnullToken(),
						"imports", imports
							.toJavaList(),
						"fields", fieldDescriptors
					);
				}

				File packageDir = createPackage(codeGenerationDir.toFile(), packageName);

				File java = new File(packageDir, className.concat(".java"));

				Configuration configuration = new Configuration(Configuration.VERSION_2_3_28);
				configuration.setClassForTemplateLoading(GeneratorMojo.class, "templates");
				configuration.setDefaultEncoding("UTF-8");

				try (FileWriter out = new FileWriter(java)) {
					Template template = configuration.getTemplate("immutable.ftl");

					template.process(dataModel, out);
					out.flush();
				} catch (IOException | TemplateException e) {
					throw new MojoExecutionException(e.getMessage(), e);
				}

			}
		} finally {
			if (buildContext != null) {
				buildContext.refresh(targetDir);
			}
			if (project != null) {
				project.addCompileSourceRoot(
					targetDir.toPath().getParent().resolve(codeGenerationDir).toString());
			}
		}
	}

	@NotNull
	private String extractPackage(@NotNull String fqdn) {
		int idx = fqdn.lastIndexOf('.');

		if (idx == -1) {
			return fqdn;
		}

		return fqdn.substring(0, idx);
	}

	private String sanitizeType(SpecificationParser.TypeContext type) {
		String result = "";

		result = combine(type.simpleType(), result)
			.map(t -> dequalify(t._1().getText()))
			.getOrElse(result);
		result = combine(type.arrayType(), result)
			.map(t -> t._1().fqdn().getText() + "[]")
			.getOrElse(result);
		result = combine(type.generifiedType(), result)
			.map(t -> dequalify(t._1().fqdn().getText()) + "<" +
				Array.ofAll(t._1().type()).map(this::sanitizeType).mkString(", ")
				+ ">")
			.getOrElse(result);

		return result;
	}

	private String dequalify(String fqdn) {
		int i = fqdn.lastIndexOf('.');

		return i != -1 ? fqdn.substring(i + 1) : fqdn;
	}

	private boolean isPrimitive(String input) {
		return Array
			.of("boolean", "byte", "short", "char", "int", "float", "double", "long")
			.contains(input);
	}

	private Set<String> findImports(SpecificationParser.TypeContext type) {
		Set<String> list = HashSet.empty();

		list = combine(type.simpleType(), list)
			.map(t -> t._2().add(t._1().getText()))
			.getOrElse(list);
		list = combine(type.generifiedType(), list)
			.map(t -> t._2().add(t._1().fqdn().getText()))
			.getOrElse(list);
		list = combine(type.generifiedType(), list)
			.map(t -> t._2().add(t._1().fqdn().getText())
				.addAll(Array.ofAll(t._1().type()).flatMap(this::findImports))
			)
			.getOrElse(list);

		return list;
	}

	private <T, U> Option<Tuple2<T, U>> combine(T a, U b) {
		return Option.of(a).map(t -> new Tuple2<>(t, b));
	}

	private List<Path> findSpecifications(File specificationDir) throws MojoExecutionException {
		if (!specificationDir.isDirectory()) {
			throw new MojoExecutionException(String.format("Specification directory '%s' does not exist", specificationDir.getAbsolutePath()));
		}

		List<File> specifications = List.empty();

		File[] directories = specificationDir.listFiles(File::isDirectory);

		if (directories != null) {
			for (File directory : directories) {
				specifications = specifications.appendAll(findSpecifications(directory).map(Path::toFile));
			}
		}

		File[] specFiles = specificationDir.listFiles(f -> f.getName().endsWith(".spec"));

		if (specFiles != null) {
			specifications = specifications.appendAll(Array.of(specFiles));
		}

		return specifications.map(File::toPath);
	}

	private File createPackage(File parent, String packageName)
		throws MojoExecutionException {
		File base = parent;

		String[] packages = packageName.split("\\.");

		for (String pkg : packages) {
			base = create(new File(base, pkg));
		}

		return base;
	}

	private File create(File file) throws MojoExecutionException {
		if (file.exists() && !file.isDirectory()) {
			throw new MojoExecutionException(String
				.format("File %s is not a directory", file.getName()));
		}

		if (!file.exists() && !file.mkdir()) {
			throw new MojoExecutionException(String
				.format("Could not create directory %s", file.getName()));
		}

		return file;
	}

	private static class ThrowingErrorListener extends BaseErrorListener {

		public static final ThrowingErrorListener INSTANCE = new ThrowingErrorListener();

		@Override
		public void syntaxError(
			Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e)
			throws ParseCancellationException {
			throw new ParseCancellationException("line " + line + ":" + charPositionInLine + " " + msg);
		}
	}

}

record GroupKey(String packageName, String groupName) {
}

record ClassKey(String packageName, String className) {
}
