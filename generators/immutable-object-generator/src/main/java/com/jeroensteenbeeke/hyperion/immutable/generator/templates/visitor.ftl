package ${package};

import io.vavr.control.Option;

<#list imports as import>
    import ${import};
</#list>

${generated}(value = "com.jeroensteenbeeke.hyperion.immutable.generator.GeneratorMojo", date = "${generationDate}")
public interface ${group}Visitor<T> {
    <#list members as member>
    ${nonnull}
    default Option<T> visit(${member} ${member?uncap_first}) {
        return Option.none();
    }
    </#list>
}
