package com.jeroensteenbeeke.hyperion.immutable.generator;

import io.vavr.collection.HashSet;
import io.vavr.collection.Set;

import org.jetbrains.annotations.NotNull;

/**
 * DTO for describing required fields
 */
public class FieldDescriptor {
	private final boolean required;

	private final boolean constant;

	private final String type;

	private final String name;

	private final Set<String> imports;

	FieldDescriptor(boolean required, boolean constant, @NotNull String type, @NotNull String name, @NotNull Set<String> imports) {
		this.required = required;
		this.constant = constant;
		this.type = type;
		this.name = name;
		this.imports = imports;
	}

	/**
	 * Whether or not the field is required (i.e. has to be supplied at construction time)
	 *
	 * @return {@code true} if required. {@code false} otherwise
	 */
	public boolean isRequired() {
		return required;
	}

	/**
	 * Whether or not the field is constant, that is: whether the generated class should provide a method to create
	 * a instance with the value of this field changed
	 *
	 * @return {@code true} if constant. {@code false} otherwise
	 */
	public boolean isConstant() {
		return constant;
	}

	/**
	 * Whether or not the field is a primitive type
	 *
	 * @return {@code true} if the field is a primitive. {@code false} otherwise
	 */
	public boolean isPrimitive() {
		return switch (type) {
			case "byte", "boolean", "double", "float", "int", "long", "short" -> true;
			default -> false;
		};
	}

	/**
	 * A functional interface to modify the current primitive type. Requires that isPrimitive() is true or it will throw an exception
	 *
	 * @return The type of unary operator
	 */
	public String getUnaryOperator() {
		return switch (type) {
			case "byte" -> "ByteUnaryOperator";
			case "boolean" -> "BooleanUnaryOperator";
			case "double" -> "DoubleUnaryOperator";
			case "float" -> "FloatUnaryOperator";
			case "int" -> "IntUnaryOperator";
			case "long" -> "LongUnaryOperator";
			case "short" -> "ShortUnaryOperator";
			default -> throw new IllegalStateException();
		};
	}

	/**
	 * Returns the list type, if this type is a vavr list
	 *
	 * @return The type, or {@code null} otherwise
	 */
	public String getVavrListElementType() {
		if (isVavrType(type)) {
			int firstBracket = type.indexOf('<');
			int lastBracket = type.lastIndexOf('>');

			if (firstBracket != -1 && lastBracket != -1) {
				int firstComma = type.indexOf(",");
				if (firstComma == -1) {
					return type.substring(firstBracket + 1, lastBracket);
				}
			}
		}

		return null;
	}

	/**
	 * Returns the append method name for the given VAVR collection
	 * @return The method name
	 */
	public String getVavrListElementTypeAppendOperation() {
		if (isVavrType(type)) {
			if (type.startsWith("List<")) {
				return "appendAll";
			}
			if (type.startsWith("Array<")) {
				return "appendAll";
			}
			if (type.startsWith("HashSet<")) {
				return "addAll";
			}
			if (type.startsWith("TreeSet<")) {
				return "addAll";
			}
		}

		return null;
	}

	private boolean isVavrType(String type) {
		return (type.startsWith("List<") && imports.contains("io.vavr.collection.List")) ||
				(type.startsWith("Array<") && imports.contains("io.vavr.collection.Array")) ||
				(type.startsWith("HashSet<") && imports.contains("io.vavr.collection.HashSet")) ||
				(type.startsWith("TreeSet<") && imports.contains("io.vavr.collection.TreeSet"));
	}

	/**
	 * A functional interface to modify the current primitive type. Requires that isPrimitive() is true or it will throw an exception
	 *
	 * @return The FQDN of the unary operator
	 */
	public String getUnaryOperatorFQDN() {
		return switch (type) {
			case "byte" -> "com.jeroensteenbeeke.hyperion.function.ByteUnaryOperator";
			case "boolean" -> "com.jeroensteenbeeke.hyperion.function.BooleanUnaryOperator";
			case "double" -> "java.util.function.DoubleUnaryOperator";
			case "float" -> "com.jeroensteenbeeke.hyperion.function.FloatUnaryOperator";
			case "int" -> "java.util.function.IntUnaryOperator";
			case "long" -> "java.util.function.LongUnaryOperator";
			case "short" -> "com.jeroensteenbeeke.hyperion.function.ShortUnaryOperator";
			default -> throw new IllegalStateException();
		};
	}

	/**
	 * The apply method invoked on the unary operator apply method
	 *
	 * @return The FQDN of the unary operator
	 */
	public String getUnaryOperatorApplyMethod() {
		return switch (type) {
			case "byte", "boolean", "float", "short" -> "apply";
			case "double" -> "applyAsDouble";
			case "int" -> "applyAsInt";
			case "long" -> "applyAsLong";
			default -> throw new IllegalStateException();
		};
	}

	/**
	 * Returns the fully qualified domain of the type of this field
	 *
	 * @return The type
	 */
	@NotNull
	public String getType() {
		return type;
	}

	/**
	 * Returns the name of the field
	 *
	 * @return The name
	 */
	@NotNull
	public String getName() {
		return name;
	}

	/**
	 * Returns the set of imports required to include this field in the class
	 *
	 * @return The set of imports
	 */
	@NotNull
	public Set<String> getImports() {
		if (isPrimitive()) {
			return HashSet.ofAll(imports).add(getUnaryOperatorFQDN());
		} else if (isVavrType(type)) {
			return HashSet.ofAll(imports).add("java.util.function.Predicate").add("io.vavr.collection.Array");
		}

		return imports;
	}
}

