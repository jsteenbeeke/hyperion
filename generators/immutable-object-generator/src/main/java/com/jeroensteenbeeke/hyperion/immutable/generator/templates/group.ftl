package ${package};

import io.vavr.control.Option;
<#list imports as import>
    import ${import};
</#list>

${generated}(value = "com.jeroensteenbeeke.hyperion.immutable.generator.GeneratorMojo", date = "${generationDate}")
public sealed interface ${group} permits <#list members as member>${member}<#sep>, </#list> {
    <T> Option<T> visit(${group}Visitor<T> visitor);

    <#list members as member>
    ${nonnull}
    default Option<${member}> as${member}() {
        return Option.none();
    }
    </#list>
}
