package com.jeroensteenbeeke.hyperion.immutable.generator {
    class Repository {
        required const field id : long
        required field name : java.lang.String
        required field units : io.vavr.collection.List<com.jeroensteenbeeke.hyperion.immutable.generator.Unit>
        field username : java.lang.String
        field image : byte[]
    }

    group Unit {
        class Alpha {
            required const field id : long
            required const field name : java.lang.String
            required field truth : boolean
            required field length : short
            required field width : long
        }

         class Beta {
            required const field id : long
            required const field name : java.lang.String
            required field parity : byte
            required field wobblyness : float
        }
    }
}
