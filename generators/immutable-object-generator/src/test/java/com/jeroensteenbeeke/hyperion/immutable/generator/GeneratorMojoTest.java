package com.jeroensteenbeeke.hyperion.immutable.generator;

import com.github.codeteapot.maven.plugin.testing.*;
import com.github.codeteapot.maven.plugin.testing.junit.jupiter.MavenPluginExtension;
import com.google.testing.compile.Compilation;
import com.google.testing.compile.CompilationSubject;
import com.google.testing.compile.JavaFileObjects;
import com.jeroensteenbeeke.andalite.java.analyzer.AnalyzedSourceFile;
import com.jeroensteenbeeke.andalite.java.analyzer.ClassAnalyzer;
import com.jeroensteenbeeke.hyperion.maven.test.HyperionMavenConfiguration;
import com.jeroensteenbeeke.lux.TypedResult;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.jetbrains.annotations.NotNull;
import javax.tools.JavaFileObject;
import java.io.File;
import java.net.MalformedURLException;

import static com.google.testing.compile.Compiler.javac;
import static com.jeroensteenbeeke.andalite.core.ResultMatchers.isOk;
import static com.jeroensteenbeeke.hyperion.maven.test.MavenFileUtil.getTestFile;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MavenPluginExtension.class)
public class GeneratorMojoTest {

	@Test
	public void testImmutableClassGeneration(MavenPluginContext context) throws
			MavenPluginExecutionException, MalformedURLException {
		File repository = getTestFile("src/test/resources/unit/testproject/target/generated-sources/immutables/com/jeroensteenbeeke/hyperion/immutable/generator/Repository.java");
		File unit = getTestFile("src/test/resources/unit/testproject/target/generated-sources/immutables/com/jeroensteenbeeke/hyperion/immutable/generator/Unit.java");
		File unitVisitor = getTestFile("src/test/resources/unit/testproject/target/generated-sources/immutables/com/jeroensteenbeeke/hyperion/immutable/generator/UnitVisitor.java");
		File alpha = getTestFile("src/test/resources/unit/testproject/target/generated-sources/immutables/com/jeroensteenbeeke/hyperion/immutable/generator/Alpha.java");
		File beta = getTestFile("src/test/resources/unit/testproject/target/generated-sources/immutables/com/jeroensteenbeeke/hyperion/immutable/generator/Beta.java");

		if (repository.exists()) {
			assertTrue(repository.delete());
		}

		if (unit.exists()) {
			assertTrue(unit.delete());
		}

		if (alpha.exists()) {
			assertTrue(alpha.delete());
		}

		if (beta.exists()) {
			assertTrue(beta.delete());
		}

		if (unitVisitor.exists()) {
			assertTrue(unitVisitor.delete());
		}

		File pom = getTestFile("src/test/resources/unit/testproject/pom.xml");
		File testRoot = pom.getParentFile();

		HyperionMavenConfiguration.configure(context, testRoot);
		MavenPluginGoalConfiguration configuration = MavenPluginContext.configuration();
		configuration.set(new MavenPluginGoalConfigurationElement() {
			@Override
			public <T> T map(MavenPluginGoalConfigurationElementMapper<T> mapper) {
				return mapper.map("annotationType", "Jetbrains");
			}
		});
		context.goal("generate").set(configuration).execute();

		assertTrue(repository.exists(), "Repository file should be generated");
		TypedResult<AnalyzedSourceFile> result = new ClassAnalyzer(repository).analyze();
		assertThat(result, isOk());

		assertTrue(unit.exists(), "Unit file should be generated");
		assertTrue(alpha.exists(), "Alpha file should be generated");
		assertTrue(beta.exists(), "Beta file should be generated");
		assertTrue(unitVisitor.exists(), "UnitVisitor file should be generated");

		String classPath = System.getProperty("java.class.path");
		Compilation compilation = javac().withOptions("-source", "17", "-target", "17", "-verbose", "-cp", classPath)
				.compile(
						java(unit),
						java(unitVisitor),
						java(repository),
						java(alpha),
						java(beta)
					);
		CompilationSubject.assertThat(compilation).succeededWithoutWarnings();
	}

	private JavaFileObject java(@NotNull File unit) throws MalformedURLException {
		return JavaFileObjects.forResource(unit.toURI().toURL());
	}

}
