package com.jeroensteenbeeke.hyperion.angulargen.model;

import com.jeroensteenbeeke.hyperion.rest.annotation.Model;
import com.jeroensteenbeeke.hyperion.rest.annotations.Queryable;

import java.util.Objects;

@Model
public class Item {
	@Queryable
	private Long id;

	private Person owner;

	public Item() {
	}

	public Item(Long id, Person owner) {
		this.id = id;
		this.owner = owner;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Person getOwner() {
		return owner;
	}

	public void setOwner(Person owner) {
		this.owner = owner;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Item item = (Item) o;
		return Objects.equals(id, item.id) && Objects.equals(owner, item.owner);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, owner);
	}
}
