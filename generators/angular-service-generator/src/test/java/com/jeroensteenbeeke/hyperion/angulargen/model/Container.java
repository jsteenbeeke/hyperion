package com.jeroensteenbeeke.hyperion.angulargen.model;

import com.jeroensteenbeeke.hyperion.rest.annotation.Model;

import java.util.List;

@Model
public class Container {
	private List<Item> items;

	private List<Classifier> classifiers;

	public Container() {

	}

	public List<Classifier> getClassifiers() {
		return classifiers;
	}

	public void setClassifiers(List<Classifier> classifiers) {
		this.classifiers = classifiers;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}
}
