package com.jeroensteenbeeke.hyperion.angulargen;

import com.github.codeteapot.maven.plugin.testing.MavenPluginContext;
import com.github.codeteapot.maven.plugin.testing.junit.jupiter.MavenPluginExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.File;

import static com.jeroensteenbeeke.hyperion.maven.test.MavenFileUtil.getBasedir;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MavenPluginExtension.class)
public class MojoExecutionTest  {

	@Test
	public void testStandaloneProjectExecution(MavenPluginContext context) throws Exception {
		File pom = new File(getBasedir(),
								"src/test/resources/mojotest/pom.xml");

		assertNotNull(pom);

		File testRoot = pom.getParentFile();

		context.setBaseDir(testRoot);
		context.goal("generate").set(MavenPluginContext.configuration()).execute();

	}
}
