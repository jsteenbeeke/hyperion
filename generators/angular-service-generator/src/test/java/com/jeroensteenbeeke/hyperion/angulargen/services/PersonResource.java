package com.jeroensteenbeeke.hyperion.angulargen.services;

import com.jeroensteenbeeke.hyperion.angulargen.model.Person;
import com.jeroensteenbeeke.hyperion.angulargen.model.query.Person_Query;
import com.jeroensteenbeeke.hyperion.rest.annotation.ReturnsListOf;
import com.jeroensteenbeeke.hyperion.rest.query.ListableResource;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Response;

@Path("/persons")
public interface PersonResource extends ListableResource<Person, Person_Query> {
	@GET
	@Path("/{category}")
	@ReturnsListOf(Person.class)
	Response getPersonsByCategory(@PathParam("category") String category);
}
