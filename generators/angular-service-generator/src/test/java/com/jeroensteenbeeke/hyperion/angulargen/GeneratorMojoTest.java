package com.jeroensteenbeeke.hyperion.angulargen;

import com.jeroensteenbeeke.hyperion.angulargen.model.Person;
import io.vavr.Tuple2;
import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import io.vavr.control.Option;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;


public class GeneratorMojoTest {
	private List<String> strings;

	private Person someOtherModel;

	private List<Person> someOtherModels;

	@Test
	public void determinePrefixes() {
		Set<String> prefixes = HashSet.empty();

		prefixes = GeneratorMojo.determinePrefixes(prefixes, "com.jeroensteenbeeke.hyperion.angulargen");

		assertThat(prefixes, equalTo(HashSet.of("com", "com.jeroensteenbeeke", "com.jeroensteenbeeke.hyperion", "com.jeroensteenbeeke.hyperion.angulargen")));
	}

	@Test
	public void determineType() throws NoSuchFieldException {
		Option<Tuple2<String, Class<?>>> type = GeneratorMojo.determineType(int.class, null, HashSet.of(Person.class));

		assertThat(type, equalTo(Option.some(new Tuple2<>("number", int.class))));

		type = GeneratorMojo.determineType(GeneratorMojoTest.class.getDeclaredField("strings"), HashSet.of(Person.class));

		assertThat(type, equalTo(Option.some(new Tuple2<>("string[]", String.class))));

		type = GeneratorMojo.determineType(GeneratorMojoTest.class.getDeclaredField("someOtherModel"), HashSet.of(Person.class));

		assertThat(type, equalTo(Option.some(new Tuple2<>("Person", Person.class))));

		type = GeneratorMojo.determineType(GeneratorMojoTest.class.getDeclaredField("someOtherModels"), HashSet.of(Person.class));

		assertThat(type, equalTo(Option.some(new Tuple2<>("Person[]", Person.class))));

	}

	@Test
	public void testLongestPrefixDetermination() {
		var prefixes = HashSet.of(
				"com",
				"com.jeroensteenbeeke",
				"com.jeroensteenbeeke.hyperion",
				"com.jeroensteenbeeke.hyperion.angulargen",
				"com.jeroensteenbeeke.hyperion.angulargen.data",
				"com.jeroensteenbeeke.hyperion.angulargen.templates",
				"com.jeroensteenbeeke.hyperion.angulargen.templates.library"
		);
		var packages = HashSet.of("com.jeroensteenbeeke.hyperion.angulargen.data", "com.jeroensteenbeeke.hyperion.angulargen.templates.library");

		assertThat(GeneratorMojo.findLongestCommonPrefix(prefixes, packages), equalTo("com.jeroensteenbeeke.hyperion.angulargen"));
	}
}
