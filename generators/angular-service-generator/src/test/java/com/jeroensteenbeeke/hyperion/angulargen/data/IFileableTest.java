package com.jeroensteenbeeke.hyperion.angulargen.data;

import org.junit.jupiter.api.Test;

import org.jetbrains.annotations.NotNull;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;


public class IFileableTest {

	@Test
	public void toTypescriptFile() {
		IFileable fileable = new IFileable() {
			@NotNull
			@Override
			public String getFilename() {
				return "";
			}

			@NotNull
			@Override
			public String getObjectName() {
				return "";
			}
		};

		assertThat(fileable.toTypescriptFile("Person"), equalTo("person.ts"));
		assertThat(fileable.toTypescriptFile("PersonQuery"), equalTo("person-query.ts"));
		assertThat(fileable.toTypescriptFile("PersonService"), equalTo("person-service.ts"));
	}
}
