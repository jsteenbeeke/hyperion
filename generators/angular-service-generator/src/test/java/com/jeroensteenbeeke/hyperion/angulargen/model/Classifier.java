package com.jeroensteenbeeke.hyperion.angulargen.model;

import com.jeroensteenbeeke.hyperion.rest.annotation.Model;

import java.util.Objects;

@Model
public class Classifier {
	private String uuid;

	public Classifier(String uuid) {
		this.uuid = uuid;
	}

	public Classifier() {
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Classifier that = (Classifier) o;
		return Objects.equals(uuid, that.uuid);
	}

	@Override
	public int hashCode() {
		return Objects.hash(uuid);
	}
}

