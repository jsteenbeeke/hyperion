package com.jeroensteenbeeke.hyperion.angulargen.model.meta;

import com.jeroensteenbeeke.hyperion.angulargen.model.Classifier;
import com.jeroensteenbeeke.hyperion.rest.annotation.Model;
import com.jeroensteenbeeke.hyperion.rest.annotations.Queryable;

import java.util.Objects;

@Model
public class Group {
	@Queryable
	private Long id;

	private Classifier classifier;

	public Group() {
	}

	public Group(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Classifier getClassifier() {
		return classifier;
	}

	public void setClassifier(Classifier classifier) {
		this.classifier = classifier;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Group group = (Group) o;
		return Objects.equals(id, group.id) && Objects.equals(classifier, group.classifier);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, classifier);
	}
}
