package com.jeroensteenbeeke.hyperion.angulargen.model.query;

import com.jeroensteenbeeke.hyperion.angulargen.model.Person;
import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;

public class Person_Query implements QueryObject<Person> {
	@Override
	public int getNextSortIndex() {
		return 0;
	}

	@Override
	public boolean matches(Person object) {
		return false;
	}
}
