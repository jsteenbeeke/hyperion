package com.jeroensteenbeeke.hyperion.angulargen.model;

import com.jeroensteenbeeke.hyperion.angulargen.model.meta.Group;
import com.jeroensteenbeeke.hyperion.rest.annotation.Model;
import com.jeroensteenbeeke.hyperion.rest.annotations.Queryable;

import java.util.Objects;

@Model
public class Person {
	@Queryable
	private Long id;

	@Queryable
	private String name;

	private Group group;

	public Person() {
	}

	public Person(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Person person = (Person) o;
		return Objects.equals(id, person.id) && Objects.equals(name, person.name) && Objects.equals(group, person.group);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, group);
	}
}
