package com.jeroensteenbeeke.hyperion.angulargen.data;


import org.junit.jupiter.api.Test;

import org.jetbrains.annotations.NotNull;

import static com.jeroensteenbeeke.vavr.hamcrest.VavrMatchers.isSuccess;
import static org.hamcrest.MatcherAssert.assertThat;

public class FolderTest {
	@Test
	public void testRelativePathSearch() {
		Folder root = Folder.root();
		Folder service = root.addSubFolder("service");
		Folder model = root.addSubFolder("model");
		Folder query = model.addSubFolder("query");

		Unit modelFile, queryFile;

		service.addFile(new Unit("application-service.ts"));
		model.addFile(modelFile = new Unit("application.ts"));
		query.addFile(queryFile = new Unit("application-query.ts"));

		assertThat(service.getRelativePosition(modelFile), isSuccess("../model/application.ts"));
		assertThat(service.getRelativePosition(queryFile), isSuccess("../model/query/application-query.ts"));
		assertThat(query.getRelativePosition(modelFile), isSuccess("../application.ts"));
	}

}

class Unit implements IFileable {
	private final String unitName;

	public Unit(String unitName) {
		this.unitName = unitName;
	}

	@NotNull
	@Override
	public String getFilename() {
		return unitName;
	}

	@NotNull
	@Override
	public String getObjectName() {
		return unitName;
	}
}
