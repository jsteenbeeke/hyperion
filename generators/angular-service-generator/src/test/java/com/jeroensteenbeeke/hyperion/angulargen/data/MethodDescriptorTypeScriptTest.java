package com.jeroensteenbeeke.hyperion.angulargen.data;


import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class MethodDescriptorTypeScriptTest {

	@Test
	public void testGetMethods() {
		MethodDescriptor method = new MethodDescriptor("get", "/", "getWidgets", "Widget[]");

		System.out.println(method.getTypescript());
		System.out.println();
		assertThat(method.getTypescript(), equalTo("\tgetWidgets(): Observable<Widget[]> {\n\t\tconst path = super.appendQueryString('/', '');\n\n\t\treturn super.http.get <Widget[]>(path, {})\n\t\t\t.pipe(\n\t\t\t\tcatchError(super.handleError('getWidgets', []))\n\t\t\t);\n\t}"));

		method = new MethodDescriptor("get", "/{category}/", "getWidgetsByCategory", "Widget[]")
				.addPathParameter("string", "category", "category");

		System.out.println(method.getTypescript());
		System.out.println();
		assertThat(method.getTypescript(), equalTo("\tgetWidgetsByCategory(category: string): Observable<Widget[]> {\n\t\tconst path = super.appendQueryString('/' + category + '/', '');\n\n\t\treturn super.http.get <Widget[]>(path, {})\n\t\t\t.pipe(\n\t\t\t\tcatchError(super.handleError('getWidgetsByCategory', []))\n\t\t\t);\n\t}"));

		method = new MethodDescriptor("get", "/{category}/", "searchInCategory", "string[]")
				.addPathParameter("string", "category", "category")
				.addQueryParameter("string", "query", "query", false)
				.addQueryParameter("number", "offset", "offset", false);

		System.out.println(method.getTypescript());
		System.out.println();

		assertThat(method.getTypescript(), equalTo("\tsearchInCategory(category: string, query?: string, offset?: number): Observable<string[]> {\n\t\tconst queryParams = [{ value: query, uriName: 'query', required: false},\n\t\t\t\t{ value: offset, uriName: 'offset', required: false}]\n\t\t\t.filter(param => param.required || param.value !== null)\n\t\t\t.map(param => param.uriName + '=' + param.value)\n\t\t\t.join('&');\n\t\tconst path = super.appendQueryString('/' + category + '/', queryParams);\n\n\t\treturn super.http.get <string[]>(path, {})\n\t\t\t.pipe(\n\t\t\t\tcatchError(super.handleError('searchInCategory', []))\n\t\t\t);\n\t}"));

	}
}
