package com.jeroensteenbeeke.hyperion.angulargen.data;

import org.jetbrains.annotations.NotNull;
import java.util.Objects;

/**
 * Representation of a field inside a TypeScript class
 */
public class FieldDescriptor {
	private final String name;

	private final String type;

	private final String fqdn;

	/**
	 * Constructor
	 * @param name The name of the field
	 * @param type The type of field
	 * @param fqdn The FQDN of the field type
	 */
	public FieldDescriptor(@NotNull String name, @NotNull String type, @NotNull String fqdn) {
		this.name = name;
		this.type = type;
		this.fqdn = fqdn;
	}

	/**
	 * @return The name
	 */
	@NotNull
	public String getName() {
		return name;
	}

	/**
	 * @return The type
	 */
	@NotNull
	public String getType() {
		return type;
	}

	/**
	 * @return The fully qualified domain name
	 */
	@NotNull
	public String getFqdn() {
		return fqdn;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		FieldDescriptor that = (FieldDescriptor) o;
		return name.equals(that.name) && type.equals(that.type) && fqdn.equals(that.fqdn);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, type, fqdn);
	}
}
