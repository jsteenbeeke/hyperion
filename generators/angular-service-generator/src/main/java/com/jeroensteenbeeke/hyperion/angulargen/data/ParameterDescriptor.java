package com.jeroensteenbeeke.hyperion.angulargen.data;

import java.util.Objects;

/**
 * Describes a parameter
 */
public class ParameterDescriptor {
	private final String type;

	private final String apiName;

	private final String paramName;

	private final boolean required;

	/**
	 * Constructor
	 * @param type The parameter type
	 * @param apiName The name as described in the API (can mean different things)
	 * @param paramName The name of the parameter
	 * @param required Whether or not the parameter is required
	 */
	public ParameterDescriptor(String type, String apiName, String paramName, boolean required) {
		this.type = type;
		this.apiName = apiName;
		this.paramName = paramName;
		this.required = required;
	}

	/**
	 * @return The type of parameter
	 */
	public String getType() {
		return type;
	}

	/**
	 * @return The API name, can be mean different things depending on the type of parameter. For instance,
	 * for a header parameter this is the field as included in the HTTP header. For a path parameter, this is
	 * the name as included in the JAX-RS path specification.
	 */
	public String getApiName() {
		return apiName;
	}

	/**
	 * @return The name of the parameter as shown in the generated source code
	 */
	public String getParamName() {
		return paramName;
	}

	/**
	 * @return Whether or not the field is required
	 */
	public boolean isRequired() {
		return required;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ParameterDescriptor that = (ParameterDescriptor) o;
		return type.equals(that.type) && apiName.equals(that.apiName) && paramName.equals(that.paramName);
	}

	@Override
	public int hashCode() {
		return Objects.hash(type, apiName, paramName);
	}
}
