package com.jeroensteenbeeke.hyperion.angulargen.data;

import io.vavr.collection.Array;
import io.vavr.collection.HashMap;

import org.jetbrains.annotations.NotNull;
import java.util.List;

/**
 * Representation of a model class from a web project
 */
public class ModelClass implements IFileable{
	private final String name;

	private HashMap<String, ImportDescriptor> imports;

	private Array<FieldDescriptor> fields;

	/**
	 * Constructor
	 *
	 * @param name Name of the model class
	 */
	public ModelClass(@NotNull String name) {
		this(name, HashMap.empty(), Array.empty());
	}

	private ModelClass(@NotNull String name, @NotNull HashMap<String, ImportDescriptor> imports, @NotNull Array<FieldDescriptor> fields) {
		this.name = name;
		this.imports = imports;
		this.fields = fields;
	}

	/**
	 * Returns the imports as a mutable Java list
	 *
	 * @return The imports
	 */
	public List<ImportDescriptor> getImports() {
		return imports.values().sortBy(ImportDescriptor::getPackageName).toJavaList();
	}

	/**
	 * Returns the fields as a mutable Java list
	 *
	 * @return The fields
	 */
	public List<FieldDescriptor> getFields() {
		return fields.toJavaList();
	}

	/**
	 * Creates a copy of the current class with the import descriptor added
	 *
	 * @param importItem The item to import from the package
	 * @param packageName The package
	 * @return The current object
	 */
	@NotNull
	public ModelClass withImport(@NotNull String importItem, @NotNull String packageName) {
		if (!importItem.equals(name)) {
			this.imports = imports.put(packageName, imports
					.getOrElse(packageName, new ImportDescriptor(packageName))
					.withItem(importItem));
		}

		return this;
	}

	/**
	 * Creates a copy of the current class with the field added
	 *
	 * @param field The field
	 * @return The current object
	 */
	@NotNull
	public ModelClass withField(@NotNull FieldDescriptor field) {
		this.fields = fields.append(field);

		return this;
	}

	@NotNull
	@Override
	public String getObjectName() {
		return getName();
	}

	@NotNull
	@Override
	public String getFilename() {
		return toTypescriptFile(name);
	}

	/**
	 * @return The name of the model class
	 */
	public String getName() {
		return name;
	}
}
