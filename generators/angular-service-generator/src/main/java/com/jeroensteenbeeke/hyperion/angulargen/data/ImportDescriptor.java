package com.jeroensteenbeeke.hyperion.angulargen.data;

import io.vavr.collection.Array;

import org.jetbrains.annotations.NotNull;
import java.util.List;
import java.util.Objects;

/**
 * Describes an import statement
 */
public class ImportDescriptor {
	private final Array<String> importItems;

	private final String packageName;

	/**
	 * Constructor
	 * @param packageName The package to import it from
	 */
	public ImportDescriptor(String packageName) {
		this.packageName = packageName;
		this.importItems = Array.of();
	}

	private ImportDescriptor(String packageName, Array<String> importItem) {
		this.importItems = importItem;
		this.packageName = packageName;
	}

	/**
	 * Gets the package to import from
	 * @return The package name
	 */
	public String getPackageName() {
		return packageName;
	}

	/**
	 * Gets the items to import
	 * @return The list of items
	 */
	public List<String> getImportItems() {
		return importItems.toJavaList();
	}

	/**
	 * Creates a new import descriptor with the item added
	 * @param importItem The item to add
	 * @return A new ImportDescriptor
	 */
	@NotNull
	public ImportDescriptor withItem(@NotNull String importItem) {
		return new ImportDescriptor(packageName, importItems.append(importItem));
	}

	/**
	 * Creates a new import descriptor that is the combination of the current and the provided descriptor
	 * @param toMerge The descriptor to merge
	 * @return A new import descriptor
	 */
	public ImportDescriptor merge(ImportDescriptor toMerge) {
		return new ImportDescriptor(packageName, importItems.toSet().addAll(toMerge.importItems).toArray());
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ImportDescriptor that = (ImportDescriptor) o;
		return importItems.equals(that.importItems) && packageName.equals(that.packageName);
	}

	@Override
	public int hashCode() {
		return Objects.hash(importItems, packageName);
	}
}
