package com.jeroensteenbeeke.hyperion.angulargen.data;

import io.vavr.collection.Array;

import org.jetbrains.annotations.NotNull;
import java.util.List;
import java.util.Set;

/**
 * Representation of a query class from a web project
 */
public class QueryClass implements IFileable{
	private final String name;

	private Array<FieldDescriptor> fields;

	/**
	 * Constructor
	 *
	 * @param name Name of the query class
	 */
	public QueryClass(@NotNull String name) {
		this(name, Array.empty());
	}

	private QueryClass(@NotNull String name, @NotNull Array<FieldDescriptor> fields) {
		this.name = name;
		this.fields = fields;
	}

	/**
	 * Returns the fields as a mutable Java list
	 *
	 * @return The fields
	 */
	public List<FieldDescriptor> getFields() {
		return fields.toJavaList();
	}

	/**
	 * Returns the types of the fields as a mutable Java set
	 *
	 * @return The property types
	 */
	public Set<String> getPropertyTypes() {
		return fields.map(FieldDescriptor::getType).toJavaSet();
	}

	/**
	 * Creates a copy of the current class with the field added
	 *
	 * @param field The field
	 * @return A new class
	 */
	@NotNull
	public QueryClass withField(@NotNull FieldDescriptor field) {
		this.fields = fields.append(field);

		return this;
	}

	@NotNull
	@Override
	public String getFilename() {
		return toTypescriptFile(name);
	}

	@NotNull
	@Override
	public String getObjectName() {
		return getName();
	}

	/**
	 * @return The name of the object
	 */
	public String getName() {
		return name;
	}
}
