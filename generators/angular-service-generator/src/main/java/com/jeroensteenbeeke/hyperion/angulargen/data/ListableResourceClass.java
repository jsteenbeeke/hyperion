package com.jeroensteenbeeke.hyperion.angulargen.data;

import io.vavr.collection.Array;

import org.jetbrains.annotations.NotNull;
import java.util.Objects;

/**
 * A service class that is listable but not mutable
 */
public class ListableResourceClass extends ServiceClass {

	private final ModelClass modelType;

	private final QueryClass queryObjectType;

	/**
	 * Constructor
	 * @param serviceName The name of the service
	 * @param resourceUrl The URL of the resource
	 * @param modelType The type of model
	 * @param queryObjectType The type of query object
	 */
	public ListableResourceClass(@NotNull String serviceName,
								 @NotNull String resourceUrl,
								 @NotNull ModelClass modelType,
								 @NotNull QueryClass queryObjectType) {
		super(serviceName, resourceUrl);

		this.modelType = modelType;
		this.queryObjectType = queryObjectType;
	}

	/**
	 * @return The type of model
	 */
	@NotNull
	public ModelClass getModelType() {
		return modelType;
	}

	/**
	 * @return The type of query object
	 */
	@NotNull
	public QueryClass getQueryObjectType() {
		return queryObjectType;
	}

	@Override
	public Array<IFileable> getReferencedTypes() {
		return Array.of(getQueryObjectType(), getModelType());
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ListableResourceClass that = (ListableResourceClass) o;
		return modelType.equals(that.modelType) && queryObjectType.equals(that.queryObjectType);
	}

	@Override
	public int hashCode() {
		return Objects.hash(modelType, queryObjectType);
	}

	@Override
	public String getTemplate() {
		return "listable-resource.ftl";
	}
}
