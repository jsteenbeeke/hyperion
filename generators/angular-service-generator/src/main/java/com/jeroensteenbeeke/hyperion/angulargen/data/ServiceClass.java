package com.jeroensteenbeeke.hyperion.angulargen.data;

import io.vavr.collection.Array;
import io.vavr.control.Option;

import org.jetbrains.annotations.NotNull;
import java.util.List;
import java.util.Objects;

/**
 * A service class
 */
public class ServiceClass implements IFileable {
	private final String serviceName;

	private final String resourceUrl;

	private Array<ImportDescriptor> imports;

	private Array<MethodDescriptor> methods;

	/**
	 * Constructor
	 * @param serviceName The name of the service
	 * @param resourceUrl The base resource URL of the service
	 */
	public ServiceClass(@NotNull String serviceName, @NotNull String resourceUrl) {
		this.serviceName = serviceName;
		this.resourceUrl = resourceUrl;
		this.imports = Array.empty();
		this.methods = Array.empty();
	}

	/**
	 * @return The name of the service
	 */
	@NotNull
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * @return The URL of the resource
	 */
	@NotNull
	public String getResourceUrl() {
		return resourceUrl;
	}

	/**
	 * @return The list of import descriptors
	 */
	@NotNull
	public List<ImportDescriptor> getImports() {
		return imports.toJavaList();
	}

	/**
	 * @return The list of methods
	 */
	@NotNull
	public List<MethodDescriptor> getMethods() {
		return methods.toJavaList();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ServiceClass that = (ServiceClass) o;
		return serviceName.equals(that.serviceName) && imports.equals(that.imports) && methods.equals(that.methods);
	}

	@Override
	public int hashCode() {
		return Objects.hash(serviceName, imports, methods);
	}

	/**
	 * Adds an import
	 * @param importDescriptor The import
	 * @return The current class
	 */
	@NotNull
	public ServiceClass addImport(@NotNull ImportDescriptor importDescriptor) {
		Option<ImportDescriptor> existingDescriptor = imports.find(i -> i
				.getPackageName()
				.equals(importDescriptor.getPackageName()));

		if (existingDescriptor.isDefined()) {
			ImportDescriptor element = existingDescriptor.get();
			this.imports = imports.replace(element, element
					.merge(importDescriptor));
		} else {
			this.imports = imports.append(importDescriptor);
		}
		return this;
	}

	/**
	 * Adds a method
	 * @param methodDescriptor The method to add
	 * @return The current class
	 */
	@NotNull
	public ServiceClass addMethod(@NotNull MethodDescriptor methodDescriptor) {
		this.methods = methods.append(methodDescriptor);
		return this;
	}

	@NotNull
	@Override
	public String getFilename() {
		return toTypescriptFile(serviceName);
	}

	@NotNull
	@Override
	public String getObjectName() {
		return serviceName;
	}

	/**
	 * @return The template to use for this service
	 */
	public String getTemplate() {
		return "service.ftl";
	}

	/**
	 * @return The types referenced by this service
	 */
	public Array<IFileable> getReferencedTypes() {
		return Array.empty();
	}
}
