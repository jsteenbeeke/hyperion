import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { QueryObject } from '@jeroensteenbeeke/hyperion-queryobjects';
import { Observable, of } from 'rxjs';
import { HyperionRestApiService } from '@jeroensteenbeeke/hyperion-rest-api';

<#list imports?sort as import>
import { <#list import.importItems as item>${item}<#sep>, </#list> } from '${import.packageName}';
</#list>


@Injectable({
    providedIn: 'root'
})
export class ${serviceName} extends HyperionRestApiService<any, any> {

    constructor(injector: Injector, httpClient: HttpClient) {
        super('${resourceUrl}', injector, httpClient);
    }

<#list methods as method>
${method.typescript}
</#list>
}
