<#list imports as import>
import { <#list import.importItems as item>${item}<#sep>, </#list> } from '${import.packageName}';
</#list>

export class ${name} {
<#list fields as field>
    ${field.name}: ${field.type};
</#list>
}
