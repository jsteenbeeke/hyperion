package com.jeroensteenbeeke.hyperion.angulargen.data;

import io.vavr.collection.Array;

import org.jetbrains.annotations.NotNull;
import java.util.Objects;

/**
 * A service class that is both listable and mutable
 */
public class ListableMutableResourceClass extends ServiceClass {

	private final ModelClass modelType;

	private final QueryClass queryObjectType;

	private final String idType;

	/**
	 * Constructor
	 * @param serviceName Tne name of the service
	 * @param resourceUrl The resource URL
	 * @param modelType The model type
	 * @param queryObjectType The query object type
	 * @param idType The type of ID
	 */
	public ListableMutableResourceClass(@NotNull String serviceName,
										@NotNull String resourceUrl,
										@NotNull ModelClass modelType,
										@NotNull QueryClass queryObjectType,
										@NotNull String idType) {
		super(serviceName, resourceUrl);

		this.modelType = modelType;
		this.queryObjectType = queryObjectType;
		this.idType = idType;
	}

	/**
	 * @return The type of model
	 */
	@NotNull
	public ModelClass getModelType() {
		return modelType;
	}

	/**
	 * @return The type of query object
	 */
	@NotNull
	public QueryClass getQueryObjectType() {
		return queryObjectType;
	}

	@Override
	public Array<IFileable> getReferencedTypes() {
		return Array.of(getModelType(), getQueryObjectType());
	}

	/**
	 * @return The type of ID
	 */
	@NotNull
	public String getIdType() {
		return idType;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ListableMutableResourceClass that = (ListableMutableResourceClass) o;
		return modelType.equals(that.modelType) && queryObjectType.equals(that.queryObjectType) && idType.equals(that.idType);
	}

	@Override
	public int hashCode() {
		return Objects.hash(modelType, queryObjectType, idType);
	}

	@Override
	public String getTemplate() {
		return "mutable-listable-resource.ftl";
	}
}
