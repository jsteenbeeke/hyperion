import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { HyperionRestApiService } from '@jeroensteenbeeke/hyperion-rest-api';

<#list imports?sort as import>
import { <#list import.importItems as item>${item}<#sep>, </#list> } from '${import.packageName}';
</#list>


@Injectable({
    providedIn: 'root'
})
export class ${serviceName} extends HyperionRestApiService<any, ${modelType.name}> {

    constructor(injector: Injector, httpClient: HttpClient) {
        super('${resourceUrl}', injector, httpClient);
    }

    get(id: ${idType}): Observable<${modelType.name}> {
        return super.getByID(id);
    }

    create(object: ${modelType.name}): Observable<${modelType.name}> {
        return super.create(object);
    }

    update(id: ${idType}, object: ${modelType.name}): Observable<${modelType.name}> {
        return super.updateByID(id, object);
    }

    deleteByID(id: ${idType}): Observable<void> {
        return super.deleteByID(id);
    }

<#list methods as method>
${method.typescript}
</#list>
}
