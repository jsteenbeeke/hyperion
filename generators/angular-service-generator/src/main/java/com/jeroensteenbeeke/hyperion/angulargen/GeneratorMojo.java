/**
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version. <p> This program is distributed in the
 * hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details. <p> You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.angulargen;

import com.jeroensteenbeeke.hyperion.angulargen.data.*;
import com.jeroensteenbeeke.hyperion.rest.annotation.*;
import com.jeroensteenbeeke.hyperion.rest.annotations.Queryable;
import com.jeroensteenbeeke.hyperion.rest.query.ListableResource;
import com.jeroensteenbeeke.hyperion.rest.query.MutableResource;
import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import io.vavr.Tuple2;
import io.vavr.collection.Array;
import io.vavr.collection.*;
import io.vavr.control.Option;
import io.vavr.control.Try;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.*;
import org.apache.maven.project.MavenProject;
import org.reflections.Reflections;
import org.reflections.scanners.*;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.sonatype.plexus.build.incremental.BuildContext;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.*;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Maven plugin for generating retrofit classes for JAX-RS services
 */
@Mojo(name = "generate", defaultPhase = LifecyclePhase.GENERATE_SOURCES,
		requiresDependencyResolution = ResolutionScope.COMPILE)
public class GeneratorMojo extends AbstractMojo {
	private static final Map<Class<?>, String> TYPESCRIPT_MAPPINGS = HashMap.<Class<?>, String>empty()
			.put(boolean.class, "boolean")
			.put(Boolean.class, "boolean")
			.put(double.class, "number")
			.put(Double.class, "number")
			.put(float.class, "number")
			.put(Float.class, "number")
			.put(int.class, "number")
			.put(Integer.class, "number")
			.put(LocalDate.class, "Date")
			.put(LocalDateTime.class, "Date")
			.put(long.class, "number")
			.put(Long.class, "number")
			.put(short.class, "number")
			.put(Short.class, "number")
			.put(String.class, "string");

	private static final Map<Class<?>, String> QUERY_MAPPINGS = HashMap.<Class<?>, String>empty()
			.put(boolean.class, "BooleanProperty")
			.put(Boolean.class, "BooleanProperty")
			.put(double.class, "DecimalProperty")
			.put(Double.class, "DecimalProperty")
			.put(float.class, "DecimalProperty")
			.put(Float.class, "DecimalProperty")
			.put(int.class, "IntegerProperty")
			.put(Integer.class, "IntegerProperty")
			.put(LocalDate.class, "DateProperty")
			.put(LocalDateTime.class, "DateProperty")
			.put(long.class, "IntegerProperty")
			.put(Long.class, "IntegerProperty")
			.put(short.class, "IntegerProperty")
			.put(Short.class, "IntegerProperty")
			.put(String.class, "StringProperty");

	@Parameter(required = true)
	public String basePackage;

	@Parameter(required = false, defaultValue = "generated-sources")
	public String generatedSourcesDir;

	@Parameter(required = false, defaultValue = "angular")
	public String generatedSourcesSubdir;

	@Parameter(required = false)
	public String resourceClassPrefix;

	@Parameter(required = false)
	public String modelClassPrefix;

	@Parameter(required = false)
	public String queryClassPrefix;

	/**
	 * @component
	 */
	@Component
	public BuildContext buildContext;

	@Parameter(required = true, property = "project")
	@Inject
	public MavenProject project;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		generatedSourcesDir = Optional.ofNullable(generatedSourcesDir).orElse("generated-sources");
		generatedSourcesSubdir = Optional.ofNullable(generatedSourcesSubdir).orElse("angular");
		resourceClassPrefix = Optional.ofNullable(resourceClassPrefix).orElse("");
		modelClassPrefix = Optional.ofNullable(modelClassPrefix).orElse("");
		queryClassPrefix = Optional.ofNullable(queryClassPrefix).orElse("");
		basePackage = Optional.ofNullable(basePackage).orElse("");

		final var target = Optional
				.ofNullable(project)
				.map(MavenProject::getBasedir)
				.orElseGet(() -> new File(""))
				.toPath()
				.resolve("target")
				.resolve(generatedSourcesDir)
				.resolve(generatedSourcesSubdir);

		try {
			Files.createDirectories(target);
		} catch (IOException e) {
			throw new MojoExecutionException("Could not create target directory");
		}


		Reflections ref = new Reflections(new ConfigurationBuilder()
				.setUrls(ClasspathHelper.forPackage(basePackage))
				.setScanners(Scanners.SubTypes, Scanners.TypesAnnotated,
						Scanners.FieldsAnnotated, Scanners.MethodsAnnotated)
		);

		Map<Class<?>, ModelClass> mappings = HashMap.empty();
		Map<String, QueryClass> queries = HashMap.empty();
		Set<ServiceClass> serviceClasses = HashSet.empty();

		Map<ModelClass, Folder> classFolders = HashMap.empty();
		Map<QueryClass, Folder> queryFolders = HashMap.empty();


		getLog().info("Converting model classes");
		Set<Class<?>> modelClasses = HashSet.ofAll(ref.getTypesAnnotatedWith(Model.class));
		Set<Class<?>> pathClasses = HashSet.ofAll(ref.getTypesAnnotatedWith(Path.class));

		getLog().info(String.format("  Found %d classes", modelClasses.size()));
		Set<String> modelPackages = HashSet.empty();
		Set<String> modelClassPrefixes = HashSet.empty();
		Set<String> serviceClassPrefixes = HashSet.empty();
		Set<String> serviceClassPackages = HashSet.empty();


		for (Class<?> modelClass : modelClasses) {
			String packageName = modelClass.getPackageName();

			modelClassPrefixes = determinePrefixes(modelClassPrefixes, packageName);

			modelPackages = modelPackages.add(packageName);
		}

		for (Class<?> pathClass : pathClasses) {
			if (!ListableResource.class.isAssignableFrom(pathClass)) {
				continue;
			}

			String packageName = pathClass.getPackageName();

			serviceClassPrefixes = determinePrefixes(serviceClassPrefixes, packageName);

			serviceClassPackages = serviceClassPackages.add(packageName);
		}

		final Folder root = Folder.root();
		final Folder lib = root.addSubFolder("lib");
		Folder services = lib.addSubFolder("services");
		Folder model = lib.addSubFolder("model");
		Folder query = lib.addSubFolder("query");


		if (!modelClassPrefixes.isEmpty()) {
			String longestCommonPrefix = findLongestCommonPrefix(modelClassPrefixes, modelPackages);
			for (Class<?> modelClass : modelClasses) {
				if (!modelClass.isInterface()) {
					Option<ModelClass> description = processModelClass(modelClass, modelClasses);
					if (description.isDefined()) {
						ModelClass modelClassDescription = description.get();
						mappings = mappings.put(modelClass, modelClassDescription);
						String packageName = modelClass.getPackageName();
						if (!packageName.startsWith(longestCommonPrefix)) {
							throw new MojoFailureException("Found model class in unmapped package " + packageName);
						}

						String relativePath = packageName.equals(longestCommonPrefix) ? "" : packageName
								.substring(longestCommonPrefix
										.length() + 1);

						Folder targetFolder = determineTargetFolder(relativePath, model);
						classFolders = classFolders.put(modelClassDescription, targetFolder);
						targetFolder.addFile(modelClassDescription);
					}
					Option<QueryClass> queryObjectOpt = processQueryObject(modelClass);
					if (queryObjectOpt.isDefined()) {
						QueryClass queryObject = queryObjectOpt.get();

						String queryPackageName = String.format("%s.query", modelClass.getPackageName());

						queries = queries.put(String.format("%s.%s_Query", queryPackageName, modelClass
								.getSimpleName()), queryObject);

						String packageName = modelClass.getPackageName();
						if (!packageName.startsWith(longestCommonPrefix)) {
							throw new MojoFailureException("Found query class in unmapped package " + packageName);
						}

						String relativePath = packageName.equals(longestCommonPrefix) ? "" : packageName
								.substring(longestCommonPrefix
										.length() + 1);

						Folder targetFolder = determineTargetFolder(relativePath, query);
						queryFolders = queryFolders.put(queryObject, targetFolder);
						targetFolder.addFile(queryObject);
					}
				} else {
					getLog().info(
							String.format("Class %s is an interface, ignoring", modelClass.getName()));
				}
			}
		}


		final var fqdnToModelClass = mappings.mapKeys(Class::getName);
		final var finalClassFolders = classFolders;


		mappings = mappings.mapValues(m -> Array
				.ofAll(m.getFields())
				.foldLeft(m, (mc, f) -> fqdnToModelClass
						.get(f.getFqdn())
						.flatMap(tmc -> finalClassFolders
								.get(mc)
								.map(mcf -> addImport(mc, mcf, tmc)))
						.getOrElse(mc)));


		getLog().info("Finding JAX-RS interfaces");
		getLog().info(String.format("Found %d classes", pathClasses.size()));

		if (!serviceClassPrefixes.isEmpty()) {
			String longestCommonPrefix = findLongestCommonPrefix(serviceClassPrefixes, serviceClassPackages);
			for (Class<?> c : pathClasses) {
				if (!c.isInterface()) {
					getLog().error(String.format(
							"Class %s is not an interface, ignoring",
							c.getName()));

					continue;
				}

				Option<Tuple2<ServiceClass, Array<IFileable>>> optionalServiceClass = processInterface(c, mappings,
						queries);

				if (optionalServiceClass.isDefined()) {
					Tuple2<ServiceClass, Array<IFileable>> serviceClassAndReferences = optionalServiceClass
							.get();
					ServiceClass serviceClass = serviceClassAndReferences._1();
					Array<IFileable> references = serviceClassAndReferences._2();

					String packageName = c.getPackageName();
					if (!packageName.startsWith(longestCommonPrefix)) {
						throw new MojoFailureException(
								"Found service class in unmapped package " + packageName + " expected prefix: " + longestCommonPrefix);
					}

					String relativePath = packageName.equals(longestCommonPrefix) ? "" : packageName
							.substring(longestCommonPrefix
									.length() + 1);

					Folder targetFolder = determineTargetFolder(relativePath, services);

					serviceClasses = serviceClasses.add(references.foldLeft(serviceClass, (sc, rf) -> targetFolder
							.getRelativePosition(rf)
							.map(rp -> sc.addImport(
									new ImportDescriptor(removeExtension(rp)).withItem(rf.getObjectName())))
							.getOrElse(sc)));

					targetFolder.addFile(serviceClass);
				}
			}
		}

		Configuration configuration = new Configuration(Configuration.VERSION_2_3_31);
		configuration.setClassForTemplateLoading(GeneratorMojo.class, "templates");
		configuration.setDefaultEncoding("UTF-8");

		mappings.values().forEach(mc -> root.getRelativePosition(mc).onSuccess(path -> {
			final var targetFile = target.resolve(path);
			final var targetFolder = targetFile.getParent();

			try {
				Files.createDirectories(targetFolder);

				Template template = configuration.getTemplate("model.ftl");

				try (Writer out = Files.newBufferedWriter(targetFile)) {
					template.process(mc, out);
				}
			} catch (IOException | TemplateException e) {
				throw new RuntimeException(e);
			}

		}));

		queries
				.values()
				.forEach(queryClass -> root.getRelativePosition(queryClass).onSuccess(path -> {
					final var targetFile = target.resolve(path);
					final var targetFolder = targetFile.getParent();

					try {
						Files.createDirectories(targetFolder);

						Template template = configuration.getTemplate("query-object.ftl");

						try (Writer out = Files.newBufferedWriter(targetFile)) {
							template.process(queryClass, out);
						}
					} catch (IOException | TemplateException e) {
						throw new RuntimeException(e);
					}
				}));

		serviceClasses
				.forEach(serviceClass -> root.getRelativePosition(serviceClass).onSuccess(path -> {
					final var targetFile = target.resolve(path);
					final var targetFolder = targetFile.getParent();

					try {
						Files.createDirectories(targetFolder);

						Template template = configuration.getTemplate(serviceClass.getTemplate());

						try (Writer out = Files.newBufferedWriter(targetFile)) {
							template.process(serviceClass, out);
						}
					} catch (IOException | TemplateException e) {
						throw new RuntimeException(e);
					}
				}));

		java.util.List<String> allPaths = root.allPaths().map(this::removeExtension).toJavaList();

		try {
			Template template = configuration.getTemplate("public-api.ftl");

			try (Writer out = Files.newBufferedWriter(target.resolve("public-api.ts"))) {
				template.process(java.util.Map.of("files", allPaths), out);
			}
		} catch (IOException | TemplateException e) {
			throw new RuntimeException(e);
		}
	}

	private ModelClass addImport(ModelClass source, Folder sourceFolder, ModelClass target) {
		Try<String> relativePosition = sourceFolder.getRelativePosition(target);

		return relativePosition.map(position -> {
			if (position.endsWith(".ts")) {
				position = position.substring(0, position.length() - 3);
			}

			if (!position.startsWith(".")) {
				position = "./" + position;
			}

			return source.withImport(target.getName(), position);
		}).getOrElse(source);
	}

	private Folder determineTargetFolder(String relativePath, Folder base) {
		String[] parts = relativePath.split("\\.");

		if (parts.length >= 1 && !parts[0].isEmpty()) {
			for (String part : parts) {
				Option<Folder> next = base.getFolder(part);
				if (next.isEmpty()) {
					base = base.addSubFolder(part);
				} else {
					base = next.get();
				}
			}
		}
		return base;
	}

	static String findLongestCommonPrefix(Set<String> prefixes, Set<String> finalPackages) {
		return prefixes.filter(p -> finalPackages.forAll(pkg -> pkg.startsWith(p)))
				.toSortedSet(Comparator.comparingInt(String::length).reversed())
				.head();
	}

	static Set<String> determinePrefixes(Set<String> prefixes, String packageName) {
		String[] parts = packageName.split("\\.");

		for (int i = 1; i <= parts.length; i++) {
			prefixes = prefixes.add(Arrays.stream(parts).limit(i).collect(Collectors.joining(".")));
		}
		return prefixes;
	}

	private Option<ModelClass> processModelClass(Class<?> modelClass, Set<Class<?>> modelClasses) {
		getLog().info(String.format("Processing model class %s", modelClass.getName()));

		ModelClass m = new ModelClass(modelClass.getSimpleName());

		for (Field f : modelClass.getDeclaredFields()) {
			if (Modifier.isStatic(f.getModifiers())) {
				continue;
			}

			if ("$jacocoData".equals(f.getName())) {
				continue;
			}

			Option<Tuple2<String, Class<?>>> type = determineType(f, modelClasses);

			if (type.isDefined()) {
				m = m.withField(new FieldDescriptor(f.getName(), type.get()._1(), type.get()._2().getName()));
			}
		}

		return Option.some(m);
	}


	private Option<QueryClass> processQueryObject(Class<?> modelClass) {
		getLog().info(String.format("Processing model class for query objects %s", modelClass.getName()));

		QueryClass m = new QueryClass(modelClass.getSimpleName() + "Query");

		Set<Field> queryableFields = HashSet.empty();

		for (Field f : modelClass.getDeclaredFields()) {
			if (Modifier.isStatic(f.getModifiers())) {
				continue;
			}

			if ("$jacocoData".equals(f.getName())) {
				continue;
			}

			if (f.isAnnotationPresent(Queryable.class)) {
				queryableFields = queryableFields.add(f);
			}
		}

		if (queryableFields.isEmpty()) {
			getLog().info("No Queryable fields found");
			return Option.none();
		}

		for (Field f : queryableFields) {
			Option<String> type = determineQueryPropertyType(f);

			if (type.isDefined()) {
				m = m.withField(new FieldDescriptor(f.getName(), type.get(), f.getType().getName()));
			}
		}

		return Option.some(m);
	}

	private Option<String> determineQueryPropertyType(Field f) {
		return QUERY_MAPPINGS.get(f.getType());
	}


	static Option<Tuple2<String, Class<?>>> determineType(Field field, Set<Class<?>> modelClasses) {
		Class<?> type = field.getType();
		Type gt = field.getGenericType();

		return determineType(type, gt, modelClasses);

	}

	static Option<Tuple2<String, Class<?>>> determineType(Class<?> type, Type genericType, Set<Class<?>> modelClasses) {
		Option<Tuple2<String, Class<?>>> regularMapping = TYPESCRIPT_MAPPINGS
				.get(type)
				.map(t -> new Tuple2<>(t, type));

		return regularMapping.orElse(() -> {
			if (modelClasses.contains(type)) {
				return Option.some(new Tuple2<>(type.getSimpleName(), type));
			}

			return Option.none();
		}).orElse(() -> {
			if (java.util.List.class.isAssignableFrom(type)) {
				if (genericType instanceof ParameterizedType) {
					ParameterizedType pType = (ParameterizedType) genericType;
					Type actualTypeArgument = pType.getActualTypeArguments()[0];

					if (actualTypeArgument instanceof Class<?>) {
						return determineType((Class<?>) actualTypeArgument, null, modelClasses)
								.map(t -> t.map1(v -> v + "[]"));
					}
				}
			}

			return Option.none();
		});
	}


	private Option<Tuple2<ServiceClass, Array<IFileable>>> processInterface(
			Class<?> c,
			Map<Class<?>, ModelClass> modelClasses, Map<String, QueryClass> queryClasses) {
		String pkg = c.getPackage().getName();

		getLog().info(String.format("Processing resource %s.%s", pkg,
				c.getSimpleName()));

		final var stringModelClasses = modelClasses.mapKeys(Class::getName);
		final var stringTypescriptMappings = TYPESCRIPT_MAPPINGS.mapKeys(Class::getName);

		Option<Tuple2<ModelClass, QueryClass>> listable = Option.none();
		Option<Tuple2<ModelClass, String>> mutable = Option.none();

		for (Type genericInterface : c.getGenericInterfaces()) {
			String typeName = genericInterface
					.getTypeName();
			if (typeName.startsWith("com.jeroensteenbeeke.hyperion.rest.query.ListableResource")) {
				ParameterizedType pt = (ParameterizedType) genericInterface;

				String modelTypeFqdn = pt.getActualTypeArguments()[0].getTypeName();
				String queryObjectTypeFqdn = pt.getActualTypeArguments()[1].getTypeName();

				listable = stringModelClasses
						.get(modelTypeFqdn)
						.flatMap(mc -> queryClasses
								.get(queryObjectTypeFqdn)
								.map(qo -> new Tuple2<>(mc, qo)));
			} else if (typeName.startsWith("com.jeroensteenbeeke.hyperion.rest.query.MutableResource")) {
				ParameterizedType pt = (ParameterizedType) genericInterface;

				String modelTypeFqdn = pt.getActualTypeArguments()[0].getTypeName();
				String idTypeFqdn = pt.getActualTypeArguments()[1].getTypeName();

				mutable = stringModelClasses
						.get(modelTypeFqdn)
						.flatMap(mc -> stringTypescriptMappings
								.get(idTypeFqdn)
								.map(qo -> new Tuple2<>(mc, qo)));
			}
		}

		Path path = c.getAnnotation(Path.class);
		String prefix = path != null ? path.value() : "";

		if (!prefix.startsWith("/")) {
			prefix = "/" + prefix;
		}

		final var finalPrefix = prefix;

		final var finalListable = listable;

		Option<ServiceClass> targetClass;

		if (listable.isDefined() || mutable.isDefined()) {
			if (listable.isEmpty()) {
				targetClass = mutable.map(t -> new MutableResourceClass(c.getSimpleName(), finalPrefix, t
						._1(), t
						._2()));
			} else if (mutable.isEmpty()) {
				targetClass = listable.map(t -> new ListableResourceClass(c.getSimpleName(), finalPrefix, t
						._1(), t
						._2()));
			} else {
				targetClass = mutable.flatMap(
						m -> finalListable.map(l -> new ListableMutableResourceClass(c.getSimpleName(), finalPrefix, m
								._1(), l._2(), m._2())));
			}
		} else {
			targetClass = Option.some(new ServiceClass(c.getSimpleName(), prefix));
		}

		return targetClass.map(serviceClass -> analyzeMethods(c, serviceClass, stringModelClasses, queryClasses));
	}

	private Tuple2<ServiceClass, Array<IFileable>> analyzeMethods(
			Class<?> serviceInterface,
			ServiceClass serviceClass,
			Map<String, ModelClass> modelClasses,
			Map<String, QueryClass> queryClasses) {
		Array<IFileable> referencedTypes = serviceClass.getReferencedTypes();

		final String basePath = serviceClass.getResourceUrl();

		final var stringTypescriptMappings = TYPESCRIPT_MAPPINGS.mapKeys(Class::getName);

		Method[] declaredMethods = serviceInterface.getDeclaredMethods();
		methods:
		for (Method method : declaredMethods) {
			Option<String> httpMethod = determineHttpMethod(method);

			if (httpMethod.isEmpty()) {
				continue;
			}

			if (method.isBridge()) {
				continue;
			}

			if (isMethodProvidedByDefault(serviceClass, method)) {
				continue;
			}

			Option<Tuple2<String, Option<IFileable>>> returnType = Option.none();

			Class<?> returnTypeClass = method.getReturnType();
			if (Response.class == returnTypeClass) {

				if (method.isAnnotationPresent(Returns.class)) {
					Returns returns = method.getAnnotation(Returns.class);

					returnType = determineType(returns.value(), modelClasses, queryClasses, stringTypescriptMappings);
				} else if (method.isAnnotationPresent(ReturnsListOf.class)) {
					ReturnsListOf returns = method.getAnnotation(ReturnsListOf.class);

					returnType = determineType(returns.value(), modelClasses, queryClasses, stringTypescriptMappings)
							.map(t -> t.map1(rt -> rt + "[]"));
				} else if (method.isAnnotationPresent(ReturnsSetOf.class)) {
					ReturnsSetOf returns = method.getAnnotation(ReturnsSetOf.class);

					returnType = determineType(returns.value(), modelClasses, queryClasses, stringTypescriptMappings)
							.map(t -> t.map1(rt -> rt + "[]"));
				} else if (method.isAnnotationPresent(NoContent.class)) {
					returnType = Option.some(new Tuple2<>("void", Option.none()));
				}
			} else {
				if (java.util.List.class.isAssignableFrom(returnTypeClass)) {
					TypeVariable<? extends Class<?>> typeParameter = returnTypeClass.getTypeParameters()[0];

					returnType = determineType(typeParameter.getGenericDeclaration(), modelClasses, queryClasses,
							stringTypescriptMappings)
							.map(t -> t.map1(rt -> rt + "[]"));
				} else if (java.util.Set.class.isAssignableFrom(returnTypeClass)) {
					TypeVariable<? extends Class<?>> typeParameter = returnTypeClass.getTypeParameters()[0];

					returnType = determineType(typeParameter.getGenericDeclaration(), modelClasses, queryClasses,
							stringTypescriptMappings)
							.map(t -> t.map1(rt -> rt + "[]"));
				} else {
					returnType = determineType(returnTypeClass, modelClasses, queryClasses, stringTypescriptMappings);
				}
			}

			if (returnType.isEmpty()) {
				continue;
			}

			Path methodPathAnnotation = method.getAnnotation(Path.class);

			String path = methodPathAnnotation != null ? basePath + methodPathAnnotation.value() : basePath;

			MethodDescriptor methodDescriptor = new MethodDescriptor(httpMethod.get(), path, method.getName(),
					returnType
							.get()._1());

			for (java.lang.reflect.Parameter parameter : method.getParameters()) {
				Option<Tuple2<String, Option<IFileable>>> parameterType = determineType(parameter.getType(),
						modelClasses, queryClasses, stringTypescriptMappings);

				if (parameterType.isEmpty()) {
					continue methods;
				}

				if (parameter.isAnnotationPresent(HeaderParam.class)) {
					HeaderParam param = parameter.getAnnotation(HeaderParam.class);


					methodDescriptor.addHeaderParameter(parameterType
							.get()
							._1(), param.value(), getName(parameter, param
							.value()));
				} else if (parameter.isAnnotationPresent(PathParam.class)) {
					PathParam param = parameter.getAnnotation(PathParam.class);
					methodDescriptor.addPathParameter(parameterType
							.get()
							._1(), param.value(), getName(parameter, param
							.value()));
				} else if (parameter.isAnnotationPresent(QueryParam.class)) {
					QueryParam param = parameter.getAnnotation(QueryParam.class);
					methodDescriptor.addQueryParameter(parameterType
									.get()
									._1(), param.value(), getName(parameter, param.value()),
							isAnnotationPresent(parameter, "javax.annotation.Nonnull") || parameter.isAnnotationPresent(
									NotNull.class));
				} else if (parameter.isAnnotationPresent(BeanParam.class) || parameter.isAnnotationPresent(
						MatrixParam.class)) {
					continue methods;
				} else {
					methodDescriptor.addBodyParameter(parameterType
							.get()
							._1(), getName(parameter, "payload"));
				}

				referencedTypes = referencedTypes.appendAll(parameterType.get()._2());

			}

			referencedTypes = referencedTypes.appendAll(returnType.get()._2());


			serviceClass.addMethod(methodDescriptor);
		}

		return new Tuple2<>(serviceClass, referencedTypes);
	}

	private boolean isAnnotationPresent(@NotNull java.lang.reflect.Parameter parameter, @NotNull String fqdn) {
		return Arrays.stream(parameter.getAnnotations()).anyMatch(a -> a.annotationType().getName().equals(fqdn));
	}

	private boolean isMethodProvidedByDefault(ServiceClass serviceClass, Method method) {
		boolean mutable = serviceClass instanceof MutableResourceClass || serviceClass instanceof ListableMutableResourceClass;
		boolean listable = serviceClass instanceof ListableResourceClass || serviceClass instanceof ListableMutableResourceClass;

		Class<?> declaringClass = method.getDeclaringClass();

		switch (method.getName()) {
			case "get":
			case "delete":
				if (mutable && method.getParameterCount() == 1) {
					Option<Tuple2<Type, Type>> resourceAndIdentifier = findResourceAndIdentifier(declaringClass);

					if (resourceAndIdentifier.isDefined()) {
						Tuple2<Type, Type> types = resourceAndIdentifier.get();
						Type identifier = types._2();

						if (identifier.equals(method.getParameters()[0].getType())) {
							return true;
						}
					}
				}
				break;
			case "create":
				if (mutable && method.getParameterCount() == 1) {
					Option<Tuple2<Type, Type>> resourceAndIdentifier = findResourceAndIdentifier(declaringClass);

					if (resourceAndIdentifier.isDefined()) {
						Tuple2<Type, Type> types = resourceAndIdentifier.get();
						Type resource = types._1();

						if (resource.equals(method.getParameters()[0].getType())) {
							return true;
						}
					}
				}
				break;
			case "update":
				if (mutable && method.getParameterCount() == 2) {
					Option<Tuple2<Type, Type>> resourceAndIdentifier = findResourceAndIdentifier(declaringClass);

					if (resourceAndIdentifier.isDefined()) {
						Tuple2<Type, Type> types = resourceAndIdentifier.get();
						Type resource = types._1();
						Type identifier = types._2();

						if (identifier.equals(method.getParameters()[0].getType()) && resource.equals(
								method.getParameters()[1]
										.getType())) {
							return true;
						} else {
							getLog().warn(
									"Update can't be processed: " + identifier + " != " + method.getParameters()[0].getType() + " || " + resource + " != " + method.getParameters()[1].getType());
						}
					} else {
						getLog().warn("\"Update can't be processed: no resource and identifier determined");
					}
				}
				break;
			case "list":
				if (listable && method.getParameterCount() == 3) {
					if (QueryObject.class.isAssignableFrom(method.getParameters()[0].getType())
							&& Long.class.isAssignableFrom(method.getParameters()[1].getType())
							&& Long.class.isAssignableFrom(method.getParameters()[2].getType())
							&& Response.class.isAssignableFrom(method.getReturnType())
					) {
						return true;
					}
				}
				break;
			case "count":
				if (listable && method.getParameterCount() == 1) {
					if (QueryObject.class.isAssignableFrom(method.getParameters()[0].getType()) && Response.class
							.isAssignableFrom(method.getReturnType())
					) {
						return true;
					}
				}
				break;
		}

		return false;
	}

	private Option<Tuple2<Type, Type>> findResourceAndIdentifier(Class<?> declaringClass) {
		for (Type genericInterface : declaringClass.getGenericInterfaces()) {
			if (genericInterface instanceof ParameterizedType) {
				ParameterizedType parameterizedType = (ParameterizedType) genericInterface;

				if (parameterizedType.getRawType() == MutableResource.class) {
					Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();

					return Option.some(new Tuple2<>(actualTypeArguments[0], actualTypeArguments[1]));
				}
			}
		}

		return Option.none();
	}

	private String getName(java.lang.reflect.Parameter parameter, String alternativeValue) {
		if (parameter.isNamePresent()) {
			return parameter.getName();
		}

		return alternativeValue;
	}

	private Option<Tuple2<String, Option<IFileable>>> determineType(
			Class<?> returns, Map<String, ModelClass> modelClasses, Map<String, QueryClass> queryClasses,
			Map<String, String> stringTypescriptMappings) {
		return Option.some(returns)
				.map(Class::getName)
				.flatMap(className -> modelClasses
						.get(className)
						.map(mc -> new Tuple2<>(mc.getName(), Option.<IFileable>some(mc)))
						.orElse(() -> queryClasses
								.get(className)
								.map(qc -> new Tuple2<>(qc.getName(), Option.some(qc))))
						.orElse(() -> stringTypescriptMappings
								.get(className)
								.map(t -> new Tuple2<>(t, Option.none())))
				);
	}

	private Option<String> determineHttpMethod(Method method) {
		if (method.isAnnotationPresent(GET.class)) {
			return Option.some("get");
		}
		if (method.isAnnotationPresent(POST.class)) {
			return Option.some("post");
		}
		if (method.isAnnotationPresent(PUT.class)) {
			return Option.some("put");
		}
		if (method.isAnnotationPresent(DELETE.class)) {
			return Option.some("delete");
		}
		if (method.isAnnotationPresent(PATCH.class)) {
			return Option.some("patch");
		}
		if (method.isAnnotationPresent(OPTIONS.class)) {
			return Option.some("options");
		}
		if (method.isAnnotationPresent(HEAD.class)) {
			return Option.some("head");
		}

		return Option.none();
	}


	private String removeExtension(String filename) {
		if (filename.endsWith(".ts")) {
			return filename.substring(0, filename.length() - 3);
		}

		return filename;
	}
}
