package com.jeroensteenbeeke.hyperion.angulargen.data;

import io.vavr.collection.Array;
import io.vavr.collection.HashSet;
import io.vavr.collection.Set;

import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Describes a method
 */
public class MethodDescriptor {
	private static final Set<String> METHODS_WITH_BODY = HashSet.of("post", "put", "patch");

	private final String httpMethod;

	private final String path;

	private final String name;

	private final String returnType;

	private final List<ParameterDescriptor> allParameters = new ArrayList<>();

	private final List<ParameterDescriptor> pathParameters = new ArrayList<>();

	private final List<ParameterDescriptor> queryParameters = new ArrayList<>();

	private final List<ParameterDescriptor> headerParameters = new ArrayList<>();

	private final List<ParameterDescriptor> bodyParameters = new ArrayList<>();

	/**
	 * Constructor
	 * @param httpMethod The http method: GET, POST, PUT, DELETE, OPTIONS, HEADER, PATCH
	 * @param path The path of the call
	 * @param name The name of the method
	 * @param returnType The method's return type
	 */
	public MethodDescriptor(String httpMethod, String path, String name, String returnType) {
		this.httpMethod = httpMethod;
		this.path = path;
		this.name = name;
		this.returnType = returnType;
	}

	/**
	 * Adds a path parameter
	 * @param type The type of parameter
	 * @param pathElement The element of the path where this parameter is located
	 * @param name The name of the parameter
	 * @return The current method
	 */
	public MethodDescriptor addPathParameter(@NotNull String type, @NotNull String pathElement, @NotNull String name) {
		ParameterDescriptor descriptor = new ParameterDescriptor(type, pathElement, name, true);
		allParameters.add(descriptor);
		pathParameters.add(descriptor);
		return this;
	}

	/**
	 * Adds a header parameter
	 * @param type The type parameter
	 * @param headerName The name of the header
	 * @param name The name of the parameter
	 * @return The current method
	 */
	public MethodDescriptor addHeaderParameter(@NotNull String type, @NotNull String headerName, @NotNull String name) {
		ParameterDescriptor descriptor = new ParameterDescriptor(type, headerName, name, true);
		allParameters.add(descriptor);
		headerParameters.add(descriptor);
		return this;
	}

	/**
	 * Adds a query parameter
	 * @param type The type of parameter
	 * @param apiName The name of the parameter in the query
	 * @param name The name of the parameter in the code
	 * @param required Whether or not the parameter is required
	 * @return The current method
	 */
	public MethodDescriptor addQueryParameter(@NotNull String type, @NotNull String apiName, @NotNull String name, boolean required) {
		ParameterDescriptor descriptor = new ParameterDescriptor(type, apiName, name, required);
		allParameters.add(descriptor);
		queryParameters.add(descriptor);
		return this;
	}

	/**
	 * Adds a body parameter, if the current HTTP method supports it, and no body parameter has been declared yet
	 * @param type The type of parameter
	 * @param name The name of the parameter
	 * @return The current method
	 */
	public MethodDescriptor addBodyParameter(@NotNull String type, @NotNull String name) {
		if (METHODS_WITH_BODY.contains(httpMethod) && bodyParameters.isEmpty()) {
			ParameterDescriptor descriptor = new ParameterDescriptor(type, name, name, true);
			allParameters.add(descriptor);
			bodyParameters.add(descriptor);
		}
		return this;
	}

	/**
	 * @return The list of path parameters
	 */
	public List<ParameterDescriptor> getPathParameters() {
		return pathParameters;
	}

	/**
	 * @return The list of body parameters
	 */
	public List<ParameterDescriptor> getBodyParameters() {
		return bodyParameters;
	}

	/**
	 * @return The list of header parameters
	 */
	public List<ParameterDescriptor> getHeaderParameters() {
		return headerParameters;
	}

	/**
	 * @return The list of query parameters
	 */
	public List<ParameterDescriptor> getQueryParameters() {
		return queryParameters;
	}

	/**
	 * Converts the method to a Typescript representation
	 * @return The source
	 */
	public String getTypescript() {
		StringBuilder ts = new StringBuilder();
		ts.append("\t").append(name).append("(");

		int pc = 0;
		for (var param : allParameters) {
			if (pc++ > 0) {
				ts.append(", ");
			}
			ts.append(param.getParamName());
			if (!param.isRequired()) {
				ts.append("?");
			}

			ts.append(": ").append(param.getType());
		}

		ts.append("): Observable<").append(returnType).append("> {\n");

		String pathExpression = "'" +
				Array.ofAll(pathParameters)
					 .foldLeft(path, (inbetweenPath, param) -> inbetweenPath.replaceFirst(String.format("\\{%s(:.*)?\\}", param
							 .getApiName()), String
																							 .format("' + %s + '", param
																									 .getParamName()))) + "'";

		if (pathExpression.endsWith(" + ''")) {
			pathExpression = pathExpression.substring(0, pathExpression.length()-5);
		}

		if (!queryParameters.isEmpty()) {
			ts.append("\t\tconst queryParams = ");
			ts.append(Array.ofAll(queryParameters)
						   .map(qp -> String.format("{ value: %s, uriName: '%s', required: %b}", qp
								   .getParamName(), qp.getApiName(), qp.isRequired()))
						   .mkString("[", ",\n\t\t\t\t", "]\n\t\t\t"));
			ts.append(".filter(param => param.required || param.value !== null)\n\t\t\t");
			ts.append(".map(param => param.uriName + '=' + param.value)\n\t\t\t");
			ts.append(".join('&');\n");
		}


		ts
				.append("\t\tconst path = super.appendQueryString(")
				.append(pathExpression);
		if (!queryParameters.isEmpty()) {
			ts.append(", queryParams);\n\n");
		} else {
			ts.append(", '');\n\n");
		}

		ts.append("\t\treturn super.http.");
		ts.append(httpMethod);
		ts.append(" <")
		  .append(returnType)
		  .append(">(path, ");

		if (!bodyParameters.isEmpty()) {
			ts.append(bodyParameters.iterator().next().getParamName());
			ts.append(", ");
		}

		ts.append("{");

		if (!headerParameters.isEmpty()) {
			ts.append("\t\t\theaders: {\n");
			ts.append("\t\t\t}");

			ts.append(Array
					.ofAll(headerParameters)
					.map(hp -> String.format("'%s': %s", hp.getApiName(), hp.getParamName()))
					.mkString("\t\t\t\t", ",\n\t\t\t\t", ""));
			ts.append("\n");
		}

		ts.append("})\n");
		ts.append("\t\t\t.pipe(\n");
		ts.append("\t\t\t\tcatchError(super.handleError('").append(name).append("', ");
		ts.append(determineDefaultReturnType(returnType));
		ts.append("))\n");

		ts.append("\t\t\t);\n");
		ts.append("\t}");

		return ts.toString();
	}

	@NotNull
	private String determineDefaultReturnType(@NotNull String returnType) {
		if (returnType.endsWith("[]")) {
			return "[]";
		} else {
			switch (returnType) {
				case "string":
					return "''";
				case "number":
					return "0";
				case "boolean":
					return "false";
				case "Date":
					return "Date.now()";
				default:
					return "{}";
			}
		}

	}
}
