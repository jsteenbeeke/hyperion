package com.jeroensteenbeeke.hyperion.angulargen.data;

import io.vavr.collection.Array;

import org.jetbrains.annotations.NotNull;
import java.util.Objects;

/**
 * A class that implements MutableResource
 */
public class MutableResourceClass extends ServiceClass {

	private final ModelClass modelType;

	private final String idType;

	/**
	 * Constructor
	 * @param serviceName The name of the service
	 * @param resourceUrl The resource URL
	 * @param modelType The type of model
	 * @param idType The type of ID
	 */
	public MutableResourceClass(@NotNull String serviceName,
								@NotNull String resourceUrl,
								@NotNull ModelClass modelType,
								@NotNull String idType) {
		super(serviceName, resourceUrl);

		this.modelType = modelType;
		this.idType = idType;
	}

	/**
	 * @return The model type
	 */
	@NotNull
	public ModelClass getModelType() {
		return modelType;
	}

	/**
	 * @return The id type
	 */
	@NotNull
	public String getIdType() {
		return idType;
	}

	@Override
	public Array<IFileable> getReferencedTypes() {
		return Array.of(getModelType());
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		MutableResourceClass that = (MutableResourceClass) o;
		return modelType.equals(that.modelType) && idType.equals(that.idType);
	}

	@Override
	public int hashCode() {
		return Objects.hash(modelType, idType);
	}

	@Override
	public String getTemplate() {
		return "mutable-resource.ftl";
	}
}
