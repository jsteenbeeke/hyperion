package com.jeroensteenbeeke.hyperion.angulargen.data;

import org.jetbrains.annotations.NotNull;

/**
 * Interface representing something that can be stored as a file
 */
public interface IFileable {
	/**
	 * @return The filename
	 */
	@NotNull
	String getFilename();

	/**
	 * @return The name of the represented object
	 */
	@NotNull
	String getObjectName();

	/**
	 * Modifies the given name to a TypeScript filename
	 * @param name The name
	 * @return The filename
	 */
	@NotNull
	default String toTypescriptFile(@NotNull String name) {
		StringBuilder result = new StringBuilder();
		for (char c: name.toCharArray()) {
			if (result.length() > 0 && Character.isUpperCase(c)) {
				result.append("-");
			}
			result.append(Character.toLowerCase(c));

		}

		result.append(".ts");

		return result.toString();
	}
}
