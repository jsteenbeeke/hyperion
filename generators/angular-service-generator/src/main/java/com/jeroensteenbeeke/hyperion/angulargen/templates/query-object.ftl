import { QueryObject, Property<#list propertyTypes as propertyType>,
    ${propertyType}</#list> } from '@jeroensteenbeeke/hyperion-queryobjects';

export class ${name} extends QueryObject {
<#list fields as property>
    private $${property.name}: ${property.type}<${name}> = new ${property.type}<${name}>(this, '${property.name}');

</#list>
<#list fields as property>
    ${property.name}(): ${property.type}<${name}> {
        return this.$${property.name};
    }

</#list>
    getProperties(): Property<any>[] {
        return [
<#list fields as property>
            this.$${property.name}<#sep>,</#sep>
</#list>
        ];
    }
};
