package com.jeroensteenbeeke.hyperion.angulargen.data;

import io.vavr.collection.Array;
import io.vavr.collection.HashSet;
import io.vavr.control.Option;
import io.vavr.control.Try;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.util.function.Predicate;

/**
 * Representation of a target folder in code generation
 */
public class Folder {
	private final String name;

	private Folder parent;

	private HashSet<Folder> subFolders;

	private HashSet<IFileable> files;

	/**
	 * Constructor
	 *
	 * @param name The folder name
	 */
	public Folder(@NotNull String name) {
		this(name, null, HashSet.empty(), HashSet.empty());
	}

	private Folder(@NotNull String name, @Nullable Folder parent, @NotNull HashSet<Folder> subFolders, @NotNull HashSet<IFileable> files) {
		this.name = name;
		this.parent = parent;
		this.subFolders = subFolders;
		this.files = files;
	}


	/**
	 * Adds the file to the folder
	 *
	 * @param file The file to add
	 * @return The file
	 */
	@NotNull
	public IFileable addFile(@NotNull IFileable file) {
		this.files = files.add(file);
		return file;
	}

	/**
	 * Adds a subfolder to the current folder
	 *
	 * @param name The name of the folder to add
	 * @return The new subfolder
	 */
	@NotNull
	public Folder addSubFolder(@NotNull String name) {
		Folder subFolder = new Folder(name).withParent(this);
		subFolders = subFolders.add(subFolder);
		return subFolder;
	}

	/**
	 * Creates a copy of this folder, with the given folder as parent
	 *
	 * @param parent The parent to set
	 * @return The current folder
	 */
	@NotNull
	public Folder withParent(@NotNull Folder parent) {
		this.parent = parent;
		return this;
	}

	/**
	 * Checks if this folder has a file matching the given predicate
	 *
	 * @param matcher   The predicate
	 * @return {@code true} if the file exists, {@code false} otherwise
	 */
	public boolean fileExistsInHierarchy(@NotNull Predicate<IFileable> matcher) {
		return files.exists(matcher) || subFolders.exists(sf -> sf.fileExistsInHierarchy(matcher));
	}

	/**
	 * Gets the relative position of the given file to the current folder
	 * @param target The file to find
	 * @return The relative path
	 */
	public Try<String> getRelativePosition(@NotNull IFileable target) {
		if (files.contains(target)) {
			return Try.success(target.getFilename());
		} else {
			for (Folder subFolder : subFolders) {
				if (subFolder.fileExistsInHierarchy(target::equals)) {
					return subFolder.getRelativePosition(target).map(relativePath -> subFolder.getName() + "/" + relativePath);
				}
			}

			if (parent != null) {
				return parent.getRelativePosition(target).map(relativePath -> "../" + relativePath);
			}

		}

		return Try.failure(new IllegalStateException("Cannot locate target"));
	}

	/**
	 * Gets the name of the current folder
	 * @return The name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get a subfolder with the given name
	 * @param name The name of the subfolder
	 * @return Optionally the folder
	 */
	public Option<Folder> getFolder(@NotNull String name) {
		return subFolders.find(f -> f.getName().equals(name));
	}

	/**
	 * Creates a root folder representation
	 * @return The folder
	 */
	public static Folder root() {
		return new Folder("");
	}

	/**
	 * Returns an array of all paths contained in the current and any subfolders
	 * @return An Array of paths
	 */
	public Array<String> allPaths() {
		return files.map(IFileable::getFilename).toArray()
				.appendAll(subFolders.flatMap(f -> f.allPaths().map(p -> f.getName() + "/" + p)));
	}
}
