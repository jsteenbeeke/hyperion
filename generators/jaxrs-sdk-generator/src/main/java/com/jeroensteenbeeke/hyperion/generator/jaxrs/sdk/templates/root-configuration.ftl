package ${packageName};

import org.springframework.context.annotation.*;

import jakarta.annotation.Generated;

@Configuration
@Import({
<#list configs as config>
    ${config}.class<#sep >,
</#list>
})
@Generated(value = "com.jeroensteenbeeke.hyperion.generator.jaxrs.sdk.GeneratorMojo",
            date = "${generationTime}")
public class ${sdkName}SDKConfiguration {

}
