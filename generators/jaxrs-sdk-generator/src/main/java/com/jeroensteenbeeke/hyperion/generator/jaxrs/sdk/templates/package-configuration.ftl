package ${packageName};

import com.jeroensteenbeeke.hyperion.solstice.spring.resteasy.ConditionalProxyProvider;
import com.jeroensteenbeeke.hyperion.solstice.spring.resteasy.HyperionResteasyContext;

import org.springframework.context.annotation.*;

import org.springframework.beans.factory.annotation.Autowired;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;

import jakarta.annotation.Generated;

@Configuration
@Generated(value = "com.jeroensteenbeeke.hyperion.generator.jaxrs.sdk.GeneratorMojo",
            date = "${generationTime}")
public class ${configurationName} implements ConditionalProxyProvider {
    @Autowired
    HyperionResteasyContext context;

    @Override
    public HyperionResteasyContext getResteasyContext() {
        return context;
    }
<#list classes as class>

    @Bean
    @Lazy
    public ${class} ${class?uncap_first}() {
        return proxyOf(${class}.class);
    }
</#list>
}
