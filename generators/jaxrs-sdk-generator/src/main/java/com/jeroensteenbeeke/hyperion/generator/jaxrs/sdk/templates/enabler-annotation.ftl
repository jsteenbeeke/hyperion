package ${packageName};

import jakarta.annotation.Generated;

import java.lang.annotation.*;

import org.springframework.context.annotation.Import;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(${sdkName}SDKConfiguration.class)
@Generated(value = "com.jeroensteenbeeke.hyperion.generator.jaxrs.sdk.GeneratorMojo",
            date = "${generationTime}")
public @interface Enable${sdkName}SDK {

}
