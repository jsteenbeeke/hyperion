package com.jeroensteenbeeke.hyperion.generator.jaxrs.sdk;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import io.vavr.Tuple2;
import io.vavr.collection.Array;
import io.vavr.collection.HashMap;
import io.vavr.collection.HashSet;
import io.vavr.collection.Set;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.*;
import org.apache.maven.project.MavenProject;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.sonatype.plexus.build.incremental.BuildContext;

import javax.inject.Inject;
import jakarta.ws.rs.Path;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Optional;

/**
 * Code generator for Spring config files for a JAX-RS based webservice backend
 */
@Mojo(name = "generate", defaultPhase = LifecyclePhase.GENERATE_SOURCES,
		requiresDependencyResolution = ResolutionScope.COMPILE)
public class GeneratorMojo extends AbstractMojo {
	@Parameter(required = true)
	public String basePackage;

	@Parameter(required = true)
	public String sdkName;

	@Parameter(required = false, defaultValue = "generated-sources")
	public String generatedSourcesDir;

	@Parameter(required = false, defaultValue = "sdk")
	public String generatedSourcesSubdir;

	/**
	 * @component
	 */
	@Component
	public BuildContext buildContext;

	@Parameter(required = true, property = "project")
	@Inject
	public MavenProject project;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		generatedSourcesDir = Optional.ofNullable(generatedSourcesDir).orElse("generated-sources");
		generatedSourcesSubdir = Optional.ofNullable(generatedSourcesSubdir).orElse("sdk");
		basePackage = Optional
				.ofNullable(basePackage)
				.orElseThrow(() -> new MojoFailureException("Undefined or invalid base package"));
		sdkName = Optional
				.ofNullable(sdkName)
				.filter(this::isValidJavaIdentifier)
				.orElseThrow(() -> new MojoFailureException("Invalid or missing parameter sdkName, must be a valid Java identifier"));
		buildContext = Optional
				.ofNullable(buildContext)
				.orElseThrow(() -> new MojoFailureException("BuildContext unavailable"));
		project = Optional
				.ofNullable(project)
				.orElseThrow(() -> new MojoFailureException("MavenProject unavailable"));

		Reflections ref = new Reflections(new ConfigurationBuilder()
												  .setUrls(ClasspathHelper.forPackage(basePackage))
												  .setScanners(new SubTypesScanner(), new TypeAnnotationsScanner())
		);

		Set<Class<?>> pathClasses = HashSet.ofAll(ref.getTypesAnnotatedWith(Path.class));
		HashMap<String, Array<Class<?>>> classesByPackage = pathClasses.foldLeft(HashMap.empty(),
																				 (m, c) -> m.put(c.getPackageName(),
																								 Array.of(c),
																								 Array::appendAll));

		File basedir = project != null && project.getBasedir() != null ? project.getBasedir() :
				new File(System.getProperty("user.dir"));
		File target = create(new File(basedir, "target"));
		File generatedSources = create(new File(target, generatedSourcesDir));
		File sdk = create(new File(generatedSources, generatedSourcesSubdir));
		File basePackageDir = createPackage(sdk, basePackage);

		Configuration configuration = new Configuration(Configuration.VERSION_2_3_28);
		configuration.setClassForTemplateLoading(GeneratorMojo.class, "templates");
		configuration.setDefaultEncoding("UTF-8");

		try (FileWriter fw = new FileWriter(new File(basePackageDir, sdkName + "SDKConfiguration.java"))) {
			Map<String, Object> model = new java.util.HashMap<>();
			model.put("packageName", basePackage);
			model.put("sdkName", sdkName);
			model.put("generationTime", DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(LocalDateTime.now()));
			model.put("configs", classesByPackage.keySet()
												 .map(this::createPackageConfigFQDN)
												 .map(identifier -> identifier + "Configuration")
												 .toArray()
												 .sorted()
												 .toJavaList());

			Template template = configuration.getTemplate("root-configuration.ftl");
			template.process(model, fw);
			fw.flush();
		} catch (IOException | TemplateException e) {
			throw new MojoExecutionException(e.getMessage(), e);
		}

		try (FileWriter fw = new FileWriter(new File(basePackageDir, "Enable" + sdkName + "SDK.java"))) {
			Map<String, Object> model = new java.util.HashMap<>();
			model.put("packageName", basePackage);
			model.put("sdkName", sdkName);
			model.put("generationTime", DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(LocalDateTime.now()));

			Template template = configuration.getTemplate("enabler-annotation.ftl");
			template.process(model, fw);
			fw.flush();
		} catch (IOException | TemplateException e) {
			throw new MojoExecutionException(e.getMessage(), e);
		}

		for (Tuple2<String, Array<Class<?>>> packageAndClasses : classesByPackage) {
			var packageName = packageAndClasses._1;
			var classes = packageAndClasses._2;
			String relativePackagePath = packageName.equals(basePackage) ? getLastSegment(basePackage) : packageName.substring(basePackage.length() + 1);
			var identifier = packageNameToIdentifier(relativePackagePath);

			File relativePackage = packageName.equals(basePackage) ? basePackageDir : createPackage(basePackageDir, relativePackagePath);

			try (FileWriter fw = new FileWriter(new File(relativePackage, identifier + "Configuration.java"))) {
				Map<String, Object> model = new java.util.HashMap<>();
				model.put("packageName", packageName);
				model.put("configurationName", identifier + "Configuration");
				model.put("generationTime", DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(LocalDateTime.now()));
				model.put("classes", classes.map(Class::getSimpleName).toJavaList());

				Template template = configuration.getTemplate("package-configuration.ftl");
				template.process(model, fw);
				fw.flush();
			} catch (IOException | TemplateException e) {
				throw new MojoExecutionException(e.getMessage(), e);
			}
		}

		if (buildContext != null) {
			buildContext.refresh(target);
		}
		if (project != null) {
			project.addCompileSourceRoot(
					String.format("%s/%s/%s", target.getName(), generatedSourcesDir, generatedSourcesSubdir));
		}
	}

	static String packageNameToIdentifier(String s) {
		StringBuilder result = new StringBuilder();

		boolean first = true;

		for (char c : s.toCharArray()) {
			if (!Character.isJavaIdentifierStart(c)) {
				first = true;
				continue;
			}

			if (first) {
				result.append(Character.toUpperCase(c));
				first = false;
			} else {
				result.append(c);
			}
		}


		return result.toString();
	}

	private boolean isValidJavaIdentifier(String string) {
		if (!string.isBlank()) {
			char first = string.charAt(0);

			if (Character.isJavaIdentifierStart(first)) {
				for (int i = 1; i < string.length(); i++) {
					char n = string.charAt(i);

					if (!Character.isJavaIdentifierPart(n)) {
						return false;
					}
				}
				return true;
			}
		}

		return false;
	}

	private File createPackage(File parent, String packageName)
			throws MojoExecutionException {
		File base = parent;

		String[] packages = packageName.split("\\.");

		for (String pkg : packages) {
			base = create(new File(base, pkg));
		}

		return base;
	}

	private File create(File file) throws MojoExecutionException {
		if (file.exists() && !file.isDirectory()) {
			throw new MojoExecutionException(String
													 .format("File %s is not a directory", file.getName()));
		}

		if (!file.exists() && !file.mkdir()) {
			throw new MojoExecutionException(String
													 .format("Could not create directory %s", file.getName()));
		}

		return file;
	}

	private String createPackageConfigFQDN(String packageName) {
		if (packageName.equals(basePackage)) {
			String lastSegment = getLastSegment(packageName);

			return packageName + "." + packageNameToIdentifier(lastSegment);
		} else {
			return packageName + "." + packageNameToIdentifier(packageName.substring(basePackage.length() + 1));
		}
	}

	private String getLastSegment(String packageName) {
		int idx = packageName.lastIndexOf(".");
		return idx != -1 ? packageName.substring(idx + 1) : packageName;
	}
}
