package com.jeroensteenbeeke.hyperion.generator.jaxrs.sdk.testdata;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

@Path("/test")
public interface TestResource {
	@GET
	Response getPayload();
}
