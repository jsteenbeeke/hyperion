package com.jeroensteenbeeke.hyperion.generator.jaxrs.sdk.testdata.sub;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

@Path("/sub")
public interface SubResource {
	@GET
	Response whatSub();
}
