package com.jeroensteenbeeke.hyperion.generator.jaxrs.sdk;

import com.github.codeteapot.maven.plugin.testing.MavenPluginContext;
import com.github.codeteapot.maven.plugin.testing.MavenPluginExecutionException;
import com.github.codeteapot.maven.plugin.testing.junit.jupiter.MavenPluginExtension;
import com.jeroensteenbeeke.andalite.java.analyzer.*;
import com.jeroensteenbeeke.andalite.java.analyzer.annotation.ArrayValue;
import com.jeroensteenbeeke.andalite.java.analyzer.annotation.ClassValue;
import com.jeroensteenbeeke.hyperion.maven.test.HyperionMavenConfiguration;
import com.jeroensteenbeeke.lux.TypedResult;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

import static com.jeroensteenbeeke.andalite.core.ResultMatchers.isOk;
import static com.jeroensteenbeeke.hyperion.maven.test.MavenFileUtil.getTestFile;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MavenPluginExtension.class)
public class GeneratorMojoTest {


	public static final String BASE_PACKAGE = "com.jeroensteenbeeke.hyperion.generator.jaxrs.sdk.testdata";

	@Test
	public void testPackageNameConversion() {
		assertThat(GeneratorMojo.packageNameToIdentifier("jaxrs.sdk"), equalTo("JaxrsSdk"));
		assertThat(GeneratorMojo.packageNameToIdentifier("foo"), equalTo("Foo"));
		assertThat(GeneratorMojo.packageNameToIdentifier("foo.bar.baz"), equalTo("FooBarBaz"));
		assertThat(GeneratorMojo.packageNameToIdentifier(""), equalTo(""));
	}

	@Test
	public void testMojoExecution(MavenPluginContext context) throws MavenPluginExecutionException, IOException {

		File pom = getTestFile("src/test/resources/testproject/pom.xml");

		File testRoot = pom.getParentFile();

		java.nio.file.Path targetPackage = testRoot
				.toPath()
				.resolve("target")
				.resolve("generated-sources")
				.resolve("sdk")
				.resolve(BASE_PACKAGE.replace('.', '/'));

		var enablerAnnotation = targetPackage.resolve("EnableExampleSDK.java");
		var rootConfig = targetPackage.resolve("ExampleSDKConfiguration.java");
		var basePackageConfiguration = targetPackage.resolve("TestdataConfiguration.java");
		var subPackageConfiguration = targetPackage.resolve("sub/SubConfiguration.java");

		Files.deleteIfExists(enablerAnnotation);
		Files.deleteIfExists(rootConfig);
		Files.deleteIfExists(basePackageConfiguration);
		Files.deleteIfExists(subPackageConfiguration);

		context.setBaseDir(testRoot);
		HyperionMavenConfiguration.configure(context, testRoot);
		context.goal("generate").set(MavenPluginContext.configuration()
													   .set(MavenPluginContext.configurationValue("basePackage", BASE_PACKAGE))
													   .set(MavenPluginContext.configurationValue("sdkName", "Example"))
		).execute();

		assertTrue(Files.exists(enablerAnnotation));
		assertTrue(Files.exists(rootConfig));
		assertTrue(Files.exists(basePackageConfiguration));
		assertTrue(Files.exists(subPackageConfiguration));

		verifyRootConfig(rootConfig);

		verifyEnablerAnnotation(enablerAnnotation);
	}

	private void verifyRootConfig(Path path) {
		TypedResult<AnalyzedSourceFile> rootConfigResult = new ClassAnalyzer(path.toFile()).analyze();
		assertThat(rootConfigResult, isOk());

		AnalyzedSourceFile rootConfigFile = rootConfigResult.getObject();
		assertThat(rootConfigFile.getClasses().size(), equalTo(1));
		AnalyzedClass rootConfig = rootConfigFile.getClasses().get(0);

		assertThat(rootConfig.getClassName(), equalTo("ExampleSDKConfiguration"));

		assertThat(rootConfig.getAnnotations().size(), equalTo(3));
		assertTrue(rootConfig
						   .getAnnotations()
						   .stream()
						   .map(AnalyzedAnnotation::getType)
						   .anyMatch("Import"::equals));
		assertTrue(rootConfig
						   .getAnnotations()
						   .stream()
						   .map(AnalyzedAnnotation::getType)
						   .anyMatch("Configuration"::equals));
		assertTrue(rootConfig
						   .getAnnotations()
						   .stream()
						   .map(AnalyzedAnnotation::getType)
						   .anyMatch("Generated"::equals));

		assertThat("Root config should have 0 fields", rootConfig.getFields().size(), equalTo(0));
		assertThat("Root config should have 0 methods", rootConfig.getMethods().size(), equalTo(0));

		AnalyzedAnnotation importAnnotation = rootConfig.getAnnotation("Import");
		assertThat("There should be an @Import annotation", importAnnotation, not(nullValue()));
		ArrayValue importedConfigurations = importAnnotation.getValue(ArrayValue.class, "value");
		assertThat("There should be a value in the @Import annotation", importedConfigurations, not(nullValue()));

		List<ClassValue> classValues = importedConfigurations.getValue().stream()
														 .filter(v -> v instanceof ClassValue)
														 .map(v -> (ClassValue) v)
														 .collect(Collectors.toList());
		assertThat(classValues.size(), equalTo(2));
		assertThat(classValues.get(0).getValue(), equalTo(BASE_PACKAGE + ".TestdataConfiguration"));
		assertThat(classValues.get(1).getValue(), equalTo(BASE_PACKAGE + ".sub.SubConfiguration"));
	}

	private void verifyEnablerAnnotation(Path path) {
		TypedResult<AnalyzedSourceFile> enablerAnnotationResult = new ClassAnalyzer(path.toFile()).analyze();
		assertThat(enablerAnnotationResult, isOk());

		AnalyzedSourceFile enablerAnnotationFile = enablerAnnotationResult.getObject();
		assertThat(enablerAnnotationFile.getAnnotations().size(), equalTo(1));
		AnalyzedAnnotationType enablerAnnotation = enablerAnnotationFile
				.getAnnotations()
				.get(0);

		assertThat(enablerAnnotation.getAnnotationName(), equalTo("EnableExampleSDK"));
		assertThat(enablerAnnotation.getAnnotations().size(), equalTo(4));
		assertTrue(enablerAnnotation
						   .getAnnotations()
						   .stream()
						   .map(AnalyzedAnnotation::getType)
						   .anyMatch("Target"::equals));
		assertTrue(enablerAnnotation
						   .getAnnotations()
						   .stream()
						   .map(AnalyzedAnnotation::getType)
						   .anyMatch("Retention"::equals));
		assertTrue(enablerAnnotation
						   .getAnnotations()
						   .stream()
						   .map(AnalyzedAnnotation::getType)
						   .anyMatch("Import"::equals));
		assertTrue(enablerAnnotation
						   .getAnnotations()
						   .stream()
						   .map(AnalyzedAnnotation::getType)
						   .anyMatch("Generated"::equals));

		assertThat(enablerAnnotation.getFields().size(), equalTo(0));
		assertThat(enablerAnnotation.getMethods().size(), equalTo(0));
	}

}
