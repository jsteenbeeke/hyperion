package com.jeroensteenbeeke.hyperion.querygen.examples;

import java.util.List;

/**
 * Strategy for creating Swagger example annotations for generated query classes
 */
public interface ExamplesGenerator {
	/**
	 * Generates the examples
	 * @return A list of examples for the current type
	 */
	List<String> generateExamples();
}
