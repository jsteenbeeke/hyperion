package com.jeroensteenbeeke.hyperion.querygen.examples;



import java.util.List;

/**
 * Example generator for BooleanProperty
 */
public class BooleanExamplesGenerator implements ExamplesGenerator {
	@Override
	public List<String> generateExamples() {
		return List.of("true", "false", "!true", "!false", "#", "!#");
	}
}
