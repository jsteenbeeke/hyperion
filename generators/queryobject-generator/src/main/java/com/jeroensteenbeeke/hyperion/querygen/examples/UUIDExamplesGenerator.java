package com.jeroensteenbeeke.hyperion.querygen.examples;


import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.stream.Stream;

/**
 * Example generator for BooleanProperty
 */
public class UUIDExamplesGenerator implements ExamplesGenerator {
	private final String uuid;

	/**
	 * Constructor
	 * @param uuid The UUID to use in examples
	 */
	public UUIDExamplesGenerator(@NotNull String uuid) {
		this.uuid = uuid;
	}

	@Override
	public List<String> generateExamples() {
		return Stream.of("{uuid}", "!{uuid}", "#", "!#").map(ex -> ex.replace("{uuid}", uuid))
			.toList();
	}
}
