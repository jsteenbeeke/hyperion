package com.jeroensteenbeeke.hyperion.querygen;


import com.jeroensteenbeeke.hyperion.apt.core.data.ReferenceType;
import com.jeroensteenbeeke.hyperion.apt.core.data.TypeVisitor;
import com.jeroensteenbeeke.hyperion.apt.core.processor.basic.BasicClassWithFields;
import com.jeroensteenbeeke.hyperion.apt.core.processor.basic.BasicFieldDescriptor;
import com.jeroensteenbeeke.hyperion.apt.core.processor.basic.BasicHyperionClassProcessor;
import com.jeroensteenbeeke.hyperion.querygen.examples.*;
import com.jeroensteenbeeke.hyperion.rest.annotations.Queryable;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.control.Option;
import org.jetbrains.annotations.NotNull;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.tools.Diagnostic;
import javax.tools.FileObject;
import javax.tools.StandardLocation;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Annotation processor that creates query objects
 */
@SupportedAnnotationTypes("com.jeroensteenbeeke.hyperion.rest.annotations.Queryable")
@SupportedSourceVersion(SourceVersion.RELEASE_21)
public class QueryObjectGenerator extends BasicHyperionClassProcessor {
	@Override
	public void initEnvironment(@NotNull ProcessingEnvironment environment) {
		super.initEnvironment(environment);
		environment.getMessager()
				.printMessage(Diagnostic.Kind.NOTE, "QueryObjectGenerator initialized");
	}

	@Override
	protected String getGeneratorName() {
		return getClass().getSimpleName();
	}

	@Override
	protected List<Class<? extends Annotation>> getRequiredAnnotations() {
		return List.of(Queryable.class);
	}

	@Override
	protected void generateCode(@NotNull BasicClassWithFields classDescriptor) {
		var fields = classDescriptor.getFields()
				.filter(f -> f.isAnnotatedWith(Queryable.class))
				.flatMap(this::toQueryObjectField);

		final String targetPackage = classDescriptor.getPackageName().concat(".query");
		final String targetClassName = classDescriptor.getClassName().concat("_Query");

		QueryObjectDescriptor data = new QueryObjectDescriptor(classDescriptor.getClassName(),
				classDescriptor.getPackageName(), fields.toJavaList());

		String fqdn = String.format("%s.%s", targetPackage, targetClassName);

		try {
			processingEnv
					.getMessager()
					.printMessage(Diagnostic.Kind.NOTE, "Writing to " + fqdn);

			Filer filer = processingEnv.getFiler();
			FileObject classFile;
			try {
				classFile = filer.createSourceFile(fqdn);
			} catch (FilerException f) {
				// File already exists, get resource instead to overwrite
				classFile = filer.getResource(StandardLocation.SOURCE_OUTPUT, targetPackage,
						targetClassName.concat(".java"));
			}
			Set<String> imports = new TreeSet<>();
			imports.add("jakarta.annotation.Generated");
			imports.add("java.io.Serializable");
			imports.add("java.util.List");
			imports.add("java.util.LinkedList");
			imports.add("com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject");
			imports.add("jakarta.ws.rs.QueryParam");
			imports.add(data.getFQDN());

			data.getFields().stream().map(QueryObjectField::getType).forEach(imports::add);

			try (Writer fw = classFile.openWriter(); PrintWriter pw = new PrintWriter(fw)) {
				pw.print("package ");
				pw.print(targetPackage);
				pw.println(";");
				pw.println();
				imports.forEach(i -> {
					if (i.indexOf('.') == -1) {
						throw new IllegalArgumentException("Default package imports not allowed");
					}
					pw.print("import ");
					pw.print(i);
					pw.println(";");
				});
				pw.println();
				pw.printf(
						"@Generated(value = \"com.jeroensteenbeeke.hyperion.querygen.QueryObjectGenerator\",\n date="
								+
								" " +
								"\"%s\")",
						getDateString());
				pw.println();
				pw.print("public class ");
				pw.print(targetClassName);
				pw.printf(" implements QueryObject<%s>, Serializable {", data.getQueryClassName());
				pw.println();
				pw.println("\tprivate static final long serialVersionUID = 1L;");
				pw.println();
				pw.println("\tprivate int nextSortIndex;");
				pw.println();

				// Fields
				data.getFields().forEach(field -> {
					final String propertyFQDN = field.getType();
					final String propertyType = extractClass(propertyFQDN);
					final String fieldName = field.getName();

					pw.printf("\t@QueryParam(\"%s\")", fieldName);
					pw.println();
					pw.printf("\tprivate %1$s<%2$s> %3$s = new %1$s<>(this, \"%3$s\");", propertyType,
							targetClassName,
							fieldName);
					pw.println();
					pw.println();
				});

				// Accessors
				data.getFields().forEach(field -> {
					final String propertyFQDN = field.getType();
					final String propertyType = extractClass(propertyFQDN);

					pw.printf("\tpublic %1$s<%2$s> %3$s() {", propertyType, targetClassName,
							field.getName());
					pw.println();
					pw.printf("\t\treturn this.%s;", field.getName());
					pw.println();
					pw.println("\t}");
					pw.println();
				});

				pw.println("\t@Override");
				pw.println("\tpublic int getNextSortIndex() {");
				pw.println("\t\treturn nextSortIndex++;");
				pw.println("\t}");
				pw.println();

				pw.println("\t@Override");
				pw
						.printf("\tpublic boolean matches(%s object) {", data.getQueryClassName())
						.println();
				if (data.getFields().isEmpty()) {
					pw.println("\t\treturn true;");
				} else {
					pw.printf("\t\treturn %s;", data.getFields().stream()
							.map(QueryObjectField::getName)
							.map(n -> n + ".appliesTo(object)")
							.collect(Collectors.joining(" && "))
					).println();
				}
				pw.println("\t}");
				pw.println();
				pw.println("}");


				pw.flush();


			}

			processingEnv.getMessager()
					.printMessage(Diagnostic.Kind.NOTE, "Generated " + classFile.getName());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private Option<QueryObjectField> toQueryObjectField(BasicFieldDescriptor fieldDescriptor) {
		return fieldDescriptor
				.getType()
				.visit(new TypeVisitor<Tuple2<String, ExamplesGenerator>>() {
					@NotNull
					@Override
					public Option<Tuple2<String, ExamplesGenerator>> visit(
							@NotNull com.jeroensteenbeeke.hyperion.apt.core.data.PrimitiveType type) {
						return switch (type) {
							case Boolean -> Option.some(
									new Tuple2<>("com.jeroensteenbeeke.hyperion.rest.query.BooleanProperty",
											new BooleanExamplesGenerator()));
							case Double -> Option.some(
									new Tuple2<>("com.jeroensteenbeeke.hyperion.rest.query.DoubleProperty",
											new ComparableExamplesGenerator("4.0", "12.0")));
							case Float -> Option.some(
									new Tuple2<>("com.jeroensteenbeeke.hyperion.rest.query.FloatProperty",
											new ComparableExamplesGenerator("4.0", "12.0")));
							case Int -> Option.some(
									new Tuple2<>("com.jeroensteenbeeke.hyperion.rest.query.IntegerProperty",
											new ComparableExamplesGenerator("4", "12")));
							case Long -> Option.some(new Tuple2<>("com.jeroensteenbeeke.hyperion.rest.query.LongProperty",
									new ComparableExamplesGenerator("4", "12")));
							case Short -> Option.some(
									new Tuple2<>("com.jeroensteenbeeke.hyperion.rest.query.ShortProperty",
											new ComparableExamplesGenerator("4", "12")));
							default -> Option.none();
						};
					}

					@NotNull
					@Override
					public Option<Tuple2<String, ExamplesGenerator>> visit(@NotNull ReferenceType type) {
						if (isInstanceOf(String.class, type)) {
							return Option.some(new Tuple2<>("com.jeroensteenbeeke.hyperion.rest.query.StringProperty",
									new StringExamplesGenerator()));
						} else if (isInstanceOf(LocalDate.class, type)) {
							return Option.some(
									new Tuple2<>("com.jeroensteenbeeke.hyperion.rest.query.LocalDateProperty",
											new ComparableExamplesGenerator("2017-10-08",
													"2017-10-12")));
						} else if (isInstanceOf(LocalDateTime.class, type)) {
							return Option.some(
									new Tuple2<>("com.jeroensteenbeeke.hyperion.rest.query.LocalDateTimeProperty",
											new ComparableExamplesGenerator("2017-10-08T14:45:33.000",
													"2017-10-08T16:45:33.000")));
						} else if (isInstanceOf(ZonedDateTime.class, type)) {
							return Option.some(new Tuple2<>("com.jeroensteenbeeke.hyperion.rest.query.DateTimeProperty",
									new ComparableExamplesGenerator(
											"2017-10-08T14:45:33.000Z",
											"2017-10-08T16:45:33.000-01:00")));

						} else if (isInstanceOf(Boolean.class, type)) {
							return Option.some(new Tuple2<>("com.jeroensteenbeeke.hyperion.rest.query.BooleanProperty",
									new BooleanExamplesGenerator()));
						} else if (isInstanceOf(Double.class, type)) {
							return Option.some(new Tuple2<>("com.jeroensteenbeeke.hyperion.rest.query.DoubleProperty",
									new ComparableExamplesGenerator("4.0", "12.0")));
						} else if (isInstanceOf(BigDecimal.class, type)) {
							return Option.some(
									new Tuple2<>("com.jeroensteenbeeke.hyperion.rest.query.BigDecimalProperty",
											new ComparableExamplesGenerator("4.0", "12.0")));
						} else if (isInstanceOf(Float.class, type)) {
							return Option.some(new Tuple2<>("com.jeroensteenbeeke.hyperion.rest.query.FloatProperty",
									new ComparableExamplesGenerator("4.0", "12.0")));
						} else if (isInstanceOf(Integer.class, type)) {
							return Option.some(new Tuple2<>("com.jeroensteenbeeke.hyperion.rest.query.IntegerProperty",
									new ComparableExamplesGenerator("4", "12")));
						} else if (isInstanceOf(Long.class, type)) {
							return Option.some(new Tuple2<>("com.jeroensteenbeeke.hyperion.rest.query.LongProperty",
									new ComparableExamplesGenerator("4", "12")));
						} else if (isInstanceOf(Short.class, type)) {
							return Option.some(new Tuple2<>("com.jeroensteenbeeke.hyperion.rest.query.ShortProperty",
									new ComparableExamplesGenerator("4", "12")));
						} else if (isInstanceOf(UUID.class, type)) {
							return Option.some(new Tuple2<>("com.jeroensteenbeeke.hyperion.rest.query.UUIDProperty",
								new UUIDExamplesGenerator("d9c5c3d1-e879-4fc3-96d2-b15d3f9b0b4d")));
						}

						return Option.none();
					}

				})
				.map(typeAndExampleGenerator -> new QueryObjectField(typeAndExampleGenerator._1,
						fieldDescriptor.getName()).setGenerator(typeAndExampleGenerator._2));
	}

	/**
	 * Returns a representation of the current date
	 *
	 * @return A String representing the date
	 */
	protected String getDateString() {
		return new Date().toString();
	}
}
