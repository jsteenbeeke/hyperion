package com.jeroensteenbeeke.hyperion.querygen;

import java.util.List;

class QueryObjectDescriptor {
	private final String queryClassName;

	private final String packageName;

	private final List<QueryObjectField> fields;

	QueryObjectDescriptor(String queryClassName, String packageName,
								 List<QueryObjectField> fields) {
		this.queryClassName = queryClassName;
		this.packageName = packageName;
		this.fields = fields;
	}

	public String getQueryClassName() {
		return queryClassName;
	}

	public String getPackageName() {
		return packageName;
	}

	public List<QueryObjectField> getFields() {
		return fields;
	}

	public String getFQDN() {
		return String.format("%s.%s", packageName, queryClassName);
	}
}
