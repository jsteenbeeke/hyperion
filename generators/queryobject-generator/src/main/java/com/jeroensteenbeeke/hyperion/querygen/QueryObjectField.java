package com.jeroensteenbeeke.hyperion.querygen;

import com.jeroensteenbeeke.hyperion.querygen.examples.ExamplesGenerator;

class QueryObjectField {
	private final String type;

	private final String name;

	private ExamplesGenerator generator;

	public QueryObjectField(String type, String name) {
		this.type = type;
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public ExamplesGenerator getGenerator() {
		return generator;
	}

	public QueryObjectField setGenerator(ExamplesGenerator generator) {
		this.generator = generator;
		return this;
	}
}
