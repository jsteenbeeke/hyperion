package com.jeroensteenbeeke.hyperion.querygen;

import com.jeroensteenbeeke.hyperion.rest.annotation.Model;
import com.jeroensteenbeeke.hyperion.rest.query.StringProperty;

import jakarta.ws.rs.Path;

/**
 * Dependencies used by generated code, but not detected by dependency plugin
 */
public class FakeReferences {
	public FakeReferences() {
		System.out.println(StringProperty.class.getName());
		System.out.println(Path.class.getName());
		System.out.println(Model.class.getName());
	}
}
