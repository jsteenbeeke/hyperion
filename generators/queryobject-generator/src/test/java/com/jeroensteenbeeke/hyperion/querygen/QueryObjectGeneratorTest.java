package com.jeroensteenbeeke.hyperion.querygen;

import com.google.testing.compile.Compilation;
import com.google.testing.compile.JavaFileObjects;
import org.junit.jupiter.api.Test;

import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.tools.JavaFileObject;

import static com.google.testing.compile.CompilationSubject.assertThat;
import static com.google.testing.compile.Compiler.javac;

public class QueryObjectGeneratorTest {
	@Test
	public void testQueryObjectGenerator() {
		JavaFileObject file =
			JavaFileObjects.forSourceLines("com.jeroensteenbeeke.hyperion.querygen.TestObject",
				"package com.jeroensteenbeeke.hyperion.querygen;",
				"",
				"import com.jeroensteenbeeke.hyperion.rest.annotations.Queryable;",
				"import com.jeroensteenbeeke.hyperion.rest.annotation.Model;",
				"",
				"@Model",
				"public class TestObject {",
				"\t@Queryable",
				"\tprivate String testValue;",
				"}");


		Compilation compilation = javac().withOptions("-source", "17", "-target", "17", "-verbose", "-cp", System.getProperty("java" +
																															  ".class.path"))
			.withProcessors(new TestQueryObjectGenerator())
			.compile
				(file);
		assertThat(compilation).succeeded();
		assertThat(compilation)
			.generatedSourceFile(
				"com.jeroensteenbeeke.hyperion.querygen.query.TestObject_Query")
			.hasSourceEquivalentTo(JavaFileObjects
				.forSourceLines(
					"com.jeroensteenbeeke.hyperion.querygen.query.TestObject_Query",
					"package com.jeroensteenbeeke.hyperion.querygen.query;",
					"",
					"import com.jeroensteenbeeke.hyperion.querygen.TestObject;",
					"import com.jeroensteenbeeke.hyperion.rest.query.StringProperty;",
					"import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;",
					"import jakarta.annotation.Generated;",
					"import jakarta.ws.rs.QueryParam;",
					"import java.io.Serializable;",
					"import java.util.LinkedList;",
					"import java.util.List;",

					"",
					"@Generated(value = \"com.jeroensteenbeeke.hyperion.querygen" +
					".QueryObjectGenerator\",",
					"\tdate= \"now\")",
					"public class TestObject_Query implements " +
					"QueryObject<TestObject>," +
					" Serializable {",
					"\tprivate static final long serialVersionUID = 1L;",
					"",
					"\tprivate int nextSortIndex;",
					"",
					"\t@QueryParam(\"testValue\")",
					"\tprivate StringProperty<TestObject_Query> testValue = new " +
					"StringProperty<>(this, \"testValue\");",
					"",
					"\tpublic StringProperty<TestObject_Query> testValue() {",
					"\t\treturn this.testValue;",
					"\t}",
					"",
					"\t@Override",
					"\tpublic int getNextSortIndex() {",
					"\t\treturn nextSortIndex++;",
					"\t}",
					"",
					"\t@Override",
					"\tpublic boolean matches(TestObject object) {",
					"\t\treturn testValue.appliesTo(object);",
					"\t}",

					"}"

				));

	}

	@SupportedAnnotationTypes("com.jeroensteenbeeke.hyperion.rest.annotations.Queryable")
	@SupportedSourceVersion(SourceVersion.RELEASE_21)
	private static class TestQueryObjectGenerator extends QueryObjectGenerator {
		@Override
		protected String getDateString() {
			return "now";
		}
	}

}
