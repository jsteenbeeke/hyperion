package org.junit;

import java.io.Serializable;

/**
 * Shim
 */
public class ComparisonFailure extends AssertionError implements Serializable {
	/**
	 * Constructor
	 */
	public ComparisonFailure() {
	}

	/**
	 * Constructor
	 * @param detailMessage detail message
	 */
	public ComparisonFailure(Object detailMessage) {
		super(detailMessage);
	}

	/**
	 * Constructor
	 * @param detailMessage detail message
	 */
	public ComparisonFailure(boolean detailMessage) {
		super(detailMessage);
	}

	/**
	 * Constructor
	 * @param detailMessage detail message
	 */
	public ComparisonFailure(char detailMessage) {
		super(detailMessage);
	}

	/**
	 * Constructor
	 * @param detailMessage detail message
	 */
	public ComparisonFailure(int detailMessage) {
		super(detailMessage);
	}

	/**
	 * Constructor
	 * @param detailMessage detail message
	 */
	public ComparisonFailure(long detailMessage) {
		super(detailMessage);
	}

	/**
	 * Constructor
	 * @param detailMessage detail message
	 */
	public ComparisonFailure(float detailMessage) {
		super(detailMessage);
	}

	/**
	 * Constructor
	 * @param detailMessage detail message
	 */
	public ComparisonFailure(double detailMessage) {
		super(detailMessage);
	}

	/**
	 * Constructor
	 * @param message detail message
	 * @param cause cause
	 */
	public ComparisonFailure(String message, Throwable cause) {
		super(message, cause);
	}
}
