package org.junit.runners.model;

/**
 * Shim
 */
public abstract class Statement {
	/**
	 * Executes the statement
	 * @throws Throwable When shit hits the fan
	 */
	public abstract void evaluate() throws Throwable;
}
