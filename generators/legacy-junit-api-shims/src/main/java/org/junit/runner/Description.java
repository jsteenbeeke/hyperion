package org.junit.runner;

import java.io.Serializable;
import java.lang.annotation.Annotation;

/**
 * Description
 * @param testClass Test class
 * @param displayName Display name
 * @param uniqueId Unique ID
 * @param annotations Annotations
 */
public record Description (Class<?> testClass, String displayName, Serializable uniqueId, Annotation... annotations) {
	/**
	 * The test count
	 * @return the test count
	 */
	public int testCount() {
		return 0;
	}
}
