package org.junit.runner;


import org.junit.runner.notification.RunNotifier;

/**
 * Shim
 */
public abstract class Runner implements Describable {
	/**
	 * @return A description
	 */
	public abstract Description getDescription();

	/**
	 * Notifier
	 * @param notifier the notifier
	 */
	public abstract void run(RunNotifier notifier);

	/**
	 * Return the test count
	 * @return The test count
	 */
	public int testCount() {
		return getDescription().testCount();
	}
}
