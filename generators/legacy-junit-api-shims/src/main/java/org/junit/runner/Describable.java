package org.junit.runner;

/**
 * Something describable
 */
public interface Describable {
	/**
	 * The description
	 * @return the description
	 */
	Description getDescription();
}
