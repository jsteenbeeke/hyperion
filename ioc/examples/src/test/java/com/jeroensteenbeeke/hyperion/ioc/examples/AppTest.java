package com.jeroensteenbeeke.hyperion.ioc.examples;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
	@Test
	void testInjection() {
		App app = new App();
		app.start();
		var alphaService = app.alphaService;
		assertThat("AlphaService should be injected into App", alphaService, notNullValue());
		var multies = alphaService.multies;
		assertThat("Dependency<Multi> should be injected into AlphaService", multies, notNullValue());
		assertTrue(multies.isSatisfied(), "AlphaService.multies is satisfied");
		assertTrue(multies.isAmbiguous(), "AlphaService.multies is ambiguous");

		var betaService = alphaService.betaService;

		assertThat(betaService, notNullValue());

		var gammaService = betaService.gammaService;

		assertThat(gammaService, notNullValue());

		var gammaBetaService = gammaService.betaService;

		assertThat(gammaBetaService, notNullValue());
	}

}
