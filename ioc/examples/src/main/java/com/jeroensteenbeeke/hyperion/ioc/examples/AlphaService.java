package com.jeroensteenbeeke.hyperion.ioc.examples;

import com.jeroensteenbeeke.hyperion.ioc.Dependency;
import com.jeroensteenbeeke.hyperion.ioc.Inject;
import com.jeroensteenbeeke.hyperion.ioc.Singleton;

/**
 * Example class
 */
@Singleton
public class AlphaService {
	@Inject
	Dependency<Multi> multies;

	@Inject
	BetaService betaService;

}
