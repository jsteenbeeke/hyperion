package com.jeroensteenbeeke.hyperion.ioc.examples;

import com.jeroensteenbeeke.hyperion.ioc.Inject;

/**
 * Example class
 */
public class App {
	@Inject
	AlphaService alphaService;

	/**
	 * Example method
	 */
	public void start() {
		Hyperion$Injector.inject(this);
	}
}
