package com.jeroensteenbeeke.hyperion.ioc.examples;

import com.jeroensteenbeeke.hyperion.ioc.Initializable;
import com.jeroensteenbeeke.hyperion.ioc.Inject;
import com.jeroensteenbeeke.hyperion.ioc.Singleton;

/**
 * Example class
 */
@Singleton
public class GammaService implements Initializable {
	@Inject
	BetaService betaService;

	@Override
	public void initialize() {
		System.out.println(betaService.toString());
	}
}
