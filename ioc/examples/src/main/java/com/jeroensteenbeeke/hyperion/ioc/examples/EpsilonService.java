package com.jeroensteenbeeke.hyperion.ioc.examples;

import com.jeroensteenbeeke.hyperion.ioc.Singleton;

/**
 * Example class
 */
@Singleton
public class EpsilonService implements Multi {

	@Override
	public void fooBar() {

	}
}
