package com.jeroensteenbeeke.hyperion.ioc.examples;

/**
 * Example interface
 */
public interface Multi {
	/**
	 * Example method
	 */
	void fooBar();
}
