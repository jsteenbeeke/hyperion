package com.jeroensteenbeeke.hyperion.ioc.examples;

import com.jeroensteenbeeke.hyperion.ioc.NewInstance;

/**
 * Example class
 */
@NewInstance
public class DeltaService implements Multi{
	@Override
	public void fooBar() {

	}
}
