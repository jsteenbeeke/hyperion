package com.jeroensteenbeeke.hyperion.ioc.generator;

import com.jeroensteenbeeke.hyperion.apt.core.data.*;
import com.jeroensteenbeeke.hyperion.apt.core.processor.extended.ExtendedClassWithFields;
import com.jeroensteenbeeke.hyperion.apt.core.processor.extended.ExtendedFieldDescriptor;
import com.jeroensteenbeeke.hyperion.apt.core.processor.extended.ExtendedHyperionClassProcessor;
import com.jeroensteenbeeke.hyperion.ioc.*;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import org.jetbrains.annotations.NotNull;

import org.jetbrains.annotations.NotNull;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.Writer;
import java.lang.annotation.Annotation;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Annotation processor for creating injector classes for IoC
 */
@SupportedAnnotationTypes({
		"com.jeroensteenbeeke.hyperion.ioc.Inject",
		"com.jeroensteenbeeke.hyperion.ioc.NewInstance",
		"com.jeroensteenbeeke.hyperion.ioc.Singleton"
})
@SupportedSourceVersion(SourceVersion.RELEASE_21)
public class IOCProcessor extends ExtendedHyperionClassProcessor {
	private io.vavr.collection.HashSet<ExtendedClassWithFields> singletons;

	private io.vavr.collection.HashSet<ExtendedClassWithFields> newInstances;

	private io.vavr.collection.HashSet<ExtendedClassWithFields> injectionTargets;

	@Override
	protected String getGeneratorName() {
		return getClass().getName();
	}

	@Override
	protected List<Class<? extends Annotation>> getRequiredAnnotations() {
		return List.of(
				Inject.class,
				NewInstance.class,
				Singleton.class
		);
	}

	@Override
	public boolean process(
			Set<? extends TypeElement> annotations,
			RoundEnvironment roundEnv) {
		singletons = io.vavr.collection.HashSet.empty();
		newInstances = io.vavr.collection.HashSet.empty();
		injectionTargets = io.vavr.collection.HashSet.empty();

		boolean process = super.process(annotations, roundEnv);

		Configuration configuration = new Configuration(Configuration.VERSION_2_3_28);
		configuration.setClassForTemplateLoading(IOCProcessor.class, "template");
		configuration.setDefaultEncoding("UTF-8");

		var singletonsByPackage = singletons.groupBy(
				ExtendedClassWithFields::getPackageName);
		var newInstancesByPackage = newInstances.groupBy(ExtendedClassWithFields::getPackageName);
		var injectionTargetsByPackage = injectionTargets.groupBy(ExtendedClassWithFields::getPackageName);

		for (Tuple2<String, io.vavr.collection.HashSet<ExtendedClassWithFields>> packageClasses : injectionTargetsByPackage) {
			var packageName = packageClasses._1;

			try {
				JavaFileObject sourceFile = processingEnv.getFiler()
						.createSourceFile(packageName + ".Hyperion$Injector");


				Map<String, Object> datamodel = new HashMap<>();
				datamodel.put("packageName", packageName);
				datamodel.put("generationTime", getGenerationTime());
				datamodel.put("hasDependencyInjectionPoints", packageClasses._2().flatMap(ClassWithFields::getFields)
						.filter(f -> f.getType().getFQDN().equals(Dependency.class.getName()))
						.filter(f -> f.getAnnotations().exists(this::isInjectionTarget))
						.exists(f -> f.getType() instanceof ReferenceType));

				datamodel.put("targets", packageClasses._2().map(packageClass -> {
					var target = new HashMap<>();
					target.put("type", packageClass.getClassName());
					target.put("injectionPoints",
							packageClass.getFields()
									.filter(f -> !f.getType().getFQDN().equals(Dependency.class.getName()))
									.filter(f -> f.getAnnotations().exists(this::isInjectionTarget))
									.filter(f -> f.getType() instanceof ReferenceType).map(efd -> {
										ReferenceType refType = (ReferenceType) efd.getType();
										var injectionPoint = new HashMap<>();
										injectionPoint.put("fieldName", efd.getName());
										injectionPoint.put("typePackage", refType.getPackageName());
										injectionPoint.put("typeName", refType.getClassName());

										return injectionPoint;
									}).asJava());
					target.put("dependencyInjectionPoints",
							packageClass.getFields()
									.filter(f -> f.getType().getFQDN().equals(Dependency.class.getName()))
									.filter(f -> f.getAnnotations().exists(this::isInjectionTarget))
									.filter(f -> f.getType() instanceof GenerifiedType t && t.getGenericParameters().size() == 1)
									.map(efd -> {
										GenerifiedType genType = (GenerifiedType) efd.getType();
										GenerifiedType refType = genType.getGenericParameters().get();


										io.vavr.collection.HashSet<ExtendedClassWithFields> implementations = io.vavr.collection.HashSet.empty();
										implementations = implementations.addAll(
												singletons.peek(this::outputInterfaces).filter(
														s -> s.implementsType(refType)));
										implementations = implementations.addAll(
												newInstances.peek(this::outputInterfaces).filter(
														s -> s.implementsType(refType)));
										processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE,
												"Dependency<%s> has %d implementations".formatted(refType.getFQDN(),
														implementations.size()));

										var injectionPoint = new HashMap<>();
										injectionPoint.put("fieldName", efd.getName());
										injectionPoint.put("implementations", implementations
												.map(imp -> {
													var impData = new HashMap<String, Object>();
													impData.put("typePackage", imp.getPackageName());
													impData.put("typeName", imp.getClassName());
													return impData;
												}).toJavaSet()
										);

										return injectionPoint;
									}).asJava());

					return target;
				}).toJavaSet());

				try (Writer out = sourceFile.openWriter()) {
					Template template = configuration.getTemplate("injector.ftl");

					template.process(datamodel, out);

				}


			} catch (IOException | TemplateException e) {
				processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
						Optional.ofNullable(e.getMessage()).orElse(e.getClass().getName()));
			}
		}

		io.vavr.collection.Set<String> instanceHolderPackages = io.vavr.collection.HashSet.empty();
		instanceHolderPackages = instanceHolderPackages.addAll(singletonsByPackage.keySet());
		instanceHolderPackages = instanceHolderPackages.addAll(newInstancesByPackage.keySet());

		for (String packageName : instanceHolderPackages) {
			try {
				JavaFileObject sourceFile = processingEnv.getFiler()
						.createSourceFile(packageName + ".Hyperion$InstanceHolder");

				Map<String, Object> datamodel = new HashMap<>();
				datamodel.put("packageName", packageName);
				datamodel.put("generationTime", getGenerationTime());
				datamodel.put("singletons", singletonsByPackage.get(packageName).map(classes -> classes.map(
						classDescriptor -> {
							var singleton = new HashMap<String, Object>();
							singleton.put("type", classDescriptor.getClassName());
							singleton.put("isInjectionTarget", injectionTargets.contains(classDescriptor));
							singleton.put("initializable", classDescriptor.getDirectlyAndIndirectlyImplementedInterfaces().map(ReferenceType::getFQDN).find(
									Initializable.class.getName()::equals).isDefined());

							return singleton;
						}
				).toJavaSet()).getOrElse(HashSet::new));
				datamodel.put("newInstances", newInstancesByPackage.get(packageName).map(classes -> classes.map(
						classDescriptor -> {
							var newInstance = new HashMap<String, Object>();
							newInstance.put("type", classDescriptor.getClassName());
							newInstance.put("isInjectionTarget", injectionTargets.contains(classDescriptor));
							newInstance.put("initializable", classDescriptor.getDirectlyAndIndirectlyImplementedInterfaces().map(ReferenceType::getFQDN).find(
									Initializable.class.getName()::equals).isDefined());

							return newInstance;
						}
				).toJavaSet()).getOrElse(HashSet::new));

				try (Writer out = sourceFile.openWriter()) {
					Template template = configuration.getTemplate("holder.ftl");

					template.process(datamodel, out);

				}
			} catch (IOException | TemplateException e) {
				processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
						Optional.ofNullable(e.getMessage()).orElse(e.getClass().getName()));
			}
		}

		return process;
	}

	private void outputInterfaces(ExtendedClassWithFields extendedClassWithFields) {
		io.vavr.collection.Set<ReferenceType> directlyAndIndirectlyImplementedInterfaces = extendedClassWithFields.getDirectlyAndIndirectlyImplementedInterfaces();
		System.out.printf("%s implements %d interfaces directly or indirectly: %s%n", extendedClassWithFields.getFQDN(),
				directlyAndIndirectlyImplementedInterfaces.size(),
				directlyAndIndirectlyImplementedInterfaces.map(ReferenceType::getFQDN).mkString(", "));
	}

	/**
	 * The time the generator created the class
	 * @return The generation time as a String
	 */
	protected String getGenerationTime() {
		return DateTimeFormatter.ISO_DATE_TIME.format(LocalDateTime.now());
	}

	@Override
	protected void generateCode(
			@NotNull ExtendedClassWithFields classDescriptor) {
		if (classDescriptor.getAnnotations().exists(this::isSingleton)) {
			checkZeroArgsConstructor(classDescriptor, Singleton.class);
			singletons = singletons.add(classDescriptor);
			checkInjectionTarget(classDescriptor);
		} else if (classDescriptor.getAnnotations()
				.exists(this::isNewInstanceClass)) {
			checkZeroArgsConstructor(classDescriptor, NewInstance.class);
			newInstances = newInstances.add(classDescriptor);
			checkInjectionTarget(classDescriptor);
		} else if (classDescriptor.getFields().flatMap(ExtendedFieldDescriptor::getAnnotations)
				.exists(this::isInjectionTarget) || classDescriptor.getConstructors()
				.flatMap(ConstructorDescriptor::getAnnotations).exists(this::isInjectionTarget)) {
			checkInjectionTarget(classDescriptor);
		}
	}

	private void checkZeroArgsConstructor(ExtendedClassWithFields classDescriptor, Class<? extends Annotation> annotationClass) {
		if (!classDescriptor.getConstructors().isEmpty()) {
			if (!classDescriptor.getConstructors().exists(ctor -> ctor.getParameters().isEmpty())) {
				processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
						"Class %s is marked as @%s but does not have a zero argument constructor".formatted(
								classDescriptor.getFQDN(), annotationClass.getSimpleName()));
			}
		}
	}

	private void checkInjectionTarget(@NotNull ExtendedClassWithFields classDescriptor) {
		for (ExtendedFieldDescriptor field : classDescriptor.getFields()) {
			if (field.getAnnotations().exists(this::isInjectionTarget)) {
				if (field.getModifiers().contains(Modifier.PRIVATE)) {
					processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR,
							"%s.%s is declared private. Visibility should be at least package-private".formatted(
									classDescriptor.getFQDN(), field.getName()));
				}
				injectionTargets = injectionTargets.add(classDescriptor);
				break;
			}
		}
	}

	private boolean isNewInstanceClass(@NotNull AnnotationDescriptor annotationDescriptor) {
		return annotationDescriptor.getType().getFQDN().equals(NewInstance.class.getName());
	}

	private boolean isSingleton(@NotNull AnnotationDescriptor annotationDescriptor) {
		return annotationDescriptor.getType().getFQDN().equals(Singleton.class.getName());
	}

	private boolean isInjectionTarget(@NotNull AnnotationDescriptor annotationDescriptor) {
		return annotationDescriptor.getType().getFQDN().equals(Inject.class.getName());
	}
}

