package ${packageName};

import com.jeroensteenbeeke.hyperion.ioc.metadata.Generated;
<#if hasDependencyInjectionPoints>
import com.jeroensteenbeeke.hyperion.ioc.Dependency;
</#if>

@Generated(when = "${generationTime}", by = "com.jeroensteenbeeke.hyperion.ioc.generator.IOCProcessor")
public class Hyperion$Injector {
    <#list targets as target>
        public static void inject(${target.type} target) {
            <#list target.injectionPoints as injectionPoint>
            if (target.${injectionPoint.fieldName} == null) {
                target.${injectionPoint.fieldName} = ${injectionPoint.typePackage}<#if injectionPoint.typePackage?length gt 0>.</#if>Hyperion$InstanceHolder.INSTANCE.get${injectionPoint.typeName}();
            }
            </#list>
            <#list target.dependencyInjectionPoints as injectionPoint>
            if (target.${injectionPoint.fieldName} == null) {
                target.${injectionPoint.fieldName} = new Dependency<>(
                    <#list injectionPoint.implementations as implementation>
                    ${implementation.typePackage}.Hyperion$InstanceHolder.INSTANCE.get${implementation.typeName}()<#sep>,
                    </#list>
                );
            }
            </#list>
    }
    </#list>
}
