package ${packageName};

import com.jeroensteenbeeke.hyperion.ioc.Initializable;
import com.jeroensteenbeeke.hyperion.ioc.metadata.Generated;
<#if singletons?size gt 0>
import com.jeroensteenbeeke.hyperion.ioc.metadata.YieldsSingleton;
</#if>
<#if newInstances?size gt 0>
import com.jeroensteenbeeke.hyperion.ioc.metadata.YieldsNewInstance;
</#if>

@Generated(when = "${generationTime}", by = "com.jeroensteenbeeke.hyperion.ioc.generator.IOCProcessor")
public enum Hyperion$InstanceHolder {
    INSTANCE;

    <#list singletons as singleton>
    private ${singleton.type} ${singleton.type?uncap_first} = null;
    </#list>

    <#list singletons as singleton>
    @YieldsSingleton
    public ${singleton.type} get${singleton.type}() {
        if (${singleton.type?uncap_first} == null) {
            ${singleton.type?uncap_first} = new ${singleton.type}();
            <#if singleton.isInjectionTarget>
            Hyperion$Injector.inject( ${singleton.type?uncap_first});
            </#if>
            <#if singleton.initializable>
            ${singleton.type?uncap_first}.initialize();
            </#if>
        }

        return ${singleton.type?uncap_first};
    }
    </#list>

    <#list newInstances as newinstance>
    @YieldsNewInstance
    public ${newinstance.type} get${newinstance.type}() {
        var ${newinstance.type?uncap_first} = new ${newinstance.type}();
        <#if newinstance.isInjectionTarget>
        Hyperion$Injector.inject( ${newinstance.type?uncap_first});
        </#if>
        <#if newinstance.initializable>
        ${newinstance.type?uncap_first}.initialize();
        </#if>

        return ${newinstance.type?uncap_first};
    }
    </#list>

}
