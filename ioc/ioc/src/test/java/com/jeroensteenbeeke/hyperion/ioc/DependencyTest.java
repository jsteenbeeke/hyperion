package com.jeroensteenbeeke.hyperion.ioc;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class DependencyTest {

	@Test
	void isSatisfied() {
		assertFalse(new Dependency<>().isSatisfied());
		assertTrue(new Dependency<>("A").isSatisfied());
		assertTrue(new Dependency<>("A", "B").isSatisfied());
	}

	@Test
	void isAmbiguous() {
		assertFalse(new Dependency<>().isAmbiguous());
		assertFalse(new Dependency<>("A").isAmbiguous());
		assertTrue(new Dependency<>("A", "B").isAmbiguous());
	}

	@Test
	void get() {
		assertThrows(UnsatisfiedDependencyException.class, () -> new Dependency<>().get());
		assertEquals("A", new Dependency<>("A").get());
		assertThrows(AmbiguousDependencyException.class, () -> new Dependency<>("A", "B").get());
	}

	@Test
	void stream() {
		assertEquals(List.of(), new Dependency<>().stream().collect(Collectors.toList()));
		assertEquals(List.of("A"), new Dependency<>("A").stream().collect(Collectors.toList()));
		assertEquals(List.of("A", "B"), new Dependency<>("A", "B").stream().collect(Collectors.toList()));
	}
}
