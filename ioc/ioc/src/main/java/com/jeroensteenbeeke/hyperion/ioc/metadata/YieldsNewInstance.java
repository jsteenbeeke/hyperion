package com.jeroensteenbeeke.hyperion.ioc.metadata;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks the given class as always yielding a new instance
 */
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.METHOD)
public @interface YieldsNewInstance {

}
