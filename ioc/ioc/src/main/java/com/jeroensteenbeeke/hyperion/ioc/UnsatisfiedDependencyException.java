package com.jeroensteenbeeke.hyperion.ioc;

/**
 * Exception thrown when {@link Dependency#get()} is invoked and there is no matching dependency
 */
public class UnsatisfiedDependencyException extends RuntimeException {
	/**
	 * Constructor
	 */
	public UnsatisfiedDependencyException() {
	}
}
