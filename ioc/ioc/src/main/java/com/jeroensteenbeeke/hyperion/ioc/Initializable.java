package com.jeroensteenbeeke.hyperion.ioc;

/**
 * A bean that has initialization logic that needs to take place after injection
 */
public interface Initializable {
	/**
	 * Method called after injection takes place
	 */
	void initialize();
}
