package com.jeroensteenbeeke.hyperion.ioc;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * When used as a field, allows any number instances of the given generic types to be injected at this point
 *
 * @param <T> The target type
 */
public class Dependency<T> implements Iterable<T> {
	private final T[] instances;

	/**
	 * Non-public API. Creates a new instance with the given injected instances
	 *
	 * @param instances The instances
	 */
	@SafeVarargs
	public Dependency(T... instances) {
		this.instances = instances;
	}

	/**
	 * Whether or not any objects match the injection point
	 *
	 * @return {@code true} if a match exists, {@code false} otherwise
	 */
	public boolean isSatisfied() {
		return instances.length > 0;
	}

	/**
	 * Whether or not there is a single match
	 *
	 * @return {@code true} if more than a single match exists, {@code false} otherwise
	 */
	public boolean isAmbiguous() {
		return instances.length > 1;
	}

	/**
	 * Gets the single dependency, if a single dependency exists
	 *
	 * @return An instance of T
	 * @throws UnsatisfiedDependencyException If no dependencies exists
	 * @throws AmbiguousDependencyException   If there is more than 1 dependency
	 */
	public T get() {
		if (!isSatisfied()) {
			throw new UnsatisfiedDependencyException();
		}
		if (isAmbiguous()) {
			throw new AmbiguousDependencyException();
		}

		return instances[0];
	}

	/**
	 * Returns a stream of all satisfying dependencies
	 *
	 * @return A stream of instances. May be empty
	 */
	public Stream<T> stream() {
		return Arrays.stream(instances);
	}

	@Override
	public Iterator<T> iterator() {
		return stream().iterator();
	}

	@Override
	public void forEach(Consumer<? super T> action) {
		stream().forEach(action);
	}

	@Override
	public Spliterator<T> spliterator() {
		return stream().spliterator();
	}
}
