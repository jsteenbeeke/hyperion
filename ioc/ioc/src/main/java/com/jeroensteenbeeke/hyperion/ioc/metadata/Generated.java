package com.jeroensteenbeeke.hyperion.ioc.metadata;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks the given type as generated
 */
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.TYPE)
public @interface Generated {
	/**
	 * Indicates when the type was generated
 	 */
	String when();

	/**
	 * Indicates the source of the generated type
	 */
	String by();
}
