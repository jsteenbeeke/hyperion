package com.jeroensteenbeeke.hyperion.ioc;

/**
 * Exception thrown when {@link Dependency#get()} is invoked and there is more than 1 matching dependency
 */
public class AmbiguousDependencyException extends RuntimeException {
	/**
	 * Constructor
	 */
	public AmbiguousDependencyException() {
	}
}
