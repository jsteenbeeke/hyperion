package com.jeroensteenbeeke.hyperion.ioc;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a given class as a dependency that yields a new instance on each inject. Similar to a prototype scope in Spring
 */
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.TYPE)
public @interface NewInstance {
}
