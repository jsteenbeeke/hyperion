package com.jeroensteenbeeke.hyperion.vavr.wicket.repeater;

import io.vavr.collection.Seq;
import org.apache.wicket.model.IModel;

/**
 * Model for seq items.
 *
 * @see SeqView
 * @param <T> Model object type
 */
public class SeqItemModel<T> implements IModel<T>
{
	private static final long serialVersionUID = 1L;

	/** The ListView itself */
	private final SeqView<T> seqView;

	/** The list item's index */
	private final int index;

	/**
	 * Construct
	 *
	 * @param seqView the SeqView
	 * @param index the index of this model
	 */
	public SeqItemModel(final SeqView<T> seqView, final int index)
	{
		this.seqView = seqView;
		this.index = index;
	}

	@Override
	public T getObject()
	{
		return seqView.getModelObject().get(index);
	}

	@Override
	public void setObject(T object)
	{
		Seq<T> seq = seqView.getModelObject();

		Seq<T> head = seq.slice(0, index);
		Seq<T> tail = seq.slice(index+1, seq.size());

		seqView.setModelObject(head.append(object).appendAll(tail));
	}
}
