package com.jeroensteenbeeke.hyperion.vavr.wicket;

import com.jeroensteenbeeke.hyperion.vavr.wicket.model.SeqModel;
import com.jeroensteenbeeke.hyperion.vavr.wicket.repeater.SeqItem;
import com.jeroensteenbeeke.hyperion.vavr.wicket.repeater.SeqView;
import io.vavr.collection.Array;
import io.vavr.collection.List;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;

public class SeqViewTestPage extends WebPage {
	public SeqViewTestPage() {
		add(new SeqView<Integer>("view1", List.of(1,2,3)) {
			@Override
			protected void populateItem(SeqItem<Integer> item) {
				item.add(new Label("number", item.getModelObject()));
			}
		});
		SeqView<Integer> view2;
		add(view2 = new SeqView<Integer>("view2") {
			@Override
			protected void populateItem(SeqItem<Integer> item) {
				item.add(new Label("number", item.getModelObject()));
			}
		});
		view2.setModel(SeqModel.of(Array.of(4)));
		view2.setModelObject(Array.of(4,5,6,7));
		view2.setReuseItems(true);
		view2.setStartIndex(1);

		add(new Link<Void>("refresh") {
			@Override
			public void onClick() {

			}
		});
	}
}
