/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
(function ( $ ) {
	function performCycle(fade, delay, current, items, itemCount) {
		var next = (current+1) % itemCount;
		
		items[next].css('z-index', 2);
		items[next].css('display', 'block');
		items[current].fadeOut(fade, function() {
			items[next].css('z-index', '');
			items[next].css('display', '');
			items[current].removeClass('hyperion-cycler-item-active');
			items[next].addClass('hyperion-cycler-item-active');
		});
		
		setTimeout(performCycle, delay, fade, delay, next, items, itemCount);
	}
	
	$.fn.contentCycler = function(minHeight, delay, fade) {
		return this.each(function(i, cycler) {
			var items = [], itemCount = 0, minWidth = 0, minHeight = 0;
			
			$(cycler).find('.hyperion-cycler-item').each(function(c, item) {
				itemCount++;
				items.push($(item));
				
				minWidth = Math.max(minWidth, $(item).width());
				minHeight = Math.max(minHeight, $(item).height());
			});
			
			$(cycler).css('min-width', minWidth);
			$(cycler).css('min-height', minHeight);
			
			setTimeout(performCycle, delay, fade, delay, 0, items, itemCount);
		});
	};
}(jQuery));