package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.FormComponentPanel;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.IMaxLengthSupportingComponent;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.IMinLengthSupportingComponent;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.form.EmailTextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.validation.validator.StringValidator;

/**
 * FormComponentPanel that renders an EmailTextField
 */
public class EmailPanel extends FormComponentPanel<EmailTextField,String> implements IMinLengthSupportingComponent, IMaxLengthSupportingComponent {
	private static final long serialVersionUID = -4342649649732618979L;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param initialValue Model containing the value of the field
	 */
	public EmailPanel(String id, IModel<String> initialValue) {
		super(id, new EmailTextField("email", initialValue));
	}

	@Override
	public void setMaxLength(int maxLength) {
		getFormComponent().add(StringValidator.maximumLength(maxLength));
		getFormComponent().add(AttributeModifier.replace("maxlength", maxLength));
	}

	@Override
	public void setMinLength(int minLength) {
		getFormComponent().add(StringValidator.minimumLength(minLength));
		getFormComponent().add(AttributeModifier.replace("minlength", minLength));
	}
}
