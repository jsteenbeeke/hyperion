package com.jeroensteenbeeke.hyperion.webcomponents.entitypage;

import org.apache.wicket.Component;
import org.apache.wicket.markup.html.WebMarkupContainer;

/**
 * No operation implementation of INavComponentProvider. Adds an empty WebMarkupContainer as navigation, and makes it invisible
 */
public class EmptyNavComponentProvider implements INavComponentProvider {
	@Override
	public Component createNavComponent(String id) {
		return new WebMarkupContainer(id).setVisible(false);
	}
}
