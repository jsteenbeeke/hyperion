/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.annotation;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.DefaultFieldType;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.IFieldType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to specify a given field should show up in a form
 */
@Target(value = ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EntityFormField {
	/**
	 * The label of the field. In case the property {@code localized} is set to {@code true}, this field is interpreted
	 * as being a localization key for a property file.
	 *
	 * @return The label of the of the field or the I18N/L10N key
	 */
	String label();

	/**
	 * Signifies whether or not the given field is required
	 * @return {@code true} if the field is required. {@code false} otherwise
	 */
	boolean required() default false;

	/**
	 * Signifies whether or ont the given field's label should be localized.
	 * @return {@code true} if localization should be applied. {@code false} otherwise
	 */
	boolean localized() default false;

	/**
	 * Signifies which factory should be used to create a form component for this field
	 * @return The Class representing the factory
	 */
	Class<? extends IFieldType> type() default DefaultFieldType.TextField.class;
}
