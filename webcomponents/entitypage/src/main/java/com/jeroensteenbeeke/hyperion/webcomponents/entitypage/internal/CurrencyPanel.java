package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import com.jeroensteenbeeke.hyperion.webcomponents.core.form.CurrencyTextField;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.FormComponentPanel;
import org.apache.wicket.model.IModel;
import org.danekja.java.util.function.serializable.SerializableFunction;

/**
 * FormComponentPanel that renders a CurrencyTextField
 * @param <N> The type of number contained
 */
public class CurrencyPanel<N extends Number> extends FormComponentPanel<CurrencyTextField<N>,N> {
	private static final long serialVersionUID = -1081056929354450651L;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param model Model containing the currency
	 * @param doubleToNumber Function for converting a double to the specified number type
	 */
	public CurrencyPanel(String id, IModel<N> model, SerializableFunction<Double,N>
			doubleToNumber) {
		super(id, new CurrencyTextField<>("currency", model, doubleToNumber));
	}
}
