package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import com.jeroensteenbeeke.hyperion.webcomponents.core.form.TimeTextField;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.FormComponentPanel;
import org.apache.wicket.model.IModel;

import org.jetbrains.annotations.NotNull;

/**
 * FormComponentPanel that renders a time component
 * @param <T> The type of time value
 */
public class TimePanel<T> extends FormComponentPanel<TimeTextField<T>,T> {
	private static final long serialVersionUID = 4382268300201708034L;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param timeModel The model containing the time value
	 * @param timeClass The class contained in the model
	 */
	public TimePanel(@NotNull String id, IModel<T> timeModel, Class<T> timeClass) {
		super(id, new TimeTextField<>("time", timeModel, timeClass));
	}
}
