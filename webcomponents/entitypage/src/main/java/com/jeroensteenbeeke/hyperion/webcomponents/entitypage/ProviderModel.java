/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.entitypage;

import org.apache.wicket.model.IModel;

import java.util.List;

/**
 * Model for IFieldMetadataProvider
 * @param <T> The type of object in the model
 */
public class ProviderModel<T> implements IModel<List<T>> {
	private static final long serialVersionUID = 1L;

	private final IFieldMetadataProvider provider;

	private final String fieldName;

	/**
	 * Constructor
	 * @param provider Metadata provider
	 * @param fieldName The name of the first
	 */
	public ProviderModel(IFieldMetadataProvider provider, String fieldName) {
		this.provider = provider;
		this.fieldName = fieldName;
	}

	@Override
	public List<T> getObject() {
		return provider.getSelectableValues(fieldName);
	}
}
