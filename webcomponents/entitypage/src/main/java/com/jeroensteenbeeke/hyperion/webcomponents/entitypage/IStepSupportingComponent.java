package com.jeroensteenbeeke.hyperion.webcomponents.entitypage;

/**
 * Interface allowing the application of steps on some select projects
 */
public interface IStepSupportingComponent {
	/**
	 * Set the step property of a form field, ensuring the increments of this vj
	 * @param step The increment of the field
	 */
	void setStep(double step);
}
