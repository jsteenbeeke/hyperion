package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.FormComponentPanel;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.IMaxLengthSupportingComponent;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.IMinLengthSupportingComponent;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.validation.validator.StringValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * FormComponentPanel that renders a {@code <input type="password">} using a PasswordTextField
 */
public class PasswordPanel extends FormComponentPanel<PasswordTextField,String> implements IMinLengthSupportingComponent, IMaxLengthSupportingComponent {
	private static final Logger log = LoggerFactory.getLogger(PasswordPanel.class);

	private static final long serialVersionUID = -5022949530980216476L;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param initialValue The model containing the password value
	 */
	public PasswordPanel(String id, IModel<String> initialValue) {
		super(id, new PasswordTextField("password", initialValue));
	}


	@Override
	public void setMaxLength(int maxLength) {
		if (maxLength != 64) {
			log.error("You have specified a maximum password length of {}. OWASP recommended password maximum length is 64", maxLength);
		}

		getFormComponent().add(StringValidator.maximumLength(maxLength));
		getFormComponent().add(AttributeModifier.replace("maxlength", maxLength));
	}

	@Override
	public void setMinLength(int minLength) {
		if (minLength < 8) {
			log.error("Password minimum length is set below OWASP recommended minimum of 8");
		}

		getFormComponent().add(StringValidator.minimumLength(minLength));
		getFormComponent().add(AttributeModifier.replace("minlength", minLength));
	}
}
