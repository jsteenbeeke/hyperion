package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import org.apache.wicket.model.IModel;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidationError;
import org.apache.wicket.validation.IValidator;
import org.apache.wicket.validation.validator.PatternValidator;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Pattern validator that works on objects that we may not be Strings
 * @param <T> The type the value actually is
 */
class GenerifiedPatternValidator<T> implements IValidator<T> {
	private final PatternValidator delegate;

	/**
	 * Constructor
	 * @param pattern The pattern to match
	 * @param reversed Whether or not the pattern should be reversed
	 */
	GenerifiedPatternValidator(@NotNull String pattern, boolean reversed) {
		this.delegate = new PatternValidator(pattern).setReverse(reversed);
	}

	@Override
	public void validate(IValidatable<T> validatable) {
		delegate.validate(
				new IValidatable<>() {
					@Override
					public String getValue() {
						return getModel().getObject();
					}

					@Override
					public void error(IValidationError error) {
						validatable.error(error);
					}

					@Override
					public boolean isValid() {
						return validatable.isValid();
					}

					@Override
					public IModel<String> getModel() {
						return validatable.getModel().map(this::stringify);
					}

					private String stringify(@Nullable T original) {
						if (original == null) {
							return null;
						}

						if (original instanceof String s) {
							return s;
						}

						return original.toString();
					}
				});

	}
}
