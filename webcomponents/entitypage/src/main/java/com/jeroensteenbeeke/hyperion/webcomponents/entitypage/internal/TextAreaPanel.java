package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.FormComponentPanel;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.IMaxLengthSupportingComponent;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.IMinLengthSupportingComponent;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.model.IModel;
import org.apache.wicket.validation.validator.StringValidator;

/**
 * FormComponentPanel that renders a text area
 *
 * @param <T> The type of data to display in the text area
 */
public class TextAreaPanel<T> extends FormComponentPanel<TextArea<T>, T> implements IMinLengthSupportingComponent, IMaxLengthSupportingComponent {
	private static final long serialVersionUID = -2701726887788225548L;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param initialValue The model of the text area
	 */
	public TextAreaPanel(String id, IModel<T> initialValue) {
		super(id, new TextArea<>("textarea", initialValue));
	}

	@Override
	public void setMaxLength(int maxLength) {
		getFormComponent().add(StringValidator.maximumLength(maxLength));
		getFormComponent().add(AttributeModifier.replace("maxlength", maxLength));
	}

	@Override
	public void setMinLength(int minLength) {
		getFormComponent().add(StringValidator.minimumLength(minLength));
		getFormComponent().add(AttributeModifier.replace("minlength", minLength));
	}
}
