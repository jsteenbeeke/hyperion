package com.jeroensteenbeeke.hyperion.webcomponents.entitypage;

/**
 * Component that allows the application of ranges as well as minima and maxima
 */
public interface IRangeSupportingComponent{
	/**
	 * Applies the given range to the component
	 * @param min The minimum value
	 * @param max The maximum value
	 */
	void applyRange(double min, double max);

	/**
	 * Applies the given minimum to the component
	 * @param min The minimum
	 */
	void applyMinimum(double min);

	/**
	 * Applies the given maximum to the component
	 * @param max The maximum
	 */
	void applyMaximum(double max);
}
