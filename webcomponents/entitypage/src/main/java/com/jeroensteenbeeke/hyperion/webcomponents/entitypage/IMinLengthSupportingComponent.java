package com.jeroensteenbeeke.hyperion.webcomponents.entitypage;

/**
 * Interface allowing the application of minimum length on selected fields
 */
public interface IMinLengthSupportingComponent {
	/**
	 * Set the minimum length of the given field
	 * @param minLength The minimum length of the field
	 */
	void setMinLength(int minLength);
}
