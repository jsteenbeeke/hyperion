package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.FormComponentPanel;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.IMaxLengthSupportingComponent;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.IMinLengthSupportingComponent;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.form.UrlTextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.validation.validator.StringValidator;

/**
 * FormComponentPanel that renders an URL as a UrlTextField
 */
public class UrlPanel extends FormComponentPanel<UrlTextField,String> implements IMinLengthSupportingComponent, IMaxLengthSupportingComponent {
	private static final long serialVersionUID = -8691992490403602008L;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param initialValue The model containing the URL
	 */
	public UrlPanel(String id, IModel<String> initialValue) {
		super(id, new UrlTextField("url", initialValue));
	}

	@Override
	public void setMaxLength(int maxLength) {
		getFormComponent().add(StringValidator.maximumLength(maxLength));
		getFormComponent().add(AttributeModifier.replace("maxlength", maxLength));
	}

	@Override
	public void setMinLength(int minLength) {
		getFormComponent().add(StringValidator.minimumLength(minLength));
		getFormComponent().add(AttributeModifier.replace("minlength", minLength));
	}
}
