package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.FormComponentPanel;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.IMaxLengthSupportingComponent;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.IMinLengthSupportingComponent;
import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.IPatternSupportingComponent;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;
import org.apache.wicket.validation.validator.StringValidator;

import org.jetbrains.annotations.NotNull;

/**
 * FormComponent that renders a default textfield
 * @param <T> The type of data to show in the textfield
 */
public class TextFieldPanel<T> extends FormComponentPanel<TextField<T>,T> implements IMinLengthSupportingComponent, IMaxLengthSupportingComponent, IPatternSupportingComponent {
	private static final long serialVersionUID = 7534230215249861530L;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param initialValue The value of the field
	 * @param propertyClass The type of field
	 */
	public TextFieldPanel(String id, IModel<T> initialValue, Class<T> propertyClass) {
		super(id, new TextField<>("text", initialValue, propertyClass));
	}

	@Override
	public void setMaxLength(int maxLength) {
		getFormComponent().add(StringValidator.maximumLength(maxLength));
		getFormComponent().add(AttributeModifier.replace("maxlength", maxLength));
	}

	@Override
	public void setMinLength(int minLength) {
		getFormComponent().add(StringValidator.minimumLength(minLength));
		getFormComponent().add(AttributeModifier.replace("minlength", minLength));
	}

	@Override
	public void setPattern(@NotNull String pattern, boolean reverse) {
		getFormComponent().add(new GenerifiedPatternValidator<>(pattern, reverse));
	}
}
