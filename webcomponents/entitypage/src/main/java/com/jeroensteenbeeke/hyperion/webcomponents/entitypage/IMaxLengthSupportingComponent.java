package com.jeroensteenbeeke.hyperion.webcomponents.entitypage;

/**
 * Interface allowing the application of maximum length on selected fields
 */
public interface IMaxLengthSupportingComponent {
	/**
	 * Set the maximum length of the given field
	 * @param maxLength The maximum length of the field
	 */
	void setMaxLength(int maxLength);
}
