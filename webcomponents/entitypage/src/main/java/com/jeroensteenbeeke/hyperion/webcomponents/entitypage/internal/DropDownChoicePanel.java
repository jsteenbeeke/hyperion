package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.FormComponentPanel;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.model.IModel;

import java.util.List;

/**
 * FormComponentPanel that renders a DropDownChoice
 * @param <T> The type contained in the dropdown
 */
public class DropDownChoicePanel<T> extends FormComponentPanel<DropDownChoice<T>, T> {
	private static final long serialVersionUID = 1612431846116235028L;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param model The model containing the selected value
	 * @param choices The model containing the available choice
	 * @param renderer The renderer that turns the choices into something the user can read
	 */
	public DropDownChoicePanel(String id, IModel<T> model, IModel<? extends List<? extends T>> choices,
							   IChoiceRenderer<? super T> renderer) {
		super(id, new DropDownChoice<>("dropdown", model, choices, renderer));
	}
}
