package com.jeroensteenbeeke.hyperion.webcomponents.entitypage;

import org.jetbrains.annotations.NotNull;

/**
 * Interface allowing the application of patterns on selected fields
 */
public interface IPatternSupportingComponent {
	/**
	 * Set the required pattern of the given field
	 * @param pattern The required pattern of the field
	 * @param reverse Whether or no the pattern should be reversed
	 */
	void setPattern(@NotNull String pattern, boolean reverse);
}
