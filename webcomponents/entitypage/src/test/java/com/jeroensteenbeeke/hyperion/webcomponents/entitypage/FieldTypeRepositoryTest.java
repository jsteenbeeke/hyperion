package com.jeroensteenbeeke.hyperion.webcomponents.entitypage;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertSame;

public class FieldTypeRepositoryTest {
	@Test
	public void testRepeatInvocation() {
		DefaultFieldType.Number number = FieldTypeRepository.instance.get(DefaultFieldType.Number.class);
		assertThat(number, not(nullValue()));

		DefaultFieldType.Number number2 = FieldTypeRepository.instance.get(DefaultFieldType.Number.class);
		assertThat(number2, not(nullValue()));
		assertSame(number, number2);
	}
}
