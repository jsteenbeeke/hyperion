package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.util.tester.FormTester;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.jupiter.api.Test;

import java.util.Locale;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DatePanelTest {

	@Test
	public void testValidDateTypes() {
		Locale.setDefault(Locale.US);

		WicketTester tester = new WicketTester();
		tester.startPage(DatePanelTestPage.class);
		tester.assertRenderedPage(DatePanelTestPage.class);

		FormTester form = tester.newFormTester("form");
		form.setValue("localdate:date", "2018-01-01");
		form.setValue("date:date", "2018-01-01");

		form.submit();

		tester.assertNoErrorMessage();
		tester.assertRenderedPage(DatePanelTestPage.class);

		form = tester.newFormTester("form");
		form.setValue("localdate:date", "2018/01/01");
		form.setValue("date:date", "2018-01-01");

		form.submit();

		tester.assertErrorMessages("The value of 'date' is not a valid LocalDate.");
		tester.assertRenderedPage(DatePanelTestPage.class);

		form = tester.newFormTester("form");
		form.setValue("localdate:date", "2018-01-01");
		form.setValue("date:date", "2018/01/01");
		form.submit();

		tester.assertErrorMessages("The value of 'date' is not a valid Date.");
	}

	@Test
	public void testInvalidDateType() {
		WicketTester tester = new WicketTester();

		WicketRuntimeException ex = assertThrows(WicketRuntimeException.class, () -> tester.startPage(InvalidDatePanelTestPage.class));

		assertThat(ex, new ExceptionCauseAndMessageMatcher(IllegalArgumentException.class, "Cannot display type java.time.ZonedDateTime as date"));
	}
}
