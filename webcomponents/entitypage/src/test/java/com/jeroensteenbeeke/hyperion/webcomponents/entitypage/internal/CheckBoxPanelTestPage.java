package com.jeroensteenbeeke.hyperion.webcomponents.entitypage.internal;

import com.jeroensteenbeeke.hyperion.webcomponents.entitypage.EmptyNavComponentProvider;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.model.Model;

public class CheckBoxPanelTestPage extends WebPage {

	private static final long serialVersionUID = 1659067460840097952L;

	public CheckBoxPanelTestPage() {
		CheckBoxPanel panel = new CheckBoxPanel("test", Model.of(true));
		panel.setLabel(Model.of("This is a test"));
		add(panel);

		// Because we're not testing this anywhere else and it's trivial
		add(new EmptyNavComponentProvider().createNavComponent("nav"));
	}
}
