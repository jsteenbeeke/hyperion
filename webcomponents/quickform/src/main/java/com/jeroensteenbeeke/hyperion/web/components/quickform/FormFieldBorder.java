/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.web.components.quickform;

import com.jeroensteenbeeke.hyperion.annotation.Annotations;
import com.jeroensteenbeeke.hyperion.web.components.quickform.annotation.FormField;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.border.Border;

/**
 * Border for form fields
 */
public class FormFieldBorder extends Border {

	private static final long serialVersionUID = 1L;

	private final String componentMarkupId;

	private final String label;

	private final boolean localized;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param component The target component
	 * @param annotations Any annotations defined on the target
	 */
	public FormFieldBorder(String id, Component component,
						   Annotations annotations) {
		super(id);

		component.setOutputMarkupId(true);

		FormField formField = annotations.getAnnotation(FormField.class).orElseThrow(() -> new IllegalStateException(
				"FormFieldBorder should only be used on FormFields"));

		this.componentMarkupId = component.getMarkupId();
		this.label = formField.label();
		this.localized = formField.localized();

	}

	@Override
	protected void onInitialize() {
		super.onInitialize();

		String lbl = localized ? getString(label) : label;

		addToBorder(new Label("label", lbl).add(AttributeModifier.replace(
				"for", componentMarkupId)));
	}

}
