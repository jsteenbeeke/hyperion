package com.jeroensteenbeeke.hyperion.webcomponents.core.repeaters;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.border.Border;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

import org.jetbrains.annotations.NotNull;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PartitionedViewTestPage extends WebPage {
	private static final long serialVersionUID = 6967972061258018129L;

	public PartitionedViewTestPage() {
		IModel<List<String>> letters = () -> IntStream
				.iterate(97, i -> i <= 122, i -> i + 1)
				.mapToObj(i -> Character.toString((char) i))
				.collect(Collectors.toList());

		add(new PartitionedView<>("customized", letters, 4) {
			private static final long serialVersionUID = 2800590920022055437L;

			@Override
			protected Panel createItemPanel(@NotNull String id, @NotNull IModel<String> model) {
				return new LabelPanel(id, model);
			}

			@Override
			public Border createPartitionBorder(@NotNull String id, @NotNull IModel<List<String>> model) {
				return new LetterGroupBorder(id);
			}

		});
	}
}
