package com.jeroensteenbeeke.hyperion.webcomponents.core.repeaters;

import org.apache.wicket.util.tester.WicketTester;
import org.junit.jupiter.api.Test;

public class PartitionedViewTest {
	@Test
	public void testPartitionComponent() {
		WicketTester tester = new WicketTester();
		tester.getApplication().getMarkupSettings().setStripWicketTags(true);

		tester.startPage(new PartitionedViewTestPage());
		tester.assertRenderedPage(PartitionedViewTestPage.class);

		tester.assertContains("<p class=\"letterGroup\">\\s*<span class=\"letter\">a</span>\\s*<span class=\"letter\">b</span>\\s*<span class=\"letter\">c</span>\\s*<span class=\"letter\">d</span>\\s*</p>");
		tester.assertContains("<p class=\"letterGroup\">\\s*<span class=\"letter\">y</span>\\s*<span class=\"letter\">z</span>\\s*</p>");

		tester.assertContainsNot("<span class=\"letter\">a</span>\\s*<span class=\"letter\">b</span>\\s*<span class=\"letter\">c</span>\\s*<span class=\"letter\">d</span>\\s*<span class=\"letter\">e</span>\\s*<");

	}
}
