package com.jeroensteenbeeke.hyperion.webcomponents.core;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.IModel;

public class TestTypedPanel<T> extends TypedPanel<T> {
	public TestTypedPanel(String id, IModel<T> model) {
		super(id, model);

		add(new Label("label", id));
	}

	public TestTypedPanel(String id) {
		super(id);

		add(new Label("label", id));
	}
}
