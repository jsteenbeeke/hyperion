package com.jeroensteenbeeke.hyperion.webcomponents.core;


import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;

import java.util.List;

public class ComponentsTestPage extends WebPage {
	private static final long serialVersionUID = -6517201805469705545L;
	ListView<String> items;

	public ComponentsTestPage() {
		add(items = new ListView<String>("items", List.of("A", "B", "C", "D")) {

			private static final long serialVersionUID = -6505980082796216249L;

			@Override
			protected void populateItem(ListItem<String> item) {
				item.add(new Label("label", item.getModelObject()));
			}
		});
	}
}
