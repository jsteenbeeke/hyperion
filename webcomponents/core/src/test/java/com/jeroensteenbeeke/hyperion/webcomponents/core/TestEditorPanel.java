package com.jeroensteenbeeke.hyperion.webcomponents.core;

import org.apache.wicket.markup.html.form.NumberTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.Model;

import java.io.Serializable;

public class TestEditorPanel extends EditorPanel<BarFoo> {
	private static final long serialVersionUID = -1911431117856732832L;
	private final TextField<String> nameField;
	private final NumberTextField<Integer> cardinalityField;
	private final TextField<String> factorField;

	public TestEditorPanel(String id, BarFoo barFoo) {
		super(id, Model.of(barFoo));

		nameField = new TextField<>("name", field(String.class)
				.withGetter(BarFoo::getName)
				.withSetter(BarFoo::setName));
		factorField = new TextField<>("factor", computed(String.class, FooFactor.class)
				.withGetter(BarFoo::getFactor)
				.transformedBy(FooFactor::name)
				.withSetter(BarFoo::setFactor)
				.transformedBy(FooFactor::valueOf));
		cardinalityField = new NumberTextField<>("cardinality", field(Integer.class)
				.withGetter(BarFoo::getCardinality)
				.withSetter(BarFoo::setCardinality));

		add(nameField);
		add(cardinalityField);
		add(factorField);
	}
}

enum FooFactor {
	HIGH, MEDIUM, LOW;
}

class BarFoo implements Serializable {
	private static final long serialVersionUID = 5726246581502520590L;

	private String name;

	private FooFactor factor;

	private Integer cardinality;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCardinality() {
		return cardinality;
	}

	public void setCardinality(Integer cardinality) {
		this.cardinality = cardinality;
	}

	public FooFactor getFactor() {
		return factor;
	}

	public void setFactor(FooFactor factor) {
		this.factor = factor;
	}
}
