package com.jeroensteenbeeke.hyperion.webcomponents.core.repeaters;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

public class LabelPanel extends Panel {
	private static final long serialVersionUID = 4870563894443161619L;

	public LabelPanel(String id, IModel<String> model) {
		super(id, model);

		add(new Label("letter", model));
	}
}
