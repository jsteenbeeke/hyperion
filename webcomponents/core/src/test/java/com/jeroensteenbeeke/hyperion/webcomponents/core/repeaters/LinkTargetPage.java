package com.jeroensteenbeeke.hyperion.webcomponents.core.repeaters;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;

public class LinkTargetPage extends WebPage {
	public LinkTargetPage(String input) {
		add(new Label("label", input));
	}
}
