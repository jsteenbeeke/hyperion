package com.jeroensteenbeeke.hyperion.webcomponents.core.form;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.WebPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serial;
import java.math.BigDecimal;


public class CurrencyTextFieldTestPage extends WebPage {
	private static final Logger log = LoggerFactory.getLogger(CurrencyTextFieldTestPage.class);

	@Serial
	private static final long serialVersionUID = 8313253187639138568L;

	public CurrencyTextFieldTestPage() {
		CurrencyTextField<BigDecimal> field;
		add(field = new CurrencyTextField<>("currency",
											CurrencyTextFieldTest.FIVE_POINT_FIVE, BigDecimal::valueOf));

		field.add(new AjaxOnInputChangedBehavior() {
			@Serial
			private static final long serialVersionUID = -3623558265232873530L;

			@Override
			protected void onInputChanged(AjaxRequestTarget target) {
				log.info("Input changed!");
			}
		});
	}
}
