package com.jeroensteenbeeke.hyperion.webcomponents.core.repeaters;


import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;

import java.util.List;

public class ClickableListViewTestPage extends WebPage {
	public ClickableListViewTestPage() {
		add(new ClickableListView<>("items", List.of("A", "B", "C")) {
			@Override
			protected void populateItem(ListItem<String> item) {
				item.add(new Label("label", item.getModelObject()));
			}

			@Override
			protected void onClick(ListItem<String> item) {
				ClickableListViewTestPage.this.setResponsePage(new LinkTargetPage(item.getModelObject()));
			}
		});
	}
}
