package com.jeroensteenbeeke.hyperion.webcomponents.core.form;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.model.Model;

import java.time.LocalTime;

public class TimeTextFieldTestPage extends WebPage {
	public TimeTextFieldTestPage() {
		add(new TimeTextField<>("time",
				Model.of(LocalTime.now()), LocalTime.class));
	}
}
