package com.jeroensteenbeeke.hyperion.webcomponents.core.repeaters;

import org.apache.wicket.markup.html.border.Border;

public class LetterGroupBorder extends Border {
	private static final long serialVersionUID = -5297078357117524465L;

	public LetterGroupBorder(String id) {
		super(id);
	}
}
