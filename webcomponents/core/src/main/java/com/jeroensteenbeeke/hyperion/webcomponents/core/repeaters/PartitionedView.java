package com.jeroensteenbeeke.hyperion.webcomponents.core.repeaters;

import com.jeroensteenbeeke.hyperion.webcomponents.core.TypedPanel;
import org.apache.wicket.markup.html.border.Border;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Panel that functions like a ListView, but displays all list elements in a partitioned fashion, such as 4 per row,
 * and allows the implementing class to provide custom markup for the surrounding bits using Borders.
 *
 * @param <T> The type of element in the list
 */
public abstract class PartitionedView<T> extends TypedPanel<List<List<T>>> {
	private static final long serialVersionUID = 3488355052164151676L;

	/**
	 * Constructor
	 *
	 * @param id            The Wicket ID
	 * @param model         The model containing the original unpartitioned list
	 * @param partitionSize The partition size: the number of items per partition
	 */
	public PartitionedView(@NotNull String id, @NotNull IModel<List<T>> model, int partitionSize) {
		super(id, model.map(
				l -> io.vavr.collection.List.ofAll(l).sliding(partitionSize, partitionSize).map(io.vavr.collection.List::toJavaList)
						.toJavaList()));

		add(new ListView<>("partitions", getModel()) {
			private static final long serialVersionUID = 329162992418559418L;

			@Override
			protected void populateItem(ListItem<List<T>> item) {
				Border partitionBorder = createPartitionBorder("partitionBorder", item.getModel());
				item.add(partitionBorder);
				partitionBorder.add(new ListView<>("items", item.getModel()) {

					private static final long serialVersionUID = 8284608114836252844L;

					@Override
					protected void populateItem(ListItem<T> innerItem) {
						innerItem.add(PartitionedView.this.createItemPanel("itemPanel", innerItem.getModel()));
					}
				});
			}
		});

	}

	/**
	 * Creates a panel to display a single item from the list
	 *
	 * @param id    The ID of the panel
	 * @param model The model containing the single item
	 * @return A panel displaying the item
	 */
	protected abstract Panel createItemPanel(@NotNull String id, @NotNull IModel<T> model);

	/**
	 * Creates a border enclosing a single partition (a row)
	 *
	 * @param id    The ID for the border
	 * @param model The model containing the items in the partition
	 * @return A border enclosing the items
	 */
	public abstract Border createPartitionBorder(@NotNull String id, @NotNull IModel<List<T>> model);
}
