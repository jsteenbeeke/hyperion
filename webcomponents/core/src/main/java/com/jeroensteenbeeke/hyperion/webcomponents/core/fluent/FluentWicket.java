package com.jeroensteenbeeke.hyperion.webcomponents.core.fluent;

import org.apache.wicket.Component;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.danekja.java.misc.serializable.SerializableRunnable;
import org.danekja.java.util.function.serializable.SerializableConsumer;

import org.jetbrains.annotations.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * Fluent methods for constructing various Wicket components
 */
public class FluentWicket {
	/**
	 * Fluent API for creating a Wicket DataView
	 * @param dataProvider The dataprovider to display
	 * @param <T> The type of object the dataprovider returns
	 * @return The next builder step
	 */
	public static <T> WithItemHandler<Item<T>, DataView<T>> dataView(@NotNull IDataProvider<T> dataProvider) {
		return itemHandler -> id -> new DataView<>(id, dataProvider) {
			@Override
			protected void populateItem(Item<T> item) {
				itemHandler.accept(item);
			}
		};
	}

	/**
	 * Fluent API for creating a Wicket ListView
	 * @param listModel The model containing the list for the view
	 * @param <T> The type of element in the list
	 * @return The next builder step
	 */
	public static <T> WithItemHandler<ListItem<T>, ListView<T>> listView(@NotNull IModel<List<T>> listModel) {
		return itemHandler -> id -> new ListView<>(id, listModel) {
			@Override
			protected void populateItem(ListItem<T> item) {
				itemHandler.accept(item);
			}
		};
	}

	/**
	 * Fluent API for creating a link without a Model and no custom label
	 * @param onClick The logic to run on click
	 * @return The next builder step
	 */
	public static WithWicketId<Link<Void>> link(@NotNull SerializableRunnable onClick) {
		return id -> new Link<>(id) {

			@Override
			public void onClick() {
				onClick.run();
			}
		};
	}

	/**
	 * Fluent API for creating a link without a Model and no custom label
	 * @param model The model for the link
	 * @param <T> The type of model object
	 * @return The next builder step
	 */
	public static <T> WithItemHandler<T,Link<T>> link(@NotNull IModel<T> model) {
		return itemHandler -> id -> new Link<>(id, model) {

			@Override
			public void onClick() {
				itemHandler.accept(getModelObject());
			}
		};
	}

	/**
	 * Fluent API for creating a link without a Model and no custom label
	 * @param model The model for the link
	 * @param <T> The type of model object
	 * @return The next builder step
	 */
	public static <T> WithBody<T,Link<T>> labeledLink(@NotNull IModel<T> model) {
		return labelModel -> itemHandler -> id -> {
			Link<T> link = new Link<>(id, model) {

				@Override
				public void onClick() {
					itemHandler.accept(getModelObject());
				}
			};

			link.setBody(labelModel);

			return link;
		};
	}

	/**
	 * Builder step for setting a link body model
	 * @param <L> The type of contained item
	 * @param <C> The type of component
	 */
	public interface WithBody<L, C extends Component> extends Serializable {
		/**
		 * Specifies the model for the link body
		 * @param bodyModel The body model
		 * @return The next builder step
		 */
		WithItemHandler<L, C> withBody(@NotNull IModel<?> bodyModel);
	}

	/**
	 * Builder step for adding logic to populate the item
	 * @param <L> The type of contained item
	 * @param <C> The type of component
	 */
	public interface WithItemHandler<L, C extends Component> extends Serializable {
		/**
		 * Specifies the logic on item population
		 * @param itemHandler The item handler
		 * @return The next builder step
		 */
		WithWicketId<C> withItemHandler(@NotNull SerializableConsumer<L> itemHandler);
	}

	/**
	 * Builder step for specifying the Wicket id
	 * @param <C> The type of component
	 */
	public interface WithWicketId<C extends Component> extends Serializable {
		/**
		 * Specifies the Wicket ID and constructs the component
		 * @param wicketId The component
		 * @return The constructed component
		 */
		C withWicketId(@NotNull String wicketId);
	}
}
