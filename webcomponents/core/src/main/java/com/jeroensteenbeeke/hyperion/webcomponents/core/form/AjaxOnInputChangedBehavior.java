package com.jeroensteenbeeke.hyperion.webcomponents.core.form;

import org.apache.wicket.Application;
import org.apache.wicket.Component;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.ajax.AbstractDefaultAjaxBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormChoiceComponentUpdatingBehavior;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.form.AbstractTextComponent;
import org.apache.wicket.markup.html.form.FormComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serial;
import java.time.Duration;

/**
 * Ajax behavior for updating a text form component a number of seconds (default: 5) after input
 * ceases.
 */
public abstract class AjaxOnInputChangedBehavior extends AbstractDefaultAjaxBehavior {
	@Serial
	private static final long serialVersionUID = -2449228724390337670L;

	private static final Logger log = LoggerFactory.getLogger(AjaxOnInputChangedBehavior.class);

	@Override
	protected void onBind() {
		super.onBind();

		Component component = getComponent();
		if (!(component instanceof FormComponent)) {
			throw new WicketRuntimeException("Behavior " + getClass().getName()
													 + " can only be added to an instance of a FormComponent");
		}

		checkComponent((FormComponent<?>) component);
	}

	@Override
	public void renderHead(Component component, IHeaderResponse response) {
		super.renderHead(component, response);

		response.render(JavaScriptHeaderItem.forReference(AjaxOnInputChangedBehaviorJSReference.get()));
		response.render(OnDomReadyHeaderItem.forScript("""
															   $('#%1$s').keyup(function(e) {
															   	   onInputCeased('%1$s', %2$d, function() {
															   	   		%3$s
															   	   });
															   });
															   															   
															   $('#%1$s').keydown(function(e) {
															   	 onResumeInput('%1$s');
															   });
															   															   
															   """.formatted(component.getMarkupId(), getTimeout().toMillis(), getCallbackScript(component))
		));
	}

	/**
	 * Sets the timeout after which the updated textfield should be submitted
	 *
	 * @return The timeout (default: 5 seconds)
	 */
	protected Duration getTimeout() {
		return Duration.ofSeconds(5L);
	}

	@Override
	protected void respond(AjaxRequestTarget target) {
		onInputChanged(target);
	}

	/**
	 * Called when the input is changed
	 *
	 * @param target The request target
	 */
	protected abstract void onInputChanged(AjaxRequestTarget target);

	/**
	 * Check the component this behavior is bound to.
	 * <p>
	 * Logs a warning in development mode when an {@link AjaxFormChoiceComponentUpdatingBehavior}
	 * should be used.
	 *
	 * @param component bound component
	 */
	protected void checkComponent(FormComponent<?> component) {
		if (Application.get().usesDevelopmentConfig()
				&& !appliesTo(component)) {
			log.warn(String
							 .format(
									 "AjaxOnInputChangedBehavior is not supposed to be added in the form component at path: \"%s\". "
											 + "It is only meant to be used with TextAreas and TextFields",
									 component.getPageRelativePath()));
		}
	}

	private boolean appliesTo(FormComponent<?> component) {
		return component instanceof AbstractTextComponent;
	}
}
