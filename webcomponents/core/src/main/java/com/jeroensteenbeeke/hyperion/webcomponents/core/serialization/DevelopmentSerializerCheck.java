package com.jeroensteenbeeke.hyperion.webcomponents.core.serialization;

import org.apache.wicket.core.util.objects.checker.CheckingObjectOutputStream;
import org.apache.wicket.core.util.objects.checker.NotDetachedModelChecker;
import org.apache.wicket.core.util.objects.checker.OrphanComponentChecker;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Object outputstream that checks serializability
 */
public class DevelopmentSerializerCheck extends CheckingObjectOutputStream {

	/**
	 * Constructor
	 * @throws IOException If initialization fails
	 */
    public DevelopmentSerializerCheck() throws IOException {
        super(new ByteArrayOutputStream(), new OrphanComponentChecker(), new NotDetachedModelChecker(), new EntitySerializationNotAllowedChecker());
    }
}
