let ajaxOnInputChangedTimers = {};

function onInputCeased(id, timeout, onTimeoutExceeded) {
    if (!ajaxOnInputChangedTimers.hasOwnProperty(id)) {
        ajaxOnInputChangedTimers[id] = setTimeout(function() {
            onTimeoutExceeded();
            delete ajaxOnInputChangedTimers[id];
        }, timeout);
    }
}

function onResumeInput(id) {
    if (ajaxOnInputChangedTimers.hasOwnProperty(id)) {
        clearTimeout(ajaxOnInputChangedTimers[id]);
        delete ajaxOnInputChangedTimers[id];
    }
}
