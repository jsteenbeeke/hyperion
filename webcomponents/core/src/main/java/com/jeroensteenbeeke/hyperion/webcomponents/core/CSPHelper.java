package com.jeroensteenbeeke.hyperion.webcomponents.core;

import com.jeroensteenbeeke.lux.TypedResult;
import org.apache.wicket.csp.CSPDirective;
import org.apache.wicket.protocol.http.WebApplication;

import org.jetbrains.annotations.NotNull;
import jakarta.servlet.ServletContext;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.function.Function;

/**
 * Helper class for generating CSP directives for external reosurces
 */
public class CSPHelper {
	/**
	 * Reads hashes from a file and adds the hashes from that file as CSP
	 * directives. If the hashes are not prefixed sha256-, sha384- or sha512- the code will assume they
	 * are sha384 hashes and add this prefix
	 * @param directive The directive type to read hashes for
	 * @return A builder
	 */
	public static RelativePathStep allow(CSPDirective directive) {
		return file -> application -> {
			ServletContext servletContext = application.getServletContext();

			String path = file.startsWith("/") ? file : "/" + file;

			try (InputStream resource = servletContext.getResourceAsStream(path); ByteArrayOutputStream out = new ByteArrayOutputStream()) {

				if (resource == null) {
					return TypedResult.fail("Invalid file. Path %s does not exist relative to servlet context", file);
				}

				int i;
				while ((i = resource.read()) != -1) {
					out.write(i);
				}
				out.flush();

				MessageDigest digest = MessageDigest.getInstance("SHA-384");
				byte[] checksum = digest.digest(out.toByteArray());

				String base64 = new String(Base64
												   .getEncoder()
												   .encode(checksum), StandardCharsets.UTF_8);

				application
						.getCspSettings()
						.blocking()
						.add(directive, String.format("'sha384-%s'", base64));

				return TypedResult.ok(String.format("'sha384-%s'", base64));
			} catch (IOException | NoSuchAlgorithmException e) {
				application.getCspSettings().blocking().add(directive, file);
				return TypedResult.fail("Unable to read path %s relative to servletContext: %s", file, e.getMessage());
			}
		};
	}

	/**
	 * Step for specifying the relative path
	 */
	@FunctionalInterface
	public interface RelativePathStep extends Function<String,ApplicationStep> {
		/**
		 * Specifies the path from which to load the hashes
		 * @param path The path
		 * @return The next builder step
		 */
		@NotNull
		default ApplicationStep fromPath(@NotNull String path) {
			return apply(path);
		}
	}

	/**
	 * Step for specifying the servlet context, and returning the action result
	 */
	@FunctionalInterface
	public interface ApplicationStep extends Function<WebApplication, TypedResult<String>> {
		/**
		 * Specifies the application
		 * @param application The application
		 * @return An ActionResult indicating success or failure
		 */
		@NotNull
		default TypedResult<String> inApplication(@NotNull WebApplication application) {
			return apply(application);
		}
	}
}
