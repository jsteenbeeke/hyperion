package com.jeroensteenbeeke.hyperion.webcomponents.core.form;

import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;

/**
 * TextField of type "time"
 * @param <T> The type of object encapsulated within the TextField
 */
public class TimeTextField<T> extends TextField<T> {
	/**
	 * Constructor
	 * @param id The component ID from the HTML (wicket:id)
	 * @param model The model
	 * @param type The type of object
	 */
	public TimeTextField(String id, IModel<T> model, Class<T> type) {
		super(id, model, type);
	}

	@Override
	protected String[] getInputTypes() {
		return new String[] {"time"};
	}
}
