/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.core;

import org.apache.wicket.IGenericComponent;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Extension of the basic Wicket panel with a generic type, which indicates the type of modelObject
 * @param <T> The type of ModelObject
 */
public abstract class TypedPanel<T> extends Panel implements IGenericComponent<T,TypedPanel<T>> {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 * @param id The component ID from the HTML (wicket:id)
	 * @param model The model
	 */
	public TypedPanel(String id, IModel<T> model) {
		super(id, model);
	}

	/**
	 * Constructor
	 * @param id The component ID from the HTML (wicket:id)
	 */
	public TypedPanel(String id) {
		super(id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T getModelObject() {
		return (T) getDefaultModelObject();
	}

	@SuppressWarnings("unchecked")
	@Override
	public IModel<T> getModel() {
		return (IModel<T>) getDefaultModel();
	}

	@Override
	@SuppressWarnings("unchecked")
	public TypedPanel<T> setModel(IModel<T> model) {
		return (TypedPanel<T>) setDefaultModel(model);

	}

	@Override
	@SuppressWarnings("unchecked")
	public TypedPanel<T> setModelObject(T modelObject) {
		return (TypedPanel<T>) setDefaultModelObject(modelObject);
	}
}
