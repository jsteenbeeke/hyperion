package com.jeroensteenbeeke.hyperion.webcomponents.core.serialization;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import org.apache.wicket.core.util.objects.checker.AbstractObjectChecker;

/**
 * Checker that ensures that no entities are part of serializable data
 */
public class EntitySerializationNotAllowedChecker extends AbstractObjectChecker {

    @Override
    protected Result doCheck(Object object) {
        if (object instanceof DomainObject obj) {
			if (obj.isSaved()) {
				return new Result(Result.Status.FAILURE, "entity serialization not allowed");
			}
		}
        return Result.SUCCESS;
    }
}

