/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.core.repeaters;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.IRequestListener;
import org.apache.wicket.core.request.handler.ListenerRequestHandler;
import org.apache.wicket.core.request.handler.PageAndComponentProvider;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

/**
 * ListItem implementation that adds click logic to the item
 * @param <T> The type of object contained in the item
 */
class LinkItem<T> extends ListItem<T> implements IRequestListener {
	private static final long serialVersionUID = 1L;

	private ClickableListView<T> view;

	/**
	 * Creates a new LinkItem for the given view
	 * @param view The view that contains the LinkItem
	 * @param index The index within the LinkItem
	 * @param model The model of the item
	 */
	LinkItem(ClickableListView<T> view, int index, IModel<T> model) {
		super(index, model);
		this.view = view;
	}

	@Override
	public void onRequest() {
		view.onClick(this);
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();

		add(new AttributeModifier("onclick", Model.of(String.format(
				"javascript:location.href='%s'",
				urlFor(new ListenerRequestHandler(getProvider()))))) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isEnabled(Component component) {
				return super.isEnabled(component)
						&& component.isEnableAllowed() && component.isEnabled();
			}
		});
	}

	private PageAndComponentProvider getProvider() {
		return new PageAndComponentProvider(getPage(), getPageRelativePath());
	}


}
