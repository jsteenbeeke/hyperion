package com.jeroensteenbeeke.hyperion.webcomponents.core.form;

import org.apache.wicket.markup.head.HeaderItem;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.resource.JavaScriptResourceReference;

import java.io.Serial;
import java.util.List;

/**
 * JS reference for the on input changed timer logic
 */
public class AjaxOnInputChangedBehaviorJSReference extends JavaScriptResourceReference {
	@Serial
	private static final long serialVersionUID = -2706629535328531990L;

	private static final AjaxOnInputChangedBehaviorJSReference instance =
			new AjaxOnInputChangedBehaviorJSReference();

	private AjaxOnInputChangedBehaviorJSReference() {
		super(AjaxOnInputChangedBehaviorJSReference.class, "js/ajax.on.input.changed.behavior.js");
	}

	/**
	 * Get the static instance
	 * @return The instance
	 */
	public static AjaxOnInputChangedBehaviorJSReference get() {
		return instance;
	}

	@Override
	public List<HeaderItem> getDependencies() {
		List<HeaderItem> dependencies = super.getDependencies();

		dependencies.add(JavaScriptHeaderItem.forReference(WebApplication.get().getJavaScriptLibrarySettings().getJQueryReference()));
		dependencies.add(JavaScriptHeaderItem.forReference(WebApplication.get().getJavaScriptLibrarySettings().getWicketAjaxReference()));

		return dependencies;
	}
}
