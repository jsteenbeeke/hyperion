/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.sitemap;

import com.jeroensteenbeeke.hyperion.util.XMLable;

/**
 * Represents a priority as specified by the sitemaps specification. Corresponds to a value between 0.0 and 1.0
 *
 * Lowest &lt;-&gt; Lower &lt;-&gt; Low &lt;-&gt; Less &lt;-&gt; Below Normal
 * &lt;-&gt; Normal &lt;-&gt; Above Normal &lt;-&gt; More &lt;-&gt; High
 * &lt;-&gt; Higher &lt;-&gt; Highest
 */
public enum Priority implements XMLable {
	/**
	 * 0.0
	 */
	LOWEST("0.0"),
	/**
	 * 0.1
	 */
	LOWER("0.1"),
	/**
	 * 0.2
	 */
	LOW("0.2"),
	/**
	 * 0.3
	 */
	LESS("0.3"),
	/**
	 * 0.4
	 */
	BELOW_NORMAL("0.4"),
	/**
	 * 0.5
	 */
	NORMAL("0.5"),
	/**
	 * 0.6
	 */
	ABOVE_NORMAL("0.6"),
	/**
	 * 0.7
	 */
	MORE("0.7"),
	/**
	 * 0.8
	 */
	HIGH("0.8"),
	/**
	 * 0.9
	 */
	HIGHER("0.9"),
	/**
	 * 1.0
	 */
	HIGHEST("1.0");

	private final String priorityString;

	Priority(String priorityString) {
		this.priorityString = priorityString;
	}

	@Override
	public String toXmlValue() {
		return priorityString;
	}
}
