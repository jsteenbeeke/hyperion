/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.sitemap;

import org.jetbrains.annotations.NotNull;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Entry within a sitemap
 */
public class SitemapEntry implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	private final String url;

	private LocalDateTime lastModification = null;

	private UpdateFrequency changeFrequency = null;

	private Priority priority = null;

	/**
	 * Creates a new entry
	 * @param url The URL
	 */
	public SitemapEntry(@NotNull String url) {
		this.url = url;
	}

	/**
	 * Sets the last modified time
	 * @param dateTime The last changed time
	 * @return This entry
	 */
	public SitemapEntry lastModifiedOn(@NotNull LocalDateTime dateTime) {
		this.lastModification = dateTime;
		return this;
	}

	/**
	 * Sets the change frequency
	 * @param freq The frequency with which this entry is updated
	 * @return This entry
	 */
	public SitemapEntry withChangeFrequency(@NotNull UpdateFrequency freq) {
		this.changeFrequency = freq;
		return this;
	}

	/**
	 * Sets the priority
	 * @param priority The priority
	 * @return This entry
	 */
	public SitemapEntry withPriority(@NotNull Priority priority) {
		this.priority = priority;
		return this;
	}

	@NotNull
	String getUrl() {
		return url;
	}

	@NotNull
	Optional<LocalDateTime> getLastModification() {
		return Optional.ofNullable(lastModification);
	}

	@NotNull
	Optional<UpdateFrequency> getChangeFrequency() {
		return Optional.ofNullable(changeFrequency);
	}

	@NotNull
	Optional<Priority> getPriority() {
		return Optional.ofNullable(priority);
	}

}
