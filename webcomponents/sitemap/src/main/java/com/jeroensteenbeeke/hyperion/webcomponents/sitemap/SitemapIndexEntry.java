/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.webcomponents.sitemap;

import org.jetbrains.annotations.NotNull;

import java.io.Serial;
import java.io.Serializable;
import java.time.Instant;

/**
 * Entry in a sitemap index
 */
public final class SitemapIndexEntry implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;

	private final String url;

	private final Instant lastModification;

	/**
	 * Constructor
	 * @param url The URL of the sitemap
	 * @param lastModification The last modified time
	 */
	public SitemapIndexEntry(
			@NotNull String url,
			@NotNull Instant lastModification) {
		this.url = url;
		this.lastModification = lastModification;
	}

	/**
	 * The time of the last modification
	 * @return The last modification
	 */
	@NotNull
	public Instant getLastModification() {
		return lastModification;
	}

	/**
	 * Gets the URL of the entry
	 * @return The entry URL
	 */
	@NotNull
	public String getUrl() {
		return url;
	}
}
