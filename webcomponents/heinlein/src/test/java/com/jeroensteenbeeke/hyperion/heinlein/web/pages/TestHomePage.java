package com.jeroensteenbeeke.hyperion.heinlein.web.pages;

import com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.annotation.NavbarItem;
import com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.annotation.NavbarPosition;
import org.apache.wicket.markup.html.WebPage;

import java.io.Serial;

import static com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.SmartNavbar.smartNavbar;

@NavbarItem(name = "Home")
public class TestHomePage extends WebPage {
	@Serial
	private static final long serialVersionUID = 8528031088153926528L;

	public TestHomePage() {
		add(smartNavbar("nav").withoutBrand().andLightStyle());
	}
}
