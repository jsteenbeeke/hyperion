package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.Component;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class ModalWindowTest {
	@Test
	public void testModalWindow() {
		WicketTester tester = new WicketTester();

		tester.startPage(ModalWindowTestPage.class);
		tester.assertRenderedPage(ModalWindowTestPage.class);
		tester.assertContains("Refresh Time");
		tester.assertContains("Your mom");
		tester.assertContains("FooBar");
		tester.assertContains("Other page");
		tester.assertContains("class=\"btn\\s*btn-dark\"");
		tester.assertContains("class=\"btn\\s*btn-primary\"");
		tester.assertContains("class=\"btn\\s*btn-info\"");
		tester.assertContains("class=\"btn\\s*btn-danger\"");

		tester.assertContains("Submit A");
		tester.assertContains("Submit B");
		tester.assertContains("Submit C");

		Component component = tester.getComponentFromLastRenderedPage("a");
		assertTrue(component instanceof BootstrapModalWindow);

		BootstrapModalWindow window = (BootstrapModalWindow) component;

		assertNotNull(window.getDialog());
		assertNotNull(window.getDocument());
		assertNotNull(window.getContent());
		assertNotNull(window.getHeader());
		assertNotNull(window.getBody());
		assertNotNull(window.getFooter());
	}

	@Test
	public void testIncompleteButtons() {
		WicketTester tester = new WicketTester();

		IllegalStateException ex = assertThrows(IllegalStateException.class, () -> tester.startPage(ModalWindowTestInvalidPage.class));

		assertThat(ex.getMessage(), equalTo("There are 7 unfinished button creation chains."));
	}
}
