package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.markup.html.WebPage;

public class ModalWindowTestInvalidPage extends WebPage {

	public ModalWindowTestInvalidPage() {

		add(new TestWindowB("b"));
	}
}
