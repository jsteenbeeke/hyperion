package com.jeroensteenbeeke.hyperion.heinlein.web.pages;

import com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.annotation.InSubmenu;
import com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.annotation.NavbarItem;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.Link;

@NavbarItem(name = "Confirmation")
@InSubmenu(key = "user")
public class ConfirmationTriggerPage extends WebPage {
	public ConfirmationTriggerPage(ConfirmationPage.ColorScheme scheme) {
		add(new Link<Void>("confirm") {
			@Override
			public void onClick() {
				setResponsePage(new ConfirmationPage("Proceed?", "Yes or no?", scheme, answer ->
						setResponsePage(answer ? new YesAnswerPage
						() : new NoAnswerPage())));
			}
		});
	}
}
