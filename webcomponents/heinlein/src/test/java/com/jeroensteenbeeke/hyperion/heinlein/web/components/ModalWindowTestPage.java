package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.markup.html.WebPage;

public class ModalWindowTestPage extends WebPage {

	public ModalWindowTestPage() {

		add(new TestWindowA("a"));
		add(new TestWindowC("c"));

	}
}
