package com.jeroensteenbeeke.hyperion.heinlein.web.components;


import com.jeroensteenbeeke.hyperion.webcomponents.core.form.choice.LambdaRenderer;
import org.apache.wicket.markup.html.WebPage;

import java.util.List;

public class IncorrectBasicMultiSelectTestPage extends WebPage {
	public IncorrectBasicMultiSelectTestPage() {
		add(new BasicMultiSelectPanel<>("basic",
				"Basic Multiselect",
				List.of("A", "B", "E", "F"),
				List.of("A", "B", "C", "D", "E"),
				LambdaRenderer.of(a -> a)
		));
	}
}
