package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.feedback.FeedbackMessage;
import org.apache.wicket.feedback.IFeedbackMessageFilter;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.Link;

public class BootstrapFeedbackPanelTestPage extends WebPage {
	private static final long serialVersionUID = -7186195425641250754L;

	public BootstrapFeedbackPanelTestPage() {
		add(new BootstrapFeedbackPanel("feedback"));
		add(new BootstrapFeedbackPanel("feedback2", (IFeedbackMessageFilter) message -> false));

		add(new Link<Void>("info") {
			private static final long serialVersionUID = -5685761498942138995L;

			@Override
			public void onClick() {
				BootstrapFeedbackPanelTestPage target = new BootstrapFeedbackPanelTestPage();
				target.info("Info");
				setResponsePage(target);
			}
		});

		add(new Link<Void>("warn") {
			private static final long serialVersionUID = 2792881182493658527L;

			@Override
			public void onClick() {
				BootstrapFeedbackPanelTestPage target = new BootstrapFeedbackPanelTestPage();
				target.warn("Warn");
				setResponsePage(target);
			}
		});

		add(new Link<Void>("error") {
			private static final long serialVersionUID = 5754010581711830791L;

			@Override
			public void onClick() {
				BootstrapFeedbackPanelTestPage target = new BootstrapFeedbackPanelTestPage();
				target.error("Error");
				setResponsePage(target);
			}
		});

		add(new Link<Void>("fatal") {
			private static final long serialVersionUID = 311113825972537845L;

			@Override
			public void onClick() {
				BootstrapFeedbackPanelTestPage target = new BootstrapFeedbackPanelTestPage();
				target.fatal("Fatal");
				setResponsePage(target);
			}
		});

		add(new Link<Void>("success") {
			private static final long serialVersionUID = 7201778524615062037L;

			@Override
			public void onClick() {
				BootstrapFeedbackPanelTestPage target = new BootstrapFeedbackPanelTestPage();
				target.success("Success");
				setResponsePage(target);
			}
		});

		add(new Link<Void>("undefined") {
			private static final long serialVersionUID = -5585817069141255756L;

			@Override
			public void onClick() {
				BootstrapFeedbackPanelTestPage target = new BootstrapFeedbackPanelTestPage();
				target.getFeedbackMessages().add(new FeedbackMessage(this, "Undefined", 1337));
				setResponsePage(target);
			}
		});

	}
}
