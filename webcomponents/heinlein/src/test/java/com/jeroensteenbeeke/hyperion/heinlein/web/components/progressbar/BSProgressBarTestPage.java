package com.jeroensteenbeeke.hyperion.heinlein.web.components.progressbar;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.model.Model;

public class BSProgressBarTestPage extends WebPage {
	public BSProgressBarTestPage() {
		add(new BSProgressBar("a", Model.of(23)));
	}

	public BSProgressBarTestPage(int percentage, BSProgressBarConfig config) {
		add(new BSProgressBar("a", Model.of(percentage), config));
	}
}
