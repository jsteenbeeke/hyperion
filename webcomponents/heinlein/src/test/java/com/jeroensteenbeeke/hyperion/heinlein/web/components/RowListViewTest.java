package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.util.tester.WicketTester;
import org.junit.jupiter.api.Test;

public class RowListViewTest {
	@Test
	public void testBootstrapPartitionedList() {
		WicketTester tester = new WicketTester();
		tester.getApplication().getMarkupSettings().setStripWicketTags(true);

		tester.startPage(new RowListViewTestPage());
		tester.assertRenderedPage(RowListViewTestPage.class);

		tester.assertContains("<div class=\"row\">\\s*<span class=\"letter\">a</span>\\s*<span class=\"letter\">b</span>\\s*<span class=\"letter\">c</span>\\s*<span class=\"letter\">d</span>\\s*</div>");
		tester.assertContains("<div class=\"row\">\\s*<span class=\"letter\">y</span>\\s*<span class=\"letter\">z</span>\\s*</div>");

		tester.assertContainsNot("<span class=\"letter\">a</span>\\s*<span class=\"letter\">b</span>\\s*<span class=\"letter\">c</span>\\s*<span class=\"letter\">d</span>\\s*<span class=\"letter\">e</span>\\s*<");

	}
}
