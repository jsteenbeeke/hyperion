package com.jeroensteenbeeke.hyperion.heinlein.web.pages;

import com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.annotation.InSubmenu;
import com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.annotation.NavbarItem;
import org.apache.wicket.markup.html.WebPage;

@NavbarItem(name = "No", priority = 1)
@InSubmenu(key = "user", precededBySeparator = true)
public class NoAnswerPage extends WebPage {
}
