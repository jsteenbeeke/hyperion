package com.jeroensteenbeeke.hyperion.heinlein.web.components.progressbar;

import com.jeroensteenbeeke.hyperion.heinlein.web.components.BSThemeColor;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.jupiter.api.Test;

class BSProgressBarTest {

	@Test
	public void canRenderBasicProgressBar() {
		WicketTester tester = new WicketTester();

		tester.startPage(BSProgressBarTestPage.class);
		tester.assertRenderedPage(BSProgressBarTestPage.class);

		tester.assertContainsNot("class=\"progress-bar bg-primary\"");
		tester.assertContains(">23%<");
		tester.assertContains("width: 23%");
		tester.assertContains("aria-valuenow=\"23\"");
	}

	@Test
	public void canRenderDefaultProgressBarWithoutLabel() {
		WicketTester tester = new WicketTester();

		tester.startPage(new BSProgressBarTestPage(88, new BSProgressBarConfig(false)));
		tester.assertRenderedPage(BSProgressBarTestPage.class);

		tester.assertContains("class=\"progress-bar\"");
		tester.assertContainsNot(">88%<");
		tester.assertContains("width: 88%");
		tester.assertContains("aria-valuenow=\"88\"");
	}
	@Test
	public void canRenderRedProgressBarWithoutLabel() {
		WicketTester tester = new WicketTester();

		tester.startPage(new BSProgressBarTestPage(88, new BSProgressBarConfig(false, BSThemeColor.Danger)));
		tester.assertRenderedPage(BSProgressBarTestPage.class);

		tester.assertContains("class=\"progress-bar bg-danger\"");
		tester.assertContainsNot(">88%<");
		tester.assertContains("width: 88%");
		tester.assertContains("aria-valuenow=\"88\"");
	}

	@Test
	public void canRenderYellowProgressBarWithStripedLabel() {
		WicketTester tester = new WicketTester();

		tester.startPage(new BSProgressBarTestPage(88, new BSProgressBarConfig(true, BSThemeColor.Warning, ProgressBarMode.Striped)));
		tester.assertRenderedPage(BSProgressBarTestPage.class);

		tester.assertContains("class=\"progress-bar progress-bar-striped bg-warning\"");
		tester.assertContains(">88%<");
		tester.assertContains("width: 88%");
		tester.assertContains("aria-valuenow=\"88\"");
	}

	@Test
	public void canRenderGreenProgressBarWithAnimatedLabel() {
		WicketTester tester = new WicketTester();

		tester.startPage(new BSProgressBarTestPage(88, new BSProgressBarConfig(true, BSThemeColor.Success, ProgressBarMode.Animated)));
		tester.assertRenderedPage(BSProgressBarTestPage.class);

		tester.assertContains("class=\"progress-bar progress-bar-striped progress-bar-animated bg-success\"");
		tester.assertContains(">88%<");
		tester.assertContains("width: 88%");
		tester.assertContains("aria-valuenow=\"88\"");
	}

	@Test
	public void canRenderPrimaryProgressBarWithAnimatedLabel() {
		WicketTester tester = new WicketTester();

		tester.startPage(new BSProgressBarTestPage(88, new BSProgressBarConfig(true, ProgressBarMode.Animated)));
		tester.assertRenderedPage(BSProgressBarTestPage.class);

		tester.assertContains("class=\"progress-bar progress-bar-striped progress-bar-animated\"");
		tester.assertContains(">88%<");
		tester.assertContains("width: 88%");
		tester.assertContains("aria-valuenow=\"88\"");
	}

	@Test
	public void sanitizesInvalidPercentages() {
		WicketTester tester = new WicketTester();

		tester.startPage(new BSProgressBarTestPage(-5, new BSProgressBarConfig(true, BSThemeColor.Success, ProgressBarMode.Animated)));
		tester.assertRenderedPage(BSProgressBarTestPage.class);

		tester.assertContains("class=\"progress-bar progress-bar-striped progress-bar-animated bg-success\"");
		tester.assertContainsNot(">0%<");
		tester.assertContains("width: 0%");
		tester.assertContains("aria-valuenow=\"0\"");

		tester.startPage(new BSProgressBarTestPage(107, new BSProgressBarConfig(true, BSThemeColor.Success, ProgressBarMode.Animated)));
		tester.assertRenderedPage(BSProgressBarTestPage.class);

		tester.assertContains("class=\"progress-bar progress-bar-striped progress-bar-animated bg-success\"");
		tester.assertContainsNot(">0%<");
		tester.assertContains("width: 0%");
		tester.assertContains("aria-valuenow=\"0\"");
	}
}
