package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

import org.jetbrains.annotations.NotNull;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RowListViewTestPage extends WebPage {
	private static final long serialVersionUID = 6967972061258018129L;

	public RowListViewTestPage() {
		IModel<List<String>> letters = () -> IntStream
				.iterate(97, i -> i <= 122, i -> i + 1)
				.mapToObj(i -> Character.toString((char) i))
				.collect(Collectors.toList());

		add(new RowListView<String>("customized", letters, 4) {
			private static final long serialVersionUID = 2800590920022055437L;

			@Override
			protected Panel createItemPanel(@NotNull String id, @NotNull IModel<String> model) {
				return new LabelPanel(id, model);
			}

		});
	}
}
