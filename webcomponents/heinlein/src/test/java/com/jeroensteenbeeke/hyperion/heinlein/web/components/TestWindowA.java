package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.Model;

public class TestWindowA extends BootstrapModalWindow {
	public TestWindowA(String id) {
		super(id, "Window A");

		Label label = new Label("label", () -> Long.toString(System.currentTimeMillis()));
		label.setOutputMarkupId(true);
		add(label);

		addAjaxButton(target -> target.add(label)).ofType(ButtonType.Dark).withLabel("Refresh Time");
		this.<String> addAjaxButton((target, str) -> {})
				.withModel(Model.of("Your mom"))
				.ofType(ButtonType.Danger)
				.withLabel("Your mom");

		addButton(() -> setResponsePage(IconLinksTestPage.class)).ofType(ButtonType.Info).withLabel("Other page");
		this.<String> addButton(foo -> {}).withModel(Model.of("FooBar")).ofType(ButtonType.Primary).withLabel("FooBar");
	}
}
