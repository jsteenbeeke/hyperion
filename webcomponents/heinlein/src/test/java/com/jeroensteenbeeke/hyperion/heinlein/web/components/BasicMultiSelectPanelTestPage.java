package com.jeroensteenbeeke.hyperion.heinlein.web.components;


import com.jeroensteenbeeke.hyperion.webcomponents.core.form.choice.LambdaRenderer;
import org.apache.wicket.markup.html.WebPage;

import java.util.List;

public class BasicMultiSelectPanelTestPage extends WebPage {
	public BasicMultiSelectPanelTestPage() {
		add(new BasicMultiSelectPanel<>("basic",
				"Basic Multiselect",
				List.of("A", "B", "E"),
				List.of("A", "B", "C", "D", "E"),
				LambdaRenderer.of(a -> a)
		));
	}
}
