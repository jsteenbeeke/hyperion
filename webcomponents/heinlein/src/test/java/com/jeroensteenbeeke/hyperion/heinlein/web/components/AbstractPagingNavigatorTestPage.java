package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import com.jeroensteenbeeke.hyperion.util.Randomizer;
import org.apache.wicket.markup.html.WebPage;

import java.util.ArrayList;
import java.util.List;

public class AbstractPagingNavigatorTestPage extends WebPage {
	private static final long serialVersionUID = 6351793661207408165L;

	public static final int LIST_SIZE = 25;

	public static final int SEGMENT_SIZE = 5;

	private final List<String> list;

	protected AbstractPagingNavigatorTestPage() {
		list = generateList();
	}

	public List<String> getList() {
		return list;
	}


	private List<String> generateList() {
		List<String> list = new ArrayList<>(LIST_SIZE);

		for (int i = 0; i < LIST_SIZE; i++) {
			list.add(i, Randomizer.random(20));
		}

		return list;
	}
}
