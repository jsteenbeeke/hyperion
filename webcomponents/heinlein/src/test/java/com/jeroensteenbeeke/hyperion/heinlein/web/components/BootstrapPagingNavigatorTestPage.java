package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PageableListView;

public class BootstrapPagingNavigatorTestPage extends AbstractPagingNavigatorTestPage {
	private static final long serialVersionUID = 8216137866454154598L;


	public BootstrapPagingNavigatorTestPage() {
		PageableListView<String> strings = new PageableListView<String>("strings", getList(), SEGMENT_SIZE) {

			private static final long serialVersionUID = 1624676085656751619L;

			@Override
			protected void populateItem(ListItem<String> listItem) {
				listItem.add(new Label("string", listItem.getModelObject()));
			}
		};

		add(strings);
		add(new BootstrapPagingNavigator("nav", strings));
	}

}
