package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;

public class BSMenuDividerTestPage extends WebPage {
	private final WebMarkupContainer trigger;

	public BSMenuDividerTestPage(boolean showDependent) {
		add(trigger = new WebMarkupContainer("trigger"));
		trigger.setVisibilityAllowed(showDependent);

		add(new BSMenuDivider("divider", trigger));
	}
}
