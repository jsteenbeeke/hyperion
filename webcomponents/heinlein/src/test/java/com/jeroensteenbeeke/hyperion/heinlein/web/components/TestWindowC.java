package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.markup.html.form.Form;

public class TestWindowC extends BootstrapModalWindow {
	public TestWindowC(String id) {
		super(id, "Window A");

		Form<String> form = new Form<>("form");

		add(form);

		addSubmitButton().forForm(form).ofType(ButtonType.Info).withLabel("Submit A");
		addAjaxSubmitButton().forForm(form).ofType(ButtonType.Info).withLabel("Submit B");
		addAjaxSubmitButton(target -> {}).forForm(form).ofType(ButtonType.Info).withLabel("Submit C");
	}
}
