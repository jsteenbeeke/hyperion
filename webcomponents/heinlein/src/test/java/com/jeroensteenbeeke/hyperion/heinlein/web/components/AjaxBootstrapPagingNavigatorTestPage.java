package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PageableListView;

public class AjaxBootstrapPagingNavigatorTestPage extends AbstractPagingNavigatorTestPage {
	private static final long serialVersionUID = 7425839204809386709L;

	public AjaxBootstrapPagingNavigatorTestPage() {
		PageableListView<String> strings = new PageableListView<String>("strings", getList(), SEGMENT_SIZE) {

			private static final long serialVersionUID = -1885191042919227465L;

			@Override
			protected void populateItem(ListItem<String> listItem) {
				listItem.add(new Label("string", listItem.getModelObject()));
			}
		};

		WebMarkupContainer container = new WebMarkupContainer("container");
		container.add(strings);
		add(container);
		add(new AjaxBootstrapPagingNavigator("nav", strings));
	}
}
