package com.jeroensteenbeeke.hyperion.heinlein.web;

import de.agilecoders.wicket.webjars.WicketWebjars;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class HeaderItemTest {
	@BeforeEach
	public void resetHeinleinStaticFields() throws NoSuchFieldException, IllegalAccessException {
		Field mainCssFileField = Heinlein.class.getDeclaredField("mainCssFile");
		mainCssFileField.trySetAccessible();
		mainCssFileField.set(null, null);
	}

	@Test
	public void testReferences() {
		WicketTester tester = new WicketTester();

		assertThrows(IllegalStateException.class, Heinlein::createCssHeaderItem);

		Heinlein.init(tester.getApplication(), "test.css");
		WicketWebjars.install(tester.getApplication());

		tester.startPage(ResourceReferenceTestPage.class);
		tester.assertRenderedPage(ResourceReferenceTestPage.class);

		tester.assertContains("test\\.css");
		tester.assertContains("jquery\\.ui\\.touch-punch\\.js");
		tester.assertContains("bootstrap-toggle\\.js");
		tester.assertContains("bootstrap-datepicker\\.js");
		tester.assertContains("bootstrap\\.min\\.js");
		tester.assertContains("bootstrap-datepicker3\\.css");
		tester.assertContains("bootstrap-toggle\\.css");
	}
}
