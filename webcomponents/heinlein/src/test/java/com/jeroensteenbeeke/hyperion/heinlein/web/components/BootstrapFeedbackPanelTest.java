package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.util.tester.WicketTester;
import org.junit.jupiter.api.Test;

public class BootstrapFeedbackPanelTest {
	@Test
	public void testFeedback() {
		WicketTester tester = new WicketTester();
		tester.startPage(BootstrapFeedbackPanelTestPage.class);
		tester.assertRenderedPage(BootstrapFeedbackPanelTestPage.class);

		tester.assertContainsNot("Success");
		tester.assertContainsNot("Info");
		tester.assertContainsNot("Warn");
		tester.assertContainsNot("Error");
		tester.assertContainsNot("Fatal");
		tester.assertContainsNot("Undefined");

		tester.clickLink("success");
		tester.assertRenderedPage(BootstrapFeedbackPanelTestPage.class);
		tester.assertContains("Success");
		tester.assertContains("alert alert-success");

		tester.clickLink("info");
		tester.assertRenderedPage(BootstrapFeedbackPanelTestPage.class);
		tester.assertContains("Info");
		tester.assertContains("alert alert-info");

		tester.clickLink("warn");
		tester.assertRenderedPage(BootstrapFeedbackPanelTestPage.class);
		tester.assertContains("Warn");
		tester.assertContains("alert alert-warning");

		tester.clickLink("error");
		tester.assertRenderedPage(BootstrapFeedbackPanelTestPage.class);
		tester.assertContains("Error");
		tester.assertContains("alert alert-danger");

		tester.clickLink("fatal");
		tester.assertRenderedPage(BootstrapFeedbackPanelTestPage.class);
		tester.assertContains("Fatal");
		tester.assertContains("alert alert-danger");

		tester.clickLink("undefined");
		tester.assertRenderedPage(BootstrapFeedbackPanelTestPage.class);
		tester.assertContains("Undefined");
	}
}
