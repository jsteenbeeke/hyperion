package com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav;

import com.jeroensteenbeeke.hyperion.heinlein.web.Heinlein;
import com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.annotation.InSubmenu;
import com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.annotation.NavbarItem;
import com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.annotation.NavbarPosition;
import com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.annotation.NavbarSubmenu;
import com.jeroensteenbeeke.hyperion.heinlein.web.pages.TestHomePage;
import org.apache.wicket.Page;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.Serial;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class SmartNavTest {
	@BeforeEach
	public void clearIndex() {
		SmartNavbarIndex.instance.clear();
	}

	@Test
	public void testNoSubmenuStartup() {
		assertThrows(IllegalStateException.class, () -> new WicketTester(new ScenarioA()));
	}

	@Test
	public void testSingleSubmenuStartup() {
		var tester = new WicketTester(new ScenarioB());

		tester.startPage(TestHomePage.class);
		tester.assertRenderedPage(TestHomePage.class);
	}

	@Test
	public void testMultiSubmenuStartup() {
		var tester = new WicketTester(new ScenarioC());

		tester.startPage(TestHomePage.class);
		tester.assertRenderedPage(TestHomePage.class);
	}

}

abstract class SmartNavApplication extends WebApplication {
	@Override
	protected void init() {
		super.init();

		Heinlein.init(this, "test.css");
	}
}

class ScenarioA extends SmartNavApplication {
	@Override
	public Class<? extends Page> getHomePage() {
		return TestHomePage.class;
	}
}

@NavbarSubmenu(key = "user", name = "User", position = NavbarPosition.RIGHT)
class ScenarioB extends SmartNavApplication {
	@Override
	public Class<? extends Page> getHomePage() {
		return TestHomePage.class;
	}
}

@NavbarSubmenu(key = "actions", name = "Actions", position = NavbarPosition.RIGHT)
@NavbarSubmenu(key = "user", name = "User", position = NavbarPosition.RIGHT, priority = 1)
class ScenarioC extends SmartNavApplication {
	@Override
	public Class<? extends Page> getHomePage() {
		return TestHomePage.class;
	}
}

@NavbarItem(name = "Some page")
@InSubmenu(key = "user")
class SomePage extends WebPage {

	@Serial
	private static final long serialVersionUID = 7712468892645216800L;
}


@NavbarItem(name = "Some other page", priority = 1)
@InSubmenu(key = "user", precededBySeparator = true)
class SomeOtherPage extends WebPage {

	@Serial
	private static final long serialVersionUID = -8987631407476366732L;

	public SomeOtherPage() {

	}
}
