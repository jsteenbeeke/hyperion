package com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.annotation;

/**
 * Position where navbar items can be placed
 */
public enum NavbarPosition {
	/**
	 * Left side
	 */
	LEFT,
	/**
	 * Right side
	 */
	RIGHT
}
