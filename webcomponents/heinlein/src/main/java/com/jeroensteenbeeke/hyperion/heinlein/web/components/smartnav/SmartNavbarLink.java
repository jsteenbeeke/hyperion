package com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Page;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.StringResourceModel;

import org.jetbrains.annotations.NotNull;
import java.io.Serial;

class SmartNavbarLink extends Panel {
	@Serial
	private static final long serialVersionUID = -7555806741816564717L;

	public SmartNavbarLink(@NotNull String id, Class<? extends Page> containingPage,
						   LinkItem item) {
		super(id);

		WebMarkupContainer container = new WebMarkupContainer("container");
		if (containingPage.equals(item.pageClass())) {
			container.add(AttributeModifier.append("class", "active"));
		}
		BookmarkablePageLink<Object> link = new BookmarkablePageLink<>("link", item.pageClass());
		link.setBody(item.localized() ? new StringResourceModel(item.getText()) :
							 Model.of(item.getText()));

		container.add(link);
		add(container);

		setRenderBodyOnly(true);
	}
}
