package com.jeroensteenbeeke.hyperion.heinlein.web.resources;


import com.googlecode.wicket.jquery.ui.resource.JQueryUIResourceReference;
import org.apache.wicket.markup.head.HeaderItem;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.request.resource.JavaScriptResourceReference;

import java.util.List;

/**
 * JS reference for touch punch, which adds drag and drop support for touch interfaces
 */
public class TouchPunchJavaScriptReference extends JavaScriptResourceReference {

	private static final long serialVersionUID = 1L;

	private static final TouchPunchJavaScriptReference INSTANCE = new TouchPunchJavaScriptReference();

	private TouchPunchJavaScriptReference() {
		super(TouchPunchJavaScriptReference.class,
				"js/jquery.ui.touch-punch.js");
	}

	@Override
	public List<HeaderItem> getDependencies() {
		return List.of(JavaScriptHeaderItem.forReference(JQueryUIResourceReference.get()));
	}

	/**
	 * Get a singleton instance of this resource
	 *
	 * @return A ResourceReference
	 */
	public static TouchPunchJavaScriptReference get() {
		return INSTANCE;
	}
}
