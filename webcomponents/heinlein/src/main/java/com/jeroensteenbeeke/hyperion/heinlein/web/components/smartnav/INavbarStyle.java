package com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav;

import java.io.Serializable;

/**
 * Navbar style
 */
public sealed interface INavbarStyle extends Serializable permits DefaultNavbarStyle, CustomNavbarStyle {
	/**
	 * Returns the CSS class(es) for the navbar
	 * @return The navbar style
	 */
	String getNavbarStyle();
}
