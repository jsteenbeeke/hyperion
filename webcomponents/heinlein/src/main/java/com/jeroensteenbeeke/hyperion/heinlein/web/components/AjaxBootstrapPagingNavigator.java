/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.heinlein.web.components;


import com.jeroensteenbeeke.hyperion.webcomponents.core.HTMLWrapper;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.markup.html.navigation.paging.AjaxPagingNavigationIncrementLink;
import org.apache.wicket.ajax.markup.html.navigation.paging.AjaxPagingNavigationLink;
import org.apache.wicket.ajax.markup.html.navigation.paging.AjaxPagingNavigator;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.markup.html.navigation.paging.IPageable;
import org.apache.wicket.markup.html.navigation.paging.IPagingLabelProvider;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigation;

/**
 * Modified version of the default AjaxPagingNavigator that adds bootstrap-specific markup
 * to the component
 */
public class AjaxBootstrapPagingNavigator extends AjaxPagingNavigator implements HTMLWrapper {
	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new AjaxBootstrapPagingNavigator
	 *
	 * @param id The id corresponding to the wicket:id in the HTML markup
	 * @param pageable The pageable component the page links are referring to
	 */
	public AjaxBootstrapPagingNavigator(String id, IPageable pageable) {
		super(id, pageable);
		setRenderBodyOnly(true);
	}

	/**
	 * Creates a new AjaxBootstrapPagingNavigator
	 *
	 * @param id The id corresponding to the wicket:id in the HTML markup
	 * @param pageable he pageable component the page links are referring to
	 * @param labelProvider The label provider for the link text
	 */
	public AjaxBootstrapPagingNavigator(String id, IPageable pageable, IPagingLabelProvider labelProvider) {
		super(id, pageable, labelProvider);
		setRenderBodyOnly(true);
	}

	@Override
	protected void onRender() {
		wrapRenderedHTML("<nav><ul class=\"pagination\">", () -> super.onRender(), "</ul></nav>");
	}



	@Override
	protected AbstractLink newPagingNavigationIncrementLink(String id, IPageable pageable, int increment) {
		AjaxPagingNavigationIncrementLink link = new AjaxPagingNavigationIncrementLink(id, pageable, increment) {
			private static final long serialVersionUID = -7650734533768164625L;

			@Override
			protected void onRender() {
				wrapRenderedHTML("<li class=\"page-item\">", () -> super.onRender(), "</li>");
			}
		};
		link.add(AttributeModifier.append("class", "page-link"));
		return link;

	}

	@Override
	protected AbstractLink newPagingNavigationLink(String id, IPageable pageable, int pageNumber) {
		AjaxPagingNavigationLink link = new AjaxPagingNavigationLink(id, pageable, pageNumber) {
			private static final long serialVersionUID = -7567615401065393552L;

			@Override
			protected void onRender() {
				wrapRenderedHTML("<li class=\"page-item\">", () -> super.onRender(), "</li>");
			}
		};
		link.add(AttributeModifier.append("class", "page-link"));
		return link;
	}

	@Override
	protected PagingNavigation newNavigation(String id, IPageable pageable, IPagingLabelProvider labelProvider) {
		return new AjaxBootstrapPagingNavigation(id, pageable, labelProvider);
	}
}
