package com.jeroensteenbeeke.hyperion.heinlein.web.components;

/**
 * Represents the primary theme colors defined by Bootstrap
 */
public enum BSThemeColor {
	Primary, Secondary, Success, Info, Warning, Danger, Light, Dark
}
