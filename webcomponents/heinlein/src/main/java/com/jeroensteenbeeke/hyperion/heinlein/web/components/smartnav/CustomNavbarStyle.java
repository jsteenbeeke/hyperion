package com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav;

/**
 * Custom style for navbars
 * @param cssClass The CSS class to add
 */
public record CustomNavbarStyle(String cssClass) implements INavbarStyle {
	@Override
	public String getNavbarStyle() {
		return cssClass;
	}
}
