/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.Component;
import org.apache.wicket.markup.html.panel.Panel;

/**
 * Bootstrap menu divider with visibility determined by one or more related components
 */
public class BSMenuDivider extends Panel {
	private static final long serialVersionUID = 1L;

	private Component[] relevantComponents;

	/**
	 * Creates a new BSMenuDivider
	 * @param id The wicket ID of the divider
	 * @param relevantComponents All components that trigger the visibility of this divider. If any
	 *                           of these components is visible then the BSMenuDivider will be visible, too. Please
	 *                           note that
	 *                           if this is an empty array, the BSMenuDivider will be invisible by default
	 */
	public BSMenuDivider(String id, Component... relevantComponents) {
		super(id);
		this.relevantComponents = relevantComponents;
	}

	@Override
	protected void onInitialize() {
		super.onInitialize();

		boolean visible = false;

		for (Component component : relevantComponents) {
			if (component.isVisibilityAllowed()) {
				visible = true;
				break;
			}
		}

		setVisibilityAllowed(visible);

	}
}
