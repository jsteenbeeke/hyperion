package com.jeroensteenbeeke.hyperion.heinlein.web.resources;


import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.HeaderItem;
import org.apache.wicket.resource.JQueryPluginResourceReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Resource reference for the bootstrap-toggle.js file
 */
public class BSCheckBoxToggleJavaScriptReference
		extends JQueryPluginResourceReference {

	private static final long serialVersionUID = 1L;
	private static final BSCheckBoxToggleJavaScriptReference instance = new BSCheckBoxToggleJavaScriptReference();

	private BSCheckBoxToggleJavaScriptReference() {
		super(BSCheckBoxToggleJavaScriptReference.class,
				"js/bootstrap-toggle.js");
	}

	@Override
	public List<HeaderItem> getDependencies() {
		List<HeaderItem> deps = new ArrayList<>(super.getDependencies());
		deps.add(CssHeaderItem.forReference(BSCheckBoxToggleCSSResourceReference.get()));

		return Collections.unmodifiableList(deps);
	}

	/**
	 * Get a singleton instance of this resource
	 *
	 * @return A ResourceReference
	 */
	public static BSCheckBoxToggleJavaScriptReference get() {
		return instance;
	}
}
