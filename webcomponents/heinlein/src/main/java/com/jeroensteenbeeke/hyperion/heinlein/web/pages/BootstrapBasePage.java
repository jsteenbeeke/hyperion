/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.heinlein.web.pages;

import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptUrlReferenceHeaderItem;
import org.apache.wicket.markup.html.CrossOrigin;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

/**
 * Base page for Bootstrap-based pages. Adds a mechanism to add a title, and sets the basic
 * structure through Wicket markup inheritance
 */
public abstract class BootstrapBasePage extends WebPage {
	private static final long serialVersionUID = 1L;

	private Label titleLabel;

	/**
	 * Create a new webpage with the given title
	 *
	 * @param title The title of the page, will be placed in a {@code <title></title>} tag of the HTML {@code
	 *              <head></head>} section
	 */
	protected BootstrapBasePage(String title) {
		add(titleLabel = new Label("title", title));
	}

	/**
	 * Replace the webpage's title with the given title
	 *
	 * @param title The title of the page, will be placed in a {@code <title></title>} tag of the HTML {@code
	 *              <head></head>} section
	 */
	public final void setTitle(String title) {
		setTitle(Model.of(title));
	}

	/**
	 * Replace the webpage's title with the given title
	 *
	 * @param titleModel An IModel that yields the title of the page, will be placed in a {@code <title></title>} tag of the HTML {@code
	 *                   <head></head>} section
	 */
	protected final void setTitle(IModel<String> titleModel) {
		titleLabel.setDefaultModel(titleModel);
	}

	@Override
	public void renderHead(IHeaderResponse response) {
		super.renderHead(response);
		response.render(JavaScriptUrlReferenceHeaderItem
								.forUrl("https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js")
								.setCrossOrigin(CrossOrigin.ANONYMOUS)
								.setIntegrity("sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3"));
		response.render(JavaScriptUrlReferenceHeaderItem
								.forUrl("https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js")
								.setCrossOrigin(CrossOrigin.ANONYMOUS)
								.setIntegrity("sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V"));
	}
}
