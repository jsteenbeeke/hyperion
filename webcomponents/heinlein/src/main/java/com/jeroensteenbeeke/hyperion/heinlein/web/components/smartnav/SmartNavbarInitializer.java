package com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav;

import com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.annotation.InSubmenu;
import com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.annotation.NavbarItem;
import com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.annotation.NavbarSubmenu;
import com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.annotation.NavbarSubmenus;
import org.apache.wicket.Page;
import org.apache.wicket.protocol.http.WebApplication;
import org.reflections.Reflections;
import org.reflections.util.ConfigurationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jetbrains.annotations.NotNull;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

/**
 * Initializer for the SmartNavbar. Attempts to use reflection to figure out annotated classes
 */
public class SmartNavbarInitializer {
	private static final Logger log = LoggerFactory.getLogger(SmartNavbarInitializer.class);

	/**
	 * Scans the given application (and any page classes in its subpackages) for {@code @Submenu}
	 * and
	 * {@code @NavbarItem} annotations, and configures the SmartNavbar
	 *
	 * @param application The application to scan
	 */
	public static void initialize(@NotNull WebApplication application) {
		Class<? extends WebApplication> appClass = application.getClass();

		List<NavbarSubmenu> navbarSubmenus;

		if (appClass.isAnnotationPresent(NavbarSubmenus.class)) {
			NavbarSubmenus submenus = appClass.getAnnotation(NavbarSubmenus.class);
			navbarSubmenus = List.of(submenus.value());
		} else if (appClass.isAnnotationPresent(NavbarSubmenu.class)) {
			navbarSubmenus = List.of(appClass.getAnnotation(NavbarSubmenu.class));
		} else {
			navbarSubmenus = List.of();
		}

		for (NavbarSubmenu navbarSubmenu : navbarSubmenus) {
			switch (navbarSubmenu.position()) {
				case LEFT -> SmartNavbarIndex.instance.addLeftSubmenu(
						navbarSubmenu.key(), new SubmenuItem(
								navbarSubmenu.name(),
								navbarSubmenu.priority(),
								navbarSubmenu.localized()));
				case RIGHT -> SmartNavbarIndex.instance.addRightSubmenu(
						navbarSubmenu.key(), new SubmenuItem(
								navbarSubmenu.name(),
								navbarSubmenu.priority(),
								navbarSubmenu.localized()));
			}
		}

		Reflections ref = new Reflections(new ConfigurationBuilder().forPackage(appClass.getPackageName(), appClass.getClassLoader()));
//		Reflections ref = new Reflections(appClass.getPackageName(), appClass
//				.getClassLoader());

		ref
				.getTypesAnnotatedWith(NavbarItem.class)
				.stream()
				.filter(Page.class::isAssignableFrom)
				.map(SmartNavbarInitializer::castToPageClass)
				.filter(SmartNavbarInitializer::hasValidConstructor)
				.forEach(SmartNavbarInitializer::processClass);

	}

	private static Class<? extends Page> castToPageClass(Class<?> aClass) {
		@SuppressWarnings("unchecked")
		Class<? extends Page> pageClass = (Class<? extends Page>) aClass;
		return pageClass;
	}

	private static void processClass(Class<? extends Page> pageClass) {
		NavbarItem item = pageClass.getAnnotation(NavbarItem.class);
		InSubmenu submenu = pageClass.getAnnotation(InSubmenu.class);

		if (submenu != null) {
			LinkItem linkItem = new LinkItem(item.name(), item.localized(), pageClass,
											 item.priority(), submenu.precededBySeparator());

			SmartNavbarIndex.instance.addToSubmenu(submenu.key(), linkItem);
		} else {
			LinkItem linkItem = new LinkItem(item.name(), item.localized(), pageClass,
											 item.priority(), false);
			switch (item.position()) {
				case LEFT -> SmartNavbarIndex.instance.addLeft(linkItem);
				case RIGHT -> SmartNavbarIndex.instance.addRight(linkItem);
			}
		}
	}


	private static boolean hasValidConstructor(Class<? extends Page> pageClass) {
		boolean hasValidConstructor =
				pageClass.getConstructors().length == 0 || Arrays.stream(pageClass.getConstructors())
											.filter(SmartNavbarInitializer::constructorIsPublic)
											.anyMatch(SmartNavbarInitializer::constructorHasZeroArgs);

		if (!hasValidConstructor) {
			log.warn("Page {} is annotated @NavbarItem but does not have a valid public 0-argument constructor", pageClass.getName());
		}

		return hasValidConstructor;
	}

	private static boolean constructorIsPublic(Constructor<?> constructor) {
		return Modifier.isPublic(constructor.getModifiers());
	}

	private static boolean constructorHasZeroArgs(Constructor<?> constructor) {
		return constructor.getParameterCount() == 0;
	}
}
