package com.jeroensteenbeeke.hyperion.heinlein.web.components.progressbar;


import com.jeroensteenbeeke.hyperion.heinlein.web.components.BSThemeColor;
import org.jetbrains.annotations.NotNull;

/**
 * Configuration for progress bars
 * @param showLabel Whether or not the label should be visible
 * @param background The color of the background
 * @param mode How the bar should be displayed
 */
public record BSProgressBarConfig(boolean showLabel, @NotNull BSThemeColor background, @NotNull ProgressBarMode mode) {
	/**
	 * Convenience constructor
	 * @param showLabel Whether or not the label should be visible
	 */
	public BSProgressBarConfig(boolean showLabel) {
		this(showLabel, BSThemeColor.Primary);
	}

	/**
	 * Convenience constructor
	 * @param showLabel Whether or not the label should be visible
	 * @param background The color of the background
	 */
	public BSProgressBarConfig(boolean showLabel, @NotNull BSThemeColor background) {
		this(showLabel, background, ProgressBarMode.Normal);
	}

	/**
	 * Convenience constructor
	 * @param showLabel Whether or not the label should be visible
	 * @param mode How the bar should be displayed
	 */
	public BSProgressBarConfig(boolean showLabel, @NotNull ProgressBarMode mode) {
		this(showLabel, BSThemeColor.Primary, mode);
	}

	/**
	 * Creates a default configuration
	 * @return The default configuration
	 */
	public static BSProgressBarConfig defaults() {
		return new BSProgressBarConfig(true, BSThemeColor.Primary, ProgressBarMode.Normal);
	}

}
