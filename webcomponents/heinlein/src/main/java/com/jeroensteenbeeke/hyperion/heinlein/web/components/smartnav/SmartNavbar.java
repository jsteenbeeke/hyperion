package com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav;

import com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.annotation.NavbarPosition;
import com.jeroensteenbeeke.hyperion.icons.Icon;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serial;

/**
 * Navbar that uses annotations to determine menu items.
 *
 * @see com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.annotation.NavbarItem
 * @see com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.annotation.NavbarSubmenu
 * @see com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.annotation.InSubmenu
 */
public class SmartNavbar extends Panel {
	@Serial
	private static final long serialVersionUID = 6205976749946159320L;

	private final WebMarkupContainer nav;

	private AttributeModifier navStyle;

	private SmartNavbar(
		@NotNull String id, @Nullable IModel<String> brandModel,
		@NotNull INavbarStyle navbarStyle, @NotNull NavbarPosition toggleTarget, @NotNull Icon icon) {
		super(id);

		nav = new WebMarkupContainer("navbar");
		nav.add(navStyle = AttributeModifier.append("class",
			navbarStyle.getNavbarStyle()));

		if (brandModel == null) {
			nav.add(new WebMarkupContainer("brand").setVisibilityAllowed(false));
		} else {
			nav.add(new Label("brand", brandModel));
		}

		WebMarkupContainer leftContainer;
		nav.add(leftContainer = new WebMarkupContainer("leftContainer"));
		leftContainer.setOutputMarkupId(true);

		WebMarkupContainer rightContainer;
		nav.add(rightContainer = new WebMarkupContainer("rightContainer"));
		rightContainer.setOutputMarkupId(true);

		leftContainer.add(new ListView<>("left", SmartNavbarIndex.instance.getLeft()) {
			@Serial
			private static final long serialVersionUID = -807492109448045365L;

			@Override
			protected void populateItem(ListItem<Item> item) {
				populateNavbarItem(item);
			}
		});

		rightContainer.add(new ListView<>("right", SmartNavbarIndex.instance.getRight()) {
			@Serial
			private static final long serialVersionUID = -807492109448045365L;

			@Override
			protected void populateItem(ListItem<Item> item) {
				populateNavbarItem(item);
			}
		});

		WebMarkupContainer toggleTargetContainer = switch (toggleTarget) {
			case LEFT -> leftContainer;
			case RIGHT -> rightContainer;
		};
		WebMarkupContainer toggler;
		nav.add(toggler = new WebMarkupContainer("toggler"));
		toggler.add(AttributeModifier.replace("data-bs-target",
			"#" + toggleTargetContainer.getMarkupId()
		)).add(AttributeModifier.replace("aria-controls",
			toggleTargetContainer.getMarkupId()
		));

		toggler.add(new WebMarkupContainer("icon").add(AttributeModifier.replace("class", icon.getCssClasses())));

		add(nav);
	}

	private void populateNavbarItem(ListItem<Item> item) {
		Item navItem = item.getModelObject();

		if (navItem instanceof LinkItem linkItem) {
			item.add(new SmartNavbarLink("item", getPage().getPageClass(), linkItem));
		} else if (navItem instanceof SubmenuItem submenu) {
			item.add(new SmartSubmenu("item", getPage().getPageClass(), submenu));
		}
	}

	/**
	 * Builder for creating a new SmartNavbar
	 *
	 * @param id The wicket:id
	 * @return A builder
	 */
	@NotNull
	public static WithBrand smartNavbar(@NotNull String id) {
		return brandModel -> new AndStyle(id, brandModel);
	}

	/**
	 * Builder step for adding the brand model
	 */
	@FunctionalInterface
	public interface WithBrand {
		/**
		 * Sets the model for the brand name (title on navbar)
		 *
		 * @param brand The brand model
		 * @return The next builder step
		 */
		@NotNull
		AndStyle withBrand(@Nullable IModel<String> brand);

		/**
		 * Sets the given string ad brand name
		 *
		 * @param brand The brand name
		 * @return The next builder step
		 */
		@NotNull
		default AndStyle withBrand(@NotNull String brand) {
			return withBrand(() -> brand);
		}

		/**
		 * Sets the navbar to hide the brand
		 *
		 * @return The next builder step
		 */
		@NotNull
		default AndStyle withoutBrand() {
			return withBrand((IModel<String>) null);
		}
	}

	/**
	 * Builder finalizer step for adding the style model
	 */
	public static class AndStyle {
		private final String id;

		private final IModel<String> brand;

		private final NavbarPosition mobileTogglesMenu;

		// Defaults to a dummy
		private final Icon icon;

		private AndStyle(@NotNull String id, @Nullable IModel<String> brand) {
			this(id, brand, NavbarPosition.RIGHT, () -> "navbar-toggler-icon");
		}

		private AndStyle(@NotNull String id, @Nullable IModel<String> brand, @NotNull NavbarPosition mobileTogglesMenu, @NotNull Icon icon) {
			this.id = id;
			this.brand = brand;
			this.mobileTogglesMenu = mobileTogglesMenu;
			this.icon = icon;
		}

		/**
		 * Sets the navbar style to the given String
		 *
		 * @param style The style
		 * @return A SmartNavbar
		 */
		public SmartNavbar andStyle(@NotNull INavbarStyle style) {
			return new SmartNavbar(id, brand, style, mobileTogglesMenu, icon);
		}

		/**
		 * Configures the toggler icon
		 *
		 * @param icon The icon to use
		 * @return The builder
		 */
		public AndStyle withTogglerIcon(@NotNull Icon icon) {
			return new AndStyle(id, brand, mobileTogglesMenu, icon);
		}

		/**
		 * Sets the navbar style to the default (light)
		 *
		 * @return A SmartNavbar
		 */
		public SmartNavbar andLightStyle() {
			return andStyle(DefaultNavbarStyle.Light);
		}

		/**
		 * Sets the navbar style to dark
		 *
		 * @return A SmartNavbar
		 */
		public SmartNavbar andDarkStyle() {
			return andStyle(DefaultNavbarStyle.Dark);
		}

		/**
		 * Sets the menu triggered by the collapsed hamburger menu to the given position
		 *
		 * @param position The menu position to open
		 * @return
		 */
		public AndStyle whereCollapsedToggleButtonTriggers(@NotNull NavbarPosition position) {
			return new AndStyle(id, brand, position, icon);
		}
	}
}
