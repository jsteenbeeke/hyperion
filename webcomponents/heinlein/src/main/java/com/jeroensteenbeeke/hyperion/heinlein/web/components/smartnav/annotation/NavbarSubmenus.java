package com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.annotation;

import java.lang.annotation.*;

/**
 * Repeatable annotation for multiple submenu annotations
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface NavbarSubmenus {
	/**
	 * The submenus
	 * @return The value
	 */
	NavbarSubmenu[] value() default {};
}
