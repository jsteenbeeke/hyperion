package com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav;

import org.apache.wicket.Page;

import org.jetbrains.annotations.NotNull;
import java.io.Serial;
import java.io.Serializable;
import java.util.*;

enum SmartNavbarIndex {
	instance;

	private final Map<String,SubmenuItem> submenus;

	private final List<Item> left;

	private final List<Item> right;

	SmartNavbarIndex() {
		this.submenus = new HashMap<>();
		this.left = new ArrayList<>();
		this.right = new ArrayList<>();
	}

	public List<Item> getLeft() {
		return left.stream().sorted(Comparator.comparing(Item::getPriority)).toList();
	}

	public List<Item> getRight() {
		return right.stream().sorted(Comparator.comparing(Item::getPriority)).toList();
	}

	public void addLeftSubmenu(@NotNull String key, @NotNull SubmenuItem left) {
		this.left.add(left);
		this.submenus.put(key, left);
	}

	public void addRightSubmenu(@NotNull String key, @NotNull SubmenuItem right) {
		this.right.add(right);
		this.submenus.put(key, right);
	}

	public void addLeft(@NotNull LinkItem left) {
		this.left.add(left);
	}

	public void addRight(@NotNull LinkItem right) {
		this.right.add(right);
	}

	public void addToSubmenu(@NotNull String key, @NotNull LinkItem item) {
		SubmenuItem submenu = this.submenus.get(key);
		if (submenu == null) {
			throw new IllegalStateException("Invalid submenu key "+ key);
		}

		submenu.addSubmenuItem(item);
	}

	/**
	 * Should only be called from unit tests
	 */
	public void clear() {
		this.left.clear();
		this.right.clear();
		this.submenus.clear();
	}
}

sealed interface Item extends Serializable permits LinkItem, SubmenuItem {
	String getText();

	int getPriority();

	boolean isLocalized();

	boolean hasSeparator();
}

final record LinkItem(String text, boolean localized, Class<? extends Page> pageClass, int priority, boolean hasSeparator) implements Item {
	@Override
	public String getText() {
		return text();
	}

	@Override
	public int getPriority() {
		return priority();
	}

	@Override
	public boolean hasSeparator() {
		return hasSeparator;
	}

	@Override
	public boolean isLocalized() {
		return localized;
	}
}

final class SubmenuItem implements Item {
	@Serial
	private static final long serialVersionUID = 1L;

	private final String text;

	private final int priority;

	private final boolean localized;

	private final List<LinkItem> submenuItems;

	public SubmenuItem(String text, int priority, boolean localized) {
		this.text = text;
		this.priority = priority;
		this.localized = localized;
		this.submenuItems = new ArrayList<>();
	}

	@Override
	public String getText() {
		return text;
	}

	@Override
	public int getPriority() {
		return priority;
	}

	public List<LinkItem> getSubmenuItems() {
		return submenuItems.stream().sorted(Comparator.comparing(Item::getPriority)).toList();
	}

	void addSubmenuItem(@NotNull LinkItem item) {
		this.submenuItems.add(item);
	}

	@Override
	public boolean hasSeparator() {
		return false;
	}

	@Override
	public boolean isLocalized() {
		return localized;
	}
}
