package com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that a NavbarItem is contained in a submenu
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface InSubmenu {
	/**
	 * The key of the submenu
	 * @return The key
	 */
	String key();

	/**
	 * Indicates whether or not the item should be preceded by a separator. Defaults to false.
	 * @return {@code true} if the item should be preceded by a separator. {@code false} otherwise
	 */
	boolean precededBySeparator() default false;
}
