/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.heinlein.web.resources;


import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.HeaderItem;
import org.apache.wicket.resource.JQueryPluginResourceReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Resource reference for the bootstrap-datepicker.js file
 */
public class BSDatePickerJavaScriptReference extends
		JQueryPluginResourceReference {
	private static final long serialVersionUID = 1L;

	private static final BSDatePickerJavaScriptReference instance = new BSDatePickerJavaScriptReference();

	private BSDatePickerJavaScriptReference() {
		super(BSDatePickerJavaScriptReference.class,
				"js/bootstrap-datepicker.js");
	}

	@Override
	public List<HeaderItem> getDependencies() {

		List<HeaderItem> deps = new ArrayList<>(super.getDependencies());
		deps.add(CssHeaderItem
				.forReference(BSDatePickerCSSResourceReference.get()));

		return Collections.unmodifiableList(deps);
	}

	/**
	 * Get a singleton instance of this resource
	 *
	 * @return A ResourceReference
	 */
	public static BSDatePickerJavaScriptReference get() {
		return instance;
	}
}
