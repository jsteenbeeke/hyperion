package com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Page;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.StringResourceModel;

import java.io.Serial;

class SmartSubmenu extends Panel {
	@Serial
	private static final long serialVersionUID = -1775558159930785653L;

	SmartSubmenu(String id, Class<? extends Page> pageClass, SubmenuItem submenu) {
		super(id);

		IModel<String> nameModel = submenu.isLocalized() ?
				new StringResourceModel(submenu.getText()) : Model.of(submenu.getText());

		Label nameLabel;
		add(nameLabel = new Label("name", nameModel));
		nameLabel.setOutputMarkupId(true);

		if (submenu
				.getSubmenuItems()
				.stream()
				.map(LinkItem::pageClass)
				.anyMatch(pageClass::equals)) {
			nameLabel.add(AttributeModifier.append("class", "active"));
		}

		WebMarkupContainer container = new WebMarkupContainer("container");
		container.add(AttributeModifier.replace("aria-labelledby", nameLabel::getMarkupId));

		container.add(new ListView<>("items",
									 submenu.getSubmenuItems()) {
			@Serial
			private static final long serialVersionUID = -2516251734566634739L;

			@Override
			protected void populateItem(ListItem<LinkItem> item) {
				LinkItem linkItem = item.getModelObject();
				item.add(new WebMarkupContainer("divider").setVisibilityAllowed(linkItem.hasSeparator()));

				IModel<String> linkText = linkItem.localized() ?
						new StringResourceModel(linkItem.text()) : Model.of(linkItem.text());

				BookmarkablePageLink<Object> link = new BookmarkablePageLink<>("link",
																			   linkItem.pageClass());
				link.setBody(linkText);

				if (pageClass.equals(linkItem.pageClass())) {
					link.add(AttributeModifier.append("class", "active"));
				}

				item.add(link);
			}
		});

		add(container);
	}
}
