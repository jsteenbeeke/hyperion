package com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation added to a page to indicate that it should be added to a Smart navbar
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface NavbarItem {
	/**
	 * Gets the name of the navbar item. If the field {@code localized} is set to {@code true}, then this
	 * value represents the resource key
	 * @return The name/key of the item in the menu
	 */
	String name();

	/**
	 * Whether or not the item is localized. Defaults to {@code false}
	 * @return Whether or not the item is localized
	 */
	boolean localized() default false;

	/**
	 * Indicates the priority of the item, and where it will appear in the menu
	 * @return The priority. Sorted in ascending order
	 */
	int priority() default 0;

	/**
	 * Determines if the submenu should be displayed left or right. Ignored if part of a submenu
	 * @return The position
	 */
	NavbarPosition position() default NavbarPosition.RIGHT;

}
