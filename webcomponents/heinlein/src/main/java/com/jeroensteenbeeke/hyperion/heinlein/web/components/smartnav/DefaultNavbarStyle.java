package com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav;

/**
 * Default navbar styles
 */
public enum DefaultNavbarStyle implements INavbarStyle {
	/**
	 * Light navbar style
	 */
	Light("bg-light"),
	/**
	 * Dark navbar style
	 */
	Dark("bg-dark");

	private final String cssClass;

	/**
	 * Constructor
	 * @param cssClass The class to use
	 */
	DefaultNavbarStyle(String cssClass) {
		this.cssClass = cssClass;
	}

	@Override
	public String getNavbarStyle() {
		return cssClass;
	}
}
