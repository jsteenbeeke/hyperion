package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import org.apache.wicket.markup.html.border.Border;

import org.jetbrains.annotations.NotNull;

/**
 * Wicket Border that wraps the contained element in a bootstrap row
 */
public class BSRowBorder extends Border {
	/**
	 * Constructor
	 * @param id The Wicket ID
	 */
	public BSRowBorder(@NotNull String id) {
		super(id);
	}
}
