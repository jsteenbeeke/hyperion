package com.jeroensteenbeeke.hyperion.heinlein.web.components.progressbar;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Represents the display mode of the progress bar
 */
public enum ProgressBarMode {
	/**
	 * Normal display mode, a solid flood fill
	 */
	Normal("progress-bar"),
	/**
	 * Striped mode
	 */
	Striped("progress-bar", "progress-bar-striped"),
	/**
	 * Animated, like striped, but the stripes move
	 */
	Animated("progress-bar", "progress-bar-striped",
		"progress-bar-animated");

	private final List<String> cssClasses;

	/**
	 * Constructor
	 * @param cssClasses The CSS classes for this display mode
	 */
	ProgressBarMode(@NotNull String... cssClasses) {
		this.cssClasses = List.of(cssClasses);
	}

	/**
	 * Returns the list of CSS classes required by the display mode
	 * @return A List of Strings
	 */
	@NotNull
	public List<String> getCssClasses() {
		return cssClasses;
	}
}
