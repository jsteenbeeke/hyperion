package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import com.jeroensteenbeeke.hyperion.webcomponents.core.repeaters.PartitionedView;
import org.apache.wicket.markup.html.border.Border;
import org.apache.wicket.model.IModel;

import org.jetbrains.annotations.NotNull;
import java.util.List;

/**
 * Partitioned view that renders each partition as a Bootstrap Row
 * @param <T>
 */
public abstract class RowListView<T> extends PartitionedView<T> {
	private static final long serialVersionUID = -8683843319075653815L;

	/**
	 * Constructor
	 *
	 * @param id            The Wicket ID
	 * @param model         The model containing the original unpartitioned list
	 * @param partitionSize The partition size: the number of items per partition
	 */
	public RowListView(@NotNull String id, @NotNull IModel<List<T>> model, int partitionSize) {
		super(id, model, partitionSize);
	}

	@Override
	public final Border createPartitionBorder(@NotNull String id, @NotNull IModel<List<T>> model) {
		return new BSRowBorder(id);
	}
}
