package com.jeroensteenbeeke.hyperion.heinlein.web.components.smartnav.annotation;

import java.lang.annotation.*;

/**
 * Indicates that the application has a submenu
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Repeatable(NavbarSubmenus.class)
public @interface NavbarSubmenu {
	/**
	 * The key of the submenu, that can be used to uniquely identify the menu
	 * @return The key
	 */
	String key();

	/**
	 * Gets the name of the submenu. If the field {@code localized} is set to {@code true}, then this
	 * value represents the resource key
	 * @return The name/resource key of the submenu
	 */
	String name();

	/**
	 * Whether or not the name is localized. Defaults to {@code false}
	 * @return Whether or not the item is localized
	 */
	boolean localized() default false;

	/**
	 * Indicates the priority of the menu, and where it will appear in the menu
	 * @return The priority. Sorted in ascending order
	 */
	int priority() default 0;

	/**
	 * Determines if the submenu should be displayed left or right
	 * @return The position
	 */
	NavbarPosition position();
}
