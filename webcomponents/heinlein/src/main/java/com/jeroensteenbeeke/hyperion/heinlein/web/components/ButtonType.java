package com.jeroensteenbeeke.hyperion.heinlein.web.components;

/**
 * Enum for the various styles for buttons defined by Bootstrap
 */
public enum ButtonType {
	/**
	 * Primary button (blue)
	 */
	Primary,
	/**
	 * Secondary button (light grey)
	 */
	Secondary,
	/**
	 * Success button (green)
	 */
	Success,
	/**
	 * Danger button (red)
	 */
	Danger,
	/**
	 * Warning button (orange)
	 */
	Warning,
	/**
	 * Info button (light blue)
	 */
	Info,
	/**
	 * Light button (white)
	 */
	Light,
	/**
	 * Dark button (dark grey)
	 */
	Dark,
	/**
	 * Link button (white with blue text)
	 */
	Link
}
