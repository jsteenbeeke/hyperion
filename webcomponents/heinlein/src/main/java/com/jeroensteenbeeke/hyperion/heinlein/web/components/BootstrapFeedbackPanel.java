/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.heinlein.web.components;

import com.jeroensteenbeeke.hyperion.webcomponents.core.TypedPanel;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.feedback.FeedbackMessage;
import org.apache.wicket.feedback.FeedbackMessagesModel;
import org.apache.wicket.feedback.IFeedback;
import org.apache.wicket.feedback.IFeedbackMessageFilter;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * FeedbackPanel implementation that uses Bootstrap-specific styling
 */
public class BootstrapFeedbackPanel extends TypedPanel<List<FeedbackMessage>> implements IFeedback {

	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new feedback panel with the given ID
	 *
	 * @param id the ID of the component, corresponding to the HTML element
	 */
	public BootstrapFeedbackPanel(@NotNull String id) {
		super(id);
		initPanel(new FeedbackMessagesModel(this));
	}

	/**
	 * Creates a new feedback panel with the given ID
	 *
	 * @param id     the ID of the component, corresponding to the HTML element
	 * @param filter A filter to use to limit the number of messages shown
	 */
	public BootstrapFeedbackPanel(@NotNull String id, @NotNull IFeedbackMessageFilter filter) {
		super(id);
		FeedbackMessagesModel model = new FeedbackMessagesModel(this);
		model.setFilter(filter);
		initPanel(model);
	}

	private void initPanel(@NotNull FeedbackMessagesModel model) {
		add(new ListView<>("feedbackMessages", model) {
			@Override
			protected void populateItem(ListItem<FeedbackMessage> item) {
				WebMarkupContainer container = new WebMarkupContainer("feedbackMessage");
				container.add(
					AttributeModifier.replace("class", getCSSClass(item.getModelObject())));
				container.add(new Label("message", item.getModelObject().getMessage()));
				item.add(container);
			}
		});
	}

	/**
	 * Determines the CSS classes for the alert component
	 * @param message The message to determine the CSS for
	 * @return The CSS classes, ready to include in HTML
	 */
	protected String getCSSClass(FeedbackMessage message) {
		return switch (message.getLevel()) {
			case FeedbackMessage.FATAL, FeedbackMessage.ERROR -> "alert alert-danger";
			case FeedbackMessage.WARNING -> "alert alert-warning";
			case FeedbackMessage.INFO -> "alert alert-info";
			case FeedbackMessage.SUCCESS -> "alert alert-success";
			default -> "alert alert-secondary";
		};

	}

}
