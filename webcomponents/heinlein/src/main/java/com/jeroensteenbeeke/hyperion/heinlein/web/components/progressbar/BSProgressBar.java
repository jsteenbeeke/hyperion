package com.jeroensteenbeeke.hyperion.heinlein.web.components.progressbar;

import com.jeroensteenbeeke.hyperion.heinlein.web.components.BSThemeColor;
import com.jeroensteenbeeke.hyperion.webcomponents.core.TypedPanel;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.IModel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

/**
 * Bootstrap progress bar
 */
public class BSProgressBar extends TypedPanel<Integer> {
	@Serial
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 *
	 * @param id            The Wicket ID
	 * @param progressModel The model representing the progress made. Should contain a value between 0 and 100 or it will
	 *                      be sanitized
	 */
	public BSProgressBar(@NotNull String id, @NotNull IModel<Integer> progressModel) {
		this(id, progressModel, BSProgressBarConfig.defaults());
	}

	/**
	 * Constructor
	 *
	 * @param id            The Wicket ID
	 * @param progressModel The model representing the progress made. Should contain a value between 0 and 100 or it will
	 *                      be sanitized
	 * @param config        The configuration for the progress bar
	 */
	public BSProgressBar(@NotNull String id, @NotNull IModel<Integer> progressModel, @NotNull BSProgressBarConfig config) {
		super(id, progressModel.filter(BSProgressBar::isValidPercentage).orElse(0));

		Component bar;
		add(bar = config.showLabel() && getModelObject() != 0 ? new Label("bar", getModel().map("%d%%"::formatted)) : new WebMarkupContainer("bar"));

		bar
			.add(AttributeModifier.replace("style", getModel().map("width: %d%%"::formatted)))
			.add(AttributeModifier.replace("aria-valuenow", getModel()));

		List<String> cssClasses = new ArrayList<>(4);
		cssClasses.addAll(config.mode().getCssClasses());
		if (config.background() != BSThemeColor.Primary) {
			cssClasses.add("bg-" + config.background().name().toLowerCase());
		}

		bar.add(AttributeModifier.replace("class", String.join(" ", cssClasses)));
	}

	private static boolean isValidPercentage(@Nullable Integer integer) {
		return integer != null && integer >= 0 && integer <= 100;
	}
}
