package com.jeroensteenbeeke.hyperion.webcomponents.tageditor;

import org.apache.wicket.request.resource.CssResourceReference;

import java.io.Serial;

/**
 * CSS reference for jquery.tag-editor.css
 */
public class JQueryTagEditorCSSReference extends CssResourceReference {
	@Serial
	private static final long serialVersionUID = -6552228628226852957L;

	private static final JQueryTagEditorCSSReference instance = new JQueryTagEditorCSSReference();

	private JQueryTagEditorCSSReference() {
		super(JQueryTagEditorCSSReference.class, "css/jquery.tag-editor.css");
	}

	/**
	 * Get the instance
	 * @return The instance
	 */
	public static JQueryTagEditorCSSReference get() {
		return instance;
	}
}
