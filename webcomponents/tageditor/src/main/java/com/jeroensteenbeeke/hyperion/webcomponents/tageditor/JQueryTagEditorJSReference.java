package com.jeroensteenbeeke.hyperion.webcomponents.tageditor;

import org.apache.wicket.markup.head.HeaderItem;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.request.resource.JavaScriptResourceReference;

import java.io.Serial;
import java.util.List;

/**
 * JavaScript reference for jquery.tag-editor.js
 */
public class JQueryTagEditorJSReference extends JavaScriptResourceReference {
	@Serial
	private static final long serialVersionUID = 1272207179042451256L;

	private static final JQueryTagEditorJSReference instance = new JQueryTagEditorJSReference();

	private JQueryTagEditorJSReference() {
		super(JQueryTagEditorJSReference.class, "js/jquery.tag-editor.js");
	}

	/**
	 * Gets the instance
	 * @return The instance
	 */
	public static JQueryTagEditorJSReference get() {
		return instance;
	}

	@Override
	public List<HeaderItem> getDependencies() {
		List<HeaderItem> dependencies = super.getDependencies();
		dependencies.add(JavaScriptHeaderItem.forReference(JQueryCaretReference.get()));
		return dependencies;
	}
}
