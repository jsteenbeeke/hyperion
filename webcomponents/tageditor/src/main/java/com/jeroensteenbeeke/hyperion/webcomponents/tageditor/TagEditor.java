package com.jeroensteenbeeke.hyperion.webcomponents.tageditor;

import com.googlecode.wicket.jquery.ui.resource.JQueryUIResourceReference;
import com.jeroensteenbeeke.hyperion.webcomponents.core.TypedPanel;
import org.apache.wicket.ajax.AbstractDefaultAjaxBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.attributes.AjaxRequestAttributes;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.model.IModel;
import org.danekja.java.util.function.serializable.SerializableBiConsumer;
import org.danekja.java.util.function.serializable.SerializableFunction;
import org.jetbrains.annotations.NotNull;

import org.jetbrains.annotations.NotNull;
import java.io.Serial;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Editor panel to work with tags: space separated keywords that should get fancy markup
 *
 * @param <T> The type of tag
 */
public abstract class TagEditor<T> extends TypedPanel<List<T>> {
	@Serial
	private static final long serialVersionUID = 4770064425602217053L;

	private final SerializableFunction<T, String> tagToString;

	private final SerializableFunction<String, T> stringToTag;

	private final WebMarkupContainer tagEditor;

	private final AbstractDefaultAjaxBehavior addBehavior;

	private final AbstractDefaultAjaxBehavior deleteBehavior;

	private boolean forceLowercase = false;

	private IModel<List<T>> autoCompleteOptions;

	/**
	 * Constructor
	 *
	 * @param id          The wicket:id
	 * @param tagsModel   The model containing the tags
	 * @param tagToString Logic for turning a tag to a String representation
	 * @param stringToTag Logic for turning a String representation back to a tag
	 */
	public TagEditor(@NotNull String id,
					 @NotNull IModel<List<T>> tagsModel,
					 @NotNull SerializableFunction<T, String> tagToString,
					 @NotNull SerializableFunction<String, T> stringToTag) {
		super(id, tagsModel);
		this.tagToString = tagToString;
		this.stringToTag = stringToTag;

		add(this.tagEditor = new WebMarkupContainer("tageditor"));
		this.tagEditor.setOutputMarkupId(true);
		postProcessTagEditorComponent(this.tagEditor);

		tagEditor.add(addBehavior = new AbstractDefaultAjaxBehavior() {
			@Serial
			private static final long serialVersionUID = -8287179581015347159L;

			@Override
			protected void updateAjaxAttributes(AjaxRequestAttributes attributes) {
				super.updateAjaxAttributes(attributes);
				attributes.getDynamicExtraParameters().add("""
																   return {
																   	"tag": addedTag
																   }
																   """);
			}

			@Override
			protected void respond(AjaxRequestTarget target) {

				String tagStr =
						getRequest()
								.getRequestParameters()
								.getParameterValue("tag")
								.toOptionalString();

				Optional.ofNullable(tagStr)
						.filter(t -> !t.isBlank() && !t.isEmpty())
						.map(stringToTag)
						.ifPresent(tag -> {
							List<T> current = getModelObject();
							current.add(tag);
							setModelObject(current);
							onTagAdded(target, tag);
						});
			}
		});

		tagEditor.add(deleteBehavior = new AbstractDefaultAjaxBehavior() {
			@Serial
			private static final long serialVersionUID = -8287179581015347159L;

			@Override
			protected void updateAjaxAttributes(AjaxRequestAttributes attributes) {
				super.updateAjaxAttributes(attributes);
				attributes.getDynamicExtraParameters().add("""
																   return {
																   	"tag": deletedTag
																   }
																   """);
			}

			@Override
			protected void respond(AjaxRequestTarget target) {
				String tagStr =
						getRequest()
								.getRequestParameters()
								.getParameterValue("tag")
								.toOptionalString();

				Optional.ofNullable(tagStr)
						.filter(t -> !t.isBlank() && !t.isEmpty())
						.map(stringToTag)
						.ifPresent(tag -> {
							List<T> current = getModelObject();
							current.remove(tag);
							setModelObject(current);
							onTagDeleted(target, tag);
						});
			}
		});
	}

	/**
	 * Sets the options for autocomplete
	 * @param autoCompleteOptions The options
	 */
	public void setAutoCompleteOptions(IModel<List<T>> autoCompleteOptions) {
		this.autoCompleteOptions = autoCompleteOptions;
	}

	/**
	 * Forces tags to lowercase
	 *
	 * @param forceLowercase Whether or not tags should be forced to lowercase (default: false)
	 */
	public void setForceLowercase(boolean forceLowercase) {
		this.forceLowercase = forceLowercase;
	}

	/**
	 * Called when a new tag is added
	 *
	 * @param target The AjaxRequestTarget
	 * @param tag    The added tag
	 */
	protected void onTagAdded(@NotNull AjaxRequestTarget target,
							  @NotNull T tag) {

	}

	/**
	 * Called when an existing tag is deleted
	 *
	 * @param target The AjaxRequestTarget
	 * @param tag    The deleted tag
	 */
	protected void onTagDeleted(@NotNull AjaxRequestTarget target,
								@NotNull T tag) {

	}

	/**
	 * Post-process the tag editor component, allowing implementing classes to add styles if needed
	 *
	 * @param tagEditorComponent The tag editor component
	 */
	protected void postProcessTagEditorComponent(WebMarkupContainer tagEditorComponent) {

	}

	@Override
	public void renderHead(IHeaderResponse response) {
		super.renderHead(response);
		response.render(JavaScriptHeaderItem.forReference(JQueryCaretReference.get()));
		response.render(JavaScriptHeaderItem.forReference(JQueryTagEditorJSReference.get()));
		response.render(CssHeaderItem.forReference(JQueryTagEditorCSSReference.get()));

		String autoComplete = "";

		if ( autoCompleteOptions != null) {
			autoComplete =
					"""
									autocomplete: {
										  source: [%1$s]
										},
							""".formatted(autoCompleteOptions
												  .getObject()
												  .stream()
												  .map(tagToString)
												  .map(this::addQuotes)
												  .collect(Collectors.joining(",")));
			response.render(JavaScriptHeaderItem.forReference(JQueryUIResourceReference.get()));
		}

		response.render(OnDomReadyHeaderItem.forScript("""
															      $('#%1$s').tagEditor({ 
															   		initialTags: [%2$s],
															   		forceLowercase: %5$b,
															   		%6$s
															   		beforeTagSave: function(field, editor, tags, tag, val) {
															   			let addedTag = val;
															   			if (tag !== '') {
															   				// Edited, delete old
															   				let deletedTag = tag;
															   				%4$s
															   			}
															   			%3$s
															   	   	},
															   	   	beforeTagDelete: function(field, editor, tags, val) {
															   	   		let deletedTag = val;
															   	   		%4$s
															   	   	}
															      });
															   """.formatted(tagEditor.getMarkupId(),
																			 getModelObject()
																					 .stream()
																					 .map(tagToString)
																					 .map(this::addQuotes)
																					 .collect(Collectors.joining(",")),
																			 addBehavior.getCallbackScript(),
																			 deleteBehavior.getCallbackScript(),
																			 forceLowercase,
																			 autoComplete

		)));
	}

	@Override
	protected void onDetach() {
		super.onDetach();
		if (autoCompleteOptions != null) {
			autoCompleteOptions.detach();
		}
	}

	/**
	 * Creates a new TagEditor that deals with tags that are just a simple list of Strings
	 *
	 * @param id           The wicket:id
	 * @param tagsModel    The model containing the tags
	 * @param onTagAdded   Callback method for when tags are added
	 * @param onTagDeleted Callback method for when tags are deleted
	 * @return A new editor
	 */
	public static TagEditor<String> newStringTagEditor(@NotNull String id,
													   @NotNull IModel<List<String>> tagsModel,
													   @NotNull SerializableBiConsumer<AjaxRequestTarget, String> onTagAdded,
													   @NotNull SerializableBiConsumer<AjaxRequestTarget, String> onTagDeleted) {
		return new TagEditor<>(id, tagsModel, SerializableFunction.identity(),
							   SerializableFunction.identity()) {
			@Serial
			private static final long serialVersionUID = -4044589724725964868L;

			@Override
			protected void onTagAdded(@NotNull AjaxRequestTarget target, @NotNull String tag) {
				onTagAdded.accept(target, tag);
			}

			@Override
			protected void onTagDeleted(@NotNull AjaxRequestTarget target, @NotNull String tag) {
				onTagDeleted.accept(target, tag);
			}
		};
	}

	private String addQuotes(String s) {
		return "'" + s.replace("'", "\\'") + "'";
	}
}
