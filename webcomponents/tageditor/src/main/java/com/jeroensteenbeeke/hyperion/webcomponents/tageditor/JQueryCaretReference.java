package com.jeroensteenbeeke.hyperion.webcomponents.tageditor;

import org.apache.wicket.markup.head.HeaderItem;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.resource.JavaScriptResourceReference;

import java.io.Serial;
import java.util.List;

/**
 * Wicket resource for JQuery caret
 */
public class JQueryCaretReference extends JavaScriptResourceReference {
	@Serial
	private static final long serialVersionUID = 1272207179042451256L;

	private static final JQueryCaretReference instance = new JQueryCaretReference();

	private JQueryCaretReference() {
		super(JQueryCaretReference.class, "js/jquery.caret.min.js");
	}

	/**
	 * Get the singleton instance
	 * @return A JQueryCaretReference
	 */
	public static JQueryCaretReference get() {
		return instance;
	}

	@Override
	public List<HeaderItem> getDependencies() {
		List<HeaderItem> dependencies = super.getDependencies();

		dependencies.add(JavaScriptHeaderItem.forReference(WebApplication
																   .get()
																   .getJavaScriptLibrarySettings()
																   .getJQueryReference()));

		return dependencies;
	}
}
