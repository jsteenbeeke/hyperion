package com.jeroensteenbeeke.hyperion.webcomponents.google_analytics;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.model.Model;

public class TrackedGALinkPage extends WebPage {
	public TrackedGALinkPage() {
		add(new TrackedGALink("a", "http://www.a.com"));
		add(new TrackedGALink("b", Model.of("http://www.b.com")));
		add(new TrackedGALink("c", "http://www.c.com", "C"));
		add(new TrackedGALink("d", Model.of("http://www.d.com"), Model.of("D")));

	}
}
