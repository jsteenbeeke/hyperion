package com.jeroensteenbeeke.hyperion.webcomponents.google_analytics.resources;

import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.html.WebPage;

public class GoogleAnalyticsHeaderItemPage extends WebPage {
	private final boolean displayFeatures;

	public GoogleAnalyticsHeaderItemPage(boolean displayFeatures) {
		this.displayFeatures = displayFeatures;
	}

	@Override
	public void renderHead(IHeaderResponse response) {
		super.renderHead(response);
		if (displayFeatures) {
			response.render(new GoogleAnalyticsHeaderItem("1337", displayFeatures));
		} else {
			response.render(new GoogleAnalyticsHeaderItem("1337"));
		}
	}
}
