package com.jeroensteenbeeke.hyperion.cousteau.web;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.icons.Icon;
import com.jeroensteenbeeke.hyperion.icons.fontawesome.FontAwesome;
import org.apache.wicket.Page;

import org.jetbrains.annotations.NotNull;

public class BaseAuthenticatedTestPage<T extends DomainObject & UserOwned<U>, U extends IUser>
		extends AbstractAuthenticatedPage<T,U> {
	protected BaseAuthenticatedTestPage(String title, U user) {
		super(title, user);
	}

	protected BaseAuthenticatedTestPage(String title, T object) {
		super(title, object);
	}

	@Override
	protected OverviewContext getOverviewContext() {
		return new TestOverviewContext();
	}

	@Override
	public Class<? extends Page> getLogoutPage() {
		return TestLogoutPage.class;
	}

	protected class TestOverviewContext implements OverviewContext {

		@NotNull
		@Override
		public Icon getIcon() {
			return FontAwesome.home;
		}

		@NotNull
		@Override
		public String getLabel() {
			return "Overview";
		}

		@NotNull
		@Override
		public Page getPage() {
			return BaseAuthenticatedTestPage.this;
		}

		@NotNull
		@Override
		public OverviewContext setIcon(Icon icon) {
			return this;
		}

		@NotNull
		@Override
		public OverviewContext setLabel(String label) {
			return this;
		}

		@Override
		public void detach() {

		}
	}
}
