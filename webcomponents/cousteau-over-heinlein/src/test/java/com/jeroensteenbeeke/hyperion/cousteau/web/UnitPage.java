package com.jeroensteenbeeke.hyperion.cousteau.web;

import java.io.Serializable;

public class UnitPage extends BaseAuthenticatedTestPage {
	public UnitPage(Unit unit) {
		super("Unit View", unit);

		initComponents();
		initComponents();
	}
}

class UnitUser implements IUser {
	@Override
	public Serializable getDomainObjectId() {
		return 1L;
	}

	@Override
	public boolean isSaved() {
		return true;
	}
}

class Unit implements UserOwned<UnitUser> {
	@Override
	public UnitUser getOwningUser() {
		return new UnitUser();
	}

	@Override
	public Serializable getDomainObjectId() {
		return 1L;
	}

	@Override
	public boolean isSaved() {
		return true;
	}

}
