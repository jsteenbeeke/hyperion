package com.jeroensteenbeeke.hyperion.cousteau.web.components;

import com.jeroensteenbeeke.hyperion.cousteau.web.components.buttons.LinkButton;
import org.apache.wicket.markup.html.WebPage;

public class FailingBarTestPage extends WebPage {
	private final HeaderBar top;
	private final BottomBar bottom;

	public FailingBarTestPage() {
		add(top = new HeaderBar("top", "Top"));
		add(bottom = new BottomBar("bottom"));

		top.setTopLeft(new LinkButton("Left") {
			@Override
			public void onClick() {

			}
		});

		top.setTopRight(new LinkButton("Right") {

			@Override
			public void onClick() {

			}
		});

		top.setTitle("!!TOP!!");

		bottom.addButton(new LinkButton("BottomButton") {
			@Override
			public void onClick() {

			}
		});
	}
}
