package com.jeroensteenbeeke.hyperion.cousteau.web;

import org.apache.wicket.util.tester.WicketTester;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class NoInitTest {
	@Test
	public void testPageWithoutInitComponents() {
		WicketTester tester = new WicketTester();

		IllegalStateException ex = assertThrows(IllegalStateException.class, () -> tester.startPage(NoInitPage.class));
		assertThat(ex.getMessage(), equalTo("You must call initComponents() at the end of your constructor"));
	}

	@Test
	public void testAuthenticatedPageWithoutInitComponents() {
		WicketTester tester = new WicketTester();
		IllegalStateException ex = assertThrows(IllegalStateException.class, () -> tester.startPage(new AuthNoInitPage(new Unit())));
		assertThat(ex.getMessage(), equalTo("You must call initComponents() at the end of your constructor"));
	}
}
