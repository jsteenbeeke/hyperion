package com.jeroensteenbeeke.hyperion.cousteau.web.components;


import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.repeater.data.ListDataProvider;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AbstractMobileDataViewTestPage extends WebPage {
	public AbstractMobileDataViewTestPage() {
		List<String> testList = List.of("A", "B", "C");

		add(new AbstractMobileDataView<>("view", "Letters", new ListDataProvider<>
				(testList)) {
			@NotNull
			@Override
			public String getLabel(String object) {
				return object;
			}
		});
	}
}
