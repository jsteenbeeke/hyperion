package com.jeroensteenbeeke.hyperion.cousteau.web.components;

import org.apache.wicket.markup.html.WebPage;

public class FactDisplayPage extends WebPage {
	public FactDisplayPage() {
		add(new FactDisplay("facts", "Facts",
				new FactDisplay.Fact("Fact 1", 5),
				new FactDisplay.Fact("Fact 2", "-")));
	}
}
