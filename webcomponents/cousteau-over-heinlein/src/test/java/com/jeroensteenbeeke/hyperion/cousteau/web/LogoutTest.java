package com.jeroensteenbeeke.hyperion.cousteau.web;

import org.apache.wicket.util.tester.WicketTester;
import org.junit.jupiter.api.Test;

public class LogoutTest {
	@Test
	public void testLogout() {
		WicketTester tester = new WicketTester();

		tester.startPage(new UnitPage(new Unit()));
		tester.assertRenderedPage(UnitPage.class);

		tester.clickLink("header:topright:link");
		tester.assertRenderedPage(TestLogoutPage.class);
	}
}
