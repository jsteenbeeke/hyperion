package com.jeroensteenbeeke.hyperion.cousteau.web.components;

import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.util.tester.WicketTester;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BarTest {
	@Test
	public void testBars() {
		WicketTester tester = new WicketTester();

		tester.startPage(BarTestPage.class);
		tester.assertRenderedPage(BarTestPage.class);

		tester.assertContains(">!!TOP!!</a>");
		tester.assertContainsNot(">TOP</a>");
		tester.assertContains("BottomButton");
	}

	@Test
	public void testFailure() {
		final String expectedMessage = "initComponents not called on com.jeroensteenbeeke.hyperion.cousteau.web.components.HeaderBar";

		WicketTester tester = new WicketTester();

		var ex = assertThrows(WicketRuntimeException.class, () -> tester.startPage(FailingBarTestPage.class));

		assertThat(ex,
				new TypeSafeDiagnosingMatcher<>() {
					@Override
					public void describeTo(Description description) {
						description.appendText("WicketRuntimeException caused by IllegalStateException with message '");
						description.appendText(expectedMessage);
						description.appendText("'");
					}

					@Override
					protected boolean matchesSafely(WicketRuntimeException item, Description mismatchDescription) {
						Throwable ex = item;

						while (ex != null && ex.getCause() != null) {
							ex = ex.getCause();
						}

						String actualMessage = ex != null ? ex.getMessage() : (item != null ? item.getMessage() : "");

						mismatchDescription.appendText("WicketRuntimeException ");
						mismatchDescription.appendText(ex != null ? "caused by " + ex.getClass().getSimpleName() : "without a cause");
						mismatchDescription.appendText(" with message '");
						mismatchDescription.appendText(actualMessage);
						mismatchDescription.appendText("'");

						return ex instanceof IllegalStateException
								&& actualMessage.equals(expectedMessage);
					}
				}
		);
	}
}
