/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cousteau.web.components;


import com.jeroensteenbeeke.hyperion.cousteau.web.components.buttons.BarButton;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Bottom bar, displayed at the bottom of the page
 */
public class BottomBar extends AbstractBar {
	private static final long serialVersionUID = 1L;

	private List<BarButton> buttons = new LinkedList<BarButton>();

	/**
	 * Constructor
	 * @param id The wicket:id
	 */
	public BottomBar(String id) {
		super(id);
	}

	/**
	 * Adds a button
	 * @param button The button
	 */
	public final void addButton(BarButton button) {
		buttons.add(button);
	}

	/**
	 * Get a list of buttons
	 * @return The buttons
	 */
	protected final List<BarButton> getButtons() {
		return Collections.unmodifiableList(buttons);
	}

	@Override
	public final void initComponents() {
		super.initComponents();

		renderButtons("buttons");

		setVisible(!getButtons().isEmpty());
	}

	private void renderButtons(String id) {
		add(new ListView<>(id, getButtons()) {

			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<BarButton> item) {
				item.add(BottomBar.this.render("link",
											   item.getModelObject()));

			}

		}.setVisible(!getButtons().isEmpty()));
	}

}
