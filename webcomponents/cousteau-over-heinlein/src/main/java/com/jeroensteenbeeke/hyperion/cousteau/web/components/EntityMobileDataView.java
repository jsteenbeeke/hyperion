/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cousteau.web.components;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.DAO;
import com.jeroensteenbeeke.hyperion.meld.SearchFilter;
import com.jeroensteenbeeke.hyperion.meld.web.EntityEncapsulator;

/**
 * DataView for entities. Essentially an AbstractMobileDataView with convenience constructor
 * @param <T> The type of entity
 * @param <F> The type of filter
 */
public abstract class EntityMobileDataView<T extends DomainObject, F extends SearchFilter<T,F>> extends
		AbstractMobileDataView<T> {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param title The panel title
	 * @param filter The search filter
	 * @param dao The DAO
	 */
	public EntityMobileDataView(String id, String title,
			F filter, DAO<T,F> dao) {
		super(id, title, EntityEncapsulator.createProvider(filter, dao));
	}
}
