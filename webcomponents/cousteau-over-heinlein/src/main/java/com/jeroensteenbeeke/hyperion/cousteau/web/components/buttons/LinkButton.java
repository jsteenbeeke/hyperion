/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cousteau.web.components.buttons;

import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.Model;

/**
 * Bar button that behaves like a regular Wicket link
 */
public abstract class LinkButton extends AbstractButton<LinkButton> {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 * @param label The button label
	 */
	public LinkButton(String label) {

		super(new Model<>(label));
	}

	@Override
	public Link<?> makeLink(String id) {
		return new Link<Void>(id) {
			private static final long serialVersionUID = 7028822204281645791L;

			@Override
			public void onClick() {
				LinkButton.this.onClick();

			}
		};
	}

	/**
	 * Method called when the link is clicked
	 */
	public abstract void onClick();
}
