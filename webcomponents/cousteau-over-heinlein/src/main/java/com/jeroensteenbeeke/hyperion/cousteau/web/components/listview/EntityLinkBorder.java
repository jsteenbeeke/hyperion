/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cousteau.web.components.listview;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.web.EntityEncapsulator;

/**
 * LinkBorder that takes an entity as object
 * @param <T> The entity type
 */
public abstract class EntityLinkBorder<T extends DomainObject> extends
		LinkBorder<T> {
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param entity The entity
	 * @param icon The icon to show with the border
	 */
	public EntityLinkBorder(String id, T entity, String icon) {
		super(id, EntityEncapsulator.createModel(entity), icon);
	}

	/**
	 * Constructor
	 * @param id The wicket:id
	 * @param entity The entity
	 */
	public EntityLinkBorder(String id, T entity) {
		super(id, EntityEncapsulator.createModel(entity));
	}

}
