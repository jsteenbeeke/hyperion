/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cousteau.web.components.buttons;

import org.apache.wicket.model.IDetachable;
import org.apache.wicket.model.IModel;

/**
 * Button to display in a bottom or header bar
 */
public interface BarButton extends IDetachable {
	/**
	 * Get (a model for) the label of the button
	 * @return The label
	 */
	IModel<String> getLabel();

	/**
	 * Create a LinkPanel for the button
	 * @param id The wicket:id
	 * @return A LinkPanel
	 */
	LinkPanel createLinkPanel(String id);
}
