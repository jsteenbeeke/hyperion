/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.cousteau.web.components.buttons;

import com.jeroensteenbeeke.hyperion.web.components.quickform.QuickForm;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.SubmitLink;
import org.apache.wicket.markup.html.link.AbstractLink;
import org.apache.wicket.model.Model;

/**
 * Bar button that submits a form or QuickForm
 */
public class FormSubmitButton extends AbstractButton<FormSubmitButton> {
	private static final long serialVersionUID = 1L;

	private Form<?> form;

	/**
	 * Create a submit button for a QuickForm
	 * @param label The label for the button
	 * @param form The QuickForm to submit
	 */
	public FormSubmitButton(String label, QuickForm<?> form) {
		this(label, form.getInternalForm());
	}

	/**
	 * Create a submit button for a regular form
	 * @param label The label for the button
	 * @param form The form to submit
	 */
	public FormSubmitButton(String label, Form<?> form) {
		super(Model.of(label));
		this.form = form;
	}

	@Override
	public AbstractLink makeLink(String id) {
		SubmitLink submitLink = new SubmitLink(id, form);
		form.setDefaultButton(submitLink);
		return submitLink;
	}
}
