package com.jeroensteenbeeke.hyperion.heinlein.web.pages.entity;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.DAO;
import com.jeroensteenbeeke.hyperion.meld.SearchFilter;
import com.jeroensteenbeeke.hyperion.meld.filter.IComparableFilterField;
import com.jeroensteenbeeke.hyperion.meld.filter.IFilterField;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Option;

import org.jetbrains.annotations.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestDAO<T extends DomainObject,F extends SearchFilter<T,F>> implements DAO<T,F> {
	private final Set<T> deleted = new HashSet<>();

	private final Set<T> updated = new HashSet<>();

	private final Set<T> saved = new HashSet<>();

	private final Set<T> evicted = new HashSet<>();

	private int flushCalled = 0;

	@Override
	public long countAll() {
		return 0;
	}

	@NotNull
	@Override
	public T delete(@NotNull T object) {
		deleted.add(object);
		return object;
	}

	@Override
	public int deleteByFilter(@NotNull F filter) {
		return 0;
	}

	@Override
	public T evict(T object) {
		evicted.add(object);
		return object;
	}

	@Override
	public Seq<T> findAll() {
		return List.empty();
	}

	@Override
	public boolean exists(F filter) {
		return false;
	}

	@Override
	public Option<T> load(Serializable id) {
		return null;
	}

	@Override
	public T save(T object) {
		saved.add(object);
		return object;
	}

	@Override
	public T update(T object) {
		updated.add(object);
		return object;
	}

	@Override
	public void flush() {
		flushCalled++;
	}

	@Override
	public Option<T> getUniqueByFilter(F filter) {
		return Option.none();
	}

	@Override
	public long countByFilter(F filter) {
		return 0;
	}

	@Override
	public Seq<T> findByFilter(F filter) {
		return List.empty();
	}

	@Override
	public Seq<T> findByFilter(F filter, long offset, long count) {
		return List.empty();
	}

	@Override
	public Stream<T> streamAll() {
		return findAll().toJavaStream();
	}

	@Override
	public Stream<T> streamByFilter(F filter) {
		return findByFilter(filter).toJavaStream();
	}

	@Override
	public Stream<T> streamByFilter(F filter, long offset, long count) {
		return findByFilter(filter, offset, count).toJavaStream();
	}

	@Override
	public <FT extends Comparable<? super FT> & Serializable> Option<FT> max(IComparableFilterField<T, FT, ? extends F> field) {
		return Option.none();
	}

	@Override
	public <FT extends Comparable<? super FT> & Serializable> Option<FT> min(IComparableFilterField<T, FT, ? extends F> field) {
		return Option.none();
	}

	@Override
	public <FT extends Number & Comparable<? super FT> & Serializable> Option<FT> sum(IComparableFilterField<T, FT, ? extends F> field) {
		return Option.none();
	}

	@Override
	public <FT extends Serializable> Option<FT> property(IFilterField<T, FT, ? extends F> field) {
		return Option.none();
	}

	@Override
	public <FT extends Serializable> Seq<FT> properties(IFilterField<T, FT,? extends F> field) {
		return List.empty();
	}

	public void assertDeleted(T object) {
		assertTrue(
				deleted.contains(object),
				String.format("%s %s was deleted", object.getClass(), object.getDomainObjectId()));
	}

	public void assertUpdated(T object) {
		assertTrue(
				updated.contains(object),
				String.format("%s %s was updated", object.getClass(), object.getDomainObjectId()));
	}

	public void assertSaved(T object) {
		assertTrue(
				saved.contains(object),
				String.format("%s %s was saved", object.getClass(), object.getDomainObjectId()));
	}

	public void assertEvicted(T object) {
		assertTrue(
				evicted.contains(object),
				String.format("%s %s was deleted", object.getClass(), object.getDomainObjectId()));
	}

	public void assertFlushed(int amount) {
		assertEquals(flushCalled, amount, String.format("Flush was called %d times", amount));
	}
}
