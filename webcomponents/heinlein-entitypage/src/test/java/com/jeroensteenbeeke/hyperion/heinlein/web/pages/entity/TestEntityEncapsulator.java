package com.jeroensteenbeeke.hyperion.heinlein.web.pages.entity;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.DAO;
import com.jeroensteenbeeke.hyperion.meld.SearchFilter;
import com.jeroensteenbeeke.hyperion.meld.web.IEntityEncapsulatorFactory;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.util.ListModel;

import org.jetbrains.annotations.NotNull;
import java.util.List;

public class TestEntityEncapsulator implements IEntityEncapsulatorFactory {
	@NotNull
	@Override
	public <T extends DomainObject, F extends SearchFilter<T, F>> IDataProvider<T> createDataProvider(@NotNull F filter, @NotNull DAO<T, F> dao) {
		return new ListDataProvider<>(dao.findByFilter(filter).asJava());
	}

	@NotNull @Override
	public <T extends DomainObject> IModel<T> createModel(@NotNull T object) {
		return Model.of(object);
	}

	@NotNull @Override
	public <T extends DomainObject> IModel<List<T>> createListModel(
			@NotNull List<T> list) {
		return new ListModel<>(list);
	}

	@Override
	public <T extends DomainObject> IModel<T> createModel(Class<T> modelClass) {
		return Model.of();
	}
}
