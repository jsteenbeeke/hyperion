package com.jeroensteenbeeke.hyperion.heinlein.web.pages.entity;

import com.jeroensteenbeeke.hyperion.data.BaseDomainObject;
import com.jeroensteenbeeke.lux.ActionResult;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestEntityPage<T extends BaseDomainObject> extends BSEntityFormPage<T> {
	private final Set<T> saved = new HashSet<>();

	private final Set<T> cancelled = new HashSet<>();

	private final Set<T> deleted = new HashSet<>();

	private String deleteRefusalReason = null;

	public TestEntityPage(BSEntityPageSettings<T> settings) {
		super(settings);
	}

	public void disableDeletion(String reason) {
		this.deleteRefusalReason = reason;
	}

	@Override
	protected void onSaved(T entity) {
		saved.add(entity);
	}

	@Override
	protected void onCancel(T entity) {
		cancelled.add(entity);
	}

	@Override
	protected ActionResult onBeforeDelete(T entity) {
		if (deleteRefusalReason != null) {
			return ActionResult.error(deleteRefusalReason);
		}

		deleted.add(entity);

		return ActionResult.ok();
	}

	public void assertSaved(T object) {
		assertTrue(
				saved.contains(object),
				String.format("%s %s was saved", object.getClass(), object.getDomainObjectId()));
	}

	public void assertNotSaved(T object) {
		assertFalse(
				saved.contains(object),
				String.format("%s %s was not saved", object.getClass(), object.getDomainObjectId()));
	}

	public void assertCancelled(T object) {
		assertTrue(
				cancelled.contains(object),
				String.format("%s %s was cancelled", object.getClass(), object.getDomainObjectId()));
	}

	public void assertNotCancelled(T object) {
		assertFalse(
				cancelled.contains(object),
				String.format("%s %s was not cancelled", object.getClass(), object.getDomainObjectId()));
	}

	public void assertDeleted(T object) {
		assertTrue(
				deleted.contains(object),
				String.format("%s %s was deleted", object.getClass(), object.getDomainObjectId()));
	}

	public void assertNotDeleted(T object) {
		assertFalse(
				deleted.contains(object),
				String.format("%s %s was not deleted", object.getClass(), object.getDomainObjectId()));
	}
}
