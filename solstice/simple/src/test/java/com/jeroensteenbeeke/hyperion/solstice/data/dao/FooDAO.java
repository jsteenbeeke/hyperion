package com.jeroensteenbeeke.hyperion.solstice.data.dao;

import com.jeroensteenbeeke.hyperion.meld.DAO;
import com.jeroensteenbeeke.hyperion.solstice.data.entities.Foo;
import com.jeroensteenbeeke.hyperion.solstice.data.entities.filter.FooFilter;

public interface FooDAO extends DAO<Foo, FooFilter> {
}
