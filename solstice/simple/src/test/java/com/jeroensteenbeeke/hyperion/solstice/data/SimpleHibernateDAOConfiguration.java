package com.jeroensteenbeeke.hyperion.solstice.data;

import com.jeroensteenbeeke.hyperion.solstice.spring.db.EnableSolstice;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableSolstice(entityBasePackage = "com.jeroensteenbeeke.hyperion.solstice.data.entities",
		liquibaseChangelog = "classpath:/com/jeroensteenbeeke/hyperion/solstice/data/entities/liquibase/db.changelog-master.xml")
@EnableTransactionManagement
@ComponentScan(basePackages = "com.jeroensteenbeeke.hyperion.solstice.data.dao.hibernate", lazyInit = true,
		scopedProxy = ScopedProxyMode.INTERFACES)
public class SimpleHibernateDAOConfiguration {

}


