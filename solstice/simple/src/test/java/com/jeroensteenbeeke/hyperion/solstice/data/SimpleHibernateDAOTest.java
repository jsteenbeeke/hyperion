package com.jeroensteenbeeke.hyperion.solstice.data;

import com.jeroensteenbeeke.hyperion.solstice.data.dao.FooDAO;
import com.jeroensteenbeeke.hyperion.solstice.data.entities.Foo;
import com.jeroensteenbeeke.hyperion.solstice.data.entities.filter.FooFilter;
import io.vavr.collection.Array;
import io.vavr.control.Option;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class SimpleHibernateDAOTest {
	private ApplicationContext context;

	@BeforeEach
	public void initSpring() {
		System.setProperty("database.username", "sa");
		System.setProperty("database.password", "-");
		System.setProperty("database.driver", "org.h2.Driver");
		System.setProperty("database.poolsize", "5");
		System.setProperty("database.timeout", "30000");
		System.setProperty("database.dialect", "org.hibernate.dialect.H2Dialect");
		System.setProperty("database.sourceClass", "org.h2.jdbcx.JdbcDataSource");
		System.setProperty("database.type", "h2");
		System.setProperty("database.url", "jdbc:h2:mem:SimpleHibernateDAOTest");

		context = new AnnotationConfigApplicationContext(SimpleHibernateDAOConfiguration.class);


	}

	@Test
	public void testDAO() {
		FooDAO fooDAO = context.getBean(FooDAO.class);

		assertThat(fooDAO, notNullValue());
		assertThat(fooDAO.countAll(), equalTo(0L));

		Foo one, two, three, four, five;

		fooDAO.save(one = createFoo("A"));
		fooDAO.save(two = createFoo("A"));
		fooDAO.save(three = createFoo("A"));
		fooDAO.save(four = createFoo("B"));
		fooDAO.save(five = createFoo("B"));

		assertThat(fooDAO.countAll(), equalTo(5L));

		assertThat(fooDAO.countByFilter(new FooFilter().dtype("A")), equalTo(3L));
		assertThat(fooDAO.countByFilter(new FooFilter().dtype("B")), equalTo(2L));

		assertThat(fooDAO.findByFilter(new FooFilter()
											   .id()
											   .orderBy(true)), equalTo(Array.of(one, two, three, four, five)));

		assertThat(fooDAO.findByFilter(new FooFilter()
											   .id()
											   .orderBy(true), 1, 3), equalTo(Array.of(two, three, four)));

		five.setDtype("C");
		fooDAO.update(five);

		assertThat(fooDAO.countByFilter(new FooFilter().dtype("A")), equalTo(3L));
		assertThat(fooDAO.countByFilter(new FooFilter().dtype("B")), equalTo(1L));
		assertThat(fooDAO.countByFilter(new FooFilter().dtype("C")), equalTo(1L));

		assertThat(fooDAO.deleteByFilter(new FooFilter().dtype("A")), equalTo(3));

		assertThat(fooDAO.countByFilter(new FooFilter().dtype("A")), equalTo(0L));
		assertThat(fooDAO.countByFilter(new FooFilter().dtype("B")), equalTo(1L));
		assertThat(fooDAO.countByFilter(new FooFilter().dtype("C")), equalTo(1L));

		fooDAO.delete(five);

		assertThat(fooDAO.countByFilter(new FooFilter().dtype("A")), equalTo(0L));
		assertThat(fooDAO.countByFilter(new FooFilter().dtype("B")), equalTo(1L));
		assertThat(fooDAO.countByFilter(new FooFilter().dtype("C")), equalTo(0L));

		assertThat(fooDAO.property(new FooFilter().dtype("B").dtype()), equalTo(Option.some("B")));
		assertThat(fooDAO.properties(new FooFilter().dtype("B").dtype()), equalTo(Array.of("B")));
	}

	private Foo createFoo(String dtype) {
		Foo foo = new Foo();
		foo.setDtype(dtype);

		return foo;
	}
}
