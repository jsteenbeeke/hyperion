package com.jeroensteenbeeke.hyperion.solstice.spring.resteasy;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ConditionalProxyProviderTest implements ConditionalProxyProvider {

	@Test
	public void testProxyCreation() {
		AwesomeAPI proxy = proxyOf(AwesomeAPI.class);

		assertNotNull(proxy);

		AwesomerAPI proxy2 = proxyOf(AwesomerAPI.class);
		assertNotNull(proxy2);

		TestMode.init(Mockito::mock);

		proxy = proxyOf(AwesomeAPI.class);

		assertNotNull(proxy);

		proxy2 = proxyOf(AwesomerAPI.class);
		assertNotNull(proxy2);
	}

	@Override
	public HyperionResteasyContext getResteasyContext() {
		return SimpleHyperionResteasyContext.multiThreaded().forBaseURL("http://localhost:8080/")
												   .withMaxConnections(12)
												   .withMaxConnectionsPerRoute(1)
												   .build();
	}


}
