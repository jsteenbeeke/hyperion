package com.jeroensteenbeeke.hyperion.solstice.spring.resteasy;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

/**
 * Convenience interface to quickly create RestEasy client proxies, with sensible defaults, and
 * allowing for extensions. This interface should generally be implemented by your Spring
 * configuration
 */
public interface ResteasyProxyProvider {
	/**
	 * Returns the ResteasyContext for communicating with the backend
	 * @return The context
	 */
	HyperionResteasyContext getResteasyContext();

	/**
	 * Creates a proxy of the given class, relative to the service URL
	 * @param serviceClass The class to proxy
	 * @param <T> The type of the class
	 * @return An instance of T that is proxied to perform rest calls
	 */
	default <T> T proxyOf(Class<T> serviceClass) {
		HyperionResteasyContext context = getResteasyContext();

		ResteasyClient client = context.client();
		ResteasyWebTarget target = client.target(context.getBackendBaseUrl());
		target = context.decorateTarget(target);

		return Proxyfier.proxy(serviceClass, target);
	}
}
