package com.jeroensteenbeeke.hyperion.solstice.spring.resteasy;

import jakarta.ws.rs.client.ClientRequestContext;
import jakarta.ws.rs.client.ClientRequestFilter;

/**
 * JAX-RS client filter that applies custom headers
 */
public class CustomHeaderFilter implements ClientRequestFilter {

	@Override
	public void filter(ClientRequestContext requestContext) {
		CustomHeaderProviderRegistry.instance.getProviders().forEach(
			provider -> provider.addHeaders(requestContext.getUri(), requestContext.getHeaders()));
	}

}
