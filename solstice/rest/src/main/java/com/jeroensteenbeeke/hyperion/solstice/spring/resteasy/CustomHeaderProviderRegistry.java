package com.jeroensteenbeeke.hyperion.solstice.spring.resteasy;

import java.util.LinkedList;
import java.util.List;

/**
 * Central registry for JAX-RS header providers
 */
public enum CustomHeaderProviderRegistry {
	/**
	 * Singleton
	 */
	instance;

	private final List<CustomHeaderProvider> providers;

	CustomHeaderProviderRegistry() {
		providers = new LinkedList<>();
	}

	/**
	 * Registers a custom header provider
	 *
	 * @param provider The provider to use
	 */
	public void registerProvider(CustomHeaderProvider provider) {
		this.providers.add(provider);
	}

	/**
	 * Get an immutable list of the current providers
	 *
	 * @return The providers
	 */
	List<CustomHeaderProvider> getProviders() {
		return List.copyOf(providers);
	}
}
