package com.jeroensteenbeeke.hyperion.solstice.spring.resteasy;

import io.vavr.Function1;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.engines.ApacheHttpClient43Engine;

import org.jetbrains.annotations.NotNull;

/**
 * Simple implementation of the HyperionResteasyContext, either using a default ResteasyClient, or one
 * configured as desired by the application
 */
public class SimpleHyperionResteasyContext implements HyperionResteasyContext {
	private final String backendBaseUrl;

	private final ResteasyClient client;

	/**
	 * Creates a context with custom settings (supplied as a function on the builder
	 *
	 * @param backendBaseUrl The backend base URL
	 * @param client         The client to use
	 */
	private SimpleHyperionResteasyContext(@NotNull String backendBaseUrl, ResteasyClient client) {
		this.backendBaseUrl = backendBaseUrl;
		this.client = client;
	}

	@NotNull
	@Override
	public ResteasyClient client() {
		return client;
	}

	@NotNull
	@Override
	public final String getBackendBaseUrl() {
		return backendBaseUrl;
	}

	/**
	 * Creates a builder for creating a thread-safe client context
	 * @return A builder
	 */
	public static ForBaseURL multiThreaded() {
		return url -> maxConnections -> maxConnectionsPerRoute -> new ContextBuilder(url).withConfig(
				config -> {
					PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
					connectionManager.setMaxTotal(maxConnections);
					connectionManager.setDefaultMaxPerRoute(maxConnectionsPerRoute);

					return config.httpEngine(new ApacheHttpClient43Engine(HttpClientBuilder
																				  .create()
																				  .setConnectionManager(connectionManager)
																				  .build()))
								 .connectionPoolSize(maxConnections);
				}
		);
	}

	/**
	 * Builder step for obtaining the backend base URL
	 */
	@FunctionalInterface
	public interface ForBaseURL {
		/**
		 * Sets the base URL
		 * @param baseURL The base URL
		 * @return The next builder step
		 */
		@NotNull
		WithMaxConnections forBaseURL(@NotNull String baseURL);
	}

	/**
	 * Builder step for setting the max number of client connections
	 */
	@FunctionalInterface
	public interface WithMaxConnections {
		/**
		 * Sets the max number of total connections
		 * @param maxConnections The max number of connections
		 * @return The next builder step
		 */
		WithMaxConnectionsPerRoute withMaxConnections(int maxConnections);
	}

	/**
	 * Builder step for setting the max number of client connections per route
	 */
	@FunctionalInterface
	public interface WithMaxConnectionsPerRoute {
		/**
		 * Sets the max number of connections per route
		 * @param maxConnectionsPerRoute The number of connections per route
		 * @return The finalizer
		 */
		ContextBuilder withMaxConnectionsPerRoute(int maxConnectionsPerRoute);

		/**
		 * Sets the default max number of connections per route
		 * @return The finalizer
		 */
		default ContextBuilder withDefaultMaxConnectionsPerRoute() {
			return withMaxConnectionsPerRoute(2);
		}
	}

	/**
	 * Finalizer for creating the context. Allows final tweaks to the RESTEasy client builder
	 */
	public static class ContextBuilder {
		private final String baseURL;

		private final ResteasyClientBuilder builder;

		private ContextBuilder(@NotNull String baseURL) {
			this.baseURL = baseURL;
			this.builder = (ResteasyClientBuilder) ResteasyClientBuilder.newBuilder();
		}

		private ContextBuilder(String baseURL, ResteasyClientBuilder builder) {
			this.baseURL = baseURL;
			this.builder = builder;
		}

		/**
		 * Tweaks the RESTEasy client builder with the given configuration tweaks
		 * @param builderConfiguration A function that provides the builder, allowing the user to tweak the config
		 * @return A new ContextBuilder with the updated information
		 */
		public ContextBuilder withConfig(@NotNull Function1<ResteasyClientBuilder, ResteasyClientBuilder> builderConfiguration) {
			return new ContextBuilder(baseURL, builderConfiguration.apply(builder));
		}

		/**
		 * Creates the new context based on the given config options
		 * @return A new context
		 */
		public SimpleHyperionResteasyContext build() {
			ResteasyClient client = builder.build();
			client.register(CustomHeaderFilter.class);

			return new SimpleHyperionResteasyContext(baseURL, client);
		}
	}
}
