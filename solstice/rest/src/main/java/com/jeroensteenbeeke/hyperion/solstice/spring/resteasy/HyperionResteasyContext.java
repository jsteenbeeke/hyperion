package com.jeroensteenbeeke.hyperion.solstice.spring.resteasy;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import org.jetbrains.annotations.NotNull;

/**
 * Context class for Resteasy-based frontends. Provides data to create proxied instances of JAX-RS interfaces
 */
public interface HyperionResteasyContext {
	/**
	 * Returns the client for communicating with the backend
	 * @return The client
	 */
	@NotNull
	ResteasyClient client();

	/**
	 * Returns the base URL of the service to communicate with
	 * @return The URL of the service, to which all resource paths are relative
	 */
	@NotNull
	String getBackendBaseUrl();

	/**
	 * Decorator method to allow the implementing client to further configure the
	 * ResteasyWebTarget
	 * @param target The target to decorate
	 * @return The decorated target
	 */
	default ResteasyWebTarget decorateTarget(ResteasyWebTarget target) {
		return target;
	}


}
