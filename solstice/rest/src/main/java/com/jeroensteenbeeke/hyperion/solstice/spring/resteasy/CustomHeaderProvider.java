package com.jeroensteenbeeke.hyperion.solstice.spring.resteasy;

import org.jetbrains.annotations.NotNull;

import jakarta.ws.rs.core.MultivaluedMap;
import java.net.URI;

/**
 * Client-side header provider for JAX-RS calls
 */
public interface CustomHeaderProvider {
	/**
	 * Add headers to the given map of headers
	 * @param headers The current headers. Add custom headers to this parameter
	 * @deprecated Use {@link CustomHeaderProvider#addHeaders(URI, MultivaluedMap)}
	 */
	@Deprecated
	default void addHeaders(@NotNull MultivaluedMap<String, Object> headers) {

	}

	/**
	 * Add headers to the given map of headers
	 * @param headers The current headers. Add custom headers to this parameter
	 */
	default void addHeaders(@NotNull URI uri, @NotNull MultivaluedMap<String,Object> headers) {
		addHeaders(headers);
	}
}
