package com.jeroensteenbeeke.hyperion.solstice.spring.resteasy;

/**
 * ProxyProvider specialization that checks whether or not we're running in
 * test mode, and if we are, defaults to a mocked proxy rather than
 * an actual proxy
 */
public interface ConditionalProxyProvider extends ResteasyProxyProvider {
	@Override
	default <T> T proxyOf(Class<T> serviceClass) {
		if (TestMode.isEnabled()) {
			return TestMode.getInstance().proxy(serviceClass);
		}

		return ResteasyProxyProvider.super.proxyOf(serviceClass);
	}
}
