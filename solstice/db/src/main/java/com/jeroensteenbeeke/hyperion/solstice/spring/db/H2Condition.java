package com.jeroensteenbeeke.hyperion.solstice.spring.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import org.jetbrains.annotations.NotNull;

/**
 * Spring condition for enabling H2 database connectivity in Solstice
 */
public class H2Condition implements Condition {
	private static final Logger log = LoggerFactory.getLogger(H2Condition.class);

	@Override
	public boolean matches(@NotNull ConditionContext context,
						   @NotNull AnnotatedTypeMetadata metadata) {
		boolean satisfied = "h2".equals(context.getEnvironment().getProperty("database.type"));
		log.debug("H2Condition: {}", satisfied);
		return satisfied;
	}

}
