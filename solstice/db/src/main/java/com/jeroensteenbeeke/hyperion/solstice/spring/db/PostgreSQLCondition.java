package com.jeroensteenbeeke.hyperion.solstice.spring.db;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.util.Set;

/**
 * Spring condition that enables PostgreSQL as database in Solstice
 */
public class PostgreSQLCondition implements Condition {
	private static final Logger log = LoggerFactory.getLogger(PostgreSQLCondition.class);

	private static final Set<String> KEYS = Set.of("pg", "postgres",
			"psql", "postgresql");

	@Override
	public boolean matches(
			@NotNull ConditionContext context,
			@NotNull AnnotatedTypeMetadata metadata) {
		boolean contains = KEYS.contains(context.getEnvironment().getProperty("database.type"));
		log.debug("PostgreSQLCondition: {}", contains);
		return contains;
	}

}
