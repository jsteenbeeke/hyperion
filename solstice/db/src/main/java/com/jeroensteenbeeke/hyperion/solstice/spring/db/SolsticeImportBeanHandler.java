package com.jeroensteenbeeke.hyperion.solstice.spring.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.AnnotatedGenericBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

import org.jetbrains.annotations.NotNull;
import java.util.Map;

/**
 * Handler class for the {@code @EnableSolstice} annotation
 */
class SolsticeImportBeanHandler implements ImportBeanDefinitionRegistrar {
private static final Logger log = LoggerFactory.getLogger(SolsticeImportBeanHandler.class);

	private static final String SOLSTICE_ANNOTATION_FQDN =
		EnableSolstice.class.getName();

	@Override
	public void registerBeanDefinitions(@NotNull AnnotationMetadata importingClassMetadata,
										@NotNull BeanDefinitionRegistry registry) {
		log.info("Enabling Solstice DB");

		if (importingClassMetadata.hasAnnotation(SOLSTICE_ANNOTATION_FQDN)) {
			Map<String, Object> solsticeConfig =
				importingClassMetadata.getAnnotationAttributes(SOLSTICE_ANNOTATION_FQDN);

			if (solsticeConfig != null) {
				SolsticeDatabaseConfiguration config = new SolsticeDatabaseConfiguration
					((String) solsticeConfig.get(EnableSolstice_.METHOD_ENTITY_BASE_PACKAGE), (String)
						solsticeConfig.get(EnableSolstice_.METHOD_LIQUIBASE_CHANGELOG),
					 (Boolean)
						 solsticeConfig.get(EnableSolstice_.METHOD_ENABLE_HIBERNATE_SEARCH)
					);

				AnnotatedGenericBeanDefinition beandef = new AnnotatedGenericBeanDefinition
					(SolsticeDatabaseConfiguration.class);
				beandef.setInstanceSupplier(() -> config);
				registry.registerBeanDefinition("solstice-data-config", beandef);

				registry.registerBeanDefinition("solstice-config-class", new RootBeanDefinition
					(SolsticeConfig.class));
			} else {
				throw new IllegalStateException("SolsticeImportBeanHandler used directly without " +
													"@EnableSolstice annotation");
			}
		} else {
			throw new IllegalStateException("SolsticeImportBeanHandler used directly without " +
												"@EnableSolstice annotation");
		}
	}
}
