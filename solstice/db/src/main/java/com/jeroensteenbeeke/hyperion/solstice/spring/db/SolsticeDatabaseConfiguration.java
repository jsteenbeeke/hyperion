package com.jeroensteenbeeke.hyperion.solstice.spring.db;

import io.vavr.control.Option;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * DTO for database configuration, as specified by the {@code @EnableSolstice} annotation
 */
public class SolsticeDatabaseConfiguration {
	private static final Logger log = LoggerFactory.getLogger(SolsticeDatabaseConfiguration.class);

	private final String entityBasePackage;

	private final String liquibaseChangelog;

	private final boolean enableHibernateSearch;

	/**
	 * Constructor
	 * @param entityBasePackage the entity base package
	 * @param liquibaseChangelog The location of the liquibase changelog
	 * @param enableHibernateSearch Whether or not to enable Hibernate Search
	 */
	SolsticeDatabaseConfiguration(String entityBasePackage, String liquibaseChangelog, boolean enableHibernateSearch) {
		log.info("Created SolsticeDatabaseConfiguration");
		this.entityBasePackage = entityBasePackage;
		this.liquibaseChangelog = liquibaseChangelog;
		this.enableHibernateSearch = enableHibernateSearch;
	}

	/**
	 * Gets the base (root) package entities are defined in
	 * @return The fully qualified domain name of the package
	 */
	@NotNull
	public String getEntityBasePackage() {
		return entityBasePackage;
	}

	/**
	 * Gets the location of the Liquibase changelog
	 * @return Optionally the changelog
	 */
	@NotNull
	public Option<String> getLiquibaseChangelog() {
		return Option.of(liquibaseChangelog);
	}

	/**
	 * Indicates whether or not Hibernate search should be enabled
	 * @return {@code true} if Hibernate Search should be enabled. {@code false} otherwise
	 */
	public boolean isEnableHibernateSearch() {
		return enableHibernateSearch;
	}
}
