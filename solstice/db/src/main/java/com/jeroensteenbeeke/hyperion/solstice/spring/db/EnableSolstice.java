package com.jeroensteenbeeke.hyperion.solstice.spring.db;

import com.jeroensteenbeeke.hyperion.annotation.Reflectable;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * Spring activator annotation that enables Solstice (Spring + Hibernate/JPA with Liquibase) functionality
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(SolsticeImportBeanHandler.class)
@Reflectable
public @interface EnableSolstice {
	/**
	 * The base package in which the JPA entities are defined
	 * @return A fully qualified package name
	 */
	String entityBasePackage();

	/**
	 * Optional: the location of the main liquibase changelog file
	 *
	 * Example: classpath:/your/base/package/entities/liquibase/db.changelog-master.xml
	 *
	 * @return The liquibase changelog path.
	 */
	String liquibaseChangelog() default "";

	/**
	 * Whether or not Hibernate properties for Hibernate search should be enabled. This does not trigger automatic
	 * inclusion of the Hibernate Search dependency, it only sets the proper Hibernate properties in the Hibernate
	 * config.
	 * @return {@code true} if Hibernate search should be configured. {@code false} otherwise
	 */
	boolean enableHibernateSearch() default false;
}
