package com.jeroensteenbeeke.hyperion.solstice.spring.db;

import com.jeroensteenbeeke.hyperion.solstice.spring.ApplicationMetadataStore;
import io.vavr.collection.Array;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

/**
 * Spring condition for determining if the metadata store should be configured
 */
public class WritableHyperionConfigDirCondition implements Condition {

	@Override
	public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
		String specificDir = context.getEnvironment().getProperty("hyperion.metadata.store.dir");
		String configDir = context.getEnvironment().getProperty("hyperion.configdir");
		String userHome = System.getProperty("user.home");

		String storePath = Array.of(specificDir, configDir).find(Objects::nonNull)
				.getOrElse(() -> userHome + File.separator + ".hyperion")  + File.separator + ApplicationMetadataStore.STORE_FILENAME;

		Path store = Path.of(storePath);

		if (Files.exists(store)) {
			return Files.isReadable(store) && Files.isWritable(store);
		} else {
			Path directory = store.getParent();

			return Files.isDirectory(directory) && Files.isReadable(directory) && Files.isWritable(directory);
		}
	}
}
