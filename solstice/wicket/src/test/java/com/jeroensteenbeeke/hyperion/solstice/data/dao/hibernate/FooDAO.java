package com.jeroensteenbeeke.hyperion.solstice.data.dao.hibernate;

import com.jeroensteenbeeke.hyperion.solstice.data.HibernateDAO;
import com.jeroensteenbeeke.hyperion.solstice.data.entity.Foo;
import com.jeroensteenbeeke.hyperion.solstice.data.entity.filter.FooFilter;
import org.springframework.stereotype.Component;

@Component
public class FooDAO extends HibernateDAO<Foo, FooFilter> implements com.jeroensteenbeeke.hyperion.solstice.data.dao.FooDAO {
}
