package com.jeroensteenbeeke.hyperion.solstice.data.entity;

import com.jeroensteenbeeke.hyperion.data.BaseDomainObject;

import org.jetbrains.annotations.NotNull;
import jakarta.persistence.*;
import java.io.Serializable;

@Entity
public class Foo extends BaseDomainObject {
	private static final long serialVersionUID = -3595592595470043724L;

	@SequenceGenerator(
			allocationSize = 1,
			initialValue = 1,
			name = "Foo",
			sequenceName = "SEQ_ID_Foo")
	@Access(value = AccessType.PROPERTY)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Foo")
	@Id
	private Long id;

	@Column(nullable = false)
	private String dtype;

	@NotNull
	public Long getId() {
		return id;
	}

	public void setId(@NotNull Long id) {
		this.id = id;
	}

	@Override
	public Serializable getDomainObjectId() {
		return getId();
	}

	@NotNull
	public String getDtype() {
		return dtype;
	}

	public void setDtype(@NotNull String dtype) {
		this.dtype = dtype;
	}

	@Override
	public String toString() {
		return "Foo{" +
				"id=" + id +
				", dtype='" + dtype + '\'' +
				'}';
	}
}
