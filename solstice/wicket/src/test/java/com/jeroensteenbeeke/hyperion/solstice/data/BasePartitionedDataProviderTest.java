package com.jeroensteenbeeke.hyperion.solstice.data;

import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BasePartitionedDataProviderTest {

	@Test
	void iterator() {
		List<Integer> baseList = Stream.iterate(1, i -> i + 1).limit(100).toList();

		IDataProvider<Integer> oneToHundred = new IDataProvider<>() {
			@Override
			public Iterator<? extends Integer> iterator(long first, long count) {
				return baseList.subList((int) first, (int) (first + count)).iterator();
			}

			@Override
			public long size() {
				return baseList.size();
			}

			@Override
			public IModel<Integer> model(Integer integer) {
				return Model.of(integer);
			}
		};

		var provider = new BasePartitionedDataProvider<>(oneToHundred, 20);

		List<Integer> oneToTwenty = Stream.iterate(1, i -> i + 1).limit(20).toList();
		List<Integer> twentyOneToForty = Stream.iterate(21, i -> i + 1).limit(20).toList();

		Iterator<List<Integer>> iterator = provider.iterator(0, 1);
		assertTrue(iterator.hasNext());
		assertThat(iterator.next(), equalTo(oneToTwenty));
		assertFalse(iterator.hasNext());

		iterator = provider.iterator(0, 2);
		assertTrue(iterator.hasNext());
		assertThat(iterator.next(), equalTo(oneToTwenty));
		assertTrue(iterator.hasNext());
		assertThat(iterator.next(), equalTo(twentyOneToForty));
		assertFalse(iterator.hasNext());

		iterator = provider.iterator(1, 1);
		assertTrue(iterator.hasNext());
		assertThat(iterator.next(), equalTo(twentyOneToForty));
		assertFalse(iterator.hasNext());
	}
}
