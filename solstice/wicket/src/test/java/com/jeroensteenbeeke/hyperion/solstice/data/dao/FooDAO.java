package com.jeroensteenbeeke.hyperion.solstice.data.dao;

import com.jeroensteenbeeke.hyperion.meld.DAO;
import com.jeroensteenbeeke.hyperion.solstice.data.entity.Foo;
import com.jeroensteenbeeke.hyperion.solstice.data.entity.filter.FooFilter;

public interface FooDAO extends DAO<Foo, FooFilter> {
}
