/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.solstice.data;

import com.jeroensteenbeeke.hyperion.data.BaseEntityFinder;
import com.jeroensteenbeeke.hyperion.data.DomainObject;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.model.IModel;
import org.hibernate.Hibernate;

import org.jetbrains.annotations.NotNull;
import jakarta.inject.Inject;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Model representing a list of entities, that is safe to use across multiple sessions
 *
 * @author Jeroen Steenbeeke
 */
class EntityListModel<T extends DomainObject> implements IModel<List<T>> {
	private static final long serialVersionUID = 1L;

	private Class<T> entityClass = null;

	private List<Serializable> ids;

	@Inject
	private BaseEntityFinder entityFinder;

	private transient List<T> list;

	/**
	 * Constructor
	 * @param list The list to wrap
	 */
	@SuppressWarnings("unchecked")
	EntityListModel(@NotNull List<T> list) {
		this.list = list;
		this.ids = new LinkedList<>();
		this.entityClass = !list.isEmpty() && list.get(0) != null ? (Class<T>) Hibernate.getClass(list.get(0)) : null;
		for (T t : list) {
			Class<T> tClass = (Class<T>) Hibernate.getClass(t);
			if (!entityClass.isAssignableFrom(tClass)) {
				if (entityClass.getSuperclass() != null
						&& entityClass.getSuperclass().isAssignableFrom(tClass)) {
					entityClass = (Class<T>) entityClass.getSuperclass();
				}
			}

			ids.add(t.getDomainObjectId());

		}

		detach(); // Fix voor achterlijke hibernate bug
	}

	@Override
	public void detach() {
		this.entityFinder = null;
		this.list = null;
	}

	@Override
	public List<T> getObject() {
		if (list == null) {
			list = load();
		}

		return list;
	}

	@Override
	public void setObject(List<T> object) {
		throw new UnsupportedOperationException();
	}

	List<T> load() {
		if (entityFinder == null) {
			Injector.get().inject(this);
		}

		if (entityClass != null) {
			this.list = new LinkedList<>();

			for (Serializable id : ids) {
				entityFinder.getEntity(entityClass, id).peek(this.list::add);
			}

			return this.list;
		}

		return new LinkedList<T>();
	}
}
