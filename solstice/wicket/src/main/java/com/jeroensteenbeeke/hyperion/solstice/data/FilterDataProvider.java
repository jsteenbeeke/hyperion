/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.solstice.data;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import com.jeroensteenbeeke.hyperion.meld.DAO;
import com.jeroensteenbeeke.hyperion.meld.SearchFilter;
import io.vavr.collection.Seq;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;

import org.jetbrains.annotations.NotNull;
import java.util.Iterator;

/**
 * Wicket DataProvider implementation that takes a SearchFilter and a DAO
 * and provides all relevant entities
 * @param <S> The type of SearchFilter
 * @param <T> The type of entity
 */
public class FilterDataProvider<S extends SearchFilter<T,S>, T extends DomainObject>
		implements IDataProvider<T> {
	private static final long serialVersionUID = 1L;

	private DAO<T,S> dao;

	private S filter;

	private FilterDataProvider(S filter, DAO<T,S> dao) {
		this.filter = filter;
		this.dao = dao;
	}

	@Override
	public Iterator<? extends T> iterator(long first, long count) {
		Seq<T> values = dao.findByFilter(filter, first, count);

		return values.iterator();
	}

	@Override
	public long size() {
		return dao.countByFilter(filter);
	}

	@Override
	public void detach() {
		filter.detach();

	}

	@Override
	public IModel<T> model(T object) {
		return ModelMaker.wrap(object);
	}

	/**
	 * Creates a new FilterDataProvider
	 * @param filter The given SearchFilter
	 * @param dao The given DAO
	 * @param <S> The type of filter
	 * @param <T> The type of entity
	 * @return A FilterDataProvider
	 */
	public static <S extends SearchFilter<T,S>, T extends DomainObject> FilterDataProvider<S, T> of(
			S filter, DAO<T,S> dao) {
		return new FilterDataProvider<>(filter, dao);
	}

	/**
	 * Creates a new FilterDataProvider in fluent style
	 * @param filter The filter to use
	 * @param <S> The type of filter
	 * @param <T> The type of entity
	 * @return A builder step
	 */
	public static <S extends SearchFilter<T,S>, T extends DomainObject> WithDAO<S,T> search(S filter) {
		return dao -> new FilterDataProvider<>(filter, dao);
	}

	/**
	 * Builder finalizer
	 * @param <S> The searchfilter type
	 * @param <T> The entity type
	 */
	@FunctionalInterface
	public interface WithDAO<S extends SearchFilter<T,S>, T extends DomainObject> {
		/**
		 * Creates a new dataprovider using the given DAO
		 * @param dao The DAO
		 * @return A dataprovider
		 */
		FilterDataProvider<S,T> withDAO(@NotNull DAO<T,S> dao);
	}
}
