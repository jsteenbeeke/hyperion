package com.jeroensteenbeeke.hyperion.solstice.data;

import com.jeroensteenbeeke.hyperion.data.DomainObject;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;

import java.util.List;

/**
 * Specialization of the BasePartitionedDataProvider class that contains logic to detach JPA entities
 * @param <T> The entity type
 */
public class PartitionedDataProvider<T extends DomainObject> extends BasePartitionedDataProvider<T> {
	/**
	 * Constructor
	 *
	 * @param delegate The dataprovider whose result to partition
	 * @param listSize The (max) number of results in each partition
	 */
	public PartitionedDataProvider(IDataProvider<T> delegate, int listSize) {
		super(delegate, listSize);
	}

	@Override
	public IModel<List<T>> model(List<T> object) {
		return ModelMaker.wrapList(object);
	}
}
