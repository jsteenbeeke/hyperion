package com.jeroensteenbeeke.hyperion.solstice.backend;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Spring configuration class that defines the exception mapper for HttpStatusException
 */
@Configuration
public class HttpStatusExceptionConfig {
	/**
	 * Creates a new HttpStatusExceptionMapper
	 * @return The mapper
	 */
	@Bean
	public HttpStatusExceptionMapper mapper() {
		return new HttpStatusExceptionMapper();
	}
}
