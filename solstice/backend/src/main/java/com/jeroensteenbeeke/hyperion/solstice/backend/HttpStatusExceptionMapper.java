package com.jeroensteenbeeke.hyperion.solstice.backend;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

/**
 * ExceptionMapper for HttpStatusException
 */
@Provider
public class HttpStatusExceptionMapper implements ExceptionMapper<HttpStatusException> {

	@Override
	public Response toResponse(HttpStatusException exception) {
		return exception.toResponse();
	}
}
