package com.jeroensteenbeeke.hyperion.solstice.backend;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * Spring activation annotation that enables mapping of HttpStatusException to JAX-RS responses
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(HttpStatusExceptionConfig.class)
public @interface EnableHttpExceptionResponses {
}
