package com.jeroensteenbeeke.hyperion.tardis.scheduler;

import org.jetbrains.annotations.NotNull;

/**
 * Representation of a running task
 */
public class HyperionTaskView {
	private final String name;
	
	private final TaskGroup group;
	
	private final HyperionTaskProgress progress;

	/**
	 * Constructor
	 * @param task The task for which to create this view
	 */
	public HyperionTaskView(@NotNull HyperionTask task) {
		this.name = task.getName();
		this.group = task.getGroup();
		this.progress = task.getProgress();
	}

	/**
	 * Returns the name of the task
	 * @return The name
	 */
	@NotNull
	public String getName() {
		return name;
	}

	/**
	 * Returns the group of the task
	 * @return The group
	 */
	@NotNull
	public TaskGroup getGroup() {
		return group;
	}

	/**
	 * Returnss the progress of the task
	 * @return The progress
	 */
	@NotNull
	public HyperionTaskProgress getProgress() {
		return progress;
	}
	
	
}
