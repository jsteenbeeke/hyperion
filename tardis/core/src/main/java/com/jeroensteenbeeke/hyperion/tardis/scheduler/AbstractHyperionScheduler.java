/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.tardis.scheduler;

import com.jeroensteenbeeke.hyperion.tardis.scheduler.intervals.Interval;
import com.jeroensteenbeeke.lux.ActionResult;
import org.danekja.java.util.function.serializable.SerializableConsumer;
import org.jetbrains.annotations.NotNull;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * Abstract scheduler that defines the basic logic used in Tardis scheduler implementations
 */
public abstract class AbstractHyperionScheduler {
	protected static Logger logger = LoggerFactory
			.getLogger(AbstractHyperionScheduler.class);

	private final Scheduler scheduler;

	private final List<SerializableConsumer<Exception>> exceptionListeners = new ArrayList<>();

	private boolean logTaskLifecycle = false;

	/**
	 * Constructor
	 *
	 * @throws SchedulerException If initialization fails
	 */
	protected AbstractHyperionScheduler() throws SchedulerException {
		SchedulerFactory schedFact = new StdSchedulerFactory();
		scheduler = schedFact.getScheduler();
		scheduler.start();
	}

	/**
	 * Run the given task as soon as possible
	 *
	 * @param task The task to run
	 * @return An ActionResult either indicating success or the reason for failure
	 */
	public ActionResult runTask(@NotNull HyperionTask task) {

		JobDetail detail = createJobDetail(task);
		try {
			scheduler.scheduleJob(detail,
					TriggerBuilder.newTrigger().startNow().build());
			return ActionResult.ok();
		} catch (SchedulerException e) {
			logger.error(e.getMessage(), e);
			return ActionResult.error(e.getMessage());
		}
	}

	/**
	 * Schedule a task for execution at a later date
	 *
	 * @param dateTime The time to schedule the task
	 * @param task     The task to schedule
	 * @return An ActionResult either indicating success or the reason for failure
	 */
	public ActionResult scheduleTask(
			@NotNull LocalDateTime dateTime,
			@NotNull HyperionTask task) {

		JobDetail detail = createJobDetail(task);
		try {
			Instant instant = dateTime.atZone(ZoneId.systemDefault()).toInstant();
			scheduler.scheduleJob(detail, TriggerBuilder.newTrigger()
					.startAt(Date.from(instant)).build());
			return ActionResult.ok();
		} catch (SchedulerException e) {
			logger.error(e.getMessage(), e);
			return ActionResult.error(e.getMessage());
		}
	}

	/**
	 * Schedules a task to be repeated on a given interval. The interval also determines
	 * the next execution time.
	 *
	 * @param interval The interval for repeating the task
	 * @param task     The task to schedule
	 * @return An ActionResult either indicating success or the reason for failure
	 */
	public ActionResult scheduleRepeatingTask(
			@NotNull Interval interval,
			@NotNull HyperionTask task) {

		JobDetail detail = createJobDetail(task);
		try {
			scheduler.scheduleJob(detail,
					TriggerBuilder.newTrigger()
							.withSchedule(CronScheduleBuilder
									.cronSchedule(interval.getCronExpression()))
							.build());
			return ActionResult.ok();
		} catch (SchedulerException e) {
			logger.error(e.getMessage(), e);
			return ActionResult.error(e.getMessage());
		}
	}

	/**
	 * Internal method: creates a Quartz job detail for the given task
	 *
	 * @param task The task to create the detail for
	 * @return The JobDetail
	 */
	protected abstract JobDetail createJobDetail(HyperionTask task);

	/**
	 * Internal method: transforms a Quartz executionContext to a view
	 *
	 * @param context The context to transform
	 * @return An instance of HyperionTaskView
	 */
	protected abstract HyperionTaskView contextToView(
			JobExecutionContext context);


	/**
	 * Indicates whether or not the scheduler is shut down
	 *
	 * @return {@code true} if the scheduler is shutdown, {@code false} otherwise
	 */
	public boolean isShutDown() {
		try {
			return scheduler.isShutdown();
		} catch (SchedulerException e) {
			return true;
		}
	}

	/**
	 * Shuts down the scheduler
	 *
	 * @return An ActionResult either indicating success or the reason for failure
	 */
	public ActionResult shutdown() {
		try {
			scheduler.shutdown();
			return ActionResult.ok();
		} catch (SchedulerException e) {
			logger.error(e.getMessage(), e);
			return ActionResult.error(e.getMessage());
		}
	}

	/**
	 * Get a list of currently running tasks
	 *
	 * @return A list of details for currently running tasks
	 */
	public List<HyperionTaskView> getRunningTasks() {
		return getRunningTasks(t -> true);
	}

	/**
	 * Gets all currently running tasks matching the predicate
	 *
	 * @param predicate The predicate each task should match
	 * @return A list of details for all tasks matching the predicate
	 */
	public List<HyperionTaskView> getRunningTasks(
			@NotNull Predicate<HyperionTaskView> predicate) {
		List<HyperionTaskView> tasks = new ArrayList<>();
		try {
			scheduler.getCurrentlyExecutingJobs().stream()
					.map(this::contextToView).filter(Objects::nonNull)
					.filter(predicate).forEach(tasks::add);
		} catch (SchedulerException e) {
			logger.error(e.getMessage(), e);
		}

		return List.copyOf(tasks);

	}

	/**
	 * Indicates whether or not the task's lifecycle should be logged
	 *
	 * @return {@code true} if tasks should have their lifecycles logged. {@code false} otherwise
	 */
	public boolean isLogTaskLifecycle() {
		return logTaskLifecycle;
	}

	/**
	 * Sets whether or not a task's lifecycle should be logged. Defaults to {@code false}
	 *
	 * @param logTaskLifecycle {@code true} if the lifecycle should be logged, {@code false} otherwise
	 */
	public void setLogTaskLifecycle(boolean logTaskLifecycle) {
		this.logTaskLifecycle = logTaskLifecycle;
	}

	/**
	 * Registers an exception listener for executing tasks. Useful for reporting background
	 * task exceptions to services such as Rollbar
	 *
	 * @param listener The listener to register
	 */
	public void registerExceptionListener(SerializableConsumer<Exception> listener) {
		this.exceptionListeners.add(listener);
	}

	/**
	 * Callback method for when an exception occurs. Passes thrown exceptions to all registered
	 * exception listeners
	 *
	 * @param e The exception to pass to listeners
	 */
	public void onException(Exception e) {
		this.exceptionListeners.forEach(l -> l.accept(e));
	}
}
