package com.jeroensteenbeeke.hyperion.tardis.scheduler.progress;

import com.jeroensteenbeeke.hyperion.tardis.scheduler.HyperionTaskProgress;

import org.jetbrains.annotations.NotNull;

/**
 * Default implementation that does not know how far along a task is
 */
public class UnknownTaskProgress implements HyperionTaskProgress {

	@Override
	public int getPercentageComplete() {
		return 0;
	}

	@NotNull
	@Override
	public String getProgressDescription() {
		// TODO: I18N support
		return "Unknown";
	}
}
