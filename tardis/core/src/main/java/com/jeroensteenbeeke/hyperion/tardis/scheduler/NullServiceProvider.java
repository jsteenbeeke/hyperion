/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jeroensteenbeeke.hyperion.tardis.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Dummy implementation of ServiceProvider that returns null for all service requests
 */
public class NullServiceProvider implements ServiceProvider {
	private static final Logger log = LoggerFactory
			.getLogger(NullServiceProvider.class);

	@Override
	public <T> T getService(Class<T> serviceClass) {
		log.error("No applicationContext known to Scheduler!");
		return null;
	}

}
