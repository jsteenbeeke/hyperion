package com.jeroensteenbeeke.hyperion.tardis.scheduler.progress;

import com.jeroensteenbeeke.hyperion.tardis.scheduler.HyperionTaskProgress;

import org.jetbrains.annotations.NotNull;

/**
 * Progress indicator that directly accepts percentages as input
 */
public class PercentageTaskProgress implements HyperionTaskProgress {

	private final int percentage;

	/**
	 * Constructor
	 * @param percentage The progress in percentages
	 */
	public PercentageTaskProgress(int percentage) {
		this.percentage = percentage;
	}
	
	@NotNull
	@Override
	public String getProgressDescription() {
		return String.format("%d%%", percentage);
	}
	
	@Override
	public int getPercentageComplete() {
		return percentage;
	}

}
