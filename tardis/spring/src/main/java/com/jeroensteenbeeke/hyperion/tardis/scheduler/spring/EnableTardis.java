package com.jeroensteenbeeke.hyperion.tardis.scheduler.spring;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * Spring configuration annotation that enables the configuration for the Tardis scheduler
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(TardisConfig.class)
public @interface EnableTardis {
}
