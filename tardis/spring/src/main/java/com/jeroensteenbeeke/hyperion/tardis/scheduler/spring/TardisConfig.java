package com.jeroensteenbeeke.hyperion.tardis.scheduler.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * Spring configuration for auto-initialization/shutdown of the scheduler on application startup/close
 */
@Configuration
public class TardisConfig {
	private static final Logger log = LoggerFactory.getLogger(TardisConfig.class);

	/**
	 * Creates the initialization listener
	 * @return The listener
	 */
	@Bean
	@Scope("singleton")
	public SpringBasedInitializer initializerBean() {
		log.info("Created Tardis initializer");
		return new SpringBasedInitializer();
	}

	/**
	 * Creates the cleanup crew
	 * @return The cleanup bean
	 */
	@Bean
	@Scope("singleton")
	public SpringBasedCleanup cleanupBean() {
		log.info("Created Tardis cleanup");
		return new SpringBasedCleanup();
	}

}
