package com.jeroensteenbeeke.hyperion.tardis.scheduler.spring;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(TardisConfig.class)
@ComponentScan("com.jeroensteenbeeke.hyperion.tardis.scheduler")
public class TestConfig {

}
