package com.jeroensteenbeeke.hyperion.tardis.scheduler.spring;

import com.jeroensteenbeeke.hyperion.tardis.scheduler.AbstractHyperionScheduler;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.OnSchedulerInitializedCallback;
import org.springframework.stereotype.Component;

import org.jetbrains.annotations.NotNull;

@Component
class TestInitializer implements OnSchedulerInitializedCallback {
	

	@Override
	public void onSchedulerInitialized(@NotNull AbstractHyperionScheduler scheduler) {
		SpringConfigTest.hasBeenCalled.set(true);
	}

}
