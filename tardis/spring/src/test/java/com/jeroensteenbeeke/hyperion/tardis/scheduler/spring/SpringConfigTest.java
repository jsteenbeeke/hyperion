package com.jeroensteenbeeke.hyperion.tardis.scheduler.spring;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class SpringConfigTest {
	public static final AtomicBoolean hasBeenCalled = new AtomicBoolean(false);
	
	@Test
	public void testSpringConfig() {
		DefaultListableBeanFactory factory = new DefaultListableBeanFactory();
		
		try (AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(
				factory)) {
			ctx.register(TestConfig.class);
			ctx.refresh();
			
			assertTrue(hasBeenCalled.get());
		}
	}
}
