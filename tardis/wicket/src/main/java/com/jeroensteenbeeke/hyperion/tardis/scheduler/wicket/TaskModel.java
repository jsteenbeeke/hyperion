package com.jeroensteenbeeke.hyperion.tardis.scheduler.wicket;

import com.jeroensteenbeeke.hyperion.tardis.scheduler.HyperionTaskView;
import org.apache.wicket.model.LoadableDetachableModel;
import org.danekja.java.util.function.serializable.SerializablePredicate;

import org.jetbrains.annotations.NotNull;
import java.util.List;

/**
 * Wicket Model that contains an up-to-date list of all running tasks
 * 
 * @author Jeroen Steenbeeke
 */
public class TaskModel extends LoadableDetachableModel<List<HyperionTaskView>> {
	private static final long serialVersionUID = 1L;

	private final SerializablePredicate<HyperionTaskView> predicate;

	/**
	 * Create a model that lists all tasks
	 */
	public TaskModel() {
		this(t -> true);
	}

	/**
	 * Create a model that lists all tasks matching the given predicate
	 * 
	 * @param predicate
	 *            The predicate to filter tasks by
	 */
	public TaskModel(
			@NotNull SerializablePredicate<HyperionTaskView> predicate) {
		this.predicate = predicate;
	}

	@Override
	protected List<HyperionTaskView> load() {

		return HyperionScheduler.getScheduler().getRunningTasks(predicate);
	}
}
