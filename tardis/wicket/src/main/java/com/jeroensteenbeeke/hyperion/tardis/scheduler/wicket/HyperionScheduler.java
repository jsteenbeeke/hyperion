package com.jeroensteenbeeke.hyperion.tardis.scheduler.wicket;

import com.jeroensteenbeeke.hyperion.solstice.spring.ApplicationContextProvider;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.AbstractHyperionScheduler;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.HyperionTask;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.HyperionTaskView;
import com.jeroensteenbeeke.hyperion.tardis.scheduler.OnSchedulerInitializedCallback;
import org.apache.wicket.Application;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.SchedulerException;

/**
 * Wicket-backed implementation of the scheduler.
 */
public class HyperionScheduler extends AbstractHyperionScheduler {

	private static HyperionScheduler instance;

	private Application application;

	private HyperionScheduler() throws SchedulerException {
		super();
	}

	/**
	 * Gets an instance of the scheduler, creating a new one if it doesn't exist (singleton pattern)
	 * @return The scheduler
	 */
	public static HyperionScheduler getScheduler() {
		if (instance == null) {
			try {
				instance = new HyperionScheduler();
			} catch (SchedulerException e) {
				logger.error(e.getMessage(), e);
			}
		}

		return instance;
	}

	/**
	 * Resets the scheduler
	 *
	 * @return The scheduler
	 */
	public static HyperionScheduler reset() {
		try {
			instance = new HyperionScheduler();
		} catch (SchedulerException e) {
			logger.error(e.getMessage(), e);
		}

		return instance;
	}

	/**
	 * Sets the application running the scheduler
	 * @param application
	 *            the application to set
	 * @param <A> The type of application/ApplicationContextProvider
	 */
	public <A extends Application & ApplicationContextProvider> void setApplication(A application) {
		this.application = application;
		application.getApplicationContext().getBeansOfType(OnSchedulerInitializedCallback.class).values().forEach(cb -> cb.onSchedulerInitialized(this));
	}

	/**
	 * Get the application running the scheduler
	 * @return the application
	 */
	public Application getApplication() {
		return application;
	}

	@Override
	protected JobDetail createJobDetail(HyperionTask task) {
		JobDetail detail = JobBuilder
				.newJob(WicketBackedHyperionTaskExecutor.class)
				.withIdentity(task.getName().concat(".").concat(Long.toString(System.currentTimeMillis())))
				.withDescription(task.getGroup().getDescriptor()).build();

		detail.getJobDataMap().put(WicketBackedHyperionTaskExecutor.TASK_KEY,
				task);
		detail.getJobDataMap().put(WicketBackedHyperionTaskExecutor.APP_KEY,
				getApplication());
		return detail;
	}

	@Override
	protected HyperionTaskView contextToView(JobExecutionContext context) {
		return new HyperionTaskView(
				(HyperionTask) context.getJobDetail().getJobDataMap()
						.get(WicketBackedHyperionTaskExecutor.TASK_KEY));
	}

}
