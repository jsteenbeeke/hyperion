package com.jeroensteenbeeke.hyperion.rest.querysupport;

/**
 * Comparison type between comparables, defines possible comparisons to be done against comparable properties
 */
public enum ComparableComparisonType {
	/**
	 * No comparison
	 */
	NONE {
		@Override
		public <C extends Comparable<? super C>> boolean test(C primary, C secondary, C actual) {
			return true;
		}
	},
	/**
	 * Test if both comparables are equal
	 */
	EQUALS {
		@Override
		public <C extends Comparable<? super C>> boolean test(C primary, C secondary, C actual) {
			return actual != null && primary.compareTo(actual) == 0;
		}
	},
	/**
	 * Tests if the first comparable is less than the other
	 */
	LESS_THAN {
		@Override
		public <C extends Comparable<? super C>> boolean test(C primary, C secondary, C actual) {
			return actual != null && actual.compareTo(primary) < 0;
		}
	},
	/**
	 * Tests if the first comparable is less than or equal to the other
	 */
	LESS_THAN_OR_EQUAL_TO {
		@Override
		public <C extends Comparable<? super C>> boolean test(C primary, C secondary, C actual) {
			return actual != null && actual.compareTo(primary) <= 0;
		}
	},
	/**
	 * Tests if the first comparable is greater than the other
	 */
	GREATER_THAN {
		@Override
		public <C extends Comparable<? super C>> boolean test(C primary, C secondary, C actual) {
			return actual != null && actual.compareTo(primary) > 0;
		}
	},
	/**
	 * Tests if the first comparable is greater than or equal to the other
	 */
	GREATER_THAN_OR_EQUAL_TO {
		@Override
		public <C extends Comparable<? super C>> boolean test(C primary, C secondary, C actual) {
			return actual != null && actual.compareTo(primary) >= 0;
		}
	},
	/**
	 * Tests if a given comparable is explicitly null
	 */
	NULL {
		@Override
		public <C extends Comparable<? super C>> boolean test(C primary, C secondary, C actual) {
			return actual == null;
		}
	},
	/**
	 * Tests if a comparable falls within a certain range
	 */
	BETWEEN {
		@Override
		public <C extends Comparable<? super C>> boolean test(C primary, C secondary, C actual) {
			return actual != null && actual.compareTo(primary) >= 0 && actual.compareTo(secondary) <= 0;
		}
	};

	/**
	 * Compares a given comparable against this comparison type's rules
	 * @param primary The primary value
	 * @param secondary The secondary value (generally null except for BETWEEN)
	 * @param actual The actual value
	 * @param <C> The type of comparable
	 * @return {@code true} if the test passes, or {@code false} otherwise
	 */
	public abstract <C extends Comparable<? super C>> boolean test(C primary, C secondary, C actual);
}
