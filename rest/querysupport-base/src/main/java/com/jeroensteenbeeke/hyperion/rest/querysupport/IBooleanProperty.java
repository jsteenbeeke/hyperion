package com.jeroensteenbeeke.hyperion.rest.querysupport;

import java.util.Objects;

/**
 * Query property representing boolean operations
 * @param <Q> The type of object containing the current property
 */
public interface IBooleanProperty<Q extends QueryObject<?>> extends IOpaqueProperty<Q,Boolean> {
	@Override
	default boolean appliesTo(Object object) {
		if (!isSet()) {
			return true;
		}

		Boolean f = getField(object, getFieldName(), Boolean.class);
		if (f == null) {
			f = getField(object, getFieldName(), boolean.class);
		}

		if (isExplicitNull()) {
			return f == null;
		} else {
			boolean equals = Objects.equals(f, getUnwrappedValue());

			return isNegated() != equals;
		}
	}

}
