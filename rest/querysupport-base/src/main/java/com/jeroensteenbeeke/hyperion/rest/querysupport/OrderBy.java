package com.jeroensteenbeeke.hyperion.rest.querysupport;

import java.io.Serializable;

/**
 * Describes how a property should be considered in
 */
public class OrderBy implements Comparable<OrderBy>, Serializable {
	public static final OrderBy IGNORE = new OrderBy(Integer.MIN_VALUE, false);

	private final int index;

	private final boolean ascending;

	/**
	 * Creates a new OrderBy instance, with the given index and ascending/descending indicator
	 * @param index The index of the order by. Lower indices will be handled first
	 * @param ascending Whether the ordering should be ascending or descending
	 */
	public OrderBy(int index, boolean ascending) {
		this.index = index;
		this.ascending = ascending;
	}

	/**
	 * Get the index of the current OrderBy. A lower index will be handled first
	 * @return The index of the current object
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * Determines whether or not the current OrderBy is ascending
	 * @return {@code true} if the property indicated by this OrderBy should be in ascending order, {@code false}
	 * if it should be in descending order
	 */
	public boolean isAscending() {
		return ascending;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		OrderBy orderBy = (OrderBy) o;

		if (index != orderBy.index) return false;
		return ascending == orderBy.ascending;
	}

	@Override
	public int hashCode() {
		int result = index;
		result = 31 * result + (ascending ? 1 : 0);
		return result;
	}

	@Override
	public int compareTo(OrderBy o) {
		return Integer.compare(index, o.getIndex());
	}
}
