package com.jeroensteenbeeke.hyperion.rest.querysupport;

import java.util.Objects;
import java.util.UUID;

/**
 * Property representing a UUID
 * @param <Q> The query object
 */
public interface IUUIDProperty<Q extends QueryObject<?>> extends IOpaqueProperty<Q, UUID> {
	@Override
	default boolean appliesTo(Object object) {
		if (!isSet()) {
			return true;
		}

		UUID uuid = getField(object, getFieldName(), UUID.class);

		if (isExplicitNull()) {
			return uuid == null;
		} else {
			boolean equals = Objects.equals(uuid, getUnwrappedValue());

			return isNegated() != equals;
		}
	}
}
