package com.jeroensteenbeeke.hyperion.rest.querysupport;

/**
 * Marker interface for Comparable properties of type Integer
 * @param <Q> The type of queryobject this property is part of
 */
public interface IIntegerProperty<Q extends QueryObject<?>> extends IComparableProperty<Integer,Q> {
}
