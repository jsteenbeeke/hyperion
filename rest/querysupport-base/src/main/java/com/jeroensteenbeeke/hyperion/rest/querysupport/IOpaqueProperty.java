package com.jeroensteenbeeke.hyperion.rest.querysupport;

/**
 * Represents a value that is opaque, i.e. cannot be compared
 * @param <Q>
 * @param <V>
 */
public interface IOpaqueProperty<Q extends QueryObject<?>,V> extends IQueryProperty<Q> {
	/**
	 * Returns the value that was set, if one exists
	 *
	 * @return The value set, or {@code null} if none was set
	 */
	V getUnwrappedValue();

	/**
	 * Return whether or not the property should be checked for an explicit null value
	 *
	 * @return {@code true} if the property should be null (or not-null if {@link #isNegated()} returns true), or
	 * {@code false} if a normal equality check should be performed
	 */
	boolean isExplicitNull();
}
