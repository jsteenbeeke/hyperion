package com.jeroensteenbeeke.hyperion.rest.annotation;

import java.lang.annotation.*;

/**
 * Indicates that a given REST method returns a list of instances of the given class wrapped in the Response type. Used for code generation
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface ReturnsListOf {
	/**
	 * The type contained in the Response object returned by the method, wrapped in a List
	 *
	 * @return A type
	 */
	Class<?> value();
}
