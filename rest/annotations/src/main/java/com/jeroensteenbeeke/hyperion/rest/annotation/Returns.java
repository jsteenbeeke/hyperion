package com.jeroensteenbeeke.hyperion.rest.annotation;

import java.lang.annotation.*;

/**
 * Indicates that a given REST method returns an instance of the given class wrapped in the Response type. Used for code generation
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface Returns {
	/**
	 * The type contained in the Response object returned by the method
	 *
	 * @return A type
	 */
	Class<?> value();
}
