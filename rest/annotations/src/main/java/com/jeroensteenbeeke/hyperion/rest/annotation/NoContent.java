package com.jeroensteenbeeke.hyperion.rest.annotation;

import java.lang.annotation.*;

/**
 * Indicates that a given REST method returns a 204 NO CONTENT status
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface NoContent {
}
