package com.jeroensteenbeeke.hyperion.rest.query;


import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import org.jetbrains.annotations.NotNull;

/**
 * Query object property representing a Double value, that can be queried as any comparable
 *
 * @param <T> The type of query object that contains this property
 */
public class DoubleProperty<T extends QueryObject<?>> extends ComparableProperty<T, Double> {
	private static final long serialVersionUID = -8944978885443431184L;

	/**
	 * Create a new Double property for the given query object
	 *
	 * @param target    The query object containing this property
	 * @param fieldName The name of the field this property is assigned to
	 */
	@SuppressWarnings("NullableProblems")
	public DoubleProperty(@NotNull T target, @NotNull String fieldName) {
		super(target, fieldName);
	}

	@Override
	protected String getStringRepresentation(@NotNull Double aDouble) {
		return aDouble.toString();
	}

	/**
	 * Parses the given String and attempts to turn it into a DoubleProperty
	 *
	 * @param input The input to parse
	 * @param <T>   The type of containing query object
	 * @return An instance of DoubleProperty without a target, so it cannot be used for further method chaining
	 */
	@SuppressWarnings("DataFlowIssue")
	public static <T extends QueryObject<?>> DoubleProperty<T> fromString(@NotNull String input) {
		return fromString(new DoubleProperty<>(null, null), input, Double::parseDouble);
	}
}
