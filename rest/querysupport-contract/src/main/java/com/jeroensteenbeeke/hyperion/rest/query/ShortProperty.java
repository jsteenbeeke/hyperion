package com.jeroensteenbeeke.hyperion.rest.query;


import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import org.jetbrains.annotations.NotNull;

/**
 * Query object property representing a Short value, that can be queried as any comparable
 *
 * @param <T> The type of query object that contains this property
 */
public class ShortProperty<T extends QueryObject<?>> extends ComparableProperty<T, Short> {
	private static final long serialVersionUID = 2564102983996807471L;

	/**
	 * Create a new Short property for the given query object
	 *
	 * @param target    The query object containing this property
	 * @param fieldName The name of the field this property is assigned to
	 */
	@SuppressWarnings("NullableProblems")
	public ShortProperty(@NotNull T target, @NotNull String fieldName) {
		super(target, fieldName);
	}

	@Override
	protected String getStringRepresentation(@NotNull Short aShort) {
		return aShort.toString();
	}

	/**
	 * Parses the given String and attempts to turn it into a ShortProperty
	 *
	 * @param input The input to parse
	 * @param <T>   The type of containing query object
	 * @return An instance of ShortProperty without a target, so it cannot be used for further method chaining
	 */
	@SuppressWarnings("DataFlowIssue")
	public static <T extends QueryObject<?>> ShortProperty<T> fromString(@NotNull String input) {
		return fromString(new ShortProperty<>(null, null), input, Short::parseShort);
	}
}
