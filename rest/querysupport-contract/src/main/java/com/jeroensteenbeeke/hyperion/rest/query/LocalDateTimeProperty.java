package com.jeroensteenbeeke.hyperion.rest.query;


import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME;


/**
 * Query object property representing a LocalDateTime value, that can be queried as any comparable
 *
 * @param <T> The type of query object that contains this property
 */
public class LocalDateTimeProperty<T extends QueryObject<?>> extends ComparableProperty<T, LocalDateTime> {
	private static final long serialVersionUID = -696833808543612809L;

	private static final DateTimeFormatter ISO_LOCAL_DATETIME_MILLIS = new DateTimeFormatterBuilder()
			.parseCaseInsensitive()
			.append(ISO_LOCAL_DATE_TIME)
			.appendFraction(ChronoField.MILLI_OF_SECOND, 3, 3, true)
			.toFormatter();

	/**
	 * Create a new LocalDateTime property for the given query object
	 *
	 * @param target    The query object containing this property
	 * @param fieldName The name of the field this property is assigned to
	 */
	@SuppressWarnings("NullableProblems")
	public LocalDateTimeProperty(@NotNull T target, @NotNull String fieldName) {
		super(target, fieldName);
	}

	@Override
	protected String getStringRepresentation(@NotNull LocalDateTime localDateTime) {
		return ISO_LOCAL_DATETIME_MILLIS.format(localDateTime);
	}

	/**
	 * Parses the given String and attempts to turn it into a LocalDateTimeProperty
	 *
	 * @param input The input to parse
	 * @param <T>   The type of containing query object
	 * @return An instance of LocalDateTimeProperty without a target, so it cannot be used for further method chaining
	 */
	@SuppressWarnings("DataFlowIssue")
	public static <T extends QueryObject<?>> LocalDateTimeProperty<T> fromString(@NotNull String input) {
		return fromString(new LocalDateTimeProperty<>(null, null), input,
				s -> LocalDateTime.parse(s, DateTimeFormatter.ISO_DATE_TIME));
	}
}
