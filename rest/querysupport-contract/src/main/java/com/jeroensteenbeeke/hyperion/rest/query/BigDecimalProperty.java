package com.jeroensteenbeeke.hyperion.rest.query;


import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;

/**
 * Query object property representing a BigDecimal value, that can be queried as any comparable
 *
 * @param <T> The type of query object that contains this property
 */
public class BigDecimalProperty<T extends QueryObject<?>> extends ComparableProperty<T, BigDecimal> {
	private static final long serialVersionUID = -1883911016890302506L;

	/**
	 * Create a new BigDecimal property for the given query object
	 *
	 * @param target    The query object containing this property
	 * @param fieldName The name of the field this property is assigned to
	 */
	@SuppressWarnings("NullableProblems")
	public BigDecimalProperty(@NotNull T target, @NotNull String fieldName) {
		super(target, fieldName);
	}

	@Override
	protected String getStringRepresentation(@NotNull BigDecimal bigDecimal) {
		return bigDecimal.toString();
	}

	/**
	 * Parses the given String and attempts to turn it into a BigDecimalProperty
	 *
	 * @param input The input to parse
	 * @param <T>   The type of containing query object
	 * @return An instance of BigDecimalProperty without a target, so it cannot be used for further method chaining
	 */
	@SuppressWarnings("DataFlowIssue")
	public static <T extends QueryObject<?>> BigDecimalProperty<T> fromString(@NotNull String input) {
		return fromString(new BigDecimalProperty<>(null, null), input, BigDecimal::new);
	}
}
