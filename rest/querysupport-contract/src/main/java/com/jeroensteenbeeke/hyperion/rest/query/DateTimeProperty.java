package com.jeroensteenbeeke.hyperion.rest.query;


import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import org.jetbrains.annotations.NotNull;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME;


/**
 * Query object property representing a ZonedDateTime value, that can be queried as any comparable
 *
 * @param <T> The type of query object that contains this property
 */
public class DateTimeProperty<T extends QueryObject<?>> extends ComparableProperty<T, ZonedDateTime> {
	private static final long serialVersionUID = 2063072913696032104L;

	private static final DateTimeFormatter ISO_ZONED_DATETIME_MILLIS = new DateTimeFormatterBuilder()
			.parseCaseInsensitive()
			.append(ISO_LOCAL_DATE_TIME)
			.appendFraction(ChronoField.MILLI_OF_SECOND, 3, 3, true)
			.appendOffset("+HH:MM", "Z")
			.toFormatter();

	/**
	 * Create a new ZonedDateTime property for the given query object
	 *
	 * @param target    The query object containing this property
	 * @param fieldName The name of the field this property is assigned to
	 */
	@SuppressWarnings("NullableProblems")
	public DateTimeProperty(@NotNull T target, @NotNull String fieldName) {
		super(target, fieldName);
	}

	@Override
	protected String getStringRepresentation(@NotNull ZonedDateTime zonedDateTime) {
		return ISO_ZONED_DATETIME_MILLIS.format(zonedDateTime);
	}

	/**
	 * Parses the given String and attempts to turn it into a DateTimeProperty
	 *
	 * @param input The input to parse
	 * @param <T>   The type of containing query object
	 * @return An instance of DateTimeProperty without a target, so it cannot be used for further method chaining
	 */
	@SuppressWarnings("DataFlowIssue")
	public static <T extends QueryObject<?>> DateTimeProperty<T> fromString(@NotNull String input) {
		return fromString(new DateTimeProperty<>(null, null), input,
				s -> ZonedDateTime.parse(s, DateTimeFormatter.ISO_OFFSET_DATE_TIME));
	}
}
