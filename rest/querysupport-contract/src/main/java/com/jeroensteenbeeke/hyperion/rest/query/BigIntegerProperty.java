package com.jeroensteenbeeke.hyperion.rest.query;


import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import org.jetbrains.annotations.NotNull;

import java.math.BigInteger;

/**
 * Query object property representing a BigInteger value, that can be queried as any comparable
 *
 * @param <T> The type of query object that contains this property
 */
public class BigIntegerProperty<T extends QueryObject<?>> extends ComparableProperty<T, BigInteger> {
	private static final long serialVersionUID = 3918696194680800475L;

	/**
	 * Create a new BigInteger property for the given query object
	 *
	 * @param target    The query object containing this property
	 * @param fieldName The name of the field this property is assigned to
	 */
	@SuppressWarnings("NullableProblems")
	public BigIntegerProperty(@NotNull T target, @NotNull String fieldName) {
		super(target, fieldName);
	}

	@Override
	protected String getStringRepresentation(@NotNull BigInteger bigInteger) {
		return bigInteger.toString();
	}

	/**
	 * Parses the given String and attempts to turn it into a BigIntegerProperty
	 *
	 * @param input The input to parse
	 * @param <T>   The type of containing query object
	 * @return An instance of BigIntegerProperty without a target, so it cannot be used for further method chaining
	 */
	@SuppressWarnings("DataFlowIssue")
	public static <T extends QueryObject<?>> BigIntegerProperty<T> fromString(@NotNull String input) {
		return fromString(new BigIntegerProperty<>(null, null), input, BigInteger::new);
	}
}
