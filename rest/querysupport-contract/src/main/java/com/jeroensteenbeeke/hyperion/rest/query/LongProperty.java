package com.jeroensteenbeeke.hyperion.rest.query;


import com.jeroensteenbeeke.hyperion.rest.querysupport.ILongProperty;
import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import org.jetbrains.annotations.NotNull;

/**
 * Query object property representing a Long value, that can be queried as any comparable
 *
 * @param <T> The type of query object that contains this property
 */
public class LongProperty<T extends QueryObject<?>> extends ComparableProperty<T, Long> implements ILongProperty<T> {

	private static final long serialVersionUID = 7106242766786097769L;

	/**
	 * Create a new Long property for the given query object
	 *
	 * @param target    The query object containing this property
	 * @param fieldName The name of the field this property is assigned to
	 */
	@SuppressWarnings("NullableProblems")
	public LongProperty(@NotNull T target, @NotNull String fieldName) {
		super(target, fieldName);
	}

	@Override
	protected String getStringRepresentation(@NotNull Long aLong) {
		return aLong.toString();
	}

	/**
	 * Parses the given String and attempts to turn it into a BigIntegerProperty
	 *
	 * @param input The input to parse
	 * @param <T>   The type of containing query object
	 * @return An instance of BigIntegerProperty without a target, so it cannot be used for further method chaining
	 */
	@SuppressWarnings("DataFlowIssue")
	public static <T extends QueryObject<?>> LongProperty<T> fromString(@NotNull String input) {
		return fromString(new LongProperty<>(null, null), input, Long::parseLong);
	}
}
