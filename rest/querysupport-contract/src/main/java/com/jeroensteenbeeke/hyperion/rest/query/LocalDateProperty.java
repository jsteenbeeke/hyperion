package com.jeroensteenbeeke.hyperion.rest.query;


import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


/**
 * Query object property representing a LocalDate value, that can be queried as any comparable
 *
 * @param <T> The type of query object that contains this property
 */
public class LocalDateProperty<T extends QueryObject<?>> extends ComparableProperty<T, LocalDate> {

	private static final long serialVersionUID = 6109547230831404418L;

	/**
	 * Create a new LocalDate property for the given query object
	 *
	 * @param target    The query object containing this property
	 * @param fieldName The name of the field this property is assigned to
	 */
	@SuppressWarnings("NullableProblems")
	public LocalDateProperty(@NotNull T target, @NotNull String fieldName) {
		super(target, fieldName);
	}

	@Override
	protected String getStringRepresentation(@NotNull LocalDate localDate) {
		return DateTimeFormatter.ISO_DATE.format(localDate);
	}

	/**
	 * Parses the given String and attempts to turn it into a LocalDateProperty
	 *
	 * @param input The input to parse
	 * @param <T>   The type of containing query object
	 * @return An instance of LocalDateProperty without a target, so it cannot be used for further method chaining
	 */
	@SuppressWarnings("DataFlowIssue")
	public static <T extends QueryObject<?>> LocalDateProperty<T> fromString(@NotNull String input) {
		return fromString(new LocalDateProperty<>(null, null), input,
				i -> LocalDate.parse(i, DateTimeFormatter.ISO_DATE));
	}
}
