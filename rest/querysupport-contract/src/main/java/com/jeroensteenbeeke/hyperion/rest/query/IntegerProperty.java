package com.jeroensteenbeeke.hyperion.rest.query;


import com.jeroensteenbeeke.hyperion.rest.querysupport.IIntegerProperty;
import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import org.jetbrains.annotations.NotNull;

/**
 * Query object property representing a Integer value, that can be queried as any comparable
 *
 * @param <T> The type of query object that contains this property
 */
public class IntegerProperty<T extends QueryObject<?>> extends ComparableProperty<T, Integer>
		implements IIntegerProperty<T> {
	private static final long serialVersionUID = -4288100548600927960L;

	/**
	 * Create a new Integer property for the given query object
	 *
	 * @param target    The query object containing this property
	 * @param fieldName The name of the field this property is assigned to
	 */
	@SuppressWarnings("NullableProblems")
	public IntegerProperty(@NotNull T target, @NotNull String fieldName) {
		super(target, fieldName);
	}

	@Override
	protected String getStringRepresentation(@NotNull Integer integer) {
		return integer.toString();
	}

	/**
	 * Parses the given String and attempts to turn it into a IntegerProperty
	 *
	 * @param input The input to parse
	 * @param <T>   The type of containing query object
	 * @return An instance of IntegerProperty without a target, so it cannot be used for further
	 * method chaining
	 */
	@SuppressWarnings("DataFlowIssue")
	public static <T extends QueryObject<?>> IntegerProperty<T> fromString(@NotNull String input) {
		return fromString(new IntegerProperty<>(null, null), input, Integer::parseInt);
	}
}
