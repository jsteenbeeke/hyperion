package com.jeroensteenbeeke.hyperion.shiro.auth;

import org.apache.shiro.authc.BearerToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static com.jeroensteenbeeke.vavr.hamcrest.VavrMatchers.isNone;
import static com.jeroensteenbeeke.vavr.hamcrest.VavrMatchers.isSome;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;

class AuthorizationTest {

	@Test
	void readBasicAuth() {
		assertThat(Authorization.readBasicAuth("I am a header"), isNone());
		assertThat(Authorization.readBasicAuth("Bearer iAmAToken"), isNone());
		assertThat(Authorization.readBasicAuth(null), isNone());
		assertThat(Authorization.readBasicAuth("Basic "), isNone());
		assertThat(Authorization.readBasicAuth("Basic garbage!"), isNone());
		assertThat(Authorization.readBasicAuth("Basic dXNlcm5hbWVQYXNzd29yZA"), isNone());
		assertThat(Authorization.readBasicAuth("Basic dXNlcm5hbWVQYXNzd29yZA="), isNone());
		assertThat(Authorization.readBasicAuth("Basic dXNlcm5hbWVQYXNzd29yZA=="), isNone());
		assertThat(Authorization.readBasicAuth("Basic Og"), isNone());
		assertThat(Authorization.readBasicAuth("Basic Og="), isNone());
		assertThat(Authorization.readBasicAuth("Basic Og==="), isNone());
		assertThat(Authorization.readBasicAuth("Basic Og=="),
				isSome(allOf(
						username(""),
						password())));

		assertThat(Authorization.readBasicAuth("Basic dXNlcm5hbWU6cGFzc3dvcmQ="), isSome(
				allOf(
						username("username"),
						password('p', 'a', 's', 's', 'w', 'o', 'r', 'd')
				)
		));
	}

	@Test
	void readBearerToken() {
		assertThat(Authorization.readBearerToken("I am a header"), isNone());
		assertThat(Authorization.readBearerToken("Basic iAmAToken"), isNone());
		assertThat(Authorization.readBearerToken("Bearer "), isSome(token("")));
		assertThat(Authorization.readBearerToken("Bearer iAmAToken"), isSome(token("iAmAToken")));

	}

	private TypeSafeDiagnosingMatcher<UsernamePasswordToken> username(@NotNull String expectedUsername) {
		return new TypeSafeDiagnosingMatcher<>() {
			@Override
			protected boolean matchesSafely(UsernamePasswordToken item, Description mismatchDescription) {
				String actualUsername = item.getUsername();

				if (expectedUsername.equals(actualUsername)) {
					return true;
				}

				mismatchDescription.appendText("A UsernamePasswordToken with username ").appendValue(actualUsername);

				return false;
			}

			@Override
			public void describeTo(Description description) {
				description.appendText("A UsernamePasswordToken with username ").appendValue(expectedUsername);
			}
		};
	}

	private TypeSafeDiagnosingMatcher<UsernamePasswordToken> password(@NotNull char... expectedPassword) {
		return new TypeSafeDiagnosingMatcher<>() {
			@Override
			protected boolean matchesSafely(UsernamePasswordToken item, Description mismatchDescription) {
				char[] actualPassword = item.getPassword();

				if (Arrays.equals(expectedPassword, actualPassword)) {
					return true;
				}

				mismatchDescription.appendText("A UsernamePasswordToken with password ").appendValue(
						new String(actualPassword)
				);

				return false;
			}

			@Override
			public void describeTo(Description description) {
				description.appendText("A UsernamePasswordToken with username ").appendValue(expectedPassword);
			}
		};
	}

	private TypeSafeDiagnosingMatcher<BearerToken> token(@NotNull String expected) {
		return new TypeSafeDiagnosingMatcher<>() {
			@Override
			protected boolean matchesSafely(BearerToken token, Description mismatchDescription) {
				String actualToken = token.getToken();

				if (expected.equals(actualToken)) {
					return true;
				}

				mismatchDescription.appendText("is a bearer token with value ").appendValue(actualToken);

				return false;
			}

			@Override
			public void describeTo(Description description) {
				description.appendText("is a bearer token with value ").appendValue(expected);

			}
		};
	}
}
