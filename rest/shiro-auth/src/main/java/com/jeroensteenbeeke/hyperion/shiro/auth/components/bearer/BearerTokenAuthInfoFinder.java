package com.jeroensteenbeeke.hyperion.shiro.auth.components.bearer;

import io.vavr.control.Option;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.BearerToken;
import org.jetbrains.annotations.NotNull;

/**
 * Functionality for finding AuthenticationInfo based on a BearerToken
 */
public interface BearerTokenAuthInfoFinder {
	/**
	 * Finds the AuthenticationInfo tied to the supplied token, if any
	 * @param bearerToken The token
	 * @return An Option containing the AuthenticationInfo if one exists for the given token, or an empty Option otherwise
	 */
	@NotNull
	Option<AuthenticationInfo> findAuthenticationInfo(@NotNull BearerToken bearerToken);
}
