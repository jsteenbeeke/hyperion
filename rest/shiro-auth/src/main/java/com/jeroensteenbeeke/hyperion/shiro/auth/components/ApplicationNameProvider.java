package com.jeroensteenbeeke.hyperion.shiro.auth.components;

/**
 * Bean interface that should be implemented by the application using Shiro, so
 * that the filters and realm know the application name (used for WWW-Authenticate headers)
 */
public interface ApplicationNameProvider {
	/**
	 * The name of the application
	 * @return The name
	 */
	String getApplicationName();
}
