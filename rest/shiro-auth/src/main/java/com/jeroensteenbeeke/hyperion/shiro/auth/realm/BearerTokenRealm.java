package com.jeroensteenbeeke.hyperion.shiro.auth.realm;

import com.jeroensteenbeeke.hyperion.shiro.auth.components.HyperionShiroApplicationContextHolder;
import com.jeroensteenbeeke.hyperion.shiro.auth.components.bearer.BearerTokenAuthInfoFinder;
import com.jeroensteenbeeke.hyperion.shiro.auth.components.bearer.BearerTokenCredentialsMatcher;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.BearerToken;
import org.apache.shiro.realm.AuthenticatingRealm;

/**
 * Shiro realm for bearer token authentication
 */
public class BearerTokenRealm extends AuthenticatingRealm {
	private String qualifier;

	/**
	 * Constructor
	 */
	public BearerTokenRealm() {
		setCredentialsMatcher(
			(token, info) -> HyperionShiroApplicationContextHolder.getBean(BearerTokenCredentialsMatcher.class,
					qualifier)
				.map(credMatcher -> {
					if (token instanceof BearerToken bearerToken && info != null) {
						return credMatcher.doCredentialsMatch(bearerToken, info);
					}
					return false;
				}).getOrElse(false));
		setAuthenticationTokenClass(BearerToken.class);
	}

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		if (token instanceof BearerToken bearerToken) {
			return HyperionShiroApplicationContextHolder.getBean(BearerTokenAuthInfoFinder.class, qualifier)
				.flatMap(authInfoFinder -> authInfoFinder.findAuthenticationInfo(bearerToken)).getOrNull();
		}
		return null;
	}

	/**
	 * Sets the expected qualifier for the beans to use for strategies
	 * @param qualifier The qualifier
	 */
	public void setQualifier(String qualifier) {
		this.qualifier = qualifier;
	}
}
