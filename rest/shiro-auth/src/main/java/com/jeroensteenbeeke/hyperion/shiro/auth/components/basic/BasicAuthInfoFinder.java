package com.jeroensteenbeeke.hyperion.shiro.auth.components.basic;

import io.vavr.control.Option;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.jetbrains.annotations.NotNull;

/**
 * Finds AuthenticationInfo based on Basic Auth (username and password)
 */
public interface BasicAuthInfoFinder {
	/**
	 * Finds the AuthenticationInfo tied to the supplied token, if any
	 * @param usernamePasswordToken The token
	 * @return An Option containing the AuthenticationInfo if one exists for the given token, or an empty Option otherwise
	 */
	Option<AuthenticationInfo> findAuthInfo(@NotNull UsernamePasswordToken usernamePasswordToken);
}
