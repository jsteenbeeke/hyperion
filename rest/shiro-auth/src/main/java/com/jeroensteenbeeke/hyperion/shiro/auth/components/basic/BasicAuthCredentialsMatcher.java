package com.jeroensteenbeeke.hyperion.shiro.auth.components.basic;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.jetbrains.annotations.NotNull;

/**
 * Credentials matcher for basic auth. For each different realm, you should provide a qualified implementation of
 * this interface
 */
public interface BasicAuthCredentialsMatcher {
	/**
	 * Verifies if the provided credentials match the info on record
	 * @param presentedToken The presented token
	 * @param authenticationInfo The authentication info belonging to the user
	 * @return {@code true} if the credentials match, {@code false} otherwise
	 */
	boolean doCredentialsMatch(
		@NotNull UsernamePasswordToken presentedToken, @NotNull AuthenticationInfo authenticationInfo);
}
