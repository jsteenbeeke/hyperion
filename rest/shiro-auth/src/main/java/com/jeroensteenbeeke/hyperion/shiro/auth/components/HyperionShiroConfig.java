package com.jeroensteenbeeke.hyperion.shiro.auth.components;

import com.jeroensteenbeeke.hyperion.shiro.auth.EnableShiroAuth;
import com.jeroensteenbeeke.hyperion.shiro.auth.EnableShiroAuth_;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.AnnotatedGenericBeanDefinition;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

import java.util.Map;

/**
 * Spring configuration initializer
 */
public class HyperionShiroConfig implements ImportBeanDefinitionRegistrar {
	private static final Logger log = LoggerFactory.getLogger(HyperionShiroConfig.class);

	private static final String SHIRO_ANNOTATION_FQDN =
		EnableShiroAuth.class.getName();

	@Override
	public void registerBeanDefinitions(
		@NotNull AnnotationMetadata importingClassMetadata,
		@NotNull BeanDefinitionRegistry registry) {
		log.info("Enabling Shiro realm supporting beans");

		if (importingClassMetadata.hasAnnotation(SHIRO_ANNOTATION_FQDN)) {
			Map<String, Object> shiroConfig =
				importingClassMetadata.getAnnotationAttributes(SHIRO_ANNOTATION_FQDN);

			if (shiroConfig != null) {
				boolean enableBasicAuth = (boolean) shiroConfig.get(EnableShiroAuth_.METHOD_USES_BASIC_AUTH);
				boolean enableBearerToken = (boolean) shiroConfig.get(EnableShiroAuth_.METHOD_USES_BEARER_TOKEN_AUTH);
				Qualifier[] basicQualifiers = (Qualifier[]) shiroConfig.get(EnableShiroAuth_.METHOD_BASIC_QUALIFIERS);
				Qualifier[] bearerQualifiers = (Qualifier[]) shiroConfig.get(EnableShiroAuth_.METHOD_BEARER_QUALIFIERS);

				registerSingletonBeanDefinition(registry, new HyperionShiroApplicationContextHolder());
				registerSingletonBeanDefinition(registry,
					new HyperionShiroConfigValidator(enableBasicAuth, enableBearerToken, basicQualifiers, bearerQualifiers));
			} else {
				throw new IllegalStateException("HyperionShiroConfig used directly without @EnableShiroAuth");
			}
		} else {
			throw new IllegalStateException("HyperionShiroConfig used directly without @EnableShiroAuth");
		}
	}

	private <T> void registerSingletonBeanDefinition(@NotNull BeanDefinitionRegistry registry, @NotNull T instance) {
		AnnotatedGenericBeanDefinition beanDefinition = new AnnotatedGenericBeanDefinition(
			instance.getClass());
		beanDefinition.setInstanceSupplier(() -> instance);
		registry.registerBeanDefinition(instance.getClass().getSimpleName(), beanDefinition);
	}
}
