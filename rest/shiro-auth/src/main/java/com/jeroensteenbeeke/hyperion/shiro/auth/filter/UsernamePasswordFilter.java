package com.jeroensteenbeeke.hyperion.shiro.auth.filter;

import com.jeroensteenbeeke.hyperion.shiro.auth.Authorization;
import com.jeroensteenbeeke.hyperion.shiro.auth.Constants;
import com.jeroensteenbeeke.hyperion.shiro.auth.components.ApplicationNameProvider;
import com.jeroensteenbeeke.hyperion.shiro.auth.components.HyperionShiroApplicationContextHolder;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.web.filter.authc.AuthenticatingFilter;

import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * Filter for username/password (HTTP basic auth) filter
 */
public class UsernamePasswordFilter extends AuthenticatingFilter {
	@Override
	protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) {
		if (request instanceof HttpServletRequest httpServletRequest) {
			return Authorization.readBasicAuth(httpServletRequest.getHeader(Constants.HttpHeaders.AUTHORIZATION))
					.getOrNull();
		}

		return null;
	}

	@Override
	protected boolean onAccessDenied(ServletRequest request, ServletResponse response) {
		if (request instanceof HttpServletRequest httpServletRequest && response instanceof HttpServletResponse httpServletResponse) {

			if (httpServletRequest.getHeader(Constants.HttpHeaders.AUTHORIZATION) != null) {
				httpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
			} else {
				httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				String authcHeader = HttpServletRequest.BASIC_AUTH + " realm=\"" + getApplicationName() + "\"";
				httpServletResponse.setHeader(Constants.HttpHeaders.AUTHENTICATE, authcHeader);
			}
		}

		return false;
	}

	private String getApplicationName() {
		return HyperionShiroApplicationContextHolder.getBean(ApplicationNameProvider.class, null)
			.map(ApplicationNameProvider::getApplicationName)
			.getOrElseThrow(() -> new IllegalStateException("Could not determine application name"));
	}
}
