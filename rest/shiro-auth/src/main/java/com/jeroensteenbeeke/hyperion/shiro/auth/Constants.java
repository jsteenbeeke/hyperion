package com.jeroensteenbeeke.hyperion.shiro.auth;

/**
 * Defines constants used by the auth components
 */
public class Constants {
	/**
	 * HTTP Header constants
	 */
	public static class HttpHeaders {
		/**
		 * Authorization header key
		 */
		public static final String AUTHORIZATION = "Authorization";
		/**
		 * Authenticate header key
		 */
		public static final String AUTHENTICATE = "WWW-Authenticate";
	}
}
