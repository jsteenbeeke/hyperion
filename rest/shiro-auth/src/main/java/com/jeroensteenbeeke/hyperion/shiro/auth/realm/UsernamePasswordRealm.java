package com.jeroensteenbeeke.hyperion.shiro.auth.realm;

import com.jeroensteenbeeke.hyperion.shiro.auth.components.HyperionShiroApplicationContextHolder;
import com.jeroensteenbeeke.hyperion.shiro.auth.components.basic.BasicAuthCredentialsMatcher;
import com.jeroensteenbeeke.hyperion.shiro.auth.components.basic.BasicAuthInfoFinder;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.realm.AuthenticatingRealm;

/**
 * Shiro realm for basic authentication
 */
public class UsernamePasswordRealm extends AuthenticatingRealm {
	private String qualifier;

	/**
	 * Constructor
	 */
	public UsernamePasswordRealm() {
		setCredentialsMatcher(
			(token, info) -> HyperionShiroApplicationContextHolder.getBean(BasicAuthCredentialsMatcher.class,
					qualifier)
				.map(credMatcher -> {
					if (token instanceof UsernamePasswordToken usernamePasswordToken && info != null) {
						return credMatcher.doCredentialsMatch(usernamePasswordToken, info);
					}
					return false;
				}).getOrElse(false));
		setAuthenticationTokenClass(UsernamePasswordToken.class);
	}

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		if (token instanceof UsernamePasswordToken usernamePasswordToken) {
			return HyperionShiroApplicationContextHolder.getBean(BasicAuthInfoFinder.class, qualifier)
				.flatMap(authInfoFinder -> authInfoFinder.findAuthInfo(usernamePasswordToken)).getOrNull();
		}
		return null;
	}

	/**
	 * Sets the expected qualifier for the beans to use for strategies
	 * @param qualifier The qualifier
	 */
	public void setQualifier(String qualifier) {
		this.qualifier = qualifier;
	}
}
