package com.jeroensteenbeeke.hyperion.shiro.auth.components;

import io.vavr.control.Option;
import io.vavr.control.Try;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.BeanFactoryAnnotationUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Reference to the Spring application context usable by the shiro components
 */
public class HyperionShiroApplicationContextHolder implements ApplicationContextAware {

	private static ApplicationContext applicationContext;


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		HyperionShiroApplicationContextHolder.applicationContext = applicationContext;
	}

	/**
	 * Gets the application context, if set
	 * @return An Option containing the applicationContext, if it exists. Empty option otherwise
	 */
	public static Option<ApplicationContext> getApplicationContext() {
		return Option.of(applicationContext);
	}

	/**
	 * Gets a bean of the specified class, with an optional qualifier
	 * @param beanClass The type of bean to get
	 * @param qualifier The qualifier to use in finding the bean
	 * @return An Option containing the bean, or an empty Option otherwise
	 * @param <T> The type of bean
	 */
	public static <T> Option<T> getBean(@NotNull Class<T> beanClass, @Nullable String qualifier) {
		if (qualifier == null) {
			return Try.of(() -> applicationContext.getBean(beanClass)).toOption();
		}

		return Try.of(() -> BeanFactoryAnnotationUtils.qualifiedBeanOfType(applicationContext.getAutowireCapableBeanFactory(),
			beanClass, qualifier)).toOption();
	}
}
