package com.jeroensteenbeeke.hyperion.shiro.auth.components;

import com.jeroensteenbeeke.hyperion.shiro.auth.components.basic.BasicAuthInfoFinder;
import com.jeroensteenbeeke.hyperion.shiro.auth.components.bearer.BearerTokenAuthInfoFinder;
import com.jeroensteenbeeke.hyperion.shiro.auth.components.bearer.BearerTokenCredentialsMatcher;
import io.vavr.collection.Array;
import io.vavr.collection.Stream;
import io.vavr.control.Option;
import io.vavr.control.Try;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.BeanFactoryAnnotationUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Validator for Shiro functionality, checks if all required beans exist
 */
public class HyperionShiroConfigValidator implements ApplicationListener<ContextRefreshedEvent> {
	private static final Logger log = LoggerFactory.getLogger(HyperionShiroConfigValidator.class);

	private final boolean enableBasicAuth;

	private final boolean enableBearerToken;
	private final Qualifier[] basicQualifiers;
	private final Qualifier[] bearerQualifiers;

	HyperionShiroConfigValidator(
		boolean enableBasicAuth, boolean enableBearerToken, @NotNull Qualifier[] basicQualifiers,
		@NotNull Qualifier[] bearerQualifiers) {
		this.enableBasicAuth = enableBasicAuth;
		this.enableBearerToken = enableBearerToken;
		this.basicQualifiers = basicQualifiers;
		this.bearerQualifiers = bearerQualifiers;
	}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		var applicationContext = event.getApplicationContext();

		log.info("Validating Hyperion Shiro configuration");

		var requiredBeans = Array.<QualifiedBeanDescriptor<?>>empty();

		requiredBeans = requiredBeans.appendAll(
			descriptor(applicationContext, ApplicationNameProvider.class, new Qualifier[0]));

		if (enableBasicAuth) {
			requiredBeans = requiredBeans.appendAll(
				descriptor(applicationContext, BasicAuthInfoFinder.class, basicQualifiers));
		}

		if (enableBearerToken) {
			requiredBeans = requiredBeans.appendAll(
				descriptor(applicationContext, BearerTokenAuthInfoFinder.class, bearerQualifiers));
			requiredBeans = requiredBeans.appendAll(
				descriptor(applicationContext, BearerTokenCredentialsMatcher.class, bearerQualifiers));

		}

		int maxLength = requiredBeans.map(QualifiedBeanDescriptor::beanName).map(String::length).max().getOrElse(0);

		for (QualifiedBeanDescriptor<?> requiredBean : requiredBeans) {
			int paddingLength = maxLength - requiredBean.beanName().length();
			String padding = Stream.fill(paddingLength, " ").collect(Collectors.joining());

			log.info(" - {}{} : {}", requiredBean.toQualification(), padding, defined(requiredBean.bean()));
		}

		var missing = requiredBeans.filter(t -> t.bean().isEmpty()).map(QualifiedBeanDescriptor::toQualification);

		if (!missing.isEmpty()) {
			throw new IllegalStateException(
				"Missing bean definitions: " + missing.map(Qualification::toString).collect(Collectors.joining(", ")));
		}

	}

	private String defined(@NotNull Option<?> option) {
		return option.isDefined() ? "Yes" : "No";
	}

	@NotNull
	private <T> Array<QualifiedBeanDescriptor<T>> descriptor(
		@NotNull ApplicationContext applicationContext, @NotNull Class<T> beanClass, @NotNull Qualifier[] qualifiers) {
		Array<String> qualifierArray = qualifiers.length > 0 ? Array.of(qualifiers).map(Qualifier::value) : Array.of("");


		return qualifierArray.map(qualifier -> new QualifiedBeanDescriptor<>(beanClass.getSimpleName(), qualifier,
			Try.of(() -> getBean(applicationContext, beanClass, qualifier)).toOption()));
	}

	private <T> T getBean(ApplicationContext applicationContext, Class<T> beanClass, String qualifier) {
		if (Objects.equals(qualifier, "")) {
			return applicationContext.getBean(beanClass);
		}

		return BeanFactoryAnnotationUtils.qualifiedBeanOfType(applicationContext.getAutowireCapableBeanFactory(),
			beanClass, qualifier);
	}

	private record QualifiedBeanDescriptor<T>(@NotNull String beanName, @NotNull String qualifier,
											  @NotNull Option<T> bean) {
		public Qualification toQualification() {
			return new Qualification(beanName(), qualifier().isEmpty() ? null : qualifier());
		}
	}

	private record Qualification(@NotNull String beanName, @Nullable String qualifier) {

		@Override
		public String toString() {
			return qualifier != null ? beanName : "%s (%s)".formatted(beanName(), qualifier());
		}
	}

}
