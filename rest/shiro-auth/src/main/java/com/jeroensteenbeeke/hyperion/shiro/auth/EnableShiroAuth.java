package com.jeroensteenbeeke.hyperion.shiro.auth;

import com.jeroensteenbeeke.hyperion.annotation.Reflectable;
import com.jeroensteenbeeke.hyperion.shiro.auth.components.HyperionShiroApplicationContextHolder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Activates shiro-related beans
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(HyperionShiroApplicationContextHolder.class)
@Reflectable
public @interface EnableShiroAuth {
	/**
	 * Enable basic auth
	 * @return {@code true} if basic auth should be enabled, {@code false} otherwise
	 */
	boolean usesBasicAuth();

	/**
	 * Enable bearer token auth
	 * @return {@code true} if bearer token auth should be enabled, {@code false} otherwise
	 */
	boolean usesBearerTokenAuth();

	/**
	 * In case of multiple endpoints with different sets of credentials, a qualifier is required to distinguish
	 * between the various implementations. In case of an empty array a single implementation is assumed
	 * @return An array of qualifiers
	 */
	Qualifier[] basicQualifiers() default {};

	/**
	 * In case of multiple endpoints with different sets of credentials, a qualifier is required to distinguish
	 * between the various implementations. In case of an empty array a single implementation is assumed
	 * @return An array of qualifiers
	 */
	Qualifier[] bearerQualifiers() default {};

}
