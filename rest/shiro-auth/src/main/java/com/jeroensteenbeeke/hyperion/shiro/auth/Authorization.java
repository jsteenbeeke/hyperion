package com.jeroensteenbeeke.hyperion.shiro.auth;

import io.vavr.control.Option;
import org.apache.shiro.authc.BearerToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;

import static io.vavr.control.Option.none;
import static io.vavr.control.Option.some;

/**
 * Utility class for extracting common authorization header information
 */
public class Authorization {

	private static final char ZERO = '\0';

	private static class Basic {
		static final char[] PREFIX = "Basic ".toCharArray();

		static final char SEPARATOR = ':';
	}

	private static class Bearer {
		static final String PREFIX = "Bearer ";
	}
	
	/**
	 * Extracts a Shiro UsernamePasswordToken from an HTTP Authorization header, assuming the
	 * use of HTTP Basic auth
	 * @param authHeader The header from the request
	 * @return An Option creating either the parsed token, or an empty Option in case it could not be extracted
	 */
	@NotNull
	public static Option<UsernamePasswordToken> readBasicAuth(@Nullable String authHeader) {
		if (authHeader == null) {
			return none();
		}

		char[] authHeaderArr = authHeader.toCharArray();
		char[] actualAuth = new char[0];
		byte[] encoded = new byte[0];
		byte[] decoded = new byte[0];
		char[] decodedChars = new char[0];
		char[] username = new char[0];

		try {
			if (!startsWith(authHeaderArr, Basic.PREFIX)) {
				return none();
			}

			actualAuth = Arrays.copyOfRange(authHeaderArr, Basic.PREFIX.length, authHeaderArr.length);

			if (actualAuth.length % 4 != 0) {
				// Invalid length to be Base64
				return none();
			}

			for (char c : actualAuth) {
				boolean isDigit = c >= '0' && c <= '9';
				boolean isLowercaseLetter = c >= 'a' && c <= 'z';
				boolean isUppercaseLetter = c >= 'A' && c <= 'Z';
				boolean isAllowedSpecialCharacter = c == '/' || c == '+' || c == '=';

				if (!isDigit && !isLowercaseLetter && !isUppercaseLetter && !isAllowedSpecialCharacter) {
					// Illegal base64 character
					return none();
				}
			}

			encoded = toByteArray(actualAuth);
			decoded = Base64.getDecoder().decode(encoded);
			decodedChars = toCharArray(decoded);

			int semi = findBasicAuthSeparator(decodedChars);
			if (semi == -1) {
				return none();
			}

			username = Arrays.copyOfRange(decodedChars, 0, semi);
			char[] password = Arrays.copyOfRange(decodedChars, semi + 1, decodedChars.length);

			return some(new UsernamePasswordToken(new String(username), password));
		} finally {
			Arrays.fill(authHeaderArr, ZERO);
			Arrays.fill(actualAuth, ZERO);
			Arrays.fill(encoded, (byte) 0);
			Arrays.fill(decoded, (byte) 0);
			Arrays.fill(decodedChars, ZERO);
			Arrays.fill(username, ZERO);
		}
	}

	/**
	 * Extracts a Shiro BearerToken from an HTTP Authorization header, requiring a header in the following form:
	 *
	 * <code>
	 *     Authorization: Bearer yourTokenGoesHere
	 * </code>
	 *
	 * Bearer tokens do not receive the same scrutiny as passwords in the sense that they're stored as Strings in-memory,
	 * rather than char[]s. Implementors should strive to have a method to revoke tokens in the event of a breach.
	 *
	 * @param authHeader The header to analyze
	 * @return An Option containing the token, or an empty Option if it could not be read
	 */
	@NotNull
	public static Option<BearerToken> readBearerToken(@Nullable String authHeader) {
		if (authHeader == null) {
			return none();
		}

		if (!authHeader.startsWith(Bearer.PREFIX)) {
			return none();
		}

		String tokenString = authHeader.substring(Bearer.PREFIX.length());

		return some(new BearerToken(tokenString));
	}

	private static boolean startsWith(@NotNull char[] input, @NotNull char[] prefix) {
		if (input.length < prefix.length) {
			return false;
		}

		if (input.length == prefix.length) {
			return Arrays.equals(input, prefix);
		}

		char[] prefixOfInput = Arrays.copyOfRange(input, 0, prefix.length);

		try {
			return Arrays.equals(prefixOfInput, prefix);
		} finally {
			Arrays.fill(prefixOfInput, ZERO);
		}
	}

	private static byte[] toByteArray(@NotNull char[] chars) {
		ByteBuffer encoded = StandardCharsets.UTF_8.encode(CharBuffer.wrap(chars));
		byte[] returnValue = new byte[encoded.remaining()];
		encoded.get(returnValue);
		return returnValue;
	}

	private static char[] toCharArray(@NotNull byte[] bytes) {
		return StandardCharsets.UTF_8.decode(ByteBuffer.wrap(bytes)).array();
	}

	private static int findBasicAuthSeparator(@NotNull char[] haystack) {
		int i = 0;
		for (char c : haystack) {
			if (c == Basic.SEPARATOR) {
				return i;
			}
			i++;
		}

		return -1;
	}
}
