package com.jeroensteenbeeke.hyperion.rest.test;

import java.io.IOException;

/**
 * @param <LOGIN_REQUEST> The type of request object to use for logging in
 * @param <LOGIN_RESULT> The type of result object received on a successful login
 */
public abstract class AbstractAuthenticatingBackendTest<LOGIN_REQUEST,LOGIN_RESULT> extends AbstractBackendTest {
	private final Class<LOGIN_RESULT> loginResultClass;

	/**
	 * Create a new instance of this class with the given type as expected result type
	 * @param loginResultClass The type of result class
	 */
	protected AbstractAuthenticatingBackendTest(Class<LOGIN_RESULT> loginResultClass) {
		this.loginResultClass = loginResultClass;
	}

	/**
	 * Creates a login request for the given username
	 * @param username The username to use for the login request
	 * @return An instance of the LOGIN_REQUEST type
	 */
	protected abstract LOGIN_REQUEST createLoginRequest(String username);

	/**
	 * Retrieves an access token from the returned login result
	 * @param result The result that contains the token
	 * @return An access token
	 */
	protected abstract String extractToken(LOGIN_RESULT result);

	/**
	 * Determines the path to post authentication requests to
	 * @return The path to post authentication to (for example: {@code /auth/userpass})
	 */
	protected abstract String getAuthPath();

	/**
	 * Log in as the given user, returning an instance of LOGIN_RESULT
	 * @param username The username to use to log in
	 * @return A LOGIN_RESULT instance
	 * @throws IOException If the communication failed
	 */
	protected LOGIN_RESULT rawLoginAs(String username) throws IOException {

		return post(getAuthPath(), createLoginRequest(username)).expectingOK()
																.getAs(loginResultClass);
	}

	/**
	 * Logs in as the given user and returns the authorization token
	 * @param username The user to log in as
	 * @return A bearer token
	 * @throws IOException If the communication failed
	 */
	protected String loginAs(String username) throws IOException {
		return extractToken(rawLoginAs(username));
	}
}
