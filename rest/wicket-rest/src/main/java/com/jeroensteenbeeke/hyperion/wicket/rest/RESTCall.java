package com.jeroensteenbeeke.hyperion.wicket.rest;

import org.jetbrains.annotations.NotNull;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * Static methods for dealing with JAX-RS responses in a more generic-friendly fashion
 */
public class RESTCall {
	/**
	 * Unused constructor, do not call
	 */
	RESTCall() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Creates a builder that takes the given response and handles it in a manner specified
	 *
	 * @param response The response to extract
	 * @return A builder stage for specifying the expected return status
	 */
	@NotNull
	public static ExpectedStatusStage call(@NotNull Supplier<Response> response) {
		return new ExpectedStatusStage(response);
	}

	/**
	 * Build stage for specifying the expected return status
	 */
	public static class ExpectedStatusStage {
		private final Supplier<Response> response;

		private ExpectedStatusStage(@NotNull Supplier<Response> response) {
			this.response = response;
		}

		/**
		 * Set the expected return status
		 *
		 * @param expectedStatus The status we are expecting
		 * @return A builder stage for setting an error handler
		 */
		@NotNull
		public ErrorHandlerStage expecting(@NotNull Response.Status expectedStatus) {
			return new ErrorHandlerStage(response, expectedStatus);
		}

	}

	/**
	 * Builder stage for setting an error handler
	 */
	public static class ErrorHandlerStage {
		private final Supplier<Response> response;

		private final Response.Status expectedStatus;

		private ErrorHandlerStage(@NotNull Supplier<Response> response, @NotNull Response.Status expectedStatus) {
			this.response = response;
			this.expectedStatus = expectedStatus;
		}

		/**
		 * Sets an error handler to be called when the response has an unexpected status
		 *
		 * @param errorConsumer A biconsumer that receives the actual error code and message
		 * @return A finalizer stage
		 */
		@NotNull
		public UntypedFinalizer onError(@NotNull BiConsumer<Integer, String> errorConsumer) {
			return new UntypedFinalizer(response, expectedStatus, errorConsumer);
		}
	}

	/**
	 * Basic finalizer stage that disregards the response body
	 */
	public static class UntypedFinalizer {
		private final Supplier<Response> responseSupplier;

		private final Response.Status expectedStatus;

		private final BiConsumer<Integer, String> errorConsumer;

		private UntypedFinalizer(@NotNull Supplier<Response> responseSupplier, @NotNull Response.Status expectedStatus,
								 @NotNull BiConsumer<Integer, String> errorConsumer) {
			this.responseSupplier = responseSupplier;
			this.expectedStatus = expectedStatus;
			this.errorConsumer = errorConsumer;
		}

		/**
		 * Create a new finalizer stage that expects a single output record of the given type
		 *
		 * @param expectedType A class representing the output type expected
		 * @param <OUTPUT>     The type of output expected
		 * @return A new finalizer stage
		 */
		@NotNull
		public <OUTPUT> TypedFinalizer<OUTPUT> yielding(@NotNull Class<OUTPUT> expectedType) {
			return new TypedFinalizer<>(responseSupplier, expectedStatus, errorConsumer, expectedType);

		}

		/**
		 * Create a new finalizer stage that expects a list of records of the given type
		 *
		 * @param expectedType A GenericType instance that represents a list of the output type
		 * @param <OUTPUT>     The type of output expected
		 * @return A new finalizer stage
		 */
		@NotNull
		public <OUTPUT> ListTypedFinalizer<OUTPUT> yielding(@NotNull GenericType<List<OUTPUT>>
																	expectedType) {
			return new ListTypedFinalizer<>(responseSupplier, expectedStatus, errorConsumer, expectedType);

		}

		/**
		 * Processes the response
		 *
		 * @param action The action to perform if the response was as expected
		 */
		public void andThen(@NotNull Runnable action) {
			try (Response response = responseSupplier.get()) {
				if (response.getStatus() == expectedStatus.getStatusCode()) {
					action.run();
				} else {
					onError(response, errorConsumer);
				}
			}
		}
	}

	/**
	 * Finalizer that expects a single output record
	 *
	 * @param <OUTPUT> The expected output type
	 */
	public static class TypedFinalizer<OUTPUT> {
		private final Supplier<Response> responseSupplier;

		private final Response.Status expectedStatus;

		private final BiConsumer<Integer, String> errorConsumer;

		private final Class<OUTPUT> outputType;

		private TypedFinalizer(Supplier<Response> responseSupplier, Response.Status expectedStatus,
							   BiConsumer<Integer, String> errorConsumer, Class<OUTPUT> outputType) {
			this.responseSupplier = responseSupplier;
			this.expectedStatus = expectedStatus;
			this.errorConsumer = errorConsumer;
			this.outputType = outputType;
		}

		/**
		 * Processes the response, calling {@code onError} as a side effect if not succesful
		 *
		 * @param consumer Consumer for the output if successful
		 */
		public void andThen(Consumer<OUTPUT> consumer) {
			try (Response response = responseSupplier.get()) {
				if (response.getStatus() == expectedStatus.getStatusCode()) {
					consumer.accept(response.readEntity(outputType));
				} else {
					onError(response, errorConsumer);
				}
			}
		}

		/**
		 * Processes the response, wrapping the result object in an optional, calling {@code
		 * onError} as a side effect if not succesful
		 *
		 * @return An optional containing the result object if successful, or empty otherwise
		 */
		public Optional<OUTPUT> asOptional() {
			try (Response response = responseSupplier.get()) {
				if (response.getStatus() == expectedStatus.getStatusCode()) {
					return Optional.of(response.readEntity(outputType));
				} else {
					onError(response, errorConsumer);
					return Optional.empty();
				}
			}
		}
	}

	/**
	 * Finalizer that expects a list of the given type
	 *
	 * @param <OUTPUT> The type contained in the list
	 */
	public static class ListTypedFinalizer<OUTPUT> {
		private final Supplier<Response> responseSupplier;

		private final Response.Status expectedStatus;

		private final BiConsumer<Integer, String> errorConsumer;

		private GenericType<List<OUTPUT>> outputType;

		private ListTypedFinalizer(Supplier<Response> responseSupplier, Response.Status expectedStatus,
								   BiConsumer<Integer, String> errorConsumer,
								   GenericType<List<OUTPUT>>
										   outputType) {
			this.responseSupplier = responseSupplier;
			this.expectedStatus = expectedStatus;
			this.errorConsumer = errorConsumer;
			this.outputType = outputType;
		}

		/**
		 * Processes the response
		 *
		 * @param consumer Consumer for the output if successful
		 */
		public void andThen(Consumer<List<OUTPUT>> consumer) {
			try (Response response = responseSupplier.get()) {
				if (response.getStatus() == expectedStatus.getStatusCode()) {
					consumer.accept(response.readEntity(outputType));
				} else {
					onError(response, errorConsumer);
				}
			}
		}

		/**
		 * Processes the response, wrapping the result object list in an optional, calling {@code
		 * onError} as a side effect if not succesful
		 *
		 * @return An optional containing the result object list if successful, or empty otherwise
		 */
		public Optional<List<OUTPUT>> asOptional() {
			try (Response response = responseSupplier.get()) {
				if (response.getStatus() == expectedStatus.getStatusCode()) {
					return Optional.of(response.readEntity(outputType));
				} else {
					onError(response, errorConsumer);
					return Optional.empty();
				}
			}
		}

	}

	private static void onError(Response response, BiConsumer<Integer, String> errorConsumer) {
		if (response.hasEntity() && response.bufferEntity()) {
			String entity = response.readEntity(String.class);
			errorConsumer.accept(response.getStatus(), entity);
		} else {
			errorConsumer.accept(response.getStatus(), String.format("Remote returned " +
																			 "error %d",
																	 response.getStatus()));
		}
	}
}
