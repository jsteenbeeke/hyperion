package com.jeroensteenbeeke.hyperion.wicket.rest;

import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;

public class TestObject implements QueryObject<String> {
	private static final long serialVersionUID = -5992389964177615964L;

	private int nextSortIndex;

	@Override
	public int getNextSortIndex() {
		return nextSortIndex++;
	}

	@Override
	public boolean matches(String object) {
		return true;
	}
}
