package com.jeroensteenbeeke.hyperion.rest.query;

import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

import org.jetbrains.annotations.NotNull;

/**
 * Query object property representing a ZonedDateTime value, that can be queried as any comparable
 *
 * @param <T> The type of query object that contains this property
 */
public class DateTimeProperty<T extends QueryObject<?>> extends ComparableProperty<T, DateTime> {

	private static final long serialVersionUID = -4438075026284668494L;

	/**
	 * Create a new ZonedDateTime property for the given query object
	 *
	 * @param target    The query object containing this property
	 * @param fieldName The name of the field this property is assigned to
	 */
	public DateTimeProperty(@NotNull T target, @NotNull String fieldName) {
		super(target, fieldName);
	}

	@Override
	protected String getStringRepresentation(@NotNull DateTime dateTime) {
		return ISODateTimeFormat.dateTime().print(dateTime);
	}
}
