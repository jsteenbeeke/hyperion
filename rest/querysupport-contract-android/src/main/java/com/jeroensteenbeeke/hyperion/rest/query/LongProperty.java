package com.jeroensteenbeeke.hyperion.rest.query;

import com.jeroensteenbeeke.hyperion.rest.querysupport.ILongProperty;
import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;

import org.jetbrains.annotations.NotNull;

/**
 * Query object property representing a Long value, that can be queried as any comparable
 *
 * @param <T> The type of query object that contains this property
 */
public class LongProperty<T extends QueryObject<?>> extends ComparableProperty<T, Long> implements ILongProperty<T> {

	private static final long serialVersionUID = 3622004762880505863L;

	/**
	 * Create a new Long property for the given query object
	 *
	 * @param target    The query object containing this property
	 * @param fieldName The name of the field this property is assigned to
	 */
	public LongProperty(@NotNull T target, @NotNull String fieldName) {
		super(target, fieldName);
	}

	@Override
	protected String getStringRepresentation(@NotNull Long aLong) {
		return aLong.toString();
	}
}
