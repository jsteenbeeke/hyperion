package com.jeroensteenbeeke.hyperion.rest.query;

import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;

import org.jetbrains.annotations.NotNull;

/**
 * Query object property representing a Short value, that can be queried as any comparable
 *
 * @param <T> The type of query object that contains this property
 */
public class ShortProperty<T extends QueryObject<?>> extends ComparableProperty<T, Short> {
	private static final long serialVersionUID = -451118842436852884L;

	/**
	 * Create a new Short property for the given query object
	 *
	 * @param target    The query object containing this property
	 * @param fieldName The name of the field this property is assigned to
	 */
	public ShortProperty(@NotNull T target, @NotNull String fieldName) {
		super(target, fieldName);
	}

	@Override
	protected String getStringRepresentation(@NotNull Short aShort) {
		return aShort.toString();
	}
}
