package com.jeroensteenbeeke.hyperion.rest.query;

import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import org.joda.time.LocalDate;
import org.joda.time.format.ISODateTimeFormat;

import org.jetbrains.annotations.NotNull;


/**
 * Query object property representing a LocalDate value, that can be queried as any comparable
 *
 * @param <T> The type of query object that contains this property
 */
public class LocalDateProperty<T extends QueryObject<?>> extends ComparableProperty<T, LocalDate> {

	private static final long serialVersionUID = 7508605215136657919L;

	/**
	 * Create a new LocalDate property for the given query object
	 *
	 * @param target    The query object containing this property
	 * @param fieldName The name of the field this property is assigned to
	 */
	public LocalDateProperty(@NotNull T target, @NotNull String fieldName) {
		super(target, fieldName);
	}

	@Override
	protected String getStringRepresentation(@NotNull LocalDate localDate) {
		return ISODateTimeFormat.date().print(localDate);
	}
}
