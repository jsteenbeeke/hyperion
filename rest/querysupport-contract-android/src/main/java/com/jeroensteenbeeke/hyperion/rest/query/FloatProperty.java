package com.jeroensteenbeeke.hyperion.rest.query;

import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;

import org.jetbrains.annotations.NotNull;

/**
 * Query object property representing a Float value, that can be queried as any comparable
 *
 * @param <T> The type of query object that contains this property
 */
public class FloatProperty<T extends QueryObject<?>> extends ComparableProperty<T, Float> {
	private static final long serialVersionUID = 1892733113934307165L;

	/**
	 * Create a new Float property for the given query object
	 *
	 * @param target    The query object containing this property
	 * @param fieldName The name of the field this property is assigned to
	 */
	public FloatProperty(@NotNull T target, @NotNull String fieldName) {
		super(target, fieldName);
	}

	@Override
	protected String getStringRepresentation(@NotNull Float aFloat) {
		return aFloat.toString();
	}
}
