package com.jeroensteenbeeke.hyperion.rest.query;

import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;
import org.joda.time.LocalDateTime;
import org.joda.time.format.ISODateTimeFormat;

import org.jetbrains.annotations.NotNull;


/**
 * Query object property representing a LocalDateTime value, that can be queried as any comparable
 *
 * @param <T> The type of query object that contains this property
 */
public class LocalDateTimeProperty<T extends QueryObject<?>> extends ComparableProperty<T, LocalDateTime> {
	private static final long serialVersionUID = 4083529588195612874L;

	/**
	 * Create a new LocalDateTime property for the given query object
	 *
	 * @param target    The query object containing this property
	 * @param fieldName The name of the field this property is assigned to
	 */
	public LocalDateTimeProperty(@NotNull T target, @NotNull String fieldName) {
		super(target, fieldName);
	}

	@Override
	protected String getStringRepresentation(@NotNull LocalDateTime localDateTime) {
		return ISODateTimeFormat.dateTime().withZoneUTC().print(localDateTime);
	}
}
