package com.jeroensteenbeeke.hyperion.rest.query;

import com.jeroensteenbeeke.hyperion.rest.querysupport.IIntegerProperty;
import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;

import org.jetbrains.annotations.NotNull;

/**
 * Query object property representing a Integer value, that can be queried as any comparable
 *
 * @param <T> The type of query object that contains this property
 */
public class IntegerProperty<T extends QueryObject<?>> extends ComparableProperty<T, Integer>
		implements IIntegerProperty<T> {
	private static final long serialVersionUID = -412765176897493428L;

	/**
	 * Create a new Integer property for the given query object
	 *
	 * @param target    The query object containing this property
	 * @param fieldName The name of the field this property is assigned to
	 */
	public IntegerProperty(@NotNull T target, @NotNull String fieldName) {
		super(target, fieldName);
	}

	@Override
	protected String getStringRepresentation(@NotNull Integer integer) {
		return integer.toString();
	}
}
