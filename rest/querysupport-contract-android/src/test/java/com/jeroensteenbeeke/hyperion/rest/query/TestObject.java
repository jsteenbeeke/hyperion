package com.jeroensteenbeeke.hyperion.rest.query;

import com.jeroensteenbeeke.hyperion.rest.querysupport.QueryObject;

/**
 * Dummy implementation of a query object for testing purposes
 */
public class TestObject implements QueryObject<TestObject> {
	private static final long serialVersionUID = 1276499686615759605L;

	private BigDecimalProperty<TestObject> bigdecimal = new BigDecimalProperty<>(this, "bigdecimal");

	private BigIntegerProperty<TestObject> biginteger = new BigIntegerProperty<>(this, "biginteger");

	private BooleanProperty<TestObject> booleanVal = new BooleanProperty<>(this, "booleanVal");

	private DoubleProperty<TestObject> doubleVal = new DoubleProperty<>(this, "doubleVal");

	private FloatProperty<TestObject> floatVal = new FloatProperty<>(this, "floatVal");

	private IntegerProperty<TestObject> integer = new IntegerProperty<>(this, "integer");

	private LocalDateProperty<TestObject> localDate = new LocalDateProperty<>(this, "localDate");

	private LocalDateTimeProperty<TestObject> localDateTime = new LocalDateTimeProperty<>(this, "localDateTime");

	private LongProperty<TestObject> longVal = new LongProperty<>(this, "longVal");

	private ShortProperty<TestObject> shortVal = new ShortProperty<>(this, "shortVal");

	private StringProperty<TestObject> name = new StringProperty<>(this, "name");

	private DateTimeProperty<TestObject> zonedDateTime = new DateTimeProperty<>(this, "zonedDateTime");

	private int nextSortIndex = 0;

	/**
	 * Accessor for the BigDecimal property
	 * @return The property
	 */
	public BigDecimalProperty<TestObject> bigdecimal() {
		return bigdecimal;
	}

	/**
	 * Accessor for the BigInteger property
	 * @return The property
	 */
	public BigIntegerProperty<TestObject> biginteger() {
		return biginteger;
	}

	/**
	 * Accessor for the Boolean property
	 * @return The property
	 */
	public BooleanProperty<TestObject> booleanVal() {
		return booleanVal;
	}

	/**
	 * Accessor for the Double property
	 * @return The property
	 */
	public DoubleProperty<TestObject> doubleVal() {
		return doubleVal;
	}

	/**
	 * Accessor for the Float property
	 * @return The property
	 */
	public FloatProperty<TestObject> floatVal() {
		return floatVal;
	}

	/**
	 * Accessor for the Integer property
	 * @return The property
	 */
	public IntegerProperty<TestObject> integer() {
		return integer;
	}

	/**
	 * Accessor for the LocalDate property
	 * @return The property
	 */
	public LocalDateProperty<TestObject> localDate() {
		return localDate;
	}

	/**
	 * Accessor for the LocalDateTime property
	 * @return The property
	 */
	public LocalDateTimeProperty<TestObject> localDateTime() {
		return localDateTime;
	}

	/**
	 * Accessor for the Long property
	 * @return The property
	 */
	public LongProperty<TestObject> longVal() {
		return longVal;
	}

	/**
	 * Accessor for the Short property
	 * @return The property
	 */
	public ShortProperty<TestObject> shortVal() {
		return shortVal;
	}

	/**
	 * Accessor for the String property
	 * @return The property
	 */
	public StringProperty<TestObject> name() {
		return name;
	}

	/**
	 * Accessor for the DateTime property
	 * @return The property
	 */
	public DateTimeProperty<TestObject> zonedDateTime() {
		return zonedDateTime;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		TestObject that = (TestObject) o;

		if (!bigdecimal.equals(that.bigdecimal)) return false;
		if (!biginteger.equals(that.biginteger)) return false;
		if (!booleanVal.equals(that.booleanVal)) return false;
		if (!doubleVal.equals(that.doubleVal)) return false;
		if (!floatVal.equals(that.floatVal)) return false;
		if (!integer.equals(that.integer)) return false;
		if (!localDate.equals(that.localDate)) return false;
		if (!localDateTime.equals(that.localDateTime)) return false;
		if (!longVal.equals(that.longVal)) return false;
		if (!shortVal.equals(that.shortVal)) return false;
		if (!name.equals(that.name)) return false;
		return zonedDateTime.equals(that.zonedDateTime);
	}

	@Override
	public int hashCode() {
		int result = bigdecimal.hashCode();
		result = 31 * result + biginteger.hashCode();
		result = 31 * result + booleanVal.hashCode();
		result = 31 * result + doubleVal.hashCode();
		result = 31 * result + floatVal.hashCode();
		result = 31 * result + integer.hashCode();
		result = 31 * result + localDate.hashCode();
		result = 31 * result + localDateTime.hashCode();
		result = 31 * result + longVal.hashCode();
		result = 31 * result + shortVal.hashCode();
		result = 31 * result + name.hashCode();
		result = 31 * result + zonedDateTime.hashCode();
		return result;
	}

	@Override
	public int getNextSortIndex() {
		return nextSortIndex++;
	}

	@Override
	public boolean matches(TestObject object) {
		return true;
	}
}
