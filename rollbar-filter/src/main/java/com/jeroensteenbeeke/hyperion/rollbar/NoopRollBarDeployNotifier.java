package com.jeroensteenbeeke.hyperion.rollbar;

import com.jeroensteenbeeke.lux.ActionResult;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Stub that does not actually communicate with Rollbar
 */
public class NoopRollBarDeployNotifier implements IRollBarDeployNotifier {
	@Override
	public ActionResult notifyDeploy(@NotNull String revision, @Nullable String comment) {
		return ActionResult.ok();
	}
}
