<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
				 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
				 xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>com.jeroensteenbeeke</groupId>
		<artifactId>jeroensteenbeeke-project-parent</artifactId>
		<version>2.0-SNAPSHOT</version>
	</parent>

	<artifactId>hyperion-parent</artifactId>
	<version>4.0-SNAPSHOT</version>
	<packaging>pom</packaging>
	<name>Hyperion Parent</name>

	<licenses>
		<license>
			<name>GNU LESSER GENERAL PUBLIC LICENSE</name>
			<url>https://www.gnu.org/licenses/lgpl-3.0.txt</url>
			<distribution>repo</distribution>
			<comments>Version 3, 29 June 2007</comments>
		</license>
	</licenses>

	<developers>
		<developer>
			<name>Jeroen Steenbeeke</name>
			<roles>
				<role>Developer</role>
			</roles>
		</developer>
	</developers>

	<repositories>
		<repository>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
			<id>central</id>
			<name>libs-release</name>
			<url>https://artifactory.jeroensteenbeeke.nl/artifactory/libs-release</url>
		</repository>
		<repository>
			<snapshots/>
			<id>snapshots</id>
			<name>libs-snapshot</name>
			<url>https://artifactory.jeroensteenbeeke.nl/artifactory/libs-snapshot</url>
		</repository>
	</repositories>
	<pluginRepositories>
		<pluginRepository>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
			<id>central</id>
			<name>libs-release</name>
			<url>https://artifactory.jeroensteenbeeke.nl/artifactory/libs-release</url>
		</pluginRepository>
		<pluginRepository>
			<snapshots/>
			<id>snapshots</id>
			<name>libs-snapshot</name>
			<url>https://artifactory.jeroensteenbeeke.nl/artifactory/libs-snapshot</url>
		</pluginRepository>
	</pluginRepositories>

	<distributionManagement>
		<repository>
			<id>repo-jeroensteenbeeke-releases</id>
			<name>repo-jeroensteenbeeke-releases</name>
			<url>https://artifactory.jeroensteenbeeke.nl/artifactory/libs-release-local</url>
		</repository>
		<snapshotRepository>
			<id>repo-jeroensteenbeeke-snapshots</id>
			<name>repo-jeroensteenbeeke-snapshots</name>
			<url>https://artifactory.jeroensteenbeeke.nl/artifactory/libs-snapshot-local</url>
		</snapshotRepository>
	</distributionManagement>

	<modules>
		<!-- Basic Stuff -->
		<module>core</module>

		<!--BBCode -->
		<module>bbcode</module>

		<!-- Metrics -->
		<module>metrics</module>

		<!-- Basic Data Handling Components -->
		<module>meld</module>

		<!-- Hibernate + Spring -->
		<module>solstice</module>

		<!-- Icon support -->
		<module>icons</module>

		<!-- Scheduler -->
		<module>tardis</module>

		<!-- Auth support -->
		<module>auth</module>

		<!-- Command line Tool library -->
		<module>cmdline</module>

		<!-- Command line colors -->
		<module>color</module>

		<!-- Social Logins -->
		<module>social</module>

		<!-- Standalone execution -->
		<module>solitary</module>

		<!-- Rollbar -->
		<module>rollbar-filter</module>

		<!-- Code generators and annotation processors -->
		<module>generators</module>

		<!-- Test stuff -->
		<module>testbase</module>

		<!-- Logging -->
		<module>logging</module>

		<!-- Rest Contract Support -->
		<module>rest</module>

		<!-- Webcomponents -->
		<module>webcomponents</module>

		<!-- IOC -->
		<module>ioc</module>
	</modules>

	<properties>
		<argLine>-Djdk.net.URLClassPath.disableClassPathURLCheck=true --enable-preview</argLine>
	</properties>


	<build>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>com.mycila</groupId>
					<artifactId>license-maven-plugin</artifactId>
					<executions>
						<execution>
							<id>check-license</id>
							<phase>compile</phase>
							<goals>
								<goal>check</goal>
							</goals>
							<inherited>false</inherited>
							<configuration>
							</configuration>
						</execution>
					</executions>
					<configuration>
						<header>header.txt</header>
						<includes>
							<include>**/src/**/*.java</include>
							<include>**/src/**/*.js</include>
						</includes>
						<excludes>
							<exclude>**/jquery.textarea.caret.js</exclude>
							<exclude>**/jquery.*.js</exclude>
							<exclude>**/jquery.js</exclude>
							<exclude>**/jquery.*.min.js</exclude>
							<exclude>**/jqplot.*.js</exclude>
							<exclude>**/less-*.js</exclude>
							<exclude>**/less-*.min.js</exclude>
							<exclude>**/excanvas.js</exclude>
							<exclude>**/excanvas.min.js</exclude>
							<exclude>**/bootstrap.js</exclude>
							<exclude>**/bootstrap.min.js</exclude>
							<exclude>**/bootstrap-datepicker.js</exclude>
							<exclude>**/bootstrap-datepicker.*.js</exclude>
						</excludes>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-checkstyle-plugin</artifactId>
					<configuration>
						<configLocation>${maven.multiModuleProjectDirectory}/checkstyle.xml</configLocation>
						<suppressionsLocation>${maven.multiModuleProjectDirectory}/checkstyle-suppressions.xml</suppressionsLocation>
					</configuration>
					<executions>
						<execution>
							<id>check-code-style</id>
							<phase>verify</phase>
							<goals>
								<goal>check</goal>
							</goals>
						</execution>
					</executions>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-dependency-plugin</artifactId>
					<executions>
						<execution>
							<id>determine-unused</id>
							<goals>
								<goal>analyze</goal>
							</goals>
							<phase>process-sources</phase>
							<configuration>
								<failOnWarning>true</failOnWarning>
								<ignoredUsedUndeclaredDependencies>
									<ignoredUsedUndeclaredDependency>org.mockito:mockito-core:*:*</ignoredUsedUndeclaredDependency>
								</ignoredUsedUndeclaredDependencies>
								<ignoredUnusedDeclaredDependencies>
									<!-- Initialized through serviceloader -->
									<dependency>com.github.codeteapot.maven.plugin-testing:maven-plugin-testing-harness-plexus</dependency>
									<!-- Initialized through reflection and non-classloading mechanisms -->
									<dependency>com.h2database:h2:*:*</dependency>
									<!-- Used by annotation processors, no reference from code -->
									<dependency>com.jeroensteenbeeke:hyperion-reflectable-generator:*:*</dependency>
									<!-- Used by annotation processors, no reference from code -->
									<dependency>com.jeroensteenbeeke:hyperion-filtergen:*:*</dependency>
									<!-- Used by annotation processors, no reference from code -->
									<dependency>com.jeroensteenbeeke:hyperion-buildergen:*:*</dependency>
									<!-- Used indirectly, referenced from compile testing -->
									<dependency>com.jeroensteenbeeke:hyperion-legacy-junit-api-shims</dependency>
									<!-- Used by annotation processors, no reference from code -->
									<dependency>com.jeroensteenbeeke:hyperion-ioc-generator:*:*</dependency>
									<!-- Needed because jakarta RS-API will try to load the Priority annotation -->
									<dependency>jakarta.annotation:jakarta.annotation-api:*:*</dependency>
									<!-- Needed as a test dependency of hyperion-logging -->
									<dependency>jakarta.activation:jakarta.activation-api:*:*</dependency>

									<!-- Implied by the use of Wicket, needed for any Wicket-related tests even if not referenced directly -->
									<dependency>jakarta.servlet:jakarta.servlet-api</dependency>
									<!-- Initialized through reflection/serviceloaders -->
									<dependency>org.glassfish.jaxb:jaxb-runtime:*:*</dependency>
									<!-- Optional dependency of spring-orm invoked during test -->
									<dependency>org.hibernate:hibernate-core:*:*</dependency>
									<!-- Used by annotation processors, no reference from code -->
									<dependency>org.hibernate:hibernate-jpamodelgen:*:*</dependency>
									<!-- Needed because jakarta RS-API will default to Jersey if this dependency is not present -->
									<dependency>org.jboss.resteasy:resteasy-client:*:*</dependency>
									<!-- Needed because the unit tests won't do anything without the engine -->
									<dependency>org.junit.jupiter:junit-jupiter-engine:*:*</dependency>
									<!-- Used indirectly by Spring, actual dependency is excluded -->
									<dependency>org.slf4j:jcl-over-slf4j:*:*</dependency>
									<!-- Initiated through service loader, not used directly -->
									<dependency>org.slf4j:slf4j-simple:*:*</dependency>
									<!-- Transitive dependency filtered out by an exclusion -->
									<dependency>commons-io:commons-io:*:*</dependency>

								</ignoredUnusedDeclaredDependencies>
								<ignoredNonTestScopedDependencies>
									<dependency>org.jetbrains:annotations:*:*</dependency>
									<dependency>io.vavr:vavr:*:*</dependency>
									<!-- Compile-time dependency of DAO superclass -->
									<dependency>jakarta.persistence:jakarta.persistence-api</dependency>
								</ignoredNonTestScopedDependencies>
							</configuration>
						</execution>
					</executions>
				</plugin>
			</plugins>
		</pluginManagement>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-checkstyle-plugin</artifactId>
			</plugin>
<!--			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-dependency-plugin</artifactId>
</plugin> -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-enforcer-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>com.mycila</groupId>
				<artifactId>license-maven-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
			</plugin>

		</plugins>
	</build>

	<profiles>
		<profile>
			<id>enable-owasp</id>
			<build>
				<plugins>
					<plugin>
						<groupId>org.owasp</groupId>
						<artifactId>dependency-check-maven</artifactId>
						<inherited>false</inherited>
						<executions>
							<execution>
								<goals>
									<goal>aggregate</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
</project>

