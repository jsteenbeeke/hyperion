#!/bin/bash

set -e

MAIN_TARGET=$(pwd)/target

PREFIX=">>>"
SUFFIX="<<<"

VERSION=`cat pom.xml | grep \<hyperion.version | awk '{gsub(/<[^>]*>/,"")};1' | sed 's/[[:blank:]]//g'`

echo $PREFIX Version: $VERSION $SUFFIX

CLEANUP_PATH="$HOME/.m2/repository/com/jeroensteenbeeke/test"
CLEANUP="parent rest-data rest-contract retrofit-rest-contract backend frontend core"
MAVEN_OPTS=-client

MAVEN_COMMAND="mvn-custom -X -e"

if hash mvn-custom 2>/dev/null; then
        MAVEN_COMMAND="mvn-custom"
else
        MAVEN_COMMAND="mvn"
fi

echo Maven command: $MAVEN_COMMAND

function andalite {
	INPUTFILE=forge-input.txt
	if [[ -e $INPUTFILE ]]; then
		rm $INPUTFILE
	fi
	for i in $@; do
		echo $i >> $INPUTFILE
	done	
	echo Andalite input:
	echo ---
	cat $INPUTFILE
	echo ---

	$MAVEN_COMMAND andalite:forge -Dforge.input=$INPUTFILE -e
	rm $INPUTFILE
}

function makeUserObjects {
	ANDALITE_NEW_ENTITY_FIRST_RUN=1
	ANDALITE_NEW_ENTITY=2
	ANDALITE_ADD_FIELD=1
	ANDALITE_LINK_ENTITY=3
	ANDALITE_EXTEND_ENTITY=4
	ANDALITE_CREATE_BEAN=5

	NOT_COMPARABLE=false
	COMPARABLE=true

	ENTITIES_PACKAGE=1

	JOINED=1

	NEWFILE=1
	NAME_BASELINE=baseline

	BASELINE=1

	andalite $ANDALITE_NEW_ENTITY_FIRST_RUN BackendUser $ENTITIES_PACKAGE $NOT_COMPARABLE $NEWFILE $NAME_BASELINE
	andalite $ANDALITE_NEW_ENTITY LoginMethod $ENTITIES_PACKAGE $NOT_COMPARABLE $BASELINE

	ENTITY_BACKENDUSER=1
	ENTITY_LOGINMETHOD=2

	andalite $ANDALITE_EXTEND_ENTITY $ENTITY_LOGINMETHOD $JOINED OldPasswordLoginMethod $ENTITIES_PACKAGE $NOT_COMPARABLE $BASELINE
	andalite $ANDALITE_EXTEND_ENTITY $ENTITY_LOGINMETHOD PasswordLoginMethod $ENTITIES_PACKAGE $NOT_COMPARABLE $BASELINE

	ENTITY_OLDPASSWORDMETHOD=3
	ENTITY_PASSWORDMETHOD=4

	TYPE_STRING=13
	TYPE_BOOLEAN=2

	NULLABLE=true
	NOT_NULLABLE=false

	REQUIRED_IN_FILTER=true
	NOT_REQUIRED_IN_FILTER=false

	SEARCHABLE=true
	NOT_SEARCHABLE=false

	MANY_TO_ONE=1
	ONE_TO_MANY=2

	COMPONENT_TYPE=1
	SERVICE_TYPE=2

	andalite $ANDALITE_ADD_FIELD $ENTITY_BACKENDUSER username $TYPE_STRING $NOT_NULLABLE $BASELINE
	andalite $ANDALITE_ADD_FIELD $ENTITY_BACKENDUSER admin $TYPE_BOOLEAN $BASELINE
	andalite $ANDALITE_ADD_FIELD $ENTITY_OLDPASSWORDMETHOD hashedPassword $TYPE_STRING $NOT_NULLABLE $BASELINE
	andalite $ANDALITE_ADD_FIELD $ENTITY_PASSWORDMETHOD hashedPassword $TYPE_STRING $NOT_NULLABLE $BASELINE
	andalite $ANDALITE_ADD_FIELD $ENTITY_PASSWORDMETHOD salt $TYPE_STRING $NOT_NULLABLE $BASELINE
	andalite $ANDALITE_LINK_ENTITY $ENTITY_LOGINMETHOD $ENTITY_BACKENDUSER $MANY_TO_ONE user loginMethods $NOT_NULLABLE $BASELINE $SEARCHABLE $REQUIRED_IN_FILTER

	PACKAGE_DEFAULT=1

	andalite $ANDALITE_CREATE_BEAN UserService $PACKAGE_DEFAULT $SERVICE_TYPE
	andalite $ANDALITE_CREATE_BEAN UserLogic $PACKAGE_DEFAULT $COMPONENT_TYPE
}

function makeJobs {
	JOB_OPTION=$1
	GROUP_OPTION=$[ $JOB_OPTION - 1 ]
	DEFAULT_GROUP=1
	DEFAULT_PACKAGE=1
	SCHEDULED=true
	NOT_SCHEDULED=false

	SECS=1
	MINUTES=2
	HOURS=3
	DAYS=4	

	andalite $GROUP_OPTION TestGroup Test
	andalite $JOB_OPTION TestJob $DEFAULT_PACKAGE Testing $DEFAULT_GROUP $SCHEDULED $SECS 10
}

function makeWicketPagesAndComponents {
	PAGE_OPTION=$1
	PANEL_OPTION=$2
	DEFAULT_PACKAGE=1
	DEFAULT_WICKET_PAGE=2
	DEFAULT_PROJECT_PAGE=1
	DEFAULT_PANEL=1
	TYPED_PANEL=2

	andalite $PAGE_OPTION InstallPage $DEFAULT_PACKAGE $DEFAULT_WICKET_PAGE
	andalite $PAGE_OPTION UserDashboardPage $DEFAULT_PACKAGE $DEFAULT_PROJECT_PAGE
	andalite $PANEL_OPTION TestPanel $DEFAULT_PACKAGE $DEFAULT_PANEL
	andalite $PANEL_OPTION TypedTestPanel $DEFAULT_PACKAGE $TYPED_PANEL
}

function makeBackendJobs {
	ANDALITE_SPRING_JOB=7
	makeJobs $ANDALITE_SPRING_JOB	
}

function makeFrontendJobs {
	ANDALITE_WICKET_JOB=3
	makeJobs $ANDALITE_WICKET_JOB
}

function makeStandaloneJobs {
	ANDALITE_WICKET_JOB=7
	makeJobs $ANDALITE_WICKET_JOB
}

function makeRestStuff {
	OPTION_SERVICE=1
	OPTION_OBJECT=2
	OPTION_PROPERTY=3
	OPTION_RELATION=4	

	REST_PACKAGE=1
	
	TYPE_STRING=13
	TYPE_BOOLEAN=2
	
	NULLABLE=true
	NOT_NULLABLE=false
	
	MANY_TO_ONE=1
	ONE_TO_MANY=2

	OPTIONAL=true
	REQUIRED=false

	andalite $OPTION_SERVICE UserResource /users
	andalite $OPTION_OBJECT RUser $REST_PACKAGE
	andalite $OPTION_OBJECT RForum $REST_PACKAGE
	andalite $OPTION_OBJECT RForumPost $REST_PACKAGE

	OBJECT_FORUM=1
	OBJECT_FORUM_POST=2
	OBJECT_USER=3

	andalite $OPTION_PROPERTY $OBJECT_FORUM name $TYPE_STRING $NOT_NULLABLE
	andalite $OPTION_PROPERTY $OBJECT_FORUM_POST text $TYPE_STRING $NOT_NULLABLE
	andalite $OPTION_RELATION $OBJECT_FORUM_POST $OBJECT_FORUM $MANY_TO_ONE forum posts $REQUIRED
}

function healthCheck {
	WD=$1
	HEALTH_PORT=$2
	HEALTH_PREFIX=$3
	CD=$4

	echo Performing health check
	CURL_STAT=$(curl -s -o /dev/null -w '%{http_code}' http://localhost:$HEALTH_PORT$HEALTH_PREFIX/health)

	if [[ $CURL_STAT != 200 ]]; then
		echo "Health check failed"
		cat $WD/execution.log
		stopProject health_check_failed
		exit 1
	fi
}

function startProject {
	WD=$1
	MAIN_CLASS=$2
	tmux new-session -d -s hyperion-quickstart "$MAVEN_COMMAND --log-file=$WD/execution.log exec:java -Dexec.mainClass=\"$MAIN_CLASS\" -Dexec.classpathScope=test -e -Dsun.net.spi.nameservice.nameservers=localhost:8600,8.8.8.8,8.8.4.4"
}

function stopProject {
	tmux kill-session -t hyperion-quickstart
}

function waitForService {
	set +e

	WD=$1
	CD=$2
	
	printf "Waiting for service to start..."

	TIMEOUT=45
	SERVICE_STATUS=$([[ -f $WD/execution.log ]] && cat $WD/execution.log | grep "STARTED STANDALONE JETTY SERVER")

	while [[ "$SERVICE_STATUS" == "" ]] && [[ "$TIMEOUT" != 0 ]]; do
		printf .
		sleep 1
		TIMEOUT=$((TIMEOUT-1))
		SERVICE_STATUS=$([[ -f $WD/execution.log ]] && cat $WD/execution.log | grep "STARTED STANDALONE JETTY SERVER")
	done

	if [[ $SERVICE_STATUS == "" ]]; then
		echo TIMEOUT
		stopProject i_was_waiting_for_service
		exit 1
	else
		echo OK, entering 10 second grace period to allow background tasks to trigger

		sleep 10
	fi
}

function checkBackgroundTask {
	WD=$1
	echo Checking execution log
	OUT=$(cat $WD/execution.log | grep "Completed background task Test:Testing")

	if [[ $OUT == "" ]]; then
		echo "Service did not start up successfully"
		cat $WD/execution.log
		stopProject
		exit 1
	fi
}

echo "####################"
echo "# Hyperion compile #"
echo "####################"

	echo $PREFIX Hyperion $VERSION $SUFFIX
if [[ "$1" == "skipcompile" ]]; then
	echo -- Skipping compile
elif [[ "$1" == "qsonly" ]]; then
	echo -- Rebuilding quickstarts only
	./rebuild-quickstarts.sh
else
	echo -- Complete maven rebuild
	$MAVEN_COMMAND clean install
fi

if [[ -d target ]]; then
	rm -rf target
fi

mkdir -p target
cd target


echo "###########"
echo "# SERVICE #"
echo "###########"

echo $PREFIX CREATING SERVICE PROJECT $SUFFIX
$MAVEN_COMMAND archetype:generate \
    -DarchetypeCatalog=local \
     -DarchetypeGroupId=com.jeroensteenbeeke \
     -DarchetypeArtifactId=hyperion-quickstart-service \
     -DarchetypeVersion=$VERSION \
     -DgroupId=com.jeroensteenbeeke.test \
     -DartifactId=service-test \
     -Dversion=$VERSION \
     -DtestServicePort=8081 \
     -DcontextPath=/service-test \
     -DinteractiveMode=false

# Andalite Forge test
echo $PREFIX TESTING BARE COMPILATION $SUFFIX
cd service-test
$MAVEN_COMMAND clean package -e
echo $PREFIX GENERATING SOME CLASSES $SUFFIX
makeRestStuff
cd service
makeUserObjects
makeBackendJobs
cd ..

echo $PREFIX BUILDING SERVICE PROJECT $SUFFIX
$MAVEN_COMMAND clean install -e
echo $PREFIX RUNNING SERVICE BACKEND $SUFFIX
cd service
startProject $(pwd) com.jeroensteenbeeke.test.service.StartService
waitForService $(pwd) $(dirname $(pwd))
checkBackgroundTask $(pwd)
stopProject
cd ..

echo "##############"
echo "# STANDALONE #"
echo "##############"

echo $PREFIX CREATING STANDALONE PROJECT $SUFFIX
$MAVEN_COMMAND archetype:generate \
    -DarchetypeCatalog=local \
    -DarchetypeGroupId=com.jeroensteenbeeke \
    -DarchetypeArtifactId=hyperion-quickstart-standalone \
    -DarchetypeVersion=$VERSION \
    -DgroupId=com.jeroensteenbeeke.test \
    -DartifactId=standalone-test \
    -Dversion=$VERSION \
    -DapplicationName=StandaloneTestApplication \
    -DinteractiveMode=false

# Build and invoke standalone
cd standalone-test
echo $PREFIX TESTING BARE COMPILATION $SUFFIX
$MAVEN_COMMAND clean package -e
echo $PREFIX GENERATING SOME CLASSES $SUFFIX
makeUserObjects
makeStandaloneJobs
makeWicketPagesAndComponents 8 9
echo $PREFIX BUILDING STANDALONE PROJECT $SUFFIX
$MAVEN_COMMAND clean install -e
echo $PREFIX RUNNING STANDALONE PROJECT $SUFFIX
startProject $(pwd) com.jeroensteenbeeke.test.frontend.StartStandaloneTestApplication
waitForService $(pwd) $(dirname $(pwd))
checkBackgroundTask $(pwd)
healthCheck $(pwd) 8081 /service-test $(dirname $(pwd))
stopProject standalone
cd ..


echo "###############"
echo "# MULTIMODULE #"
echo "###############"

echo $PREFIX CREATING MULTIMODULE PROJECT $SUFFIX
$MAVEN_COMMAND archetype:generate -DarchetypeCatalog=local -DarchetypeGroupId=com.jeroensteenbeeke -DarchetypeArtifactId=hyperion-quickstart-multimodule -DarchetypeVersion=$VERSION -DgroupId=com.jeroensteenbeeke.test -DartifactId=multimodule-test -Dversion=$VERSION -DapplicationName=MultimoduleTestApplication -DinteractiveMode=false

# Andalite Forge test
echo $PREFIX TESTING BARE COMPILATION $SUFFIX
cd multimodule-test
$MAVEN_COMMAND clean package -e
echo $PREFIX GENERATING SOME CLASSES $SUFFIX
makeRestStuff
cd backend
makeUserObjects
makeBackendJobs
cd ..
cd frontend
makeFrontendJobs
makeWicketPagesAndComponents 4 5
cd ..

echo $PREFIX BUILDING MULTIMODULE PROJECT $SUFFIX
$MAVEN_COMMAND clean install -e
echo $PREFIX RUNNING MULTIMODULE BACKEND $SUFFIX
cd backend
startProject $(pwd) com.jeroensteenbeeke.test.backend.StartTestBackend
waitForService $(pwd) $(dirname $(pwd))
checkBackgroundTask $(pwd)
stopProject multimodule-backend
cd ../frontend
startProject $(pwd) com.jeroensteenbeeke.test.frontend.StartTestFrontend
waitForService $(pwd) $(dirname $(pwd))
checkBackgroundTask $(pwd)
stopProject multimodule-frontend

echo "###########"
echo "# CLEANUP #"
echo "###########"

echo $PREFIX CLEANUP $SUFFIX
cd ../..
rm -rf multimodule-test standalone-test microservice-parent-test
for i in $CLEANUP; do
	rm -rf $CLEANUP_PATH/multimodule-test-$i/
done
cd ..

